﻿using AnraUssServices.BackgroundServices;
using AnraUssServices.Common;
using AnraUssServices.Common.Airspace;
using AnraUssServices.Common.Billing;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.HealthChecks;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.Kafka;
using AnraUssServices.Common.Notification;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.Security;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Common.Validators;
using AnraUssServices.Converters;
using AnraUssServices.Data;
using AnraUssServices.Data.Seeding;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Models.Audits;
using AnraUssServices.Models.Detects;
using AnraUssServices.Models.DetectSubscribers;
using AnraUssServices.Models.Signup;
using AnraUssServices.Sockets;
using AnraUssServices.Utility;
using AnraUssServices.Utm.Api;
using AnraUssServices.WebSocketManager;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using HostedServices = Microsoft.Extensions.Hosting;

namespace AnraUssServices
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddApplicationInsightsSettings(developerMode: env.IsDevelopment());

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();

            TypeConverters.Configure();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            AnraConfiguration.Configure(services, Configuration);
            var environment = Configuration.GetSection("ProductSettings").GetValue<string>("Environment");
			var fimsIssuer = Configuration.GetSection("FIMSSettings").GetValue<string>("FIMSIssuer");

            //To Do : Currently Issuer is not in place
            //We can add "AnraTechnologies" as valid issuer in oAuth token response
            //Will decide later so for now it just a dummy value and have no impact
            string anraIssuer = "AnraTechnologies";

			services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    Configuration.GetConnectionString("DataAccessPostgreSqlProvider"),
                    b => b.MigrationsAssembly(nameof(AnraUssServices))
                )
            .EnableSensitiveDataLogging(Configuration.GetSection("Logging").GetValue<bool>("EnableSensitiveDataLogging"))
            );

            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.IncludeErrorDetails = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuers = new string[] { anraIssuer },
                    ValidateLifetime = true,
                    ValidateAudience = false,
                    ValidateIssuer = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKeys = new List<SecurityKey> { SecurityKeysStore.GetAnraPublicKey(environment), SecurityKeysStore.GetFimsSecurityKey() }
                };
            });

            services.AddAuthorization(options =>
            {
                //oAuth Policies
                options.AddPolicy("uss:read-drone", policy => policy.Requirements.Add(new HasScope("uss.anra.read.drone", anraIssuer)));
                options.AddPolicy("uss:write-drone", policy => policy.Requirements.Add(new HasScope("uss.anra.write.drone", anraIssuer)));
                options.AddPolicy("uss:read-dashboard", policy => policy.Requirements.Add(new HasScope("uss.anra.read.dashboard", anraIssuer)));
                options.AddPolicy("uss:read-detection", policy => policy.Requirements.Add(new HasScope("uss.anra.read.detection", anraIssuer)));
                options.AddPolicy("uss:write-detection", policy => policy.Requirements.Add(new HasScope("uss.anra.write.detection" , anraIssuer)));
                options.AddPolicy("uss:read-message", policy => policy.Requirements.Add(new HasScope("uss.anra.read.message" , anraIssuer)));
                options.AddPolicy("uss:write-message", policy => policy.Requirements.Add(new HasScope("uss.anra.write.message" , anraIssuer)));
                options.AddPolicy("uss:read-constraint", policy => policy.Requirements.Add(new HasScope("uss.anra.read.constraint" , anraIssuer)));
                options.AddPolicy("uss:write-constraint", policy => policy.Requirements.Add(new HasScope("uss.anra.write.constraint" , anraIssuer)));

                // by ravi
                options.AddPolicy("uss:read-anra-operation", policy => policy.Requirements.Add(new HasScope("uss.anra.read.operation" , anraIssuer)));
                options.AddPolicy("uss:write-anra-operation", policy => policy.Requirements.Add(new HasScope("uss.anra.write.operation" , anraIssuer)));
                options.AddPolicy("uss:read-uvr", policy => policy.Requirements.Add(new HasScope("uss.anra.read.constraint" , anraIssuer)));
                options.AddPolicy("uss:write-uvr", policy => policy.Requirements.Add(new HasScope("uss.anra.write.constraint" , anraIssuer)));
                options.AddPolicy("uss:read-urep", policy => policy.Requirements.Add(new HasScope("uss.anra.read.urep" , anraIssuer)));
                options.AddPolicy("uss:write-urep", policy => policy.Requirements.Add(new HasScope("uss.anra.write.urep" , anraIssuer)));
                // options.AddPolicy("uss:read-uss", policy => policy.Requirements.Add(new HasScope("uss.anra.read.uss" }, anraIssuer)));
                // options.AddPolicy("uss:write-uss", policy => policy.Requirements.Add(new HasScope("uss.anra.write.uss" }, anraIssuer)));
                options.AddPolicy("uss:read-telemetry", policy => policy.Requirements.Add(new HasScope("uss.anra.read.position" , anraIssuer)));
                options.AddPolicy("uss:write-telemetry", policy => policy.Requirements.Add(new HasScope("uss.anra.write.position", anraIssuer)));
                options.AddPolicy("uss:read-utm", policy => policy.Requirements.Add(new HasScope("uss.anra.read.uss", anraIssuer)));
                options.AddPolicy("uss:write-utm", policy => policy.Requirements.Add(new HasScope("uss.anra.write.uss", anraIssuer)));
                options.AddPolicy("uss:read-viewer", policy => policy.Requirements.Add(new HasScope("uss.anra.read.operation", anraIssuer)));
                options.AddPolicy("uss:write-viewer", policy => policy.Requirements.Add(new HasScope("uss.anra.write.operation", anraIssuer)));
                options.AddPolicy("uss:read-audit", policy => policy.Requirements.Add(new HasScope("uss.anra.read.audit", anraIssuer)));
                options.AddPolicy("uss:read-billing", policy => policy.Requirements.Add(new HasScope("uss.anra.read.billing", anraIssuer)));

				//Note : USS Controller Policies
				// Do we need this or not will decide later
				options.AddPolicy("uss:read-operation", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.operation", fimsIssuer)));
                options.AddPolicy("uss:read-operation", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_read.publicsafety", fimsIssuer)));
                options.AddPolicy("uss:read-enhanced-operation", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_read.publicsafety", fimsIssuer)));
                options.AddPolicy("uss:put-enhanced-operation", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.publicsafety", fimsIssuer)));
                options.AddPolicy("uss:put-constraint", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.constraint", fimsIssuer)));
                options.AddPolicy("uss:put-message", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.message", fimsIssuer)));
                options.AddPolicy("uss:put-operation", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.operation", fimsIssuer)));
                options.AddPolicy("uss:put-uss", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.operation", fimsIssuer)));
                options.AddPolicy("uss:put-position", policy => policy.Requirements.Add(new HasScope("utm.nasa.gov_write.operation", fimsIssuer)));
            });

            services
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    options.SerializerSettings.DateParseHandling = DateParseHandling.None;
                    options.SerializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                    options.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffZ";
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new Common.Converters.CustomIntConverter());
                });

            services.AddWebSocketManager();

            AddCustomDependencies(services);

            services.AddHealthChecks(checks =>
            {
                checks.AddCheck<HealthCheckDatabase>("Database Communication");
                checks.AddCheck<HealthCheckBroker>("Broker Communication");
                checks.AddCheck<HealthCheckFimsService>("FIMS Communication");
                checks.AddCheck<HealthCheckUasService>("UAS Communication");
                checks.AddCheck<HealthCheckInterUssService>("InterUss Communication");
                checks.AddCheck<HealthCheckMapEngine>("MapEngine Communication");
                checks.AddCheck<HealthCheckOauthService>("Oauth Service");
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ANRA USS API", Version = "v1" });
                c.DescribeAllEnumsAsStrings();
                var basePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, Assembly.GetExecutingAssembly().GetName().Name + ".xml");
                c.IncludeXmlComments(basePath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMiddleware<ResponseLoggingMiddleware>();

            app.UseCors(b => b.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseAuthentication();

            app.UseSwagger();

            app.UseStatusCodePagesWithReExecute("/Error", "?status={0}");

            app.UseMvc();

            app.UseWebSockets();
            app.MapWebSocketManager("/telemetry", serviceProvider.GetService<TelemetryConnectionHandler>());

            app.UseExceptionHandler();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
            }

            await SeedData.EnsureSeedDataAsync(app.ApplicationServices);

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            var path = Configuration.GetSection("ProductSettings").GetValue<string>("UssServiceUrl");
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(path + "swagger/v1/swagger.json", "ANRA USS API");
            });
        }

        private void AddCustomDependencies(IServiceCollection services)
        {
            FluentScheduler.JobManager.Initialize();
            //FluentScheduler.JobManager.JobException += info => logger.LogError("An error just happened with a scheduled job: " + info.Exception);

            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);

            services.AddSingleton<TokenValidator>();
            services.AddSingleton<TelemetryConnectionHandler>();
            services.AddSingleton<OperationMqttNotification>();
            services.AddSingleton<GenericMqttNotification>();
            services.AddSingleton<ViewerMqttNotification>();
            services.AddSingleton<HealthCheckDiskspace>();
            services.AddSingleton<HealthCheckDatabase>();
            services.AddSingleton<HealthCheckBroker>();
            services.AddSingleton<HealthCheckFimsService>();
            services.AddSingleton<HealthCheckUasService>();
            services.AddSingleton<HealthCheckInterUssService>();
            services.AddSingleton<HealthCheckMapEngine>();
            services.AddSingleton<HealthCheckOauthService>();
            services.AddSingleton<AnraMqttClient>();
            services.AddSingleton<ElevationApi>();

			services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<UtmMessenger>();
            services.AddSingleton<HttpClientLogger>();
            services.AddScoped<AnraHttpClient>();
            services.AddSingleton<FimsTokenProvider>();

            services.AddScoped<IRepository<Models.Operation>, OperationRepository>();
            services.AddScoped<IRepository<OperationVolume>, OperationVolumeRepository>();
            services.AddScoped<IRepository<ConstraintMessage>, ConstraintMessageRepository>();
            services.AddScoped<IRepository<UtmMessage>, UtmMessageRepository>();
            services.AddScoped<IRepository<NegotiationMessage>, NegotiationMessageRepository>();
            services.AddScoped<IRepository<UtmInstance>, UtmInstanceRepository>();
            services.AddScoped<IRepository<LocalNetwork>, LocalNetworkRepository>();
            services.AddScoped<IRepository<TelemetryMessage>, TelemetryMessageRepository>();
            services.AddScoped<IRepository<Drone>, DroneRepository>();
            services.AddScoped<IRepository<ContingencyPlan>, ContingencyPlanRepository>();
            services.AddScoped<IRepository<PriorityElement>, PriorityElementRepository>();            
            services.AddScoped<IRepository<Notam>, NotamRepository>();
            services.AddScoped<IRepository<NotamArea>, NotamAreaRepository>();
            services.AddScoped<IRepository<Pointout>, PointoutRepository>();
            services.AddScoped<IRepository<Urep>, UrepRepository>();
            services.AddScoped<IRepository<UasRegistration>, UasRegistrationRepository>();

            services.AddScoped<DatabaseFunctions>();
            services.AddScoped<OperationValidator>();
            services.AddScoped<UtmValidator>();
            services.AddScoped<UtmMessageValidator>();
            services.AddScoped<PositionValidator>();
            services.AddScoped<UvrValidator>();
            services.AddScoped<OperationVolumeValidator>();            
            services.AddScoped<NotamClient>();
            services.AddScoped<UssDiscoveryApi>();
            services.AddScoped<UssVehicleRegistrationApi>();
            services.AddScoped<UrepApi>();
			services.AddScoped<RestrictionApi>();

			services.AddScoped<IRepository<VehicleData>, VehicleDataRepository>();
            services.AddScoped<IRepository<VehicleClassDetail>, VehicleClassDetailRepository>();
            services.AddScoped<IRepository<VehicleEngineDetail>, VehicleEngineDetailRepository>();
            services.AddScoped<IRepository<VehicleProperty>, VehiclePropertyRepository>();
            services.AddScoped<IRepository<VehicleRegistration>, VehicleRegistrationRepository>();
            services.AddScoped<IRepository<VehicleType>, VehicleTypeRepository>();
            services.AddScoped<IRepository<UserDroneAssociation>, UserDroneAssociationRepository>();
            services.AddScoped<IRepository<Detect>, DetectRepository>();
            services.AddScoped<IRepository<OperationConflict>, OperationConflictRepository>();
            services.AddScoped<IRepository<DetectSubscriber>, DetectSubscriberRepository>();
            services.AddScoped<IRepository<Models.Collisions.Collision>, CollisionRepository>();
            services.AddScoped<IRepository<NegotiationAgreement>, NegotiationAgreementRepository>();
            services.AddScoped<IRepository<ServiceArea>, ServiceAreaRepository>();
			services.AddScoped<IRepository<Signup>,SignupRepository>();

			services.AddScoped<OperationStatusLogic>();
            services.AddScoped<NegotiationApi>();
            services.AddScoped<UssApi>();
			services.AddScoped<VolumeSplitHelper>();
			services.AddScoped<UtmMessagesHelper>();
			services.AddScoped<OperationSeverityHelper>();
			services.AddScoped<AirSpaceIntersectionValidator>();
			services.AddScoped<ReplanGeographyHelper>();
			

			//LiteDB
			ConfigureLiteDB(services);

            //InterUss Dependencies Block
            services.AddScoped<InterUssApi>();
            services.AddScoped<InterUssHelper>();
            services.AddScoped<SlippyTiles>();
            services.AddScoped<GridCell>();


            services.AddSingleton<JwsHelper>();
            services.AddSingleton<Common.Mqtt.MqttLogger>();
            services.AddSingleton<IHostedService, BackgroundServices.SchedulingService>();
            //services.AddSingleton<IHostedService, BackgroundServices.FimsServerHealthStatusService>();

            services.AddScoped<BackgroundJobs.Operation.OperationStatusJob>();            

            services.AddScoped<ApiUssExchangeFilterAttribute>();
            services.AddScoped<RelmatechApi>();
            services.AddScoped<OperationSearchUtility>();
			services.AddScoped<OrganizationClient>();
			services.AddScoped<UserClient>();
			services.AddSingleton<SendNotification>();
			//services.AddSingleton<AuditClient>(); //will remove
			services.AddSingleton<BillingHelper>();
            services.AddSingleton<OAuthHttpClient>();
            services.AddSingleton<RoleScopeClient>(); 

            services.AddScoped<IRepository<Language>, LanguageRepository>();
            //services.AddScoped<IRepository<LocaleStringResource>, LocaleStringResourceRepository>(); //will be removed
            services.AddScoped<IRepository<LanguageResource>, LanguageResourceRepository>();
            services.AddScoped<IRepository<Issue>, IssueRepository>();
            services.AddScoped<IRepository<Audit>, AuditRepository>();

            services.AddSingleton<KafkaProducer>();
            services.AddSingleton<KafkaConsumer>();
            services.AddSingleton<IHostedService, KafkaBackgroundService>();

        }

        private void ConfigureLiteDB(IServiceCollection services)
        {
            services.AddSingleton<PositionDocument>();
            services.AddSingleton<LightDatabaseContext>();
            services.AddSingleton<OperationDocument>();
            services.AddSingleton<DroneDocument>();
            services.AddSingleton<UssInstanceDocument>();
            services.AddSingleton<DetectSubscribersDocument>();
            services.AddSingleton<CollisionDocument>();
        }
    }
}