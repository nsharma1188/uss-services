﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class OperationConflictRepository : IRepository<OperationConflict>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public OperationConflictRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(OperationConflict item)
        {
            _context.OperationConflicts.Add(item);
            _context.SaveChanges();
        }

        public OperationConflict Find(long key)
        {
            //return _context.OperationConflicts
            //    .FirstOrDefault(v => v.OperationConflictId == key);
            throw new NotImplementedException();
        }

        public OperationConflict Find(Guid key)
        {
            //return _context.OperationConflicts
            //    .AsNoTracking()
            //    .First(t => t.Gufi == key);
            return _context.OperationConflicts
                .FirstOrDefault(v => v.OperationConflictUid == key);
        }

        public IQueryable<OperationConflict> GetAll(string id)
        {
            return _context.OperationConflicts;
        }

        public OperationConflict GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(OperationConflict item)
        {
            throw new NotImplementedException();
        }
    }
}