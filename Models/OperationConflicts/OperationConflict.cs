﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class OperationConflict : ModelBase
    {
        //public int OperationConflictId { get; set; }
        [Key]
        public Guid OperationConflictUid { get; set; }

        public Guid ConflictingGufi { get; set; }

        public string ConflictingUssName { get; set; }

        public DateTime TimeStamp { get; set; }

        //public int OperationId { get; set; }

        public bool IsAnraPrimary { get; set; }

        [Column(TypeName = "jsonb")]
        public string ConflictDetails { get; set; }

        public Guid Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
        public virtual Operation Operation { get; set; }

    }
}