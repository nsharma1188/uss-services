﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class UrepRepository : IRepository<Urep>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public UrepRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Urep item)
        {
            _context.Ureps.Add(item);
            _context.SaveChanges();
        }

        public Urep Find(long key)
        {
            //return _context.Ureps
            //    //.Include(r => r.Pointout)
            //    .FirstOrDefault(V => V.UrepId == key);
            throw new NotImplementedException();
        }

        public Urep Find(Guid key)
        {
            return _context.Ureps
                //.Include(r => r.Pointout)
                .FirstOrDefault(V => V.UrepUid == key);
        }

        public IQueryable<Urep> GetAll(string EmailId)
        {
            return _context.Ureps;
        }

        public Urep GetNewItem()
        {
            return new Urep();
        }

        public void Remove(long key)
        {
            //var entity = _context.Ureps.First(t => t.UrepId == key);
            //_context.Ureps.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Ureps.First(t => t.UrepUid == key);
            _context.Ureps.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Urep item)
        {
            _context.Ureps.Update(item);
            _context.SaveChanges();
        }
    }
}