﻿using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class Urep : ModelBase
    {
        //public int UrepId { get; set; }
        [Key]
        public Guid UrepUid { get; set; }

        public double? AirTemperature { get; set; }

        public List<Pointout> AircraftSighting { get; set; }

        public double? Altitude { get; set; }

        public Guid Gufi { get; set; }

        public string IcingIntensity { get; set; }

        public string IcingType { get; set; }

        public NpgsqlPoint Location { get; set; }

        public string Proximity { get; set; }

        public string Remarks { get; set; }

        public string Source { get; set; }

        public DateTime? TimeSubmitted { get; set; }

        public DateTime? TimeMeasured { get; set; }

        public DateTime? TimeReceived { get; set; }

        public string TurbulenceIntensity { get; set; }

        public string UserId { get; set; }

        public double? Visibility { get; set; }

        public string Weather { get; set; }

        public string WeatherIntensity { get; set; }

        public double? WindDirection { get; set; }

        public int? WindSpeed { get; set; }

        public Guid? OrganizationId { get; set; }

        public string GroupId { get; set; }
    }
}