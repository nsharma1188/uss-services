﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class TelemetryMessageRepository : IRepository<TelemetryMessage>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public TelemetryMessageRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(TelemetryMessage item)
        {
            _context.TelemetryMessages.Add(item);
            _context.SaveChanges();
        }

        public TelemetryMessage Find(long key)
        {
            //return _context.TelemetryMessages.FirstOrDefault(V => V.TelemetryMessageId == key);
            throw new NotImplementedException();
        }

        public TelemetryMessage Find(Guid key)
        {
            string stringKey = key.ToString();
            return _context.TelemetryMessages.FirstOrDefault(V => V.Gufi == stringKey);
        }

        public IQueryable<TelemetryMessage> GetAll(string EmailId)
        {
            return _context.TelemetryMessages;
        }

        public TelemetryMessage GetNewItem()
        {
            return new TelemetryMessage();
        }

        public void Remove(long key)
        {
            //var entity = _context.TelemetryMessages.First(t => t.TelemetryMessageId == key);
            //_context.TelemetryMessages.Remove(entity);
            //_context.SaveChanges();
            
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            string stringKey = key.ToString();
            var entity = _context.TelemetryMessages.First(t => t.TelemetryMessageId == stringKey);
            _context.TelemetryMessages.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(TelemetryMessage item)
        {
            _context.TelemetryMessages.Update(item);
            _context.SaveChanges();
        }
    }
}