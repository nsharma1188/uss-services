﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class TelemetryMessage
    {
        [Key]
        public string TelemetryMessageId { get; set; }

        public string AltitudeGps { get; set; }

        public int AltitudeNumGpsSatellites { get; set; }

        public int? BatteryRemaining { get; set; }

        public double? Climbrate { get; set; }

        public string DiscoveryReference { get; set; }

        public string EnroutePositionsId { get; set; }

        public string Gufi { get; set; }

        public double HdopGps { get; set; }

        public int? Heading { get; set; }

        public string Location { get; set; }

        public string Mode { get; set; }

        public double? Pitch { get; set; }

        public string Registration { get; set; }

        public double? Roll { get; set; }

        public string TimeMeasured { get; set; }

        public string TimeSent { get; set; }

        public double TrackBearing { get; set; }

        public int? TrackBearingReference { get; set; }

        public double TrackGroundSpeed { get; set; }

        public string UssInstanceId { get; set; }

        public string UssName { get; set; }

        public double VdopGps { get; set; }

        public double? Yaw { get; set; }

        public string DateCreated { get; set; }
    }
}