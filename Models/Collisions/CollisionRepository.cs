﻿using AnraUssServices.Data;
using AnraUssServices.Models.Collisions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class CollisionRepository : IRepository<Collision>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public CollisionRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Collision item)
        {
            throw new NotImplementedException();
        }

        public Collision Find(long key)
        {
            //return _context.Collisions
            //    .FirstOrDefault(v => v.CollisionId == key);
            throw new NotImplementedException();
        }

        public Collision Find(Guid key)
        {
            //return _context.Collisions
            //    .AsNoTracking()
            //    .First(t => t.Gufi == key);

            return _context.Collisions
                .FirstOrDefault(v => v.CollisionUid == key);
        }

        public IQueryable<Collision> GetAll(string id)
        {
            return _context.Collisions;
        }

        public Collision GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(Collision item)
        {
            throw new NotImplementedException();
        }
    }
}