﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models.Collisions
{
    public class Collision : ModelBase
    {
        [Key]
        public Guid CollisionUid { get; set; }

        //public int CollisionId { get; set; }

        public Guid Gufi { get; set; }

        public Guid Uid { get; set; }

        public int DetectId { get; set; }

        public int PositionId { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Detect { get; set; }

        public string Position { get; set; }
    }
}