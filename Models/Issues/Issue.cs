﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
	public class Issue : ModelBase
	{
        [Key]
		public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string Description { get; set; }

        public string UserNotes { get; set; }

        public int Category { get; set; }

        public int Priority { get; set; }

        public int Status { get; set; }

        public bool IsPublic { get; set; }

        public string InternalNotes { get; set; }

        public string Assignee { get; set; }

        public DateTime DateClosed { get; set; }

        public int TotalAttachments { get; set; }

        public Guid ClosedBy { get; set; }

        public string SystemGenerated { get; set; }

        public Guid CreateBy { get; set; }

        public Guid ModifiedBy { get; set; }
    }
}