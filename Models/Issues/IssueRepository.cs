﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class IssueRepository : IRepository<Issue>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public IssueRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Issue item)
        {
            _context.Issues.Add(item);
            _context.SaveChanges();
        }

        public Issue Find(long key)
        {
            throw new NotImplementedException();
        }

        public Issue Find(Guid key)
        {
            return _context.Issues.FirstOrDefault(t => t.Id == key);
        }

        public IQueryable<Issue> GetAll(string EmailId)
        {
            return _context.Issues;
        }

        public Issue GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(Issue item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}