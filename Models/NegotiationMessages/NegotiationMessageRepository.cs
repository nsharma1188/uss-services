﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class NegotiationMessageRepository : IRepository<NegotiationMessage>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public NegotiationMessageRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(NegotiationMessage item)
        {
            _context.NegotiationMessages.Add(item);
            _context.SaveChanges();
        }

        public NegotiationMessage Find(long key)
        {
            //return _context.NegotiationMessages.FirstOrDefault(V => V.NegotiationMessageId == key);
            throw new NotImplementedException();
        }

        public NegotiationMessage Find(Guid key)
        {
            return _context.NegotiationMessages.FirstOrDefault(V => V.NegotiationMessageUid == key);
        }

        public IQueryable<NegotiationMessage> GetAll(string EmailId)
        {
            return _context.NegotiationMessages;
        }

        public NegotiationMessage GetNewItem()
        {
            return new NegotiationMessage();
        }

        public void Remove(long key)
        {
            //var entity = _context.NegotiationMessages.First(t => t.NegotiationMessageId == key);
            //_context.NegotiationMessages.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.NegotiationMessages.First(t => t.NegotiationMessageUid == key);
            _context.NegotiationMessages.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(NegotiationMessage item)
        {
            _context.NegotiationMessages.Update(item);
            _context.SaveChanges();
        }
    }
}