﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class NegotiationMessage : ModelBase
    {
        //public int NegotiationMessageId { get; set; }
        [Key]
        public Guid NegotiationMessageUid { get; set; }

        public Guid? MessageId { get; set; }

        public Guid? NegotiationId { get; set; }

        public string UssNameOfOriginator { get; set; }

        public string UssNameOfReceiver { get; set; }

        public string Type { get; set; }

        public string ReplanSuggestion { get; set; }

        public bool V2vForOriginator { get; set; }

        public bool V2vForReceiver { get; set; }

        public Guid GufiOfOriginator { get; set; }

        public Guid GufiOfReceiver { get; set; }

        public string DiscoveryReference { get; set; }

        public string FreeText { get; set; }        
    }
}