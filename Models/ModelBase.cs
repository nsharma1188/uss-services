﻿using System;

namespace AnraUssServices.Models
{
    public class ModelBase
    {
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}