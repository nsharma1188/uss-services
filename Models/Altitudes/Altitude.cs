﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class Altitude : ModelBase
    {
        //public int AltitudeId { get; set; }
        [Key]
        public Guid AltitudeUid { get; set; }

        public double AltitudeValue { get; set; }

        public string VerticalReference { get; set; }

        public string UnitsOfMeasure { get; set; }

        public string Source { get; set; }

        public Guid OperationVolumeUid { get; set; }

        [ForeignKey(nameof(OperationVolumeUid))]
        public OperationVolume OperationVolume { get; set; }
    }
}