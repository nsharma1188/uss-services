﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class LanguageResourceRepository : IRepository<LanguageResource>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public LanguageResourceRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(LanguageResource item)
        {
            _context.LanguageResources.Add(item);
            _context.SaveChanges();
        }

        public LanguageResource Find(long key)
        {
            throw new NotImplementedException();
        }

        public LanguageResource Find(Guid key)
        {
            return _context.LanguageResources.FirstOrDefault(v => v.ResourceId == key);
        }

        public IQueryable<LanguageResource> GetAll(string EmailId)
        {
            return _context.LanguageResources;
        }

        public LanguageResource GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.LanguageResources.First(t => t.ResourceId == key);
            _context.LanguageResources.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(LanguageResource item)
        {
            _context.LanguageResources.Update(item);
            _context.SaveChanges();
        }
    }
}