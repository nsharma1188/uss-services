﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class LanguageRepository : IRepository<Language>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public LanguageRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Language item)
        {
            _context.Languages.Add(item);
            _context.SaveChanges();
        }

        public Language Find(long key)
        {
            //return _context.Languages.FirstOrDefault(v => v.LanguageId == key);
            throw new NotImplementedException();
        }

        public Language Find(Guid key)
        {
            return _context.Languages.FirstOrDefault(v => v.LanguageGufi == key);
        }

        public IQueryable<Language> GetAll(string EmailId)
        {
            return _context.Languages;
        }

        public Language GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            //var entity = _context.Languages.First(t => t.LanguageId == key);
            //_context.Languages.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Languages.FirstOrDefault(t => t.LanguageGufi == key);
            _context.Languages.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Language item)
        {
            _context.Languages.Update(item);
            _context.SaveChanges();
        }
    }
}