﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class Language : ModelBase
    {
        //public int LanguageId { get; set; }
        [Key]
        public Guid LanguageGufi { get; set; }

        public string Name { get; set; }

        public string LanguageCode { get; set; }

        public string LanguageCulture { get; set; }

        public string FlagImageFileName { get; set; }

        public bool Published { get; set; }

        public int DisplayOrder { get; set; }

        public string UserId { get; set; }
    }
}