﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class LanguageResource : ModelBase
    {
        public Guid ResourceId { get; set; }

        public string ResourceName { get; set; }

        public string ResourceValue { get; set; }

        public Guid LanguageGufi { get; set; }

        [ForeignKey(nameof(LanguageGufi))]
        public Language Language { get; set; }
    }
}