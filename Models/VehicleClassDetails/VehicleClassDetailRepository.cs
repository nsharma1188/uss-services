﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnraUssServices.Data;
using Microsoft.Extensions.Logging;

namespace AnraUssServices.Models
{
    public class VehicleClassDetailRepository : IRepository<VehicleClassDetail>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public VehicleClassDetailRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(VehicleClassDetail item)
        {
            _context.vehicleClassDetail.Add(item);
            _context.SaveChanges();
        }

        public VehicleClassDetail Find(long key)
        {
            throw new NotImplementedException();
        }

        public VehicleClassDetail Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VehicleClassDetail> GetAll(string Id = "")
        {
            throw new NotImplementedException();
        }

        public VehicleClassDetail GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleClassDetail item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}
