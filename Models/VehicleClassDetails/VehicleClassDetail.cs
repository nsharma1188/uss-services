﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class VehicleClassDetail
    {
        //public int VehicleClassDetailId { get; set; }
        [Key]
        public Guid VehicleClassDetailUid { get; set; }

        //public int VehicleDataId { get; set; }

        public double? NumRotors { get; set; }

        public double? RotorDiameter { get; set; }

        public double? MaxRollRate { get; set; }

        public double? MaxPitchRate { get; set; }

        public decimal? MaxYawRate { get; set; }

        public Guid VehicleDataUid { get; set; }

        [ForeignKey(nameof(VehicleDataUid))]
        public VehicleData VehicleData { get; set; }
    }
}