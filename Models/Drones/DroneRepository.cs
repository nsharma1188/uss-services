﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class DroneRepository : IRepository<Drone>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public DroneRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Drone item)
        {
            _context.Drones.Add(item);
            _context.SaveChanges();
        }

        public Drone Find(long key)
        {
            //return _context.Drones.FirstOrDefault(v => v.DroneId == key);
              throw new NotImplementedException();
        }

        public Drone Find(Guid key)
        {
            return _context.Drones.FirstOrDefault(v => v.Uid == key);
        }

        public IQueryable<Drone> GetAll(string EmailId)
        {
            return _context.Drones;
        }

        public Drone GetNewItem()
        {
            return new Drone();
        }

        public void Remove(long key)
        {
            //var entity = _context.Drones.First(t => t.DroneId == key);
            //_context.Drones.Remove(entity);
            //_context.SaveChanges();

            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Drones.First(t => t.Uid == key);
            _context.Drones.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Drone item)
        {
            _context.Drones.Update(item);
            _context.SaveChanges();
        }
    }
}