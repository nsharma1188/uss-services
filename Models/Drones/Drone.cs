﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class Drone : ModelBase
    {
        //public int DroneId { get; set; }
        [Key]
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public int BrandId { get; set; }

        public int TypeId { get; set; }

        public int UpdPort { get; set; }

        public string BaseLng { get; set; }

        public string BaseLat { get; set; }

        public string BaseAlt { get; set; }

        public string FpvSrcUrl { get; set; }

        public string SensorSrcUrl { get; set; }

        public bool IsRadarEnabled { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsUtmEnabled { get; set; }

        public string UserId { get; set; }

        public string RemoteId { get; set; }

        public int CollisionThreshold { get; set; }

        public Guid? OrganizationId { get; set; }

        public string GroupId { get; set; }
    }
}