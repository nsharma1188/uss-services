﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models.Audits
{
    public class AuditRepository : IRepository<Audit>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public AuditRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Audit item)
        {
            _context.Audits.Add(item);
            _context.SaveChanges();
        }

        public Audit Find(long key)
        {
            //return _context.Audits.FirstOrDefault(v => v.AuditId == key);
            throw new NotImplementedException();
        }

        public Audit Find(Guid key)
        {
            return _context.Audits.FirstOrDefault(v => v.AuditUid == key);
            //throw new NotImplementedException();
        }

        public IQueryable<Audit> GetAll(string Id = "")
        {
            return _context.Audits;
        }

        public Audit GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            //var entity = _context.Audits.First(t => t.AuditId == key);
            //_context.Audits.Remove(entity);
            //_context.SaveChanges();

            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Audits.First(t => t.AuditUid == key);
            _context.Audits.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Audit item)
        {
            _context.Audits.Update(item);
            _context.SaveChanges();
        }
    }
}
