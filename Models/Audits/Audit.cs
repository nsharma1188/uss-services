﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace AnraUssServices.Models.Audits
{
    public class Audit : ModelBase
    {
        [Key]
        public Guid AuditUid { get; set; }
        //public int AuditId { get; set; }

        public string GroupId { get; set; }

        public string IpAddress { get; set; }

        public bool IsValidUser { get; set; }

        public string Platform { get; set; }

        public bool Status { get; set; }

        public string UserId { get; set; }

        public string OrganizationId { get; set; }

        public string UserName { get; set; }

        public string TimeStamp { get; set; }
    }
}
