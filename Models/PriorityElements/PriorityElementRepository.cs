﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class PriorityElementRepository : IRepository<PriorityElement>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public PriorityElementRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(PriorityElement item)
        {
            _context.PriorityElements.Add(item);
            _context.SaveChanges();
        }

        public PriorityElement Find(long key)
        {
            //return _context.PriorityElements.FirstOrDefault(V => V.PriorityElementId == key);
            throw new NotImplementedException();
        }

        public PriorityElement Find(Guid key)
        {
            return _context.PriorityElements.FirstOrDefault(V => V.PriorityElementUid == key);
        }

        public IQueryable<PriorityElement> GetAll(string EmailId)
        {
            return _context.PriorityElements;
        }

        public PriorityElement GetNewItem()
        {
            return new PriorityElement();
        }

        public void Remove(long key)
        {
            //var entity = _context.PriorityElements.First(t => t.PriorityElementId == key);
            //_context.PriorityElements.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.PriorityElements.First(t => t.PriorityElementUid == key);
            _context.PriorityElements.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(PriorityElement item)
        {
            _context.PriorityElements.Update(item);
            _context.SaveChanges();
        }
    }
}