﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class PriorityElement : ModelBase
    {
        //public int PriorityElementId { get; set; }
        [Key]
        public Guid PriorityElementUid { get; set; }

        //public int OperationId { get; set; }

        public string PriorityLevel { get; set; }

        public string PriorityStatus { get; set; }

        public Guid Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
        public Operation Operation { get; set; }
    }
}