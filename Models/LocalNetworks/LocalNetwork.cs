﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class LocalNetwork : ModelBase
    {
        //public int Id { get; set; }
        [Key]
        public Guid LocalNetworkId { get; set; }

        public Guid UssInstanceId { get; set; }

        public string UssName { get; set; }

        public DateTime? TimeSubmitted { get; set; }

        public DateTime? TimeAvailableBegin { get; set; }

        public DateTime? TimeAvailableEnd { get; set; }

        public NpgsqlTypes.NpgsqlPolygon CoverageArea { get; set; }

        public DateTime? TimeLastModified { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string UssBaseCallbackUrl { get; set; }

        public string UssInformationalUrl { get; set; }

        public string UssOpenapiUrl { get; set; }

        public string UssRegistrationUrl { get; set; }

        public string Notes { get; set; }
    }
}