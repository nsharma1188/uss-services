﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class LocalNetworkRepository : IRepository<LocalNetwork>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public LocalNetworkRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(LocalNetwork item)
        {
            throw new NotImplementedException();
        }

        public LocalNetwork Find(long key)
        {
            throw new NotImplementedException();
        }

        public LocalNetwork Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LocalNetwork> GetAll(string Id)
        {
            throw new NotImplementedException();
            return _context.Set<LocalNetwork>().FromSql<LocalNetwork>("select * from public.__getlocalussnetwork({0})", Guid.Parse(Id));
        }

        public LocalNetwork GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(LocalNetwork item)
        {
            throw new NotImplementedException();
        }
    }
}