﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class EventMetadata : ModelBase
    {
        public int EventMetadataId { get; set; }

        public bool? DataCollection { get; set; }

        public string TestCard { get; set; }

        public string Scenario { get; set; }

        public string TestRun { get;set;}

        public string CallSign { get; set; }

        public string TestType { get; set; }

        public string Source { get; set; }

        public string EventId { get; set; }

        public string Location { get; set; }

        public string Setting { get; set; }

        public string FreeText { get; set; }

        public string DataQualityEngineer { get; set; }

        public bool? Modified { get; set; }

        public Guid? MessageId { get; set; }
        

        [ForeignKey(nameof(MessageId))]
        public UtmMessage UtmMessage { get; set; }


        public Guid? Gufi { get; set; }
        
        [ForeignKey(nameof(Gufi))]
        public Operation Operation { get; set; }
    }
}