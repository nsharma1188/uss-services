﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class EventMetadataRepository : IRepository<EventMetadata>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public EventMetadataRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(EventMetadata item)
        {
            _context.EventMetadatas.Add(item);
            _context.SaveChanges();
        }

        public EventMetadata Find(long key)
        {
            return _context.EventMetadatas.FirstOrDefault(V => V.EventMetadataId == key);
        }

        public EventMetadata Find(string key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<EventMetadata> GetAll(string EmailId)
        {
            return _context.EventMetadatas;
        }

        public EventMetadata GetNewItem()
        {
            return new EventMetadata();
        }

        public void Remove(long key)
        {
            var entity = _context.EventMetadatas.First(t => t.EventMetadataId == key);
            _context.EventMetadatas.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(EventMetadata item)
        {
            _context.EventMetadatas.Update(item);
            _context.SaveChanges();
        }
    }
}