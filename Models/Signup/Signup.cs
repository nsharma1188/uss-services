﻿using AnraUssServices.ViewModel.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Models.Signup
{
	public class Signup : ModelBase
	{
		//public int Id { get; set; }
		[Key]
		public Guid SignupUid { get; set; }

		public string Name { get; set; }

		public string OrganizationId { get; set; }

		public string UserId { get; set; }

		public string AdsbSource { get; set; }

		public int CurrencyId { get; set; }

		public string BaseLat { get; set; }

		public string BaseLng { get; set; }

		public int UnitId { get; set; }

		public string LogoFile { get; set; }

		public string ContactPhone { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string UserName { get; set; }

		public string Password { get; set; }

		public string ContactEmail { get; set; }

		public string Address { get; set; }

		public string GroupId { get; set; }

		public string Website { get; set; }

		public string GovtLicenseNumber { get; set; }

		public string FederalTaxId { get; set; }

		public bool SendSMS { get; set; } = false;

		public bool SendEmail { get; set; } = true;

		public string VerificationCode { get; set; }

		public bool IsVerified { get; set; } = false;

		public bool IsPhoneVerified { get; set; } = false;

		public DateTime DateVerified { get; set; }

		public string SignupType { get; set; }

		public string[] UserRole { get; set; }

        public bool IsUsed { get; set; } //For checking the OTP has been used or not
	}
}
