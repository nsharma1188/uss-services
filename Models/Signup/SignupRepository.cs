﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Models.Signup
{
	public class SignupRepository : IRepository<Signup>
	{
		private readonly ILogger _logger;
		private readonly ApplicationDbContext _context;

		public SignupRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
		{
			_context = context;
			_logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
		}

		public void Add(Signup item)
		{
			_context.Signups.Add(item);
			_context.SaveChanges();
		}

		public Signup Find(long key)
		{
			//return _context.Signups.
			//	FirstOrDefault(x => x.Id == key);
			throw new NotImplementedException();
		}

		public Signup Find(Guid key)
		{
			//return _context.Signups.FirstOrDefault(x => x.UserName == key);
			return _context.Signups.
				FirstOrDefault(x => x.SignupUid == key);
		}

		public IQueryable<Signup> GetAll(string Id = "")
		{
			return _context.Signups;
		}

		public Signup GetNewItem()
		{
			throw new NotImplementedException();
		}

		public void Remove(long key)
		{
			//var entity = Find(key);
			//_context.Signups.Remove(entity);
			//_context.SaveChanges();
			throw new NotImplementedException();
		}

		public void Remove(Guid key)
		{
			var entity = Find(key);
			_context.Signups.Remove(entity);
			_context.SaveChanges();
		}

		public void Update(Signup item)
		{
			_context.Update(item);
			_context.SaveChanges();
		}
	}
}
