﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class NotamRepository : IRepository<Notam>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public NotamRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Notam item)
        {
            _context.Notams.Add(item);
            _context.SaveChanges();
        }

        public Notam Find(long key)
        {
            //return _context.Notams
            //    .Include(r => r.AffectedArea)
            //    .FirstOrDefault(v => v.NotamId == key);

            throw new NotImplementedException();
        }

        public Notam Find(Guid key)
        {
            return _context.Notams
                .Include(r => r.AffectedArea)
                .FirstOrDefault(v => v.NotamNumber == key);
        }

        public IQueryable<Notam> GetAll(string EmailId)
        {
            return _context.Notams
                .Include(r => r.AffectedArea);
        }

        public Notam GetNewItem()
        {
            return new Notam();
        }

        public void Remove(long key)
        {
            //var entity = _context.Notams.First(t => t.NotamId == key);
            //_context.Notams.Remove(entity);
            //_context.SaveChanges();

            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Notams.First(t => t.NotamNumber == key);
            _context.Notams.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Notam item)
        {
            _context.Notams.Update(item);
            _context.SaveChanges();
        }
    }
}