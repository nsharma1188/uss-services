﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class Notam : ModelBase
    {
        //public int NotamId { get; set; }
        [Key]
        public Guid NotamNumber { get; set; }

        public DateTime IssueDate { get; set; }        

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }        

        public string NotamReason { get; set; }

        public int TypeId { get; set; }

        public int FacilityId { get; set; }

        public int StateId { get; set; }

        public string Description { get; set; }

        public string UserId { get; set; }
        
        public NotamArea AffectedArea { get; set; }

        public string Restriction { get; set; }

        public string[] Requirements { get; set; }

        public string Authority { get; set; }

        public string PointOfContact { get; set; }

        public string Status { get; set; }

        public Guid? OrganizationId { get; set; }

        public string GroupId { get; set; }
    }
}