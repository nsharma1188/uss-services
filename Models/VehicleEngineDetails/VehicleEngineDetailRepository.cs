﻿using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class VehicleEngineDetailRepository : IRepository<VehicleEngineDetail>
    {
        public void Add(VehicleEngineDetail item)
        {
            throw new System.NotImplementedException();
        }

        public VehicleEngineDetail Find(long key)
        {
            throw new System.NotImplementedException();
        }

        public VehicleEngineDetail Find(Guid key)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<VehicleEngineDetail> GetAll(string Id = "")
        {
            throw new System.NotImplementedException();
        }

        public VehicleEngineDetail GetNewItem()
        {
            throw new System.NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleEngineDetail item)
        {
            throw new System.NotImplementedException();
        }
    }
}