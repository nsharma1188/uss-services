﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class VehicleEngineDetail
    {
        //public int VehicleEngineDetailsId { get; set; }
        [Key]
        public Guid VehicleEngineDetailsUid { get; set; }

        //public int VehicleDataId { get; set; }

        public string EngineType { get; set; }

        public string BatteryType { get; set; }

        public double? BatteryCapacity { get; set; }

        public double? BatteryVoltage { get; set; }

        public Guid VehicleDataUid { get; set; }

        [ForeignKey(nameof(VehicleDataUid))]
        public VehicleData VehicleData { get; set; }
    }
}