﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class ConformanceLog : ModelBase
    {
        [Key]
        public Guid ConformanceLogUid { get; set; }
        //public int ConformanceLogId { get; set; }

        public bool IsConfirming { get; set; }

        public bool IsGeographyConfirming { get; set; }

        public bool IsAltitudeConfirming { get; set; }

        public bool IsTimeConfirming { get; set; }

        public DateTime TimeStamp { get; set; }

        public int DifferenceInSeconds { get; set; }

        public string State { get; set; }

        public int NonConformances { get; set; }

        public Guid Gufi { get; set; }

        public string TelemetryMessageId { get; set; }

        [ForeignKey(nameof(TelemetryMessageId))]
        public TelemetryMessage TelemetryMessage { get; set; }
    }
}