﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class ConformanceLogRepository : IRepository<ConformanceLog>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public ConformanceLogRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(ConformanceLog item)
        {
            _context.ConformanceLogs.Add(item);
            _context.SaveChanges();
        }

        public ConformanceLog Find(long key)
        {
            //return _context.ConformanceLogs.FirstOrDefault(V => V.ConformanceLogId == key);
            throw new NotImplementedException();
        }

        public ConformanceLog Find(Guid key)
        {
            return _context.ConformanceLogs.FirstOrDefault(V => V.ConformanceLogUid == key);
        }

        public IQueryable<ConformanceLog> GetAll(string EmailId)
        {
            return _context.ConformanceLogs;
        }

        public ConformanceLog GetNewItem()
        {
            return new ConformanceLog();
        }

        public void Remove(long key)
        {
            //var entity = _context.ConformanceLogs.First(t => t.ConformanceLogId == key);
            //_context.ConformanceLogs.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.ConformanceLogs.First(t => t.ConformanceLogUid == key);
            _context.ConformanceLogs.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(ConformanceLog item)
        {
            _context.ConformanceLogs.Update(item);
            _context.SaveChanges();
        }
    }
}