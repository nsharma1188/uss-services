﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class UtmMessageRepository : IRepository<UtmMessage>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public UtmMessageRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(UtmMessage item)
        {
            _context.UtmMessages.Add(item);
            _context.SaveChanges();
        }

        public UtmMessage Find(long key)
        {
            //return _context.UtmMessages
            //    .Include(p => p.Contingency)
            //    .FirstOrDefault(V => V.UtmMessageId == key);
            throw new NotImplementedException();
        }

        public UtmMessage Find(Guid key)
        {
            return _context.UtmMessages
                .Include(p => p.Contingency)
                .FirstOrDefault(V => V.MessageId == key);
        }

        public IQueryable<UtmMessage> GetAll(string EmailId)
        {
            return _context.UtmMessages
                .Include(p => p.Contingency);
        }

        public UtmMessage GetNewItem()
        {
            return new UtmMessage();
        }

        public void Remove(long key)
        {
            //var entity = _context.UtmMessages.First(t => t.UtmMessageId == key);
            //_context.UtmMessages.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.UtmMessages.First(t => t.MessageId == key);
            _context.UtmMessages.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(UtmMessage item)
        {
            _context.UtmMessages.Update(item);
            _context.SaveChanges();
        }
    }
}