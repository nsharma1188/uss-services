﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class UtmMessage : ModelBase
    {
        //public int UtmMessageId { get; set; }
        [Key]
        public Guid MessageId { get; set; }

        public string UssName { get; set; }        

        public Guid? Gufi { get; set; }

        public DateTime SentTime { get; set; }

        public string Severity { get; set; }

        public string MessageType { get; set; }

        public Guid? PrevMessageId { get; set; }

        public string FreeText { get; set; }

        public string Callback { get; set; }

        public string DiscoveryReference { get; set; }

        public Guid? ContingencyPlanUid { get; set; }        

        [ForeignKey(nameof(ContingencyPlanUid))]
        public ContingencyPlan Contingency { get; set; }

        public string TelemetryMessageId { get; set; }

        [ForeignKey(nameof(TelemetryMessageId))]
        public TelemetryMessage LastKnownPosition { get; set; }
    }
}