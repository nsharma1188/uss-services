﻿using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class VehicleRegistrationRepository : IRepository<VehicleRegistration>
    {
        public void Add(VehicleRegistration item)
        {
            throw new NotImplementedException();
        }

        public VehicleRegistration Find(long key)
        {
            throw new NotImplementedException();
        }

        public VehicleRegistration Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VehicleRegistration> GetAll(string Id = "")
        {
            throw new NotImplementedException();
        }

        public VehicleRegistration GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleRegistration item)
        {
            throw new NotImplementedException();
        }
    }
}