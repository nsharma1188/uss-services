﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class VehicleRegistration : ModelBase
    {
        //public int VehicleRegistrationId { get; set; }
        [Key]
        public Guid VehicleRegistrationUid { get; set; }

        //public int VehicleDataId { get; set; }

        public Guid? Uvin { get; set; }

        public string Date { get; set; }

        public string RegisteredBy { get; set; }

        public string NNumber { get; set; }

        public string FaaNumber { get; set; }

        public string VehicleName { get; set; }

        public Guid VehicleDataUid { get; set; }

        [ForeignKey(nameof(VehicleDataUid))]
        public VehicleData VehicleData { get; set; }
    }
}