﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models.Detects
{
    public class Detect : ModelBase
    {
        //public int Id { get; set; }
        [Key]
        public Guid DetectId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Altitude { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Source { get; set; }

        public string SourceClass { get; set; }

        public string Vendor { get; set; }

        public string Metadata1 { get; set; }

        public string Metadata2 { get; set; }

        public string RawData { get; set; }

        public int? Heading { get; set; }

        public double? Bearing { get; set; }

        public Guid Uid { get; set; }

        public Guid UserId { get; set; }
    }
}