﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models.Detects
{
    public class DetectRepository : IRepository<Detect>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public DetectRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Detect item)
        {
            _context.Detects.Add(item);
            _context.SaveChanges();
        }

        public Detect Find(long key)
        {
            throw new NotImplementedException();
        }

        public Detect Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Detect> GetAll(string Id = "")
        {
            throw new NotImplementedException();
        }

        public Detect GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(Detect item)
        {
            throw new NotImplementedException();
        }
    }
}