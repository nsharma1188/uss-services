﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class NegotiationAgreement : ModelBase
	{
        //public int NegotiationAgreementId { get; set; }
        [Key]
        public Guid NegotiationAgreementUid { get; set; }

        public Guid? MessageId { get; set; }

        public Guid? NegotiationId { get; set; }

        public string UssNameOfOriginator { get; set; }

        public string UssNameOfReceiver { get; set; }

        public Guid GufiOriginator { get; set; }

        public Guid GufiReceiver { get; set; }

        public string Type { get; set; }               

        public string DiscoveryReference { get; set; }

        public string FreeText { get; set; }

        //public int OperationId { get; set; }
        public Guid Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
		public virtual Operation Operation { get; set; }
	}
}