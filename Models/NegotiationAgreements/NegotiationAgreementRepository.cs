﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class NegotiationAgreementRepository : IRepository<NegotiationAgreement>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public NegotiationAgreementRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(NegotiationAgreement item)
        {
            _context.NegotiationAgreements.Add(item);
            _context.SaveChanges();
        }

        public NegotiationAgreement Find(long key)
        {
            //return _context.NegotiationAgreements.FirstOrDefault(V => V.NegotiationAgreementId == key);
            throw new NotImplementedException();
        }

        public NegotiationAgreement Find(Guid key)
        {
            return _context.NegotiationAgreements.FirstOrDefault(V => V.NegotiationAgreementUid == key);
        }

        public IQueryable<NegotiationAgreement> GetAll(string EmailId)
        {
            return _context.NegotiationAgreements;
        }

        public NegotiationAgreement GetNewItem()
        {
            return new NegotiationAgreement();
        }

        public void Remove(long key)
        {
            //var entity = _context.NegotiationAgreements.First(t => t.NegotiationAgreementId == key);
            //_context.NegotiationAgreements.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.NegotiationAgreements.First(t => t.NegotiationAgreementUid == key);
            _context.NegotiationAgreements.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(NegotiationAgreement item)
        {
            _context.NegotiationAgreements.Update(item);
            _context.SaveChanges();
        }
    }
}