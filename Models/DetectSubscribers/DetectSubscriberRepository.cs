﻿using AnraUssServices.Data; 
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models.DetectSubscribers
{
    public class DetectSubscriberRepository : IRepository<DetectSubscriber>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public DetectSubscriberRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(DetectSubscriber item)
        {
            _context.DetectSubscribers.Add(item);
            _context.SaveChanges();
        }

        public DetectSubscriber Find(long key)
        {
            throw new NotImplementedException();
        }

        public DetectSubscriber Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<DetectSubscriber> GetAll(string Id = "")
        {
            return _context.DetectSubscribers;
        }

        public DetectSubscriber GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            //var entity = _context.DetectSubscribers.First(t => t.DetectSubscriberId == key);
            //_context.DetectSubscribers.Remove(entity);
            //_context.SaveChanges();
            
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.DetectSubscribers.First(t => t.SubscriptionId == key);
            _context.DetectSubscribers.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(DetectSubscriber item)
        {
            _context.DetectSubscribers.Update(item);
            _context.SaveChanges();
        }
    }
}