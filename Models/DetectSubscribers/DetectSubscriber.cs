﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models.DetectSubscribers
{
    public class DetectSubscriber : ModelBase
    {
        //public int DetectSubscriberId { get; set; }
        [Key]
        public Guid SubscriptionId { get; set; }

        public Guid Gufi { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public bool IsActive { get; set; }

        public string DetectSoure { get; set; }

        public Guid UserId { get; set; }

        public Guid UssInstanceId { get; set; }

        public Guid OperationId { get; set; }

        public string[] SubscribedTypes { get; set; }

        public string AdsbSource { get; set; }

        public string AdsbQuery { get; set; }
    }
}