﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class UasRegistration : ModelBase
    {
        //public int UasRegistrationId { get; set; }
        [Key]
        public Guid UasRegistrationUid { get; set; }

        public Guid? RegistrationId { get; set; }

        public string RegistrationLocation { get; set; }

        //public int OperationId { get; set; }
        public Guid Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
        public Operation Operation { get; set; }
    }
}