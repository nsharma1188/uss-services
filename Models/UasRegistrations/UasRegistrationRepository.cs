﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class UasRegistrationRepository : IRepository<UasRegistration>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public UasRegistrationRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(UasRegistration item)
        {
            _context.UasRegistrations.Add(item);
            _context.SaveChanges();
        }

        public UasRegistration Find(long key)
        {
            //return _context.UasRegistrations.FirstOrDefault(V => V.UasRegistrationId == key);
            throw new NotImplementedException();
        }

        public UasRegistration Find(Guid key)
        {
            return _context.UasRegistrations.FirstOrDefault(V => V.UasRegistrationUid == key);
        }

        public IQueryable<UasRegistration> GetAll(string EmailId)
        {
            return _context.UasRegistrations;
        }

        public UasRegistration GetNewItem()
        {
            return new UasRegistration();
        }

        public void Remove(long key)
        {
            //var entity = _context.UasRegistrations.First(t => t.UasRegistrationId == key);
            //_context.UasRegistrations.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.UasRegistrations.First(t => t.UasRegistrationUid == key);
            _context.UasRegistrations.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(UasRegistration item)
        {
            _context.UasRegistrations.Update(item);
            _context.SaveChanges();
        }
    }
}