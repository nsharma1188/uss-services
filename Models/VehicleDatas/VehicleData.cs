﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Models
{
    public class VehicleData : ModelBase
    {
        //public int VehicleDataId { get; set; }
        [Key]
        public Guid VehicleDataUid { get; set; }

        public VehicleRegistration VehicleRegistration { get; set; }

        public VehicleType VehicleType { get; set; }

        public VehicleProperty Properties { get; set; }

        public VehicleClassDetail ClassDetails { get; set; }

        public VehicleEngineDetail EngineDetails { get; set; }
    }
}