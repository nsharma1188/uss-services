﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class VehicleDataRepository : IRepository<VehicleData>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public VehicleDataRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(VehicleData item)
        {
            _context.vehicleData.Add(item);
            _context.SaveChanges();
        }

        public VehicleData Find(long key)
        {
            throw new System.NotImplementedException();
        }

        public VehicleData Find(Guid key)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<VehicleData> GetAll(string Id = "")
        {
            throw new System.NotImplementedException();
        }

        public VehicleData GetNewItem()
        {
            throw new System.NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new System.NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleData item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}