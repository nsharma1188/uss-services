﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class ConstraintMessageRepository : IRepository<ConstraintMessage>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public ConstraintMessageRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(ConstraintMessage item)
        {
            _context.ConstraintMessages.Add(item);
            _context.SaveChanges();
        }

        public ConstraintMessage Find(long key)
        {
            //return _context.ConstraintMessages.FirstOrDefault(V => V.ConstraintMessageId == key);
            throw new NotImplementedException();
        }

        public ConstraintMessage Find(Guid key)
        {
            return _context.ConstraintMessages.FirstOrDefault(V => V.MessageId == key);
        }

        public IQueryable<ConstraintMessage> GetAll(string EmailId)
        {
            return _context.ConstraintMessages;
        }

        public ConstraintMessage GetNewItem()
        {
            return new ConstraintMessage();
        }

        public void Remove(long key)
        {
            //var entity = _context.ConstraintMessages.First(t => t.ConstraintMessageId == key);
            //_context.ConstraintMessages.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.ConstraintMessages.First(t => t.MessageId == key);
            _context.ConstraintMessages.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(ConstraintMessage item)
        {
            _context.ConstraintMessages.Update(item);
            _context.SaveChanges();
        }
    }
}