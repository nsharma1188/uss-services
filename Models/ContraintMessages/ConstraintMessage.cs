﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class ConstraintMessage : ModelBase
    {
        //public int ConstraintMessageId { get; set; }

        [Key]
        public Guid MessageId { get; set; }

        public string UssName { get; set; }

        public string Type { get; set; }

        public NpgsqlTypes.NpgsqlPolygon Geography { get; set; }

        public DateTime EffectiveTimeBegin { get; set; }

        public DateTime EffectiveTimeEnd { get; set; }
        
        public DateTime? ActualTimeEnd { get; set; }

        [Column(TypeName = "jsonb")]
        public string MinAltitude { get; set; }

        [Column(TypeName = "jsonb")]
        public string MaxAltitude { get; set; }

        public string Reason { get; set; }

        public string Cause { get; set; }

        public bool IsRestriction { get; set; }

        public string[] PermittedUas { get; set; }

        public string[] PermittedGufis { get; set; }

        public bool IsExpired { get; set; }

        public string GroupId { get; set; }
    }
}