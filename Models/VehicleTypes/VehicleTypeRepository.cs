﻿using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class VehicleTypeRepository : IRepository<VehicleType>
    {
        public void Add(VehicleType item)
        {
            throw new NotImplementedException();
        }

        public VehicleType Find(long key)
        {
            throw new NotImplementedException();
        }

        public VehicleType Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VehicleType> GetAll(string Id = "")
        {
            throw new NotImplementedException();
        }

        public VehicleType GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleType item)
        {
            throw new NotImplementedException();
        }
    }
}