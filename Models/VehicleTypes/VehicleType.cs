﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class VehicleType
    {
        //public int VehiclePropertiesId { get; set; }
        [Key]
        public Guid VehicleTypeUid { get; set; }

        //public int VehicleDataId { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Class { get; set; }

        public string AccessType { get; set; }

        public Guid VehicleDataUid { get; set; }

        [ForeignKey(nameof(VehicleDataUid))]
        public VehicleData VehicleData { get; set; }
    }
}