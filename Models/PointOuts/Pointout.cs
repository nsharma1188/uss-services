﻿using NpgsqlTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class Pointout : ModelBase
    {
        //public int PointoutId { get; set; }
        [Key]
        public Guid PointoutUid { get; set; }

        public double? Altitude { get; set; }

        public int? Bearing { get; set; }

        public double? Distance { get; set; }

        public string NorthRef { get; set; }

        public NpgsqlPoint Point { get; set; }

        public string Remark { get; set; }

        public string State { get; set; }

        public double? Track { get; set; }

        public string VehicleType { get; set; }

        //public int UrepId { get; set; }
        public Guid UrepUid { get; set; }

        [ForeignKey(nameof(UrepUid))]
        public Urep Urep { get; set; }
    }
}