﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class PointoutRepository : IRepository<Pointout>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public PointoutRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Pointout item)
        {
            _context.Pointouts.Add(item);
            _context.SaveChanges();
        }

        public Pointout Find(long key)
        {
            //return _context.Pointouts.FirstOrDefault(V => V.PointoutId == key);
            throw new NotImplementedException();
        }

        public Pointout Find(Guid key)
        {
            return _context.Pointouts.FirstOrDefault(V => V.PointoutUid == key);
        }

        public IQueryable<Pointout> GetAll(string EmailId)
        {
            return _context.Pointouts;
        }

        public Pointout GetNewItem()
        {
            return new Pointout();
        }

        public void Remove(long key)
        {
            //var entity = _context.Pointouts.First(t => t.PointoutId == key);
            //_context.Pointouts.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.Pointouts.First(t => t.PointoutUid == key);
            _context.Pointouts.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Pointout item)
        {
            _context.Pointouts.Update(item);
            _context.SaveChanges();
        }
    }
}