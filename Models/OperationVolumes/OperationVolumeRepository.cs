﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class OperationVolumeRepository : IRepository<OperationVolume>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public OperationVolumeRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(OperationVolume item)
        {
            _context.OperationVolumes.Add(item);
            _context.SaveChanges();
        }

        public OperationVolume Find(long key)
        {
            //return _context.OperationVolumes.FirstOrDefault(V => V.OperationVolumeId == key);
            throw new NotImplementedException();
        }

        public OperationVolume Find(Guid key)
        {
            return _context.OperationVolumes.FirstOrDefault(V => V.OperationVolumeUid == key);
        }

        public IQueryable<OperationVolume> GetAll(string EmailId)
        {
            return _context.OperationVolumes;
        }

        public OperationVolume GetNewItem()
        {
            return new OperationVolume();
        }

        public void Remove(long key)
        {
            //var entity = Find(key);
            //_context.OperationVolumes.Remove(entity);
            //_context.SaveChanges();
            
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = Find(key);
            _context.OperationVolumes.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(OperationVolume item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}