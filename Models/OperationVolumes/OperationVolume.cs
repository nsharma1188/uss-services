﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class OperationVolume : ModelBase
    {
        public OperationVolume()
        {
            BeyondVisualLineOfSight = true; 
        }

        //public int OperationVolumeId { get; set; }
        [Key]
        public Guid OperationVolumeUid { get; set; }

        public int Ordinal { get; set; }
        
        public string VolumeType { get; set; }

        public bool? NearStructure { get; set; }

        public DateTime EffectiveTimeBegin { get; set; }

        public DateTime EffectiveTimeEnd { get; set; }

        public DateTime? ActualTimeEnd { get; set; }

        [Column(TypeName = "jsonb")]
        public string MinAltitude { get; set; }

        [Column(TypeName = "jsonb")]
        public string MaxAltitude { get; set; }

        public NpgsqlTypes.NpgsqlPolygon OperationGeography { get; set; }

        public NpgsqlTypes.NpgsqlPolygon? NonConformanceGeography { get; set; }

        public NpgsqlTypes.NpgsqlPolygon? ProtectedGeography { get; set; }

        public bool BeyondVisualLineOfSight { get; set; }

        public double DistanceInFeet { get; set; }

		public double AglAltitude { get; set; }

        //public int OperationId { get; set; }
        public Guid Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
        public virtual Operation Operation { get; set; }        
    }
}