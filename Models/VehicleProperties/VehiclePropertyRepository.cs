﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Models
{
    public class VehiclePropertyRepository : IRepository<VehicleProperty>
    {
        public void Add(VehicleProperty item)
        {
            throw new NotImplementedException();
        }

        public VehicleProperty Find(long key)
        {
            throw new NotImplementedException();
        }

        public VehicleProperty Find(Guid key)
        {
            throw new NotImplementedException();
        }

        public IQueryable<VehicleProperty> GetAll(string Id = "")
        {
            throw new NotImplementedException();
        }

        public VehicleProperty GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            throw new NotImplementedException();
        }

        public void Update(VehicleProperty item)
        {
            throw new NotImplementedException();
        }
    }
}
