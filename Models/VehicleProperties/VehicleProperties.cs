﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class VehicleProperty : ModelBase
    {
        //public int VehiclePropertiesId { get; set; }
        [Key]
        public Guid VehiclePropertiesUid { get; set; }

        //public int VehicleDataId { get; set; }

        public string WebLink { get; set; }

        public double? Length { get; set; }

        public double? Width { get; set; }

        public double? Height { get; set; }

        public double? MaxVel { get; set; }

        public double? CruiseVel { get; set; }

        public double? MaxWindVel { get; set; }

        public double? MaxEndurance { get; set; }

        public double? Mtow { get; set; }

        public double? MaxEmptyWeight { get; set; }

        public double? PayloadCapacity { get; set; }

        public double? MaxThrust { get; set; }

        public double? MaxRange { get; set; }

        public double? MaxCeiling { get; set; }

        public Guid VehicleDataUid { get; set; }

        [ForeignKey(nameof(VehicleDataUid))]
        public VehicleData VehicleData { get; set; }
    }
}