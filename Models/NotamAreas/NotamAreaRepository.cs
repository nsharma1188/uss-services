﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class NotamAreaRepository : IRepository<NotamArea>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public NotamAreaRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(NotamArea item)
        {
            _context.NotamAreas.Add(item);
            _context.SaveChanges();
        }

        public NotamArea Find(long key)
        {
            //return _context.NotamAreas.FirstOrDefault(V => V.NotamAreaId == key);
            throw new NotImplementedException();
        }

        public NotamArea Find(Guid key)
        {
            return _context.NotamAreas.FirstOrDefault(V => V.NotamAreaUid == key);
        }

        public IQueryable<NotamArea> GetAll(string EmailId)
        {
            return _context.NotamAreas;
        }

        public NotamArea GetNewItem()
        {
            return new NotamArea();
        }

        public void Remove(long key)
        {
            //var entity = _context.NotamAreas.First(t => t.NotamAreaId == key);
            //_context.NotamAreas.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.NotamAreas.First(t => t.NotamAreaUid == key);
            _context.NotamAreas.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(NotamArea item)
        {
            _context.NotamAreas.Update(item);
            _context.SaveChanges();
        }
    }
}