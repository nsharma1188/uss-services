﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class NotamArea : ModelBase
    {
        private NpgsqlTypes.NpgsqlPolygon? _region;
        private NpgsqlTypes.NpgsqlPoint? _center;


        //public int NotamAreaId { get; set; }
        [Key]
        public Guid NotamAreaUid { get; set; }

        public string AreaName { get; set; }        

        public DateTime[] EffetiveDates { get; set; }

        //public NpgsqlTypes.NpgsqlPolygon? Region { get; set; }

        public NpgsqlTypes.NpgsqlPolygon? Region
        {
            get { return _region.HasValue ? _region.Value : _region; }
            set
            {
                _region = value.HasValue ? value.Value : value;                
            }
        }

        //public NpgsqlTypes.NpgsqlPoint? Center { get; set; }

        public NpgsqlTypes.NpgsqlPoint? Center
        {
            get { return _center.HasValue ? _center.Value : _center; }
            set
            {
                _center = value.HasValue ? value.Value : value;
            }
        }

        public int Radius { get; set; }

        public int MinAltitude { get; set; }

        public int MaxAltitude { get; set; }

        public Guid NotamNumber { get; set; }

        [ForeignKey(nameof(NotamNumber))]
        public Notam Notam { get; set; }
    }
}