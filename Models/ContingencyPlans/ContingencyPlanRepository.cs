﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class ContingencyPlanRepository : IRepository<ContingencyPlan>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public ContingencyPlanRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(ContingencyPlan item)
        {
            _context.ContingencyPlans.Add(item);
            _context.SaveChanges();
        }

        public ContingencyPlan Find(long key)
        {
            //return _context.ContingencyPlans.FirstOrDefault(V => V.ContingencyPlanId == key);
            throw new NotImplementedException();
        }

        public ContingencyPlan Find(Guid key)
        {
            return _context.ContingencyPlans.FirstOrDefault(V => V.ContingencyPlanUid == key);
        }

        public IQueryable<ContingencyPlan> GetAll(string EmailId)
        {
            return _context.ContingencyPlans;
        }

        public ContingencyPlan GetNewItem()
        {
            return new ContingencyPlan();
        }

        public void Remove(long key)
        {
            //var entity = Find(key);
            //_context.ContingencyPlans.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = Find(key);
            _context.ContingencyPlans.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(ContingencyPlan item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}