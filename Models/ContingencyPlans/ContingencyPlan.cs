﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class ContingencyPlan : ModelBase
    {
        [Key]
        public Guid ContingencyPlanUid { get; set; }
        //public int ContingencyPlanId { get; set; }

        public int ContingencyId { get; set; }

        public string[] ContingencyCause { get; set; }

        public string ContingencyResponse { get; set; }

        public NpgsqlTypes.NpgsqlPolygon ContingencyPolygon { get; set; }

        [Column(TypeName = "jsonb")]
        public string LoiterAltitude { get; set; }

        public double? RelativePreference { get; set; }

        public string ContingencyLocationDescription { get; set; }

        public string[] RelevantOperationVolumes { get; set; }

        public DateTime? ValidTimeBegin { get; set; }

        public DateTime? ValidTimeEnd { get; set; }

        public string FreeText { get; set; }

        //public int? OperationId { get; set; }
        public Guid? Gufi { get; set; }

        [ForeignKey(nameof(Gufi))]
        public Operation Operation { get; set; }

        //public int? UtmMessageId { get; set; }
        public Guid? MessageId { get; set; }

        [ForeignKey(nameof(MessageId))]
        public UtmMessage UtmMessage { get; set; }
    }
}