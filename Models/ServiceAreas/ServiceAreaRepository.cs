﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class ServiceAreaRepository : IRepository<ServiceArea>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public ServiceAreaRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(ServiceArea item)
        {
            _context.ServiceAreas.Add(item);
            _context.SaveChanges();
        }

        public ServiceArea GetNewItem()
        {
            throw new NotImplementedException();
        }

        public ServiceArea Find(long key)
        {
            //return _context.ServiceAreas.FirstOrDefault(v => v.ServiceAreaId.Equals(key));
            throw new NotImplementedException();
        }

        public ServiceArea Find(Guid key)
        {
            return _context.ServiceAreas.FirstOrDefault(v => v.Id == key);
        }

        public IQueryable<ServiceArea> GetAll(string EmailId)
        {
            return _context.ServiceAreas;
        }

        public void Remove(long key)
        {
            //var entity = _context.ServiceAreas.First(t => t.ServiceAreaId == key);
            //_context.ServiceAreas.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Update(ServiceArea item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }

        public void Remove(Guid key)
        {
            var entity = _context.ServiceAreas.First(t => t.Id == key);
            _context.ServiceAreas.Remove(entity);
            _context.SaveChanges();
        }
    }
}