﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
	public class ServiceArea : ModelBase
	{
		//public int ServiceAreaId { get; set; }
        [Key]
        public Guid Id { get; set; }

        public string FlightsUrl { get; set; }

        public string Owner { get; set; }        

        public DateTime? TimeStart { get; set; }

        public DateTime? TimeEnd { get; set; }

        public string Version { get; set; }
    }
}