﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class UserDroneAssociation: ModelBase
    {
        //public long UserDroneAssociationId { get; set; } 
        [Key]
        public Guid UserDroneAssociationUid { get; set; }

        public string UserId { get; set; }

        public string RegistrationId { get; set; }

        public Guid Uid { get; set; }
        
        [ForeignKey("Uid")]
        public Drone Drone { get; set; }
    }
}