﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class UserDroneAssociationRepository : IRepository<UserDroneAssociation>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public UserDroneAssociationRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(UserDroneAssociation item)
        {
            _context.UserDroneAssociations.Add(item);
            _context.SaveChanges();
        }

        public UserDroneAssociation Find(long key)
        {
            //return _context.UserDroneAssociations.FirstOrDefault(v => v.UserDroneAssociationId == key);
            throw new NotImplementedException();
        }

        public UserDroneAssociation Find(Guid key)
        {
            return _context.UserDroneAssociations.FirstOrDefault(v => v.UserDroneAssociationUid == key);
        }

        public IQueryable<UserDroneAssociation> GetAll(string EmailId)
        {
            return _context.UserDroneAssociations;
        }

        public UserDroneAssociation GetNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(long key)
        {
            //var entity = _context.UserDroneAssociations.First(t => t.UserDroneAssociationId == key);
            //_context.UserDroneAssociations.Remove(entity);
            //_context.SaveChanges();
            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.UserDroneAssociations.First(t => t.UserDroneAssociationUid == key);
            _context.UserDroneAssociations.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(UserDroneAssociation item)
        {
            _context.UserDroneAssociations.Update(item);
            _context.SaveChanges();
        }
    }
}