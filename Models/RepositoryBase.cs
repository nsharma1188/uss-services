﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace AnraUssServices.Models
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected ApplicationDbContext applicationDbContext { get; set; }

        public RepositoryBase(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.applicationDbContext.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.applicationDbContext.Set<T>()
                .Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            this.applicationDbContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.applicationDbContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.applicationDbContext.Set<T>().Remove(entity);
        }
    }
}