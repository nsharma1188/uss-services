﻿using AnraUssServices.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class UtmInstanceRepository : IRepository<UtmInstance>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public UtmInstanceRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(UtmInstance item)
        {
            _context.UtmInstances.Add(item);
            _context.SaveChanges();
        }

        public UtmInstance Find(long key)
        {
            //return _context.UtmInstances.FirstOrDefault(V => V.Id == key);
            throw new NotImplementedException();
        }

        public UtmInstance Find(Guid key)
        {
            return _context.UtmInstances.FirstOrDefault(V => V.UssInstanceId == key);
        }

        public IQueryable<UtmInstance> GetAll(string EmailId)
        {
            return _context.UtmInstances;
        }

        public UtmInstance GetNewItem()
        {
            return new UtmInstance();
        }

        public void Remove(long key)
        {
            //var entity = _context.UtmInstances.First(t => t.Id == key);
            //_context.UtmInstances.Remove(entity);
            //_context.SaveChanges();

            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = _context.UtmInstances.First(t => t.UssInstanceId == key);
            _context.UtmInstances.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(UtmInstance item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}