﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
    public class UtmInstance : ModelBase
    {
        //public int Id { get; set; }
        [Key]
        public Guid UssInstanceId { get; set; }

        public string UssName { get; set; }

        public DateTime? TimeSubmitted { get; set; }

        public DateTime? TimeAvailableBegin { get; set; }

        public DateTime? TimeAvailableEnd { get; set; }

        public NpgsqlTypes.NpgsqlPolygon CoverageArea { get; set; }

        public DateTime? TimeLastModified { get; set; } 

        public string UssBaseCallbackUrl { get; set; }

        public string UssInformationalUrl { get; set; }

        public string UssOpenapiUrl { get; set; }

        public string UssRegistrationUrl { get; set; }

        public string Notes { get; set; }

        public Guid? OrganizationId { get; set; }

        [Column(TypeName = "jsonb")]
        public string Contact { get; set; }

        [Column(TypeName = "jsonb")]
        public string GridCells { get; set; }

        public string GroupId { get; set; }

        public string UserId { get; set; }
    }
}