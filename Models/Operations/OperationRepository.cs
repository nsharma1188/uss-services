﻿using AnraUssServices.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public class OperationRepository : IRepository<Operation>
    {
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public OperationRepository(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("DataAccessPostgreSqlProvider");
        }

        public void Add(Operation item)
        {
            _context.Operations.Add(item);
            _context.SaveChanges();
        }

        public Operation Find(long key)
        {
    //        return _context.Operations
    //            .Include(r => r.OperationVolumes)
    //            .Include(r => r.PriorityElements)
    //            .Include(r => r.ContingencyPlans)                
    //            .Include(r => r.UasRegistrations)
				//.Include(r => r.NegotiationAgreements)
    //            .FirstOrDefault(v => v.OperationId == key);

            throw new NotImplementedException();
        }

        public Operation Find(Guid key)
        {
            return _context.Operations
                .Include(r => r.OperationVolumes)
                .Include(r => r.PriorityElements)
                .Include(r => r.ContingencyPlans)                
                .Include(r => r.UasRegistrations)
				.Include(r => r.NegotiationAgreements)
                .AsNoTracking()
                .FirstOrDefault(t => t.Gufi == key);
        }

        public IQueryable<Operation> GetAll(string EmailId)
        {
            return _context.Operations
                .Include(r => r.OperationVolumes)
                .Include(r => r.PriorityElements)
                .Include(r => r.ContingencyPlans)                
                .Include(r => r.UasRegistrations)
                .Include(r => r.OperationConflicts)
				.Include(r => r.NegotiationAgreements);
        }

        public Operation GetNewItem()
        {
            return new Operation();
        }

        public void Remove(long key)
        {
            //var entity = Find(key);
            //_context.Operations.Remove(entity);
            //_context.SaveChanges();

            throw new NotImplementedException();
        }

        public void Remove(Guid key)
        {
            var entity = Find(key);
            _context.Operations.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Operation item)
        {
            _context.Update(item);
            _context.SaveChanges();
        }
    }
}