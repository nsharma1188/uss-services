﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnraUssServices.Models
{
	public class Operation : ModelBase
	{
		//public int OperationId { get; set; }
		[Key]
		public Guid Gufi { get; set; }

		public string UssName { get; set; }

		public Guid UssInstanceId { get; set; }

		public DateTime SubmitTime { get; set; }

		public DateTime? UpdateTime { get; set; }

		public string FlightComments { get; set; }

		public ICollection<UasRegistration> UasRegistrations { get; set; }

		[Column(TypeName = "jsonb")]
		public string Contact { get; set; }

		public string UserId { get; set; }

		public string State { get; set; }

		public NpgsqlTypes.NpgsqlPoint ControllerLocation { get; set; }        

        public virtual ICollection<ContingencyPlan> ContingencyPlans { get; set; }

		public string FaaRule { get; set; }

		public virtual PriorityElement PriorityElements { get; set; }

		public virtual ICollection<OperationVolume> OperationVolumes { get; set; }

		public DateTime? DecisionTime { get; set; }

		public virtual ICollection<OperationConflict> OperationConflicts { get; set; }

		public bool IsInternalOperation { get; set; }				

		public Guid? OrganizationId { get; set; }

		[Column(TypeName = "jsonb")]
		public string SlippyTileData { get; set; }

		public virtual ICollection<NegotiationAgreement> NegotiationAgreements { get; set; }

		[Column(TypeName = "jsonb")]
		public string WayPointsList { get; set; }

		[Column(TypeName = "jsonb")]
		public string CoordinatesList { get; set; }

        public string ATCComments { get; set; }

        public string GroupId { get; set; }
    }
}