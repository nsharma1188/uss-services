﻿using System;
using System.Linq;

namespace AnraUssServices.Models
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll(string Id = "");

        T Find(long key);

        T Find(Guid key);

        void Remove(long key);

        void Remove(Guid key);

        void Add(T item);

        void Update(T item);

        T GetNewItem();
    }
}