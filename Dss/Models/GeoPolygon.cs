using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AnraUssServices.Dss.Models
{ 
    /// <summary>
    /// An enclosed area on the earth. The bounding edges of this polygon shall be the shortest paths between connected vertices.  This means, for instance, that the edge between two points both defined at a particular latitude is not generally contained at that latitude. The winding order shall be interpreted as the order which produces the smaller area. The path between two vertices shall be the shortest possible path between those vertices. Edges may not cross. Vertices may not be duplicated.  In particular, the final polygon vertex shall not be identical to the first vertex.
    /// </summary>
    [DataContract]
    public class GeoPolygon
    { 
        /// <summary>
        /// Gets or Sets Vertices
        /// </summary>
        [Required]
        [DataMember(Name="vertices", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vertices")]
        [MinLength(3)]
        public List<LatLngPoint> Vertices { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GeoPolygon {\n");
            sb.Append("  Vertices: ").Append(Vertices).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
