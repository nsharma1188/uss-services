using System;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AnraUssServices.Dss.Models
{ 
    /// <summary>
    /// Point on the earth&#x27;s surface.
    /// </summary>
    [DataContract]
    public class LatLngPoint
    { 
        /// <summary>
        /// Gets or Sets Lng
        /// </summary>
        [Required]
        [DataMember(Name="lng")]
        public double? Lng { get; set; }

        /// <summary>
        /// Gets or Sets Lat
        /// </summary>
        [Required]
        [DataMember(Name="lat")]
        public double? Lat { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class LatLngPoint {\n");
            sb.Append("  Lng: ").Append(Lng).Append("\n");
            sb.Append("  Lat: ").Append(Lat).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
