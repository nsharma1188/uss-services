using System;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AnraUssServices.Dss.Models
{ 
    /// <summary>
    /// A three-dimensional geographic volume consisting of a vertically-extruded polygon.
    /// </summary>
    [DataContract]
    public class Volume3D
    { 
        /// <summary>
        /// Projection of this volume onto the earth&#x27;s surface.
        /// </summary>
        /// <value>Projection of this volume onto the earth&#x27;s surface.</value>
        [Required]        
        [DataMember(Name = "footprint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "footprint")]
        public GeoPolygon Footprint { get; set; }

        /// <summary>
        /// Minimum bounding altitude of this volume.
        /// </summary>
        /// <value>Minimum bounding altitude of this volume.</value>        
        [DataMember(Name = "altitude_lo", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude_lo")]
        public double AltitudeLo { get; set; }

        /// <summary>
        /// Maximum bounding altitude of this volume.
        /// </summary>
        /// <value>Maximum bounding altitude of this volume.</value>        
        [DataMember(Name = "altitude_hi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude_hi")]
        public double AltitudeHi { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Volume3D {\n");
            sb.Append("  Footprint: ").Append(Footprint).Append("\n");
            sb.Append("  AltitudeLo: ").Append(AltitudeLo).Append("\n");
            sb.Append("  AltitudeHi: ").Append(AltitudeHi).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
