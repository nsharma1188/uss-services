using System;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using AnraUssServices.Common.Validators;

namespace AnraUssServices.Dss.Models
{ 
    /// <summary>
    /// Contiguous block of geographic spacetime.
    /// </summary>
    [DataContract]
    public class Volume4D
    { 
        /// <summary>
        /// Constant spatial extent of this volume.
        /// </summary>
        /// <value>Constant spatial extent of this volume.</value>
        [Required]        
        [DataMember(Name = "spatial_volume", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "spatial_volume")]
        public Volume3D SpatialVolume { get; set; }

        /// <summary>
        /// Beginning time of this volume.  RFC 3339 format, per OpenAPI specification.
        /// </summary>
        /// <value>Beginning time of this volume.  RFC 3339 format, per OpenAPI specification.</value>        
        [DataMember(Name = "time_start", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_start")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeStart { get; set; }

        /// <summary>
        /// End time of this volume.  RFC 3339 format, per OpenAPI specification.
        /// </summary>
        /// <value>End time of this volume.  RFC 3339 format, per OpenAPI specification.</value>        
        [DataMember(Name = "time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeEnd { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Volume4D {\n");
            sb.Append("  SpatialVolume: ").Append(SpatialVolume).Append("\n");
            sb.Append("  TimeStart: ").Append(TimeStart).Append("\n");
            sb.Append("  TimeEnd: ").Append(TimeEnd).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
