﻿using AnraUssServices.Dss.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Dss.ViewModel.IdentificationServiceArea
{
    /// <summary>
    /// Parameters for a request to create/update an Identification Service Area in the DSS.
    /// </summary>
    [DataContract]
    public class ISAParametersViewModel
    {
        /// <summary>
        /// The bounding spacetime extents of this Identification Service Area.  Start and end times must be specified.  These extents should not reveal any sensitive information about the flight or flights within them.  This means, for instance, that extents should not tightly-wrap a flight path, nor should they generally be centered around the takeoff point of a single flight.
        /// </summary>
        /// <value>The bounding spacetime extents of this Identification Service Area.  Start and end times must be specified.  These extents should not reveal any sensitive information about the flight or flights within them.  This means, for instance, that extents should not tightly-wrap a flight path, nor should they generally be centered around the takeoff point of a single flight.</value>
        [Required]
        [DataMember(Name = "extents", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "extents")]
        public Volume4D Extents { get; set; }
    }
}
