﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Controllers;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AnraUssServices.Utility
{
	public class OperationSearchUtility
	{
		private UssDiscoveryApi ussDiscoveryApi;
		private FimsTokenProvider fimsAuthentication;
		private readonly AnraHttpClient httpClient;
		private AnraConfiguration _config;
		private readonly DatabaseFunctions dbFunctions;
		private readonly IRepository<Models.UtmInstance> utmInstanceRepository;
		private readonly ILogger<OperationController> logger;
		private readonly IRepository<Models.Operation> operationRepository;
		private SlippyTiles slippyTilesLogic;
		private GridCell gridCellLogic;
		private readonly InterUssApi interUssApi;

		public OperationSearchUtility(UssDiscoveryApi ussDiscoveryApi,
			FimsTokenProvider fimsAuthentication,
			AnraHttpClient httpClient,
			AnraConfiguration configuration,
			DatabaseFunctions dbFunctions,
			IRepository<Models.Operation> operationRepository,
			SlippyTiles slippyTilesLogic,
			GridCell gridCellLogic,
			InterUssApi interUssApi,
			IRepository<UtmInstance> utmInstanceRepository, ILogger<OperationController> logger)
		{
			this.ussDiscoveryApi = ussDiscoveryApi;
			this.fimsAuthentication = fimsAuthentication;
			this.httpClient = httpClient;
			this._config = configuration;
			this.dbFunctions = dbFunctions;
			this.operationRepository = operationRepository;
			this.utmInstanceRepository = utmInstanceRepository;
			this.logger = logger;
			this.slippyTilesLogic = slippyTilesLogic;
			this.gridCellLogic = gridCellLogic;
			this.interUssApi = interUssApi;
		}

		public List<OperationSearchResultItem> GetOperationDataFromLocal(OperationSearchItem criteria, bool isPublicSearch = false, string authorization = "")
		{
            //When the search Radius is 0 then the default is 5000 feet
            if (criteria.SearchRadius == 0)
            {
                criteria.SearchRadius = 5000;
            }

            //Covert radius from feet to meter
            int searchRadius = criteria.SearchRadius == 0 ? Convert.ToInt32(5000 / 3.28084) : Convert.ToInt32(criteria.SearchRadius / 3.28084);

            var result = dbFunctions.GetActiveOperations(criteria.SearchBy, criteria.Location.ToNpgsqlPointNullable(), criteria.Drone, searchRadius).ToList();

			//Fetch Operation Dependent Entities Data
			List<OperationSearchResultItem> lstOperation = new List<OperationSearchResultItem>();
			result.ForEach(x =>
			{                
                var operationViewModel = operationRepository.Find(x.Gufi).Adapt<OperationViewModel>();

                OperationSearchResultItem operationSearchResult = new OperationSearchResultItem();

                operationSearchResult.OperationVolumes = operationViewModel.OperationVolumes;

                operationSearchResult.SubmitTime = operationViewModel.SubmitTime;
                operationSearchResult.UssName = operationViewModel.UssName;
                operationSearchResult.UserId = operationViewModel.UserId;
                operationSearchResult.Gufi = operationViewModel.Gufi;
                operationSearchResult.Contact = operationViewModel.Contact;
                operationSearchResult.UserEmail = operationViewModel.Contact.EmailAddresses.Count > 0 ? string.Join(',', operationViewModel.Contact.EmailAddresses) : string.Empty;                
                operationSearchResult.ControllerLocation = operationViewModel.ControllerLocation;
                operationSearchResult.UserName = operationViewModel.Contact.Name;
                operationSearchResult.UserPhoneNumber = operationViewModel.Contact.PhoneNumbers.Count > 0 ? string.Join(',', operationViewModel.Contact.PhoneNumbers) : string.Empty;
                operationSearchResult.State = operationViewModel.State;
                operationSearchResult.IsInternalOperation = operationViewModel.IsInternalOperation;
                operationSearchResult.UasRegistrations = operationViewModel.UasRegistrations;
				operationSearchResult.FaaRule = operationViewModel.FaaRule;


				lstOperation.Add(operationSearchResult);
			});

            return lstOperation;
		}

		public List<OperationSearchResultItem> GetOperationsFromExternalUss(OperationSearchItem criteria, string authorization = "")
		{
			List<OperationSearchResultItem> lstOperationResultItems = new List<OperationSearchResultItem>();

			try
			{
					double lat = (double)criteria.Location.Coordinates[1];
					double lon = (double)criteria.Location.Coordinates[0];

                    if(lat == 0 ||  lon == 0)
                    {
                        return lstOperationResultItems;
                    }

					//When the search Radius is 0 then the default is 5000 feet
					if (criteria.SearchRadius == 0)
					{
						criteria.SearchRadius = 5000;
					}

					//Create the circle using the point and the radius value
					var searchRadius = dbFunctions.GetCircle("Point(" + lon + " " + lat + ")", (criteria.SearchRadius / 3.28084));//Converting Radius to meters
					var slippyResponse = slippyTilesLogic.GetSlippyTiles(_config.DefaultZoomLevel, GetCSVCoordinateList(searchRadius));

					if (slippyResponse.Data.GridCells.Any())
					{
						var gridCellMetaData = new List<JSendGridCellMetadataResponse>();

						foreach (var grid in slippyResponse.Data.GridCells)
						{
							var response = gridCellLogic.GridCellOperators(grid.Zoom, grid.X, grid.Y);

							if (response != null)
							{
								gridCellMetaData.Add(response);
							}
						}

						if (gridCellMetaData.Any())
						{
							//Get External Operators
							var extOperators = gridCellMetaData.SelectMany(x => x.Data.Operators).Where(p => !p.Uss.Equals(_config.ClientId, StringComparison.InvariantCultureIgnoreCase)).ToList();

							if (extOperators.Any())
							{
								var externalOperations = new List<Utm.Models.Operation>();

								extOperators.ForEach(opr =>
								{
									opr.Operations.ForEach(op =>
									{
										var externalOperation = interUssApi.GetOperatorOperationAsync(opr, op.Gufi.ToString());
										if (externalOperation != null && externalOperation.Gufi != Guid.Empty)
										{
											if (externalOperation.SubmitTime >= DateTime.UtcNow.Subtract(TimeSpan.FromHours(12)))
											{
												externalOperations.Add(externalOperation);
											}
										}
									});
								});

								//Check For External Operations
								if (externalOperations.Any())
								{
									var sortedOps = externalOperations.OrderByDescending(x => x.SubmitTime).ToList();
									var filterOps = new List<Utm.Models.Operation>();

									if (criteria.SearchBy.Equals(EnumUtils.GetDescription(OperationSearchType.DRONE)))
									{
										filterOps = sortedOps.Where(x => x.UasRegistrations.FirstOrDefault().RegistrationId.ToString().EndsWith(criteria.Drone, StringComparison.InvariantCultureIgnoreCase) && !x.State.Equals(OperationState.CLOSED)).ToList();
									}

									if (criteria.SearchBy.Equals(EnumUtils.GetDescription(OperationSearchType.LOCATION)))
									{										
										filterOps = sortedOps.Where(x => !x.State.Equals(OperationState.CLOSED)).ToList();
									}

									if (filterOps.Any())
									{
										filterOps.ForEach(op =>
										{
											OperationSearchResultItem operationSearchResult = new OperationSearchResultItem();

											var operationViewModel = op.Adapt<OperationViewModel>();
											operationSearchResult.OperationVolumes = operationViewModel.OperationVolumes;

											operationSearchResult.SubmitTime = operationViewModel.SubmitTime;
											operationSearchResult.UssName = operationViewModel.UssName;
											operationSearchResult.UserId = operationViewModel.UserId;
											operationSearchResult.Gufi = operationViewModel.Gufi;
											operationSearchResult.Contact = operationViewModel.Contact;
											operationSearchResult.UserEmail = operationViewModel.Contact.EmailAddresses.Count > 0 ? string.Join(',', operationViewModel.Contact.EmailAddresses) : string.Empty;                                            
                                            operationSearchResult.ControllerLocation = operationViewModel.ControllerLocation;
                                            operationSearchResult.UserName = operationViewModel.Contact.Name;
											operationSearchResult.UserPhoneNumber = operationViewModel.Contact.PhoneNumbers.Count > 0 ? string.Join(',', operationViewModel.Contact.PhoneNumbers) : string.Empty;
                                            operationSearchResult.State = operationViewModel.State;
                                            operationSearchResult.IsInternalOperation = "False";
                                            operationSearchResult.UasRegistrations = operationViewModel.UasRegistrations;
                                            operationSearchResult.FaaRule = operationViewModel.FaaRule;

                                            //Add Search List
                                            lstOperationResultItems.Add(operationSearchResult);
										});

									}
								}
							}

                            //Look for Internal Operations

                            var internalOps = string.IsNullOrEmpty(authorization) ? GetOperationDataFromLocal(criteria, true) : GetOperationDataFromLocal(criteria, false, authorization);
                            

                            if (internalOps.Any())
							{
								lstOperationResultItems.AddRange(internalOps);
							}
						}
					}

				return lstOperationResultItems;
			}
			catch (Exception ex)
			{
				logger.LogError("Exception ::: OperationSearchUtility :: GetOperationsFromExternalUss : Exception {0}", ex.Message);

				return lstOperationResultItems;
			}
		}

		public string GetCSVCoordinateList(Polygon searchGeoFenceGeography)
		{
			string csvList = string.Empty;

            searchGeoFenceGeography.Coordinates[0].ForEach(x =>
			{
				csvList = csvList + string.Format("{0},{1},", x[1], x[0]);
			});

			return csvList.Substring(0, csvList.Length - 1);
		}
	}
}