﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AnraUssServices.Utility
{
    public class SSLClient
    {
        public static String username = "anratechnologies";
        public static String apikey = "cb2dee457c781264e88884e08f0b7cd706ce6032";
        public static Boolean useCompression = false;
        public static String initiation_command =
        //"live version 6.0 username anratechnologies password cb2dee457c781264e88884e08f0b7cd706ce6032 events \"position\" latlong \"29 -95 30 -94\"";

        "live username " + username + " password " + apikey 
            +
            (useCompression ? " compression deflate" : "") 
            + "\n";

        //"live username " + username + " password " + apikey +
        //    (useCompression ? " compression deflate" : "") + "\n";

        // The following method is invoked by the RemoteCertificateValidationDelegate
        // prevent communication with unauthenticated server
        public static bool ValidateServerCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                // authenticated
                return true;
            }

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);
            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }
        private static void RunClient(string machineName, string serverName)
        {
            // Create a TCP/IP client socket.
            TcpClient client = new TcpClient(machineName, 1501);

            // Create ssl stream to read data
            SslStream sslStream = new SslStream(
                client.GetStream(),
                true,
                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                null);
            try
            {
                // server name must match name on the server certificate.
                sslStream.AuthenticateAsClient(serverName);
                Console.WriteLine("sslStream AuthenticateAsClient completed.");
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                client.Close();
                return;
            }

            // Send initiation command to the server.
            // Encode to a byte array.
            Console.WriteLine(initiation_command);
            byte[] messsage = Encoding.UTF8.GetBytes(initiation_command + "\n");
            sslStream.Write(messsage);

            sslStream.Flush();

            //read from server, print to console:
            StreamReader sr;
            if (useCompression)
            {
                DeflateStream deflateStream = new DeflateStream(sslStream, CompressionMode.Decompress);
                sr = new StreamReader(deflateStream);
            }
            else
            {
                sr = new StreamReader(sslStream);
            }
            int limit = 1000;
            while (limit > 0)
            {
                string line = null;
                if (useCompression)
                {
                    try
                    {
                        Task<string> s = sr.ReadLineAsync();
                        line = s.Result;
                    }
                    catch (Exception e)
                    {

                    }
                }
                else
                {
                    line = sr.ReadLine();
                }

                if (line == null)
                {
                    limit = 0;
                }
                else
                {
                    Console.WriteLine(" Received: " + line);
                    parse(line);
                    limit--;
                }

            }

            // Close the client connection.
            sr.Close();
            client.Close();
            Console.WriteLine("Client closed.");
        }

        public static void parse(string mes)
        {
            //parse with JSON.NET
            //FlightObject flight = JsonConvert.DeserializeObject<FlightObject>(mes);
            Console.WriteLine(" --------------- Message ------------------ \n");
            Console.WriteLine(mes);
            Console.WriteLine(" ------------------------------------------ \n");
        }

        public static void connectToADSB()
        {
            // machineName is the host running the server application.
            String machineName = "firehose.flightaware.com";
            String serverCertificateName = machineName;

            //connect, read data
            RunClient(machineName, serverCertificateName);

        
        }

        
        

    }
}
