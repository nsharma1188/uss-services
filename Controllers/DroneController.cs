﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.Lookups;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Data;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;

using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Contacts;
using AnraUssServices.ViewModel.Drones;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Drone Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DroneController : BaseController
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepository<Drone> droneRepository;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ApplicationDbContext _context; 
        private AnraConfiguration _config;
        private AnraMqttClient _anraMqttClient { get; set; }
        private readonly IRepository<UserDroneAssociation> userDroneRepository;
        private DroneDocument DroneDocument { get; }
		private readonly OrganizationClient organazationClient;

		public DroneController(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            FimsTokenProvider fimsAuthentication, 
            AnraConfiguration configuration,
            DroneDocument droneDocument,
            IRepository<UserDroneAssociation> userDroneRepository,
            IRepository<Drone> droneRepository,
			OrganizationClient organazationClient)
        {
            _context = context;         
            _config = configuration;
            DroneDocument = droneDocument;
            this.fimsAuthentication = fimsAuthentication;
            this.droneRepository = droneRepository;
            this.httpContextAccessor = httpContextAccessor;
            this.userDroneRepository = userDroneRepository;
			this.organazationClient = organazationClient;
        }

        /// <summary>
        /// Request New Drone Instance
        /// </summary>
        /// <remarks>Allows querying for New Drone Instance.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>        
        [HttpGet]
        [Authorize("uss:read-drone")]
        [Route("/drone/new_drone")]
        [SwaggerOperation("GetNewDroneInstance"), SwaggerResponse(200, type: typeof(DroneViewModel))]
        public virtual IActionResult GetNewItem()
        {

            var drone = new DroneViewModel
            {
                BrandLookup = DroneBrandLookup.GetLookup(),
                TypeLookup = DroneTypeLookup.GetLookup()
            };

            return new ObjectResult(drone);
        }

        /// <summary>
        /// Request information regarding Drones
        /// </summary>
        /// <param name="userid">Drone Listing for the user</param>
        /// <remarks>Allows querying for Drone data.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-drone")] 
        [Route("/drone/listing")]
        [SwaggerOperation("GetDrones"), SwaggerResponse(200, type: typeof(List<DroneViewModel>))]
        public virtual IActionResult GetDrones()
        {
            var organizationId = GetCurrentUserOrganizationId();
            var droneList = new List<DroneViewModel>();
            var roles = GetCurrentUserRole();

            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                //List all the drone
                droneList = droneRepository.GetAll().OrderByDescending(x => x.DateModified)
                                .Where(x => !x.IsDeleted).Adapt<List<DroneViewModel>>();
            }
            else if (CheckRole(EnumUserRole.ANSP.ToString()))
            {
                //List all under current Group Id
                droneList = droneRepository.GetAll().OrderByDescending(x => x.DateModified)
                                .Where(x => !x.IsDeleted)
                                .Where(x => x.GroupId.Equals(GetCurrentGroupId())).Adapt<List<DroneViewModel>>();
            }
            else
            {
                //List all under the current organization
                droneList = droneRepository.GetAll().OrderByDescending(x => x.DateModified)
                                .Where(x => !x.IsDeleted)
                                .Where(x => x.OrganizationId.Equals(organizationId)).Adapt<List<DroneViewModel>>();
            }

            return new ObjectResult(droneList);
        }

        /// <summary>
        /// Save New Drone or Update existing Drone.
        /// </summary>
        /// <param name="drone">The operation data</param>
        /// <remarks>A POST to the Drone endpoint allows for saving new or update drone data</remarks>        
        /// <response code="201">Drone data received.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-drone")]        
        [Route("/drone/save")]        
        [SwaggerOperation("PostDrone"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostDrone([FromBody]DroneViewModel drone)
        {
            if(Guid.TryParse(drone.Uid, out Guid droneGuid))
            {
                
                Guid guidDrone = Guid.Parse(drone.Uid);
                var organizationId = GetCurrentUserOrganizationId();

                //if (drone.Uid != null)
                if(droneRepository.Find(guidDrone) != null) //check if it is exist update drone information.
                {
                    var existingDrone = droneRepository.Find(guidDrone);

                    drone.Adapt(existingDrone);
                    droneRepository.Update(existingDrone);
                    DroneDocument.AddOrUpdate(drone);
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"{nameof(Drone).ToString()} Updated Successfully.", Id = drone.Uid.ToString() });
                }
                else // if it is not, add new drone
                {
                    //Generate Guid from service
                    //var newId = Guid.NewGuid();
                    //drone.Uid = newId.ToString();
                    //drone.RemoteId = newId.ToString();

                    //Check if there is any Drone exist with given UUID.
                    //if (existingDrone != null)
                    //{
                    //    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"This UUID is already exist.", Id = drone.Uid.ToString() });
                    //}

                    var droneModel = drone.Adapt<Drone>();
                    droneModel.OrganizationId = organizationId;
                    droneModel.UserId = GetCurrentUserId();
                    droneModel.GroupId = GetCurrentGroupId();
                    droneRepository.Add(droneModel);

                    DroneDocument.AddOrUpdate(drone);

                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"{nameof(Drone).ToString()} Created Successfully.", Id = drone.Uid });
                }
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"This UUID is not valid.", Id = drone.Uid });
            }

        }

        /// <summary>
        /// Request information regarding a Drone
        /// </summary>
        /// <param name="uuid">todo: describe uuid parameter on GetDroneByRegistrationNo</param>
        /// <remarks>Allows querying for Drone data.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-drone")]        
        [Route("/drone/registration/{uuid}")]        
        [SwaggerOperation("GetDroneByRegistrationNo"), SwaggerResponse(200, type: typeof(DroneViewModel))]        
        public virtual IActionResult GetDroneByRegistrationNo([FromRoute]Guid uuid)
        {
            var newDroneItem = droneRepository.GetAll().FirstOrDefault(x => x.Uid == uuid &&  !x.IsDeleted).Adapt<Drone, DroneViewModel>();

            if(newDroneItem != null)
            {
                newDroneItem.BrandLookup = DroneBrandLookup.GetLookup();
                newDroneItem.TypeLookup = DroneTypeLookup.GetLookup();

                return new ObjectResult(newDroneItem);
            }
            else
            {
                //If no drone found then return blank model response
                return new ObjectResult(new DroneViewModel());
            }
        }

        /// <summary>
        /// Delete a Drone
        /// </summary>
        /// <param name="uuid">todo: describe uuid parameter on DeleteDroneByRegistrationNo</param>
        /// <remarks>Allows deleting a Drone.</remarks>
        /// <response code="200">Successful data request.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpDelete]
        [Authorize("uss:write-drone")]        
        [Route("/drone/{uuid}")]        
        [SwaggerOperation("DeleteDroneByRegistrationNo"), SwaggerResponse(200, type: typeof(DroneViewModel))]        
        public virtual IActionResult DeleteDroneByRegistrationNo(Guid uuid)
        {            
            var drone = droneRepository.GetAll().FirstOrDefault(x => x.Uid == uuid && !x.IsDeleted);
            //Soft Delete As Per PHP Code
            drone.IsDeleted = true;
            droneRepository.Update(drone);
            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"{nameof(Drone).ToString()} Deleted Successfully.", Id = uuid.ToString() });
        }

        [HttpGet]
        [Authorize("uss:read-drone")]        
        [Route("/drone/associated-users/{uuid}")]
        [SwaggerOperation("GetAssociatedUsers"), SwaggerResponse(200, type: typeof(List<UserDroneAssociation>))]
        public virtual IActionResult GetAssociatedUsers(string uuid)
        {            
            var result = userDroneRepository.GetAll().Where(p => p.RegistrationId.Equals(uuid)).ToList();
            return new ObjectResult(result);
        }

        [HttpPost]
        [Authorize("uss:write-drone")]        
        [Route("/drone/{uuid}/associate-users")]
        [SwaggerOperation("PostDroneAssociation"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostDroneAssociation([FromRoute] string uuid, [FromBody] List<UserDroneAssociation> userDroneAssociations)
        {
            //Find Previous Assignations If any
            var existingAssociations = userDroneRepository.GetAll().Where(p => p.RegistrationId.Equals(uuid)).ToList();


            //Remove All Previous Assignation
            existingAssociations.ForEach(x => { userDroneRepository.Remove(x.UserDroneAssociationUid); });

            //Add New Assignations
            userDroneAssociations.ForEach(x => { userDroneRepository.Add(x); });

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = string.Concat("Drone ", (userDroneAssociations.Count() > 0 ? "Associated":"Disassociated"), " Successfully."), Id = uuid });
        }

		/// <summary>
		/// Request Drone Associated Organization Information 
		/// </summary>
		/// <param name="droneId">Drone UUID </param>
		/// <remarks>Allows querying for Orginazation data based on drone Id.</remarks>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpGet]
        [Authorize("uss:read-drone")]        
        [Route("/drone/associated-organization/{droneId}")]
		[SwaggerOperation("GetDroneOrganization"), SwaggerResponse(200, type: typeof(PersonOrOrganizationViewModel))]
		public virtual IActionResult GetDroneOrganization([FromRoute]string droneId, [FromHeader]string authorization)
		{
			var organizationDetails = new OperationSearchResultItem();

			if (string.IsNullOrEmpty(droneId))
			{
				return new ObjectResult(organizationDetails);
			}

			var drone = droneRepository.GetAll().FirstOrDefault(x => x.Uid.ToString().EndsWith(droneId, StringComparison.InvariantCultureIgnoreCase) && !x.IsDeleted).Adapt<Drone, DroneViewModel>();

			if (drone != null)
			{
				organizationDetails = organazationClient.GetOrganizationDetails(drone.OrganizationId.ToString(), authorization);
			}
			
			return new ObjectResult(organizationDetails);
		}

	}
}