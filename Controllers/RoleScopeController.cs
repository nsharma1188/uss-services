﻿using AnraUssServices.Common;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Roles;
using AnraUssServices.ViewModel.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    [Produces("application/json")]
    [Route(@"api/[controller]")]
    public class RoleScopeController : BaseController
    {
        private readonly RoleScopeClient roleScopeClient;
        public RoleScopeController(RoleScopeClient roleScopeClient)
        {
            this.roleScopeClient = roleScopeClient;
        }

        /// <summary>
        /// Returns the list of roles with their scope
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/roles")]
        [SwaggerOperation(nameof(GetRoles)), SwaggerResponse(200, type: typeof(List<RoleItem>))]
        public virtual IActionResult GetRoles([FromHeader] string authorization)
        {
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                return new ObjectResult(RoleItem.GetRoleDescription().Where(x => x.Name != EnumUserRole.SUPERADMIN.ToString()));
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });

            }
        }

        /// <summary>
        /// Return a new model for Role
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/role/new")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerOperation(nameof(GetNewRole)), SwaggerResponse(200, type: typeof(RolesModel))]
        public virtual IActionResult GetNewRole([FromHeader] string authorization)
        {
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                var role = new RolesModel();

                var scopesList = roleScopeClient.GetScopes(authorization);

                role.Scopes = scopesList;
                role.Id = RoleItem.GetRoleDescription().Select(x => x.Id).Max() + 1;

                return new ObjectResult(role);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });

            }
        }

        /// <summary>
        /// Create a new Role with appropriate scopes
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route(@"/role")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [SwaggerOperation(nameof(CreateRole)), SwaggerResponse(201, type: typeof(UTMRestResponse))]
        public virtual IActionResult CreateRole([FromHeader] string authorization, [FromBody] RolesModel role)
        {
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                var response = roleScopeClient.CreateRole(authorization, role);

                return new ObjectResult(response);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });

            }
        }

        /// <summary>
        /// Returns the list of scope based on role
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/scopes/{roleId}")]
        [SwaggerOperation(nameof(GetScopesByRoleId)), SwaggerResponse(200, type: typeof(RolesViewModel))]
        public virtual IActionResult GetScopesByRoleId([FromHeader] string authorization, [FromRoute]int roleId)
        {
            var roles = new RolesViewModel();

            try
            {
                if(!RoleItem.GetRoleDescription().Exists(x=>x.Id == roleId))
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = @"Role Id doesn't exist" });
                }

                if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
                {
                    var roleScopeList = roleScopeClient.GetScopes(authorization, roleId);
                    var scopeList = roleScopeClient.GetScopes(authorization);
                    roles = new RolesViewModel();
                    roles.Id = roleId;
                    roles.RoleName = RoleItem.GetRoleDescription().Where(x => x.Id == roleId).FirstOrDefault().Name;
                    var scopeItemList = new List<ScopesViewModel>();

                    foreach (var item in scopeList)
                    {
                        var scopeItem = new ScopesViewModel()
                        {
                            Id = item.Id,
                            ScopeName = item.ScopeName,
                            IsAssigned = roleScopeList.Exists(x => x.ScopeName == item.ScopeName)
                        };

                        scopeItemList.Add(scopeItem);
                    }
                    roles.Scopes = scopeItemList;

                    return new ObjectResult(roles);
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
                }
            }
            catch(Exception ex)
            {
                return new ObjectResult(roles);
            }
        }


        [HttpGet]
        [Authorize]
        [Route(@"/scopes")]
        [SwaggerOperation(nameof(GetScopesList)), SwaggerResponse(200, type: typeof(List<RoleItem>))]
        public virtual IActionResult GetScopesList([FromHeader] string authorization)
        {
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                var response = roleScopeClient.GetScopes(authorization);

                return new ObjectResult(response);

            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });

            }
        }

        /// <summary>
        /// Update the scope of the current role
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route(@"/scopes")]
        [SwaggerOperation(nameof(UpdateScopes)), SwaggerResponse(201, type: typeof(UTMRestResponse))]
        public virtual IActionResult UpdateScopes([FromHeader] string authorization, [FromBody] RolesViewModel role)
        {
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                if (!RoleItem.GetRoleDescription().Exists(x => x.Name == role.RoleName))
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = @"Invalid Role Name" });
                }
                else
                { 
                    var updatedRole = new RolesModel();
                    updatedRole.Id = role.Id;
                    updatedRole.RoleName = role.RoleName;
                    var scopesList = new List<ScopeModel>();

                    foreach(var item in role.Scopes)
                    {
                        var scope = new ScopeModel();

                        if (item.IsAssigned)
                        {
                            scope.Id = item.Id;
                            scope.ScopeName = item.ScopeName;
                            scopesList.Add(scope);
                        }
                    }
                    updatedRole.Scopes = scopesList;

                    var response = roleScopeClient.UpdateRole(authorization, updatedRole);

                    return new ObjectResult(response);
                }
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });

            }
        }

    }
}
