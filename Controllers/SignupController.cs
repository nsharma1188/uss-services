﻿using AnraUssServices.Common;
using AnraUssServices.Common.Notification;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Models;
using AnraUssServices.Models.Audits;
using AnraUssServices.Models.Signup;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Audits;
using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Roles;
using AnraUssServices.ViewModel.Users;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AnraUssServices.Controllers
{
	[Route(@"api/[controller]")]
	public class SignupController : BaseController
	{
		private readonly UserClient userClient;
		private readonly IRepository<Signup> signupRepository;
		private readonly SendNotification sendNotification;
		private readonly OrganizationClient organizationClient;
		private readonly IRepository<Audit> auditRepository;
		private readonly AnraConfiguration configuration;

		public SignupController(UserClient userClient, SendNotification sendNotification,
			IRepository<Signup> signupRepository, OrganizationClient organizationClient,
			IRepository<Audit> auditRepository, AnraConfiguration configuration)
		{
			this.userClient = userClient;
			this.signupRepository = signupRepository;
			this.sendNotification = sendNotification;
			this.organizationClient = organizationClient;
			this.auditRepository = auditRepository;
			this.configuration = configuration;
		}

		/// <summary>
		/// Get the new signup model
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route(@"/signup/new")]
		[SwaggerOperation(nameof(GetSignupItem)), SwaggerResponse(200, type: typeof(OrganizationItem))]
		public virtual IActionResult GetSignupItem()
		{
			var token = userClient.GetToken(true);

			var orginazation = new OrganizationItem
			{
				AdsbSourceLookup = AdsbSourceLookupItem.GetLookup(),
				CurrencyLookup = CurrencyLookupItem.GetLookup(),
				UnitLookup = UnitLookupItem.GetLookup()
			};

			var usersList = userClient.GetUsers(token.TokenType + " " + token.AccessToken);
			var anspUsersList = new List<OrganizationItem>();

			//Filtering only ANSP users
			foreach (var item in usersList)
			{
				var anspUser = new OrganizationItem();

				if (item.Roles.Exists(x => x.Name.Equals(EnumUserRole.ANSP.ToString())))
				{
					anspUser.Name = item.Email;
					anspUser.GroupId = item.GroupId;
					anspUser.FirstName = item.FirstName;
					anspUser.LastName = item.LastName;
					anspUsersList.Add(anspUser);
				}
			}
			orginazation.Organizations = anspUsersList;

			return new ObjectResult(orginazation);
		}

		/// <summary>
		/// Create users or organizations 
		/// </summary>
		/// <param name="signupItem"></param>
		/// <param name="signupType"></param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/signup/{signupType}")]
		[SwaggerOperation(nameof(Signup)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public async Task<IActionResult> Signup([FromBody] OrganizationItem signupItem, string signupType)
		{
			try
			{
				//Create a verification code or OTP
				string verificationCode = GenerateVerificationCode();

				if (signupItem.Password.Length > 10 || signupItem.Password.Length < 6 || signupItem.Password.Contains("!@#$%&*()"))
				{
					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = @"Password must use between 6-10 characters and must not contain any special character or symbol.", Id = signupItem.OrganizationId });
				}

				//Check if User already exists with signup email.
				//var user = signupRepository.Find(signupItem.UserName);
				var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(signupItem.UserName));

				if (user != null)
				{
					//If user exists then check if the user is verified or not
					if (user.IsVerified)
					{
						return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"An user having email - {signupItem.Email} already exists.", Id = user.UserId });
					}
					else
					{
						if (user.SendEmail)
						{
							var message = $"Verification Code: {verificationCode}";
							sendNotification.SendEmail(user.ContactEmail, message);
						}
						user.VerificationCode = verificationCode;
						signupRepository.Update(user);

						return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Accepted, Message = string.Format("You have already signed-up but not verified yourself. Please check your {0} and verify yourself.", user.SendSMS ? "email/sms" : "email"), Id = user.UserId });
					}
				}
				else
				{
                    string newUserId = Guid.NewGuid().ToString();

					//As Organization
					if (signupType.Equals(SignupType.Organization.ToString()))
					{
						var organization = signupItem.Adapt<Signup>();
						organization.GroupId = signupItem.GroupId;
						organization.OrganizationId = Guid.NewGuid().ToString();
						organization.UserId = newUserId;
						organization.VerificationCode = verificationCode;
						organization.SignupType = signupType;
						string[] role = { EnumUserRole.ADMIN.ToString() };
						organization.UserRole = role;

						signupRepository.Add(organization);

						//Send OTP for email verification
						var message = $"The one time password (otp) for registration is {verificationCode}";
						sendNotification.SendEmail(organization.ContactEmail, message);
					}

					//As User
					if (signupType.Equals(SignupType.Individual.ToString()))
					{
						var organization = signupItem.Adapt<Signup>();
						organization.GroupId = signupItem.GroupId;
						organization.OrganizationId = Guid.NewGuid().ToString();
						organization.UserId = newUserId;
						string[] role = { EnumUserRole.ADMIN.ToString(), EnumUserRole.PILOT.ToString() };
						organization.UserRole = role;
						organization.VerificationCode = verificationCode;
						organization.Name = signupItem.Name != null ? signupItem.Name : signupItem.FirstName + " Org";
						organization.SignupType = signupType;

						signupRepository.Add(organization);

						//Send OTP for email verification
						var message = $"The one time password (otp) for registration is {verificationCode}";
						sendNotification.SendEmail(organization.ContactEmail, message);
					}

					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Registration Complete. Please check you email and verify the account.", Id = newUserId });
				}
			}
			catch (Exception ex)
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = ex.Message.ToString(), Id = signupItem.OrganizationId });
			}
		}

		/// <summary>
		/// Verify the user using the VerificationCode
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/signup/verify")]
		[SwaggerOperation(nameof(UserVerification)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult UserVerification([FromBody]UserSignin user)
		{
			//var signupUser = signupRepository.Find(user.UserName);
			var signupUser = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(user.UserName));

			if (signupUser != null)
			{
				if (signupUser.IsVerified)
				{
					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Format("{0} is already active. You can SignIn.", signupUser.SignupType), Id = signupUser.UserId });
				}

				if (signupUser.VerificationCode.Equals(user.VerificationCode))
				{
					var token = userClient.GetToken(true);

					//Creating a new orginazationModel
					var organization = new OrganizationModel()
					{
						Address = signupUser.Address,
						LogoFile = signupUser.LogoFile,
						LastName = signupUser.LastName,
						GroupId = signupUser.GroupId,
						AdsbSource = signupUser.AdsbSource,
						Password = signupUser.Password,
						BaseLat = signupUser.BaseLat,
						BaseLng = signupUser.BaseLng,
						ContactEmail = signupUser.ContactEmail,
						ContactPhone = signupUser.ContactPhone,
						CurrencyId = signupUser.CurrencyId,
						FederalTaxId = signupUser.FederalTaxId,
						FirstName = signupUser.FirstName,
						GovtLicenseNumber = signupUser.GovtLicenseNumber,
						Name = signupUser.Name,
						OrganizationId = signupUser.OrganizationId,
						UnitId = signupUser.UnitId,
						UserId = signupUser.UserId,
						UserName = signupUser.UserName,
						Website = signupUser.Website
					};

					//If signup type is individual then assign both Pilot and Administrator role for now.
					if (signupUser.SignupType.Equals(SignupType.Individual.ToString()))
					{
						organization.UserRole.Add(RoleItem.GetRoleDescription().Where(x => x.Name ==
						EnumUserRole.ADMIN.ToString()).ToList().FirstOrDefault().Id);

						organization.UserRole.Add(RoleItem.GetRoleDescription().Where(x => x.Name ==
						EnumUserRole.PILOT.ToString()).ToList().FirstOrDefault().Id);
					}
					else
					{
						organization.UserRole.Add(RoleItem.GetRoleDescription().Where(x => x.Name ==
							EnumUserRole.ADMIN.ToString()).ToList().FirstOrDefault().Id);
					}

					var response = organizationClient.CreateOrganazation(organization, token.GetAuthorizationToken());

					if (response.HttpStatusCode == 201)
					{
                        //Updating the user as the user is now verified
                        signupUser.IsVerified = true;
                        signupUser.DateVerified = DateTime.UtcNow;
                        signupRepository.Update(signupUser);

                        response.Message = string.Format("{0} Created.", signupUser.SignupType);
					}
					else
					{
						response.Message = string.Format("Error while creating {0}.", signupUser.SignupType);
					}

					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "User verified.", Id = signupUser.UserId });
				}
				else
				{
					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Invalid verification code.", Id = signupUser.UserId });
				}
			}
			else
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "No Signup user details found.", Id = signupUser.UserId });
			}
		}

		/// <summary>
		/// Verify new User.
		/// </summary>
		/// <remarks>Verify new User.</remarks>
		/// <param name="user">The operation data</param>
		/// <response code="200">User Created.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		[HttpPost]
		[Route(@"/signup/resendotp")]
		[SwaggerOperation(nameof(UserVerification)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult ResendVerificationCode([FromBody]UserSignin user)
		{
			//var signupUser = signupRepository.Find(user.UserName);
			var signupUser = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(user.UserName));

			//Create a verification code or OTP
			string verificationCode = GenerateVerificationCode();

			if (signupUser != null)
			{
				//Updating the new verification code and sending it. 
				signupUser.VerificationCode = verificationCode;
				signupRepository.Update(signupUser);

				var message = $"Your Verification Code: {verificationCode}";
				sendNotification.SendEmail(signupUser.ContactEmail, message);

				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Verification code sent.", Id = signupUser.UserId });
			}
			else
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "User doesn't exist.", Id = signupUser.UserId });
			}
		}

		/// <summary>
		/// Returns the token from OAuth based on the username and password
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/user/login")]
		[SwaggerOperation(nameof(UserLogin)), SwaggerResponse(200, type: typeof(OAuthToken))]
		public async Task<IActionResult> UserLogin([FromBody] UserSignin user)
		{
			//Loging the audit when the user tries to login 
			//We are not maintaining audit trail for SuperAdmin
			var audit = new Audit()
			{
				IpAddress = HttpContext.Connection.RemoteIpAddress.ToString(),
				UserName = user.UserName,
				Platform = "USS",
				IsValidUser = false,
				Status = false,
				TimeStamp = DateTime.Now.ToString()
			};

			//For now 2FA is disabled for super admin
			if (user.UserName == configuration.OAuthClientId && user.Password == configuration.OAuthSecret)
			{
				var newToken = userClient.GetToken(true);

				audit.IsValidUser = true;
				audit.Status = true;

				return new ObjectResult(newToken);
			}
			else
			{
                //If there is some issues getting the token from OAuth
				var newToken = userClient.GetToken(user.UserName, user.Password);

				if (newToken == null)
				{
					auditRepository.Add(audit);

					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Please Contact System Administrator" });
				}
				else
				{
					//var existingUser = signupRepository.Find(user.UserName);
					var existingUser = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(user.UserName));

					var verificationCode = GenerateVerificationCode();

                    //Existing user will be null for the user has been created by other users and not been created from signup process.
					if (existingUser == null)
					{
                        //If the username or email is valid it contains the token
						if (newToken.Contains("token"))
						{
							var token = JsonConvert.DeserializeObject<OAuthToken>(newToken);
							var validUser = userClient.GetUsers(token.GetAuthorizationToken()).Where(x => x.UserName == user.UserName).ToList().FirstOrDefault();

                            var newUser = new Signup()
                            {
                                Address = string.IsNullOrEmpty(validUser.AddressLine1) ? null : validUser.AddressLine1,
                                ContactEmail = validUser.UserName,
                                UserId = validUser.UserId,
                                ContactPhone = string.IsNullOrEmpty(validUser.PhoneNumber) ? null : validUser.PhoneNumber,
                                FirstName = string.IsNullOrEmpty(validUser.FirstName) ? null : validUser.FirstName,
                                LastName = string.IsNullOrEmpty(validUser.LastName) ? null : validUser.LastName,
                                GroupId = validUser.GroupId,
                                OrganizationId = validUser.Organizations.FirstOrDefault().OrganizationId,
                                Name = string.IsNullOrEmpty(validUser.OrganizationName) ? null : validUser.OrganizationName,
                                UserName = user.UserName,
                                IsVerified = true,
                                SendEmail = true,
                                Password = user.Password,
                                VerificationCode = verificationCode,
                                IsUsed = false
							};

							signupRepository.Add(newUser);

							var message = $"Verification Code: {verificationCode}";
							sendNotification.SendEmail(user.UserName, message);

							return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Accepted, Message = "Check your Email. Enter Verification Code.", Id = validUser.UserId });
						}
						else
						{
                            //If the token contains null then it has invalid username 
							if (!newToken.Contains("null"))
							{
								audit.IsValidUser = true;
							}
							
							auditRepository.Add(audit);
							
							return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Invalid Username or Password" });
						}
					}
					else
					{
                        //creating the audit report
						audit.GroupId = existingUser.GroupId;
						audit.UserId = existingUser.UserId;
						audit.OrganizationId = existingUser.OrganizationId;
						audit.IsValidUser = true;

                        //Check if the user is valid
						if (newToken.Contains("token"))
						{
							var token = JsonConvert.DeserializeObject<OAuthToken>(newToken);

                            //Checking if this is the first login or you are loging in using the verification code.
							if (string.IsNullOrEmpty(user.VerificationCode))
							{
								existingUser.VerificationCode = verificationCode;
                                existingUser.IsUsed = false;
								signupRepository.Update(existingUser);

								var message = $"Verification Code: {verificationCode}";
								sendNotification.SendEmail(user.UserName, message);

								return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Accepted, Message = "Check your Email. Enter Verification Code.", Id = existingUser.UserId });
							}
							else
                            { 
                                //Comparing the OTP
								if (existingUser.VerificationCode == user.VerificationCode && !existingUser.IsUsed)
								{
                                    existingUser.IsUsed = true;
                                    signupRepository.Update(existingUser);

                                    audit.Status = true;
									auditRepository.Add(audit);

									return new ObjectResult(token);
								}
								else
								{
									auditRepository.Add(audit);

									return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Invalid Verification Code", Id = existingUser.UserId });
								}
							}
						}
						else
						{
							auditRepository.Add(audit);
							
							return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Invalid Username or Password" });
						}
					}
				}
			}
		}

		/// <summary>
		/// Generate randam 6 digit numeric verification code.
		/// </summary>
		/// <returns></returns>
		private string GenerateVerificationCode()
		{
			string oneTimePassword = "";
			Random rng = new Random();

			oneTimePassword = rng.Next(100000, 500000).ToString();

			return oneTimePassword;
		}
	}
}
