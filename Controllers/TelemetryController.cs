﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data; 
using AnraUssServices.Models; 
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.TelemetryMessages;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Telemetry Interface
    /// </summary>
    [Route("api/[controller]")]
    public class TelemetryController : BaseController
    {
        private readonly UtmMessenger utmMessenger;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IMediator mediator;
        private readonly IRepository<TelemetryMessage> telemetryMessageRepository;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ApplicationDbContext _context; 
        private AnraConfiguration _config;
        private AnraMqttClient _anraMqttClient { get; set; }
		private readonly ElevationApi elevationApi;

		public TelemetryController(ApplicationDbContext context,
            FimsTokenProvider fimsAuthentication, 
            AnraMqttClient anraMqttClient,
            AnraConfiguration configuration,
            UtmMessenger utmMessenger,
            IRepository<TelemetryMessage> telemetryMessageRepository,
			ElevationApi elevationApi)
        {
            _context = context; 
            _anraMqttClient = anraMqttClient;
            _config = configuration;
            this.fimsAuthentication = fimsAuthentication;
            this.telemetryMessageRepository = telemetryMessageRepository;
            this.utmMessenger = utmMessenger;
			this.elevationApi = elevationApi;
        }

        /// <summary>
        /// For submitting position/telemetry data for an active operation.
        /// </summary>
        /// <remarks>For submitting position/telemetry data for an active operation.</remarks>
        /// <param name="message">Telemetry packet</param>
        /// <response code="201">Message data received.</response>
        //[HttpPost]
        //[Authorize]
        //[Route("/telemetry")]
        //[SwaggerOperation("telemetry"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        //public virtual IActionResult TelemetryMessage([FromBody]TelemetryMessageItem message)
        //{
        //    message.TimeMeasured = DateTime.UtcNow;
        //    message.TimeSent = DateTime.UtcNow;

        //    var utmPosition = message.Adapt<Position>();

        //    var model = message.Adapt<TelemetryMessage>();
        //    telemetryMessageRepository.Add(model);

        //    utmMessenger.PositionNotification(utmPosition, message.Gufi);

        //    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Position Update Received." });
        //}

        /// <summary>
        /// Get all position updates for a particular Gufi
        /// </summary>
        /// <remarks>Allows querying for position updates.</remarks>
        /// <param name="gufi">Gufi</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-telemetry")]
        [Route("/telemetry/{gufi}")]
        [SwaggerOperation(nameof(GetPositionUpdatesByGufi)), SwaggerResponse(200, type: typeof(List<TelemetryMessageViewModel>))]
        public virtual IActionResult GetPositionUpdatesByGufi([FromRoute]string gufi)
        {
			//To Do - Code refactor required
			var newTelemetryList = new List<TelemetryMessageViewModel>();
            var result = telemetryMessageRepository.GetAll().Where(p => p.Gufi.Equals(Guid.Parse(gufi))).OrderBy(x => x.DateCreated);
            var telemetryList = result.Adapt<List<TelemetryMessageViewModel>>();

			//TO-Do need to find a proper solution for hadling telemetry for 3D Viewer.
			var count = 0;
			var elevation = 0.0;
			foreach (var telemetry in telemetryList)
			{
				//Calling the Elevation for every 10th packets
				if (count == 0 || count == 10)
				{
					var newElevation = elevationApi.GetElevationData(telemetry.Location.Coordinates[1].Value, telemetry.Location.Coordinates[0].Value)
						.Results.Select(x => x.Elevation).FirstOrDefault();
					telemetry.Roll = newElevation;
					elevation = newElevation;
					count = count == 10 ? 0 : count+1;
				}
				else
				{
					telemetry.Roll = elevation;
					count++;
				}
				newTelemetryList.Add(telemetry);
			}

            return new ObjectResult(telemetryList);
        }
    }
}