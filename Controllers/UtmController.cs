﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.Kafka;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.UtmInstances;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class UtmController : BaseController
    {
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly GridCell gridCell;
        private readonly SlippyTiles slippyTiles;
        private readonly AnraConfiguration configuration;
        private readonly UssDiscoveryApi ussDiscoveryApi;
        private readonly UssApi ussApi;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly ApplicationDbContext _context;
        private readonly ILogger<UtmController> logger;
        private readonly AnraConfiguration anraConfig;
        private UtmValidator _utmValidator;
        private readonly IMediator mediator;
		private readonly UserClient userClient;
        private readonly AnraMqttClient anraMqttClient;
        private readonly KafkaProducer kafkaProducer;

        public UtmController(ApplicationDbContext context,
            ILogger<UtmController> logger,
            AnraConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            DatabaseFunctions dbFunctions,
            AnraConfiguration anraConfig,
            IRepository<Models.Operation> operationRepository,
            SlippyTiles slippyTiles,
            UssDiscoveryApi ussDiscoveryApi,
            UssApi ussApi,
            GridCell gridCell,
            IMediator mediator,
            UtmValidator utmValidator,
            IRepository<UtmInstance> utmInstanceRepository,
			UserClient userClient, AnraMqttClient anraMqttClient,
            KafkaProducer kafkaProducer)
        {
            _context = context;
            _utmValidator = utmValidator;
            this.logger = logger;
            this.httpContextAccessor = httpContextAccessor;
            this.anraConfig = anraConfig;
            this.dbFunctions = dbFunctions;
            this.utmInstanceRepository = utmInstanceRepository;
            this.ussDiscoveryApi = ussDiscoveryApi;
            this.ussApi = ussApi;
            this.configuration = configuration;
            this.slippyTiles = slippyTiles;
            this.gridCell = gridCell;
            this.mediator = mediator;
            this.operationRepository = operationRepository;
			this.userClient = userClient;
            this.anraMqttClient = anraMqttClient;
            this.kafkaProducer = kafkaProducer;
        }

        /// <summary>
        /// Request information regarding specific USS Instance
        /// </summary>
        /// <remarks>Returns USS Instance.</remarks>
        /// <param name="ussInstanceId">USS Instance Id</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-utm")]        
        [Route("/utm/{ussInstanceId}")]
        [SwaggerOperation("GetUSSById"), SwaggerResponse(200, type: typeof(UtmInstanceItem))]
        public virtual IActionResult GetUSSById([FromRoute]Guid ussInstanceId)
        {
            var result = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssInstanceId == ussInstanceId);
            var uss = result.Adapt<UtmInstanceItem>();

            return new ObjectResult(uss);
        }

        /// <summary>
        /// Request information regarding USS Instances
        /// </summary>
        /// <remarks>Returns Active USS Instances.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-utm")]        
        [Route("/utm/listing")]
        [SwaggerOperation("GetUtmInstanceListing"), SwaggerResponse(200, type: typeof(List<UtmInstanceItem>))]
        public virtual IActionResult GetListing()
        { 
            var result = utmInstanceRepository.GetAll()
                .Where(o => o.TimeAvailableEnd.HasValue && o.TimeAvailableEnd >= DateTime.UtcNow)
                .OrderByDescending(x => x.DateCreated).ToList();

            var utmListing = new List<UtmInstanceItem>();
            
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                //List all the USS for SuperAdmin
                utmListing = result.Adapt<List<UtmInstanceItem>>();
            }
            else if(CheckRole(EnumUserRole.ANSP.ToString()))
            {
                //List all the USS under that ANSP
                utmListing = result.Where(x=>x.GroupId.Equals(GetCurrentGroupId())).Adapt<List<UtmInstanceItem>>();
            }
            else if(CheckRole(EnumUserRole.ATC.ToString()))
            {
                //List all the USS assigned for respective ATC operator
                utmListing = result.Where(x => x.UserId.Equals(GetCurrentUserId())).Adapt<List<UtmInstanceItem>>();
            }
            else
            {
                //List all the USS for other admin and pilot user that are signed up under that ANSP 
                utmListing = result.Where(x => x.GroupId.Equals(GetCurrentGroupId())).Adapt<List<UtmInstanceItem>>();
            }

            var newUtmListing = new List<UtmInstanceItem>();

            //Adding only our Internal USS Instance on the Listing. 
            foreach(var item in utmListing)
            {
                if(!string.IsNullOrEmpty(item.UssName))
                {
                    if(item.UssName.Equals(anraConfig.ClientId))
                    {
                        item.IsAnraUssInstance = true;
                        newUtmListing.Add(item);
                    }
                }
            }

            return new ObjectResult(newUtmListing);
        }

        /// <summary>
        /// Submit updates to a previously communicated USS Instance.
        /// </summary>
        /// <param name="ussInstance">Uss Instance</param>
        /// <param name="authorization">todo: describe authorization parameter on UpdateUssInstance</param>
        /// <remarks>Allows to update a previous created USS Instance.</remarks>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Authorize("uss:write-utm")]        
        [Route("/utm")]
        [SwaggerOperation("UpdateUssInstance"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult UpdateUssInstance([FromBody]UtmInstanceItem ussInstance, [FromHeader]string authorization)
        {
            if (!_utmValidator.IsValid(ussInstance,true))
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Join(@", ", _utmValidator.Errors), Id = ussInstance.UssInstanceId.ToString() });
            }

            var modelUss = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssInstanceId.Equals(ussInstance.UssInstanceId));

            if (modelUss != null)
            {
                var userDetails = userClient.GetUserDetails(GetCurrentUserId(), authorization);

                ussInstance.Contact = new PersonOrOrganization
                {
                    Name = $"{userDetails.FirstName} {userDetails.LastName}",
                    EmailAddresses = new List<string> { userDetails.Email },
                    PhoneNumbers = new List<string> { userDetails.PhoneNumber }
                };

                ussInstance.Adapt(modelUss);

                var tilesInfo = slippyTiles.GetSlippyTileInfo(ussInstance.CoverageArea);

                modelUss.GridCells = JsonConvert.SerializeObject(tilesInfo);

                //Check if Uss has active operations in the grid
                //then inform user to close these operations before edit USS
                var operations = gridCell.GetSelfOperations(tilesInfo.ToList());

                if (operations.Any())
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "Can't edit USS Instance as it has active operations in grid.", Id = ussInstance.UssInstanceId.ToString() });
                }

                utmInstanceRepository.Update(modelUss);

                var responses = gridCell.PutGridCellOperator(modelUss);
                RefreshLun(modelUss, responses);

                //Update DSS Service Area
                if (configuration.IsDssEnabled)
                {
                    string token = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                    mediator.Send(new AsyncDssNotification(ussInstance, CRUDActionEnum.UPDATE, token));
                }

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = "USS Instance updated", Id = ussInstance.UssInstanceId.ToString() });
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "USS Instance not found.", Id = ussInstance.UssInstanceId.ToString() });
            }
        }

        /// <summary>
        /// Share USS Instance data
        /// </summary>
        /// <param name="newUssInstance">The USS Instance information being sent</param>
        /// <param name="authorization">todo: describe authorization parameter on CreateUssInstance</param>
        /// <remarks>As per the USS Discovery Service specification, this endpoint is for receiving messages about the USS Network from the USS Discovery Service.  For example, when a new USS Instance is registered, that announcement would come to this endpoint.</remarks>
        /// <response code="201">USS Instance data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-utm")]        
        [Route("/utm")]
        [SwaggerOperation("CreateUssInstance"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CreateUssInstance([FromBody]UtmInstanceItem newUssInstance, [FromHeader]string authorization)
        {
            var organizationId = GetCurrentUserOrganizationId();

			if (!_utmValidator.IsValid(newUssInstance))
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Join(@", ", _utmValidator.Errors), Id = newUssInstance.UssInstanceId.ToString() });
            }

            var userDetails = userClient.GetUserDetails(GetCurrentUserId(), authorization);
            newUssInstance.Contact = new PersonOrOrganization
            {
                Name = $"{userDetails.FirstName} {userDetails.LastName}",
                EmailAddresses = new List<string> { userDetails.Email },
                PhoneNumbers = new List<string> { userDetails.PhoneNumber }
            };

            newUssInstance.UssInstanceId = Guid.NewGuid();
            var model = newUssInstance.Adapt<UtmInstance>();
            model.OrganizationId = organizationId;
            model.GroupId = GetCurrentGroupId();
            model.UserId = GetCurrentUserId();
            model.GridCells = JsonConvert.SerializeObject(slippyTiles.GetSlippyTileInfo(newUssInstance.CoverageArea));

            //Set Client Id as Uss Name
            model.UssName = anraConfig.ClientId;

            utmInstanceRepository.Add(model);

            //Notify USS creatin using Mqtt for AnraMonitoring Services
            NotifyUssCreation(model);

            var responses = gridCell.PutGridCellOperator(model);
            RefreshLun(model, responses);

            //Create DSS Service Area
            if (configuration.IsDssEnabled)
            {
                string token = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                mediator.Send(new AsyncDssNotification(newUssInstance, CRUDActionEnum.INSERT, token));
            }

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = "Uss Instance created.", Id = newUssInstance.UssInstanceId.ToString() });
        }

        private void RefreshLun(UtmInstance model, List<JSendGridCellMetadataResponse> responses)
        {
            var existingOperators = utmInstanceRepository.GetAll().Where(p => p.GridCells == model.GridCells && p.UssInstanceId != model.UssInstanceId).ToList();
            foreach (var item in existingOperators)
            {
                utmInstanceRepository.Remove(item.UssInstanceId);
            }

            List<UtmInstance> externalUsss = new List<UtmInstance>();

            foreach (var response in responses)
            {
                //Remove Self
                response.Data.Operators.RemoveAll(x => x.Uss.Equals(model.UssName));

                var operatorsMetaDataList = response.Data.Operators;
                foreach (var operatorMetaData in operatorsMetaDataList)
                {
                    var instance = model.Adapt<UtmInstance>();
                    instance.UssInstanceId = Guid.NewGuid();
                    instance.UssName = operatorMetaData.Uss;
                    instance.UssBaseCallbackUrl = operatorMetaData.UssBaseurl;
                    instance.TimeAvailableBegin = operatorMetaData.MinimumOperationTimestamp;
                    instance.TimeAvailableEnd = operatorMetaData.MaximumOperationTimestamp;
                    //instance.Id = 0;
                    instance.UssInformationalUrl = string.Empty;
                    instance.UssOpenapiUrl = string.Empty;
                    instance.UssRegistrationUrl = string.Empty;
                    instance.OrganizationId = null;
                    instance.Notes = null;
                    instance.Contact = null;
                    //Add Only Unique USS Operators
                    if (!externalUsss.Exists(x => x.UssName.Equals(instance.UssName)))
                    {
                        externalUsss.Add(instance);
                        utmInstanceRepository.Add(instance);
                    }
                }

            }
        }

        /// <summary>
        /// Request information regarding Current Local Uss Network
        /// </summary>
        /// <param name="ussInstanceId">UssInstanceId</param>
        /// <remarks>Provides current local uss network</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-utm")]        
        [Route("/uss/localnetwork/{ussInstanceId}")]
        public virtual List<UssInstance> GetLocalNetwork(Guid ussInstanceId)
        {
            return dbFunctions.GetLocalUssNetworkByUtmInstanceIdInclusive(ussInstanceId).Adapt<List<Utm.Models.UssInstance>>();
        }

        /// <summary>
        /// Resync Operations While USS Service is down
        /// </summary>
        /// <param name="ussInstanceId">UssInstanceId</param>
        /// <remarks>Provides current local uss network</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-utm")]        
        [Route("/uss/resync/{ussInstanceId}")]
        [SwaggerOperation("ResyncOperators"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult ResyncOperators(Guid ussInstanceId)
        {
            //Fetch Uss Instance Detail of passed uss instance id
            var model = utmInstanceRepository.Find(ussInstanceId);

            var existingOperators = utmInstanceRepository.GetAll().Where(p => p.GridCells == model.GridCells && p.UssInstanceId != model.UssInstanceId).ToList();
            foreach (var item in existingOperators)
            {
                utmInstanceRepository.Remove(item.UssInstanceId);
            }

            var gridCellOperators = gridCell.GetGridCellOperators(model.GridCells,false);

            
            bool isAnraUssExistsInAllGrids = true;

            var anraOperator = gridCellOperators.Where(x => x.Uss.Equals(model.UssName)).ToList();
            var grids = JsonConvert.DeserializeObject<List<JSendSlippyResponseDataGridCells>>(model.GridCells);
            List<bool> isAnraOperatorInGrid = new List<bool>();

            if (grids.Any())
            {
                grids.ForEach(grid => {

                    bool flag = anraOperator.Any(opt => opt.Zoom.Equals(grid.Zoom) && opt.X.Equals(grid.X) && opt.Y.Equals(grid.Y));
                    isAnraOperatorInGrid.Add(flag);
                });

                //Check If ANRA Uss Operator is in all the grids acquire by its coverage area
                isAnraUssExistsInAllGrids = !isAnraOperatorInGrid.Any(x => x.Equals(false));
            }

            if (isAnraUssExistsInAllGrids)
            {
                //Remove Self
                gridCellOperators.RemoveAll(x => x.Uss.Equals(model.UssName));

                List<UtmInstance> externalUsss = new List<UtmInstance>();

                foreach (var operatorMetaData in gridCellOperators)
                {
                    var instance = model.Adapt<UtmInstance>();
                    instance.UssInstanceId = Guid.NewGuid();
                    instance.UssName = operatorMetaData.Uss;
                    instance.UssBaseCallbackUrl = operatorMetaData.UssBaseurl;
                    instance.TimeAvailableBegin = operatorMetaData.MinimumOperationTimestamp;
                    instance.TimeAvailableEnd = operatorMetaData.MaximumOperationTimestamp;
                    //instance.Id = 0;
                    instance.UssInformationalUrl = string.Empty;
                    instance.UssOpenapiUrl = string.Empty;
                    instance.UssRegistrationUrl = string.Empty;
                    instance.OrganizationId = null;
                    instance.Notes = null;
                    instance.Contact = null;
                    //Add Only Unique USS Operators
                    if (!externalUsss.Exists(x => x.UssName.Equals(instance.UssName)))
                    {
                        externalUsss.Add(instance);
                        utmInstanceRepository.Add(instance);
                    }
                }

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = "USS Synced", Id = ussInstanceId.ToString() });
            }
            else
            {
                //Remove any existing instance from Grid
                gridCell.DeleteGridCellOperator(model);

                //Remove USS from DB
                //utmInstanceRepository.Remove(model.Id);
                utmInstanceRepository.Remove(model.UssInstanceId);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "USS operator not exists.Please create new USS." });
            }
        }

        [HttpDelete]
        [Authorize("uss:write-utm")]        
        [Route("/utm/{ussInstanceId}")]
        [SwaggerOperation("DeleteUSS"), SwaggerResponse(200, type: typeof(Utm.Models.UssInstance))]
        public virtual IActionResult DeleteUSS([FromRoute]Guid ussInstanceId)
        {
            var anyActiveOperations = operationRepository.GetAll().Any(p => p.UssInstanceId == ussInstanceId && p.IsInternalOperation && p.State != OperationState.CLOSED.ToString());
            if (!anyActiveOperations)
            {
                var utmInstance = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssInstanceId == ussInstanceId);

                gridCell.DeleteGridCellOperator(utmInstance);

                utmInstanceRepository.Remove(utmInstance.UssInstanceId);

                //Delete DSS Service Area
                if (configuration.IsDssEnabled)
                {
                    string token = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                    mediator.Send(new AsyncDssNotification(utmInstance.Adapt<UtmInstanceItem>(), CRUDActionEnum.DELETE, token));
                }

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "USS deleted successfully.", Id = ussInstanceId.ToString() });
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Error! open operation exists in the USS.", Id = ussInstanceId.ToString() });
            }
        }

        private void SaveExternalOperation(UssInstance uss, List<Utm.Models.Operation> operations)
        {
            //TODO: needs refactoring 17aug,2018
            /*
            if (operations.Any())
            {
                foreach (var operation in operations)
                {
                    var response = ussApi.PostOperations(uss, operation);
                }
            }*/
        }

        /// <summary>
        /// Notify USS creation to AnraMonitoring Services using Mqtt.
        /// </summary>
        /// <param name="uss"></param>
        private void NotifyUssCreation(UtmInstance uss)
        {
            kafkaProducer.Produce(uss.UssInstanceId.ToString(), KafkaTopics.NotifyUssInstanceCreated).ConfigureAwait(false);

            //anraMqttClient.PublishTopic(MqttTopics.NotifyUssInstanceCreated, uss.UssInstanceId);
        }
    }
}