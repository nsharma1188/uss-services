using AnraUssServices.BackgroundJobs.DetectSubscriptions;
using AnraUssServices.Common;
using AnraUssServices.Common.Kafka;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Data;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Models.DetectSubscribers;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel;
using AnraUssServices.ViewModel.Detects;
using FluentScheduler;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NpgsqlTypes;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Linq;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Drone Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DetectionController : BaseController
    {
        private readonly OrganizationClient organazationClient;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ApplicationDbContext _context;
        private AnraConfiguration configuration;

        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;
        private readonly AnraMqttClient anraMqttClient;

        private readonly DetectSubscribersDocument detectSubscribersDocument;
        private readonly IRepository<Models.Operation> operationsRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<DetectSubscriber> detectSubscriberRepository;
        private readonly RelmatechApi relmatechApi;
        private readonly KafkaProducer kafkaProducer;

        public DetectionController(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            AnraConfiguration configuration,
            DatabaseFunctions dbFunctions,
            AnraMqttClient anraMqttClient,
            DetectSubscribersDocument detectSubscribersDocument,
            IRepository<Models.Operation> operationsRepository,
            IRepository<UtmInstance> utmInstanceRepository,
            IRepository<DetectSubscriber> detectSubscriberRepository,
            ILogger<string> logger,
            RelmatechApi relmatechApi,
			OrganizationClient organazationClient,
            KafkaProducer kafkaProducer)
        {
            _context = context;
            this.logger = logger;
            this.anraMqttClient = anraMqttClient;
            this.configuration = configuration;
            this.detectSubscribersDocument = detectSubscribersDocument;
            this.operationsRepository = operationsRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.detectSubscriberRepository = detectSubscriberRepository;
            this.httpContextAccessor = httpContextAccessor;
            this.dbFunctions = dbFunctions;
            this.organazationClient = organazationClient;
            this.relmatechApi = relmatechApi;
            this.kafkaProducer = kafkaProducer;
        }

        /// <summary>
        /// Subscribe detects
        /// </summary>
        /// <param name="gufi">operation gufi</param>
        /// <param name="types">detect types</param>
        /// <remarks>A POST to subscribe to detects</remarks>
        /// <response code="200">subscription requestion received.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-detection")]
        [Route(@"/detection/operation/{gufi}/subscribe")]
        [SwaggerOperation("Subscribe"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult Subscribe([FromRoute]Guid gufi, [FromBody] DetectSourceEnum[] types, [FromHeader] string authorization)
        {
            var operation = operationsRepository.Find(gufi);
            operation.ThrowIfNull("Invalid Operation");

            if (operation.State == OperationState.CLOSED.ToString() || operation.State == OperationState.PROPOSED.ToString())
            {
                #if DEBUG
                    logger.LogError("ANRA:: ADSB detect Bad Request");
                #endif
                return ApiResponse(StatusCodes.Status400BadRequest, "Invalid Operation");
            }

            var userId = GetCurrentUserId();

            var ussCoverageArea = utmInstanceRepository.Find(operation.UssInstanceId).CoverageArea;
            var model = detectSubscriberRepository.GetAll().FirstOrDefault(p => p.Gufi == gufi && p.UserId == Guid.Parse(userId)); 

            AdsbInput adsbIp = new AdsbInput();
            var ip = GetMinMaxLatLong(adsbIp, ussCoverageArea);
            ip.UserId = Guid.Parse(userId);
            ip.USSInstanceId = operation.UssInstanceId;

            var detectSubsciberRepoInfo = detectSubscriberRepository.GetAll();
            foreach (var info in detectSubsciberRepoInfo) { var x = info.SubscribedTypes; }
            var foundUssInstance = detectSubscriberRepository.GetAll().Select(p => p.UssInstanceId.Equals(operation.UssInstanceId) && p.SubscribedTypes.Contains(DetectSourceEnum.ADSB.ToString()));
            var count = foundUssInstance.ToList().Count;
            int num = 0;
            foreach (bool found in foundUssInstance) { if (found == true) num++; }

            var subscriptionTypes = types.Adapt<string[]>();

            //Notifying if there is a subscription for collision
            var subscription = new SubscriptionViewModel()
            {
                Gufi = gufi.ToString(),
                SubscribedTypes = subscriptionTypes,
                UserId = userId,
                UssInstanceId = operation.UssInstanceId.ToString()
            };

            if(types.Contains(DetectSourceEnum.COLLISION))
            {
                subscription.IsCollisionSubscribed = true;
            }
            else
            {
                subscription.IsCollisionSubscribed = false;
            }

            if (types.Contains(DetectSourceEnum.RADAR))
            {
                subscription.IsRadarSubscribed = true;
            }
            else
            {
                subscription.IsRadarSubscribed = false;
            }

            kafkaProducer.Produce(subscription, KafkaTopics.CheckSubscription).ConfigureAwait(false);
            //anraMqttClient.PublishTopic(MqttTopics.CheckSubscription, subscription);

            if (model == null)
            {
                model = new DetectSubscriber
                {
                    SubscriptionId = Guid.NewGuid(),
                    Gufi = operation.Gufi,
                    UssInstanceId = operation.UssInstanceId,
                    UserId = Guid.Parse(userId),
                    SubscribedTypes = subscriptionTypes,
                    IsActive = true,
                    OperationId = gufi
                };

                if (types.ToList().Contains(DetectSourceEnum.ADSB))
                {
                    var adsbSrc = GetAdsbSourceConfiguration(authorization);//gets from security DB 
                    if (string.IsNullOrEmpty(adsbSrc))
                    {
                        #if DEBUG
                            logger.LogError("Subscription not processed! ADSB source not configured.");
                        #endif
                        return ApiErrorResponse("Subscription not processed! ADSB source not configured.");
                    }

                    model.StartTime = DateTime.UtcNow;
                    model.EndTime = DateTime.UtcNow.AddMinutes(configuration.AdsbSubscriptionDuration);
                    model.AdsbSource = adsbSrc;
                    model.AdsbQuery = new AdsbCriteria(ussCoverageArea.ToNTSCoordinates()).ToQuery();
                   
                    if (configuration.IsOpenSkyEnabled && (foundUssInstance == null || num < 1) && adsbSrc.Equals("OpenSky"))
                    {
                        JobManager.AddJob(() => new AdsbJob(serviceProvider, logger, anraMqttClient, configuration, ip).Execute(), (s) => s.WithName(model.UssInstanceId.ToString()).ToRunEvery(configuration.OpenskyApiCallInterval).Seconds());
                        #if DEBUG
                            logger.LogInformation("ANRA::: Adsb Job created :: USS Instance: {0}", model.UssInstanceId);
                        #endif
                    }
                }
                detectSubscriberRepository.Add(model);
                UpdateNosqlDatabase(ussCoverageArea, model);
            }
            else
            {
                //If not subscribed to ADSB
                if (!model.SubscribedTypes.Contains(DetectSourceEnum.ADSB.ToString()) && types.ToList().Contains(DetectSourceEnum.ADSB))
                {
                    var adsbSrc = GetAdsbSourceConfiguration(authorization);
                    if (string.IsNullOrEmpty(adsbSrc))
                    {
                        #if DEBUG
                            logger.LogError("Subscription not processed! ADSB source not configured.");
                        #endif
                        return ApiErrorResponse("Subscription not processed! ADSB source not configured.");
                    }

                    model.StartTime = DateTime.UtcNow;
                    model.EndTime = DateTime.UtcNow.AddMinutes(configuration.AdsbSubscriptionDuration);
                    model.AdsbSource = adsbSrc;
                    model.AdsbQuery = new AdsbCriteria(ussCoverageArea.ToNTSCoordinates()).ToQuery();
                    var scheduleName = JobManager.RunningSchedules.ToAsyncEnumerable().ToList();

                    if (configuration.IsOpenSkyEnabled && (foundUssInstance == null || num < 1) && adsbSrc.Equals("OpenSky"))
                    {                       
                        JobManager.AddJob(() => new AdsbJob(serviceProvider, logger, anraMqttClient, configuration, ip).Execute(), (s) => s.WithName(model.UssInstanceId.ToString()).ToRunEvery(configuration.OpenskyApiCallInterval).Seconds());
                        #if DEBUG
                            logger.LogInformation("ANRA::: Adsb Job created");
                        #endif
                    }
                }
                else if (model.SubscribedTypes.Contains(DetectSourceEnum.ADSB.ToString()) && !types.ToList().Contains(DetectSourceEnum.ADSB))
                {
                    if (num == 1)
                    {
                        JobManager.RemoveJob(operation.UssInstanceId.ToString());
                        #if DEBUG
                            logger.LogInformation("ANRA::: Adsb Job removed on unsubscription");
                        #endif
                    }
                }

                model.SubscribedTypes = subscriptionTypes;
                detectSubscriberRepository.Update(model);

                UpdateNosqlDatabase(ussCoverageArea, model);
            }

            return ApiResponse(StatusCodes.Status200OK);
        }
        private AdsbInput GetMinMaxLatLong(AdsbInput adsbIp, NpgsqlPolygon ussCoverageArea)
        {
            var list = ussCoverageArea.ToList();
            double lamin, lamax, lngmin, lngmax;
            lamin = list.ElementAt(0).Y;
            lngmin = list.ElementAt(0).X;
            lamax = list.ElementAt(0).Y;
            lngmax = list.ElementAt(0).X;

            for (int i = 1; i < list.Count; i++)
            {
                if (lamin > list.ElementAt(i).Y) { lamin = list.ElementAt(i).Y; }
                if (lngmin > list.ElementAt(i).X) { lngmin = list.ElementAt(i).X; }
                if (lamax < list.ElementAt(i).Y) { lamax = list.ElementAt(i).Y; }
                if (lngmax < list.ElementAt(i).X) { lngmax = list.ElementAt(i).X; }
            }
            adsbIp.lngmin = lngmin;
            adsbIp.lngmax = lngmax;
            adsbIp.latmin = lamin;
            adsbIp.latmax = lamax;
            return adsbIp;
        }

        [HttpGet]
        [Authorize("uss:read-detection")]
        [Route(@"/detection/operation/{gufi}")]
        [SwaggerOperation("GetSubscription"), SwaggerResponse(200, type: typeof(string[]))]
        public virtual IActionResult GetSubscription([FromRoute]Guid gufi)
        {
            var userId = GetCurrentUserId();
            var model = detectSubscriberRepository.GetAll().FirstOrDefault(p => p.Gufi == gufi && p.UserId == Guid.Parse(userId));

            var retval = Array.Empty<string>();
            if (model != null && model.SubscribedTypes != null)
            {
                retval = model.SubscribedTypes.Adapt<string[]>();
            }

            return new ObjectResult(retval.ToArray());
        }

        private string GetAdsbSourceConfiguration(string authorization)
        {
            var organizationId = GetCurrentUserOrganizationId();
            return organazationClient.GetOrganizationAdsbSource(organizationId, authorization);
        }

        private void UpdateNosqlDatabase(NpgsqlPolygon ussCoverageArea, DetectSubscriber model)
        {
            var eDoc = model.Adapt<DetectSubscriberViewModel>();
            eDoc.UssCoverageArea = ussCoverageArea.ToPolygon();
            detectSubscribersDocument.AddUpdateSubscription(eDoc);
        }

        [HttpPost]
        [Authorize("uss:read-detection")]
        [Route(@"/detection/relmatech/login")]
        [SwaggerOperation("DoLogin"), SwaggerResponse(200, type: typeof(RelmatechLoginResponse))]
        public virtual IActionResult DoLogin()
        {
            var response = this.relmatechApi.DoLogin();
            return new ObjectResult(response);
        }
    }
}