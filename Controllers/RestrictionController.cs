﻿using AnraUssServices.Common;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.ConstraintMessages;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// DR - Restriction Controller
    /// </summary>
    [Route(@"api/[controller]")]
    public class RestrictionController : BaseController
    {
        private readonly IMediator mediator;
        private readonly ILogger<RestrictionController> logger;

        private readonly UtmMessenger utmMessenger;
        private readonly IRepository<ConstraintMessage> constraintMessageRepository;

        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private AnraConfiguration _config;
        private readonly GridCell gridCell;
        private UvrValidator _uvrValidator;
        private readonly RestrictionApi restrictionApi;
        private readonly ViewerMqttNotification viewerMqttNotification;

        private readonly AnraMqttClient anraMqttClient; //For showing delete message

        public RestrictionController(ApplicationDbContext context,
            ILogger<RestrictionController> logger,
            UtmMessenger utmMessenger,
            IRepository<ConstraintMessage> constraintMessageRepository,
            IRepository<UtmInstance> utmInstanceRepository,
            AnraConfiguration configuration,
            AnraMqttClient anraMqttClient,
            UvrValidator uvrValidator,
            GridCell gridCell,
            RestrictionApi restrictionApi, ViewerMqttNotification viewerMqttNotification
        )

        {
            this.constraintMessageRepository = constraintMessageRepository;
            this.logger = logger;
            this.utmMessenger = utmMessenger;
            this.utmInstanceRepository = utmInstanceRepository;
            _config = configuration;
            this.gridCell = gridCell;
            _uvrValidator = uvrValidator;
            this.restrictionApi = restrictionApi;
            this.anraMqttClient = anraMqttClient;
            this.viewerMqttNotification = viewerMqttNotification;

        }

        /// <summary>
        /// Request information regarding Constraints
        /// </summary>
        /// <remarks>Allows querying for Constraints data.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-uvr")]
        [Route(@"/restriction/listing/{showExpired?}")]
        [SwaggerOperation("GetConstraints"), SwaggerResponse(200, type: typeof(List<ConstraintMessageViewModel>))]
        public virtual IActionResult GetConstraints([FromRoute] bool showExpired = true)
        {
            var restrictionList = new List<ConstraintMessageViewModel>();

            try
            {
                var result = showExpired ?
                    constraintMessageRepository.GetAll().OrderByDescending(x => x.DateModified).ToList()
                    :
                    constraintMessageRepository.GetAll().Where(x => x.IsExpired.Equals(showExpired)).OrderByDescending(x => x.DateModified).ToList();

                if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
                {
                    restrictionList = result.Adapt<List<ConstraintMessageViewModel>>();
                    return new ObjectResult(restrictionList);
                }
                else
                {
                    restrictionList = result.Where(x => x.GroupId == GetCurrentGroupId()).Adapt<List<ConstraintMessageViewModel>>();
                    return new ObjectResult(restrictionList);
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(restrictionList);
            }
        }

        /// <summary>
        /// Request information regarding Constraints
        /// </summary>        
        /// <remarks>Allows querying for Constraints data.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-uvr")]
        [Route(@"/restriction/{message_id}")]
        [SwaggerOperation("GetConstraint"), SwaggerResponse(200, type: typeof(ConstraintMessageViewModel))]
        public virtual IActionResult GetConstraint([FromRoute] Guid message_id)
        {
            var result = constraintMessageRepository.Find(message_id)
                                                    .Adapt<ConstraintMessageViewModel>();

            return new ObjectResult(result);
        }

        [HttpGet]
        [Authorize("uss:read-uvr")]
        [Route("/restriction/{messageId}")]
        [SwaggerOperation("GetRestrictionById"), SwaggerResponse(200, type: typeof(ConstraintMessageViewModel))]
        public virtual IActionResult GetRestrictionById([FromRoute]Guid messageId)
        {
            var result = constraintMessageRepository.GetAll().FirstOrDefault(p => p.MessageId == messageId);
            var item = result.Adapt<ConstraintMessageViewModel>();
            return new ObjectResult(item);
        }

        [HttpPost, ApiValidationFilter]
        [Route(@"/restriction")]
        [Authorize("uss:write-uvr")]
        [SwaggerOperation(nameof(PostRestriction)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostRestriction([FromBody]ConstraintMessageViewModel restriction_message)
        {
            try
            {
                restriction_message.MessageId = Guid.NewGuid();
                var _ussInstance = utmInstanceRepository.GetAll().Where(p => p.UssInstanceId.ToString() == restriction_message.UssInstanceId).FirstOrDefault();
                if (!_uvrValidator.IsValid(restriction_message.Adapt<UASVolumeReservation>(), true, _ussInstance, message_id: restriction_message.MessageId))
                {
                    return ApiErrorResponse(string.Join(", ", _uvrValidator.Errors));
                }

                //Write UVR to Grid
                var response = gridCell.WriteUVR(_config.DefaultZoomLevel, restriction_message.Adapt<UASVolumeReservation>());
                if (response.HttpStatusCode.Value != (int)HttpStatusCode.OK)
                {
                    #if DEBUG
                    logger.LogInformation("RestrictionController:: WriteUVR Failed: Message Id = {0}, Response:{1}", restriction_message.MessageId, response);
                    #endif

                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Status, Id = restriction_message.MessageId.ToString() });
                }

                //If UVR Written in Grid Then Save to DB
                var model = restriction_message.Adapt<ConstraintMessage>();

                model.IsRestriction = false;
                model.GroupId = GetCurrentGroupId();
                constraintMessageRepository.Add(model);

                //Submit UVR to FAA FIMS (Implementation for UPP) in case when SendUVRToFIMSEnable is true
                if (_config.SendUVRToFIMSEnable)
                {
                    restrictionApi.PutUVR(restriction_message.Adapt<UASVolumeReservation>());
                }

                //Notify to Lun
                utmMessenger.NotifyUVRToLUN(model.Adapt<UASVolumeReservation>(), MqttMessageType.CREATED, response.Data.Operators);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = "UVR Created", Id = restriction_message.MessageId.ToString() });
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = ex.Message });
            }
        }

        [HttpPut, ApiValidationFilter]
        [Route(@"/restriction")]
        [Authorize("uss:write-uvr")]
        [SwaggerOperation(nameof(PutRestriction)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PutRestriction([FromBody]ConstraintMessageViewModel restriction_message)
        {
            try
            {
                var _ussInstance = utmInstanceRepository.GetAll().Where(p => p.UssInstanceId.ToString() == restriction_message.UssInstanceId).FirstOrDefault();
                //Validate UVR inputs
                if (!_uvrValidator.IsValid(restriction_message.Adapt<UASVolumeReservation>(), true, _ussInstance, message_id: restriction_message.MessageId))
                {
                    return ApiErrorResponse(string.Join(", ", _uvrValidator.Errors));
                }

                var existingUvr = constraintMessageRepository.Find(restriction_message.MessageId);

                if (existingUvr != null)
                {
                    //First Delete existing UVR from Grid
                    existingUvr.ActualTimeEnd = null;// Added this field beacuse we are not able to delete the active UVR from the grid

                    //Delete UVR from Grid
                    var response = gridCell.DeleteUVR(_config.DefaultZoomLevel, existingUvr.Adapt<UASVolumeReservation>());

                    if (response.HttpStatusCode.Value == (int)HttpStatusCode.OK)
                    {
                        var uvrModel = restriction_message.Adapt<UASVolumeReservation>();
                        uvrModel.ActualTimeEnd = null;

                        //Write UVR to Grid
                        response = gridCell.WriteUVR(_config.DefaultZoomLevel, uvrModel);

                        if (response.HttpStatusCode.Value != (int)HttpStatusCode.OK)
                        {
#if DEBUG
                            logger.LogInformation("RestrictionController:: WriteUVR Failed: Message Id = {0}, Response:{1}", restriction_message.MessageId, response);
#endif

                            return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Status, Id = restriction_message.MessageId.ToString() });
                        }

                        //If UVR Written in Grid Then Update to DB
                        restriction_message.Adapt(existingUvr);
                        constraintMessageRepository.Update(existingUvr);

                        //Submit updated UVR to FAA FIMS (Implementation for UPP) in case when SendUVRToFIMSEnable is true
                        if (_config.SendUVRToFIMSEnable)
                        {
                            restrictionApi.PutUVR(restriction_message.Adapt<UASVolumeReservation>());
                        }

                        //Notify to Lun
                        utmMessenger.NotifyUVRToLUN(restriction_message.Adapt<UASVolumeReservation>(), MqttMessageType.UPDATED, response.Data.Operators);

                        return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = "UVR Updated", Id = restriction_message.MessageId.ToString() });
                    }
                    else
                    {
#if DEBUG
                        logger.LogInformation("RestrictionController:: DeleteUVR Failed: Message Id = {0}, Response:{1}", restriction_message.MessageId, response);
#endif

                        return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Status, Id = restriction_message.MessageId.ToString() });
                    }
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Uvr not found." });
                }

            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = ex.Message });
            }
        }

        /// <summary>
        /// Delete a UVR
        /// </summary>
        [HttpDelete]
        [Authorize("uss:write-uvr")]
        [Route(@"/restriction/{message_id}")]
        [SwaggerOperation("DeleteUVR"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult DeleteUVR([FromRoute] Guid message_id)

        {
            var restriction_message = constraintMessageRepository.Find(message_id);
            restriction_message.ActualTimeEnd = null;// Added this field beacuse we are not able to delete the active UVR from the grid

            //Delete UVR from Grid
            var response = gridCell.DeleteUVR(_config.DefaultZoomLevel, restriction_message.Adapt<UASVolumeReservation>());

            //Changing the UVR start and end time to the past to delete the uvr.
            restriction_message.DateModified = DateTime.UtcNow;
            restriction_message.EffectiveTimeBegin = DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(2));
            restriction_message.EffectiveTimeEnd = DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(1));


            //Submit UVR to FAA FIMS (Implementation for UPP) in case when SendUVRToFIMSEnable is true
            if (_config.SendUVRToFIMSEnable)
            {
                restrictionApi.PutUVR(restriction_message.Adapt<UASVolumeReservation>());
            }

            //Notify to Lun
            utmMessenger.NotifyUVRToLUN(restriction_message.Adapt<UASVolumeReservation>(), MqttMessageType.CLOSED, response.Data.Operators);

            //Notify UVR Delete
            //Need to refractor this
            anraMqttClient.PublishTopic(MqttTopics.UvrDelete, message_id.ToString());


            if (response.HttpStatusCode.Value != (int)HttpStatusCode.OK)
            {
#if DEBUG
                logger.LogInformation("RestrictionController:: DeleteUVR Failed: Message Id = {0}, Response:{1}", restriction_message.MessageId, response);
#endif

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Status, Id = restriction_message.MessageId.ToString() });
            }

            //Remove UVR From DB
            //Set it expire
            restriction_message.IsExpired = true;
            constraintMessageRepository.Remove(restriction_message.MessageId);

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"UVR Deleted.", Id = restriction_message.MessageId.ToString() });
        }
    }
}