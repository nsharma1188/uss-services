﻿using AnraUssServices.Common;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Models;
using AnraUssServices.Models.Signup;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Roles;
using AnraUssServices.ViewModel.Users;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    [Route(@"api/[controller]")]
    public class UserController : BaseController
    {
        private readonly UserClient userClient;
        private readonly OrganizationClient organizationClient;
        private readonly IRepository<Signup> signupRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly GridCell gridCell;
        private readonly ILogger<UserController> logger;

        public UserController(UserClient userClient, OrganizationClient organizationClient,
            IRepository<Signup> signupRepository, IRepository<UtmInstance> utmInstanceRepository,
           IRepository<Models.Operation> operationRepository, GridCell gridCell, ILogger<UserController> logger)
        {
            this.userClient = userClient;
            this.organizationClient = organizationClient;
            this.signupRepository = signupRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.operationRepository = operationRepository;
            this.gridCell = gridCell;
            this.logger = logger;
        }

        /// <summary>
        /// Returns new user model 
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/user/new")]
        [SwaggerOperation(nameof(GetNewUserItem)), SwaggerResponse(200, type: typeof(UserItem))]
        public virtual IActionResult GetNewUserItem([FromHeader] string authorization)
        {
            var currentUserRoles = GetCurrentUserRole();

            var requiredRoles = new List<string>();
            requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
            requiredRoles.Add(EnumUserRole.ANSP.ToString());
            requiredRoles.Add(EnumUserRole.ADMIN.ToString());
            var IsRolesAuthorized = CheckRoles(requiredRoles);

            //Checks for the current user role
            if (!IsRolesAuthorized)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
            }
            else
            {
                var user = new UserItem();
                user.Roles = GetRolesList(currentUserRoles);

                if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
                {
                    //ANSP User Creation
                    return new ObjectResult(user);
                }
                else if (CheckRole(EnumUserRole.ANSP.ToString()))
                {
                    //ATC user creation
                    var anspUser = userClient.GetUsers(authorization, GetCurrentGroupId()).FirstOrDefault();
                    var anspUsersList = new List<OrganizationItem>();
                    anspUsersList.Add(new OrganizationItem
                    {
                        ContactEmail = anspUser.Email,
                        GroupId = anspUser.GroupId,
                        FirstName = anspUser.FirstName,
                        LastName = anspUser.LastName
                    });
                    user.Organizations = anspUsersList;

                    return new ObjectResult(user);
                }
                else
                {
                    user.Organizations = GetOrginazationList(authorization);
                    return new ObjectResult(user);
                }
            }
        }

        /// <summary>
        /// Returns the list of users.
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/user/getUserListing")]
        [SwaggerOperation(nameof(GetUserList)), SwaggerResponse(200, type: typeof(List<UserItem>))]
        public virtual IActionResult GetUserList([FromHeader] string authorization)
        {
            var currentUserRoles = GetCurrentUserRole();

            var requiredRoles = new List<string>();
            requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
            requiredRoles.Add(EnumUserRole.ANSP.ToString());
            requiredRoles.Add(EnumUserRole.ADMIN.ToString());
            var IsRolesAuthorized = CheckRoles(requiredRoles);

            //UserListing is only accessible  by Superadmin, Administrator and ANSP
            if (!IsRolesAuthorized)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
            }
            else
            {
                var test = CheckRole(EnumUserRole.SUPERADMIN.ToString());
                if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
                {
                    var newUserList = new List<UserItem>();

                    userClient.GetUsers(authorization).ToList().ForEach(user =>
                        {
                            user.Roles.ForEach(x =>
                            {
                                if (x.Name == EnumUserRole.ANSP.ToString())
                                {
                                    newUserList.Add(user);
                                }
                            });
                        });

                    return new ObjectResult(newUserList);
                }
                else if (CheckRole(EnumUserRole.ANSP.ToString()))
                {
                    var groupId = GetCurrentGroupId();
                    var newUserList = userClient.GetUsers(authorization, groupId);
                    var userList = newUserList.Where(x => x.UserId != GetCurrentUserId()).ToList();

                    return new ObjectResult(userList);
                }
                else
                {
                    var organizationId = GetCurrentUserOrganizationId();
                    var newUserList = userClient.GetUsers(authorization, null, organizationId.ToString());
                    var userList = newUserList.Where(x => x.UserId != GetCurrentUserId()).ToList();
                    return new ObjectResult(userList);
                }
            }
        }

        /// <summary>
        /// Returns the user details based on the userId.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/user/getUser/{userId}")]
        [SwaggerOperation(nameof(GetUser)), SwaggerResponse(200, type: typeof(UserItem))]
        public virtual IActionResult GetUser(string userId, [FromHeader] string authorization)
        {
            var currentUserRoles = GetCurrentUserRole();

            var user = userClient.GetUserDetails(userId, authorization);
            var possibleRoles = GetRolesList(currentUserRoles);
            var newPossibleRoles = new List<RoleItem>();

            if (!user.Roles.Exists(x => x.Name == EnumUserRole.ANSP.ToString() ||
                 x.Name == EnumUserRole.ATC.ToString() || x.Name == EnumUserRole.SUPERADMIN.ToString()))
            {

                //Checking if the current user role have the possible role already assigned
                foreach (var role in possibleRoles)
                {
                    if (user.Roles.Exists(x => x.Id == role.Id))
                    {
                        newPossibleRoles.Add(role);
                    }
                }
            }

            user.Roles.AddRange(newPossibleRoles);

            return new ObjectResult(user);
        }

        /// <summary>
        /// Returns the current logged in user details.
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [Route(@"/user/getCurrentUser")]
        [SwaggerOperation(nameof(GetCurrentUser)), SwaggerResponse(200, type: typeof(UserItem))]
        public virtual IActionResult GetCurrentUser([FromHeader] string authorization)
        {
            var userId = GetCurrentUserId();
            var user = userClient.GetUserDetails(userId, authorization);

            return new ObjectResult(user);
        }

        /// <summary>
		/// Creates a new User
		/// </summary>
		/// <param name="user"></param>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpPost]
        [Authorize]
        [Route(@"/user/createUser")]
        [SwaggerOperation(nameof(CreateUser)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CreateUser([FromBody] UserItem user, [FromHeader] string authorization)
        {
            UTMRestResponse response;
            var id = Guid.NewGuid().ToString();

            //Create a new User Model
            var newUser = user.Adapt<UserModel>();
            newUser.UserId = id;
            newUser.UserName = user.Email;

            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                //Creating the ANSP user
                newUser.GroupId = id;
                newUser.OrganizationId = organizationClient.GetANSPId(authorization);

                var role = new List<int>
                {
                    RoleItem.GetRoleDescription().Where(x => x.Name == EnumUserRole.ANSP.ToString()).ToList().FirstOrDefault().Id
                };

                newUser.UserRole = role;
            }
            else if (CheckRole(EnumUserRole.ANSP.ToString()))
            {
                //Creating the ATC user                
                newUser.OrganizationId = organizationClient.GetATCId(authorization);
                newUser.GroupId = GetCurrentGroupId();

                var role = new List<int>
                {
                    RoleItem.GetRoleDescription().Where(x => x.Name == EnumUserRole.ATC.ToString()).ToList().FirstOrDefault().Id
                };

                newUser.UserRole = role;
            }
            else if (CheckRole(EnumUserRole.ADMIN.ToString()))
            {
                newUser.GroupId = GetCurrentGroupId();

                //user of specific organization can be created by Administrator

                newUser.OrganizationId = user.OrganizationId;

                //Set the user roles based on the user selection while creating user
                newUser.UserRole = SetUserRole(user.Roles);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
            }

            response = userClient.CreateUser(newUser, authorization);

            return new ObjectResult(response);
        }

        /// <summary>
        /// Edit the existing user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        [Route(@"/user/updateUser")]
        [SwaggerOperation(nameof(UpdateUser)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult UpdateUser([FromBody] UserItem user, [FromHeader] string authorization)
        {
            var currentUserRoles = GetCurrentUserRole();

            var requiredRoles = new List<string>();
            requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
            requiredRoles.Add(EnumUserRole.ANSP.ToString());
            requiredRoles.Add(EnumUserRole.ADMIN.ToString());
            var IsRolesAuthorized = CheckRoles(requiredRoles);

            //User can be updated by ANSP, Administrator or SuperAdmin
            if (!IsRolesAuthorized)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
            }
            else
            {
                var existingUser = userClient.GetUserDetails(user.UserId, authorization);

                var newUpdatedUser = user.Adapt<UserModel>();
                newUpdatedUser.UserId = existingUser.UserId;
                newUpdatedUser.OrganizationId = existingUser.OrganizationId;
                newUpdatedUser.GroupId = existingUser.GroupId;
                newUpdatedUser.UserRole = SetUserRole(user.Roles);

                var response = userClient.UpdateUser(newUpdatedUser, authorization, existingUser.UserId);

                return new ObjectResult(response);

            }
        }


        /// <summary>
        /// Rove the existing user based on the userId
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        [Route(@"/user/delete/{userId}")]
        [SwaggerOperation(nameof(DeleteUser)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult DeleteUser([FromHeader] string authorization, string userId)
        {
            var response = new UTMRestResponse();

            try
            {
                var currentUserRoles = GetCurrentUserRole();

                var requiredRoles = new List<string>();
                requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
                requiredRoles.Add(EnumUserRole.ANSP.ToString());
                requiredRoles.Add(EnumUserRole.ADMIN.ToString());
                var IsRolesAuthorized = CheckRoles(requiredRoles);

                //User can be updated by ANSP and Administrator of that organization
                if (!IsRolesAuthorized)
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
                }
                else
                {
                    //Get the existing user we are going to remove
                    var existingUser = userClient.GetUserDetails(userId, authorization);

                    //Check if the user is admin or not
                    if (existingUser.Roles.Exists(x => x.Name == EnumUserRole.ADMIN.ToString()))
                    {
                        var organizationId = existingUser.Organizations.Select(x => x.OrganizationId).FirstOrDefault();

                        //Getting all the user from that organization whose admin we are going to remove
                        //Need to check the admin count on that organization
                        var usersList = userClient.GetUsers(authorization, null, organizationId);
                        var adminCount = 0;

                        //Checking number of admin for a perticular organization
                        foreach (var user in usersList)
                        {
                            if (user.Roles.Exists(x => x.Name == EnumUserRole.ADMIN.ToString()))
                            {
                                adminCount++;
                            }
                        }

                        //if there is only 1 admin then remove the organization if not then just only remove that admin user
                        if (adminCount == 1)
                        {
                            response = organizationClient.DeleteOrganization(authorization, organizationId);

                            if (response.HttpStatusCode == 200)
                            {
                                var existingUserList = usersList.Select(x => x.UserName).ToList();

                                //Removing all the users that are in that organizations
                                foreach (var username in existingUserList)
                                {
                                    //Removing the user from the signup database if exist
                                    //var user = signupRepository.Find(existingUser.UserName);
                                    var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(existingUser.UserName));
                                    if (user != null)
                                    {
                                        signupRepository.Remove(user.SignupUid);
                                    }
                                }

                                response.Message = "User Removed Successfully";
                            }
                            else
                            {
                                response.Message = "Error while removing user";
                            }
                        }
                        else
                        {
                            //Remove the particular user
                            response = userClient.RemoveUser(authorization, userId);

                            if (response.HttpStatusCode == 200)
                            {
                                //Removing the user from the signup database if exist
                                //var user = signupRepository.Find(existingUser.UserName);
                                var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(existingUser.UserName));
                                if (user != null)
                                {
                                    signupRepository.Remove(user.SignupUid);
                                }

                            }
                        }
                        return new ObjectResult(response);
                    }

                    //If the user is ATC then we need to delete the respective USS assigned to that ATC user
                    else if (existingUser.Roles.Exists(x => x.Name == EnumUserRole.ATC.ToString()))
                    {
                        //USS list that is assigned to that ATC user
                        var utmInstanceList = utmInstanceRepository.GetAll().Where(x => x.UserId == existingUser.UserId).ToList();
                        bool anyActiveOperations = false;

                        //Checking if there are  any active operations present on that USS area
                        foreach (var utmInstance in utmInstanceList)
                        {
                            anyActiveOperations = operationRepository.GetAll().Any(p => p.UssInstanceId == utmInstance.UssInstanceId && p.IsInternalOperation && p.State != OperationState.CLOSED.ToString());

                            //If any active operations present then return error
                            if (anyActiveOperations)
                            {
                                response.HttpStatusCode = 400;
                                response.Message = "Active operation exists in the USS. Please close the active operation before removing the ATC user";
                                return new ObjectResult(response);
                            }
                        }

                        //If there are not any active operations present then remove the USS area then remove the user
                        if (!anyActiveOperations)
                        {
                            foreach (var utmInstance in utmInstanceList)
                            {
                                gridCell.DeleteGridCellOperator(utmInstance);

                                utmInstanceRepository.Remove(utmInstance.UssInstanceId);

                                //Need to implement deleting DSS Service Area
                                //Example from UtmController.
                                //if (configuration.IsDssEnabled)
                                //{
                                //    string token = httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                                //    mediator.Send(new AsyncDssNotification(utmInstance.Adapt<UtmInstanceItem>(), CRUDActionEnum.DELETE, token));
                                //}
                            }
                        }

                        response = userClient.RemoveUser(authorization, userId);

                        //Removing the user from the signup database if exist
                        //var user = signupRepository.Find(existingUser.UserName);
                        var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(existingUser.UserName));
                        if (user != null)
                        {
                            signupRepository.Remove(user.SignupUid);
                        }

                        return new ObjectResult(response);
                    }
                    else
                    {
                        response = userClient.RemoveUser(authorization, userId);

                        //Removing the user from the signup database if exist
                        //var user = signupRepository.Find(existingUser.UserName);
                        var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(existingUser.UserName));
                        if (user != null)
                        {
                            signupRepository.Remove(user.SignupUid);
                        }

                        return new ObjectResult(response);
                    }
                }
            }
            catch(Exception ex)
            {
                response.HttpStatusCode = 500;
                response.Message = "UNDEFINED_ERROR Please contact system administrator";
                logger.LogError($"Exception ::: UserController :: DeleteUser : ex = {ex.Message}");

                return new ObjectResult(response);
            }
        }

        [HttpPut]
        [Authorize]
        [Route(@"/user/ChangePassword")]
        [SwaggerOperation(nameof(ChangePassword)), SwaggerResponse(200, type: typeof(UserItem))]
        public virtual IActionResult ChangePassword([FromBody]UserChangePassword user, [FromHeader] string authorization)
        {
            var response = userClient.ChangePassword(authorization, user);

            return new ObjectResult(response);
        }

        /// <summary>
        /// Return the OTP for the provided user.
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route(@"/user/GetOTP")]
        [SwaggerOperation(nameof(GetOTP)), SwaggerResponse(200)]
        public virtual IActionResult GetOTP([FromQuery] string UserName)
        {
            //var user = signupRepository.Find(UserName);
            var user = signupRepository.GetAll().FirstOrDefault(x => x.UserName.Equals(UserName));
            if (user == null)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = @"User doesn't exist" });
            }
            else
            {
                return new ObjectResult(user.VerificationCode);
            }
        }

        /// <summary>
        /// Returns the list of orginazation based on the user
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        private List<OrganizationItem> GetOrginazationList(string authorization)
        {
            var orginazationList = new List<OrganizationItem>();

            if (GetCurrentUserRole().Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP))))
            {
                orginazationList = organizationClient.GetOrganizations(authorization, GetCurrentGroupId());
            }
            else if (GetCurrentUserRole().Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ADMIN))))
            {
                var orgId = GetCurrentUserOrganizationId();
                var organization = organizationClient.GetOrganization(orgId.ToString(), authorization);

                orginazationList.Add(organization);
            }
            else
            {
                orginazationList = organizationClient.GetOrganizations(authorization);
            }

            return orginazationList;
        }

        /// <summary>
        /// Returns the list of roles based on the user
        /// </summary>
        /// <returns></returns>
        private List<RoleItem> GetRolesList(List<string> currentUserRole)
        {
            var roles = new List<RoleItem>();

            if (currentUserRole.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.SUPERADMIN))))
            {
                roles = RoleItem.GetRoleDescription();
                roles.RemoveAll(x => x.Name != EnumUserRole.ANSP.ToString());
            }

            else if (currentUserRole.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP))))
            {
                roles = RoleItem.GetRoleDescription();
                roles.RemoveAll(x => x.Name != EnumUserRole.ATC.ToString());

            }
            else
            {
                roles = RoleItem.GetRoleDescription();
                roles.RemoveAll(x => x.Name == EnumUserRole.ANSP.ToString() ||
                x.Name == EnumUserRole.ATC.ToString() || x.Name == EnumUserRole.ADMIN.ToString()
                || x.Name == EnumUserRole.SUPERADMIN.ToString());
            }

            return roles;
        }

        /// <summary>
        /// Assign the role to current user based on the roles selection from the UI
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        private List<int> SetUserRole(List<RoleItem> roles)
        {
            var assignedRole = new List<int>();

            foreach (var item in roles)
            {
                if (item.IsAssigned)
                {
                    assignedRole.Add(item.Id);
                }
            }

            return assignedRole;
        }
    }
}
