﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// UREP Interface
    /// </summary>
    [Route("api/[controller]")]
    public class UrepController : BaseController
    {
        private readonly ILogger<UrepController> logger;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepository<Models.Urep> urepRepository;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly UrepApi urepApi;
        private readonly AnraMqttClient anraMqttClient;
        private AnraConfiguration anraConfiguration;

        public UrepController(ILogger<UrepController> logger,
            IHttpContextAccessor httpContextAccessor,
            FimsTokenProvider fimsAuthentication,
            UrepApi urepApi,
            AnraMqttClient anraMqttClient,
            AnraConfiguration anraConfiguration,
            IRepository<Models.Urep> urepRepository)
        {
            this.fimsAuthentication = fimsAuthentication;
            this.urepRepository = urepRepository;
            this.logger = logger;
            this.httpContextAccessor = httpContextAccessor;
            this.urepApi = urepApi;
            this.anraMqttClient = anraMqttClient;
            this.anraConfiguration = anraConfiguration;
        }

        /// <summary>
        /// Add UREP
        /// </summary>
        /// <param name="urep">The UREP to be submitted. The ID will be minted by the service, so should not be provided.</param>
        /// <param name="gufi">operatio gufi</param>
        /// <response code="201">Created</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpPost]
        [Authorize("uss:write-urep")]
        [Route("/operation/{gufi}/urep")]
        public UTMRestResponse AddUrepUsingPOST([FromRoute]Guid gufi, [FromBody]Utm.Models.Urep urep)
        {                        
            urep.UserId = GetCurrentUserId();
            urep.TimeSubmitted = DateTime.UtcNow;            

            var response = this.anraConfiguration.IsUtmEnabled ? urepApi.PostUrepInstance(urep) : new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = @"Urep created.", Id = gufi.ToString() };

            if(response.HttpStatusCode == (int)HttpStatusCode.Created || response.HttpStatusCode == (int)HttpStatusCode.OK)
            {
                //Save into Local DB
                var model = urep.Adapt<Models.Urep>();
                model.OrganizationId = GetCurrentUserOrganizationId();
                model.GroupId = GetCurrentGroupId();
				urepRepository.Add(model);

                //Notify User
                anraMqttClient.PublishTopic(string.Format(MqttTopics.UrepPostTopic, urep.UserId, urep.Gufi.ToString()), urep);
            }

            return response;
        }

        /// <param name="gufi">operation gufi</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet]
        [Authorize("uss:read-urep")]
        [Route("/operation/{gufi}/ureps")]
        public List<Utm.Models.Urep> GetListing([FromRoute]Guid gufi)
        {
            var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();
            var ureps = new List<Utm.Models.Urep>();

            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                ureps = urepRepository.GetAll().Where(p => p.Gufi == gufi).Adapt<List<Utm.Models.Urep>>();

            }
            else if (CheckRole(EnumUserRole.ANSP.ToString()))
            {
                ureps = urepRepository.GetAll().Where(p => p.Gufi == gufi).Where(x=>x.GroupId.Equals(groupId)).Adapt<List<Utm.Models.Urep>>();
            }
            else
            {
                ureps = urepRepository.GetAll().Where(p => p.Gufi == gufi).Where(x => x.OrganizationId.Equals(organizationId)).Adapt<List<Utm.Models.Urep>>();
            }

            return ureps;
        }

        
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet]        
        [Authorize("uss:read-urep")]
        [Route("/operation/ureps")]
        public List<Utm.Models.Urep> GetUreps()
        {
            var response = urepApi.GetUreps();
            return response;
        }
    }
}