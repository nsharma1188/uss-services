﻿using AnraUssServices.Common;
using AnraUssServices.ViewModel;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.HealthChecks;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Controllers
{
    [Route("api/[controller]")]
    public class HealthCheckController : Controller
    {
        private readonly IServiceProvider serviceProvider;
        private readonly IHealthCheckService _healthCheckService;
        private readonly AnraConfiguration configuration;

        public HealthCheckController(IHealthCheckService healthCheckService, IServiceProvider serviceProvider, AnraConfiguration configuration)
        {
            _healthCheckService = healthCheckService;
            this.serviceProvider = serviceProvider;
            this.configuration = configuration;
        }

        [HttpGet]
        [Route("/healthcheck")]
        public async Task<IActionResult> Get()
        {
            using (var timedTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(5))) // extend more time 3 -> 5  // if delay system, the light will not green
            {
                var healthCheckResult = await _healthCheckService.CheckHealthAsync(timedTokenSource.Token);

                var results = new List<HealthCheckResultViewModel>();

                foreach (var result in healthCheckResult.Results)
                {
                    results.Add(new HealthCheckResultViewModel { Key = result.Key, Value = result.Value.CheckStatus.ToString() });
                }

                return Ok(new { Status = healthCheckResult.CheckStatus, Results = results });
            }
        }


        [HttpGet]        
        [Route("/versioncheck")]
        public async Task<IActionResult> GetDeployedServicesVersion()
        {
            List<string> serviceUrls = new List<string>();
            //Currently there is no version endpoint for all the other services 
            //serviceUrls.Add(string.Format("{0}{1}", configuration.OAuthServerUrl, "getVersion"));
            //serviceUrls.Add(string.Format("{0}{1}", configuration.TelemetryServiceUrl, "getVersion"));
            //serviceUrls.Add(string.Format("{0}{1}", configuration.DetectionServiceUrl, "getVersion"));
            //serviceUrls.Add(string.Format("{0}{1}", configuration.WeatherServiceUrl, "getVersion"));

            try
            {
                var results = new List<HealthCheckResultViewModel>();

                //Add Uss Service Version
                results.Add(new HealthCheckResultViewModel { Key = "USS Service", Value = configuration.CurrentVersion });

                foreach (var url in serviceUrls)
                {
                    var request = new HttpRequest(url, HttpMethod.Get);
                    var httpClient = HttpHandler.Builder.Create;
                    using (var response = await httpClient.SendAsync(request))
                    {
                        var result = JsonConvert.DeserializeObject<VersionDetails>(response.Content.Read());
                        results.Add(new HealthCheckResultViewModel { Key = result.Name, Value = result.Version });
                    }
                }

                return Ok(new { Status = "Ok", Results = results });
            }
            catch(Exception ex)
            {
                return Ok(new { Status = "Error" });
            }
        }                   
    }

    public class VersionDetails
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }
}