﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace AnraUssServices.Controllers
{
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = false)]
    public class PingController : BaseController
    {
        private readonly AnraConfiguration anraConfiguration;
        private readonly ApplicationDbContext context;

        public PingController(ILogger<string> logger, ApplicationDbContext context, AnraConfiguration anraConfiguration)
        {
            #if DEBUG
            //logger.LogInformation("Ping Controller");
            #endif
            this.context = context;
            this.anraConfiguration = anraConfiguration;
        }

        [HttpGet]
		[Authorize]
        //[Authorize(Policy = "RequireAdminRole")]
        //[Authorize(Roles = "Visual Observer")]
        //[Authorize(Roles = "Super Admin")]
        public string Get()
        {
            var msg = "Anra Rest Api";
            return msg;
        }

        [HttpGet]
        [Route(@"/.well-known/uas-traffic-management/utm.jwks")]
		[Authorize]
		public ActionResult WellKnownUri()
        {
            var jsonWebKeySet = new JsonWebKeySet();
            var publicJwk = SecurityKeysStore.GetPublicKeyAsJWK(anraConfiguration);
            jsonWebKeySet.Keys.Add(publicJwk);

            return new ObjectResult(jsonWebKeySet);
        }
    }
}