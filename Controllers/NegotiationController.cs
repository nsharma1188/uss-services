﻿using AnraUssServices.Common;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.NegotiationMessages;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    [Route(@"api/[controller]")]
    public class NegotiationController : BaseController
    {
        private readonly NegotiationApi negotiationApi;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly ILogger<NegotiationController> logger;
        private readonly IRepository<Models.NegotiationMessage> negotiationMessageRepository;

        public NegotiationController(ILogger<NegotiationController> logger,
            IRepository<Models.NegotiationMessage> negotiationMessageRepository,
            IRepository<Models.Operation> operationRepository,
            IRepository<UtmInstance> utmInstanceRepository,
            NegotiationApi negotiationApi)
        {
            this.logger = logger;
            this.negotiationMessageRepository = negotiationMessageRepository;
            this.operationRepository = operationRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.negotiationApi = negotiationApi;
        }

        /// <summary>
        /// Returns all negotations messages for an operation
        /// </summary>
        /// <param name="gufi">operation gufi</param>
        /// <remarks>Returns all negotations messages for an operation</remarks>
        /// <response code="200">Returns all negotations messages for an operation</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet, ApiValidationFilter]
        [Authorize("uss:read-message")]        
        [Route(@"/negotiation/operation/{gufi}/listing")]
        [SwaggerOperation("GetUasOperations")]
        [SwaggerResponse(200, typeof(List<NegotiationMessageViewModel>), "Returns all operations within the specified geography")]
        [SwaggerResponse(400, typeof(List<NegotiationMessageViewModel>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<NegotiationMessageViewModel>), "Unauthorized")]
        [SwaggerResponse(403, typeof(List<NegotiationMessageViewModel>), "Forbidden")]
        [SwaggerResponse(404, typeof(List<NegotiationMessageViewModel>), "Not Found")]
        public virtual IActionResult GetNegotiations([FromRoute]Guid gufi)
        {
            var messages = negotiationMessageRepository.GetAll().Where(p => p.GufiOfOriginator == gufi || p.GufiOfReceiver == gufi).OrderByDescending(p => p.DateCreated).ToList().Adapt<List<NegotiationMessageViewModel>>();
            return new ObjectResult(messages);
        }

        /// <summary>
        /// Allow operators to send negotiation message for an operation.
        /// </summary>
        /// <param name="negotiationMessage">Negotation data</param>
        ///
        /// <param name="gufi">operation Gufi</param>
        /// <remarks>Create new negotation message</remarks>
        /// <response code="201">Negotation data received.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")]        
        [Route(@"/negotiation/operation/{gufi}")]
        [SwaggerOperation(nameof(PostNegotiationMessage)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostNegotiationMessage([FromRoute]Guid gufi, [FromBody]NegotiationMessageInputModel negotiationMessage)
        {
            var externalUssInstance = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssName.Equals(negotiationMessage.UssNameOfReceiver));

            if (externalUssInstance == null)
            {
                return ApiResponse(StatusCodes.Status400BadRequest, "Invalid USS");
            }

            var operation = operationRepository.Find(gufi);
            if (operation == null)
            {
                return ApiResponse(StatusCodes.Status400BadRequest, "Invalid Operation");
            }

            negotiationMessage.Update(operation);
            var response = negotiationMessage.Send(externalUssInstance.UssBaseCallbackUrl, negotiationApi, externalUssInstance.UssName);

			negotiationMessageRepository.Add(negotiationMessage.Adapt<Models.NegotiationMessage>());

            return new ObjectResult(response);
        }
    }
}