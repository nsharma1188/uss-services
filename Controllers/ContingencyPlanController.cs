﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route(@"api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ContingencyPlanController : BaseController
    { 
        private readonly DatabaseFunctions dbFunctions;
        private readonly IMediator mediator;
        private readonly AnraMqttClient anraMqttClient;
        private readonly IRepository<Models.UtmMessage> utmMessageRepository;
        private readonly IRepository<Models.NegotiationMessage> negoationMessageRepository;
        private readonly IRepository<Models.ConstraintMessage> constraintsRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ContingencyPlanController> logger;
        private readonly IRepository<Models.OperationVolume> operationVolumeRepository;
        private readonly IRepository<Models.ContingencyPlan> contingencyPlanRepository;        

        public ContingencyPlanController(ApplicationDbContext context,
            ILogger<ContingencyPlanController> logger,
            IRepository<Models.Operation> operationRepository,
            IRepository<Models.ConstraintMessage> constraintsRepository,
            IRepository<Models.NegotiationMessage> negoationMessageRepository,
            IRepository<UtmMessage> utmMessageRepository,
            IRepository<Models.OperationVolume> operationVolumeRepository,
            IRepository<Models.ContingencyPlan> contingencyPlanRepository,             
            IMediator mediator,
            DatabaseFunctions dbFunctions,
            AnraMqttClient anraMqttClient)
        {
            _context = context;
            this.logger = logger;
            this.operationRepository = operationRepository;
            this.constraintsRepository = constraintsRepository;
            this.negoationMessageRepository = negoationMessageRepository;
            this.utmMessageRepository = utmMessageRepository;
            this.operationVolumeRepository = operationVolumeRepository;
            this.contingencyPlanRepository = contingencyPlanRepository;
            this.anraMqttClient = anraMqttClient;
            this.mediator = mediator;
            this.dbFunctions = dbFunctions;             
        }

        /// <summary>Submit new Contigency Plan for an Operation.</summary>
        /// <param name="gufi">Operation Gufi</param>
        /// <param name="contingency">Contingency plan data</param>
        /// <remarks>Operator can submit operation contigency plan for an operation</remarks>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize]
        [Route("/operation/{gufi}/contigencyplan")]
        [SwaggerOperation(nameof(PostContingencyPlan)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        //public virtual IActionResult PostContingencyPlan(Guid gufi, [FromBody]ViewModel.ContigencyPlan contingency)
        public virtual IActionResult PostContingencyPlan(Guid gufi, [FromBody]Utm.Models.ContingencyPlan contingency)
        {
            var operation = operationRepository.GetAll().FirstOrDefault(p => p.Gufi.Equals(gufi));

            if (operation != null)
            {
                var model = contingency.Adapt<Models.ContingencyPlan>();
                model.Gufi = operation.Gufi;
                contingencyPlanRepository.Add(model);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource created.", Id =  model.ContingencyPlanUid.ToString()});
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "Resource not found." });
            }
        }

        /// <summary>Submit new Contigency Plan for an Operation.</summary>
        /// <param name="gufi">Operation Gufi</param>
        /// <param name="contingency">Contingency plan data</param>
        /// <remarks>Operator can submit operation contigency plan for an operation</remarks>
        /// <response code="200">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Authorize]
        [Route("/operation/{gufi}/contigencyplan")]
        [SwaggerOperation(nameof(PostContingencyPlan)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        //public virtual IActionResult PostContingencyPlan([FromBody]ViewModel.ContigencyPlan contingency)
        public virtual IActionResult PostContingencyPlan([FromBody]Utm.Models.ContingencyPlan contingency)
        {
            var contingencyPlan = contingencyPlanRepository.Find(contingency.ContingencyId);

            if (contingencyPlan != null)
            {
                contingency.Adapt(contingencyPlan);
                contingencyPlanRepository.Update(contingencyPlan);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource updated.", Id = contingencyPlan.ContingencyPlanUid.ToString() });
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "Resource not found." });
            }
        }

        /// <summary>
        /// Listing of Contingency Plans for an Operation
        /// </summary>
        /// <remarks>Allows querying for Contingency Plans.</remarks>
        /// <param name="gufi">Operation Gufi</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize]
        [Route("/operation/{gufi}/contingencyplan/listing")]
        [SwaggerOperation(nameof(GetContingencyPlanListing)), SwaggerResponse(200, type: typeof(List<Utm.Models.ContingencyPlan>))]
        public virtual IActionResult GetContingencyPlanListing([FromRoute]Guid gufi)
        {
            var operation = operationRepository.GetAll().FirstOrDefault(p => p.Gufi.Equals(gufi));

            if (operation != null)
            {
                var contingencies = contingencyPlanRepository.GetAll().Where(p => p.Gufi == operation.Gufi).Adapt<List<Utm.Models.ContingencyPlan>>();

                return new ObjectResult(contingencies);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = "Resource not found." });
            }
        }
    }
}