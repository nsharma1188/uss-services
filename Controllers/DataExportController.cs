﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.DMPData.DataHandlers.DynamicRestriction;
using AnraUssServices.DMPData.DataHandlers.OffNominalReport;
using AnraUssServices.DMPData.DataHandlers.Uss2OperatorExchange;
using AnraUssServices.DMPData.DataHandlers.UssExchangeData;
using AnraUssServices.DMPData.DataHandlers.UssNegotiation;
using AnraUssServices.DMPData.Models;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Controllers
{
	/// <summary>
	/// The Data Export Controller is for NASA's use for submitting the data NASA.
	/// </summary>
	[Route(@"api/[controller]")]
	public class DataExportController : Controller
	{
		private readonly ExportOffNominalReport exportOffNominal;
		private readonly AnraConfiguration anraConfiguration;
		private readonly AnraHttpClient httpClient;
		private readonly FimsTokenProvider fimsAuthentication;
		private readonly IRepository<Models.Operation> operationRepository;
		private readonly ExportUssExchangesReport exportUssExchanges;
		private readonly ExportDynamicRestrictionOccurenceReport exportDynamicRestrictionOccurence;
		private readonly ExportUssNegotiation exportUssNegotiation;
		private readonly TokenValidator tokenValidator;
		private readonly ExportUss2OperatorExchangeData exportUss2OperatorExchange;


		public DataExportController(ExportOffNominalReport exportOffNominal, AnraConfiguration anraConfiguration,
			AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication, IRepository<Models.Operation> operationRepository,
			ExportUssExchangesReport exportUssExchanges, ExportDynamicRestrictionOccurenceReport exportDynamicRestrictionOccurence,
			ExportUssNegotiation exportUssNegotiation, TokenValidator tokenValidator, ExportUss2OperatorExchangeData exportUss2OperatorExchange)
		{
			this.anraConfiguration = anraConfiguration;
			this.exportOffNominal = exportOffNominal;
			this.httpClient = httpClient;
			this.fimsAuthentication = fimsAuthentication;
			this.operationRepository = operationRepository;
			this.exportUssExchanges = exportUssExchanges;
			this.exportDynamicRestrictionOccurence = exportDynamicRestrictionOccurence;
			this.exportUssNegotiation = exportUssNegotiation;
			this.tokenValidator = tokenValidator;
			this.exportUss2OperatorExchange = exportUss2OperatorExchange;
		}


		/// <summary>
		/// Submit Off Nominal Report to NASA
		/// </summary>
		/// <param name="Date">Date for which we want to generate the off nominal report. Should be in format of "yyyy_MM_dd" </param>
		/// <param name="Token"> Security Token</param>
		/// <response code="200">Successful data report submisson</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		[HttpPost]
		[Route(@"/offnominalreport/{Date}/submit")]
		[SwaggerOperation(@"SubmitOffNominalReport"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitOffNominal([FromRoute]string Date, [FromHeader]string Token, [FromBody]OffNominalOperatorResponse operatorResponse)
		{
			var validateToken = tokenValidator.IsTokenValid(Token);
			var response = new UTMRestResponse();
			if (!validateToken)
			{
				response.HttpStatusCode = 403;
				response.Message = "Invalid Security Token";
			}
			else
			{
				var url = anraConfiguration.UASServiceUrl + "v4/off-nominal";
				var offNominalReportList = exportOffNominal.GetOffNominal(Date);

				var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);
				foreach (var offNominal in offNominalReportList)
				{
					if (offNominal.Gufi.ToString() == operatorResponse.gufi)
					{
						offNominal.ReporterNarrative = operatorResponse.reporter_narrative;
						offNominal.MetaDataDumpUss.test_run = operatorResponse.test_run;
						var offNominalList = new List<OffNominal>();
						offNominalList.Add(offNominal);
						response = httpClient.Post(url, offNominalList, token, "");
					}
				}
			}

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Message });
		}

		/// <summary>
		/// Submit LossOffUssServices data to NASA
		/// </summary>
		/// <param name="Token"> Security Token</param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/lossOfUssServices/submit")]
		[SwaggerOperation(@"SubmitLossOffUssServices"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitLossOfUssServices([FromHeader]string Token, [FromBody] LossOfUss lossOfUss)
		{
			var validateToken = tokenValidator.IsTokenValid(Token);
			var response = new UTMRestResponse();

			if (!validateToken)
			{
				response.HttpStatusCode = 403;
				response.Message = "Invalid Security Token";
			}
			else
			{
				var url = anraConfiguration.UASServiceUrl + "v4/loss-of-uss";

				var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);
				var lossOfUssList = new List<LossOfUss>();
				lossOfUssList.Add(lossOfUss);
				response = httpClient.Post(url, lossOfUssList, token, "");
			}

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Message });
		}

		/// <summary>
		/// Submit Uss2OperatorExchange data to NASA
		/// </summary>
		/// <param name="Date">Date format yyyy_MM_dd</param>
		/// <param name="Gufi">Operations Gufi</param>
		/// <param name="MessageCount">Number messages from USS sent</param>
		/// <param name="Token">Security Token</param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/uss2operatorexchangereport/submit")]
		[SwaggerOperation(@"SubmitUss2OperatorExchange"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitUss2OperatorExchange([FromQuery] string Date, [FromHeader]string Token, [FromHeader] string Gufi, [FromQuery] int MessageCount)
		{
			var validateToken = tokenValidator.IsTokenValid(Token);

			var uss2OperatorExchange = new Uss2OperatorExchangeModel();
			var metaData = new MetaDataDmpUSS();
			var uss2OperatorExchangeList = new List<Uss2OperatorExchangeModel>();
			var response = new UTMRestResponse();

			if (!validateToken)
			{
				response.HttpStatusCode = 403;
				response.Message = "Invalid Security Token";
			}
			else
			{
				var url = anraConfiguration.UASServiceUrl + "v4/uss-2-operator-exchange";

				var operation = operationRepository.Find(Gufi);

				if (operation != null)
				{
					metaData.call_sign = operation.Metadata.CallSign;
					metaData.data_collection = operation.Metadata.DataCollection.Value;
					metaData.test_run = Convert.ToInt32(operation.Metadata.TestRun);
					metaData.uss_name = operation.UssName;
					uss2OperatorExchange.UssOperatorExchangeId = Guid.NewGuid().ToString();
					uss2OperatorExchange.Gufi = Gufi;
					uss2OperatorExchange.NumberPositionReportsReceived = exportUss2OperatorExchange.GetPosition(Date, uss2OperatorExchange.Gufi);
					uss2OperatorExchange.HumanInteractions = true;
					uss2OperatorExchange.ContinuousUssService = true;
					uss2OperatorExchange.MetaDataDumpUss = metaData;
					uss2OperatorExchange.Operator = "Anra Pilot";
					uss2OperatorExchange.Notes = "NASA TCL4 Flight";
					uss2OperatorExchange.NumberMessagesFromUssSent = MessageCount;

					if (uss2OperatorExchange.NumberPositionReportsReceived == 0)
					{
						response.HttpStatusCode = 400;
						response.Message = "Could not get Position records from LiteDb. Check the Date and OperationGufi";
					}
					else
					{
						uss2OperatorExchangeList.Add(uss2OperatorExchange);
						var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);
						response = httpClient.Post(url, uss2OperatorExchangeList, token, "");
					}
				}
				else
				{
					response.HttpStatusCode = 400;
					response.Message = "Bad Gufi Operation Doesn't Exist";
				}
			}

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Message });
		}

		/// <summary>
		/// Submit DynamicRestricOccurence data to NASA (Currently Automated)
		/// </summary>
		/// <param name="Date"></param>
		/// <returns></returns>
		[HttpPost]
		[ApiExplorerSettings(IgnoreApi = true)]
		[Route(@"/dynamicrestrictionoccurence/{Date}/submit")]
		[Authorize]
		[SwaggerOperation(@"SubmitDynamicRestricOccurence"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitDynamicRestricOccurence([FromRoute] string Date)
		{
			var response = new UTMRestResponse();
			var url = anraConfiguration.UASServiceUrl + "v4/dynamic-restriction-occurrence";
			var dynamicRestrictionOccurenceList = exportDynamicRestrictionOccurence.GetDynamicRestrictionOccurence(Date);
			var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);

			response = httpClient.Post(url, dynamicRestrictionOccurenceList, token, "");

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Message });
		}

		/// <summary>
		/// Submit UssNegotiation data to NASA (Currently Automated)
		/// </summary>
		/// <param name="Date"></param>
		/// <returns></returns>
		[HttpPost]
		[ApiExplorerSettings(IgnoreApi = true)]
		[Route(@"/ussnegotiation/{Date}/submit")]
		[Authorize]
		[SwaggerOperation(@"SubmitUssNegotiation"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitUssNegotiation([FromRoute] string Date)
		{
			var response = new UTMRestResponse();
			var url = anraConfiguration.UASServiceUrl + "v2/uss-negotiation";
			var ussNegotationList = exportUssNegotiation.GetUssNegotiation(Date);
			var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);

			response = httpClient.Post(url, ussNegotationList, token, "");

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = response.HttpStatusCode, Message = response.Message });
		}

		/// <summary>
		/// Submit UssExchage data to NASA
		/// </summary>
		/// <param name="Token">Security Token</param>
		/// <param name="Date">Date should be in format yyyy_MM_dd</param>
		/// <param name="TestRunList">List of Test Run you are submitting the data to NASA</param>
		/// <returns></returns>
		[HttpPost]
		[Route(@"/ussexchangesreport/submit")]
		[SwaggerOperation(@"SubmitUssExchanges"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public IActionResult SubmitUssExchanges([FromQuery] string Date, [FromHeader] string Token, [FromBody] List<string> TestRunList)
		{
			var validateToken = tokenValidator.IsTokenValid(Token);
			var responseList = new List<UTMRestResponse>();

			var response = new UTMRestResponse();

			if (!validateToken)
			{
				response.HttpStatusCode = 403;
				response.Message = "Invalid Security Token";
				responseList.Add(response);
			}
			else
			{
				var url = anraConfiguration.UASServiceUrl + "v4/uss-exchange";
				var ussExchangesReportList = exportUssExchanges.GetUssExchanges(Date);
				var ussExchangeDataList = new List<USSExchange>();

				foreach (var ussExchange in ussExchangesReportList)
				{
					var submitUssExchange = GetUssExchangeSubmission(ussExchange);

					if (submitUssExchange.metaDataDmpUss != null)
					{
						ussExchangeDataList.Add(submitUssExchange);
					}
				}

				foreach (var testRun in TestRunList)
				{
					var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE).Substring(7);

					var submitUssExchangeData = ussExchangeDataList.Where(x => x.metaDataDmpUss.test_run == Convert.ToInt32(testRun)).ToList();

					var newResponse = httpClient.Post(url, submitUssExchangeData, token, "");
					newResponse.Message = newResponse.Message + " Data Submission of TestRun "+ testRun;
					responseList.Add(newResponse);

				}
			}

			return new ObjectResult(responseList);
		}

		private USSExchange GetUssExchangeSubmission(UssExchangeDataModel UssExchange)
		{
			var ussExchange = new USSExchange();

			try
			{
				var exchangeDataType = UssExchange.exchanged_data_type.ToString();
				var operationGufi = "";
				switch (exchangeDataType)
				{
					case "OPERATION":
						if (!string.IsNullOrEmpty(UssExchange.request_content))
						{
							var operationRequest = JsonConvert.DeserializeObject<GufiModel>(UssExchange.request_content);
							if (operationRequest != null)
							{
								operationGufi = operationRequest.gufi.ToString();
							}
						}
						break;
					case "POSITION":
						if (!string.IsNullOrEmpty(UssExchange.request_content))
						{
							var positionRequest = JsonConvert.DeserializeObject<GufiModel>(UssExchange.request_content);
							if (positionRequest != null)
							{
								operationGufi = positionRequest.gufi.ToString();
							}
						}
						break;
					case "UTM_MESSAGE":
						if (!string.IsNullOrEmpty(UssExchange.request_content))
						{
							var utmMessageRequest = JsonConvert.DeserializeObject<GufiModel>(UssExchange.request_content);
							if (utmMessageRequest != null)
							{
								operationGufi = utmMessageRequest.gufi.ToString();
							}
						}
						break;
					case "NEGOTIATION_MESSAGE":
						if (!string.IsNullOrEmpty(UssExchange.request_content))
						{
							var negotiationMessageRequest = JsonConvert.DeserializeObject<Utm.Models.NegotiationMessage>(UssExchange.request_content);
							if (negotiationMessageRequest != null)
							{
								operationGufi = negotiationMessageRequest.GufiOfOriginator.ToString();
							}
						}
						break;
					default:
						operationGufi = "";
						break;
				}

				if (!string.IsNullOrEmpty(operationGufi))
				{
					var internalOperation = operationRepository.Find(operationGufi);
					if (internalOperation != null)
					{
						if (internalOperation.IsInternalOperation)
						{
							var operation = operationRepository.Find(operationGufi).Adapt<Utm.Models.Operation>();
							if (operation != null)
							{
								var metaData = new MetaDataDmpUSS()
								{
									call_sign = operation.Metadata.CallSign,
									data_collection = operation.Metadata.DataCollection,
									uss_name = operation.UssName
								};

								var ussExchangeData = new USSExchange()
								{
									metaDataDmpUss = metaData,
									measurement_id = UssExchange.measurement_id,
									event_id = UssExchange.event_id,
									exchanged_data_pk = UssExchange.exchanged_data_pk,
									exchanged_data_type = UssExchange.exchanged_data_type.ToString(),
									source_uss = UssExchange.source_uss,
									target_uss = UssExchange.target_uss,
									reporting_uss_role = UssExchange.reporting_uss_role,
									time_request_initiation = UssExchange.time_request_initiation,
									time_request_completed = UssExchange.time_request_completed,
									endpoint = UssExchange.endpoint,
									http_method = UssExchange.http_method,
									actual_http_response = UssExchange.actual_http_response,
									expected_http_response = UssExchange.expected_http_response,
									comments = UssExchange.comments

								};

								if (operation.Metadata.DataCollection && ussExchangeData.source_uss == anraConfiguration.ClientId)
								{
									ussExchangeData.metaDataDmpUss.test_run = Convert.ToInt32(operation.Metadata.TestRun);
									ussExchange = ussExchangeData;
								}
							}
						}
					}
				}

				return ussExchange;
			}
			catch (Exception ex)
			{
				return ussExchange;
			}
		}
	}
}
