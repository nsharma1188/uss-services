﻿using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;

namespace AnraUssServices.Controllers
{
    public class BaseController : Controller
    {
        //Currently claims doesn't supports objectIdentifier
        private string OrganizationId = "http://schemas.microsoft.com/identity/claims/objectidentifier";

        private string GetClaimValue(string claimType)
        {
            if (User.HasClaim(p => p.Type == claimType))
            {
                return User.Claims.Where(p => p.Type == claimType).Select(p => p.Value).SingleOrDefault();
            }

            return null;
        }

        /// <summary>
        /// Generic method to check if requested roles are part of Claim or not
        /// </summary>
        /// <param name="rolesToCheck"> List of roles need to check</param>
        /// <param name="checkAll"> If true then check all requested roles are part of claim value or not, 
        /// If false then check if any of the requested role is part of claim value. </param>
        /// <returns> true/ false</returns>
        protected bool CheckRoles(List<string> rolesToCheck, bool checkAll=false)
        {
            var roles = User.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).ToList();

            if (checkAll)
            {
                return rolesToCheck.All(inputrole => roles.Contains(inputrole));
            }
            else
            {
                return roles.Any(a => rolesToCheck.Contains(a));
            }            
        }

        /// <summary>
        /// Generic method to check if requested role is part of Claim or not
        /// </summary>
        /// <param name="roleToCheck"> Role need to check</param>
        /// <returns> true/ false</returns>
        protected bool CheckRole(string roleToCheck)
        {
            var roles = User.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).ToList();

            return roles.Any(x => x.Contains(roleToCheck));
           
        }

        /// <summary>
        /// Returns current user organization id
        /// </summary>
        /// <returns></returns>
        protected Guid GetCurrentUserOrganizationId()
        {
            var organizationId = User.FindFirst(OrganizationId).Value;

            return organizationId.Adapt<Guid>();
		}

        /// <summary>
        /// Get current user user id
        /// </summary>
        /// <returns></returns>
        protected string GetCurrentUserId()
        {
			var userId = User.FindFirst("uid").Value;

            return userId;
		}

        /// <summary>
        /// Returns the list of current users role
        /// </summary>
        /// <returns></returns>
		protected List<string> GetCurrentUserRole()
		{
            var rolesList = new List<string>();
            var roles = User.Claims.Where(x => x.Type == ClaimTypes.Role).ToList();

            rolesList.AddRange(roles.Select(x=>x.Value));
            return rolesList;
		}

        /// <summary>
        /// Get current user group id
        /// </summary>
        /// <returns></returns>
		protected string GetCurrentGroupId()
        { 
			var groupId = User.FindFirst("gid").Value;

            return groupId;
		}

		protected IActionResult ApiResponse(int statusCode)
        {
            return StatusCode(statusCode);
        }

        protected IActionResult ApiResponse(int statusCode, string message)
        {
            return StatusCode(statusCode, new UTMRestResponse { HttpStatusCode = statusCode, Message = message });
        }

        protected IActionResult ApiResponse(int statusCode, string message, string resource)
        {
            return StatusCode(statusCode, new UTMRestResponse { HttpStatusCode = statusCode, Message = message});
        }

        protected IActionResult ApiErrorResponse(string errorMessage)
        {
            return BadRequest(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = errorMessage });
        } 
    }
}