﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.NearEarth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AnraUssServices.Common;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Near Earth Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class NearEarthController : BaseController
    {
        private readonly ILogger<NearEarthController> logger;        
        private readonly AnraMqttClient anraMqttClient;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;        

        public NearEarthController(ILogger<NearEarthController> logger,
            IRepository<Models.Operation> operationRepository,            
            IRepository<UtmInstance> utmInstanceRepository,            
            AnraMqttClient anraMqttClient)
        {
            this.logger = logger;            
            this.anraMqttClient = anraMqttClient;
            this.operationRepository = operationRepository;            
            this.utmInstanceRepository = utmInstanceRepository;                                   
        }

        /// <summary>
        /// Process Landing Zone
        /// </summary>
        /// <param name="gufi">Operation Gufi</param>
        /// <response code="200">Ok</response>
        [HttpPost]
        [Authorize("uss:write-detection")]  
        [Route(@"/nearearth/landingzone/{gufi}")]
        [SwaggerOperation(nameof(ProcessLandingZone)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult ProcessLandingZone([FromRoute] Guid gufi, [FromBody] LandingZoneEvaluation landingZoneResult)
        {
            var operation = operationRepository.Find(gufi);

            if (operation == null || !operation.IsInternalOperation)
            {
                return ApiResponse(StatusCodes.Status404NotFound,"Operation not found.");
            }
            else
            {
                var sourceUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(operation.UssName)).FirstOrDefault();

                //Create LandingZoneMessage To Notify Operator
                LandingZoneMessage landingZoneMessage = new LandingZoneMessage();

                //Set Landing Point
                landingZoneMessage.LandingPoint = landingZoneResult.selected_point != null ? 
                                            new Point{ Type = nameof(Point), Coordinates = new List<double?> {
                                                landingZoneResult.selected_point.lon,
                                                landingZoneResult.selected_point.lat
                                            }} : null;

                //Set Message to Notify
                landingZoneMessage.FreeText = landingZoneResult.safe_point_found ? $"Safe landing point found.Gufi - {gufi}" : $"No safe landing point found.Gufi - {gufi}";

                //Notify Operator
                anraMqttClient.PublishTopic(string.Format(MqttTopics.LandingZoneEvaluationTopic, gufi), landingZoneMessage);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = @"Operator notified.", Id = gufi.ToString() });
            }
        }
    }
}