using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NpgsqlTypes;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ServiceFilter(typeof(ApiUssExchangeFilterAttribute))]
    public partial class UssController : BaseController
    {
        private readonly GenericMqttNotification genericMqttNotification;
        private readonly IRepository<OperationConflict> operationConflictRepository;
        private readonly IMediator mediator;
        private readonly OperationStatusLogic operationStatusLogic;
        private readonly MqttLogger mqttLogger;
        private readonly AnraMqttClient anraMqttClient;
        private readonly IRepository<LocalNetwork> localNetworkRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<UtmMessage> utmMessageRepository;
        private readonly IRepository<Models.NegotiationMessage> negoationMessageRepository;
        private readonly IRepository<Models.ConstraintMessage> constraintsRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly IRepository<Models.OperationVolume> operationVolumeRepository;
        private readonly IRepository<Models.ContingencyPlan> contingencyPlanRepository;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ApplicationDbContext _context;
        private readonly ILogger<UssController> logger;
        private OperationValidator _operationValidator;
        private UtmMessageValidator _utmMessageValidator;       
        private PositionValidator _positionValidator;
        private UvrValidator _uvrValidator;

        private readonly IRepository<Models.VehicleData> vehicleDataRepository;

        private readonly UssDiscoveryApi ussDiscoveryApi;
        private readonly DatabaseFunctions dbFunctions;
        private AnraConfiguration anraConfiguration;
        private UssVehicleRegistrationApi ussVehicleRegistrationApi;

        private ViewerMqttNotification ViewerMqttNotification { get; }
        private readonly UtmMessenger utmMessenger;
		private readonly ReplanGeographyHelper replanGeography;
        private readonly InterUssHelper interUssHelper;

        public UssController(ApplicationDbContext context, ILogger<UssController> logger,
            MqttLogger mqttLogger,
            FimsTokenProvider fimsAuthentication,
            IRepository<Models.Operation> operationRepository,
            IRepository<Models.ConstraintMessage> constraintsRepository,
            IRepository<Models.NegotiationMessage> negoationMessageRepository,
            IRepository<Models.OperationVolume> operationVolumeRepository,
            IRepository<UtmMessage> utmMessageRepository,
            IRepository<LocalNetwork> localNetworkRepository,
            IRepository<Models.ContingencyPlan> contingencyPlanRepository,
            AnraMqttClient anraMqttClient,
            OperationValidator operationValidator,
            UtmMessageValidator utmMessageValidator,
            PositionValidator positionValidator,
            UvrValidator uvrValidator,
            OperationStatusLogic operationStatusLogic,
            IMediator mediator,
            IRepository<UtmInstance> utmInstanceRepository,
            UssDiscoveryApi ussDiscoveryApi,
            DatabaseFunctions dbFunctions,
            AnraConfiguration anraConfiguration,
            UssVehicleRegistrationApi ussVehicleRegistrationApi,
            ViewerMqttNotification viewerMqttNotification,
            IRepository<OperationConflict> operationConflictRepository,
            GenericMqttNotification genericMqttNotification,
            UtmMessenger utmMessenger,
            IRepository<Models.VehicleData> vehicleDataRepository,
			InterUssHelper interUssHelper,
            ReplanGeographyHelper replanGeography)
        {
            _context = context;
            this.logger = logger;
            _operationValidator = operationValidator;
            _utmMessageValidator = utmMessageValidator;            
            _positionValidator = positionValidator;
            _uvrValidator = uvrValidator;
            this.fimsAuthentication = fimsAuthentication;
            this.operationRepository = operationRepository;
            this.constraintsRepository = constraintsRepository;
            this.negoationMessageRepository = negoationMessageRepository;
            this.utmMessageRepository = utmMessageRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.localNetworkRepository = localNetworkRepository;
            this.operationVolumeRepository = operationVolumeRepository;
            this.contingencyPlanRepository = contingencyPlanRepository;
            this.anraMqttClient = anraMqttClient;
            this.ussDiscoveryApi = ussDiscoveryApi;
            this.dbFunctions = dbFunctions;
            this.anraConfiguration = anraConfiguration;
            this.ussVehicleRegistrationApi = ussVehicleRegistrationApi;
            ViewerMqttNotification = viewerMqttNotification;
            this.vehicleDataRepository = vehicleDataRepository;
            this.mqttLogger = mqttLogger;
            this.operationStatusLogic = operationStatusLogic;
            this.mediator = mediator;
            this.operationConflictRepository = operationConflictRepository;
            this.genericMqttNotification = genericMqttNotification;
            this.utmMessenger = utmMessenger;
			this.replanGeography = replanGeography;
            this.interUssHelper = interUssHelper;
        }

        /// <summary>
        /// Request information regarding Operations
        /// </summary>
        /// <remarks>Allows querying for Operation data.</remarks>
        /// <param name="registration_id">Return only results that match the registration_ids provided.  Only operations that have not completed or have completed within the last 24 hours are required to be returned. If multiple IDs are provided, they must be unique and separated by commas.</param>
        /// <param name="submit_time">A single date-time value that will be used to provide all operations submitted AFTER that time.</param>
        /// <param name="state">Return only operations that are in the states provided.  Comma separated list of states.</param>
        /// <param name="distance">Distance from reference_point to find operations. Ignored if reference_point is not provided.  Units are feet.  Returns all operations that have any operation_volumes that interesect the 2D circle defined by distance and reference_point.  Default value only has meaning when reference_point parameter is provided.</param>
        /// <param name="reference_point">A single point used to find all operations within some distance from that point. Returns all operations that have any operation_volumes that interesect the 2D circle defined by distance and reference_point.  When distance it excluded and reference_point is included, use default value (300ft) for distance. Described as a GeoJSON position.  The value is equivalent to what would be seen in the \&quot;coordinates\&quot; field for a GeoJSON Point object.  See https://tools.ietf.org/html/rfc7946#section-3.1.1 for further reference.  Example would be reference_point&#x3D;[-122.056364, 37.414371] (URL safe: reference_point%3D%5B-122.056364%2C%2037.414371%5D). As per GeoJSON spec, this is long-lat format in the WGS84 reference system. MUST NOT include a third coordinate element, strictly 2D.</param>
        /// <param name="sort">A valid field name to use for sorting records. If multiple fields are provided, the sorting is based on the ordered priorty of that list.</param>
        /// <param name="sort_increasing">For optional use with the &#39;sort&#39; parameter. If &#39;sort&#39; is not provided, then &#39;sort_increasing&#39; will be ignored. Boolean value.  If multiple fields are provided in the &#39;sort&#39; parameter, this boolean value will apply to all of them.</param>
        /// <param name="limit">The maximum number or records to return.</param>
        /// <param name="offset">The index from which to begin the list of returned records.</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Invalid or missing access_token provided.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]        
        [Authorize("uss:read-operation")]
        [Route("/uss/operations")]
        [SwaggerOperation("OperationsGet")]
        [SwaggerResponse(200, typeof(List<Utm.Models.Operation>), "Successful data request. Response includes requested data.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(423, typeof(List<Utm.Models.Operation>), "The requested data are in a transition state.Please retry request.")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        public virtual IActionResult OperationsGet([FromQuery]string registration_id,
            [FromQuery]DateTime? submit_time, [FromQuery]string state,
            [FromQuery]int? distance, [FromQuery]string reference_point,
            [FromQuery]string sort, [FromQuery]bool? sort_increasing,
            [FromQuery]int? limit,
            [FromQuery]int? offset)
        {
            var newOperationList = new List<Utm.Models.Operation>();

            //Distance : Default value 300
            distance = distance.HasValue ? distance : 300;

            //Validate reference point and convert it inot comma seperated string
            string refPoint = string.Empty;

            if(!string.IsNullOrEmpty(reference_point))
            {
                    decimal[] convertedItems = reference_point.StartsWith('[') && reference_point.EndsWith(']') ? Array.ConvertAll(reference_point.Substring(1, reference_point.Length - 2).Split(','), decimal.Parse) : Array.ConvertAll(reference_point.Split(','), decimal.Parse);
                    refPoint = string.Join(',', convertedItems);
            }

            //Check submit Time and convert it to UTC before calculation
            DateTime? submitTime = null;

            if (submit_time.HasValue)
            {                
                submitTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(submit_time.Value, DateTimeKind.Local));
            }

            //Check For State - Valid values are : ACCEPTED, ACTIVATED, CLOSED, NONCONFORMING, ROGUE 
            if (!string.IsNullOrEmpty(state))
            {
                List<string> validStates = new List<string> { "ACCEPTED", "ACTIVATED", "CLOSED", "NONCONFORMING", "ROGUE" };

                var states = new List<string>(state.Split(','));

                states.RemoveAll(x => !validStates.Contains(x.ToString()));

                state = string.Join(',', states);
            }

            //Get Operations based on criteria passed
            var operationList = dbFunctions.GetOperationsByCriteria(registration_id, submit_time,state,distance,refPoint);

            //Get Operation Ids
            List<Guid> opsIds = operationList.Select(o => o.Gufi).ToList();

            //Now Get complete operation details of fetched Ids
            var ops = operationRepository.GetAll().Where(x => opsIds.Contains(x.Gufi)).Adapt<List<Utm.Models.Operation>>();            

            ops.ForEach(operation => {

                if (operation.NegotiationAgreements == null || operation.NegotiationAgreements.Count == 0)
                {
                    operation.NegotiationAgreements = null;
                }

                if (string.IsNullOrEmpty(operation.DiscoveryReference) || string.IsNullOrWhiteSpace(operation.DiscoveryReference))
                {
                    operation.DiscoveryReference = null;
                }

                newOperationList.Add(operation);
            });

            //Check For Sort & Sort_Increasing parameter : Permitted Values : submit_time, gufi, state
            List<string> sortFields = new List<string> { "SubmitTime" }; //Default Sort field is Submit Time            

            if (!string.IsNullOrEmpty(sort))
            {
                //Create Field Mapping as Result field name is not in json format
                //But in the query string sort fields are coming in equivalent json format
                var validSortFields = new List<KeyValuePair<string, string>>();                
                validSortFields.Add(new KeyValuePair<string, string>("gufi", "Gufi"));
                validSortFields.Add(new KeyValuePair<string, string>("state", "State"));

                var fields = new List<string>(sort.Split(','));                
                fields.RemoveAll(x => !validSortFields.Select(z => z.Key).ToList().Contains(x.ToString()));
                
                sortFields.AddRange(validSortFields.FindAll(z => fields.Contains(z.Key.ToString())).Select(x => x.Value));
            }

            //Create sorting expression
            var sortExpressions = Utilities.GetMultiSortExpression(string.Join(',',sortFields), sort_increasing);

            //Final result set
            var result = newOperationList.MultipleSort(sortExpressions).Skip(offset ?? 0).Take(limit ?? 10);                        

            return new ObjectResult(result);
		}

        /// <summary>
        /// Request information regarding Specific Operation
        /// </summary>
        /// <remarks>Allows querying for Operation data.</remarks>
        /// <param name="gufi">Gufi</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Invalid or missing access_token provided.</response>
        [HttpGet, ApiValidationFilter]
        [Route("/uss/operations/{gufi}")]
        [SwaggerOperation(nameof(GetOpByGufi)), SwaggerResponse(200, type: typeof(Utm.Models.Operation))]
        [SwaggerResponse(200, typeof(List<Utm.Models.Operation>), "Ok")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(404, typeof(List<Utm.Models.Operation>), "Resource not found.")]
        [SwaggerResponse(423, typeof(List<Utm.Models.Operation>), "The requested data are in a transition state.Please retry request.")]
        [Authorize("uss:read-operation")]        
        public virtual IActionResult GetOpByGufi([FromRoute]Guid gufi)
        {
            var result = operationRepository.Find(gufi);

            if (result == null || !result.IsInternalOperation)
            {
				return ApiResponse(StatusCodes.Status404NotFound);
			}
            else
            {
                var operation = result.Adapt<Utm.Models.Operation>();
                if (operation.NegotiationAgreements.Count == 0)
                {
                    operation.NegotiationAgreements = null;
                }
				if (string.IsNullOrEmpty(operation.DiscoveryReference) || string.IsNullOrWhiteSpace(operation.DiscoveryReference))
				{
					operation.DiscoveryReference = null;
				}
				return new ObjectResult(operation);
            }
        }


        /// <summary>
        /// Retrieve an operation by GUFI, including details relevant to safety and security
        /// </summary>        
        /// <param name="gufi">Gufi</param>
        [HttpGet, ApiValidationFilter]
        [Route("/uss/enhanced/operations/{gufi}")]
        [SwaggerOperation(nameof(GetOpByGufi)), SwaggerResponse(200, type: typeof(Utm.Models.Operation))]
        [SwaggerResponse(200, typeof(List<Utm.Models.Operation>), "Ok")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(404, typeof(List<Utm.Models.Operation>), "Resource not found.")]
        [SwaggerResponse(423, typeof(List<Utm.Models.Operation>), "The requested data are in a transition state.Please retry request.")]
        [Authorize("uss:read-enhanced-operation")]
        //[Authorize("uss:put-enhanced-operation")]        
        public virtual IActionResult GetEnhancedOpByGufi([FromRoute]Guid gufi)
        {
			var result = operationRepository.Find(gufi);

			if (result == null || !result.IsInternalOperation)
			{
				return ApiResponse(StatusCodes.Status404NotFound);
			}
			else
			{
				var operation = result.Adapt<Utm.Models.Operation>();

				if (operation.NegotiationAgreements.Count == 0)
				{
					operation.NegotiationAgreements = null;
				}
				if (string.IsNullOrEmpty(operation.DiscoveryReference) || string.IsNullOrWhiteSpace(operation.DiscoveryReference))
				{
					operation.DiscoveryReference = null;
				}

				return new ObjectResult(operation);
			}
		}

        /// <summary>
        /// Announce a new, or update an already announced ENHANCED operation.
        /// </summary>        
        /// <param name="gufi">todo: describe gufi parameter on PutOp</param>
        /// <param name="operation">todo: describe operation parameter on PutOp</param>
        /// <remarks>Allows for other USSs to update a previous plan submission.</remarks>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/enhanced/operations/{gufi}")]
        [SwaggerOperation(nameof(PutOp)), SwaggerResponse(200, type: typeof(ObjectResult))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden. Data and or token do not meet security requirements.")]
        [SwaggerResponse(409, typeof(List<Utm.Models.Operation>), "Conflict. This USS Instance has an Operation that intersects.")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-enhanced-operation")]        
        public virtual IActionResult PutEnhancedOp([FromRoute]Guid gufi, [FromBody]Utm.Models.Operation operation)
        {
            if (operation.PriorityElements == null)
            {
                return ApiResponse(403, "Priority Element was null");
            }

            //Non Public safety operation at enhanced endpoint
            if (operation.PriorityElements.PriorityStatus != PriortyStatus.PUBLIC_SAFETY)
            {
                return ApiResponse(403, "Non Public safety operation at enhanced endpoint");
            }

            //Public safety operation and with severity less than CRITICAL at enhanced endpoint
            if (operation.PriorityElements.PriorityStatus == PriortyStatus.PUBLIC_SAFETY &&
                ((int)operation.PriorityElements.PriorityLevel < (int)Severity.CRITICAL))
            {
                return ApiResponse(403, "Public safety operation and with severity less than CRITICAL at enhanced endpoint");
            }

            if (!_operationValidator.IsValid(operation, gufi: gufi))
            {
                mqttLogger.Log("/uss/operations/" + gufi, "Validation Fail: " + string.Join(", ", _operationValidator.Errors));

                return ApiErrorResponse(string.Join(", ", _operationValidator.Errors));
            }

            //Check for valid request
            if (!_operationValidator.IsValidRequest(operation))
            {
                return ApiResponse(403, "Operation update doesn't match original operation.");
            }

            //Save Operation
            var model = SaveOperation(operation);

            //Check If Uss Operator exist locally into DB
            //If Not Save it locally
            var externalUssOperator = interUssHelper.GetOperationUssOperator(operation);

            if (externalUssOperator != null)
            {
                var utmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalUssOperator.Uss) && x.UssBaseCallbackUrl.Equals(externalUssOperator.UssBaseurl)).FirstOrDefault();

                if (utmInstance == null)
                {
                    var instance = model.Adapt<UtmInstance>();
                    instance.UssInstanceId = Guid.NewGuid();
                    instance.UssName = externalUssOperator.Uss;
                    instance.UssBaseCallbackUrl = externalUssOperator.UssBaseurl;
                    instance.TimeAvailableBegin = externalUssOperator.MinimumOperationTimestamp;
                    instance.TimeAvailableEnd = externalUssOperator.MaximumOperationTimestamp;
                    //instance.Id = 0;
                    instance.UssInformationalUrl = string.Empty;
                    instance.UssOpenapiUrl = string.Empty;
                    instance.UssRegistrationUrl = string.Empty;
                    instance.OrganizationId = null;
                    instance.Notes = null;
                    instance.Contact = null;
                    //Set a blank polygon to Coverage Area for external uss
                    instance.CoverageArea = new NpgsqlPolygon(new NpgsqlPoint(0, 0));

                    //Save Data
                    utmInstanceRepository.Add(instance);
                }
            }

            //ViewerMqttNotification.NotifyOperation(model.Adapt<Utm.Models.Operation>()).ConfigureAwait(false);

            if (IsConflicting(operation, out string conflictingOperationGufi))
            {
                return ApiResponse(StatusCodes.Status409Conflict, "Operation conflict detected", conflictingOperationGufi);
            }
            else
            {
                return ApiResponse(StatusCodes.Status204NoContent);
            }            
        }

        /// <summary>
        /// Allow constraints to be submitted from authorized entities.
        /// </summary>
        /// <param name="message_id"></param>
        /// <param name="constraint_message">todo: describe constraint_message parameter on PutConstraint</param>
        /// <response code="201">Constraint data received. </response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/uvrs/{message_id}")]
        [SwaggerOperation(nameof(PutConstraint)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]        
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-constraint")]        
        public IActionResult PutConstraint([FromRoute] Guid message_id, [FromBody]UASVolumeReservation uvr)
        {
            bool isExpireUVRRequest = false;

            if (!_uvrValidator.IsValid(uvr,false, utmInstance : null, message_id: message_id))
            {
                #if DEBUG
                mqttLogger.Log("/uss/uvrs/" + message_id, "Validation Fail: " + string.Join(", ", _uvrValidator.Errors));
                #endif

                return ApiErrorResponse(string.Join(", ", _uvrValidator.Errors));
            }

            var existingConstraintMessage = constraintsRepository.Find(uvr.MessageId);
            
            if (existingConstraintMessage != null)
            {
                //Check for valid request
                if (!existingConstraintMessage.UssName.Equals(uvr.UssName))
                {
                    return ApiResponse(403, "UVR update doesn't match original uvr.");
                }

                uvr.Adapt(existingConstraintMessage);

                // Adding check to see if the incoming uvr request has end datetime in past
                // i.e. it is before current utc datetime, then set "isExpired" flag true
                // to mark this expired.

                if(uvr.EffectiveTimeEnd.Value.Ticks < DateTime.UtcNow.Ticks)
                {
                    existingConstraintMessage.IsExpired = true;
                    isExpireUVRRequest = true;
                }

                constraintsRepository.Update(existingConstraintMessage);                
            }
            else
            {
                var model = uvr.Adapt<ConstraintMessage>();
                model.IsRestriction = true;
                constraintsRepository.Add(model);
            }

            #if DEBUG
            mqttLogger.Log("/uss/constraints/" + message_id, uvr.Adapt<ConstraintMessage>());
            #endif

            if (!isExpireUVRRequest)
            {
                //Notify UVR to ANRA Operators  
                //If existingConstraintMessage is null then new UVR is created if not null then its an update
                mediator.Send(new AsyncConstraintMessageNotification(uvr, existingConstraintMessage==null ? MqttMessageType.CREATED 
                    : MqttMessageType.UPDATED));
            }
            else
            {
                //Notify UVR expiry to ANRA Operators
                //anraMqttClient.PublishTopic(string.Format(MqttTopics.UvrTopic, message_id), existingConstraintMessage);

                //New Mqtt notification for UVR
                ViewerMqttNotification.NotificationToViewer(MqttMessageType.CLOSED.ToString(), uvr, MqttViewerType.UVR);
            }

            return ApiResponse(StatusCodes.Status204NoContent);
        }

        /// <summary>
        /// Allow other USSs to submit a negotiation request
        /// </summary>
        /// <param name="message_id"></param>
        /// <param name="negotiation">The NegotiationMessage data</param>
        /// <param name="negotiation_message">todo: describe negotiation_message parameter on PutNegotiation</param>
        /// <remarks>A POST to the negotiations endpoint is interpretted as a request for alteration in an existing operation managed by the receiving USS.  The requesting USS supplies a GUFI that references the operation that may be altered if the request is granted by the receiving USS.</remarks>
        /// <response code="200">OK</response>
        /// <response code="400">Problem with submitted data</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/negotiations/{message_id}")]
        [SwaggerOperation(nameof(PutNegotiation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(409, typeof(List<Utm.Models.Operation>), "Conflicting operation will not accommodate new operation.")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-message")]        
        public virtual IActionResult PutNegotiation([FromRoute] Guid message_id, [FromBody]Utm.Models.NegotiationMessage negotiation_message)
        {
			if (message_id != negotiation_message.MessageId)
			{
				return ApiResponse(StatusCodes.Status400BadRequest, "The message_id provided did not match with the negotiation message message_id");
			}
			
			var operation = operationRepository.Find(negotiation_message.GufiOfReceiver);

			if (operation == null)
			{
				return ApiResponse(StatusCodes.Status400BadRequest, "Invalid gufi_of_receiver");
			}

			if (negotiation_message.Type.ToString() == "INTERSECTION_REQUESTED")
			{
				if (negotiation_message.ReplanSuggestion != null)
				{
					return ApiResponse(StatusCodes.Status400BadRequest, "Replan Suggestion should be null for type INTERSECTION_REQUESTED");
				}

				if (!(negotiation_message.V2vForOriginator.Value && negotiation_message.V2vForReceiver.Value))
				{
					return ApiResponse(StatusCodes.Status400BadRequest, "V2V is false");
				}

			}

			 //Still testing the functionality

			//var response = new UTMRestResponse();
			//if (negotiation_message.Type.ToString() == "REPLAN_REQUESTED")
			//{
			//	response = replanGeography.GetReplanGeography(negotiation_message);

			//	if (response.HttpStatusCode == 204)
			//	{
			//		//TO-Do Notify about the new operationGeography
			//		anraMqttClient.PublishTopic(string.Format(MqttTopics.NotifyOperationToViewer, negotiation_message.GufiOfReceiver), "OperationGeography has been Updated");
			//	}
			//}
			//if (negotiation_message.Type.ToString() == "INTERSECTION_REQUESTED")
			//{
			//	response = replanGeography.GetReplanGeography(negotiation_message);

			//	if (response.HttpStatusCode == 204)
			//	{
			//		//TO-Do Notify about the new operationGeography
			//		anraMqttClient.PublishTopic(string.Format(MqttTopics.NotifyOperationToViewer, negotiation_message.GufiOfReceiver), "OperationGeography has been Updated");
			//	}
			//}

			var model = negotiation_message.Adapt<Models.NegotiationMessage>();
            negoationMessageRepository.Add(model);


            var topic = string.Format(MqttTopics.OperationNotification, negotiation_message.GufiOfReceiver);

            var mqttMessagePayload = new MqttMessagePayload()
            {
                Type = MqttMessageType.NEGOTIATION.ToString(),
                Message = negotiation_message,
                Topic = $"{anraConfiguration.Product}/{anraConfiguration.Environment}/{topic}"
            };

            anraMqttClient.PublishTopic(topic, mqttMessagePayload);

            #if DEBUG
            mqttLogger.Log("/uss/negotiations/" + message_id, model);
            #endif

            if (negotiation_message.Type.ToString() == "REPLAN_REQUESTED")
			{
				return ApiResponse(StatusCodes.Status409Conflict);
			}
			else
			{
				return ApiResponse(StatusCodes.Status204NoContent);
			}
		}

        /// <summary>
        /// Submit updates to a previously communicated UAS Operation.
        /// </summary>
        /// <param name="externalOperation">Operational plan with the following properties:   1. Contains a valid uss_operation_id.   2. time_available_end value that is in the future.   3. No date-time fields that are in the past are modified.   4. Other rules for a USS Operation POST are satisfied. </param>
        /// <param name="gufi">todo: describe gufi parameter on PutOp</param>
        /// <param name="operation">todo: describe operation parameter on PutOp</param>
        /// <remarks>Allows for other USSs to update a previous plan submission.</remarks>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/operations/{gufi}")]
        [SwaggerOperation(nameof(PutOp)), SwaggerResponse(200, type: typeof(ObjectResult))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(409, typeof(List<Utm.Models.Operation>), "Conflict. This USS Instance has an Operation that intersects.")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-operation")]        
        public virtual IActionResult PutOp([FromRoute]Guid gufi, [FromBody]Utm.Models.Operation operation)
        {
            if (!_operationValidator.IsValid(operation, gufi: gufi))
            {
                #if DEBUG
                mqttLogger.Log("/uss/operations/" + gufi, "Validation Fail: " + string.Join(", ", _operationValidator.Errors));
                #endif

                return ApiErrorResponse(string.Join(", ", _operationValidator.Errors));
            }

            //Check for valid request
            if (!_operationValidator.IsValidRequest(operation))
            {
                return ApiResponse(403, "Operation update doesn't match original operation.");
            }

            if (operation.PriorityElements != null)
            {
                //PUBLIC_SAFETY operation at the non-enhanced operation endpoint
                if (operation.PriorityElements.PriorityStatus == PriortyStatus.PUBLIC_SAFETY)
                {
                    return ApiResponse(403, "PUBLIC_SAFETY operation at the non-enhanced operation endpoint");
                }
            }

            //Save Operation
            var model = SaveOperation(operation);

            //Check If Uss Operator exist locally into DB
            //If Not Save it locally
            var externalUssOperator = interUssHelper.GetOperationUssOperator(operation);

            if(externalUssOperator != null)
            {
                var utmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalUssOperator.Uss) && x.UssBaseCallbackUrl.Equals(externalUssOperator.UssBaseurl)).FirstOrDefault();

                if (utmInstance == null)
                {
                    var instance = model.Adapt<UtmInstance>();
                    instance.UssInstanceId = Guid.NewGuid();
                    instance.UssName = externalUssOperator.Uss;
                    instance.UssBaseCallbackUrl = externalUssOperator.UssBaseurl;
                    instance.TimeAvailableBegin = externalUssOperator.MinimumOperationTimestamp;
                    instance.TimeAvailableEnd = externalUssOperator.MaximumOperationTimestamp;
                    //instance.Id = 0;
                    instance.UssInformationalUrl = string.Empty;
                    instance.UssOpenapiUrl = string.Empty;
                    instance.UssRegistrationUrl = string.Empty;
                    instance.OrganizationId = null;
                    instance.Notes = null;
                    instance.Contact = null;
                    //Set a blank polygon to Coverage Area for external uss
                    instance.CoverageArea = new NpgsqlPolygon(new NpgsqlPoint(0, 0));

                    //Save Data
                    utmInstanceRepository.Add(instance);
                }                
            }

            if (IsConflicting(operation, out string conflictingOperationGufi))
            {
				//In case we already negotiated to intersect then we should return 204 instead of 409 for conflicting operation

				if (operation.NegotiationAgreements != null)
				{
					var negotationAggrementList = operation.NegotiationAgreements.ToList();

					foreach (var negotationAggrement in negotationAggrementList)
					{
						if (negotationAggrement.Type == NegotiationAgreementTypeEnum.INTERSECTION)
						{
							if (negotationAggrement.GufiReceiver.ToString() == conflictingOperationGufi)
							{
								return ApiResponse(StatusCodes.Status204NoContent);
							}
						}
					}
				}

				return ApiResponse(StatusCodes.Status409Conflict, "Conflict. This USS Instance has an Operation that intersects.", conflictingOperationGufi);
            }
            else
            {
                return ApiResponse(StatusCodes.Status204NoContent);
            }
        }

        private bool IsConflicting(Utm.Models.Operation externalOperation, out string conflictingOperationGufi)
        {
            var isConflicting = false;
            conflictingOperationGufi = string.Empty;

            var anraActiveOperations = operationRepository.GetAll()
                                .Where(p => p.State != OperationState.CLOSED.ToString() && p.IsInternalOperation).Adapt<List<Utm.Models.Operation>>();

            var conflictOps = GisHelper.GetIntersectingOperations(externalOperation, anraActiveOperations);

            #if DEBUG
                logger.LogDebug("UssController:: Any Conflicts with operation:{0} ", conflictOps.Any());
            #endif

            if (conflictOps.Any() && conflictOps.Count > 0)
            {
                var conflictingOperation = conflictOps.FirstOrDefault().Key;
				conflictingOperationGufi = conflictingOperation.Gufi.ToString();
                
                #if DEBUG
                    logger.LogDebug("UssController:: External Operation {0} Conflicts with ANRA Operation:{1} ", externalOperation.Gufi, conflictingOperation.Gufi.ToString());
                #endif

                //At this time expecting only one operation to conflict.
                var conflictedOperation = operationRepository.Find(conflictingOperation.Gufi);

                if (externalOperation.IsPrimary(conflictedOperation.Adapt<Utm.Models.Operation>()))
                {
                    #if DEBUG
                        logger.LogDebug("UssController:: EXTERNAL IS PRIMARY Operation {0} ", externalOperation.Gufi);
                    #endif
                    //Sprint 3 - UssInstanceId is not part of operation model so just setting empty guid for less impact on existing implementations.
                    mediator.Send(new AsyncSecondaryOperationNotification(conflictedOperation.Gufi, externalOperation.Gufi, Guid.Empty, externalOperation.UssName));
                }
                else
                {
                    #if DEBUG
                        logger.LogDebug("UssController:: EXTERNAL IS SECONDARY Operation {0} ", externalOperation.Gufi);
                    #endif

                    //Build Mqtt Notification Message For Conflicted Operation
                    var conflictDetailsList = BuildMessageToNotify(externalOperation);                    

                    foreach (var op in conflictOps)
                    {
                        var conflictedOps = operationRepository.Find(op.Key.Gufi);
						operationConflictRepository.Add(new OperationConflict
                        {
                            //OperationId = conflictedOps.OperationId,
                            Gufi = conflictedOps.Gufi,
                            ConflictingGufi = externalOperation.Gufi,
                            ConflictingUssName = externalOperation.UssName,
                            TimeStamp = DateTime.UtcNow,
                            IsAnraPrimary = true
                        });

                        ConflictInformation message = new ConflictInformation()
                        {
                            Message = "Your operation is in conflict with another operation",
                            ConflictDetails = conflictDetailsList
                        };

                        operationStatusLogic.NotifyOperator(conflictedOps, JsonConvert.SerializeObject(message, Formatting.Indented));

                        genericMqttNotification.Send(conflictedOps.UserId, GenericNotificationType.CONFLICT, $"External USS operation {externalOperation.Gufi} conflicted with {conflictedOperation.Gufi}");
                    }

                    isConflicting = true;
                }
            }

            return isConflicting;
        }

        private List<Conflict> BuildMessageToNotify(Utm.Models.Operation operation)
        {
            List<Conflict> conflictDetails = new List<Conflict>();
            foreach (var opsVol in operation.OperationVolumes)
            {
                conflictDetails.Add(
                    new Conflict()
                    {
                        EffectiveTimeBegin = opsVol.EffectiveTimeBegin.Value,
                        EffectiveTimeEnd = opsVol.EffectiveTimeEnd.Value,
                        MaxAltitude = opsVol.MaxAltitude.AltitudeValue,
                        MinAltitude = opsVol.MinAltitude.AltitudeValue,
                        OperationGeography = JsonConvert.SerializeObject(opsVol.OperationGeography),
                        Ordinal = opsVol.Ordinal
                    });
            }
            return conflictDetails;
        }

        private Models.Operation SaveOperation(Utm.Models.Operation externalOperation)
        {
            var model = operationRepository.Find(externalOperation.Gufi);
            if (model == null)
            {
                model = externalOperation.Adapt<Models.Operation>();
                model.IsInternalOperation = false;
                operationRepository.Add(model);

                #if DEBUG
                    logger.LogDebug("UssController:: SaveOperation {0} ", JsonConvert.SerializeObject(model.Adapt<OperationViewModel>()));
                #endif

                //Old Mqtt Viewer Notification
                ViewerMqttNotification.NotifyOperation(model.Adapt<OperationViewModel>()).ConfigureAwait(false);

                //New Mqtt viewer Notification
                ViewerMqttNotification.NotificationToViewer(MqttMessageType.CREATED.ToString(), model.Adapt<OperationViewModel>(), MqttViewerType.OPERATION);
            }
            else
            {
                model = externalOperation.Adapt(model);
                operationRepository.Update(model);
                ViewerMqttNotification.NotifyOperation(model.Adapt<OperationViewModel>()).ConfigureAwait(false);

                //New Mqtt viewer Notification
                ViewerMqttNotification.NotificationToViewer(MqttMessageType.UPDATED.ToString(), model.Adapt<OperationViewModel>(), MqttViewerType.OPERATION);
            }
            return model;
        }

        /// <summary>
        /// Allow other authorized entities to send a message to this USS.
        /// </summary>
        /// <param name="message_id"></param>
        /// <param name="message">The message being sent</param>
        /// <param name="utm_message">todo: describe utm_message parameter on PostUtmMessageAsync</param>
        /// <remarks>Allows another USS or FIMS to POST a message to this USS.</remarks>
        /// <response code="204">Message data received.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Invalid or missing access_token provided.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/utm_messages/{message_id}")]
        [SwaggerOperation("PuttUtmMessage"), SwaggerResponse(200, type: typeof(HttpStatusCode))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]        
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-message")]        
        public IActionResult PostUtmMessageAsync([FromRoute]Guid message_id, [FromBody]UTMMessage utm_message)
        {
            if (!_utmMessageValidator.IsValid(utm_message, message_id: message_id))
            {
                #if DEBUG
                mqttLogger.Log("/uss/utm_messages/" + utm_message.MessageId, "Validation Fail: " + string.Join(", ", _utmMessageValidator.Errors));
                #endif

                return ApiErrorResponse(string.Join(", ", _utmMessageValidator.Errors));
            }

            //Check For Valid Request
            if (!_utmMessageValidator.IsValidRequest(utm_message))
            {
                return ApiResponse(403, "Receiving message for an operation doesn't match original operation");
            }

            var model = utm_message.Adapt<UtmMessage>();
            utmMessageRepository.Add(model);
            
            ViewerMqttNotification.NotifyUtmMessage(utm_message).ConfigureAwait(false);


            //If Incoming Utm Message is related to Operation then only update operation entity
            if(utm_message.MessageType.Equals(MessageTypeEnum.OPERATION_NONCONFORMING) ||
                utm_message.MessageType.Equals(MessageTypeEnum.OPERATION_ROGUE) ||
                utm_message.MessageType.Equals(MessageTypeEnum.OPERATION_CONFORMING) ||
                utm_message.MessageType.Equals(MessageTypeEnum.OPERATION_CLOSED))
            {
                UpdateExternalOperation(utm_message.Gufi, utm_message.MessageType);
            }                

            return ApiResponse(StatusCodes.Status204NoContent);
        }

        private async Task AutoRequestPositionReport(Models.Operation operation, MessageTypeEnum messageType)
        {          
            var originatorUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(anraConfiguration.ClientId)).FirstOrDefault();
            var externalUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(operation.UssName)).FirstOrDefault();

            string freeText = string.Empty;

            if (messageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_START))
            {
                freeText = string.Format("Please send periodic position reports for operation (Gufi - {0}).",operation.Gufi.ToString());
            }

            if (messageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_END))
            {
                freeText = string.Format("Please stop periodic position reports for operation (Gufi - {0}).", operation.Gufi.ToString());
            }

            if (originatorUtmInstance != null && externalUtmInstance != null)
            {
                var message = new UTMMessage
                {
                    MessageId = Guid.NewGuid(),
                    Gufi = operation.Gufi,
                    UssName = originatorUtmInstance.UssName,
                    SentTime = DateTime.UtcNow,
                    Severity = Severity.NOTICE,
                    MessageType = messageType,
                    Callback = originatorUtmInstance.UssBaseCallbackUrl,
                    FreeText = freeText
                };

                var model = message.Adapt<UtmMessage>();
                utmMessageRepository.Add(model);

                //create GridCellOperatorResponse object manually
                var ussOperator = new GridCellOperatorResponse {
                    Uss = externalUtmInstance.UssName,
                    UssBaseurl = externalUtmInstance.UssBaseCallbackUrl,
                    Version = 0,
                    Timestamp = DateTime.UtcNow.ToString(),
                    MinimumOperationTimestamp = DateTime.UtcNow,
                    MaximumOperationTimestamp = DateTime.UtcNow,
                    AnnouncementLevel=AnnouncementLevelEnum.ALL,
                    Zoom=0,
                    X=0,
                    Y=0,
                    Operations = new List<GridCellOperationResponse>()
                };

                mediator.Send(new AsyncUtmMessageNotification(message, new List<GridCellOperatorResponse> { ussOperator }));
            }
        }

        private void UpdateExternalOperation(Guid gufi, MessageTypeEnum messageType)
        {
            var operation = operationRepository.GetAll().Where(p => p.Gufi == gufi).FirstOrDefault();
            if (operation != null)
            {
                switch (messageType)
                {
                    case MessageTypeEnum.OPERATION_NONCONFORMING:
                        operation.State = OperationState.NONCONFORMING.ToString();
                        break;

                    case MessageTypeEnum.OPERATION_ROGUE:
                        operation.State = OperationState.ROGUE.ToString();
                        break;

                    case MessageTypeEnum.OPERATION_CONFORMING:
                        operation.State = OperationState.ACTIVATED.ToString();
                        break;

                    case MessageTypeEnum.OPERATION_CLOSED:
                        operation.State = OperationState.CLOSED.ToString();

                        //New Mqtt viewer Notification for external operation closed.
                        ViewerMqttNotification.NotificationToViewer(MqttMessageType.CLOSED.ToString(), operation.Adapt<OperationViewModel>(), MqttViewerType.OPERATION);
                        break;
                }

                operationRepository.Update(operation);

                if (anraConfiguration.SendAutoPositionRequest)
                {
                    //Send Start Position Request If Operation State is ROGUE/NONCONFORMING

                    if (operation.State.Equals(OperationState.NONCONFORMING.ToString()) || operation.State.Equals(OperationState.ROGUE.ToString()))
                    {
                        var utmMessage = utmMessageRepository.GetAll().Where(p => p.Gufi == operation.Gufi && p.MessageType.StartsWith("PERIODIC_POSITION_REPORTS", StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(x => x.SentTime).FirstOrDefault();

                        if (utmMessage == null)
                        {
                            AutoRequestPositionReport(operation, MessageTypeEnum.PERIODIC_POSITION_REPORTS_START).ConfigureAwait(false);
                        }
                        else
                        {
                            if (utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_END.ToString()))
                            {
                                AutoRequestPositionReport(operation, MessageTypeEnum.PERIODIC_POSITION_REPORTS_START).ConfigureAwait(false);
                            }
                        }
                    }

                    if (operation.State.Equals(OperationState.ACTIVATED.ToString()) || operation.State.Equals(OperationState.CLOSED.ToString()))
                    {
                        var utmMessage = utmMessageRepository.GetAll().Where(p => p.Gufi == operation.Gufi && p.MessageType.StartsWith("PERIODIC_POSITION_REPORTS", StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(x => x.SentTime).FirstOrDefault();

                        if (utmMessage != null && utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_START.ToString()))
                        {
                            AutoRequestPositionReport(operation, MessageTypeEnum.PERIODIC_POSITION_REPORTS_END).ConfigureAwait(false);
                        }
                    }
                }

                //Notify to Operators
                var notification = new OperationNotification
                {
                    Gufi = operation.Gufi,
                    Message = $"Operation state changed to {operation.State}.",
                    Status = operation.State.ToString(),
                    Timestamp = DateTime.UtcNow
                };

                ViewerMqttNotification.NotifyOperationStatus(notification).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Share USS Instance data
        /// </summary>
        /// <param name="uss_instance_id"></param>
        /// <param name="message">The USS Instance information being sent</param>
        /// <param name="uss_instance">todo: describe uss_instance parameter on UssPut</param>
        /// <remarks>As per the USS Discovery Service specification, this endpoint is for receiving messages about the USS Network from the USS Discovery Service.  For example, when a new USS Instance is registered, that announcement would come to this endpoint.</remarks>
        /// <response code="204">USS Instance data received.</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/uss/{uss_instance_id}")]
        [SwaggerOperation(nameof(UssPut)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-uss")]        
        public virtual IActionResult UssPut([FromRoute] Guid uss_instance_id, [FromBody]UssInstance uss_instance)
        {
			if (uss_instance_id != uss_instance.UssInstanceId)
			{
				return ApiResponse(StatusCodes.Status400BadRequest, "The uss_instance_id provided did not match with UssInstance uss_instance_id ");
			}

            Uri uriResult;
            var isValidCallback = Uri.TryCreate(uss_instance.UssBaseCallbackUrl, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!isValidCallback)
            {
                return ApiErrorResponse("Invalid callback url!");
            }

            var existingModel = utmInstanceRepository.Find(uss_instance.UssInstanceId);

            if (existingModel != null)
            {
                uss_instance.Adapt(existingModel);
                utmInstanceRepository.Update(existingModel);
            }
            else
            {
                var model = uss_instance.Adapt<UtmInstance>();
                utmInstanceRepository.Add(model);
            }

            return ApiResponse(StatusCodes.Status204NoContent);
        }

        /// <summary>
        /// For submitting position data from another USS.
        /// </summary>
        /// <remarks>For submitting position data for another USS operation.</remarks>
        /// <param name="positionId"></param>
        /// <param name="position"></param>
        /// <response code="201">Message data received.</response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/positions/{position_id}")]
        [SwaggerResponse(204, typeof(List<Utm.Models.Operation>), "Operation data received. No content returned.")]
        [SwaggerResponse(400, typeof(List<Utm.Models.Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Utm.Models.Operation>), "Invalid or missing access_token provided.")]
        [SwaggerResponse(403, typeof(List<Utm.Models.Operation>), "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?")]
        [SwaggerResponse(429, typeof(List<Utm.Models.Operation>), "Too many recent requests from you. Wait to make further queries.")]
        [Authorize("uss:put-position")]        
        public virtual IActionResult PositionMessage([FromRoute]Guid position_id, [FromBody] Position position)
        {
			if (!_positionValidator.IsValid(position, position_id: position_id))
            {
                #if DEBUG
                mqttLogger.Log("/uss/positions/" + position.EnroutePositionsId, "Validation Fail: " + string.Join(", ", _positionValidator.Errors));
                #endif

                return ApiErrorResponse(string.Join(", ", _positionValidator.Errors));
            }

            //Check For Valid Request
            if (!_positionValidator.IsValidRequest(position))
            {
                return ApiResponse(403, "Receiving position for an operation doesn't match original operation");
            }

            anraMqttClient.PublishTopic(string.Format(MqttTopics.ExternalTelementary, position.Gufi), position);

            ViewerMqttNotification.NotifyPositionMessage(position).ConfigureAwait(false);

            return ApiResponse(StatusCodes.Status204NoContent);
        }

        [HttpGet("/Error"), HttpPut("/Error"), HttpPost("/Error")]
        public IActionResult Error([FromQuery] int status = 400)
        {
            JsonResult result = null;

			if (status == 401)
			{
				result = new JsonResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Unauthorized, Message = "Invalid or missing access_token provided." });
				result.StatusCode = 401;
				return result;
			}
			else if (status == 403)
			{
				result = new JsonResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = "Forbidden.Do not retry with same access token.Reason not provided, but do you have the right scopes?" });
				result.StatusCode = 403;
				return result;
			}
			else if (status == 404)
			{
				result = new JsonResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NotFound, Message = "Resource not found." });
				result.StatusCode = 404;
				return result;
			}
			else if (status == 409)
			{
				result = new JsonResult(new UTMRestResponse { HttpStatusCode = 409, Message = "Conflicting operation" });
				result.StatusCode = 409;
				return result;
			}
			else if (status == 429)
			{
				result = new JsonResult(new UTMRestResponse { HttpStatusCode = 429, Message = "Too many recent requests from you. Wait to make further queries." });
				result.StatusCode = 429;
				return result;
			}
			else
			{
				result = new JsonResult(new { error = "UNDEFINED_ERROR" });
				result.StatusCode = 500;
				return result;
			}
        }
    }
}