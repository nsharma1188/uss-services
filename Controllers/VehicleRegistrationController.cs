﻿using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AnraUssServices.Controllers
{
    [Route(@"api/[controller]")]
    public class VehicleRegistrationController : Controller
    {
        private UssVehicleRegistrationApi ussVehicleRegistrationApi;

        public VehicleRegistrationController(UssVehicleRegistrationApi ussVehicleRegistrationApi)
        {
            this.ussVehicleRegistrationApi = ussVehicleRegistrationApi;
        }

        /// <summary>
        /// Returns information on vehicle with the given uvin.
        /// </summary>
        /// <remarks>Returns information on vehicle with the given uvin.</remarks>
        /// <param name="uvin">Specifies the vehicle id</param>
        /// <param name="accept">Must be &#39;application/json&#39;</param>
        /// <param name="xUTMAuth">Keyed-hash message authentication code obtained from the vehicle owner.  The purpose of this code is to ensure that uvin submitted was registered by the owner and the owner is not being impersonated. If the USS has PUBLIC_SAFETY scope - this field is not needed. </param>
        /// <response code="200">Successfully returned uvin and its associated vehicle information</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <response code="503">Service Unavailable</response>
        [HttpGet]
        [Authorize]
        [Route(@"/api/uvins/{uvin}")]
        [SwaggerOperation("UvinsUvinGet")]
        [SwaggerResponse(200, typeof(VehicleData), "Successfully returned uvin and its associated vehicle information")]
        [SwaggerResponse(400, typeof(VehicleData), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(VehicleData), "Unauthorized")]
        [SwaggerResponse(403, typeof(VehicleData), "Forbidden")]
        [SwaggerResponse(404, typeof(VehicleData), "Not Found")]
        [SwaggerResponse(503, typeof(VehicleData), "Service Unavailable")]
        public IActionResult UvinsUvinGet([FromRoute]string uvin, [FromHeader]string accept, [FromHeader]string xUTMAuth)
        {
            if (!accept.Contains("application/json"))
            {
                UTMRestResponse restResponse = new UTMRestResponse();
                restResponse.HttpStatusCode = 400;
                restResponse.Message = "Bad request. Typically validation error. Fix your request and retry.";
                restResponse.Id = uvin;
                return new ObjectResult(restResponse);
            }

            VehicleReg v = ussVehicleRegistrationApi.GetVehicleData(uvin, xUTMAuth);

            if (v != null)
            {
                return new ObjectResult(v);
            }
            else
            {
                UTMRestResponse restResponse = new UTMRestResponse();
                restResponse.HttpStatusCode = 404;
                restResponse.Message = "Not Found";
                restResponse.Id = uvin;
                return new ObjectResult(restResponse);
            }
        }

        /// <summary>
        /// Returns information on vehicle with the given uvin.
        /// </summary>
        /// <remarks>USS registers a vehicele on behalf of a vehicle owner</remarks>
        /// <param name="accept">Must be &#39;application/json&#39;</param>
        /// <param name="xUTMAuth">Keyed-hash message authentication code obtained from the vehicle owner.  The purpose of this code is to ensure that the owner is not being impersonated.</param>
        /// <response code="201">Successfully registration of a vehicle</response>
        [HttpPost]
        [Authorize]
        [Route(@"/api/uvins")]
        [SwaggerOperation("UvinsPost")]
        [SwaggerResponse(200, typeof(UTMRestResponse), "Successfully registered.")]
        public IActionResult UvinsPost([FromHeader]string accept, [FromHeader]string xUTMAuth) //X-UTM-Auth
        {
            if (!accept.Contains("application/json"))
            {
                UTMRestResponse restResponse = new UTMRestResponse();
                restResponse.HttpStatusCode = 400;
                restResponse.Message = "Bad request. Typically validation error. Fix your request and retry.";
                return new ObjectResult(restResponse);
            }

            VehiclePost vP = ussVehicleRegistrationApi.registerVehicle(xUTMAuth);

            if (vP != null)
            {
                return new ObjectResult(vP);
            }

            UTMRestResponse uTMRestResponse = new UTMRestResponse();
            uTMRestResponse.Message = "Not Registered";
            uTMRestResponse.HttpStatusCode = 404;

            return new ObjectResult(uTMRestResponse);
        }

        /// <summary>
        ///
        /// </summary>
        /// <remarks>Returns information on vehicle with the given uvin</remarks>
        /// <param name="uvin">Specifies the vehicle id</param>
        /// <param name="accept">Must be &#39;application/json&#39;</param>
        /// <response code="200">Successfully registration of a vehicle</response>
        [HttpGet]
        [Route("/api/orgs/{uvin}")]
        [Authorize]
        [SwaggerOperation("OrgsUvinGet")]
        [SwaggerResponse(200, typeof(OrgData), "Successfully registration of a vehicle")]
        public virtual IActionResult OrgsUvinGet([FromRoute]string uvin, [FromHeader]string accept)
        {
            if (!accept.Contains("application/json"))
            {
                UTMRestResponse restResponse = new UTMRestResponse();
                restResponse.HttpStatusCode = 400;
                restResponse.Message = "Bad request. Typically validation error. Fix your request and retry.";
                restResponse.Id = uvin;
                return new ObjectResult(restResponse);
            }

            string exampleJson = null;

            var example = exampleJson != null
            ? JsonConvert.DeserializeObject<OrgData>(exampleJson)
            : default(OrgData);
            return new ObjectResult(example);
        }
    }
}