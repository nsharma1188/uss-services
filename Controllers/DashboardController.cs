﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.Dashboard;
using AnraUssServices.ViewModel.UtmInstances;
using AnraUssServices.ViewModel.ConstraintMessages;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Dashboard Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DashboardController : BaseController
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepository<Drone> droneRepository;
        private readonly IRepository<LocalNetwork> localNetworkRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly IRepository<Models.OperationVolume> operationVolumeRepository;
        private readonly IRepository<Models.PriorityElement> priorityElementRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<Models.ConstraintMessage> constraintsRepository;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ApplicationDbContext _context;
        private AnraConfiguration _config;

        public DashboardController(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            FimsTokenProvider fimsAuthentication,
            AnraConfiguration configuration,
            IRepository<Drone> droneRepository,
            IRepository<LocalNetwork> localNetworkRepository,
            IRepository<Models.Operation> operationRepository,
            IRepository<Models.OperationVolume> operationVolumeRepository,
            IRepository<Models.PriorityElement> priorityElementRepository,
            IRepository<Models.ConstraintMessage> constraintsRepository,
            IRepository<UtmInstance> utmInstanceRepository)
        {
            _context = context;
            _config = configuration;
            this.fimsAuthentication = fimsAuthentication;
            this.httpContextAccessor = httpContextAccessor;
            this.droneRepository = droneRepository;
            this.localNetworkRepository = localNetworkRepository;
            this.operationRepository = operationRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.operationVolumeRepository = operationVolumeRepository;
            this.priorityElementRepository = priorityElementRepository;
            this.constraintsRepository = constraintsRepository;
        }

        /// <summary>
        /// Request information regarding Drones
        /// </summary>
        /// <param name="userid">todo: describe userid parameter on GetDashboardItems</param>
        /// <remarks>Allows querying for Drone data.</remarks>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-dashboard")]        
        [Route("/dashboard/listing/{userid}")]
        [SwaggerOperation("GetDashboardItems"), SwaggerResponse(200, type: typeof(DashboardItem))]
        public virtual IActionResult GetDashboardItems([FromRoute]string userid)
        {
            var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();
            var dashboard = new DashboardItem();

            var dronesList = droneRepository.GetAll().Where(x => x.IsActive && !x.IsDeleted).ToList();
            var operationsList = operationRepository.GetAll().Where(x => x.State == OperationState.ACTIVATED.ToString() && x.UssName == _config.ClientId).ToList();

            //USS Instances
            var result = utmInstanceRepository.GetAll().OrderByDescending(x => x.DateCreated)
                .Where(p => p.TimeAvailableEnd.HasValue && p.TimeAvailableEnd >= DateTime.UtcNow).ToList();

            //ANRA USS Instances
            var anraUtms = result.FindAll(x => !string.IsNullOrEmpty(x.UssName) && x.UssName.Equals(_config.ClientId)).ToList();
            var activeUvrs = constraintsRepository.GetAll().Where(x => x.IsExpired.Equals(false)).ToList();

            //If SuperAdmin show everything
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                dashboard.TotalDrones = dronesList.Count;
                dashboard.TotalOperations = operationsList.Count;
                dashboard.TotalActiveUSSNetwork = result.Count;
                dashboard.TotalUSSInstances = anraUtms.Count;
                dashboard.TotalActiveUvrs = activeUvrs.Count;
            }
            //If ANSP logged in 
            else if(CheckRole(EnumUserRole.ANSP.ToString()))
            {
                dashboard.TotalDrones = dronesList.Where(x=>x.GroupId == groupId).ToList().Count;
                dashboard.TotalOperations = operationsList.Where(x => x.GroupId == groupId).ToList().Count;
                dashboard.TotalActiveUSSNetwork = result.Count;
                dashboard.TotalUSSInstances = anraUtms.Where(x => x.GroupId == groupId).ToList().Count;
                dashboard.TotalActiveUvrs = activeUvrs.Where(x => x.GroupId == groupId).ToList().Count;
            }
            else
            {
                dashboard.TotalDrones = dronesList.Where(x => x.OrganizationId == organizationId).ToList().Count;
                dashboard.TotalOperations = operationsList.Where(x => x.OrganizationId == organizationId).ToList().Count;
                dashboard.TotalActiveUSSNetwork = result.Count;
                dashboard.TotalUSSInstances = anraUtms.Where(x => x.GroupId == groupId).ToList().Count;
                dashboard.TotalActiveUvrs = activeUvrs.Where(x => x.GroupId == groupId).ToList().Count;

            }

            return new ObjectResult(dashboard);
        }

        /// <summary>
        /// Request information regarding Flight Summary
        /// </summary>
        /// <param name="userid">todo: describe userid parameter on GetFlightSummary</param>
        [HttpGet]
        [Authorize("uss:read-dashboard")]        
        [Route("/dashboard/flightsummary/{userid}")]
        [SwaggerOperation("GetFlightSummary"), SwaggerResponse(200, type: typeof(List<FlightSummary>))]
        public virtual IActionResult GetFlightSummary([FromRoute]string userid)
        {
            var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();

            //Get First Date & Last Date Of Current Year
            var firstDay = new DateTime(DateTime.Now.Year, 1, 1);
            var lastDay = new DateTime(DateTime.Now.Year, 12, 31);

            //First Initialize FlightSummary With Empty Data For All Months
            var lstFlightSummary = new List<FlightSummary>();

            for (var i = 1; i <= 12; i++)
            {
                var flight = new FlightSummary
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i)
                };
                lstFlightSummary.Add(flight);
            }

            var operationsList = new List<Operation>();

            //Fetch the operation based on the loggedin user
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation && x.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED))).ToList();
            }
            else if (CheckRole(EnumUserRole.ANSP.ToString()))
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation && x.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED)))
                    .Where(x => x.GroupId == groupId).ToList();
            }
            else
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation && x.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED)))
                    .Where(x => x.OrganizationId == organizationId).ToList();
            }

            //Fetch Operation Volumes
            var opsVolumes = operationsList
                .OrderBy(x => x.Gufi)
                .Select(x => x.OperationVolumes.ToList()).ToList();

            //Filter volumes based on conditions
            var filteredVolumes = new List<OperationVolume>();

            opsVolumes.ToList().ForEach(vol => {
                vol.ForEach(x => {
                    if (x.EffectiveTimeBegin.Date >= firstDay && x.EffectiveTimeEnd.Date <= lastDay)
                        filteredVolumes.Add(x);
                });
            });

            //Flight Summary Graph Data
            var flightSummaryList = filteredVolumes
                .GroupBy(x => new { Month = x.EffectiveTimeBegin.Month })
                .ToList()
                .Select(g => new FlightSummary
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(g.Key.Month),
                    TotalFlights = g.GroupBy(x => x.Gufi).Count(),
                    TotalHours = g.Sum(x => Convert.ToDecimal(x.EffectiveTimeEnd.Subtract(x.EffectiveTimeBegin).TotalHours))
                });

            //Update TotalFlights Value
            lstFlightSummary.ForEach(x => x.TotalFlights = (flightSummaryList.FirstOrDefault(v => v.Month == x.Month) == null ? 0 : flightSummaryList.FirstOrDefault(v => v.Month == x.Month).TotalFlights));

            //Update TotalHours Value
            lstFlightSummary.ForEach(x => x.TotalHours = (flightSummaryList.FirstOrDefault(v => v.Month == x.Month) == null ? 0 : Math.Round((flightSummaryList.FirstOrDefault(v => v.Month == x.Month).TotalHours), 2)));

            return new ObjectResult(lstFlightSummary);
        }

        /// <summary>
        /// Request information regarding Operational Summary
        /// </summary>
        [HttpGet]
        [Authorize("uss:read-dashboard")]        
        [Route("/dashboard/operationalsummary/{userid}")]
        [SwaggerOperation("GetOperationalSummary"), SwaggerResponse(200, type: typeof(List<OperationalSummary>))]
        public virtual IActionResult GetOperationalSummary([FromRoute]string userid)
        {
            var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();

			bool filterOperation(Models.Operation o) => CheckRole(EnumUserRole.SUPERADMIN.ToString()) ? true : o.OrganizationId.Equals(organizationId);

            //Get First Date & Last Date Of Current Year
            var firstDay = new DateTime(DateTime.Now.Year, 1, 1);
            var lastDay = new DateTime(DateTime.Now.Year, 12, 31);

            //First Initialize FlightSummary With Empty Data For All Months
            var lstOperationalSummary = new List<OperationalSummary>();

            for (var i = 1; i <= 12; i++)
            {
                var operationalSummary = new OperationalSummary
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i)
                };
                lstOperationalSummary.Add(operationalSummary);
            }

            var operationsList = new List<Operation>();

            //Fetch the operation based on the loggedin user
            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation).ToList();
            }
            else if (CheckRole(EnumUserRole.ANSP.ToString()))
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation && x.GroupId == groupId).ToList();
            }
            else
            {
                operationsList = operationRepository
                    .GetAll()
                    .Where(x => x.IsInternalOperation && x.OrganizationId == organizationId).ToList();
            }

            //Fetch Priority Elements
            var priorityElements = operationsList.Select(x => x.PriorityElements).OfType<PriorityElement>();

            //Filter volumes based on conditions
            var filteredPriorityElements = new List<PriorityElement>();
            priorityElements.ToList().ForEach(v => {
                if (v.DateCreated.Date >= firstDay && v.DateCreated.Date <= lastDay)
                {
                    filteredPriorityElements.Add(v);
                }                        
            });

            //Operational Summary Graph Data
            var operationalSummaryList = filteredPriorityElements                
                .GroupBy(x => new { Month = x.DateCreated.Month })
                .ToList()
                .Select(g => new OperationalSummary
                {
                    Month = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(g.Key.Month),
                    TotalNoneOps = g.Where(x => x.PriorityStatus == EnumUtils.GetDescription(PriortyStatus.NONE)).Count(),
                    TotalPublicSafteyOps = g.Where(x => x.PriorityStatus == EnumUtils.GetDescription(PriortyStatus.PUBLIC_SAFETY)).Count(),
                    TotalInflightEmergencyOps = g.Where(x => x.PriorityStatus.Contains("EMERGENCY")).Count()
                });

            //Update TotalNoneOps Value
            lstOperationalSummary.ForEach(x => x.TotalNoneOps = (operationalSummaryList.FirstOrDefault(v => v.Month == x.Month) == null ? 0 : operationalSummaryList.FirstOrDefault(v => v.Month == x.Month).TotalNoneOps));

            //Update TotalPublicSafteyOps Value
            lstOperationalSummary.ForEach(x => x.TotalPublicSafteyOps = (operationalSummaryList.FirstOrDefault(v => v.Month == x.Month) == null ? 0 : operationalSummaryList.FirstOrDefault(v => v.Month == x.Month).TotalPublicSafteyOps));

            //Update TotalInflightEmergencyOps Value
            lstOperationalSummary.ForEach(x => x.TotalInflightEmergencyOps = (operationalSummaryList.FirstOrDefault(v => v.Month == x.Month) == null ? 0 : operationalSummaryList.FirstOrDefault(v => v.Month == x.Month).TotalInflightEmergencyOps));

            return new ObjectResult(lstOperationalSummary);
        }
    }
}