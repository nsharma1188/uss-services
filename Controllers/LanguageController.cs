﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Language;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json.Linq;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Language Interface
    /// </summary>
    [Route(@"api/[controller]")]
    public class LanguageController : BaseController
    {
        private readonly ILogger<LanguageController> logger;
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Language> languageRepository;
        //private readonly IRepository<LocaleStringResource> localeStringResourceRepository;
        private readonly IRepository<LanguageResource> languageResourceRepository;
        private AnraConfiguration _config;

        public LanguageController(ApplicationDbContext context,
            ILogger<LanguageController> logger,
            IRepository<Language> languageRepository,
            //IRepository<LocaleStringResource> localeStringResourceRepository,
            IRepository<LanguageResource> languageResourceRepository,
            AnraConfiguration configuration)
        {
            _context = context;
            _config = configuration;
            this.languageRepository = languageRepository;
            //this.localeStringResourceRepository = localeStringResourceRepository;
            this.languageResourceRepository = languageResourceRepository;
            this.logger = logger;
        }

        /// <summary>
        /// Request information regarding Languages
        /// </summary>
        /// <remarks>Returns list of Languages.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        //[Authorize]        
        [Route(@"/language/listing")]
        [SwaggerOperation(@"GetLanguageList"), SwaggerResponse(200, type: typeof(List<Language>))]
        public virtual IActionResult GetLanguageList()
        {            
            var result = languageRepository.GetAll().OrderBy(x => x.DisplayOrder).Adapt<List<Language>>();
            
            return new ObjectResult(result);
        }

        /// <summary>
        /// Create new language.
        /// </summary>
        /// <remarks>Create new language.</remarks>
        /// <param name="languageData">The Language data</param>
        /// <response code="201">Language data received.</response>
        /// <response code="400">Bad request. Language Name exist, use another name.</response>        
        [HttpPost, ApiValidationFilter]
        [Route(@"/language/save")]
        //[Authorize]
        [SwaggerOperation(@"CreateLanguage"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CreateLanguage([FromBody]LanguageItem languageData)
        {
            try
            {
                //Name check
                if (LanguageNameCheck(languageData.Name) != null) return LanguageNameCheck(languageData.Name);

                LanguageDisplayOrder(languageData.DisplayOrder);

                var model = languageData.Adapt<LanguageItem, Language>();
                model.LanguageGufi = Guid.NewGuid();
                string producedId = model.LanguageGufi.ToString();

                languageRepository.Add(model);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Created, Message = "Language created.", Id = producedId });
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }
        }

        /// <summary>
        /// Update language details.
        /// </summary>
        /// <remarks>Update language details.</remarks>
        /// <param name="languageData">The Language data</param>
        /// <param name="languageGufi">The Language Gufi</param>
        /// <response code="200">Language data updated.</response>
        /// <response code="400">Bad request. Language Name exist, use another name.</response>
        /// <response code="204">Data not found by Id, check with the server administrator.</response>
        [HttpPut, ApiValidationFilter]
        [Route(@"/language/update/{languageGufi}")]
        //[Authorize]
        [SwaggerOperation(@"UpdateLanguageDetails"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult UpdateLanguageDetails([FromBody]LanguageItem languageData, [FromRoute] Guid languageGufi)
        {
            var modelLanguage = languageRepository.Find(languageGufi);

            //Name check
            if(modelLanguage.Name != languageData.Name)
            {
                if (LanguageNameCheck(languageData.Name) != null) return LanguageNameCheck(languageData.Name);
            }
            
            try
            {
                if (modelLanguage != null)
                {
                    //Display order set
                    if (modelLanguage.DisplayOrder != languageData.DisplayOrder) LanguageDisplayOrder(languageData.DisplayOrder);

                    languageData.Adapt(modelLanguage);

                    languageRepository.Update(modelLanguage);

                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Language details updated.", Id = languageGufi.ToString() });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NoContent, Message = "No data found.", Id = languageGufi.ToString() });
                }
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }
            
        }

        /// <summary>
        /// Delete a Language
        /// </summary>
        /// <param name="languageGufi"></param>
        /// <remarks>Allows deleting a language.</remarks>
        /// <response code="200">Successful data deleted.</response>
        /// <response code="204">Data not found by Id, check with the server administrator.</response>
        [HttpDelete]
        //[Authorize]
        [Route("/language/{languageGufi}")]
        [SwaggerOperation(@"DeleteLanguage"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult DeleteLanguage([FromRoute] Guid languageGufi)
        {
            try
            {
                var model = languageRepository.Find(languageGufi);

                if (model != null)
                {
                    languageRepository.Remove(languageGufi);
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Language deleted.", Id = languageGufi.ToString() });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NoContent, Message = "No data found.", Id = languageGufi.ToString() });
                }
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }

        }

        /// <summary>
        /// Request information regarding Resources of a language
        /// </summary>
        /// <remarks>Returns list of Resources of requested Language.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        //[Authorize]
        [Route(@"/language/resource/{languageGufi}")]
        //[SwaggerOperation(@"GetLanguageResources"), SwaggerResponse(200, type: typeof(List<LocaleStringResource>))]
        [SwaggerOperation(@"GetLanguageResources"), SwaggerResponse(200, type: typeof(List<LanguageResource>))]
        public virtual IActionResult GetLanguageResources([FromRoute] Guid languageGufi)
        {
            var language = languageRepository.Find(languageGufi);

            if (language != null)
            {
                //var result = localeStringResourceRepository.GetAll().Where(x => x.LanguageId.Equals(language.LanguageId)).Adapt<List<LocaleStringResource>>();
                var result = languageResourceRepository.GetAll().Where(x => x.LanguageGufi.Equals(language.LanguageGufi)).Adapt<List<LanguageResource>>();

                return new ObjectResult(result);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"No data found.", Id = languageGufi.ToString() });
            }
        }

        /// <summary>
        /// Simple information regarding Resources of a language
        /// </summary>
        /// <remarks>Returns simple list of Resources of requested Language.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        [HttpGet]
        //[Authorize]
        [Route(@"/language/simpleresource/{languageGufi}")]
        //[SwaggerOperation(@"GetSimpleLanguageResources"), SwaggerResponse(200, type: typeof(List<LocaleStringResource>))]
        [SwaggerOperation(@"GetSimpleLanguageResources"), SwaggerResponse(200, type: typeof(List<LanguageResource>))]
        public virtual IActionResult GetSimpleLanguageResources([FromRoute] Guid languageGufi)
        {
            var language = languageRepository.Find(languageGufi);
            List<string> values = new List<string>();

            if (language != null)
            {
                //var resources = localeStringResourceRepository.GetAll()
                var resources = languageResourceRepository.GetAll()
                    .Where(x => x.LanguageGufi.Equals(language.LanguageGufi))
                    //.Adapt<List<LocaleStringResource>>();
                    .Adapt<List<LanguageResource>>();

                var o = new JObject();
                //foreach (LocaleStringResource l in resources)
                foreach (LanguageResource l in resources)
                {    
                    o[l.ResourceName] = l.ResourceValue;   
                }

                return new ObjectResult(o);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"No data found.", Id = languageGufi.ToString() });
            }
        }

        /// <summary>
        /// Simple information regarding Resources of a English
        /// </summary>
        /// <remarks>Returns simple list of Resources of English.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        [HttpGet]
        //[Authorize]
        [Route(@"/language/simpleresource/english")]
        //[SwaggerOperation(@"GetSimpleEnglishResources"), SwaggerResponse(200, type: typeof(List<LocaleStringResource>))]
        [SwaggerOperation(@"GetSimpleEnglishResources"), SwaggerResponse(200, type: typeof(List<LanguageResource>))]
        public virtual IActionResult GetSimpleEnglishResources()
        {
            var language = _context.Languages.FirstOrDefault(x => x.LanguageCode.Equals("en"));
            List<string> values = new List<string>();

            if (language != null)
            {
                //var resources = localeStringResourceRepository.GetAll()
                var resources = languageResourceRepository.GetAll()
                    .Where(x => x.LanguageGufi.Equals(language.LanguageGufi))
                    //.Adapt<List<LocaleStringResource>>();
                    .Adapt<List<LanguageResource>>();

                var o = new JObject();
                //foreach (LocaleStringResource l in resources)
                foreach (LanguageResource l in resources)
                {
                    o[l.ResourceName] = l.ResourceValue;
                }

                return new ObjectResult(o);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"No data found for English." });
            }
        }

        /// <summary>
        /// English Local list will return
        /// </summary>
        /// <remarks>Returns total english resources</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        [HttpGet]
        //[Authorize]
        [Route(@"/language/englishresources")]
        //[SwaggerOperation(@"GetEnglishResources"), SwaggerResponse(200, type: typeof(List<LocaleStringResourceItem>))]
        [SwaggerOperation(@"GetEnglishResources"), SwaggerResponse(200, type: typeof(List<LanguageResourceItem>))]
        public virtual IActionResult GetEnglishResources()
        {
            var language = _context.Languages.FirstOrDefault(x => x.LanguageCode.Equals("en"));
            //var result = _context.LocaleStringResources.Where(x => x.LanguageId == language.LanguageId).ToList();
            var result = _context.LanguageResources.Where(x => x.LanguageGufi == language.LanguageGufi).ToList();

            return new ObjectResult(result);
        }

        /// <summary>
        /// Create new resource for a language.
        /// </summary>
        /// <remarks>Create new resource.</remarks>
        /// <param name="resourceData">The resource data</param>
        /// <response code="201">Resource data created.</response>
        /// <response code="400">Bad request. Bad request. Resource Name exist, use another name.</response>
        [HttpPost, ApiValidationFilter]
        [Route(@"/language/resource/save")]
        //[Authorize]
        [SwaggerOperation("CreateLanguageResource"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        //public virtual async Task<IActionResult> CreateLanguageResource([FromBody]LocaleStringResourceItem resourceData)
        public virtual IActionResult CreateLanguageResource([FromBody]LanguageResourceItem resourceData)
        {
            try
            {
                //Resource Name Check (if existing return bad request message)
                string stringId = ResourceNameCheck(resourceData.ResourceName, resourceData.LanguageGufi);
                if (stringId != null) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"Resource Name - {resourceData.ResourceName} exist", Id = stringId });

                //var model = resourceData.Adapt<LocaleStringResourceItem, LocaleStringResource>();
                var model = resourceData.Adapt<LanguageResourceItem, LanguageResource>();
                //model.ResourceGufi = Guid.NewGuid();
                model.ResourceId = Guid.NewGuid();
                //string producedId = model.ResourceGufi.ToString();
                string producedId = model.ResourceId.ToString();

                //localeStringResourceRepository.Add(model);
                //model.DateCreated = DateTime.Now;
                //model.DateModified = DateTime.Now;

                //await _context.LocaleStringResources.AddAsync(model);
                //int result = await _context.SaveChangesAsync();
                languageResourceRepository.Add(model);

                //if (result > 0) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Created, Message = "Resource created.", Id = producedId });

                //return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = "Error occured." });

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Created, Message = "Resource created.", Id = producedId });
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }

        }

        /// <summary>
        /// Create new resources for languages.
        /// </summary>
        /// <remarks>Create new resources by List.</remarks>
        /// <param name="resourcesData">The resources data</param>
        /// <response code="201">Resources data created.</response>
        /// <response code="400">Bad request. Bad request. Resource Name exist, use another name.</response>
        [HttpPost, ApiValidationFilter]
        [Route(@"/languages/resources/save")]
        //[Authorize]
        [SwaggerOperation("CreateLanguagesResources"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        // public virtual async Task<IActionResult> CreateLanguagesResourcesAsync([FromBody]List<LocaleStringResourceItem> resourcesData)
        public virtual IActionResult CreateLanguagesResourcesAsync([FromBody]List<LanguageResourceItem> resourcesData)
        {
            try
            { 
                List<string> Ids = new List<string>();

                //foreach (LocaleStringResourceItem item in resourcesData)
                foreach (LanguageResourceItem item in resourcesData)
                {
                    //Resource Name Check (if existing return bad request message)
                    string stringId = ResourceNameCheck(item.ResourceName, item.LanguageGufi);
                    if (stringId != null) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"Resource Name - {item.ResourceName} exist", Id = stringId });

                    //var model = item.Adapt<LocaleStringResourceItem, LocaleStringResource>();
                    var model = item.Adapt<LanguageResourceItem, LanguageResource>();
                    //model.ResourceGufi = Guid.NewGuid();
                    model.ResourceId = Guid.NewGuid();
                    //string producedId = model.ResourceGufi.ToString();
                    string producedId = model.ResourceId.ToString();

                    Ids.Add(producedId);

                    //model.DateCreated = DateTime.Now;
                    //model.DateModified = DateTime.Now;

                    //await _context.LocaleStringResources.AddAsync(model);
                    //int result = await _context.SaveChangesAsync();
                    //Console.WriteLine(result);
                    languageResourceRepository.Add(model);
                }

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Created, Message = "Resources created successfully.", Id = string.Join(",", Ids.ToArray()) });

            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int) HttpStatusCode.Conflict, Message = e.Message });
            }
        }

        /// <summary>
        /// Update Resource details.
        /// </summary>
        /// <remarks>Update resource details.</remarks>
        /// <param name="resourceData">The Resource data</param>
        /// <param name="resourceGufi">The Resource Gufi</param>
        /// <response code="200">Resource data created.</response>
        /// <response code="404">Bad request. Resource name exist. use other name</response>
        [HttpPut, ApiValidationFilter]
        [Route(@"/language/resource/update/{resourceGufi}")]
        //[Authorize]
        [SwaggerOperation(@"UpdateResourceDetails"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        //public virtual IActionResult UpdateResourceDetails([FromBody]LocaleStringResourceItem resourceData, [FromRoute] string resourceGufi)
        public virtual IActionResult UpdateResourceDetails([FromBody]LanguageResourceItem resourceData, [FromRoute] Guid resourceGufi)
        {
            try
            {
                //var modelResource = localeStringResourceRepository.Find(resourceGufi);
                var modelResource = languageResourceRepository.Find(resourceGufi);

                //Resource Name Check (if existing return bad request message)
                if (modelResource.ResourceName != resourceData.ResourceName)
                {
                    string stringId = ResourceNameCheck(resourceData.ResourceName, resourceData.LanguageGufi);

                    if (stringId != null) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"Resource Name - {resourceData.ResourceName} exist", Id = stringId });
                }

                if (modelResource != null)
                {
                    resourceData.Adapt(modelResource);

                    //localeStringResourceRepository.Update(modelResource);
                    languageResourceRepository.Update(modelResource);

                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource details updated.", Id = resourceGufi.ToString() });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NoContent, Message = "No data found.", Id = resourceGufi.ToString() });
                }
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }
        }


        /// <summary>
        /// Delete a Resource
        /// </summary>
        /// <param name="resourceName"></param>
        /// <remarks>Allows deleting a resource.</remarks>
        /// <response code="200">Successful data request.</response>
        /// <response code="204">Bad request. Typically validation error. Fix your request and retry.</response>        
        [HttpDelete]
        //[Authorize]
        [Route("/language/resource/{resourceName}")]
        [SwaggerOperation(@"DeleteLanguage"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult DeleteResource([FromRoute] string resourceName)
        {
            try
            {
                //string resourceIdsString = "";
                List<string> Ids = new List<string>();

                //var model = _context.LocaleStringResources.Where(x => x.ResourceName.Contains(resourceName)).ToList();
                var model = _context.LanguageResources.Where(x => x.ResourceName.Equals(resourceName)).ToList();

                if (model.Count > 0)
                {
                    //foreach(LocaleStringResource l in model)
                    foreach (LanguageResource l in model)
                    {
                        Ids.Add(l.ResourceId.ToString());
                        //_context.LocaleStringResources.Remove(l);
                        _context.LanguageResources.Remove(l);
                        //resourceIdsString = resourceIdsString + l.ResourceGufi.ToString() + " ";
                    }

                    _context.SaveChanges();

                    //return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource deleted." , Id = resourceIdsString });
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource deleted.", Id = string.Join(",", Ids.ToArray()) });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NoContent, Message = $"No data found for - {resourceName}" });
                }
            }
            catch (Exception e)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Conflict, Message = e.Message });
            }
        }

        private void LanguageDisplayOrder(int number)
        {
            var entities = _context.Languages.Where(x => x.DisplayOrder >= number);

            if (entities != null)
            {
                foreach (Language l in entities)
                {
                    l.DisplayOrder += 1;
                    _context.Languages.Update(l);
                }
                _context.SaveChanges();
            }
        }

        private ObjectResult LanguageNameCheck(string languageName)
        {
            var nameCheck = _context.Languages.FirstOrDefault(x => x.Name.Equals(languageName));
            if (nameCheck != null) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"Language Name - {languageName} exist", Id = nameCheck.LanguageGufi.ToString() });
            return null;
        }

        private string ResourceNameCheck(string resourceName, Guid languageGufi)
        {
            //var nameCheck = _context.LocaleStringResources.FirstOrDefault(x => x.ResourceName.Equals(resourceName) && x.LanguageId == languageId);
            var nameCheck = _context.LanguageResources.FirstOrDefault(x => x.ResourceName.Equals(resourceName) && x.LanguageGufi == languageGufi);
            //if (nameCheck != null) return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = $"Resource Name - {resourceName} exist", Id = nameCheck.ResourceGufi.ToString() });
            if (nameCheck != null) return nameCheck.ResourceId.ToString();
            return null;
        }
    }
}
