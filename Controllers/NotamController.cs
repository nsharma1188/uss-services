﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common;
using AnraUssServices.Common.Lookups;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.NotamAreas;
using AnraUssServices.ViewModel.Notams;
using AnraUssServices.ViewModel.Operations;
using AnraUssServices.ViewModel.Users;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route(@"api/[controller]")]
    public class NotamController : BaseController
    {
        private readonly AnraMqttClient anraMqttClient;
        private readonly ILogger<NotamController> logger;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly DatabaseFunctions dbFunctions;
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Notam> notamRepository;
        private readonly IRepository<NotamArea> notamareaRepository;
        private AnraConfiguration _config;
        private readonly ViewerMqttNotification viewerMqttNotification;

        public NotamController(ApplicationDbContext context,
            ILogger<NotamController> logger,
            IHttpContextAccessor httpContextAccessor,
            IRepository<Notam> notamRepository,
            AnraMqttClient anraMqttClient,
            IRepository<NotamArea> notamareaRepository,
            DatabaseFunctions dbFunctions,
            AnraConfiguration configuration,
            ViewerMqttNotification viewerMqttNotification)
        {
            _context = context;
            _config = configuration;
            this.notamRepository = notamRepository;
            this.notamareaRepository = notamareaRepository;
            this.dbFunctions = dbFunctions;
            this.httpContextAccessor = httpContextAccessor;
            this.logger = logger;
            this.anraMqttClient = anraMqttClient;
            this.viewerMqttNotification = viewerMqttNotification;
        }

        /// <summary>
        /// Request information regarding Notams
        /// </summary>
        /// <remarks>Returns list of notams.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-constraint")]        
        [Route(@"/notam/listing")]
        [SwaggerOperation(@"GetNotams"), SwaggerResponse(200, type: typeof(List<NotamItem>))]
        public virtual IActionResult GetNotams()
        {            
            var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();
            var notams = new List<NotamItem>();

            if(CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                notams = notamRepository.GetAll().Where(z => z.Status == EnumUtils.GetDescription(NotamState.ACTIVATED))
                    .OrderByDescending(x => x.DateCreated).Adapt<List<NotamItem>>();
            }
            else
            {
                notams = notamRepository.GetAll().Where(z => z.Status == EnumUtils.GetDescription(NotamState.ACTIVATED)).
                    Where(x => x.GroupId.Equals(groupId)).OrderByDescending(x => x.DateCreated).Adapt<List<NotamItem>>();
            }

            return new ObjectResult(notams);
        }

        /// <summary>
        /// Request information regarding New Notam
        /// </summary>
        /// <remarks>Returns new notam instance</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-constraint")]        
        [Route(@"/notam/new")]
        [SwaggerOperation(@"GetNewNotam"), SwaggerResponse(200, type: typeof(NotamItem))]
        public virtual IActionResult GetNewNotam()
        {

            var notam = new NotamItem
            {
                BeginDate = DateTime.UtcNow,
                EndDate= DateTime.UtcNow,
                IssueDate= DateTime.UtcNow,
                AffectedArea = new NotamAreaItem(),
                TypesLookup = NotamTypeLookup.GetLookup(),
                FacilitiesLookup = FacilityLookup.GetLookup(),
                StatesLookup=StateLookup.GetLookup()
            };

            return new ObjectResult(notam);
        }

        /// <summary>
        /// Request information regarding Specific Notam
        /// </summary>
        /// <remarks>Allows querying for Notam data.</remarks>
        /// <param name="notamnumber">Gufi</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-constraint")]        
        [Route(@"/notam/{notamnumber}")]
        [SwaggerOperation(@"GetNotamByNumber"), SwaggerResponse(200, type: typeof(NotamItem))]
        public virtual IActionResult GetNotamByNumber([FromRoute]Guid notamnumber) //Null check notam number
        {
            var notamModel = notamRepository.Find(notamnumber);
            if (notamModel != null)
            {
                var notam = notamModel.Adapt<Notam, NotamItem>();
                notam.TypesLookup = NotamTypeLookup.GetLookup();
                notam.FacilitiesLookup = FacilityLookup.GetLookup();
                notam.StatesLookup = StateLookup.GetLookup();
                return new ObjectResult(notam);
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Resource not found." });
            }


        }

        /// <summary>
        /// Save Notam.
        /// </summary>
        /// <remarks>Create new notam.</remarks>
        /// <param name="notam">The Notam data</param>
        /// <response code="201">Notam data received.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
        [HttpPost, ApiValidationFilter]
        [Route(@"/notam")]
        [Authorize("uss:write-constraint")]        
        [SwaggerOperation(nameof(PostNotam)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostNotam([FromBody]NotamItem notam)
        {
            var organizationId = GetCurrentUserOrganizationId();

			var model = notam.Adapt<NotamItem,Notam>();
            model.NotamNumber = Guid.NewGuid();
            model.OrganizationId = organizationId;
            model.GroupId = GetCurrentGroupId();
            model.Status = EnumUtils.GetDescription(NotamState.ACTIVATED);

            notamRepository.Add(model);


            //Publish Notam Data
            //anraMqttClient.PublishTopic(MqttTopics.NotamTopic, model.Adapt<NotamDetailItem>());
            //New Mqtt notification
            viewerMqttNotification.NotificationToViewer(MqttMessageType.CREATED.ToString(), model.Adapt<NotamDetailItem>(), MqttViewerType.NOTAM);


            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = model.NotamNumber.ToString(), Id = model.NotamNumber.ToString() });
        }

        /// <summary>
        /// Submit updates to a previously communicated Notam.
        /// </summary>
        /// <remarks>Allows for update to a previous notam submission.</remarks>
        /// <param name="notam"></param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPut, ApiValidationFilter]
        [Route(@"/notam")]
        [Authorize("uss:write-constraint")]        
        [SwaggerOperation(nameof(PutNotam)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PutNotam([FromBody]NotamItem notam)
        {            
            var modelNotam = notamRepository.Find(notam.NotamNumber);

            if (modelNotam != null)
            {
                notam.Adapt(modelNotam);

                notamRepository.Update(modelNotam);

                //New Mqtt notification for Notams
                viewerMqttNotification.NotificationToViewer(MqttMessageType.UPDATED.ToString(), modelNotam.Adapt<NotamDetailItem>(), MqttViewerType.NOTAM);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 201, Message = @"Notam updated.", Id = notam.NotamNumber.ToString() });
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Notam not found.", Id = notam.NotamNumber.ToString() });
            }
        }

        /// <summary>
        /// Request Notam Expiration by Notam Number.
        /// </summary>
        /// <remarks>Uss Operator Will Send Notam Expiration Request By Sending Notam Number i.e. Gufi</remarks>
        /// <param name="notamnumber">Post by notamnumber</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-constraint")]        
        [Route(@"/notam/{notamnumber}/expire")]
        [SwaggerOperation(nameof(ExpireNotam)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult ExpireNotam([FromRoute]Guid notamnumber)
        {
            try
            { 
                var model = notamRepository.Find(notamnumber);
                if (model != null)
                {
                    if (model.Status != EnumUtils.GetDescription(NotamState.CANCELLED) && model.Status == EnumUtils.GetDescription(NotamState.ACTIVATED))
                    {
                        model.Status = EnumUtils.GetDescription(NotamState.EXPIRED);
                        model.DateModified = DateTime.UtcNow;
                        notamRepository.Update(model);

                        //New Mqtt notification for Notams
                        viewerMqttNotification.NotificationToViewer(MqttMessageType.CLOSED.ToString(), model.Adapt<NotamDetailItem>(), MqttViewerType.NOTAM);

                    }
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = @"Resource updated.", Id = notamnumber.ToString() });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Notam not found.", Id = notamnumber.ToString() });
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
            }
        }

        /// <summary>
        /// Request Notam Cancellation by Notam Number.
        /// </summary>
        /// <remarks>Uss Operator Will Send Notam Cancellation Request By Sending Notam Number i.e. Gufi</remarks>
        /// <param name="notamnumber">Post by notamnumber</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-constraint")]        
        [Route(@"/notam/{notamnumber}/cancel")]
        [SwaggerOperation(nameof(CancelNotam)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CancelNotam([FromRoute]Guid notamnumber)
        {
            try
            {
                var model = notamRepository.Find(notamnumber);
                if (model != null)
                {
                    if (model.Status != EnumUtils.GetDescription(NotamState.EXPIRED) && model.Status == EnumUtils.GetDescription(NotamState.ACTIVATED))
                    {
                        model.Status = EnumUtils.GetDescription(NotamState.CANCELLED);
                        model.DateModified = DateTime.UtcNow;
                        notamRepository.Update(model);

                        //Notify Notam Delete
                        //anraMqttClient.PublishTopic(MqttTopics.NotamDelete, notamnumber);

                        //New Mqtt notification for Notams
                        viewerMqttNotification.NotificationToViewer(MqttMessageType.CLOSED.ToString(), model.Adapt<NotamDetailItem>(), MqttViewerType.NOTAM);
                    }
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Resource updated.", Id = notamnumber.ToString() });
                }
                else
                {
                    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Notam not found.", Id = notamnumber.ToString() });
                }
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
            }
        }
    }
}