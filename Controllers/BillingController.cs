﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.Common.Billing;
using AnraUssServices.ViewModel.Billing;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AnraUssServices.Controllers
{
	[Route(@"api/[controller]")]
	public class BillingController : BaseController
    {
		private readonly BillingHelper billingHelper;

		public BillingController(BillingHelper billingHelper)
		{
			this.billingHelper = billingHelper;
		}

		/// <summary>
		/// Returns the Billing record for the user
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Authorize("uss:read-billing")]
		[Route(@"/billing")]
		[SwaggerOperation(nameof(GetBillingListing)), SwaggerResponse(200, type: typeof(List<Receipt>))]
		public virtual IActionResult GetBillingListing([FromHeader] string authorization)
		{
			var groupId = GetCurrentGroupId();
			var receiptList = billingHelper.GetReceipts(authorization, groupId);

			return new ObjectResult(receiptList);
		}
	}
}
