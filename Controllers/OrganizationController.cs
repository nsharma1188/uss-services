﻿using AnraUssServices.Common;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Models;
using AnraUssServices.Models.Signup;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Roles;
using AnraUssServices.ViewModel.Users;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
	[Route(@"api/[controller]")]
	public class OrganizationController : BaseController
	{
		private readonly OrganizationClient organizationClient;
        private readonly IRepository<Signup> signupRepository;

        public OrganizationController(OrganizationClient organizationClient, IRepository<Signup> signupRepository)
		{
			this.organizationClient = organizationClient;
            this.signupRepository = signupRepository;
		}

		/// <summary>
		/// Returns a new Orginazation Item 
		/// </summary>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpGet]
		[Authorize]
		[Route(@"/organization/new")]
		[SwaggerOperation(nameof(GetNewOrganization)), SwaggerResponse(200, type: typeof(OrganizationItem))]
		public virtual IActionResult GetNewOrganization([FromHeader] string authorization)
		{
			var currentUserRoles = GetCurrentUserRole();

			if (!currentUserRoles.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP))))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
			}
			else
			{
				var orginazation = new OrganizationItem
				{
					AdsbSourceLookup = AdsbSourceLookupItem.GetLookup(),
					CurrencyLookup = CurrencyLookupItem.GetLookup(),
					UnitLookup = UnitLookupItem.GetLookup()
				};
				return new ObjectResult(orginazation);
			}
		}

		/// <summary>
		/// Returns the list of Organizations.
		/// </summary>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpGet]
		[Authorize]
		[Route(@"/organization/getOrganizationList")]
		[SwaggerOperation(nameof(GetOrganizationList)), SwaggerResponse(200, type: typeof(List<OrganizationItem>))]
		public virtual IActionResult GetOrganizationList([FromHeader] string authorization)
		{
			var currentUserRoles = GetCurrentUserRole();

			//Organization list is only accessible by super admin and ANSP 
			if (!currentUserRoles.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.SUPERADMIN))
			|| x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP))))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
			}
			else
			{
				var groupId = CheckRole(EnumUserRole.SUPERADMIN.ToString()) ? null : GetCurrentGroupId();
				var organizationList = organizationClient.GetOrganizations(authorization, groupId);

				var newOrganizationList = new List<OrganizationItem>();

				foreach (var item in organizationList)
				{
					var newOrganization = item;
					newOrganization.AdsbSourceLookup = AdsbSourceLookupItem.GetLookup();
					newOrganization.UnitLookup = UnitLookupItem.GetLookup();
					newOrganization.CurrencyLookup = CurrencyLookupItem.GetLookup();
					newOrganizationList.Add(newOrganization);
				}

				return new ObjectResult(newOrganizationList);
			}
		}

		/// <summary>
		/// Returns the organization details based on the organizationId.
		/// </summary>
		/// <param name="organizationId"></param>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpGet]
		[Authorize]
		[Route(@"/organization/getOrganization/{organizationId}")]
		[SwaggerOperation(nameof(GetOrganization)), SwaggerResponse(200, type: typeof(OrganizationItem))]
		public virtual IActionResult GetOrganization(string organizationId, [FromHeader] string authorization)
		{
			var organization = organizationClient.GetOrganization(organizationId, authorization);
			if (organization != null)
			{
				organization.CurrencyLookup = CurrencyLookupItem.GetLookup();
				organization.UnitLookup = UnitLookupItem.GetLookup();
				organization.AdsbSourceLookup = AdsbSourceLookupItem.GetLookup();
				return new ObjectResult(organization);

			}
			else
			{
				organization = new OrganizationItem();
				return new ObjectResult(organization);
			}
		}


		/// <summary>
		/// Create a new Orginazations
		/// </summary>
		/// <param name="organization"></param>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpPost]
		[Authorize]
		[Route(@"/organization/createOrganization")]
		[SwaggerOperation(nameof(CreateOrganization)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult CreateOrganization([FromBody] OrganizationItem organization, [FromHeader] string authorization)
		{
			var currentUserRoles = GetCurrentUserRole();

			//Organization Creation is only accessible by super admin and ANSP 
			if (!currentUserRoles.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP))))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
			}
			else
			{
				var newOrginazation = organization.Adapt<OrganizationModel>();
				newOrginazation.GroupId = GetCurrentGroupId();
				newOrginazation.OrganizationId = Guid.NewGuid().ToString();
				newOrginazation.UserId = Guid.NewGuid().ToString();
				newOrginazation.UserRole.Add(RoleItem.GetRoleDescription().Where(x => x.Name ==
					EnumUserRole.ADMIN.ToString()).ToList().FirstOrDefault().Id);

				var response = organizationClient.CreateOrganazation(newOrginazation, authorization);

				return new ObjectResult(response);

			}
		}

		/// <summary>
		/// Update the existing organization
		/// </summary>
		/// <param name="organization"></param>
		/// <param name="authorization"></param>
		/// <returns></returns>
		[HttpPut]
		[Authorize]
		[Route(@"/organization/updateOrganization")]
		[SwaggerOperation(nameof(UpdateOrganization)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult UpdateOrganization([FromBody] OrganizationItem organization, [FromHeader] string authorization)
		{
			var currentUserRoles = GetCurrentUserRole();

			if (!currentUserRoles.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP)) || 	x.Equals(EnumUtils.GetDescription(EnumUserRole.SUPERADMIN))))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
			}
			else
			{
				var existingOrganization = organizationClient.GetOrganization(organization.OrganizationId, authorization);

				var newUpdatedOrganization = organization.Adapt<OrganizationModel>();
				newUpdatedOrganization.OrganizationId = existingOrganization.OrganizationId;

				var response = organizationClient.UpdateOrganization(newUpdatedOrganization, authorization, existingOrganization.OrganizationId);

				return new ObjectResult(response);

			}
		}

		/// <summary>
		/// Delete existing organization based on the organizationId
		/// </summary>
		/// <param name="authorization"></param>
		/// <param name="organizationId"></param>
		/// <returns></returns>
		[HttpDelete]
		[Authorize]
		[Route(@"/organization/delete/{organizationId}")]
		[SwaggerOperation(nameof(DeleteOrganization)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult DeleteOrganization([FromHeader] string authorization, string organizationId)
		{
			var currentUserRoles = GetCurrentUserRole();

			if (!currentUserRoles.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP)) || x.Equals(EnumUtils.GetDescription(EnumUserRole.SUPERADMIN))))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
			}
			else
			{
                var response = organizationClient.DeleteOrganization(authorization, organizationId);
                if(response.HttpStatusCode == 200)
                {
                    //Removing all the users related to that organization from the signup table.
                    var usersList = signupRepository.GetAll().Where(x => x.OrganizationId == organizationId).ToList();
                    foreach(var user in usersList)
                    {
                        signupRepository.Remove(user.SignupUid);
                    }
                }

				return new ObjectResult(response);
			}
		}
	}
}
