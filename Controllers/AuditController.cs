﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AnraUssServices.Common;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Models;
using AnraUssServices.Models.Audits;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Audits;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AnraUssServices.Controllers
{
	[Route(@"api/[controller]")]
	public class AuditController : BaseController
	{
        private readonly IRepository<Audit> auditRepository;

        public AuditController(IRepository<Audit> auditRepository)
        {
            this.auditRepository = auditRepository;
		}

		/// <summary>
		/// Returns the final audit log
		/// </summary>
		/// <returns></returns>
		[HttpGet]
        [Authorize("uss:read-audit")]
        [Route(@"/audit/log")]
		[SwaggerOperation(nameof(GetAuditLog)), SwaggerResponse(200, type: typeof(FinalAudit))]
        public virtual IActionResult GetAuditLog()
        {
            var auditList = auditRepository.GetAll().ToList();
            var finalAudit = new FinalAudit();
            var currentUserRole = GetCurrentUserRole();

            if (!currentUserRole.Exists(x => x.Equals(EnumUtils.GetDescription(EnumUserRole.SUPERADMIN))
            || x.Equals(EnumUtils.GetDescription(EnumUserRole.ANSP)) || x.Equals(EnumUtils.GetDescription(EnumUserRole.ADMIN))))
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = @"User not authorized" });
            }

            if (CheckRole(EnumUserRole.SUPERADMIN.ToString()))
            {
                finalAudit.AuditList = auditList.ToList();
            }
            else if(CheckRole(EnumUserRole.ANSP.ToString()))
            {
                var currentGroupId = GetCurrentGroupId();
                finalAudit.AuditList = auditList.Where(x => x.GroupId == currentGroupId || x.GroupId == null).ToList();
            }
            else
            {
                var currentOrganizationId = GetCurrentUserOrganizationId().ToString();
                finalAudit.AuditList = auditList.Where(x => x.OrganizationId == currentOrganizationId || x.OrganizationId == null).ToList();
            }

			foreach (var audit in finalAudit.AuditList)
			{
				if(audit.Status){ finalAudit.TotalSuccessfull++;}
				if (!audit.Status) { finalAudit.TotalFail++; }
				if (audit.IsValidUser && !audit.Status) { finalAudit.InvalidPassword++; }
				if (!audit.IsValidUser) { finalAudit.InvalidUserName++; }
			}

			finalAudit.TotalLogins = finalAudit.TotalSuccessfull + finalAudit.TotalFail;

			return new ObjectResult(finalAudit);
		}

        /// <summary>
        /// Returns the audit based on the user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize("uss:read-audit")]
        [Route(@"/audit/{userName}")]
        [SwaggerOperation(nameof(GetAudit)), SwaggerResponse(200, type: typeof(Audit))]
        public virtual IActionResult GetAudit([FromRoute] string userName)
        {
            var audit = auditRepository.GetAll().Where(x => x.UserName.Equals(userName)).ToList();
            return new ObjectResult(audit);
        }
    }
}
