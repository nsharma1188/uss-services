﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Data;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    public partial class UssController 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Returns vehicle information when a partial uvin or an faa_number is supplied. Atleast one parameter is required.</remarks>
        /// <param name="lon">Longitude of point for Geographic search. Approximate position of the vehicle.</param>
        /// <param name="partialUvin">Part of the uvin uuid.</param>
        /// <param name="faaNumber">Full faa_number.</param>
        /// <param name="lat">Latitude of point for Geographic search. Approximate position of the vehicle.</param>
        /// <response code="200">Successfully returns matched uvins and its associated operator information</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <response code="503">Service Unavailable</response>
        [HttpGet]
        [Authorize]
        [Route("/uss/pubsafe/uas")]
        [ApiValidationFilter]
        [SwaggerOperation("UasGet")]
        [SwaggerResponse(200, typeof(List<VehicleOperationData>), "Successfully returns matched uvins and its associated operator information")]
        [SwaggerResponse(400, typeof(List<VehicleOperationData>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<VehicleOperationData>), "Unauthorized")]
        [SwaggerResponse(403, typeof(List<VehicleOperationData>), "Forbidden")]
        [SwaggerResponse(404, typeof(List<VehicleOperationData>), "Not Found")]
        [SwaggerResponse(503, typeof(List<VehicleOperationData>), "Service Unavailable")]
        public virtual IActionResult UasGet([FromQuery]double? lon, [FromQuery]string partialUvin, [FromQuery]string faaNumber, [FromQuery]double? lat)
        {          
            return new ObjectResult(GetListOperationData(dbFunctions, fimsAuthentication, partialUvin, lat, lon, logger));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Returns all operations around the point of reference</remarks>
        /// <param name="altitudeGpsWgs84Ft">The altitude as measured via a GPS device on the aircraft. Units in feet using the WGS84 reference system</param>
        /// <param name="time">Known operations in this area as of this time</param>
        /// <param name="distance">Distance from reference_point to find operations. Ignored if reference_point is not provided.  Units are feet.  Returns all operations that have any operation_volumes that interesect the 2D circle defined by distance and reference_point.  Default value only has meaning when reference_point parameter is provided.</param>
        /// <param name="referencePoint">A single point used to find all operations within some distance from that point. Returns all operations that have any operation_volumes that interesect the 2D circle defined by distance and reference_point.  When distance it excluded and reference_point is included, use default value (300ft) for distance. Described as a GeoJSON position.  The value is equivalent to what would be seen in the \&quot;coordinates\&quot; field for a GeoJSON Point object.  See https://tools.ietf.org/html/rfc7946#section-3.1.1 for further reference.  Example would be reference_point&#x3D;[-122.056364, 37.414371] (URL safe: reference_point%3D%5B-122.056364%2C%2037.414371%5D). As per GeoJSON spec, this is long-lat format in the WGS84 reference system. MUST NOT include a third coordinate element, strictly 2D.</param>
        /// <response code="200">Returns all operations within the specified geography</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet]
        [Authorize]
        [Route("/uss/pubsafe/uas/operations")]
        [ApiValidationFilter]
        [SwaggerOperation("UasOperationsGet")]
        [SwaggerResponse(200, typeof(List<Operation>), "Returns all operations within the specified geography")]
        [SwaggerResponse(400, typeof(List<Operation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(List<Operation>), "Unauthorized")]
        [SwaggerResponse(403, typeof(List<Operation>), "Forbidden")]
        [SwaggerResponse(404, typeof(List<Operation>), "Not Found")]
        public virtual IActionResult UasOperationsGet([FromQuery]double? altitudeGpsWgs84Ft, [FromQuery]DateTime? time, [FromQuery]int? distance, [FromQuery]string referencePoint)
        {            
            return new ObjectResult(getOperationList(altitudeGpsWgs84Ft, time, distance, referencePoint, ussDiscoveryApi, dbFunctions, 
                fimsAuthentication, anraConfiguration,
                 logger));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Returns information on vehicle with the given uvin.</remarks>
        /// <param name="uvin">specifies the vehicle id</param>
        /// <param name="lat">Latitude of point for Geographic search. Approximate position of the vehicle.</param>
        /// <param name="lon">Longitude of point for Geographic search. Approximate position of the vehicle.</param>
        /// <response code="200">Successfully returned uvin and its associated operator information</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        /// <response code="503">Service Unavailable</response>
        [HttpGet]
        [Authorize]
        [Route("/uss/pubsafe/uas/{uvin}")]
        [ApiValidationFilter]
        [SwaggerOperation("UasUvinGet")]
        [SwaggerResponse(200, typeof(VehicleOperationData), "Successfully returned uvin and its associated operator information")]
        [SwaggerResponse(400, typeof(VehicleOperationData), "Bad request. Typically validation error. Fix your request and retry.")]
        [SwaggerResponse(401, typeof(VehicleOperationData), "Unauthorized")]
        [SwaggerResponse(403, typeof(VehicleOperationData), "Forbidden")]
        [SwaggerResponse(404, typeof(VehicleOperationData), "Not Found")]
        [SwaggerResponse(503, typeof(VehicleOperationData), "Service Unavailable")]
        public virtual IActionResult UasUvinGet([FromRoute]string uvin, [FromQuery]double? lat, [FromQuery]double? lon)
        {
            List<VehicleOperationData> listVehicleOp = GetListOperationData(dbFunctions, fimsAuthentication, uvin, lat, lon, logger);

            if (listVehicleOp.Count == 0)
            {
                return new ObjectResult(new VehicleOperationData());
            }
            else
            {
                return new ObjectResult(listVehicleOp[0]);
            }
        }


        private static List<VehicleOperationData> GetListOperationData(DatabaseFunctions dbFunctions, FimsTokenProvider fimsAuthentication
            , string uvin, double? lat, double? lon, ILogger<UssController> logger)
        {
            List<VehicleOperationData> listVehicleOp = new List<VehicleOperationData>();
            try
            {
                //TODO-- Search by uvin or lat lon -- Himanshu

                OperationSearchItem criteria = new OperationSearchItem();
                criteria.SearchBy = "DRONE";
                criteria.Location = new Point();
                criteria.Location.Type = "Point";
                criteria.Location.Coordinates = new List<double?>();
                criteria.Location.Coordinates.Add(lon);
                criteria.Location.Coordinates.Add(lat);


                criteria.Drone = uvin;

                //ExtraContactInfo To be removed from database --Himanshu
                var result = dbFunctions.GetActiveOperations(criteria.SearchBy,
                    criteria.Location.ToNpgsqlPoint(), criteria.Drone, criteria.SearchRadius).ToList().Adapt<List<Models.Operation>,
                    List<OperationSearchResultItem>>();
                //********

                //Map Result to vehicle operation data****
                foreach (OperationSearchResultItem t in result)
                {
                    VehicleOperationData o = new VehicleOperationData();
                    o.Gufi = t.Gufi;                    
                    o.OperationState = EnumUtils.GetDescription(t.State);
                    o.OwnerContact = t.Contact.PhoneNumbers.Count > 0 ? string.Join(',',t.Contact.PhoneNumbers) : string.Empty;
                    o.OwnerName = t.Contact.Name;
                    o.UssInstanceId = t.UssInstanceId;
                    o.VehicleType = "DRONE";
                    o.UssName = t.UssName;

                    listVehicleOp.Add(o);
                }
                //*******************
            }
            catch (Exception e)
            {
                logger.LogInformation("Anra USS Controller :: Public Safety :: PublicSafetyUtility:: getListOperationData :: " + e.Message);
            }

            return listVehicleOp;
        }

        private static List<Models.Operation> getOperationList(
           double? altitudeGpsWgs84Ft, DateTime? time, int? distance, string referencePoint,
           UssDiscoveryApi ussDiscoveryApi,
           DatabaseFunctions dbFunctions, FimsTokenProvider fimsAuthentication, AnraConfiguration anraConfiguration
           , ILogger<UssController> logger)
        {

            List<Models.Operation> list = new List<Models.Operation>();
            try
            {
                string temp = referencePoint.Replace("[", "").Replace("]", "");
                double[] lonLat = temp.Split(',').Select(n => Convert.ToDouble(n)).ToArray();
                double lon = lonLat[0];
                double lat = lonLat[1];


                OperationSearchItem criteria = new OperationSearchItem();
                criteria.SearchBy = "DRONE";
                criteria.Location = new Point();
                criteria.Location.Type = "Point";
                criteria.Location.Coordinates = new List<double?>();
                criteria.Location.Coordinates.Add(lon);
                criteria.Location.Coordinates.Add(lat);


                criteria.Drone = "";

                list = dbFunctions.GetActiveOperations(criteria.SearchBy, criteria.Location.ToNpgsqlPoint(), criteria.Drone, criteria.SearchRadius).ToList();

            }
            catch (Exception e)
            {
                logger.LogInformation("Anra USS Controller :: Public Safety :: PublicSafetyUtility:: getOperationList :: " + e.Message);
            }

            return list;
        }

        /// <summary>
        /// Get constraint messages.
        /// </summary>
        /// <remarks>Allows querying for constraint messages.</remarks>
        /// <param name="limit">The maximum number or records to return.</param>
        /// <param name="offset">The index from which to begin the list of returned records.</param>
        /// <param name="distance">Distance from reference_point to find restrictions. Ignored if reference_point is not provided.  Units are feet.  Returns all restrictions that have any geography that interesect the 2D circle defined by distance and reference_point.  Default value only has meaning when reference_point parameter is provided.</param>
        /// <param name="referencePoint">A single point used to find all restrictions within some distance from that point. Returns all restrictions that have any geography that interesect the 2D circle defined by distance and reference_point.  When distance it excluded and reference_point is included, use default value (300ft) for distance. Described as a GeoJSON position.  The value is equivalent to what would be seen in the \&quot;coordinates\&quot; field for a GeoJSON Point object.  See https://tools.ietf.org/html/rfc7946#section-3.1.1 for further reference.  Example would be reference_point&#x3D;[-122.056364, 37.414371] (URL safe: reference_point%3D%5B-122.056364%2C%2037.414371%5D). As per GeoJSON spec, this is long-lat format in the WGS84 reference system. MUST NOT include a third coordinate element, strictly 2D.</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="401">Invalid or missing access_token provided.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize]
        [Route("/uss/pubsafe/restrictions")]
        [SwaggerOperation("GetRestrictions")]
        [SwaggerResponse(200, typeof(List<UASVolumeReservation>), "Successful data request.")]
        [SwaggerResponse(401, typeof(List<UASVolumeReservation>), "Unauthorized")]
        [SwaggerResponse(403, typeof(List<UASVolumeReservation>), "Forbidden")]
        [SwaggerResponse(404, typeof(List<UASVolumeReservation>), "Not Found")]        
        [SwaggerResponse(400, typeof(List<UASVolumeReservation>), "Bad request. Typically validation error. Fix your request and retry.")]
        public virtual IActionResult GetRestrictions([FromQuery]int? limit,[FromQuery]int? offset, [FromQuery]int? distance, [FromQuery]string referencePoint)
        {            
            var result = constraintsRepository.GetAll().Where(x => x.IsRestriction)
                .AsEnumerable().Skip(offset ?? 0).Take(limit ?? 10)
                .Adapt<List<UASVolumeReservation>>();
            return new ObjectResult(result);
        }

        /// <summary>
        /// Get a specific constraint message        
        /// </summary>
        /// <param name="message_id"></param>        
        /// <response code="201">Constraint data received. </response>
        [HttpGet]
        [Route("/uss/pubsafe/restrictions/{message_id}")]
        [SwaggerOperation(nameof(GetRestriction)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        [SwaggerResponse(200, typeof(UASVolumeReservation), "Successful data request.")]
        [SwaggerResponse(401, typeof(UASVolumeReservation), "Unauthorized")]
        [SwaggerResponse(403, typeof(UASVolumeReservation), "Forbidden")]
        [SwaggerResponse(404, typeof(UASVolumeReservation), "Not Found")]        
        [SwaggerResponse(400, typeof(List<UASVolumeReservation>), "Bad request. Typically validation error. Fix your request and retry.")]
        [Authorize]
        public virtual IActionResult GetRestriction([FromRoute] Guid message_id)
        {
            var result = constraintsRepository.GetAll().FirstOrDefault(x => x.MessageId.Equals(message_id) && x.IsRestriction);            
            return new ObjectResult(result.Adapt<UASVolumeReservation>());
        }

        /// <summary>
        /// Endpoint to PUT a constraint message that will be sent to LUN members
        ///intersecting the constraint geography.
        /// </summary>
        /// <param name="message_id"></param>
        /// <param name="constraint_message">The message being sent</param>
        /// <response code="201">Constraint data received. </response>
        [HttpPut, ApiValidationFilter]
        [Route("/uss/pubsafe/restrictions/{message_id}")]
        [SwaggerOperation(nameof(PutRestriction)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        [Authorize]
        public virtual IActionResult PutRestriction([FromRoute] Guid message_id, [FromBody]UASVolumeReservation uvr)
        {
            if (message_id != uvr.MessageId)
            {
                return ApiResponse(StatusCodes.Status400BadRequest, "The message_id provided did not match with the uvr message_id");
            }

            var model = uvr.Adapt<Models.ConstraintMessage>();
            //Set Message Type i.e. Whether it is a restriction message or constraint message
            model.IsRestriction = true;

            //Notify to Lun
            //utmMessenger.NotifyRestrictionMessageToLUN(model);

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Restriction Message Submitted." });
        }        
    }
}