﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route(@"api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ViewerController : BaseController
    {
        private readonly IRepository<UtmMessage> utmMessageRepository;
        private readonly AnraConfiguration configuration;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ILogger<OperationController> logger;
        private readonly ElevationApi elevationApi;

        public ViewerController(
            ILogger<OperationController> logger,
            IHttpContextAccessor httpContextAccessor,
            IRepository<Models.Operation> operationRepository,
            IRepository<UtmInstance> utmInstanceRepository,
            IRepository<UtmMessage> utmMessageRepository,
            DatabaseFunctions dbFunctions,
            ElevationApi elevationApi,
            AnraConfiguration configuration)

        {
            this.logger = logger;
            this.httpContextAccessor = httpContextAccessor;
            this.operationRepository = operationRepository;
            this.utmInstanceRepository = utmInstanceRepository;
            this.dbFunctions = dbFunctions;
            this.configuration = configuration;
            this.utmMessageRepository = utmMessageRepository;
            this.elevationApi = elevationApi;
        }

        [HttpGet]
        [Authorize("uss:read-viewer")]
        [Route(@"/viewer/{anraUssInstanceId}/data")]
        public virtual IActionResult GetData([FromRoute]Guid anraUssInstanceId)
        {
            var ussInstances = new List<UtmInstance>();
            var ussInstanceIds = new List<Guid>();

            //Anra Uss
            var anraUssInstance = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssInstanceId == anraUssInstanceId);

            if(anraUssInstance != null)
            {
                ussInstances.Add(anraUssInstance);
                ussInstanceIds.Add(anraUssInstance.UssInstanceId);
            }
            
            //External Uss's
            var externalUssInstances = dbFunctions.GetLocalUssNetworkByUtmInstanceId(anraUssInstanceId);

            if (externalUssInstances.Any())
            {
                externalUssInstances.ForEach(x => {
                    ussInstances.Add(x);
                    ussInstanceIds.Add(x.UssInstanceId);
                });
            }
            

            var result = new LunActiveOperationsViewModel();
            if (ussInstanceIds.Any())
            {
                result.UssInstances = ussInstances.Adapt<IEnumerable<UssInstance>>();

                result.Operations = GetActiveOperations(ussInstanceIds);

                List<Guid> opGufis = result.Operations.Select(x => x.Gufi).ToList();

                result.UtmMessages = GetUtmMessages(opGufis);
            }

            return new ObjectResult(result);
        }

        private IEnumerable<UTMMessage> GetUtmMessages(List<Guid> gufis)
        {
            return utmMessageRepository.GetAll()
                .Where(p => gufis.Contains(p.Gufi.Value))
                .OrderByDescending(p => p.SentTime)
                .Adapt<IEnumerable<UTMMessage>>();
        }

        private List<OperationViewModel> GetActiveOperations(List<Guid> ussInstanceIds)
        {
            return operationRepository.GetAll()
                                .Where(p => ussInstanceIds.Contains(p.UssInstanceId))
                                .Where(p => p.State != OperationState.CLOSED.ToString())
                                .Where(p => p.State != OperationState.PROPOSED.ToString())
                                .Where(p => p.OperationVolumes.Any(x => x.EffectiveTimeEnd > DateTime.UtcNow))                                

                                .Distinct()
                                 .Adapt<List<OperationViewModel>>();
        }

        private List<OperationViewModel> GetAllOperations(List<Guid> ussInstanceIds)
        {
            var lstActiveOperations = GetActiveOperations(ussInstanceIds);

            var lstClosedOperations = operationRepository.GetAll()
                                        .Where(p => ussInstanceIds.Contains(p.UssInstanceId))                                        
                                        .Where(p => p.State.Equals(OperationState.CLOSED.ToString()) && p.DateModified > DateTime.UtcNow.AddMinutes(-120)) //Fetch all operations Activated/Closed in last 10 mins.
                                        .Distinct()
                                        .Adapt<List<OperationViewModel>>();

            var operations = lstActiveOperations.Concat(lstClosedOperations).Distinct();

            return operations.Adapt<List<OperationViewModel>>();

        }

        [HttpGet]        
        [Authorize("uss:read-viewer")]
        [Route(@"/viewer/3d/{anraUssInstanceId}/data")]
        public virtual IActionResult Get3DViewerData([FromRoute]Guid anraUssInstanceId)
        {
            var ussInstances = new List<UtmInstance>();
            var ussInstanceIds = new List<Guid>();

            //Anra Uss
            var anraUssInstance = utmInstanceRepository.GetAll().FirstOrDefault(p => p.UssInstanceId == anraUssInstanceId);

            if (anraUssInstance != null)
            {
                ussInstances.Add(anraUssInstance);
                ussInstanceIds.Add(anraUssInstance.UssInstanceId);
            }

            //External Uss's
            var externalUssInstances = dbFunctions.GetLocalUssNetworkByUtmInstanceId(anraUssInstanceId);

            if (externalUssInstances.Any())
            {
                externalUssInstances.ForEach(x => {
                    ussInstances.Add(x);
                    ussInstanceIds.Add(x.UssInstanceId);
                });
            }


            var result = new LunActiveOperationsViewModel();
            if (ussInstanceIds.Any())
            {
                result.UssInstances = ussInstances.Adapt<IEnumerable<UssInstance>>();

                var operations = GetAllOperations(ussInstanceIds);

                List<Guid> opGufis = operations.Select(x => x.Gufi).ToList();

                result.UtmMessages = GetUtmMessages(opGufis);

                if (operations.Any())
                {
                    operations.ForEach(op => {
                        bool isABOVTypeOperation = op.OperationVolumes.FirstOrDefault().VolumeType.Equals(VolumeType.ABOV);

                        if (isABOVTypeOperation)
                        {
                            var coordinates = op.OperationVolumes.FirstOrDefault().OperationGeography.Coordinates.FirstOrDefault();
                            if (coordinates.Any())
                            {
                                var cord = coordinates.FirstOrDefault();
                                double latitude = cord[1].Value;
                                double longitude = cord[0].Value;
                                double altitude = elevationApi.GetMsl(latitude, longitude) * 3.28084; // Altitude in Feet
                                //Add altitude/msl for lat/lon
                                cord.Add(altitude);
                            }

                            op.WayPointsList = GetGeometryFromWaypointList(coordinates);
                        }
                    });

                    result.Operations = operations;
                }                
			}
			return new ObjectResult(result);
        }

        [HttpPost]
        [Authorize("uss:write-viewer")]
        [Route(@"/getmsl")]
        public virtual async Task<IActionResult> GetMsl([FromBody] Point point)
        {
            var result = "";
            
            if (point != null)
            {
                try
                {
                    double lon = point.Coordinates[0].Value;
                    double lat = point.Coordinates[1].Value;

                    using (var client = new HttpClient())
                    {
                        var weatherUrl = string.Format(this.configuration.MslUrl + lat+","+lon+ "&key="+this.configuration.MslKey);
                        var response = await client.GetAsync(weatherUrl);
                        if (response.IsSuccessStatusCode)
                        {
                            result = await response.Content.ReadAsStringAsync();
                        }
                    }
                    return new ObjectResult(result);
                }
                catch (Exception ex)
                {
                    return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
                }
            }
            else
            {
                return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Bad Request" });
            }
        }

        /// <summary>
        /// Return geometry json for waypoints created
        /// To Do - this is for temporary use , will remove once after refactor
        /// </summary>
        /// <returns>string</returns>
        private string GetGeometryFromWaypointList(List<List<double?>> wayPoints)
        {
            if (wayPoints.Any())
            {
                var geometry = new LineString();

                geometry.Type = "Polygon";

                List<List<double?>> coords = new List<List<double?>>();

                wayPoints.ForEach(waypoint => {

                    List<double?> coord = new List<double?>();
                    coord.Add(waypoint[1].Value);
                    coord.Add(waypoint[0].Value);
                    coord.Add(waypoint.Count > 2 ? waypoint[2].Value : 0);

                    //Add to List
                    coords.Add(coord);
                });


                geometry.Coordinates = coords;


                //Return geojson
                return geometry.ToJson();
            }

            return string.Empty;
        }
    }
}