﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Models.Collisions;
using AnraUssServices.Models.DetectSubscribers;
using AnraUssServices.ViewModel.Collisions;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Drone Interface
    /// </summary>
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CollisionController : BaseController
    {
        private readonly CollisionDocument collisionDocument; 
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ApplicationDbContext _context;
        private AnraConfiguration _config;  
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<DetectSubscriber> detectSubscriberRepository;

        public CollisionController(ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            AnraConfiguration configuration,
            CollisionDocument collisionDocument)
        {
            _context = context;
            _config = configuration;  
            this.httpContextAccessor = httpContextAccessor; 
            this.collisionDocument = collisionDocument;
        }

        /// <param name="gufi">operation gufi</param>
        /// <response code="200">OK</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Not Found</response>
        [HttpGet]
        [Route(@"/operation/{gufi}/collisions")]
        public IActionResult GetListing([FromRoute]string gufi)
        {
            try
            {
                var collisions = collisionDocument.GetCollisionsList(gufi)
                    .Take(20)
                    .Adapt<List<CollisionViewModel>>();

                return new ObjectResult(collisions);

            }
            catch (Exception ex)
            {
                return new ObjectResult(new List<CollisionViewModel>());
            }
        }
    }
}