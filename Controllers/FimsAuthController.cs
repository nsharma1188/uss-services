﻿using AnraUssServices.Common.FimsAuth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AnraUssServices.Controllers
{
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class FimsAuthController : BaseController
    {
        private readonly ILogger<FimsAuthController> logger;
        private readonly FimsTokenProvider fimsAuthentication;

        public FimsAuthController(ILogger<FimsAuthController> logger, FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
        }

        [HttpGet]
        [Route("/scope/{scope}")]
        public string Get([FromRoute]string scope)
        {
            return fimsAuthentication.GetToken(scope);
        }
    }
}