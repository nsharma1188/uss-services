﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using AnraUssServices.ViewModel.Issues;
using System;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// Issue / Enhancement Interface
    /// </summary>
    [Route(@"api/[controller]")]
    public class IssueController : BaseController
    {
        private readonly ILogger<IssueController> logger;
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Issue> issueRepository;
        private AnraConfiguration _config;

        public IssueController(ApplicationDbContext context,
            ILogger<IssueController> logger,
            IRepository<Issue> issueRepository,
            AnraConfiguration configuration)
        {
            _context = context;
            _config = configuration;
            this.issueRepository = issueRepository;
            this.logger = logger;
        }

        /// <summary>
        /// Request information regarding Issues
        /// </summary>
        /// <remarks>Returns list of Languages.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        //[Authorize]        
        [Route(@"/issue/listing")]
        [SwaggerOperation(@"GetIssueOrEnhancementList"), SwaggerResponse(200, type: typeof(List<IssueItem>))]
        public virtual IActionResult GetIssueOrEnhancementList([FromQuery] int category,
            [FromQuery] int priority,[FromQuery] int status)
        {
            var filteredItems = new List<IssueItem>();

            var result = issueRepository.GetAll().OrderByDescending(x => x.DateCreated).Adapt<List<IssueItem>>();

            //If no filter criteria then return all data
            if(category.Equals(0) && priority.Equals(0) && status.Equals(0))
            {
                filteredItems.AddRange(result);
            }
            else
            {
                //Check For Issue Category
                if (category > 0)
                {
                    filteredItems.AddRange(result.Where(x => x.Category.Equals(category)));
                }

                //Check For Issue Priority
                if (priority > 0)
                {
                    filteredItems.AddRange(result.Where(x => x.Priority.Equals(priority)));
                }

                //Check For Issue Status
                if (status > 0)
                {
                    filteredItems.AddRange(result.Where(x => x.Status.Equals(status)));
                }
            }

            return new ObjectResult(filteredItems);
        }

        /// <summary>
        /// Create new issue/enhancement.
        /// </summary>
        /// <remarks>Create new issue/enhancement.</remarks>
        /// <param name="requestData">The issue/enhancement data</param>
        /// <response code="201">Data received.</response>
        /// <response code="400">Bad request. Language Name exist, use another name.</response>        
        [HttpPost, ApiValidationFilter]
        [Route(@"/issue/save")]
        //[Authorize]
        [SwaggerOperation(@"CreateIssueOrEnhancement"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CreateIssueOrEnhancement([FromBody]IssueItem requestData)
        {
            var model = requestData.Adapt<IssueItem, Issue>();
            issueRepository.Add(model);

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Created, Message = "Request logged.", Id = model.Id.ToString() });
        }

        /// <summary>
        /// Update issue/enhancement details.
        /// </summary>
        /// <remarks>Update issue/enhancement details.</remarks>
        /// <param name="requestData">The issue/enhancement data</param>
        /// <param name="requestId">The issue/enhancement Id</param>
        /// <response code="200">Request data updated.</response>
        /// <response code="400">Bad request. Language Name exist, use another name.</response>
        /// <response code="204">Data not found by Id, check with the server administrator.</response>
        [HttpPut, ApiValidationFilter]
        [Route(@"/issue/update/{requestId}")]
        //[Authorize]
        [SwaggerOperation(@"UpdateDetails"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult UpdateDetails([FromBody]IssueItem requestData, [FromRoute] Guid requestId)
        {
            var modelIssue = issueRepository.Find(requestId);

            if (modelIssue != null)
            {
                requestData.Adapt(modelIssue);

                issueRepository.Update(modelIssue);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Requested details updated.", Id = requestId.ToString() });
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.NoContent, Message = "No data found.", Id = requestId.ToString() });
            }
        }

        /// <summary>
        /// Request information regarding issue/enhancement
        /// </summary>
        /// <remarks>Returns requested issue/enhancement details.</remarks>        
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        //[Authorize]
        [Route(@"/issue/{Id}")]
        [SwaggerOperation(@"GetIssueOrEnhancementDetails"), SwaggerResponse(200, type: typeof(List<IssueItem>))]
        public virtual IActionResult GetIssueOrEnhancementDetails([FromRoute] Guid Id)
        {
            var issueModel = issueRepository.Find(Id);

            if (issueModel != null)
            {
                return new ObjectResult(issueModel.Adapt<IssueItem>());
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = $"No data found.", Id = Id.ToString() });
            }
        }
    }
}