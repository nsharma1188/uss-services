﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common;
using AnraUssServices.Common.ElevationHelper;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utility;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Conflicts;
using AnraUssServices.ViewModel.Contacts;
using AnraUssServices.ViewModel.ContingencyPlans;
using AnraUssServices.ViewModel.Operations;
using GeoCoordinatePortable;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
	/// <summary>
	/// USS/Operator Interface
	/// </summary>
	[Route(@"api/[controller]")]
	public class OperationController : BaseController
	{
		private readonly UserClient userClient;
		private readonly ElevationApi elevationApi;
		protected ILogger<OperationController> logger;
		private readonly IMediator mediator;
		private readonly DatabaseFunctions dbFunctions;
		private readonly UtmMessenger utmMessenger;
		private readonly IHttpContextAccessor httpContextAccessor;
		private readonly AnraMqttClient anraMqttClient;
		private readonly IRepository<UtmMessage> utmMessageRepository;
		private readonly IRepository<Models.NegotiationMessage> negoationMessageRepository;
		private readonly IRepository<ConstraintMessage> constraintsRepository;
		private readonly IRepository<Models.Operation> operationRepository;
		private readonly IRepository<UtmInstance> utmInstanceRepository;
		private readonly IRepository<Models.OperationVolume> operationVolumeRepository;
		private readonly IRepository<Models.ContingencyPlan> contingencyPlanRepository;
		private readonly IRepository<OperationConflict> operationConflictRepository;
		private AnraConfiguration _config;
		private OperationValidator _operationValidator;
		private UssDiscoveryApi ussDiscoveryApi;
		private FimsTokenProvider fimsAuthentication;
		private readonly AnraHttpClient httpClient;
		private UssVehicleRegistrationApi ussVehicleRegistrationApi;
		private VolumeSplitHelper volumeSplitHelper;
		private OperationSearchUtility searchUtility;
		private readonly OperationStatusLogic operationStatusLogic;
        private readonly ViewerMqttNotification viewerMqttNotification;

        public OperationController(
			ILogger<OperationController> logger,
			IHttpContextAccessor httpContextAccessor,
			IRepository<Models.Operation> operationRepository,
			IRepository<ConstraintMessage> constraintsRepository,
			IRepository<Models.NegotiationMessage> negoationMessageRepository,
			IRepository<UtmMessage> utmMessageRepository,
			IRepository<Models.OperationVolume> operationVolumeRepository,
			IRepository<Models.ContingencyPlan> contingencyPlanRepository,
			IRepository<UtmInstance> utmInstanceRepository,
			IRepository<OperationConflict> operationConflictRepository,
			UtmMessenger utmMessenger,
			DatabaseFunctions dbFunctions,
			AnraConfiguration configuration,
			OperationValidator operationValidator,
			UserClient userClient,
			IMediator mediator,
			AnraMqttClient anraMqttClient,
			UssDiscoveryApi ussDiscoveryApi,
			FimsTokenProvider fimsAuthentication,
			AnraHttpClient httpClient,
			ElevationApi elevationApi,
			UssVehicleRegistrationApi ussVehicleRegistrationApi,
			OperationSearchUtility searchUtility,
			VolumeSplitHelper volumeSplitHelper,
			OperationStatusLogic operationStatusLogic, ViewerMqttNotification viewerMqttNotification)
		{
			_config = configuration;
			_operationValidator = operationValidator;
			this.operationRepository = operationRepository;
			this.constraintsRepository = constraintsRepository;
			this.negoationMessageRepository = negoationMessageRepository;
			this.utmMessageRepository = utmMessageRepository;
			this.logger = logger;
			this.operationVolumeRepository = operationVolumeRepository;
			this.contingencyPlanRepository = contingencyPlanRepository;
			this.anraMqttClient = anraMqttClient;
			this.httpContextAccessor = httpContextAccessor;
			this.utmInstanceRepository = utmInstanceRepository;
			this.operationConflictRepository = operationConflictRepository;
			this.utmMessenger = utmMessenger;
			this.dbFunctions = dbFunctions;
			this.mediator = mediator;
			this.ussDiscoveryApi = ussDiscoveryApi;
			this.fimsAuthentication = fimsAuthentication;
			this.httpClient = httpClient;
			this.ussVehicleRegistrationApi = ussVehicleRegistrationApi;
			this.elevationApi = elevationApi;
			this.userClient = userClient;
			this.volumeSplitHelper = volumeSplitHelper;
			this.searchUtility = searchUtility;
			this.operationStatusLogic = operationStatusLogic;
            this.viewerMqttNotification = viewerMqttNotification;
        }

		/// <summary>
		/// Returns operations list for the authenticated user
		/// </summary>
		/// <param name="showExternalOps">todo: describe showExternalOps parameter on GetOpsByUserId</param>
		/// <remarks>Returns operations list for the authenticated user.</remarks>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpGet]
		[Authorize("uss:read-anra-operation")]
		[Route(@"/operation/listing/{showExternalOps?}")]
		[SwaggerOperation(@"GetOperations"), SwaggerResponse(200, type: typeof(List<OperationViewModel>))]
		public virtual IActionResult GetOpsByUserId([FromRoute] bool showExternalOps = false)
		{
			var organizationId = GetCurrentUserOrganizationId();

            var requiredRoles = new List<string>();
            requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
            requiredRoles.Add(EnumUserRole.ANSP.ToString());
            requiredRoles.Add(EnumUserRole.ATC.ToString());
            var IsRequiredRoles = CheckRoles(requiredRoles);

            bool fiter(Models.Operation o) => IsRequiredRoles ? true : o.OrganizationId.Equals(organizationId);
			var result = operationRepository.GetAll().Where(fiter);
			var ops = result.Where(x => x.IsInternalOperation.Equals(!showExternalOps)).Adapt<List<OperationViewModel>>();
			return new ObjectResult(ops.OrderByDescending(x => x.SubmitTime));
		}

		/// <summary>
		/// Returns operations list for the authenticated user based on filter
		/// </summary>
		/// <param name="showExternalOps">todo: describe showExternalOps parameter on GetOperations</param>
		/// <remarks>Returns operations list for the authenticated user.</remarks>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpPost]
		[Authorize("uss:read-anra-operation")]		
		[Route(@"/operation/{showExternalOps?}")]
		[SwaggerOperation(@"GetFilteredOperations"), SwaggerResponse(200, type: typeof(List<OperationViewModel>))]
		public virtual IActionResult GetOperations([FromBody] OperationFilterItem filter, [FromRoute] bool showExternalOps = false)
		{
			var organizationId = GetCurrentUserOrganizationId();

            var requiredRoles = new List<string>();
            requiredRoles.Add(EnumUserRole.SUPERADMIN.ToString());
            requiredRoles.Add(EnumUserRole.ANSP.ToString());
            requiredRoles.Add(EnumUserRole.ATC.ToString());
            var IsRolesAuthorized = CheckRoles(requiredRoles);

            Point location = null;
			int radius = 0;
			int reclimit = 0;
			Guid ussinstanceid = Guid.Empty;
			if (filter != null)
			{
				location = filter.Location;
				radius = filter.Radius;
				reclimit = filter.RecordLimit;
				ussinstanceid = filter.UssInstanceId;
			}
			var result = dbFunctions.GetActiveOperations(ussinstanceid, (IsRolesAuthorized ? Guid.Empty : organizationId), location.Adapt<NpgsqlTypes.NpgsqlPoint?>(), radius, reclimit);
			List<Guid> opsIds = showExternalOps ? result.Select(o => o.Gufi).ToList() : result.Where(x => x.IsInternalOperation.Equals(true)).Select(o => o.Gufi).ToList();
			var ops = operationRepository.GetAll().Where(x => opsIds.Contains(x.Gufi)).Adapt<List<OperationViewModel>>();

			return new ObjectResult(ops.OrderByDescending(x => x.SubmitTime));
		}

		/// <summary>
		/// Request information regarding Specific Operation
		/// </summary>
		/// <remarks>Allows querying for Operation data.</remarks>
		/// <param name="gufi">Gufi</param>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpGet]
		[Authorize("uss:read-anra-operation")]		
		[Route(@"/operation/{gufi}")]
		[SwaggerOperation(@"GetOperationByGufi"), SwaggerResponse(200, type: typeof(OperationViewModel))]
		public virtual IActionResult GetOpByGufi([FromRoute]Guid gufi)
		{
			var opsModel = operationRepository.Find(gufi);
			var operation = new OperationViewModel();

			if (opsModel != null)
			{
				var contact = opsModel.Contact.Adapt<PersonOrOrganizationViewModel>();

				operation = opsModel.Adapt<OperationViewModel>();
				operation.ContactId = contact.ContactId;
				return new ObjectResult(operation);
			}
			else
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Operation doesn't exist" });
			}
		}

		/// <summary>
		/// Delete Specific Operation
		/// </summary>
		/// <remarks>Delete Specific Operation</remarks>
		/// <param name="gufi">Gufi</param>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpDelete]
		[Authorize("uss:write-anra-operation")]		
		[Route(@"/operation/{gufi}")]
		[SwaggerOperation(@"DeleteOperationByGufi"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult DeleteOperation([FromRoute]Guid gufi)
		{
			var organizationId = GetCurrentUserOrganizationId();

			var operation = operationRepository.Find(gufi);
			if (operation != null && operation.OrganizationId == organizationId)
			{
				operationRepository.Remove(operation.Gufi);
				//operationStatusLogic.NotifyOperationDelete(gufi, MqttOperationMessageType.DELETE.ToString());

				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = "Operation deleted successfully." });
			}
			else
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = "Invalid operation." });
			}
		}

		/// <summary>
		/// Allow operators to submit their UAS Operations.
		/// </summary>
		/// <param name="operation">The operation data</param>
		/// <param name="authorization">todo: describe authorization parameter on PostOperation</param>
		/// <remarks>Create new operation.</remarks>
		/// <response code="201">Operation data received.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
		[HttpPost, ApiValidationFilter]
		[Route(@"/operation")]
		[Authorize("uss:write-anra-operation")]		
		[SwaggerOperation(nameof(PostOperation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult PostOperation([FromBody]OperationViewModel operation, [FromHeader]string authorization)
		{
			var userId = GetCurrentUserId();
			var organizationId = GetCurrentUserOrganizationId();
            var groupId = GetCurrentGroupId();
			operation.UserId = userId;
			operation.State = OperationState.PROPOSED;
			operation.SubmitTime = DateTime.UtcNow;
			operation.UpdateTime = DateTime.UtcNow;
			operation.Gufi = Guid.NewGuid();
            operation.OrganizationId = organizationId;

            //Set Drone location url from config

            //remove dependency from client to maintain the url

            if (operation.UasRegistrations.Any())			{				operation.UasRegistrations.ForEach(drone =>				{					drone.RegistrationLocation = _config.DroneLocationUrl;				});			}

			//Set Uss
			SetUssInstanceFields(operation);

			//Here we will calculate the min and max altitude if the operation volume is type ABOV
			bool isABOVTypeOperation = operation.OperationVolumes.FirstOrDefault().VolumeType.Equals(VolumeType.ABOV);

			if (isABOVTypeOperation)
			{
				operation.OperationVolumes = CalculateMinMaxAltitude(operation.OperationVolumes.ToList());
				//Create the CoordinatesList for the case of 3D viewer.
				operation.CoordinatesList = GetCoordinatesList(operation.OperationVolumes.ToList());
			}

			//Operation Basic Validation Checks
			if (!_operationValidator.IsValid(operation.Adapt<Utm.Models.Operation>(), isInternalOperation: true, validateOperationTime: true, gufi: operation.Gufi, ussInstanceId: operation.UssInstanceId))
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Join(@", ", _operationValidator.Errors) });
			}

			var userDetails = userClient.GetUserDetails(operation.ContactId, authorization);
			operation.Contact = new PersonOrOrganizationViewModel
			{
				ContactId = userDetails.UserId,
				Name = $"{userDetails.FirstName} {userDetails.LastName}",
				EmailAddresses = new List<string> { userDetails.Email },
				PhoneNumbers = new List<string> { userDetails.PhoneNumber }
			};

			// set contingency plan if it is not provided
			if (operation.ContingencyPlans == null)			{				operation.ContingencyPlans = SetContingencyPlan(operation, isABOVTypeOperation);			}

			var model = operation.Adapt<Models.Operation>();
			model.IsInternalOperation = true;
			//model.OrganizationId = organizationId;
            model.GroupId = groupId;
            operationRepository.Add(model);

            //New Mqtt Notification for viewer
            viewerMqttNotification.NotificationToViewer(MqttMessageType.CREATED.ToString(), operation, MqttViewerType.OPERATION);


            mediator.Send(new AsyncOperationProposedNotification(model.Adapt<Utm.Models.Operation>()));

			return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = @"Operation created.", Id = operation.Gufi.ToString() });
		}

		/// <summary>
		/// Submit updates to a previously communicated Operation.
		/// </summary>
		/// <param name="operation">Operational plan with the following properties:   1. Contains a valid uss_operation_id.   2. time_available_end value that is in the future.   3. No date-time fields that are in the past are modified.   4. Other rules for a USS Operation POST are satisfied. </param>
		/// <param name="authorization">todo: describe authorization parameter on PutOp</param>
		/// <remarks>Allows for update to a previous opeation submission.</remarks>
		/// <response code="201">OK</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpPut, ApiValidationFilter]
		[Route(@"/operation")]
		[Authorize("uss:write-anra-operation")]		
		[SwaggerOperation(nameof(PutOp)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult PutOp([FromBody]OperationViewModel operation, [FromHeader]string authorization)
		{
			//Removed the elevation check since we are calculating the min and max altitude in the service side
			bool isABOVTypeOperation = operation.OperationVolumes.FirstOrDefault().VolumeType.Equals(VolumeType.ABOV);
			var existingOperation = operationRepository.Find(operation.Gufi).Adapt<Models.Operation>();

			if (existingOperation != null)
			{
				//Handling Contingency - remove existing contingency and create new one.
				if (existingOperation.ContingencyPlans.Count != 0)
				{
					foreach (var existingContingency in existingOperation.ContingencyPlans)
					{
						contingencyPlanRepository.Remove(existingContingency.ContingencyPlanUid);
					}
				}

				//Handling Operation Volumes - Remove existing volumes 
				//and accept fresh submitted volumes.
				List<Guid> existingVolumeIds = existingOperation.OperationVolumes.Select(x => x.OperationVolumeUid).ToList();
				List<Guid> selectedVolumeIds = operation.OperationVolumes.Select(x => x.OperationVolumeUid).ToList();
				var filterVolIds = existingVolumeIds.Except(selectedVolumeIds).ToList();

				if (filterVolIds.Any())
				{
					existingVolumeIds.ForEach(x =>
					{
						operationVolumeRepository.Remove(x);
					});
					operation.OperationVolumes = CalculateMinMaxAltitude(operation.OperationVolumes.ToList());

					//Create the CoordinatesList for the case of 3D viewer.

					operation.CoordinatesList = GetCoordinatesList(operation.OperationVolumes.ToList());
				}

				//Operation Basic Validation Checks
				if (!_operationValidator.IsValid(operation.Adapt<Utm.Models.Operation>(), isInternalOperation: true, gufi: operation.Gufi, ussInstanceId: operation.UssInstanceId))
				{
					return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Join(@", ", _operationValidator.Errors) });
				}

				var userDetails = userClient.GetUserDetails(operation.ContactId, authorization);
				operation.Contact = new PersonOrOrganizationViewModel
				{
					ContactId = userDetails.UserId,
					Name = $"{userDetails.FirstName} {userDetails.LastName}",
					EmailAddresses = new List<string> { userDetails.Email },
					PhoneNumbers = new List<string> { userDetails.PhoneNumber }
				};

				operation.UserId = GetCurrentUserId();
				operation.UpdateTime = DateTime.UtcNow;
				SetUssInstanceFields(operation);

				// set contingency plan
				operation.ContingencyPlans = SetContingencyPlan(operation, isABOVTypeOperation);

				existingOperation = operation.Adapt(existingOperation);
				operationRepository.Update(existingOperation);

                //New Mqtt Notification for viewer
                viewerMqttNotification.NotificationToViewer(MqttMessageType.UPDATED.ToString(), operation, MqttViewerType.OPERATION);

                mediator.Send(new AsyncOperationProposedNotification(existingOperation.Adapt<Utm.Models.Operation>()));

				return new ObjectResult(new UTMRestResponse { HttpStatusCode = 201, Message = @"Operation updated.", Id = operation.Gufi.ToString() });
			}
			else
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
			}
		}

		private void SetUssInstanceFields(OperationViewModel operation)
		{
			if (_config.IsUtmEnabled)
			{
				var ussInstance = utmInstanceRepository.GetAll().Where(p => p.UssInstanceId == operation.UssInstanceId).OrderByDescending(p => p.DateModified).FirstOrDefault();
				operation.UssName = ussInstance.UssName;
			}
		}

		/// <summary>
		/// Clone an existing UAS Operation.
		/// </summary>
		/// <remarks>Create new operation.</remarks>
		/// <param name="gufi">Gufi</param>
		/// <response code="201">Operation data received.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further submissions.</response>
		[HttpPost, ApiValidationFilter]
		[Route(@"/operation/{gufi}/clone")]
		[Authorize("uss:write-anra-operation")]		
		[SwaggerOperation(nameof(PostOperation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
		public virtual IActionResult CloneOperation([FromRoute]Guid gufi)
		{
			try
			{
				var newOperation = operationRepository.Find(gufi).Adapt<OperationViewModel>();
				newOperation.OperationVolumes.ToList().ForEach(p =>
				{
					p.OperationVolumeUid = Guid.Parse("");
				});

				newOperation.ContingencyPlans.ToList().ForEach(p =>
				{
					p.ContingencyPlanId = 0;
				});

				//Reset Uas Registration Record
				newOperation.UasRegistrations.ToList().ForEach(p =>
				{
					p.UasRegistrationId = 0;
				});

				newOperation.Gufi = Guid.NewGuid();
				newOperation.SubmitTime = DateTime.UtcNow;
				newOperation.UpdateTime = DateTime.UtcNow;
				newOperation.State = OperationState.PROPOSED;
				var model = newOperation.Adapt<Models.Operation>();
				model.IsInternalOperation = true;
				operationRepository.Add(model);

				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = @"Operation cloned.", Id = gufi.ToString() });
			}
			catch (Exception ex)
			{
				logger.LogError("CloneOperation :: Exception {0}, Stack: {1}", ex.Message, ex.StackTrace);
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError });
			}
		}

		/// <summary>
		/// Request information regarding Operations
		/// </summary>
		/// <param name="criteria">describe criteria parameter on GetActiveOperations</param>
		/// <remarks>Allows querying for Operation data.</remarks>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpPost]
		[Route(@"/operation/search")]
		[Authorize("uss:read-anra-operation")]		
		[SwaggerOperation(@"GetActiveOperations"), SwaggerResponse(200, type: typeof(List<OperationSearchResultItem>))]
		public virtual IActionResult GetActiveOperations([FromBody]OperationSearchItem criteria, [FromHeader]string authorization)
		{
			var ops = _config.IsUtmEnabled ? searchUtility.GetOperationsFromExternalUss(criteria, authorization) : searchUtility.GetOperationDataFromLocal(criteria, false, authorization);
			return new ObjectResult(ops);
		}

		/// <summary>
		/// Request information regarding Operations
		/// </summary>
		/// <param name="criteria">describe criteria parameter on GetActiveOperations</param>
		/// <remarks>Allows querying for Operation data.</remarks>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpPost]
		[Route(@"/operation/searchOpenApi")]
		[SwaggerOperation(@"GetActiveOperationsOpen"), SwaggerResponse(200, type: typeof(List<OperationSearchResultItemForOpenApi>))]
		public virtual IActionResult GetActiveOperationsOpen([FromBody]OperationSearchItem criteria)
		{
			List<OperationSearchResultItem> l1 = new List<OperationSearchResultItem>();
			l1 = _config.IsUtmEnabled ? searchUtility.GetOperationsFromExternalUss(criteria) : searchUtility.GetOperationDataFromLocal(criteria, true);

			//Wanted to return all the details for the external operations as well. 
			List<OperationSearchResultItemForOpenApi> l2 = new List<OperationSearchResultItemForOpenApi>();
			foreach (OperationSearchResultItem temp in l1)
			{
				l2.Add(new OperationSearchResultItemForOpenApi(temp));
			}
			return new ObjectResult(l2);
		}

		/// <summary>
		/// Request information regarding LUN and its active operations
		/// </summary>
		/// <remarks>Allows querying LUN and its active operations.</remarks>
		/// <param name="gufi">Operation Gufi</param>
		/// <response code="200">Successful data request. Response includes requested data.</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
		[HttpGet]
		[Authorize("uss:read-anra-operation")]
		[Route(@"/operation/{gufi}/lun/operations")]
		[SwaggerOperation(@"GetActiveOperationsInLUN"), SwaggerResponse(200, type: typeof(LunActiveOperationsViewModel))]
		public virtual IActionResult GetActiveOperationsInLUN([FromRoute]Guid gufi)
		{
			var ussInstances = dbFunctions.GetLocalUssNetworkByGufi(gufi).Adapt<List<UssInstance>>();
			var ussInstanceNames = ussInstances.Select(p => p.UssName).ToList();
			//To Do : Below Query Needs to be refactor

			var operations = operationRepository.GetAll()
				.Where(p => ussInstanceNames.Contains(p.UssName) && !p.IsInternalOperation)
				.Where(p => p.State != OperationState.CLOSED.ToString())
				.Adapt<List<OperationViewModel>>();

			var result = new LunActiveOperationsViewModel
			{
				UssInstances = ussInstances,
				Operations = operations
			};

			return new ObjectResult(result);
		}

		/// <summary>
		/// Returns all conflicts for an operation
		/// </summary>
		/// <param name="gufi">operation gufi</param>
		/// <remarks>Returns all conflicts for an operation</remarks>
		/// <response code="200">Returns all conflicts for an operation</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="401">Unauthorized</response>
		/// <response code="403">Forbidden</response>
		/// <response code="404">Not Found</response>
		[HttpGet]
		[Authorize("uss:read-anra-operation")]		
		[Route(@"/operation/conflicts/{gufi}")]
		[SwaggerOperation(nameof(GetConflictsByGufi)), SwaggerResponse(200, type: typeof(List<ConflictViewModel>))]
		public virtual IActionResult GetConflictsByGufi([FromRoute]Guid gufi)
		{
			try
			{
				var lstOperationConflicts = new List<ConflictViewModel>();

				//Get All conflict data for input gufi
				var conflicts = operationConflictRepository.GetAll().Where(p => p.Gufi == gufi).Adapt<List<OperationConflictViewModel>>();

				//ToDo : Show all conflict history. Dont put state check to remove external operations having close state

				////Make conflict gufis list

				//var conflictGufis = conflicts.Select(x => x.ConflictingGufi).ToList();



				////Filter out expired conflicts & get only active conflict

				//var activeConflictGufis = operationRepository

				//                        .GetAll()

				//                        .Where(p => conflictGufis.Contains(p.Gufi) 

				//                                    && !p.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED))).Select(x => x.Gufi).ToList();



				////Remove expired conflicts

				//conflicts.RemoveAll(x => !activeConflictGufis.Contains(x.ConflictingGufi));



				if (conflicts.Any())
				{
					conflicts.ForEach(x =>
					{
						var opsConflict = new ConflictViewModel();
						opsConflict.OperationConflict = x;
						opsConflict.Reason = operationRepository.Find(x.ConflictingGufi).OperationVolumes.Adapt<List<Utm.Models.OperationVolume>>();
						opsConflict.IsInternalConflict = operationRepository.Find(x.ConflictingGufi).IsInternalOperation;
						lstOperationConflicts.Add(opsConflict);
					});
				}

				return new ObjectResult(lstOperationConflicts);
			}
			catch (Exception ex)
			{
				return ApiResponse(412, "Conflicting operation not found in system.");
			}
		}

		/// <summary>
		/// Returns operation volumes using the uploaded kml or way point.
		/// </summary>
		/// <param name="operationVolumeProperties"></param>
		/// <returns>Returns operation volumes using the flight path</returns>
		/// <response code="200">Returns all the operation volumes using polystring</response>
		/// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
		/// <response code="401">Unauthorized</response>
		/// <response code="403">Forbidden</response>
		/// <response code="404">Not Found</response>
		[HttpPost, ApiValidationFilter]
		[Authorize("uss:write-anra-operation")]		
		[Route(@"/operation/calculate/volume")]
		[SwaggerOperation(nameof(CalculateOperationVolume)), SwaggerResponse(200, type: typeof(GenerateOperationVolumeKml))]
		public virtual IActionResult CalculateOperationVolume([FromBody]GenerateOperationVolumeKml operationVolumeProperties)
		{
			try
			{
				var responseOperationVolume = new OperationVolumeHelperViewModel();
				var volumes = new List<OperationVolumeViewModel>();
				List<LineStringViewModel> lineStrings = new List<LineStringViewModel>();

				//Creating LineString to handel the CoordinatesList for 3D viewer.
				var geometry = new LineString();
				geometry.Type = "LineString";
				List<List<double?>> coordsList = new List<List<double?>>();
				var kmlJson = operationVolumeProperties.KmlJson;
				var reader = new GeoJsonReader();
				var result = reader.Read<NetTopologySuite.Features.FeatureCollection>(kmlJson);
				var features = result.Features.ToList().Where(p => p.Geometry.GeometryType.Equals("Point")).ToList();
				var finalPoint = features.Count - 1;

				//Point("Longitude, Latitude")
				var takeoffPolygon = dbFunctions.GetCircle("Point(" + features[0].Geometry.Coordinates[0].X + " " + features[0].Geometry.Coordinates[0].Y + ")", (operationVolumeProperties.TakeoffLandingRadius / 3.28084)); //Converting TakeoffLandingRadius to meters.
				var landingPolygon = dbFunctions.GetCircle("Point(" + features[finalPoint].Geometry.Coordinates[0].X + " " + features[finalPoint].Geometry.Coordinates[0].Y + ")", (operationVolumeProperties.TakeoffLandingRadius / 3.28084)); //Converting TakeoffLandingRadius to meters.

				var takeoffPointElevation = elevationApi.GetElevationData(features[0].Geometry.Coordinates[0].Y, features[0].Geometry.Coordinates[0].X)
					.Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084 - _config.AltitudeBuffer; //Adding a altitude buffer to make sure its starts from the ground (Elevation in feets)
				var landingPointElevation = elevationApi.GetElevationData(features[finalPoint].Geometry.Coordinates[0].Y, features[finalPoint].Geometry.Coordinates[0].X)
					.Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084 - _config.AltitudeBuffer; //Adding a altitude buffer to make sure its starts form the ground (Elevation in Feets)

				var takeoffOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(takeoffPolygon, takeoffPointElevation, takeoffPointElevation + features[0].Geometry.Coordinates[0].Z + operationVolumeProperties.AltitudeBuffer, features[0].Geometry.Coordinates[0].Z);
				var landingOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(landingPolygon, landingPointElevation, landingPointElevation + features[finalPoint].Geometry.Coordinates[0].Z + operationVolumeProperties.AltitudeBuffer, features[finalPoint].Geometry.Coordinates[0].Z);
				volumes.Add(takeoffOperationalVolume);
				for (var i = 0; i < features.Count - 1; i++)
				{
					var a = new GeoCoordinate(Convert.ToDouble(features[i].Geometry.Coordinates[0].Y), Convert.ToDouble(features[i].Geometry.Coordinates[0].X));
					var b = new GeoCoordinate(Convert.ToDouble(features[i + 1].Geometry.Coordinates[0].Y), Convert.ToDouble(features[i + 1].Geometry.Coordinates[0].X));

					if (a.Equals(b) && (features[i].Geometry.Coordinates[0].Z != features[i + 1].Geometry.Coordinates[0].Z))
					{
						var pointAElevation = elevationApi.GetElevationData(a.Latitude, a.Longitude).Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084; // In Feets
						var pointAPolygon = dbFunctions.GetCircle("Point(" + a.Longitude + " " + a.Latitude + ")", (operationVolumeProperties.TakeoffLandingRadius / 3.28084));
						var minAltitude = pointAElevation + features[i].Geometry.Coordinates[0].Z - operationVolumeProperties.AltitudeBuffer;
						if (minAltitude < pointAElevation)
						{
							minAltitude = pointAElevation;
						}
						var maxAltitude = pointAElevation + features[i + 1].Geometry.Coordinates[0].Z + operationVolumeProperties.AltitudeBuffer;
						var pointAOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(pointAPolygon, minAltitude, maxAltitude, features[i].Geometry.Coordinates[0].Z);
						var wayPointCoordinate = GetCoordinates(a.Latitude, a.Longitude, pointAElevation, false);
						coordsList.Add(wayPointCoordinate);
						volumes.Add(pointAOperationalVolume);
					}
					else
					{
						var distance = (a.GetDistanceTo(b)) * 3.28084; //distance in feet 
						if (distance > _config.VolumeSplittingLength)
						{
							lineStrings = volumeSplitHelper.SplitLineString(a.Longitude, a.Latitude, b.Longitude, b.Latitude, distance, (features[i].Geometry.Coordinates[0].Z), (features[i + 1].Geometry.Coordinates[0].Z));
							foreach (var lineString in lineStrings)
							{
								var wayPointACoordinate = GetCoordinates(lineString.Coordinates[0][1].Value, lineString.Coordinates[0][0].Value, 0, true);
								coordsList.Add(wayPointACoordinate);
								var volume = volumeSplitHelper.CalculateVolume(lineString, operationVolumeProperties, features[i].Geometry.Coordinates[0].Z);
								volumes.Add(volume);
							}
							var lineStringLastCoordinate = GetCoordinates(lineStrings[lineStrings.Count - 1].Coordinates[1][1].Value, lineStrings[lineStrings.Count - 1].Coordinates[1][0].Value, 0, true);
							coordsList.Add(lineStringLastCoordinate);
						}
						else
						{
							var splitGeographyPoint = dbFunctions.GetSplitGeography(a.Longitude, a.Latitude, b.Longitude, b.Latitude, 0, 1.0);
							var lineString = JsonConvert.DeserializeObject<LineStringViewModel>(splitGeographyPoint.ToString());
							lineString.AltitudePointA = features[i].Geometry.Coordinates[0].Z; // Altitude in Feet
							lineString.AltitudePointB = features[i + 1].Geometry.Coordinates[0].Z;
							var coordsListSize = coordsList.Count;
							if (coordsListSize != 0)
							{
								if (Math.Abs(coordsList[coordsListSize - 1][0].Value - a.Latitude) > 0.000001 && Math.Abs(coordsList[coordsListSize - 1][1].Value - a.Longitude) > 0.000001)
								{
									var wayPointA = GetCoordinates(a.Latitude, a.Longitude, 0, true);
									coordsList.Add(wayPointA);
								}
							}
							else
							{
								var wayPointA = GetCoordinates(a.Latitude, a.Longitude, 0, true);

								coordsList.Add(wayPointA);
							}
							var volumeNew = volumeSplitHelper.CalculateVolume(lineString, operationVolumeProperties, features[i].Geometry.Coordinates[0].Z);
							volumes.Add(volumeNew);
						}
					}
				}
				var lastPointA = new GeoCoordinate(Convert.ToDouble(features[features.Count - 1].Geometry.Coordinates[0].Y), Convert.ToDouble(features[features.Count - 1].Geometry.Coordinates[0].X));
				var lastWaypointCoordinate = GetCoordinates(lastPointA.Latitude, lastPointA.Longitude, 0, true);
				coordsList.Add(lastWaypointCoordinate);
				volumes.Add(landingOperationalVolume);
				SetOperationVolumeTimes(operationVolumeProperties.FlightSpeed, operationVolumeProperties.EffectiveTimeBegin, volumes);
				responseOperationVolume.OperationVolumes = volumes;
				responseOperationVolume.Waypoints = GetGeometryFromWaypointList(features);
				geometry.Coordinates = coordsList;
				responseOperationVolume.CoordinatesList = geometry.ToJson();

				return new ObjectResult(responseOperationVolume);
			}
			catch (Exception ex)
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = ex.Message });
			}
		}

		/// <summary>
		/// Set the operation flight time
		/// </summary>
		private void SetOperationVolumeTimes(int flightSpeedPerSec, DateTime flightStartDateTime, List<OperationVolumeViewModel> volumes)
		{
			if (volumes != null && volumes.Any())
			{
				//Making same start time and end time same through all the volume for the case of simulated flight.
				if (_config.IsSimulatedFlight)
				{
					var effectiveTimeBegin = flightStartDateTime.ToUniversalTime();
					var effectiveTimeEnd = flightStartDateTime.AddMinutes(119).ToUniversalTime();
					for (var i = 0; i < volumes.Count; i++)
					{
						volumes[i].EffectiveTimeBegin = effectiveTimeBegin;
						volumes[i].EffectiveTimeEnd = effectiveTimeEnd;
						volumes[i].Ordinal = i;
					}
				}
				else
				{

					//Setting the start time and endtime based on the speed and distance for each volume.
					var timeBufferinMinutes = 3;
					for (var i = 0; i < volumes.Count; i++)
					{
						var estimateTimeInSeconds = volumes[i].DistanceInFeet / flightSpeedPerSec;
						volumes[i].EffectiveTimeBegin = i == 0 ? flightStartDateTime.ToUniversalTime() : volumes[i - 1].EffectiveTimeEnd.AddMinutes(-1 * timeBufferinMinutes).ToUniversalTime();
						volumes[i].EffectiveTimeEnd = volumes[i].EffectiveTimeBegin.AddSeconds(estimateTimeInSeconds + 60 * timeBufferinMinutes).ToUniversalTime();
						volumes[i].Ordinal = i;
					}
				}
			}
		}

		/// <summary>
		/// Operation Volume min altitude validator for area based operation 
		/// </summary>
		/// <returns></returns>
		private List<string> OperationVolumeAltitudeValidation(List<OperationVolumeViewModel> operationVolumes)
		{
			var errors = new List<string>();
			foreach (var operation in operationVolumes)
			{
				var operationMinimumAltitude = operation.MinAltitude.AltitudeValue;
				var latitude = operation.OperationGeography.Coordinates[0][0][1];
				var longitude = operation.OperationGeography.Coordinates[0][0][0];
				var elevation = Math.Round(((elevationApi.GetElevationData(Convert.ToDouble(latitude), Convert.ToDouble(longitude)).Results
					.Select(x => x.Elevation).FirstOrDefault() * 3.28084) - 300), 2); //In Feet (- 300 for the altitude buffer)
				if (operationMinimumAltitude < elevation)
				{
					errors.Add("Operation Volume: " + operation.Ordinal + "  Minimum Altitude is below ground level. Current Ground Level: " + elevation + " ft");
				}
			}
			return errors;
		}

		/// <summary>
		/// Get Elevations
		/// </summary>
		[HttpGet]
		[Authorize("uss:read-anra-operation")]		
		[Route(@"/operation/getelevations")]
		[SwaggerOperation(@"GetElevations"), SwaggerResponse(200, type: typeof(ElevationResult))]
		public virtual IActionResult GetElevations([FromQuery]string path, [FromQuery]string sample)
		{
			var result = elevationApi.GetElevationData(path, sample);
			return new ObjectResult(result);
		}

		/// <summary>
		/// Generates the volume using the waypoints provided
		/// </summary>
		[HttpPost]
		[Authorize("uss:write-anra-operation")]		
		[Route(@"/operation/getVolume")]
		[ApiExplorerSettings(IgnoreApi = true)]
		[SwaggerOperation(@"GetVolume"), SwaggerResponse(200, type: typeof(List<OperationVolumeViewModel>))]
		public virtual IActionResult GetVolume([FromBody] WayPointViewModel wayPoints)
		{
			try
			{
				GenerateOperationVolumeKml operationVolumeProperties = new GenerateOperationVolumeKml()
				{
					ExpandFactor = wayPoints.ExpandFactor,
					EffectiveTimeBegin = wayPoints.EffectiveTimeBegin,
					AltitudeBuffer = wayPoints.AltitudeBuffer
				};
				var volumes = new List<OperationVolumeViewModel>();
				List<LineStringViewModel> lineStrings = new List<LineStringViewModel>();
				var takeoffPolygon = dbFunctions.GetCircle("Point(" + wayPoints.TakeOffPoint.Longititude + " " + wayPoints.TakeOffPoint.Latitude + ")", (wayPoints.TakeoffLandingRadius / 3.28084)); //Converting TakeoffLandingRadius to meters.
				var landingPolygon = dbFunctions.GetCircle("Point(" + wayPoints.LandingPoint.Longititude + " " + wayPoints.LandingPoint.Latitude + ")", (wayPoints.TakeoffLandingRadius / 3.28084)); //Converting TakeoffLandingRadius to meters.
				var takeoffPointElevation = elevationApi.GetElevationData(wayPoints.TakeOffPoint.Latitude, wayPoints.TakeOffPoint.Longititude)
					.Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084; //In Feets
				var landingPointElevation = elevationApi.GetElevationData(wayPoints.LandingPoint.Latitude, wayPoints.LandingPoint.Longititude)
					.Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084; //In Feets

				var takeoffOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(takeoffPolygon, takeoffPointElevation, takeoffPointElevation + wayPoints.TakeOffPoint.Altitude + wayPoints.AltitudeBuffer, wayPoints.TakeOffPoint.Altitude);
				var landingOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(landingPolygon, landingPointElevation, landingPointElevation + wayPoints.LandingPoint.Altitude + wayPoints.AltitudeBuffer, wayPoints.LandingPoint.Altitude);
				volumes.Add(takeoffOperationalVolume);
				var wayPointsList = wayPoints.Waypoints.ToList();
				for (var i = 0; i < wayPointsList.Count - 1; i++)
				{
					var a = new GeoCoordinate(Convert.ToDouble(wayPointsList[i].Latitude), Convert.ToDouble(wayPointsList[i].Longititude));
					var b = new GeoCoordinate(Convert.ToDouble(wayPointsList[i + 1].Latitude), Convert.ToDouble(wayPointsList[i + 1].Longititude));
					if (a.Equals(b) && (wayPointsList[i].Altitude != wayPointsList[i + 1].Altitude))
					{
						var pointAElevation = elevationApi.GetElevationData(a.Latitude, a.Longitude).Results.Select(x => x.Elevation).FirstOrDefault() * 3.28084; // In Feets
						var pointAPolygon = dbFunctions.GetCircle("Point(" + a.Longitude + " " + a.Latitude + ")", (operationVolumeProperties.TakeoffLandingRadius / 3.28084));
						var minAltitude = pointAElevation + wayPointsList[i].Altitude - operationVolumeProperties.AltitudeBuffer;
						if (minAltitude < pointAElevation)
						{
							minAltitude = pointAElevation;						}
						var maxAltitude = pointAElevation + wayPointsList[i + 1].Altitude + operationVolumeProperties.AltitudeBuffer;
						var pointAOperationalVolume = volumeSplitHelper.GetTakeoffLandingVolume(pointAPolygon, minAltitude, maxAltitude, wayPointsList[i].Altitude);
						volumes.Add(pointAOperationalVolume);
					}
					else
					{
						var distance = (a.GetDistanceTo(b)) * 3.28084; //distance in feet 
						if (distance > _config.VolumeSplittingLength)
						{
							lineStrings = volumeSplitHelper.SplitLineString(a.Longitude, a.Latitude, b.Longitude, b.Latitude, distance, (wayPointsList[i].Altitude), (wayPointsList[i + 1].Altitude));
							foreach (var lineString in lineStrings)
							{
								var volume = volumeSplitHelper.CalculateVolume(lineString, operationVolumeProperties, wayPointsList[i].Altitude);
								volumes.Add(volume);
							}
						}
						else
						{
							var splitGeographyPoint = dbFunctions.GetSplitGeography(a.Longitude, a.Latitude, b.Longitude, b.Latitude, 0, 1.0);
							var lineString = JsonConvert.DeserializeObject<LineStringViewModel>(splitGeographyPoint.ToString());
							lineString.AltitudePointA = wayPointsList[i].Altitude; // Altitude in Feet
							lineString.AltitudePointB = wayPointsList[i + 1].Altitude;
							var volumeNew = volumeSplitHelper.CalculateVolume(lineString, operationVolumeProperties, wayPointsList[i].Altitude);
							volumes.Add(volumeNew);
						}
					}
				}
				volumes.Add(landingOperationalVolume);
				SetOperationVolumeTimes(wayPoints.FlightSpeed, operationVolumeProperties.EffectiveTimeBegin, volumes);
				return new ObjectResult(volumes);
			}
			catch (Exception ex)
			{
				return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = ex.Message });
			}
		}

		/// <summary>
		/// Return geometry json for waypoints created
		/// </summary>
		/// <returns>string</returns>
		private string GetGeometryFromWaypointList(List<NetTopologySuite.Features.IFeature> wayPoints)
		{
			if (wayPoints.Any())
			{
				var geometry = new LineString();
				geometry.Type = "LineString";
				List<List<double?>> coords = new List<List<double?>>();
				wayPoints.ForEach(waypoint =>
				{					List<double?> coord = new List<double?>();

					//Longitude
					coord.Add(waypoint.Geometry.Coordinate.X);
					//Latitude
					coord.Add(waypoint.Geometry.Coordinate.Y);
					//Altitude
					coord.Add(waypoint.Geometry.Coordinate.Z);
					//Add to List
					coords.Add(coord);
				});
				geometry.Coordinates = coords;
				//Return geojson
				return geometry.ToJson();
			}
			return string.Empty;
		}

        /// <summary>
		/// Returns coordinates using the lat lon and elevation
		/// </summary>
		/// <returns>string</returns>
		private List<double?> GetCoordinates(double lat, double lon, double elevation, bool callElevationApi)
		{			List<double?> coord = new List<double?>();
			if (!callElevationApi)
			{
				//Longitude
				coord.Add(lon);
				//Latitude
				coord.Add(lat);
				//Altitude
				coord.Add(elevation);
			}
			else
			{
				var elevationData = elevationApi.GetMsl(lat, lon) * 3.28084; //In feet ;

				//Longitude
				coord.Add(lon);
				//Latitude
				coord.Add(lat);
				//Altitude
				coord.Add(elevationData);
			}
			return coord;
		}

		/// <summary>
		/// Returns the CoordinatesList for 3D viewer 
		/// </summary>
		/// <returns></returns>
		private string GetCoordinatesList(List<OperationVolumeViewModel> operationVolumeList)
		{
			var geometry = new LineString();
			geometry.Type = "LineString";
			List<List<double?>> coords = new List<List<double?>>();
			foreach (var operationVolume in operationVolumeList)
			{
				List<double?> coord = new List<double?>();
				var coordinates = operationVolume.OperationGeography.Coordinates.FirstOrDefault().FirstOrDefault();
				var elevationData = elevationApi.GetMsl(coordinates[1].Value, coordinates[0].Value) * 3.28084; //In Feet
				//Longitude
				coord.Add(coordinates[0].Value);
				//Latitude
				coord.Add(coordinates[1].Value);
				//Altitude

				coord.Add(elevationData);
				//Add to List
				coords.Add(coord);
			}
			geometry.Coordinates = coords;

			//Return geojson
			return geometry.ToJson();

		}

		/// <summary>
		/// Returns Contingency Plan 
		/// </summary>
		/// <returns></returns>
		private List<ContingencyPlanViewModel> SetContingencyPlan(OperationViewModel operation, bool isABOVTypeOperation)
		{

			List<ContingencyPlanViewModel> objContingencyPlans = new List<ContingencyPlanViewModel>();
			ContingencyPlanViewModel objContingencyPlanViewModel = new ContingencyPlanViewModel();

			//This is the case when there are existing contingency
			if (operation.ContingencyPlans != null)
			{
				foreach (var contingencies in operation.ContingencyPlans)
				{
					contingencies.ContingencyPlanId = 0;
					objContingencyPlans.Add(contingencies);
				}
			}
			else
			{
				//Creating a new contingency only if there is no existing contingency 
				Utm.Models.Altitude objLoiterAltitude = new Utm.Models.Altitude
				{
					AltitudeValue = 0,
					VerticalReference = VerticalReferenceTypeEnum.W84,
					UnitsOfMeasure = UomHeightTypeEnum.FT,
					Source = AltitudeSourceEnum.ONBOARD_SENSOR
				};
				objContingencyPlanViewModel.ContingencyCause = new List<string>() { EnumUtils.GetDescription(ContingencyCauseEnum.OTHER) };
				objContingencyPlanViewModel.ContingencyResponse = ContingencyResponseEnum.OTHER;
				objContingencyPlanViewModel.LoiterAltitude = objLoiterAltitude;
				objContingencyPlanViewModel.ContingencyLocationDescription = EnumUtils.GetDescription(ContingencyLocationDescription.OTHER);
				if (isABOVTypeOperation)
				{
					var abovVolume = operation.OperationVolumes.FirstOrDefault();
					objContingencyPlanViewModel.ContingencyPolygon = abovVolume.OperationGeography;
					objContingencyPlanViewModel.ValidTimeBegin = abovVolume.EffectiveTimeBegin;
					objContingencyPlanViewModel.ValidTimeEnd = abovVolume.EffectiveTimeEnd;
				}
				else
				{
					var tbovVolume = operation.OperationVolumes.LastOrDefault();
					objContingencyPlanViewModel.ContingencyPolygon = tbovVolume.OperationGeography;
					objContingencyPlanViewModel.ValidTimeBegin = tbovVolume.EffectiveTimeBegin;
					objContingencyPlanViewModel.ValidTimeEnd = tbovVolume.EffectiveTimeEnd;
				}
				objContingencyPlans.Add(objContingencyPlanViewModel);
			}
			return objContingencyPlans;
		}
        
        /// <summary>
		/// Calculate the min and max of the operation vilume based on the AGL altitude
		/// </summary>
		/// <returns></returns>
		private List<OperationVolumeViewModel> CalculateMinMaxAltitude(List<OperationVolumeViewModel> operationVolumesList)
		{
			var newOperationVolumes = new List<OperationVolumeViewModel>();
			foreach (var operationVolumes in operationVolumesList)
			{
				//Calling the elevation api to get the WGS84 altitude at that lat and lon
				var latitude = operationVolumes.OperationGeography.Coordinates[0][0][1];
				var longitude = operationVolumes.OperationGeography.Coordinates[0][0][0];

				//In Feet Adding the altitude buffer since we are just taking the first coordinates of the volume to find the elevation
				var elevation = Math.Round(((elevationApi.GetElevationData(Convert.ToDouble(latitude), Convert.ToDouble(longitude)).Results
						.Select(x => x.Elevation).FirstOrDefault() * 3.28084) - _config.AltitudeBuffer), 2);
				operationVolumes.MinAltitude.AltitudeValue = elevation;
				operationVolumes.MaxAltitude.AltitudeValue = elevation + operationVolumes.AglAltitude;
				newOperationVolumes.Add(operationVolumes);
			}
			return newOperationVolumes;
		}
	}
}