﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route(@"api/[controller]")]
    public class MessageController : BaseController
    {
        private readonly OperationStatusLogic operationStatusLogic;
        private readonly IRepository<Models.UtmInstance> utmInstanceRepository;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IMediator mediator;
        private readonly AnraMqttClient anraMqttClient;
        private readonly IRepository<Models.UtmMessage> utmMessageRepository;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly ApplicationDbContext _context;
        private readonly ILogger<MessageController> logger;
		private UtmMessageValidator _utmMessageValidator;
        private AnraConfiguration _config;
        private GridCell gridCellLogic;

        public MessageController(ApplicationDbContext context,
            ILogger<MessageController> logger,
            IRepository<Models.Operation> operationRepository,
            IRepository<UtmMessage> utmMessageRepository,
            IRepository<Models.UtmInstance> utmInstanceRepository,
            OperationStatusLogic operationStatusLogic,
            UtmMessageValidator utmMessageValidator,
            IMediator mediator,
            AnraConfiguration configuration,
            GridCell gridCellLogic,
            DatabaseFunctions dbFunctions,
            AnraMqttClient anraMqttClient)
        {
            _context = context;
            _config = configuration;
            this.logger = logger;
            this.operationRepository = operationRepository;
            this.utmMessageRepository = utmMessageRepository;
            this.anraMqttClient = anraMqttClient;
            this.mediator = mediator;
            this.dbFunctions = dbFunctions;
            this.utmInstanceRepository = utmInstanceRepository;
            this.operationStatusLogic = operationStatusLogic;
            _utmMessageValidator = utmMessageValidator;
            this.gridCellLogic = gridCellLogic;
        }

        /// <summary>
        /// Opeation specific Message.
        /// </summary>
        /// <param name="message">The message being sent</param>
        /// <param name="gufi">Operation Gufi</param>
        /// <remarks>Allows POST a Utm message for an opeation.</remarks>
        /// <response code="201">Message data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")]  
        [Route(@"/operation/{gufi}/message")]
        [SwaggerOperation(@"PostMessage"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult PostUtmMessage([FromRoute]Guid gufi, [FromBody]UtmMessageViewModel message)
        {
            if (!_utmMessageValidator.IsValid(message.Adapt<UTMMessage>(), message_id: message.MessageId))
            {                
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Join(@", ", _utmMessageValidator.Errors), Id = gufi.ToString() });             
            }

            if (message.Contingency != null)
            {
                var contingencyCount = utmMessageRepository.GetAll().Count(p => p.Gufi == gufi && p.ContingencyPlanUid.HasValue) + 1;

                //always associating new copy of existing contingency with the message.
                message.Contingency.ContingencyPlanId = 0;
                message.Contingency.ContingencyId = contingencyCount;
            }

            message.MessageId = Guid.NewGuid();
            message.SentTime = DateTime.UtcNow;

            var model = message.Adapt<UtmMessage>();
            model.LastKnownPosition = operationStatusLogic.GetLastKnownPosition(gufi).Adapt<TelemetryMessage>();
            utmMessageRepository.Add(model);

            //Get it back from database so that contingency id is set
            var utmMessage = utmMessageRepository.GetAll().FirstOrDefault(p => p.MessageId == model.MessageId).Adapt<UTMMessage>();

            var ops = operationRepository.Find(gufi);

            mediator.Send(new AsyncUtmMessageNotification(model.Adapt<UTMMessage>(), gridCellLogic.GetGridCellOperators(ops.SlippyTileData)));

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.OK, Message = @"Message created/" + message.MessageId, Id = message.MessageId.ToString() });
        }

        /// <summary>
        /// Get all Messages for an operation
        /// </summary>
        /// <remarks>Allows querying for utm message.</remarks>
        /// <param name="gufi">Operation Gufi</param>
        /// <response code="200">Successful data request. Response includes requested data.</response>
        /// <response code="400">Bad request. Typically validation error. Fix your request and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpGet]
        [Authorize("uss:read-message")] 
        [Route(@"/operation/{gufi}/messages")]
        [SwaggerOperation(nameof(GetUtmMessagesByGufi)), SwaggerResponse(200, type: typeof(List<UTMMessage>))]
        public virtual IActionResult GetUtmMessagesByGufi([FromRoute]Guid gufi)
        {

            var result = utmMessageRepository.GetAll().Where(p => p.Gufi == gufi);
            var msgs = result.Adapt<List<UTMMessage>>();
            return new ObjectResult(msgs);

        }

        /// <summary>
        /// Request Position Reports Start
        /// </summary>
        /// <param name="gufi">Operation Gufi</param>
        /// <param name="externalGufi">Gufi of other USS Operation</param>
        /// <remarks>Allows requesting position reports for an opeation.</remarks>
        /// <response code="201">Message data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")]
        [Route(@"/operation/{gufi}/message/{externalGufi}/positions/start")]
        [SwaggerOperation(@"RequestPositionReportStart"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult RequestPositionReportStart([FromRoute]Guid gufi, [FromRoute]Guid externalGufi)
        {
            var operation = operationRepository.Find(gufi);
            var externalOperation = operationRepository.Find(externalGufi);
            if (operation == null || externalOperation == null)
            {
                return ApiErrorResponse("Invalid Operation");
            }

            var sourceUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(operation.UssName)).FirstOrDefault();
            var targetUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalOperation.UssName)).FirstOrDefault();

            if (sourceUtmInstance == null || targetUtmInstance == null)
            {
                return ApiErrorResponse("Invalid USS");
            }

            var message = new UtmMessageViewModel
            {
                MessageId = Guid.NewGuid(),
                Gufi = externalGufi,
                UssName = operation.UssName,
                SentTime = DateTime.UtcNow,
                Severity = Severity.NOTICE,
                MessageType = MessageTypeEnum.PERIODIC_POSITION_REPORTS_START,
                Callback = sourceUtmInstance.UssBaseCallbackUrl,
                FreeText = $"ANRA USS service requests that you please send periodic position reports."
            };

            var model = message.Adapt<UtmMessage>();
            utmMessageRepository.Add(model);

            //create GridCellOperatorResponse object manually
            var targetOperator = new GridCellOperatorResponse
            {
                Uss = targetUtmInstance.UssName,
                UssBaseurl = targetUtmInstance.UssBaseCallbackUrl,
                Version = 0,
                Timestamp = DateTime.UtcNow.ToString(),
                MinimumOperationTimestamp = DateTime.UtcNow,
                MaximumOperationTimestamp = DateTime.UtcNow,
                AnnouncementLevel = AnnouncementLevelEnum.ALL,
                Zoom = 0,
                X = 0,
                Y = 0,
                Operations = new List<GridCellOperationResponse>()
            };

            mediator.Send(new AsyncUtmMessageNotification(model.Adapt<UTMMessage>(), new List<GridCellOperatorResponse> { targetOperator }));

            return ApiResponse(StatusCodes.Status200OK, "Position reports requested.");
        }

        /// <summary>
        /// Request External Position Reports Start
        /// </summary>        
        /// <param name="externalGufi">Gufi of other USS Operation</param>
        /// <remarks>Allows requesting position reports for an opeation.</remarks>
        /// <response code="201">Message data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")] 
        [Route(@"/{externalGufi}/positions/start")]
        [SwaggerOperation(@"RequestExternalPositionReportStart"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult RequestExternalPositionReportStart([FromRoute]Guid externalGufi)
        {
            var existingUtmMessage = utmMessageRepository.GetAll().Where(x => x.Gufi.Equals(externalGufi)).OrderByDescending(x => x.SentTime).FirstOrDefault();

            if(existingUtmMessage != null && existingUtmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_START.ToString()))
            {
                return ApiErrorResponse("Start position report already requested for this operation.");
            }

            var externalOperation = operationRepository.Find(externalGufi);
            if (externalOperation == null)
            {
                return ApiErrorResponse("Invalid Operation");
            }

            var sourceUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(_config.ClientId)).FirstOrDefault();
            var targetUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalOperation.UssName)).FirstOrDefault();

            if (sourceUtmInstance == null || targetUtmInstance == null)
            {
                return ApiErrorResponse("Invalid USS");
            }

            var message = new UtmMessageViewModel
            {
                MessageId = Guid.NewGuid(),
                Gufi = externalGufi,
                UssName = sourceUtmInstance.UssName,
                SentTime = DateTime.UtcNow,
                Severity = Severity.NOTICE,
                MessageType = MessageTypeEnum.PERIODIC_POSITION_REPORTS_START,
                Callback = sourceUtmInstance.UssBaseCallbackUrl,
                FreeText = $"ANRA USS service requests that you please send periodic position reports."
            };

            var model = message.Adapt<UtmMessage>();
            utmMessageRepository.Add(model);

            //create GridCellOperatorResponse object manually
            var targetOperator = new GridCellOperatorResponse
            {
                Uss = targetUtmInstance.UssName,
                UssBaseurl = targetUtmInstance.UssBaseCallbackUrl,
                Version = 0,
                Timestamp = DateTime.UtcNow.ToString(),
                MinimumOperationTimestamp = DateTime.UtcNow,
                MaximumOperationTimestamp = DateTime.UtcNow,
                AnnouncementLevel = AnnouncementLevelEnum.ALL,
                Zoom = 0,
                X = 0,
                Y = 0,
                Operations = new List<GridCellOperationResponse>()
            };

            mediator.Send(new AsyncUtmMessageNotification(model.Adapt<UTMMessage>(), new List<GridCellOperatorResponse> { targetOperator }));

            return ApiResponse(StatusCodes.Status200OK, "Position reports requested.");
        }

        /// <summary>
        /// Request External Position Reports End
        /// </summary>        
        /// <param name="externalGufi">Gufi of other USS Operation</param>
        /// <remarks>Allows requesting position reports for an opeation.</remarks>
        /// <response code="201">Message data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")]
        [Route(@"/{externalGufi}/positions/end")]
        [SwaggerOperation(@"RequestExternalPositionReportEnd"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult RequestExternalPositionReportEnd([FromRoute]Guid externalGufi)
        {
            var existingUtmMessage = utmMessageRepository.GetAll().Where(x => x.Gufi.Equals(externalGufi)).OrderByDescending(x => x.SentTime).FirstOrDefault();

            if (existingUtmMessage != null && existingUtmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_END.ToString()))
            {
                return ApiErrorResponse("Stop position report already requested for this operation.");
            }

            var externalOperation = operationRepository.Find(externalGufi);
            if (externalOperation == null)
            {
                return ApiErrorResponse("Invalid Operation");
            }

            var sourceUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(_config.ClientId)).FirstOrDefault();
            var targetUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalOperation.UssName)).FirstOrDefault();

            if (sourceUtmInstance == null || targetUtmInstance == null)
            {
                return ApiErrorResponse("Invalid USS");
            }

            var message = new UtmMessageViewModel
            {
                MessageId = Guid.NewGuid(),
                Gufi = externalGufi,
                UssName = sourceUtmInstance.UssName,
                SentTime = DateTime.UtcNow,
                Severity = Severity.NOTICE,
                MessageType = MessageTypeEnum.PERIODIC_POSITION_REPORTS_END,
                Callback = sourceUtmInstance.UssBaseCallbackUrl,
                FreeText = $"ANRA USS service requests that you please stop periodic position reports."
            };

            var model = message.Adapt<UtmMessage>();
            utmMessageRepository.Add(model);

            //create GridCellOperatorResponse object manually
            var targetOperator = new GridCellOperatorResponse
            {
                Uss = targetUtmInstance.UssName,
                UssBaseurl = targetUtmInstance.UssBaseCallbackUrl,
                Version = 0,
                Timestamp = DateTime.UtcNow.ToString(),
                MinimumOperationTimestamp = DateTime.UtcNow,
                MaximumOperationTimestamp = DateTime.UtcNow,
                AnnouncementLevel = AnnouncementLevelEnum.ALL,
                Zoom = 0,
                X = 0,
                Y = 0,
                Operations = new List<GridCellOperationResponse>()
            };

            mediator.Send(new AsyncUtmMessageNotification(model.Adapt<UTMMessage>(), new List<GridCellOperatorResponse> { targetOperator }));

            return ApiResponse(StatusCodes.Status200OK, "Position reports requested.");
        }

        /// <summary>
        /// Request Position Reports End
        /// </summary>
        /// <param name="gufi">Operation Gufi</param>
        /// <param name="externalGufi">Gufi of other USS Operation</param>
        /// <remarks>Allows stopping position reports for an opeation.</remarks>
        /// <response code="201">Message data received.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-message")]
        [Route(@"/operation/{gufi}/message/{externalGufi}/positions/end")]
        [SwaggerOperation(@"RequestPositionReportEnd"), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult RequestPositionReportEnd([FromRoute]Guid gufi, [FromRoute]Guid externalGufi)
        {
            var operation = operationRepository.Find(gufi);
            var externalOperation = operationRepository.Find(externalGufi);
            if (operation == null || externalOperation == null)
            {
                return ApiErrorResponse("Invalid Operation");
            }

            var sourceUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(operation.UssName)).FirstOrDefault();
            var targetUtmInstance = utmInstanceRepository.GetAll().Where(x => x.UssName.Equals(externalOperation.UssName)).FirstOrDefault();

            if (sourceUtmInstance == null || targetUtmInstance == null)
            {
                return ApiErrorResponse("Invalid USS");
            }

            var message = new UtmMessageViewModel
            {
                MessageId = Guid.NewGuid(),
                Gufi = externalGufi,
                UssName = operation.UssName,
                SentTime = DateTime.UtcNow,
                Severity = Severity.NOTICE,
                MessageType = MessageTypeEnum.PERIODIC_POSITION_REPORTS_END,
                Callback = sourceUtmInstance.UssBaseCallbackUrl,
                FreeText = $"ANRA USS service requests that you please stop periodic position reports."
            };

            var model = message.Adapt<UtmMessage>();
            utmMessageRepository.Add(model);

            //create GridCellOperatorResponse object manually
            var targetOperator = new GridCellOperatorResponse
            {
                Uss = targetUtmInstance.UssName,
                UssBaseurl = targetUtmInstance.UssBaseCallbackUrl,
                Version = 0,
                Timestamp = DateTime.UtcNow.ToString(),
                MinimumOperationTimestamp = DateTime.UtcNow,
                MaximumOperationTimestamp = DateTime.UtcNow,
                AnnouncementLevel = AnnouncementLevelEnum.ALL,
                Zoom = 0,
                X = 0,
                Y = 0,
                Operations = new List<GridCellOperationResponse>()
            };

            mediator.Send(new AsyncUtmMessageNotification(model.Adapt<UTMMessage>(), new List<GridCellOperatorResponse> { targetOperator }));

            return ApiResponse(StatusCodes.Status200OK, "Position reports stopped.");
        }
    }
}