﻿using AnraUssServices.Common;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Net;

namespace AnraUssServices.Controllers
{
    /// <summary>
    /// USS/Operator Interface
    /// </summary>
    [Route(@"api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class OperationStatusController : BaseController
    {
        private readonly ILogger<OperationStatusController> logger;
        private readonly UtmMessenger utmMessenger;
        private readonly OperationStatusLogic operationStatusLogic;
        private readonly IRepository<Models.Operation> operationRepository;

        public OperationStatusController(ApplicationDbContext context,
            ILogger<OperationStatusController> logger,
            UtmMessenger utmMessenger,
            IRepository<Models.Operation> operationRepository,
            OperationStatusLogic operationStatusLogic)
        {
            this.operationRepository = operationRepository;
            this.operationStatusLogic = operationStatusLogic;
            this.logger = logger;
            this.utmMessenger = utmMessenger;
        }

        /// <summary>
        /// Request Operation Activation by operation ID.
        /// </summary>
        /// <remarks>Uss Operator Will Send Operation Activation Request By Sending Operation Id i.e. Gufi</remarks>
        /// <param name="gufi">Post by gufi</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-anra-operation")]
        [Route(@"/operation/{gufi}/activate")]
        [SwaggerOperation(nameof(ActivateOperation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult ActivateOperation([FromRoute]Guid gufi)
        {
            try
            {
                return operationStatusLogic.ToActivateAsync(gufi).Result;
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
            }
        }

        /// <summary>
        /// Request Operation Accept by operation ID.
        /// </summary>
        /// <remarks>Uss Operator Will Send Operation Aceptation Request By Sending Operation Id i.e. Gufi</remarks>
        /// <param name="gufi">Post by gufi</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:read-anra-operation")]
        [Route(@"/operation/{gufi}/atcaction")]
        [SwaggerOperation(nameof(AcceptOperation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult AcceptOperation([FromRoute]Guid gufi,[FromBody] AtcActionItem atcAction)
        {
            try
            {
                if (atcAction.Action.Equals("APPROVE"))
                {
                    return operationStatusLogic.ToAcceptedAsync(gufi,atcAction.Comments).Result;
                }
                else
                {
                    return operationStatusLogic.ToClosedAsync(gufi,atcAction.Comments).Result;
                }
                
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse() { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
            }
        }

        /// <summary>
        /// Request Operation Closing by operation ID.
        /// </summary>
        /// <remarks>Uss Operator Will Send Operation Closing Request By Sending Operation Id i.e. Gufi</remarks>
        /// <param name="gufi">Operation Gufi</param>
        /// <response code="201">OK</response>
        /// <response code="400">Bad request. Typically validation error. Fix and retry.</response>
        /// <response code="429">Too many recent requests from you. Wait to make further queries.</response>
        [HttpPost, ApiValidationFilter]
        [Authorize("uss:write-anra-operation")]
        [Route(@"/operation/{gufi}/close")]
        [SwaggerOperation(nameof(CloseOperation)), SwaggerResponse(200, type: typeof(UTMRestResponse))]
        public virtual IActionResult CloseOperation([FromRoute]Guid gufi)
        {
            try
            {
                return operationStatusLogic.ToClosedAsync(gufi).Result;
            }
            catch (Exception ex)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.InternalServerError, Message = "Internal Server Error" });
            }
        }
    }
}