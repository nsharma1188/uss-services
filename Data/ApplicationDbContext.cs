﻿using AnraUssServices.Models;
using AnraUssServices.Models.Audits;
using AnraUssServices.Models.Collisions;
using AnraUssServices.Models.Detects;
using AnraUssServices.Models.DetectSubscribers;
using AnraUssServices.Models.Signup;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;

namespace AnraUssServices.Data
{
    public sealed class ApplicationDbContext : DbContext
    {
        public DbSet<Operation> Operations { get; set; }
        public DbSet<OperationVolume> OperationVolumes { get; set; }
        public DbSet<ContingencyPlan> ContingencyPlans { get; set; }
        public DbSet<ConstraintMessage> ConstraintMessages { get; set; }
        public DbSet<NegotiationMessage> NegotiationMessages { get; set; }
        public DbSet<UtmMessage> UtmMessages { get; set; }
        public DbSet<UtmInstance> UtmInstances { get; set; }
        public DbSet<TelemetryMessage> TelemetryMessages { get; set; }
        public DbSet<Drone> Drones { get; set; }
        public DbSet<ConformanceLog> ConformanceLogs { get; set; }
        public DbSet<PriorityElement> PriorityElements { get; set; }        
        public DbSet<Notam> Notams { get; set; }
        public DbSet<NotamArea> NotamAreas { get; set; }
        public DbSet<Urep> Ureps { get; set; }
        public DbSet<Pointout> Pointouts { get; set; }
        public DbSet<UasRegistration> UasRegistrations { get; set; }

        public DbSet<VehicleData> vehicleData { get; set; }
        public DbSet<VehicleClassDetail> vehicleClassDetail { get; set; }
        public DbSet<VehicleEngineDetail> vehicleEngineDetail { get; set; }
        public DbSet<VehicleProperty> vehicleProperties { get; set; }
        public DbSet<VehicleRegistration> vehicleRegistration { get; set; }
        public DbSet<VehicleType> vehicleType { get; set; }
        public DbSet<UserDroneAssociation> UserDroneAssociations { get; set; }
        public DbSet<Collision> Collisions { get; set; }
        public DbSet<Detect> Detects { get; set; }
        public DbSet<OperationConflict> OperationConflicts { get; set; }
        public DbSet<DetectSubscriber> DetectSubscribers { get; set; }
        public DbSet<NegotiationAgreement> NegotiationAgreements { get; set; }
        public DbSet<ServiceArea> ServiceAreas { get; set; }
		public DbSet<Signup> Signups { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LanguageResource> LanguageResources { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<Audit> Audits { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Operation>().ToTable(nameof(Operation)).HasKey(m => m.Gufi);

            builder.Entity<OperationVolume>().ToTable(nameof(OperationVolume)).HasKey(m => m.OperationVolumeUid);
            builder.Entity<ContingencyPlan>().ToTable(nameof(ContingencyPlan)).HasKey(m => m.ContingencyPlanUid);
            builder.Entity<ConstraintMessage>().ToTable(nameof(ConstraintMessage)).HasKey(m => m.MessageId);
            builder.Entity<NegotiationMessage>().ToTable(nameof(NegotiationMessage)).HasKey(m => m.NegotiationMessageUid);
            builder.Entity<UtmMessage>().ToTable(nameof(UtmMessage)).HasKey(m => m.MessageId);
            builder.Entity<UtmInstance>().ToTable(nameof(UtmInstance)).HasKey(m => m.UssInstanceId);
            builder.Entity<TelemetryMessage>().ToTable(nameof(TelemetryMessage)).HasKey(m => m.TelemetryMessageId);
            builder.Entity<Drone>().ToTable(nameof(Drone)).HasKey(m => m.Uid);
            builder.Entity<LocalNetwork>().ToTable(nameof(LocalNetwork)).HasKey(m => m.LocalNetworkId);
            builder.Entity<ConformanceLog>().ToTable(nameof(ConformanceLog)).HasKey(m => m.ConformanceLogUid);
            builder.Entity<PriorityElement>().ToTable(nameof(PriorityElement)).HasKey(m => m.PriorityElementUid);            
            builder.Entity<Notam>().ToTable(nameof(Notam)).HasKey(m => m.NotamNumber);
            builder.Entity<NotamArea>().ToTable(nameof(NotamArea)).HasKey(m => m.NotamAreaUid);
            builder.Entity<Urep>().ToTable(nameof(Urep)).HasKey(m => m.UrepUid);
            builder.Entity<Pointout>().ToTable(nameof(Pointout)).HasKey(m => m.PointoutUid);
            builder.Entity<UasRegistration>().ToTable(nameof(UasRegistration)).HasKey(m => m.UasRegistrationUid);

            builder.Entity<VehicleData>().ToTable(nameof(VehicleData)).HasKey(m => m.VehicleDataUid);
            builder.Entity<VehicleClassDetail>().ToTable(nameof(VehicleClassDetail)).HasKey(m => m.VehicleClassDetailUid);
            builder.Entity<VehicleEngineDetail>().ToTable(nameof(VehicleEngineDetail)).HasKey(m => m.VehicleEngineDetailsUid);
            builder.Entity<VehicleProperty>().ToTable(nameof(VehicleProperty)).HasKey(m => m.VehiclePropertiesUid);
            builder.Entity<VehicleRegistration>().ToTable(nameof(VehicleRegistration)).HasKey(m => m.VehicleRegistrationUid);
            builder.Entity<VehicleType>().ToTable(nameof(VehicleType)).HasKey(m => m.VehicleTypeUid);
            builder.Entity<UserDroneAssociation>().ToTable(nameof(UserDroneAssociation)).HasKey(m => m.UserDroneAssociationUid);
            builder.Entity<Collision>().ToTable(nameof(Collision)).HasKey(p => p.CollisionUid);
            builder.Entity<Detect>().ToTable(nameof(Detect)).HasKey(p => p.DetectId);
            builder.Entity<OperationConflict>().ToTable(nameof(OperationConflict)).HasKey(p => p.OperationConflictUid);
            builder.Entity<DetectSubscriber>().ToTable(nameof(DetectSubscriber)).HasKey(p => p.SubscriptionId);
            builder.Entity<NegotiationAgreement>().ToTable(nameof(NegotiationAgreement)).HasKey(p => p.NegotiationAgreementUid);
            builder.Entity<ServiceArea>().ToTable(nameof(ServiceArea)).HasKey(p => p.Id);
			builder.Entity<Signup>().ToTable(nameof(Signup)).HasKey(p => p.SignupUid);
            builder.Entity<Language>().ToTable(nameof(Language)).HasKey(p => p.LanguageGufi);
            //builder.Entity<LocaleStringResource>().ToTable(nameof(LocaleStringResource)).HasKey(p => p.ResourceId); //will be removed
            builder.Entity<LanguageResource>().ToTable(nameof(LanguageResource)).HasKey(p => p.ResourceId);
            builder.Entity<Issue>().ToTable(nameof(Issue)).HasKey(p => p.Id);
            builder.Entity<Audit>().ToTable(nameof(Audit)).HasKey(p => p.AuditUid);

            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("./appsettings.json")
                .AddEnvironmentVariables();

            var configuration = builder.Build();

            var sqlConnectionString = configuration["ConnectionStrings:DataAccessPostgreSqlProvider"];

            optionsBuilder.UseNpgsql(sqlConnectionString);
        }

        public override int SaveChanges()
        {
            //ChangeTracker.DetectChanges(); //Commneting for now due to migration issue, will revert after fix.

            UpdateUpdatedProperty<Drone>();
            UpdateUpdatedProperty<Operation>();
            UpdateUpdatedProperty<OperationVolume>();
            UpdateUpdatedProperty<ContingencyPlan>();
            UpdateUpdatedProperty<ConstraintMessage>();
            UpdateUpdatedProperty<NegotiationMessage>();
            UpdateUpdatedProperty<TelemetryMessage>();
            UpdateUpdatedProperty<UtmMessage>();
            UpdateUpdatedProperty<UtmInstance>();
            UpdateUpdatedProperty<ConformanceLog>();
            UpdateUpdatedProperty<PriorityElement>();            
            UpdateUpdatedProperty<Notam>();
            UpdateUpdatedProperty<NotamArea>();
            UpdateUpdatedProperty<Urep>();
            UpdateUpdatedProperty<Pointout>();
            UpdateUpdatedProperty<UasRegistration>();
            UpdateUpdatedProperty<UserDroneAssociation>();
            UpdateUpdatedProperty<Collision>();
            UpdateUpdatedProperty<Detect>();
            UpdateUpdatedProperty<OperationConflict>();
            UpdateUpdatedProperty<DetectSubscriber>();
            UpdateUpdatedProperty<NegotiationAgreement>();
            UpdateUpdatedProperty<ServiceArea>();
			UpdateUpdatedProperty<Signup>();
            UpdateUpdatedProperty<Language>();
            //UpdateUpdatedProperty<LocaleStringResource>();
            UpdateUpdatedProperty<LanguageResource>();
            UpdateUpdatedProperty<Issue>();
            UpdateUpdatedProperty<Audit>();

            return base.SaveChanges();
        }

        private void UpdateUpdatedProperty<T>() where T : class
        {
            var addedSourceInfo = ChangeTracker.Entries<T>().Where(e => e.State == EntityState.Added);

            foreach (var entry in addedSourceInfo)
            {
                entry.Property("DateCreated").CurrentValue = DateTime.UtcNow;
                entry.Property("DateModified").CurrentValue = DateTime.UtcNow;
            }

            var modifiedSourceInfo = ChangeTracker.Entries<T>().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("DateModified").CurrentValue = DateTime.UtcNow;
            }
        }
    }
}