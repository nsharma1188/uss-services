﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Api;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AnraUssServices.Data.Seeding
{
    public class SeedData
    {
        private static IServiceScope _serviceScope;
        private static ApplicationDbContext Context => _serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
        private static AnraConfiguration Configuration => _serviceScope.ServiceProvider.GetService<AnraConfiguration>();
        private static FimsTokenProvider FimsAuthentication => _serviceScope.ServiceProvider.GetService<FimsTokenProvider>();
        private static AnraHttpClient AnraHttpClient => _serviceScope.ServiceProvider.GetService<AnraHttpClient>();
        private static UssDiscoveryApi UssDiscoveryApi => _serviceScope.ServiceProvider.GetService<UssDiscoveryApi>();
        private static ILogger<string> Logger => _serviceScope.ServiceProvider.GetService<ILogger<string>>();

        public static async Task EnsureSeedDataAsync(IServiceProvider serviceProvider)
        {
            try
            {
                _serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (_serviceScope)
                {
                    var db = _serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                    db.Database.EnsureCreated();

                    //Initialize LUN
                    //await SeedDefaultUtmInstance.CreateAsync(Context, Configuration, UssDiscoveryApi);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Seeding Data::: Exception {0}", ex);
            }
        }
    }
}