﻿using AnraUssServices.Common;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.ViewModel.Contacts;
using Mapster;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Data.Seeding
{
    internal class SeedDefaultUtmInstance
    {
        public static async Task CreateAsync(ApplicationDbContext context, AnraConfiguration configuration, UssDiscoveryApi ussDiscoveryApi)
        {
            if (!configuration.IsUtmEnabled)
                return;

            if (!(context.UtmInstances.Any(u => u.UssInstanceId == configuration.DefaultUssInstanceId)))
            {
                var defaultCoverageArea = new List<NpgsqlTypes.NpgsqlPoint> {
                        {new NpgsqlTypes.NpgsqlPoint(-102.451876401901, 47.6513536786375)},
                        {new NpgsqlTypes.NpgsqlPoint(-102.458742856979, 47.9732173662051)},
                        {new NpgsqlTypes.NpgsqlPoint(-102.131899595261, 47.9741367460231)},
                        {new NpgsqlTypes.NpgsqlPoint(-102.126406431198, 47.6300719498239)},
                        {new NpgsqlTypes.NpgsqlPoint(-102.451876401901, 47.6513536786375)}
                };

                var defaultUtmInstance = new UtmInstance
                {
                    UssInstanceId = configuration.DefaultUssInstanceId,
                    UssName = configuration.ClientId,
                    UssBaseCallbackUrl = configuration.UssServiceUrl + "/uss",
                    TimeAvailableBegin = DateTime.UtcNow,
                    TimeAvailableEnd = DateTime.UtcNow.AddYears(1),
                    CoverageArea = new NpgsqlTypes.NpgsqlPolygon(defaultCoverageArea),
                    Contact = JsonConvert.SerializeObject(new PersonOrOrganizationViewModel
                    {
                        Name = "Amit Ganjoo",
                        EmailAddresses = new List<string> { "support@anratechnologies.com" },
                        PhoneNumbers = new List<string> { "777-888-8888" }
                    }),
                    Notes = "Anra USS Master",
                    UssInformationalUrl = configuration.UssServiceUrl,
                    UssOpenapiUrl = configuration.UssServiceUrl,
                    UssRegistrationUrl = configuration.UssServiceUrl
                };

                context.UtmInstances.Add(defaultUtmInstance);

                context.SaveChanges();
            }

            if (configuration.IsUtmEnabled)
            {
                InitLun(context, ussDiscoveryApi);
            }
        }

        private static void InitLun(ApplicationDbContext context, UssDiscoveryApi ussDiscoveryApi)
        {
            var resultItems = ussDiscoveryApi.GetUssList();

            if (resultItems != null && resultItems.Any())
            {
                foreach (var utmInstance in resultItems)
                {
                    var existingItem = context.UtmInstances.FirstOrDefault(p => p.UssInstanceId == utmInstance.UssInstanceId);
                    if (existingItem == null)
                    {
                        context.UtmInstances.Add(utmInstance.Adapt<UtmInstance>());
                        context.SaveChanges();
                    }
                    else
                    {
                        utmInstance.Adapt(existingItem);
                        context.Update(existingItem);
                        context.SaveChanges();
                    }
                }
            }
        }
    }
}