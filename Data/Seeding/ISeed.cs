﻿using System.Threading.Tasks;

namespace AnraUssServices.Data.Seeding
{
    internal interface ISeedData
    {
        Task CreateAsync();
    }
}