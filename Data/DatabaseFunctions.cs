﻿using AnraUssServices.Common;
using AnraUssServices.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Data
{
	public class DatabaseFunctions
	{
		private readonly ApplicationDbContext context;
		AnraConfiguration anraConfiguration;

		public DatabaseFunctions(ApplicationDbContext context, AnraConfiguration anraConfiguration)
		{
			this.context = context;
			this.anraConfiguration = anraConfiguration;
		}

		public List<UtmInstance> GetLocalUssNetworkByGufi(Guid gufi)
		{
			return context.Set<UtmInstance>().FromSql<UtmInstance>("select * from public.__getnetworkbygufi({0})", gufi).ToList();
		}

		/// <summary>
		/// Excludes Anra USS Instance
		/// </summary>
		/// <param name="UtmInstanceId"></param>
		/// <returns></returns>
		public List<UtmInstance> GetLocalUssNetworkByUtmInstanceId(Guid UtmInstanceId)
		{
			var data = context.Set<UtmInstance>().FromSql<UtmInstance>("select * from public.__getnetworkbyinstanceid({0})", UtmInstanceId).ToList();
			return data.Where(p => p.UssName != anraConfiguration.ClientId).ToList();
		}

		/// <summary>
		/// Includes Anra USS Instance also
		/// </summary>
		/// <param name="UtmInstanceId"></param>
		/// <returns></returns>
		public List<UtmInstance> GetLocalUssNetworkByUtmInstanceIdInclusive(Guid UtmInstanceId)
		{
			return context.Set<UtmInstance>().FromSql<UtmInstance>("select * from public.__getnetworkbyinstanceid({0})", UtmInstanceId).ToList();
		}

		public IEnumerable<Operation> GetActiveOperations(string searchby, NpgsqlTypes.NpgsqlPoint? location, string drone, int radius)
		{
			return context.Set<Operation>().FromSql<Operation>("select * from public.__getactiveoperations({0},{1},{2},{3})", searchby, location, drone, radius).ToList().Distinct();
		}

        public IEnumerable<Operation> GetActiveOperations(Guid? ussinstanceid, Guid? orgid, NpgsqlTypes.NpgsqlPoint? location, int radius, int reclimit)
        {
            var locationPoint = location.HasValue && location.Value.X.Equals(0) && location.Value.Y.Equals(0) ? null : location;
            var orgId = orgid.Equals(Guid.Empty) ? null : orgid;
            var ussInstanceId = ussinstanceid.Equals(Guid.Empty) ? null : ussinstanceid;
            //Set default reclimit to 50 if not passed
            reclimit = reclimit > 0 ? reclimit : 50;
            return context.Set<Operation>().FromSql<Operation>("select * from public.__getoperationswithinuss({0},{1},{2},{3},{4})", locationPoint,ussInstanceId, radius, reclimit, orgId).ToList();
        }

        public IEnumerable<Operation> GetOperationsByCriteria(string registrationId, DateTime? submit_time, string state, int? distance, string reference_Point)
        {
            return context.Set<Operation>().FromSql<Operation>("select * from public.__getoperationsbycriteria({0},{1},{2},{3},{4})", registrationId, submit_time, state, distance, reference_Point).ToList();
        }

  //      public IEnumerable<Operation> GetActiveOperations()
		//{
		//	return context.Set<Operation>().FromSql<Operation>("select * from public.__getrunningoperations()").ToList();
		//}

		public IEnumerable<Notam> ValidateOperationWithNotam(NpgsqlTypes.NpgsqlPolygon OperationGeography, DateTime BeginDt, DateTime EndDt, double MinAltitude, double MaxAltitude)
		{
			return context.Set<Notam>().FromSql<Notam>("select * from public.__validateoperationwithtfr({0},{1},{2},{3},{4})", OperationGeography, BeginDt, EndDt, MinAltitude, MaxAltitude).ToList();
		}

        public IEnumerable<ConstraintMessage> ValidateOperationWithUVR(NpgsqlTypes.NpgsqlPolygon OperationGeography, DateTime BeginDt, DateTime EndDt, double MinAltitude, double MaxAltitude)
        {
            return context.Set<ConstraintMessage>().FromSql<ConstraintMessage>("select * from public.__validateoperationwithuvr({0},{1},{2},{3},{4})", OperationGeography, BeginDt, EndDt, MinAltitude, MaxAltitude).ToList();
        }

        public IEnumerable<Operation> GetConflictingOperations(Guid gufi)
		{
			return context.Set<Operation>().FromSql("select * from public.__checkoperationintersection({0})", gufi).AsEnumerable().ToList().Distinct();
		}

        public IEnumerable<Operation> ValidateDroneAssignation(Guid droneid, DateTime BeginDt, DateTime EndDt)
        {
            return context.Set<Operation>().FromSql("select * from public.__validatedroneassignation({0},{1},{2})", droneid,BeginDt,EndDt).AsEnumerable().ToList().Distinct();
        }

        public IEnumerable<Operation> GetIntersectingOperations(Guid message_id)
		{
			return context.Set<Operation>().FromSql("select * from public.__getintersectingoperations({0})", message_id).AsEnumerable().ToList().Distinct();
		}

		public IEnumerable<Operation> GetOperationsByGridData(int zoom, int x, int y)
		{
			return context.Set<Operation>().FromSql("select * from public.__getoperationsbygriddata({0},{1},{2})", zoom, x, y).AsEnumerable().ToList().Distinct();
		}

		//public List<UtmInstance> GetIntersectingLocalUssNetworkByDR(NpgsqlTypes.NpgsqlPolygon ConstraintGeography, DateTime BeginDt, DateTime EndDt)
		//{
		//	return context.Set<UtmInstance>().FromSql<UtmInstance>("select * from public.__getintersectingusss({0},{1},{2})", ConstraintGeography, BeginDt, EndDt).ToList();
		//}

		public object GetSplitGeography(double x0, double y0, double x1, double y1, double startFraction, double endFraction)
		{
			string output = "";
			using (var conn = new NpgsqlConnection(anraConfiguration.ConnectionString))
			{
				conn.Open();
				NpgsqlCommand cmd = new NpgsqlCommand("CREATE EXTENSION postgis", conn);

				cmd = new NpgsqlCommand("SELECT ST_AsGeoJSON(ST_Line_SubString(ST_GeomFromGeoJSON('{\"type\": \"LineString\",\"coordinates\": [["+x0+","+y0+"],["+x1+","+y1+"]]}'),"+ startFraction +"," + endFraction +"))", conn);

				NpgsqlDataReader dr = cmd.ExecuteReader();
				
				while (dr.Read())
				{
					output = dr[0].ToString();
				}
			}
				return output;
		}


		public List<Utm.Models.Polygon> GetPolygon(string polygon)
		{
			List<Utm.Models.Polygon> outputPolygon = new List<Utm.Models.Polygon>();

			using (var conn = new NpgsqlConnection(anraConfiguration.ConnectionString))
			{
				conn.Open();
				//Query to change the polygon geography into geoJSON
				//NpgsqlCommand cmd = new NpgsqlCommand("select ST_AsGeoJSON((ST_DUMP(ST_Buffer('" + polygon + "', -0.00001))).geom) as polygon", conn);
				NpgsqlCommand cmd = new NpgsqlCommand("SELECT ST_AsGeoJSON((ST_DUMP(ST_GeometryFromText('" + polygon + "'))).geom) AS polygon", conn);

				NpgsqlDataReader dr = cmd.ExecuteReader();
				while (dr.Read())
				{

					var output = JsonConvert.DeserializeObject<Utm.Models.Polygon>(dr["polygon"].ToString());
					outputPolygon.Add(new Utm.Models.Polygon
					{
						Coordinates = output.Coordinates,
						Type = "Polygon"
					});
				}
			}
			return outputPolygon;
		}

		/// <summary>
		/// Point(Longitude, Latitude)
		/// </summary>
		public Utm.Models.Polygon GetCircle(string point, double radius)
        {
            var outputPolygon = new Utm.Models.Polygon();

            using (var conn = new NpgsqlConnection(anraConfiguration.ConnectionString))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(" Select ST_AsGeoJson(ST_Transform(ST_Buffer(ST_Transform(ST_GeomFromText('" + point + "',4326), 2163),"+radius+",'quad_segs=4'),4326)) AS polygon", conn);

                NpgsqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    outputPolygon = JsonConvert.DeserializeObject<Utm.Models.Polygon>(dr["polygon"].ToString());
                }
            }
            return outputPolygon;
        }


	}
}