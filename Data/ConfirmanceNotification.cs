﻿using AnraUssServices.Common;
using System;

namespace AnraUssServices.Data.Notifications
{
    public class ConfirmanceNotification
    {
        public Guid Gufi { get; set; }

        public DateTime TimeStamp { get; set; }

        public bool IsConfirming { get; set; }

        public int TimeDiff { get; set; }

        public int NonConformances { get; set; }

        public OperationState State { get; set; }
    }
}