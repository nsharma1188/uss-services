﻿using AnraUssServices.Common;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AnraUssServices.Data
{
    public class LightDatabaseContext
    {
        private readonly AnraMqttClient mqttClient;
        private readonly ILogger<string> _logger;        
        public LiteDatabase LiteDBInstance { get; private set; }

        public LightDatabaseContext(ILogger<string> logger, AnraConfiguration configuration, AnraMqttClient mqttClient)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.UssLiteDbPath, $"{DateTime.Now.ToString("yyyy_MM_dd")}.db");

            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            LiteDBInstance = new LiteDatabase(dbName);
        }

        public async void SaveUssExchangeAsync(Common.UssExchangeData.UssExchange item)
        {
            //mqttClient.PublishTopic(MqttTopics.NotifyUssExchangeToViewer, item);
            await Task.Run(() => LiteDBInstance.GetCollection<Common.UssExchangeData.UssExchange>("ussexchange").Insert(item));
        }
	}
}