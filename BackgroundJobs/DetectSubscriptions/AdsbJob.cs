﻿using AnraUssServices.Common;
using AnraUssServices.Common.Adsb;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.ViewModel.Detects;
using FluentScheduler;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnraUssServices.BackgroundJobs.DetectSubscriptions
{
    public class AdsbJob: IJob
    {
        private readonly ILogger<string> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly AnraMqttClient _anraMqttClient;
        private AnraConfiguration _anraConfiguration;
        public AdsbInput _adsbInput;

        public AdsbJob(IServiceProvider serviceProvider, ILogger<string> logger, AnraMqttClient anraMqttClient, AnraConfiguration anraConfiguration, AdsbInput adsbInput)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
            _anraMqttClient = anraMqttClient;
            _anraConfiguration = anraConfiguration;
            _adsbInput = adsbInput;
        }

        public void Execute()
        {
            ExecuteHelperAsync();
        }
        public async Task ExecuteHelperAsync()
        {
            try
            {
                var reqURL = string.Format("https://opensky-network.org/api/states/all?lamin={0}&lomin={1}&lamax={2}&lomax={3}", _adsbInput.latmin, _adsbInput.lngmin, _adsbInput.latmax, _adsbInput.lngmax);
                AdsbSendObject send = new AdsbSendObject();
                AdsbSendObject data = await GetAdsbDataAsync(reqURL, send, _adsbInput.UserId);
                string jsonData = JsonConvert.SerializeObject(data.Detectpackets);
                _anraMqttClient.PublishTopic(string.Format(MqttTopics.SendAdsbData, _adsbInput.USSInstanceId), data);
                #if DEBUG
                    _logger.LogInformation("Adsb Data Published"+DateTime.Now);
                #endif
            }
            catch (Exception ex)
            {
				_logger.LogError("Exception ::: AdsbJob :: {0}\nException {1}", DateTime.Now, ex.Message);          
			}
        }

        private async Task<AdsbSendObject> GetAdsbDataAsync(string reqURL, AdsbSendObject send, Guid uId)
        {
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(reqURL);
                var result = await response.Content.ReadAsStringAsync();
                var adsbAreaModel = JsonConvert.DeserializeObject<AdsbOneskyModel>(result);

                int timePacketReceived = adsbAreaModel.Time;
                Guid detect_id = Guid.NewGuid();

                if (adsbAreaModel.States != null)
                {
                    send.Detectpackets = new List<AdsbJsonStruct>();
                    for (int index = 0; index < adsbAreaModel.States.GetLength(0); index++)
                    {
                        AdsbAreaData adsbdata = new AdsbAreaData();
                        float floatOutPut;
                        DateTime intOutPut;
                        adsbdata.icao24 = adsbAreaModel.States[index, 0];
                        adsbdata.callsign = adsbAreaModel.States[index, 1];
                        adsbdata.origin_country = adsbAreaModel.States[index, 2];
                        var x1 = DateTime.TryParse(adsbAreaModel.States[index, 3], out intOutPut) ? adsbdata.timeStamp = intOutPut : adsbdata.timeStamp = DateTime.UtcNow;

                        adsbdata.last_contact = int.Parse(adsbAreaModel.States[index, 4]);
                        var x2 = float.TryParse(adsbAreaModel.States[index, 5], out floatOutPut) ? adsbdata.longitude = floatOutPut : adsbdata.longitude = null;
                        var x3 = float.TryParse(adsbAreaModel.States[index, 6], out floatOutPut) ? adsbdata.latitude = floatOutPut : adsbdata.latitude = null;
                        var x4 = float.TryParse(adsbAreaModel.States[index, 7], out floatOutPut) ? adsbdata.baro_altitude = floatOutPut : adsbdata.baro_altitude = null;
                        adsbdata.on_ground = bool.Parse(adsbAreaModel.States[index, 8]);
                        var x5 = float.TryParse(adsbAreaModel.States[index, 9], out floatOutPut) ? adsbdata.velocity = floatOutPut : adsbdata.velocity = -1;//-ve when null value is received
                        adsbdata.true_track = float.TryParse(adsbAreaModel.States[index, 10], out floatOutPut) ? adsbdata.true_track = floatOutPut : adsbdata.true_track = null;
                        var x7 = float.TryParse(adsbAreaModel.States[index, 11], out floatOutPut) ? adsbdata.vertical_rate = floatOutPut : adsbdata.vertical_rate = -1;//-ve when null is received
                         
                        if (adsbAreaModel.States[index, 12] != null)
                        {
                            int numSensors = adsbAreaModel.States[index, 12].Length;
                            adsbdata.sensors = new int[numSensors];
                            for (int num = 0; num < numSensors; numSensors++)
                            {
                                adsbdata.sensors[num] = (adsbAreaModel.States[index, 12])[num];
                            }
                        }
                        else
                        {
                            adsbdata.sensors = null;
                        }
                        var x8 = float.TryParse(adsbAreaModel.States[index, 13], out floatOutPut) ? adsbdata.geo_altitude = floatOutPut : adsbdata.geo_altitude = -1;//-1 when value received is null

                        adsbdata.squawk = adsbAreaModel.States[index, 14];
                        adsbdata.spi = bool.Parse(adsbAreaModel.States[index, 15]);
                        adsbdata.position_source = int.Parse(adsbAreaModel.States[index, 16]);//0 = ADS-B, 1 = ASTERIX, 2 = MLAT
                        var packet = adsbdata.ToDetectPacket(uId, detect_id);
                        send.Detectpackets.Add(packet);
                    }
                }
            }
            catch (Exception ex)
            {
				_logger.LogError("Exception ::: AdsbJob Data Read :: Exception {0}", ex.Message);
			}

            return send;
        }
        public DateTime ConvertEpochTime(int epochTime)
        {
            DateTime humanReadableTime = DateTime.Now;
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epochTime);

        }
    }
}
