﻿using AnraUssServices.Common;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Models.DetectSubscribers;
using AnraUssServices.ViewModel.Detects;
using FluentScheduler;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.BackgroundJobs.Operation
{
    public class DetectionSubscriptionStatus : IJob
    {
        private AnraMqttClient mqttClient;
        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;

        public DetectionSubscriptionStatus(IServiceProvider serviceProvider, ILogger<string> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public void Execute()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("DetectionSubscriptionStatus");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    this.mqttClient = serviceScope.ServiceProvider.GetService<AnraMqttClient>();
                    var subscriberRepository = serviceScope.ServiceProvider.GetService<IRepository<DetectSubscriber>>();
                    var operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();
                    var detectSubscribersDocument = serviceScope.ServiceProvider.GetService<DetectSubscribersDocument>();

                    CheckSubscriptionExpiry(detectSubscribersDocument, subscriberRepository, operationRepository);

                    CheckAdsbSubscriptionExpiryAsync(detectSubscribersDocument, subscriberRepository);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("DetectionSubscriptionStatus ::: Exception : {0}", ex);
            }
        }

        private static void CheckSubscriptionExpiry(DetectSubscribersDocument detectSubscribersDocument, IRepository<DetectSubscriber> subscriberRepository, IRepository<Models.Operation> operationRepository)
        {
            var activeSubscribersClosedOperations = subscriberRepository.GetAll()
                                    .Where(p => p.IsActive)
                                    .Join(operationRepository.GetAll().Where(o => o.State == OperationState.CLOSED.ToString()),
                                    s => s.Gufi, o => o.Gufi,
                                    (s, o) => s)
                                    .ToList();

            foreach (var subscription in activeSubscribersClosedOperations)
            {
                subscription.IsActive = false;
                subscriberRepository.Update(subscription);

                detectSubscribersDocument.AddUpdateSubscription(new DetectSubscriberViewModel
                {
                    SubscriptionId = subscription.SubscriptionId,
                    Gufi = subscription.Gufi,
                    UserId = subscription.Gufi,
                    IsActive = false
                });
            }
        }

        private void CheckAdsbSubscriptionExpiryAsync(DetectSubscribersDocument detectSubscribersDocument, IRepository<DetectSubscriber> subscriberRepository)
        {
            var subscribers = subscriberRepository.GetAll()
                                    .Where(p => p.IsActive)
                                    .Where(p => p.SubscribedTypes.Contains(DetectSourceEnum.ADSB.ToString()))
                                    .Where(p => p.EndTime <= DateTime.UtcNow.AddMinutes(1))
                                    .ToList();

            foreach (var subscriber in subscribers)
            {
                //subscribers with same ussinstanceID
                var activeSubscribers = subscriberRepository.GetAll()
                                                            .Where(p => p.IsActive)
                                                            .Where(p => p.UssInstanceId.Equals(subscriber.UssInstanceId))
                                                            .Where(p => p.SubscribedTypes.Contains(DetectSourceEnum.ADSB.ToString()));

                if (activeSubscribers.ToList().Count == 1)//check this for 
                {
                    JobManager.RemoveJob(subscriber.UssInstanceId.ToString());
                    #if DEBUG
                        logger.LogInformation("Job removed by DetectionSubscriptionStatusChecker for USS {0}", subscriber.UssInstanceId.ToString());
                    #endif
                }
                subscriber.SubscribedTypes = Array.FindAll(subscriber.SubscribedTypes, (s) => s != DetectSourceEnum.ADSB.ToString()).ToArray();
                subscriberRepository.Update(subscriber);

                detectSubscribersDocument.AddUpdateSubscription(new DetectSubscriberViewModel
                {
                    SubscriptionId = subscriber.SubscriptionId,
                    Gufi = subscriber.Gufi,
                    UserId = subscriber.Gufi,
                    SubscribedTypes = subscriber.SubscribedTypes,
                    IsActive = false
                });
            }
        }
    }
}