﻿using AnraUssServices.Common;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Models;
using FluentScheduler;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.BackgroundJobs.Operation
{
    public class OperationStatusJob : IJob
    {
        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;

        public OperationStatusJob(IServiceProvider serviceProvider, ILogger<string> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public void Execute()
        {
            OperationActivation();
            OperationClosing();
        }

        private void OperationClosing()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("OperationStatusJob :: OperationClosing");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    var operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();
                    //// .AddHours(-24)
                    var operations = operationRepository.GetAll()
                        .Where(p => p.IsInternalOperation && p.State != OperationState.CLOSED.ToString())
                        .Where(p => p.OperationVolumes.Any(x => x.EffectiveTimeEnd <= DateTime.UtcNow.AddHours(-2)))
                        .Select(p => p.Gufi)
                        .Distinct()
                        .ToArray();

                    var operationStatusLogic = serviceScope.ServiceProvider.GetService<OperationStatusLogic>();
                    operationStatusLogic.BulkCloseAsync(operations);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("OperationStatusJob ::: Exception : {0}", ex);
            }
        }

        private void OperationActivation()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("OperationStatusJob :: OperationActivation");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    var operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();

                    var operations = operationRepository.GetAll()
                        .Where(p => p.IsInternalOperation && p.State == OperationState.ACCEPTED.ToString())
                        .Where(p => p.OperationVolumes.Any(x => x.EffectiveTimeBegin <= DateTime.UtcNow.AddMinutes(2)))
                        .Select(p => p.Gufi)
                        .Distinct()
                        .ToArray();

                    var operationStatusLogic = serviceScope.ServiceProvider.GetService<OperationStatusLogic>();
                    operationStatusLogic.BulkActivateAsync(operations);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("OperationStatusJob ::: Exception : {0}", ex);
            }
        }
    }
}