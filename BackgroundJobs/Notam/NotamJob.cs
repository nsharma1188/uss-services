﻿using AnraUssServices.Common;
using AnraUssServices.Common.Lookups;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.Notams;
using FluentScheduler;
using Mapster;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.BackgroundJobs.Notam
{
    public class NotamJob : IJob
    {
        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;
        private AnraMqttClient mqttClient;
        private AnraConfiguration configuration;

        public NotamJob(IServiceProvider serviceProvider, ILogger<string> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public void Execute()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("NotamJob");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    this.mqttClient = serviceScope.ServiceProvider.GetService<AnraMqttClient>();

                    var notamRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Notam>>();
                    this.configuration = serviceScope.ServiceProvider.GetService<AnraConfiguration>();

                    var notams = notamRepository.GetAll().Where(p => p.Status == NotamState.ACTIVATED.ToString() && DateTime.UtcNow >= p.EndDate.AddSeconds(-10)).ToList();

                    //Now Set Notams Expired Whose End Date is in past
                    notams.ForEach(x =>
                    {
                        if (x.EndDate <= DateTime.UtcNow)
                        {
                            x.Status = EnumUtils.GetDescription(NotamState.EXPIRED);
                            x.DateModified = DateTime.UtcNow;
                            notamRepository.Update(x);

                            //Publish Notam Data
                            //mqttClient.PublishTopic(string.Format(MqttTopics.NotamTopic, x.NotamNumber), x.Adapt<NotamDetailItem>());

                            //New Mqtt for NOTAMS notification
                            //Creating mqtt message payload
                            var mqttMessagePayload = new MqttMessagePayload()
                            {
                                Type = MqttMessageType.CLOSED.ToString(),
                                Message = x.Adapt<NotamDetailItem>(),
                                Topic = $"{configuration.Product}/{configuration.Environment}/{MqttTopics.NotifyNotamToViewer}"
                            };

                            mqttClient.PublishTopic(MqttTopics.NotifyNotamToViewer, mqttMessagePayload);

                        }
                    });
                }
            }
            catch (Exception ex)
            {
                logger.LogError("NotamJob ::: Exception : {0}", ex);
            }
        }
    }
}