﻿using AnraUssServices.Common;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.ConstraintMessages;
using FluentScheduler;
using Mapster;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace AnraUssServices.BackgroundJobs.Notam
{
    public class UVRJob : IJob
    {
        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;
        private AnraMqttClient mqttClient;
        private AnraConfiguration configuration;

        public UVRJob(IServiceProvider serviceProvider, ILogger<string> logger)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
        }

        public void Execute()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("UVRJob");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    this.mqttClient = serviceScope.ServiceProvider.GetService<AnraMqttClient>();
                    var constraintMessageRepository = serviceScope.ServiceProvider.GetService<IRepository<ConstraintMessage>>();
                    this.configuration = serviceScope.ServiceProvider.GetService<AnraConfiguration>();

                    var uvrs = constraintMessageRepository.GetAll().Where(p => DateTime.UtcNow >= p.EffectiveTimeEnd.AddSeconds(-10) && !p.IsExpired ).ToList();

                    //Now Notify Users
                    uvrs.ForEach(x =>
                    {
                        x.IsExpired = true;
                        x.DateModified = DateTime.UtcNow;
                        x.ActualTimeEnd = DateTime.UtcNow;
                        constraintMessageRepository.Update(x);

                        //Current UVR mqtt notification 
                        //mqttClient.PublishTopic(string.Format(MqttTopics.UvrTopic, x.MessageId), x);

                        //New Mqtt for UVR notification
                        //Creating mqtt message payload
                        var mqttMessagePayload = new MqttMessagePayload()
                        {
                            Type = MqttMessageType.CLOSED.ToString(),
                            Message = x.Adapt<ConstraintMessageViewModel>(),
                            Topic = $"{configuration.Product}/{configuration.Environment}/{MqttTopics.NotifyUvrToViewer}"
                        };

                        mqttClient.PublishTopic(MqttTopics.NotifyUvrToViewer, mqttMessagePayload);
                    });
                }
            }
            catch (Exception ex)
            {
                logger.LogError("NotamJob ::: Exception : {0}", ex);
            }
        }
    }
}