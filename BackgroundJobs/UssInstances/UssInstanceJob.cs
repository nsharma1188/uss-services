﻿using AnraUssServices.Common;
using AnraUssServices.Common.InterUss;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.UtmInstances;
using AnraUssServices.ViewModel.Operations;
using AnraUssServices.Common.Mqtt;
using FluentScheduler;
using Mapster;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.BackgroundJobs.UssInstances
{
    public class UssInstanceJob : IJob
    {
        private readonly AnraMqttClient anraMqttClient;
        private readonly AnraConfiguration anraConfiguration;
        private readonly ILogger<string> logger;
        private readonly IServiceProvider serviceProvider;

        public UssInstanceJob(IServiceProvider serviceProvider, ILogger<string> logger, AnraConfiguration anraConfiguration, AnraMqttClient anraMqttClient)
        {
            this.serviceProvider = serviceProvider;
            this.logger = logger;
            this.anraConfiguration = anraConfiguration;
            this.anraMqttClient = anraMqttClient;
        }

        public void Execute()
        {
            try
            {
                #if DEBUG
                    logger.LogDebug("UssInstanceJob");
                #endif

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (serviceScope)
                {
                    var ussInstanceRepository = serviceScope.ServiceProvider.GetService<IRepository<UtmInstance>>();
                    var ussInstanceDocument = serviceScope.ServiceProvider.GetService<UssInstanceDocument>();
                    var gridCell = serviceScope.ServiceProvider.GetService<GridCell>();
                    var operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();

                    // Notice will expired soon Operation // will remove
                    //NotifyOperationExpiredSoon(ussInstanceRepository, operationRepository);

                    //Delete Expired Uss From Grid & DB & Lite DB
                    DeleteExpiredUssInstancesFromGrid(ussInstanceRepository, gridCell, ussInstanceDocument, operationRepository);

                    IndexActiveUssInstances(ussInstanceRepository, ussInstanceDocument);
                }
            }
            catch (Exception ex)
            {
                logger.LogError("OperationStatusJob ::: Exception : {0}", ex);
            }
        }

        /// <summary>
        /// New Activations in next 5 minutes
        /// </summary>
        /// <param name="ussInstanceRepository"></param>
        /// <param name="ussInstanceDocument"></param>
        private void IndexActiveUssInstances(IRepository<UtmInstance> ussInstanceRepository, UssInstanceDocument ussInstanceDocument)
        {
            var activeUssInstances = ussInstanceRepository.GetAll()
                                    .Where(p => (p.TimeAvailableBegin <= DateTime.UtcNow.AddMinutes(5)) && p.TimeAvailableEnd > DateTime.UtcNow)
                                    .Where(p => !string.IsNullOrEmpty(p.UssName) && p.UssName.ToLower().Equals(anraConfiguration.ClientId, StringComparison.InvariantCultureIgnoreCase))
                                    .Distinct()
                                    .Adapt<List<UtmInstanceItem>>();

            ussInstanceDocument.AddMany(activeUssInstances);
        }

        /// <summary> // will remove
        /// Nodify Will Expired Soon Operation
        /// </summary>
        /// <param name="ussInstanceRepository"></param>
        /// <param name="operationRepository"></param>
        //private void NotifyOperationExpiredSoon(IRepository<UtmInstance> ussInstanceRepository, IRepository<Models.Operation> operationRepository)
        //{
        //    var soonExpiredUssInstances = ussInstanceRepository.GetAll()
        //                                        .Where(p => !string.IsNullOrEmpty(p.UssName) && p.UssName.ToLower().Equals(anraConfiguration.ClientId, StringComparison.InvariantCultureIgnoreCase) && p.TimeAvailableEnd < DateTime.UtcNow.AddMinutes(+5))
        //                                        .Select(p => p)
        //                                       .ToList();
        //    if (soonExpiredUssInstances.Any())
        //    {
        //        soonExpiredUssInstances.ForEach(x =>
        //        {
        //            var operationInUss = operationRepository.GetAll()
        //                                        .Where(o => o.UssInstanceId.Equals(x.UssInstanceId))
        //                                        .Select(o => o)
        //                                        .ToList();

        //            operationInUss.ForEach(y =>
        //            {
        //                var notification = new OperationDeleteNotification
        //                {
        //                    Gufi = y.Gufi,
        //                    IsDeleted = true,
        //                    Timestamp = DateTime.UtcNow
        //                };

        //                anraMqttClient.PublishTopic(MqttTopics.NotifyDeleteOperationToViewer, notification);
        //            });
        //        });
        //    }
        //}

        /// <summary>
        /// Delete Expired Uss From DB & InterUSS Grid
        /// </summary>
        /// <param name="ussInstanceRepository"></param>
        /// <param name="gridCell"></param>
        /// <param name="ussInstanceDocument"></param>        
        private void DeleteExpiredUssInstancesFromGrid(IRepository<UtmInstance> ussInstanceRepository, GridCell gridCell, UssInstanceDocument ussInstanceDocument, IRepository<Models.Operation> operationRepository)
        {
            var expiredUssInstances = ussInstanceRepository.GetAll()
                                               .Where(p => !string.IsNullOrEmpty(p.UssName) && p.UssName.ToLower().Equals(anraConfiguration.ClientId, StringComparison.InvariantCultureIgnoreCase) && p.TimeAvailableEnd < DateTime.UtcNow.AddMinutes(-2))
                                               .Select(p => p)
                                               .ToList();

            if (expiredUssInstances.Any())
            {                
                expiredUssInstances.ForEach(x => {

                    //Remove Uss From Grid
                    gridCell.DeleteGridCellOperator(x);

                    //Remove related operators
                    var existingOperators = ussInstanceRepository.GetAll().Where(p => p.GridCells == x.GridCells && p.UssInstanceId != x.UssInstanceId).ToList();

                    if (existingOperators.Any())
                    {
                        existingOperators.ForEach(op => {

                            //Notify before remove related operators
                            var operationInUss = operationRepository.GetAll()
                                                .Where(o => o.UssInstanceId.Equals(op.UssInstanceId))
                                                .Select(o => o)
                                                .ToList();

                            operationInUss.ForEach(y =>
                            {
                                //var notification = new OperationDeleteNotification
                                //{
                                //    Gufi = y.Gufi,
                                //    IsDeleted = true,
                                //    Timestamp = DateTime.UtcNow
                                //};

                                //anraMqttClient.PublishTopic(MqttTopics.NotifyDeleteOperationToViewer, notification);

                                //Remove Uss From DB
                                //ussInstanceRepository.Remove(op.Id);
                                ussInstanceRepository.Remove(op.UssInstanceId);
                            });
                        });
                    }

                    //Remove Uss From DB
                    ussInstanceRepository.Remove(x.UssInstanceId);

                    //Remove from LiteDB UssInstance Table as well
                    ussInstanceDocument.DeleteMany(expiredUssInstances.Select(p => p.UssInstanceId).ToList());
                });
            }
        }
    }
}