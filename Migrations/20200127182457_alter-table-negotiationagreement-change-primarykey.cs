﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablenegotiationagreementchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NegotiationAgreement",
                table: "NegotiationAgreement");

            migrationBuilder.DropColumn(
                name: "NegotiationAgreementId",
                table: "NegotiationAgreement");

            migrationBuilder.AddColumn<Guid>(
                name: "NegotiationAgreementUid",
                table: "NegotiationAgreement",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_NegotiationAgreement",
                table: "NegotiationAgreement",
                column: "NegotiationAgreementUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NegotiationAgreement",
                table: "NegotiationAgreement");

            migrationBuilder.DropColumn(
                name: "NegotiationAgreementUid",
                table: "NegotiationAgreement");

            migrationBuilder.AddColumn<int>(
                name: "NegotiationAgreementId",
                table: "NegotiationAgreement",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NegotiationAgreement",
                table: "NegotiationAgreement",
                column: "NegotiationAgreementId");
        }
    }
}
