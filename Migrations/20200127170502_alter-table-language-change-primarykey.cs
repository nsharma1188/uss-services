﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablelanguagechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LanguageResource_Language_LanguageId",
                table: "LanguageResource");

            migrationBuilder.DropTable(
                name: "LocaleStringResource");

            migrationBuilder.DropIndex(
                name: "IX_LanguageResource_LanguageId",
                table: "LanguageResource");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Language",
                table: "Language");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "LanguageResource");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "Language");

            migrationBuilder.AddColumn<Guid>(
                name: "LanguageGufi",
                table: "LanguageResource",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Language",
                table: "Language",
                column: "LanguageGufi");

            migrationBuilder.CreateIndex(
                name: "IX_LanguageResource_LanguageGufi",
                table: "LanguageResource",
                column: "LanguageGufi");

            migrationBuilder.AddForeignKey(
                name: "FK_LanguageResource_Language_LanguageGufi",
                table: "LanguageResource",
                column: "LanguageGufi",
                principalTable: "Language",
                principalColumn: "LanguageGufi",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LanguageResource_Language_LanguageGufi",
                table: "LanguageResource");

            migrationBuilder.DropIndex(
                name: "IX_LanguageResource_LanguageGufi",
                table: "LanguageResource");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Language",
                table: "Language");

            migrationBuilder.DropColumn(
                name: "LanguageGufi",
                table: "LanguageResource");

            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "LanguageResource",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "Language",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Language",
                table: "Language",
                column: "LanguageId");

            migrationBuilder.CreateTable(
                name: "LocaleStringResource",
                columns: table => new
                {
                    ResourceId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    LanguageId = table.Column<int>(nullable: false),
                    ResourceGufi = table.Column<Guid>(nullable: false),
                    ResourceName = table.Column<string>(nullable: true),
                    ResourceValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocaleStringResource", x => x.ResourceId);
                    table.ForeignKey(
                        name: "FK_LocaleStringResource_Language_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Language",
                        principalColumn: "LanguageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LanguageResource_LanguageId",
                table: "LanguageResource",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_LocaleStringResource_LanguageId",
                table: "LocaleStringResource",
                column: "LanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_LanguageResource_Language_LanguageId",
                table: "LanguageResource",
                column: "LanguageId",
                principalTable: "Language",
                principalColumn: "LanguageId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
