﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class initial_create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Collision",
                columns: table => new
                {
                    CollisionId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Detect = table.Column<string>(nullable: true),
                    DetectId = table.Column<int>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    Position = table.Column<string>(nullable: true),
                    PositionId = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    Uid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Collision", x => x.CollisionId);
                });

            migrationBuilder.CreateTable(
                name: "ConstraintMessage",
                columns: table => new
                {
                    ConstraintMessageId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ActualTimeEnd = table.Column<DateTime>(nullable: true),
                    Cause = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    EffectiveTimeBegin = table.Column<DateTime>(nullable: false),
                    EffectiveTimeEnd = table.Column<DateTime>(nullable: false),
                    Geography = table.Column<NpgsqlPolygon>(nullable: false),
                    IsRestriction = table.Column<bool>(nullable: false),
                    MaxAltitude = table.Column<string>(type: "jsonb", nullable: true),
                    MessageId = table.Column<Guid>(nullable: false),
                    MinAltitude = table.Column<string>(type: "jsonb", nullable: true),
                    PermittedGufis = table.Column<string[]>(nullable: true),
                    PermittedUas = table.Column<string[]>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UssName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConstraintMessage", x => x.ConstraintMessageId);
                });

            migrationBuilder.CreateTable(
                name: "Detect",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Altitude = table.Column<double>(nullable: false),
                    Bearing = table.Column<double>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DetectId = table.Column<Guid>(nullable: false),
                    Heading = table.Column<int>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Metadata1 = table.Column<string>(nullable: true),
                    Metadata2 = table.Column<string>(nullable: true),
                    RawData = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    SourceClass = table.Column<string>(nullable: true),
                    TimeStamp = table.Column<DateTime>(nullable: false),
                    Uid = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Vendor = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Detect", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DetectSubscriber",
                columns: table => new
                {
                    DetectSubscriberId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AdsbQuery = table.Column<string>(nullable: true),
                    AdsbSource = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DetectSoure = table.Column<string>(nullable: true),
                    EndTime = table.Column<DateTime>(nullable: true),
                    Gufi = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    OperationId = table.Column<Guid>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    SubscribedTypes = table.Column<string[]>(nullable: true),
                    SubscriptionId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    UssInstanceId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DetectSubscriber", x => x.DetectSubscriberId);
                });

            migrationBuilder.CreateTable(
                name: "Drone",
                columns: table => new
                {
                    DroneId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BaseAlt = table.Column<string>(nullable: true),
                    BaseLat = table.Column<string>(nullable: true),
                    BaseLng = table.Column<string>(nullable: true),
                    BrandId = table.Column<int>(nullable: false),
                    CollisionThreshold = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    FpvSrcUrl = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsRadarEnabled = table.Column<bool>(nullable: false),
                    IsUtmEnabled = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    RemoteId = table.Column<string>(nullable: true),
                    SensorSrcUrl = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false),
                    Uid = table.Column<Guid>(nullable: false),
                    UpdPort = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drone", x => x.DroneId);
                });

            migrationBuilder.CreateTable(
                name: "LocalNetwork",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ContactEmail = table.Column<string>(nullable: true),
                    ContactPhone = table.Column<string>(nullable: true),
                    CoverageArea = table.Column<NpgsqlPolygon>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    TimeAvailableBegin = table.Column<DateTime>(nullable: true),
                    TimeAvailableEnd = table.Column<DateTime>(nullable: true),
                    TimeLastModified = table.Column<DateTime>(nullable: true),
                    TimeSubmitted = table.Column<DateTime>(nullable: true),
                    UssBaseCallbackUrl = table.Column<string>(nullable: true),
                    UssInformationalUrl = table.Column<string>(nullable: true),
                    UssInstanceId = table.Column<Guid>(nullable: false),
                    UssName = table.Column<string>(nullable: true),
                    UssOpenapiUrl = table.Column<string>(nullable: true),
                    UssRegistrationUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalNetwork", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "NegotiationMessage",
                columns: table => new
                {
                    NegotiationMessageId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DiscoveryReference = table.Column<string>(nullable: true),
                    FreeText = table.Column<string>(nullable: true),
                    GufiOfOriginator = table.Column<Guid>(nullable: false),
                    GufiOfReceiver = table.Column<Guid>(nullable: false),
                    MessageId = table.Column<Guid>(nullable: true),
                    NegotiationId = table.Column<Guid>(nullable: true),
                    ReplanSuggestion = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UssNameOfOriginator = table.Column<string>(nullable: true),
                    UssNameOfReceiver = table.Column<string>(nullable: true),
                    V2vForOriginator = table.Column<bool>(nullable: false),
                    V2vForReceiver = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NegotiationMessage", x => x.NegotiationMessageId);
                });

            migrationBuilder.CreateTable(
                name: "Notam",
                columns: table => new
                {
                    NotamId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Authority = table.Column<string>(nullable: true),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: false),
                    FacilityId = table.Column<int>(nullable: false),
                    IssueDate = table.Column<DateTime>(nullable: false),
                    NotamNumber = table.Column<Guid>(nullable: false),
                    NotamReason = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    PointOfContact = table.Column<string>(nullable: true),
                    Requirements = table.Column<string[]>(nullable: true),
                    Restriction = table.Column<string>(nullable: true),
                    StateId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    TypeId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notam", x => x.NotamId);
                });

            migrationBuilder.CreateTable(
                name: "Operation",
                columns: table => new
                {
                    OperationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AircraftComments = table.Column<string>(nullable: true),
                    AirspaceAuthorization = table.Column<Guid>(nullable: false),
                    Contact = table.Column<string>(type: "jsonb", nullable: true),
                    ControllerLocation = table.Column<NpgsqlPoint>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DecisionTime = table.Column<DateTime>(nullable: true),
                    DiscoveryReference = table.Column<string>(nullable: true),
                    EasyId = table.Column<string>(nullable: true),
                    FaaRule = table.Column<string>(nullable: true),
                    FlightComments = table.Column<string>(nullable: true),
                    FlightNumber = table.Column<string>(nullable: true),
                    FlightSpeed = table.Column<int>(nullable: false),
                    FlightStartTime = table.Column<DateTime>(nullable: false),
                    GcsLocation = table.Column<NpgsqlPoint>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    IsInternalOperation = table.Column<bool>(nullable: false),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    SlippyTileData = table.Column<string>(type: "jsonb", nullable: true),
                    State = table.Column<string>(nullable: true),
                    SubmitTime = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    UssInstanceId = table.Column<Guid>(nullable: false),
                    UssName = table.Column<string>(nullable: true),
                    VolumesDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operation", x => x.OperationId);
                });

            migrationBuilder.CreateTable(
                name: "TelemetryMessage",
                columns: table => new
                {
                    TelemetryMessageId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AltitudeGps = table.Column<string>(type: "jsonb", nullable: true),
                    AltitudeNumGpsSatellites = table.Column<int>(nullable: false),
                    BatteryRemaining = table.Column<int>(nullable: true),
                    Climbrate = table.Column<double>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DiscoveryReference = table.Column<string>(nullable: true),
                    EnroutePositionsId = table.Column<Guid>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    HdopGps = table.Column<double>(nullable: false),
                    Heading = table.Column<int>(nullable: true),
                    Location = table.Column<NpgsqlPoint>(nullable: false),
                    Mode = table.Column<string>(nullable: true),
                    Pitch = table.Column<double>(nullable: true),
                    Registration = table.Column<Guid>(nullable: false),
                    Roll = table.Column<double>(nullable: true),
                    TimeMeasured = table.Column<DateTime>(nullable: false),
                    TimeSent = table.Column<DateTime>(nullable: false),
                    TrackBearing = table.Column<double>(nullable: false),
                    TrackBearingReference = table.Column<int>(nullable: true),
                    TrackGroundSpeed = table.Column<double>(nullable: false),
                    UssInstanceId = table.Column<Guid>(nullable: false),
                    UssName = table.Column<string>(nullable: true),
                    VdopGps = table.Column<double>(nullable: false),
                    Yaw = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TelemetryMessage", x => x.TelemetryMessageId);
                });

            migrationBuilder.CreateTable(
                name: "Urep",
                columns: table => new
                {
                    UrepId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AirTemperature = table.Column<double>(nullable: true),
                    Altitude = table.Column<double>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    IcingIntensity = table.Column<string>(nullable: true),
                    IcingType = table.Column<string>(nullable: true),
                    Location = table.Column<NpgsqlPoint>(nullable: false),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    Proximity = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    TimeMeasured = table.Column<DateTime>(nullable: true),
                    TimeReceived = table.Column<DateTime>(nullable: true),
                    TimeSubmitted = table.Column<DateTime>(nullable: true),
                    TurbulenceIntensity = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    Visibility = table.Column<double>(nullable: true),
                    Weather = table.Column<string>(nullable: true),
                    WeatherIntensity = table.Column<string>(nullable: true),
                    WindDirection = table.Column<double>(nullable: true),
                    WindSpeed = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Urep", x => x.UrepId);
                });

            migrationBuilder.CreateTable(
                name: "UtmInstance",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Contact = table.Column<string>(type: "jsonb", nullable: true),
                    CoverageArea = table.Column<NpgsqlPolygon>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    GridCells = table.Column<string>(type: "jsonb", nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: true),
                    TimeAvailableBegin = table.Column<DateTime>(nullable: true),
                    TimeAvailableEnd = table.Column<DateTime>(nullable: true),
                    TimeLastModified = table.Column<DateTime>(nullable: true),
                    TimeSubmitted = table.Column<DateTime>(nullable: true),
                    UssBaseCallbackUrl = table.Column<string>(nullable: true),
                    UssInformationalUrl = table.Column<string>(nullable: true),
                    UssInstanceId = table.Column<Guid>(nullable: false),
                    UssName = table.Column<string>(nullable: true),
                    UssOpenapiUrl = table.Column<string>(nullable: true),
                    UssRegistrationUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtmInstance", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleData",
                columns: table => new
                {
                    VehicleDataId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleData", x => x.VehicleDataId);
                });

            migrationBuilder.CreateTable(
                name: "UserDroneAssociation",
                columns: table => new
                {
                    UserDroneAssociationId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DroneId = table.Column<int>(nullable: false),
                    RegistrationId = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDroneAssociation", x => x.UserDroneAssociationId);
                    table.ForeignKey(
                        name: "FK_UserDroneAssociation_Drone_DroneId",
                        column: x => x.DroneId,
                        principalTable: "Drone",
                        principalColumn: "DroneId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NotamArea",
                columns: table => new
                {
                    NotamAreaId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AreaName = table.Column<string>(nullable: true),
                    Center = table.Column<NpgsqlPoint>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    EffetiveDates = table.Column<DateTime[]>(nullable: true),
                    MaxAltitude = table.Column<int>(nullable: false),
                    MinAltitude = table.Column<int>(nullable: false),
                    NotamId = table.Column<int>(nullable: false),
                    Radius = table.Column<int>(nullable: false),
                    Region = table.Column<NpgsqlPolygon>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotamArea", x => x.NotamAreaId);
                    table.ForeignKey(
                        name: "FK_NotamArea_Notam_NotamId",
                        column: x => x.NotamId,
                        principalTable: "Notam",
                        principalColumn: "NotamId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NegotiationAgreement",
                columns: table => new
                {
                    NegotiationAgreementId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DiscoveryReference = table.Column<string>(nullable: true),
                    FreeText = table.Column<string>(nullable: true),
                    GufiOriginator = table.Column<Guid>(nullable: false),
                    GufiReceiver = table.Column<Guid>(nullable: false),
                    MessageId = table.Column<Guid>(nullable: true),
                    NegotiationId = table.Column<Guid>(nullable: true),
                    OperationId = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    UssNameOfOriginator = table.Column<string>(nullable: true),
                    UssNameOfReceiver = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NegotiationAgreement", x => x.NegotiationAgreementId);
                    table.ForeignKey(
                        name: "FK_NegotiationAgreement_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperationConflict",
                columns: table => new
                {
                    OperationConflictId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ConflictDetails = table.Column<string>(type: "jsonb", nullable: true),
                    ConflictingGufi = table.Column<Guid>(nullable: false),
                    ConflictingUssName = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    IsAnraPrimary = table.Column<bool>(nullable: false),
                    OperationId = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationConflict", x => x.OperationConflictId);
                    table.ForeignKey(
                        name: "FK_OperationConflict_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperationVolume",
                columns: table => new
                {
                    OperationVolumeId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ActualTimeEnd = table.Column<DateTime>(nullable: true),
                    BeyondVisualLineOfSight = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DistanceInFeet = table.Column<double>(nullable: false),
                    EffectiveTimeBegin = table.Column<DateTime>(nullable: false),
                    EffectiveTimeEnd = table.Column<DateTime>(nullable: false),
                    MaxAltitude = table.Column<string>(type: "jsonb", nullable: true),
                    MinAltitude = table.Column<string>(type: "jsonb", nullable: true),
                    NearStructure = table.Column<bool>(nullable: true),
                    NonConformanceGeography = table.Column<NpgsqlPolygon>(nullable: true),
                    OperationGeography = table.Column<NpgsqlPolygon>(nullable: false),
                    OperationId = table.Column<int>(nullable: false),
                    Ordinal = table.Column<int>(nullable: false),
                    ProtectedGeography = table.Column<NpgsqlPolygon>(nullable: true),
                    VolumeType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationVolume", x => x.OperationVolumeId);
                    table.ForeignKey(
                        name: "FK_OperationVolume_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PriorityElement",
                columns: table => new
                {
                    PriorityElementId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    OperationId = table.Column<int>(nullable: false),
                    PriorityLevel = table.Column<string>(nullable: true),
                    PriorityStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriorityElement", x => x.PriorityElementId);
                    table.ForeignKey(
                        name: "FK_PriorityElement_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UasRegistration",
                columns: table => new
                {
                    UasRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    OperationId = table.Column<int>(nullable: false),
                    RegistrationId = table.Column<Guid>(nullable: true),
                    RegistrationLocation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UasRegistration", x => x.UasRegistrationId);
                    table.ForeignKey(
                        name: "FK_UasRegistration_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConformanceLog",
                columns: table => new
                {
                    ConformanceLogId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DifferenceInSeconds = table.Column<int>(nullable: false),
                    Gufi = table.Column<Guid>(nullable: false),
                    IsAltitudeConfirming = table.Column<bool>(nullable: false),
                    IsConfirming = table.Column<bool>(nullable: false),
                    IsGeographyConfirming = table.Column<bool>(nullable: false),
                    IsTimeConfirming = table.Column<bool>(nullable: false),
                    NonConformances = table.Column<int>(nullable: false),
                    State = table.Column<string>(nullable: true),
                    TelemetryMessageId = table.Column<int>(nullable: false),
                    TimeStamp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConformanceLog", x => x.ConformanceLogId);
                    table.ForeignKey(
                        name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageId",
                        column: x => x.TelemetryMessageId,
                        principalTable: "TelemetryMessage",
                        principalColumn: "TelemetryMessageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pointout",
                columns: table => new
                {
                    PointoutId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Altitude = table.Column<double>(nullable: true),
                    Bearing = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Distance = table.Column<double>(nullable: true),
                    NorthRef = table.Column<string>(nullable: true),
                    Point = table.Column<NpgsqlPoint>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Track = table.Column<double>(nullable: true),
                    UrepId = table.Column<int>(nullable: false),
                    VehicleType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pointout", x => x.PointoutId);
                    table.ForeignKey(
                        name: "FK_Pointout_Urep_UrepId",
                        column: x => x.UrepId,
                        principalTable: "Urep",
                        principalColumn: "UrepId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleClassDetail",
                columns: table => new
                {
                    VehicleClassDetailId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MaxPitchRate = table.Column<double>(nullable: true),
                    MaxRollRate = table.Column<double>(nullable: true),
                    MaxYawRate = table.Column<decimal>(nullable: true),
                    NumRotors = table.Column<double>(nullable: true),
                    RotorDiameter = table.Column<double>(nullable: true),
                    VehicleDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleClassDetail", x => x.VehicleClassDetailId);
                    table.ForeignKey(
                        name: "FK_VehicleClassDetail_VehicleData_VehicleDataId",
                        column: x => x.VehicleDataId,
                        principalTable: "VehicleData",
                        principalColumn: "VehicleDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleEngineDetail",
                columns: table => new
                {
                    VehicleEngineDetailsId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    BatteryCapacity = table.Column<double>(nullable: true),
                    BatteryType = table.Column<string>(nullable: true),
                    BatteryVoltage = table.Column<double>(nullable: true),
                    EngineType = table.Column<string>(nullable: true),
                    VehicleDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleEngineDetail", x => x.VehicleEngineDetailsId);
                    table.ForeignKey(
                        name: "FK_VehicleEngineDetail_VehicleData_VehicleDataId",
                        column: x => x.VehicleDataId,
                        principalTable: "VehicleData",
                        principalColumn: "VehicleDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleProperty",
                columns: table => new
                {
                    VehiclePropertiesId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CruiseVel = table.Column<double>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    Height = table.Column<double>(nullable: true),
                    Length = table.Column<double>(nullable: true),
                    MaxCeiling = table.Column<double>(nullable: true),
                    MaxEmptyWeight = table.Column<double>(nullable: true),
                    MaxEndurance = table.Column<double>(nullable: true),
                    MaxRange = table.Column<double>(nullable: true),
                    MaxThrust = table.Column<double>(nullable: true),
                    MaxVel = table.Column<double>(nullable: true),
                    MaxWindVel = table.Column<double>(nullable: true),
                    Mtow = table.Column<double>(nullable: true),
                    PayloadCapacity = table.Column<double>(nullable: true),
                    VehicleDataId = table.Column<int>(nullable: false),
                    WebLink = table.Column<string>(nullable: true),
                    Width = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleProperty", x => x.VehiclePropertiesId);
                    table.ForeignKey(
                        name: "FK_VehicleProperty_VehicleData_VehicleDataId",
                        column: x => x.VehicleDataId,
                        principalTable: "VehicleData",
                        principalColumn: "VehicleDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleRegistration",
                columns: table => new
                {
                    VehicleRegistrationId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Date = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    FaaNumber = table.Column<string>(nullable: true),
                    NNumber = table.Column<string>(nullable: true),
                    RegisteredBy = table.Column<string>(nullable: true),
                    Uvin = table.Column<Guid>(nullable: true),
                    VehicleDataId = table.Column<int>(nullable: false),
                    VehicleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleRegistration", x => x.VehicleRegistrationId);
                    table.ForeignKey(
                        name: "FK_VehicleRegistration_VehicleData_VehicleDataId",
                        column: x => x.VehicleDataId,
                        principalTable: "VehicleData",
                        principalColumn: "VehicleDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleType",
                columns: table => new
                {
                    VehiclePropertiesId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccessType = table.Column<string>(nullable: true),
                    Class = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true),
                    Model = table.Column<string>(nullable: true),
                    VehicleDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleType", x => x.VehiclePropertiesId);
                    table.ForeignKey(
                        name: "FK_VehicleType_VehicleData_VehicleDataId",
                        column: x => x.VehicleDataId,
                        principalTable: "VehicleData",
                        principalColumn: "VehicleDataId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UtmMessage",
                columns: table => new
                {
                    UtmMessageId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Callback = table.Column<string>(nullable: true),
                    ContingencyPlanId = table.Column<int>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DiscoveryReference = table.Column<string>(nullable: true),
                    FreeText = table.Column<string>(nullable: true),
                    Gufi = table.Column<Guid>(nullable: true),
                    MessageId = table.Column<Guid>(nullable: false),
                    MessageType = table.Column<string>(nullable: true),
                    PrevMessageId = table.Column<Guid>(nullable: true),
                    SentTime = table.Column<DateTime>(nullable: false),
                    Severity = table.Column<string>(nullable: true),
                    TelemetryMessageId = table.Column<int>(nullable: true),
                    UssName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UtmMessage", x => x.UtmMessageId);
                    table.ForeignKey(
                        name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                        column: x => x.TelemetryMessageId,
                        principalTable: "TelemetryMessage",
                        principalColumn: "TelemetryMessageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContingencyPlan",
                columns: table => new
                {
                    ContingencyPlanId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ContingencyCause = table.Column<string[]>(nullable: true),
                    ContingencyId = table.Column<int>(nullable: false),
                    ContingencyLocationDescription = table.Column<string>(nullable: true),
                    ContingencyPolygon = table.Column<NpgsqlPolygon>(nullable: false),
                    ContingencyResponse = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    FreeText = table.Column<string>(nullable: true),
                    LoiterAltitude = table.Column<string>(type: "jsonb", nullable: true),
                    OperationId = table.Column<int>(nullable: true),
                    RelativePreference = table.Column<double>(nullable: true),
                    RelevantOperationVolumes = table.Column<string[]>(nullable: true),
                    UtmMessageId = table.Column<int>(nullable: true),
                    ValidTimeBegin = table.Column<DateTime>(nullable: true),
                    ValidTimeEnd = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContingencyPlan", x => x.ContingencyPlanId);
                    table.ForeignKey(
                        name: "FK_ContingencyPlan_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContingencyPlan_UtmMessage_UtmMessageId",
                        column: x => x.UtmMessageId,
                        principalTable: "UtmMessage",
                        principalColumn: "UtmMessageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventMetadata",
                columns: table => new
                {
                    EventMetadataId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CallSign = table.Column<string>(nullable: true),
                    DataCollection = table.Column<bool>(nullable: true),
                    DataQualityEngineer = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    EventId = table.Column<string>(nullable: true),
                    FreeText = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Modified = table.Column<bool>(nullable: true),
                    OperationId = table.Column<int>(nullable: true),
                    Scenario = table.Column<string>(nullable: true),
                    Setting = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    TestCard = table.Column<string>(nullable: true),
                    TestRun = table.Column<string>(nullable: true),
                    TestType = table.Column<string>(nullable: true),
                    UtmMessageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventMetadata", x => x.EventMetadataId);
                    table.ForeignKey(
                        name: "FK_EventMetadata_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EventMetadata_UtmMessage_UtmMessageId",
                        column: x => x.UtmMessageId,
                        principalTable: "UtmMessage",
                        principalColumn: "UtmMessageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConformanceLog_TelemetryMessageId",
                table: "ConformanceLog",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_OperationId",
                table: "ContingencyPlan",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_UtmMessageId",
                table: "ContingencyPlan",
                column: "UtmMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_EventMetadata_OperationId",
                table: "EventMetadata",
                column: "OperationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EventMetadata_UtmMessageId",
                table: "EventMetadata",
                column: "UtmMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationAgreement_OperationId",
                table: "NegotiationAgreement",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_NotamArea_NotamId",
                table: "NotamArea",
                column: "NotamId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OperationConflict_OperationId",
                table: "OperationConflict",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationVolume_OperationId",
                table: "OperationVolume",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_Pointout_UrepId",
                table: "Pointout",
                column: "UrepId");

            migrationBuilder.CreateIndex(
                name: "IX_PriorityElement_OperationId",
                table: "PriorityElement",
                column: "OperationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UasRegistration_OperationId",
                table: "UasRegistration",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDroneAssociation_DroneId",
                table: "UserDroneAssociation",
                column: "DroneId");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_ContingencyPlanId",
                table: "UtmMessage",
                column: "ContingencyPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_TelemetryMessageId",
                table: "UtmMessage",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleClassDetail_VehicleDataId",
                table: "VehicleClassDetail",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineDetail_VehicleDataId",
                table: "VehicleEngineDetail",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleProperty_VehicleDataId",
                table: "VehicleProperty",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRegistration_VehicleDataId",
                table: "VehicleRegistration",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_VehicleDataId",
                table: "VehicleType",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_ContingencyPlan_ContingencyPlanId",
                table: "UtmMessage",
                column: "ContingencyPlanId",
                principalTable: "ContingencyPlan",
                principalColumn: "ContingencyPlanId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_Operation_OperationId",
                table: "ContingencyPlan");

            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_UtmMessage_UtmMessageId",
                table: "ContingencyPlan");

            migrationBuilder.DropTable(
                name: "Collision");

            migrationBuilder.DropTable(
                name: "ConformanceLog");

            migrationBuilder.DropTable(
                name: "ConstraintMessage");

            migrationBuilder.DropTable(
                name: "Detect");

            migrationBuilder.DropTable(
                name: "DetectSubscriber");

            migrationBuilder.DropTable(
                name: "EventMetadata");

            migrationBuilder.DropTable(
                name: "LocalNetwork");

            migrationBuilder.DropTable(
                name: "NegotiationAgreement");

            migrationBuilder.DropTable(
                name: "NegotiationMessage");

            migrationBuilder.DropTable(
                name: "NotamArea");

            migrationBuilder.DropTable(
                name: "OperationConflict");

            migrationBuilder.DropTable(
                name: "OperationVolume");

            migrationBuilder.DropTable(
                name: "Pointout");

            migrationBuilder.DropTable(
                name: "PriorityElement");

            migrationBuilder.DropTable(
                name: "UasRegistration");

            migrationBuilder.DropTable(
                name: "UserDroneAssociation");

            migrationBuilder.DropTable(
                name: "UtmInstance");

            migrationBuilder.DropTable(
                name: "VehicleClassDetail");

            migrationBuilder.DropTable(
                name: "VehicleEngineDetail");

            migrationBuilder.DropTable(
                name: "VehicleProperty");

            migrationBuilder.DropTable(
                name: "VehicleRegistration");

            migrationBuilder.DropTable(
                name: "VehicleType");

            migrationBuilder.DropTable(
                name: "Notam");

            migrationBuilder.DropTable(
                name: "Urep");

            migrationBuilder.DropTable(
                name: "Drone");

            migrationBuilder.DropTable(
                name: "VehicleData");

            migrationBuilder.DropTable(
                name: "TelemetryMessage");

            migrationBuilder.DropTable(
                name: "Operation");

            migrationBuilder.DropTable(
                name: "UtmMessage");

            migrationBuilder.DropTable(
                name: "ContingencyPlan");
        }
    }
}
