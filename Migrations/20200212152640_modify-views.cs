﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class modifyviews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.anra_view_latest_location
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            var sql = @"CREATE OR REPLACE VIEW public.anra_view_latest_location AS
                        WITH rankedpositions AS (
                                SELECT ""TelemetryMessage"".""TimeMeasured"",
                                ""TelemetryMessage"".""Gufi"",
                                dense_rank() OVER(PARTITION BY ""TelemetryMessage"".""Gufi"" ORDER BY ""TelemetryMessage"".""TimeMeasured"" DESC) AS rnk
                                FROM ""TelemetryMessage""
                                GROUP BY ""TelemetryMessage"".""Gufi"", ""TelemetryMessage"".""TimeMeasured""
                            ), latestpositions AS(
                                SELECT
                                t.""TelemetryMessageUid"", 
			                    t.""AltitudeNumGpsSatellites"", 
			                    t.""BatteryRemaining"", 
			                    t.""Climbrate"", 
			                    t.""DateCreated"", 
			                    t.""DateModified"", 
			                    t.""EnroutePositionsId"", 
			                    t.""Gufi"", 
			                    t.""HdopGps"", 
			                    t.""Heading"", 
			                    t.""Location"", 
			                    t.""Mode"", 
			                    t.""Pitch"", 
			                    t.""Registration"", 
			                    t.""Roll"", 
			                    t.""TimeMeasured"", 
			                    t.""TimeSent"", 
			                    t.""UssName"", 
			                    t.""VdopGps"", 
			                    t.""Yaw"", 
			                    t.""UssInstanceId"", 
			                    t.""TrackBearing"",
                                t.""TrackBearingReference"",
			                    (t.""AltitudeGps"" ->> 'altitude_value'::text)::numeric AS ""AltitudeGps"", 
                                t.""TrackGroundSpeed""

                                FROM rankedpositions rp
                                    JOIN ""TelemetryMessage"" t ON t.""TimeMeasured"" = rp.""TimeMeasured""
                                WHERE rp.rnk = 1 AND rp.""TimeMeasured"" > (timezone('UTC'::text, CURRENT_TIMESTAMP) - '00:00:03'::interval second)
                            )
                        SELECT o.""Gufi"",
                        o.""UssInstanceId"",
                        o.""State"",
                        lp.""TelemetryMessageUid"",
                        lp.""Location"",
                        lp.""AltitudeGps"",
                        lp.""TimeMeasured"",
                        d.""Uid"",
                        d.""CollisionThreshold"",
                        format('SRID=4326;POINT(%s %s %s)'::text, st_x(lp.""Location""::geometry), st_y(lp.""Location""::geometry), lp.""AltitudeGps"") AS ""Location3D""
                        FROM latestpositions lp
                            JOIN ""Operation"" o ON lp.""Gufi"" = o.""Gufi""
                            JOIN ""Drone"" d ON lp.""Registration"" = d.""Uid"";

                        ALTER TABLE public.anra_view_latest_location
                            OWNER TO postgres;

                        GRANT ALL ON TABLE public.anra_view_latest_location TO postgres;
                        GRANT SELECT ON TABLE public.anra_view_latest_location TO PUBLIC;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");
        }
    }
}
