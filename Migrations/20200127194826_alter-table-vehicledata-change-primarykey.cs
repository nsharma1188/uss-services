﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablevehicledatachangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleClassDetail_VehicleData_VehicleDataId",
                table: "VehicleClassDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEngineDetail_VehicleData_VehicleDataId",
                table: "VehicleEngineDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleProperty_VehicleData_VehicleDataId",
                table: "VehicleProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRegistration_VehicleData_VehicleDataId",
                table: "VehicleRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleType_VehicleData_VehicleDataId",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_VehicleType_VehicleDataId",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_VehicleRegistration_VehicleDataId",
                table: "VehicleRegistration");

            migrationBuilder.DropIndex(
                name: "IX_VehicleProperty_VehicleDataId",
                table: "VehicleProperty");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEngineDetail_VehicleDataId",
                table: "VehicleEngineDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleData",
                table: "VehicleData");

            migrationBuilder.DropIndex(
                name: "IX_VehicleClassDetail_VehicleDataId",
                table: "VehicleClassDetail");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleRegistration");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleProperty");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleEngineDetail");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleData");

            migrationBuilder.DropColumn(
                name: "VehicleDataId",
                table: "VehicleClassDetail");

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleType",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleRegistration",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleProperty",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleEngineDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleData",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleDataUid",
                table: "VehicleClassDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleData",
                table: "VehicleData",
                column: "VehicleDataUid");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_VehicleDataUid",
                table: "VehicleType",
                column: "VehicleDataUid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRegistration_VehicleDataUid",
                table: "VehicleRegistration",
                column: "VehicleDataUid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleProperty_VehicleDataUid",
                table: "VehicleProperty",
                column: "VehicleDataUid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineDetail_VehicleDataUid",
                table: "VehicleEngineDetail",
                column: "VehicleDataUid",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleClassDetail_VehicleDataUid",
                table: "VehicleClassDetail",
                column: "VehicleDataUid",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleClassDetail_VehicleData_VehicleDataUid",
                table: "VehicleClassDetail",
                column: "VehicleDataUid",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEngineDetail_VehicleData_VehicleDataUid",
                table: "VehicleEngineDetail",
                column: "VehicleDataUid",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleProperty_VehicleData_VehicleDataUid",
                table: "VehicleProperty",
                column: "VehicleDataUid",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRegistration_VehicleData_VehicleDataUid",
                table: "VehicleRegistration",
                column: "VehicleDataUid",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleType_VehicleData_VehicleDataUid",
                table: "VehicleType",
                column: "VehicleDataUid",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataUid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleClassDetail_VehicleData_VehicleDataUid",
                table: "VehicleClassDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleEngineDetail_VehicleData_VehicleDataUid",
                table: "VehicleEngineDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleProperty_VehicleData_VehicleDataUid",
                table: "VehicleProperty");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleRegistration_VehicleData_VehicleDataUid",
                table: "VehicleRegistration");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleType_VehicleData_VehicleDataUid",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_VehicleType_VehicleDataUid",
                table: "VehicleType");

            migrationBuilder.DropIndex(
                name: "IX_VehicleRegistration_VehicleDataUid",
                table: "VehicleRegistration");

            migrationBuilder.DropIndex(
                name: "IX_VehicleProperty_VehicleDataUid",
                table: "VehicleProperty");

            migrationBuilder.DropIndex(
                name: "IX_VehicleEngineDetail_VehicleDataUid",
                table: "VehicleEngineDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleData",
                table: "VehicleData");

            migrationBuilder.DropIndex(
                name: "IX_VehicleClassDetail_VehicleDataUid",
                table: "VehicleClassDetail");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleRegistration");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleProperty");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleEngineDetail");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleData");

            migrationBuilder.DropColumn(
                name: "VehicleDataUid",
                table: "VehicleClassDetail");

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleRegistration",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleProperty",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleEngineDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleData",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "VehicleDataId",
                table: "VehicleClassDetail",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleData",
                table: "VehicleData",
                column: "VehicleDataId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleType_VehicleDataId",
                table: "VehicleType",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleRegistration_VehicleDataId",
                table: "VehicleRegistration",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleProperty_VehicleDataId",
                table: "VehicleProperty",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleEngineDetail_VehicleDataId",
                table: "VehicleEngineDetail",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleClassDetail_VehicleDataId",
                table: "VehicleClassDetail",
                column: "VehicleDataId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleClassDetail_VehicleData_VehicleDataId",
                table: "VehicleClassDetail",
                column: "VehicleDataId",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleEngineDetail_VehicleData_VehicleDataId",
                table: "VehicleEngineDetail",
                column: "VehicleDataId",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleProperty_VehicleData_VehicleDataId",
                table: "VehicleProperty",
                column: "VehicleDataId",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleRegistration_VehicleData_VehicleDataId",
                table: "VehicleRegistration",
                column: "VehicleDataId",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleType_VehicleData_VehicleDataId",
                table: "VehicleType",
                column: "VehicleDataId",
                principalTable: "VehicleData",
                principalColumn: "VehicleDataId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
