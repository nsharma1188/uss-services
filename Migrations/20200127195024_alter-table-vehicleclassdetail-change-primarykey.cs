﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablevehicleclassdetailchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "VehicleClassDetailUid",
                table: "VehicleClassDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_VehicleClassDetail_VehicleClassDetailUid",
                table: "VehicleClassDetail",
                column: "VehicleClassDetailUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_VehicleClassDetail_VehicleClassDetailUid",
                table: "VehicleClassDetail");

            migrationBuilder.DropColumn(
                name: "VehicleClassDetailUid",
                table: "VehicleClassDetail");
        }
    }
}
