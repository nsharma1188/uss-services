﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablecollisionchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Collision",
                table: "Collision");

            migrationBuilder.DropColumn(
                name: "CollisionId",
                table: "Collision");

            migrationBuilder.AddColumn<Guid>(
                name: "CollisionUid",
                table: "Collision",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Collision",
                table: "Collision",
                column: "CollisionUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Collision",
                table: "Collision");

            migrationBuilder.DropColumn(
                name: "CollisionUid",
                table: "Collision");

            migrationBuilder.AddColumn<int>(
                name: "CollisionId",
                table: "Collision",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Collision",
                table: "Collision",
                column: "CollisionId");
        }
    }
}
