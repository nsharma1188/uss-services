﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableuserdroneassociationchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserDroneAssociation",
                table: "UserDroneAssociation");

            migrationBuilder.DropColumn(
                name: "UserDroneAssociationId",
                table: "UserDroneAssociation");

            migrationBuilder.AddColumn<Guid>(
                name: "UserDroneAssociationUid",
                table: "UserDroneAssociation",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserDroneAssociation",
                table: "UserDroneAssociation",
                column: "UserDroneAssociationUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserDroneAssociation",
                table: "UserDroneAssociation");

            migrationBuilder.DropColumn(
                name: "UserDroneAssociationUid",
                table: "UserDroneAssociation");

            migrationBuilder.AddColumn<long>(
                name: "UserDroneAssociationId",
                table: "UserDroneAssociation",
                nullable: false,
                defaultValue: 0L)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserDroneAssociation",
                table: "UserDroneAssociation",
                column: "UserDroneAssociationId");
        }
    }
}
