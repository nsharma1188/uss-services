﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableoperationv5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventMetadata");

            migrationBuilder.DropColumn(
                name: "AircraftComments",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "AirspaceAuthorization",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "DiscoveryReference",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "EasyId",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "FlightNumber",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "FlightSpeed",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "FlightStartTime",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "VolumesDescription",
                table: "Operation");

            migrationBuilder.AlterColumn<NpgsqlPoint>(
                name: "GcsLocation",
                table: "Operation",
                nullable: true,
                oldClrType: typeof(NpgsqlPoint));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<NpgsqlPoint>(
                name: "GcsLocation",
                table: "Operation",
                nullable: false,
                oldClrType: typeof(NpgsqlPoint),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AircraftComments",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "AirspaceAuthorization",
                table: "Operation",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiscoveryReference",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EasyId",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FlightNumber",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FlightSpeed",
                table: "Operation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "FlightStartTime",
                table: "Operation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "VolumesDescription",
                table: "Operation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EventMetadata",
                columns: table => new
                {
                    EventMetadataId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CallSign = table.Column<string>(nullable: true),
                    DataCollection = table.Column<bool>(nullable: true),
                    DataQualityEngineer = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateModified = table.Column<DateTime>(nullable: false),
                    EventId = table.Column<string>(nullable: true),
                    FreeText = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Modified = table.Column<bool>(nullable: true),
                    OperationId = table.Column<int>(nullable: true),
                    Scenario = table.Column<string>(nullable: true),
                    Setting = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    TestCard = table.Column<string>(nullable: true),
                    TestRun = table.Column<string>(nullable: true),
                    TestType = table.Column<string>(nullable: true),
                    UtmMessageId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventMetadata", x => x.EventMetadataId);
                    table.ForeignKey(
                        name: "FK_EventMetadata_Operation_OperationId",
                        column: x => x.OperationId,
                        principalTable: "Operation",
                        principalColumn: "OperationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EventMetadata_UtmMessage_UtmMessageId",
                        column: x => x.UtmMessageId,
                        principalTable: "UtmMessage",
                        principalColumn: "UtmMessageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EventMetadata_OperationId",
                table: "EventMetadata",
                column: "OperationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EventMetadata_UtmMessageId",
                table: "EventMetadata",
                column: "UtmMessageId");
        }
    }
}
