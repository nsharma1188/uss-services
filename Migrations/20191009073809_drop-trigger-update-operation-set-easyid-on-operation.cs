﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class droptriggerupdateoperationseteasyidonoperation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getoperationsbygriddata
            var sql = @"DROP TRIGGER IF EXISTS update_operation_set_easyid ON public.""Operation"";";
            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sql = @"CREATE TRIGGER update_operation_set_easyid
                        AFTER INSERT
                        ON public.""Operation""
                        FOR EACH ROW
                        EXECUTE PROCEDURE public.generate_operation_easyid();";

            migrationBuilder.Sql(sql.ToString());
        }
    }
}
