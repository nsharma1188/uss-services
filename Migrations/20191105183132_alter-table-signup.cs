﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablesignup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Roles",
                table: "Signup",
                newName: "UserRole");

            migrationBuilder.RenameColumn(
                name: "PhoneNumber",
                table: "Signup",
                newName: "Website");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Signup",
                newName: "UserName");

            migrationBuilder.AddColumn<string>(
                name: "AdsbSource",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BaseLat",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BaseLng",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "Signup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FederalTaxId",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovtLicenseNumber",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogoFile",
                table: "Signup",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UnitId",
                table: "Signup",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdsbSource",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "BaseLat",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "BaseLng",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "FederalTaxId",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "GovtLicenseNumber",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "LogoFile",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "Signup");

            migrationBuilder.RenameColumn(
                name: "Website",
                table: "Signup",
                newName: "PhoneNumber");

            migrationBuilder.RenameColumn(
                name: "UserRole",
                table: "Signup",
                newName: "Roles");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "Signup",
                newName: "Email");
        }
    }
}
