﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctionsvalidateoperationwithtfraddedGroupId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__validateoperationwithtfr
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");


            var sql = @"DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);
                        CREATE OR REPLACE FUNCTION public.__validateoperationwithtfr(
	                        flightgeo polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone,
	                        minaltitudewgs84ft double precision,
	                        maxaltitudewgs84ft double precision)
                            RETURNS TABLE(""NotamId"" integer, ""Authority"" text, ""BeginDate"" timestamp without time zone, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""Description"" text, ""EndDate"" timestamp without time zone, ""FacilityId"" integer, ""IssueDate"" timestamp without time zone, ""NotamNumber"" uuid, ""NotamReason"" text, ""PointOfContact"" text, ""Requirements"" text[], ""Restriction"" text, ""StateId"" integer, ""Status"" text, ""TypeId"" integer, ""UserId"" text, ""OrganizationId"" uuid, ""GroupId"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                                BEGIN

                                    --Create Temporary Notam Table
                                    DROP TABLE IF EXISTS notam_table;
                                    CREATE TABLE notam_table AS

                                    SELECT
                                        notam.""NotamId"",
                                        notam.""BeginDate"",
                                        notam.""EndDate"",
                                        CASE
                                            WHEN notamarea.""Center""::geometry ISNULL
                                                THEN
                                                    ST_SetSRID(notamarea.""Region""::geometry, 4326)
                                                ELSE
                                                    ST_Buffer(ST_SetSRID(notamarea.""Center""::geometry, 4326), notamarea.""Radius"")
                                            END AS ""Geography""

                                    FROM public.""Notam"" AS notam

                                        INNER JOIN public.""NotamArea"" AS notamarea

                                            ON notam.""NotamId"" = notamarea.""NotamId"" AND notam.""Status"" = 'ACTIVATED'

                                    WHERE
                                        tsrange(notam.""BeginDate"", notam.""EndDate"", '[]') && tsrange(begindt, enddt, '[]')

                                        AND
                                        int8range(notamarea.""MinAltitude""::bigint, notamarea.""MaxAltitude""::bigint)
        		                        && int8range(minaltitudewgs84ft::bigint, maxaltitudewgs84ft::bigint);
        
                                        --Create Final Result Table
                                        DROP TABLE IF EXISTS final_table;
                                        CREATE TABLE final_table AS
                                        SELECT * FROM notam_table WHERE ST_Intersects(""Geography"", ST_SetSRID(flightgeo::geometry,4326));

                                        RETURN QUERY
                                        SELECT
                                            notam.""NotamId"",
                                            notam.""Authority"",
                                            notam.""BeginDate"",
                                            notam.""DateCreated"",
                                            notam.""DateModified"",
                                            notam.""Description"",
                                            notam.""EndDate"",
                                            notam.""FacilityId"",
                                            notam.""IssueDate"",
                                            notam.""NotamNumber"",
                                            notam.""NotamReason"",
                                            notam.""PointOfContact"",
                                            notam.""Requirements"",
                                            notam.""Restriction"",
                                            notam.""StateId"",
                                            notam.""Status"",
                                            notam.""TypeId"",
                                            notam.""UserId"",
                                            notam.""OrganizationId"",
                                            notam.""GroupId""

                                        FROM public.""Notam"" AS notam
                                        INNER JOIN final_table AS tblFinal
                                            ON notam.""NotamId"" = tblFinal.""NotamId"";                                                                                        
                                END;                   
                        $BODY$;

                        ALTER FUNCTION public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
        }
    }
}
