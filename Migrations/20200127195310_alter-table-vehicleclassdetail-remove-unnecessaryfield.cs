﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablevehicleclassdetailremoveunnecessaryfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleClassDetail",
                table: "VehicleClassDetail");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_VehicleClassDetail_VehicleClassDetailUid",
                table: "VehicleClassDetail");

            migrationBuilder.DropColumn(
                name: "VehicleClassDetailId",
                table: "VehicleClassDetail");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleClassDetail",
                table: "VehicleClassDetail",
                column: "VehicleClassDetailUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleClassDetail",
                table: "VehicleClassDetail");

            migrationBuilder.AddColumn<int>(
                name: "VehicleClassDetailId",
                table: "VehicleClassDetail",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleClassDetail",
                table: "VehicleClassDetail",
                column: "VehicleClassDetailId");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_VehicleClassDetail_VehicleClassDetailUid",
                table: "VehicleClassDetail",
                column: "VehicleClassDetailUid");
        }
    }
}
