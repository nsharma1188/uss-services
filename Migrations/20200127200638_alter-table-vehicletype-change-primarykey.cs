﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablevehicletypechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleRegistration",
                table: "VehicleRegistration");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleProperty",
                table: "VehicleProperty");

            migrationBuilder.DropColumn(
                name: "VehiclePropertiesId",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "VehicleRegistrationId",
                table: "VehicleRegistration");

            migrationBuilder.DropColumn(
                name: "VehiclePropertiesId",
                table: "VehicleProperty");

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleTypeUid",
                table: "VehicleType",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleRegistrationUid",
                table: "VehicleRegistration",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "VehiclePropertiesUid",
                table: "VehicleProperty",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType",
                column: "VehicleTypeUid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleRegistration",
                table: "VehicleRegistration",
                column: "VehicleRegistrationUid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleProperty",
                table: "VehicleProperty",
                column: "VehiclePropertiesUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleRegistration",
                table: "VehicleRegistration");

            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleProperty",
                table: "VehicleProperty");

            migrationBuilder.DropColumn(
                name: "VehicleTypeUid",
                table: "VehicleType");

            migrationBuilder.DropColumn(
                name: "VehicleRegistrationUid",
                table: "VehicleRegistration");

            migrationBuilder.DropColumn(
                name: "VehiclePropertiesUid",
                table: "VehicleProperty");

            migrationBuilder.AddColumn<int>(
                name: "VehiclePropertiesId",
                table: "VehicleType",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "VehicleRegistrationId",
                table: "VehicleRegistration",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "VehiclePropertiesId",
                table: "VehicleProperty",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleType",
                table: "VehicleType",
                column: "VehiclePropertiesId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleRegistration",
                table: "VehicleRegistration",
                column: "VehicleRegistrationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleProperty",
                table: "VehicleProperty",
                column: "VehiclePropertiesId");
        }
    }
}
