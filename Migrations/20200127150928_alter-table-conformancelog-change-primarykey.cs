﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableconformancelogchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConformanceLog",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "ConformanceLogId",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<Guid>(
                name: "ConformanceLogUid",
                table: "ConformanceLog",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConformanceLog",
                table: "ConformanceLog",
                column: "ConformanceLogUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConformanceLog",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "ConformanceLogUid",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<int>(
                name: "ConformanceLogId",
                table: "ConformanceLog",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConformanceLog",
                table: "ConformanceLog",
                column: "ConformanceLogId");
        }
    }
}
