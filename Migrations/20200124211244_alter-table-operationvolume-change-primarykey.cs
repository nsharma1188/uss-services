﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableoperationvolumechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationVolume",
                table: "OperationVolume");

            migrationBuilder.DropColumn(
                name: "DroneId",
                table: "UserDroneAssociation");

            migrationBuilder.DropColumn(
                name: "OperationVolumeId",
                table: "OperationVolume");

            migrationBuilder.AlterColumn<Guid>(
                name: "Uid",
                table: "UserDroneAssociation",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OperationVolumeUid",
                table: "OperationVolume",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationVolume",
                table: "OperationVolume",
                column: "OperationVolumeUid");

            migrationBuilder.AddForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation",
                column: "Uid",
                principalTable: "Drone",
                principalColumn: "Uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationVolume",
                table: "OperationVolume");

            migrationBuilder.DropColumn(
                name: "OperationVolumeUid",
                table: "OperationVolume");

            migrationBuilder.AlterColumn<Guid>(
                name: "Uid",
                table: "UserDroneAssociation",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<int>(
                name: "DroneId",
                table: "UserDroneAssociation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationVolumeId",
                table: "OperationVolume",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationVolume",
                table: "OperationVolume",
                column: "OperationVolumeId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation",
                column: "Uid",
                principalTable: "Drone",
                principalColumn: "Uid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
