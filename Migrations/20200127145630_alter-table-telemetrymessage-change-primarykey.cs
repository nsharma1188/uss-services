﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertabletelemetrymessagechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {//public.anra_view_latest_location
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            migrationBuilder.DropForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage");

            migrationBuilder.DropIndex(
                name: "IX_ConformanceLog_TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "TelemetryMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "TelemetryMessage",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "ConformanceLog",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage",
                column: "TelemetryMessageUid");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_TelemetryMessageUid",
                table: "UtmMessage",
                column: "TelemetryMessageUid");

            migrationBuilder.CreateIndex(
                name: "IX_ConformanceLog_TelemetryMessageUid",
                table: "ConformanceLog",
                column: "TelemetryMessageUid");

            migrationBuilder.AddForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageUid",
                table: "ConformanceLog",
                column: "TelemetryMessageUid",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageUid",
                table: "UtmMessage",
                column: "TelemetryMessageUid",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageUid",
                onDelete: ReferentialAction.Restrict);

            var sql = @"CREATE OR REPLACE VIEW public.anra_view_latest_location AS
                        WITH rankedpositions AS (
                                SELECT ""TelemetryMessage"".""TimeMeasured"",
                                ""TelemetryMessage"".""Gufi"",
                                dense_rank() OVER(PARTITION BY ""TelemetryMessage"".""Gufi"" ORDER BY ""TelemetryMessage"".""TimeMeasured"" DESC) AS rnk
                                FROM ""TelemetryMessage""
                                GROUP BY ""TelemetryMessage"".""Gufi"", ""TelemetryMessage"".""TimeMeasured""
                            ), latestpositions AS(
                                SELECT
                                t.""TelemetryMessageUid"", 
			                    t.""AltitudeNumGpsSatellites"", 
			                    t.""BatteryRemaining"", 
			                    t.""Climbrate"", 
			                    t.""DateCreated"", 
			                    t.""DateModified"", 
			                    t.""EnroutePositionsId"", 
			                    t.""Gufi"", 
			                    t.""HdopGps"", 
			                    t.""Heading"", 
			                    t.""Location"", 
			                    t.""Mode"", 
			                    t.""Pitch"", 
			                    t.""Registration"", 
			                    t.""Roll"", 
			                    t.""TimeMeasured"", 
			                    t.""TimeSent"", 
			                    t.""UssName"", 
			                    t.""VdopGps"", 
			                    t.""Yaw"", 
			                    t.""UssInstanceId"", 
			                    t.""TrackBearing"",
                                t.""TrackBearingReference"",
			                    (t.""AltitudeGps"" ->> 'altitude_value'::text)::numeric AS ""AltitudeGps"", 
                                t.""TrackGroundSpeed""

                                FROM rankedpositions rp
                                    JOIN ""TelemetryMessage"" t ON t.""TimeMeasured"" = rp.""TimeMeasured""
                                WHERE rp.rnk = 1 AND rp.""TimeMeasured"" > (timezone('UTC'::text, CURRENT_TIMESTAMP) - '00:00:03'::interval second)
                            )
                        SELECT o.""UssInstanceId"",
                        o.""Gufi"",
                        o.""State"",
                        lp.""TelemetryMessageUid"",
                        lp.""Location"",
                        lp.""AltitudeGps"",
                        lp.""TimeMeasured"",
                        d.""Uid"",
                        d.""CollisionThreshold"",
                        format('SRID=4326;POINT(%s %s %s)'::text, st_x(lp.""Location""::geometry), st_y(lp.""Location""::geometry), lp.""AltitudeGps"") AS ""Location3D""
                        FROM latestpositions lp
                            JOIN ""Operation"" o ON lp.""Gufi"" = o.""Gufi""
                            JOIN ""Drone"" d ON lp.""Registration"" = d.""Uid"";

                        ALTER TABLE public.anra_view_latest_location
                            OWNER TO postgres;

                        GRANT ALL ON TABLE public.anra_view_latest_location TO postgres;
                        GRANT SELECT ON TABLE public.anra_view_latest_location TO PUBLIC;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            migrationBuilder.DropForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage");

            migrationBuilder.DropIndex(
                name: "IX_ConformanceLog_TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "TelemetryMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<int>(
                name: "TelemetryMessageId",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TelemetryMessageId",
                table: "TelemetryMessage",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "TelemetryMessageId",
                table: "ConformanceLog",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_TelemetryMessageId",
                table: "UtmMessage",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ConformanceLog_TelemetryMessageId",
                table: "ConformanceLog",
                column: "TelemetryMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageId",
                table: "ConformanceLog",
                column: "TelemetryMessageId",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                table: "UtmMessage",
                column: "TelemetryMessageId",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
