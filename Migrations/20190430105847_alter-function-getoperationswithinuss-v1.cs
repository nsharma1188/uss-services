﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctiongetoperationswithinussv1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, integer, integer, uuid);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationswithinuss(
	                        locationgeo point,
	                        radius integer,
	                        reclimit integer,
	                        orgid uuid)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text, ""WayPointsList"" jsonb) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS 
                        $BODY$

	                    DECLARE pointgeo geometry;

                        BEGIN
                            CREATE TEMPORARY TABLE temp_operations
                            (
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
				                ""OperationId"" integer NOT NULL
			                )
			                ON COMMIT DROP;

                            --If radius is 0 then use default radius as 100

                            IF(radius = 0) THEN
                                radius := 10000;
                            END IF;

                            IF(locationgeo IS NOT NULL) THEN

                                SELECT ST_SetSRID(locationgeo::geometry, 4326) INTO pointgeo;

                                IF(reclimit > 0) THEN
                                    --Fetch Internal Operation
                                    INSERT INTO temp_opvolume

                                        SELECT

                                            opv.""OperationGeography"",
						                    opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE (orgid IS NULL OR ops.""OrganizationId"" = orgid)

                                        AND ops.""IsInternalOperation"" = true
					                    AND ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
                                        ORDER BY ops.""OperationId"" DESC
                                        LIMIT reclimit;
					
					                    --Fetch External Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops

                                                ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE ops.""IsInternalOperation"" = false
                                        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                        AND ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
                                        ORDER BY ops.""OperationId"" DESC
					                    LIMIT reclimit;
                                ELSE
					                    --Fetch Internal Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE (orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                        AND ops.""IsInternalOperation"" = true
					                    AND ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
                                        ORDER BY ops.""OperationId"" DESC;
					
					                    --Fetch External Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops

                                                ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE ops.""IsInternalOperation"" = false
                                        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                        AND ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
                                        ORDER BY ops.""OperationId"" DESC;
			                    END IF;
                            ELSE
                                IF(reclimit > 0) THEN
					                    --Fetch Internal Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE (orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                        AND ops.""IsInternalOperation"" = true 
                                        ORDER BY ops.""OperationId"" DESC
                                        LIMIT reclimit;

					                    --Fetch External Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops

                                                ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE ops.""IsInternalOperation"" = false 
                                        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                        ORDER BY ops.""OperationId"" DESC
                                        LIMIT reclimit;
                                ELSE
					                    --Fetch Internal Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE (orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                        AND ops.""IsInternalOperation"" = true 
                                        ORDER BY ops.""OperationId"" DESC;

					                    --Fetch External Operation
                                        INSERT INTO temp_opvolume

                                        SELECT
                                            opv.""OperationGeography"",
                                            opv.""OperationId""


                                        FROM public.""OperationVolume"" AS opv

                                            INNER JOIN public.""Operation"" AS ops

                                                ON opv.""OperationId"" = ops.""OperationId"" 

                                        WHERE ops.""IsInternalOperation"" = false 
                                        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                        ORDER BY ops.""OperationId"" DESC;
                                END IF;
                            END IF;

                            INSERT INTO temp_operations
                            SELECT DISTINCT ops.""OperationId"" 
			                    FROM temp_opvolume ops;

                            RETURN QUERY

                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
                                    ops.""SlippyTileData"",
                                    ops.""DiscoveryReference"",
                                    ops.""WayPointsList""

                                FROM ""Operation"" ops
                                INNER JOIN temp_operations co
                                    ON co.""OperationId"" = ops.""OperationId""

                                    Order By ops.""OperationId"" DESC;
                        END;                                                                                                                            
                        $BODY$;

                        ALTER FUNCTION public.__getoperationswithinuss(point, integer, integer, uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION public.__getoperationswithinuss(point, integer, integer, uuid);");
        }
    }
}
