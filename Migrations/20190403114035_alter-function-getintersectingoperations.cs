﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctiongetintersectingoperations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getintersectingoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingoperations(
	                    messageid uuid)
                    RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


	                        Declare Geography polygon;
                            Declare EffectiveTimeBegin timestamp without time zone;
                            Declare EffectiveTimeEnd timestamp without time zone;
                            Declare MinAltitude numeric;
                            Declare MaxAltitude numeric;
                            DECLARE cur_constraintmsg CURSOR FOR SELECT* FROM temp_constraint_message;
                            BEGIN
                                CREATE TEMPORARY TABLE temp_constraint_message
                                (
                                    ""Geography"" polygon NOT NULL,
                                    ""EffectiveTimeBegin"" timestamp without time zone,
                                    ""EffectiveTimeEnd"" timestamp without time zone,
                                    ""MinAltitude"" numeric,
                                    ""MaxAltitude"" numeric
                                )
                                ON COMMIT DROP;

                                INSERT INTO temp_constraint_message
                                SELECT
                                    ""Geography"",
			                        ""EffectiveTimeBegin"", 
			                        ""EffectiveTimeEnd"", 
			                        (""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                        (""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude""
                                FROM public.""ConstraintMessage"" WHERE ""MessageId"" = messageid ;

                                CREATE TEMPORARY TABLE temp_intersectingOperations
                                (
			                        ""OperationId"" integer NOT NULL
                                )
                                ON COMMIT DROP;

		                        OPEN cur_constraintmsg;
                                LOOP
			                        -- fetch row into the film
                                    FETCH cur_constraintmsg INTO Geography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude;

			                        Insert into temp_intersectingOperations
                                    SELECT opv.""OperationId"" 
			                        from ""OperationVolume"" opv join ""Operation"" o on opv.""OperationId"" = o.""OperationId"" 
													                        AND o.""State"" NOT IN('CANCELLED', 'CLOSED')

                                                                            AND o.""IsInternalOperation""

			                        WHERE
                                        tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                        && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')


                                        AND
                                        numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                        && numrange(MinAltitude, MaxAltitude)

                                        AND

                                        ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(Geography::geometry, 4326));

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;

		                        -- Close the cursor
                                CLOSE cur_constraintmsg;

		                        RETURN QUERY

                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference""

                                FROM ""Operation"" ops
                                INNER JOIN temp_intersectingOperations co
                                    ON co.""OperationId"" = ops.""OperationId"";
                                END;                                        

                        
            $BODY$;

            ALTER FUNCTION public.__getintersectingoperations(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");
        }
    }
}
