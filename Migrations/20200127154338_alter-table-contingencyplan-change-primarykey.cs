﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablecontingencyplanchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_ContingencyPlan_ContingencyPlanId",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_ContingencyPlanId",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContingencyPlan",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "ContingencyPlanId",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "ContingencyPlanId",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<Guid>(
                name: "ContingencyPlanUid",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ContingencyPlanUid",
                table: "ContingencyPlan",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContingencyPlan",
                table: "ContingencyPlan",
                column: "ContingencyPlanUid");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_ContingencyPlanUid",
                table: "UtmMessage",
                column: "ContingencyPlanUid");

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_ContingencyPlan_ContingencyPlanUid",
                table: "UtmMessage",
                column: "ContingencyPlanUid",
                principalTable: "ContingencyPlan",
                principalColumn: "ContingencyPlanUid",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_ContingencyPlan_ContingencyPlanUid",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_ContingencyPlanUid",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ContingencyPlan",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "ContingencyPlanUid",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "ContingencyPlanUid",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<int>(
                name: "ContingencyPlanId",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContingencyPlanId",
                table: "ContingencyPlan",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ContingencyPlan",
                table: "ContingencyPlan",
                column: "ContingencyPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_ContingencyPlanId",
                table: "UtmMessage",
                column: "ContingencyPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_ContingencyPlan_ContingencyPlanId",
                table: "UtmMessage",
                column: "ContingencyPlanId",
                principalTable: "ContingencyPlan",
                principalColumn: "ContingencyPlanId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
