﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class createfunctionst_line_substring : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"CREATE OR REPLACE FUNCTION public.st_line_substring(
	                        geometry, double precision, double precision)
                            RETURNS geometry
                            LANGUAGE 'sql'

                            COST 100
                            IMMUTABLE STRICT PARALLEL SAFE
                        AS $BODY$ 
            
                            SELECT public._postgis_deprecate('ST_Line_Substring', 'ST_LineSubstring', '2.1.0');
                            SELECT public.ST_LineSubstring($1, $2, $3);
	 
                        $BODY$;

                        ALTER FUNCTION public.st_line_substring(geometry, double precision, double precision)
                            OWNER TO postgres;";
            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("ALTER FUNCTION IF EXISTS public.st_line_substring(geometry, double precision, double precision);");
        }
    }
}
