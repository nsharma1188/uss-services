﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class updatedbfunction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getintersectingusss
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingusss(
	                        constraint_geography polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone)
                            RETURNS TABLE(""Id"" integer,""Contact"" jsonb,""CoverageArea"" polygon,""DateCreated"" timestamp without time zone,""DateModified"" timestamp without time zone,""GridCells"" jsonb,""Notes"" text,""OrganizationId"" uuid,""TimeAvailableBegin"" timestamp without time zone,""TimeAvailableEnd"" timestamp without time zone,""TimeLastModified"" timestamp without time zone,""TimeSubmitted"" timestamp without time zone,""UssBaseCallbackUrl"" text,""UssInformationalUrl"" text,""UssInstanceId"" uuid,""UssName"" text,""UssOpenapiUrl"" text,""UssRegistrationUrl"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                        BEGIN

                            --Final Resultset
                            RETURN QUERY

                            SELECT 
	                            ""UtmInstance"".""Id"", 
                                ""UtmInstance"".""Contact"", 
	                            ""UtmInstance"".""CoverageArea"", 
	                            ""UtmInstance"".""DateCreated"", 
	                            ""UtmInstance"".""DateModified"", 
	                            ""UtmInstance"".""GridCells"", 
	                            ""UtmInstance"".""Notes"", 
	                            ""UtmInstance"".""OrganizationId"", 
	                            ""UtmInstance"".""TimeAvailableBegin"", 
	                            ""UtmInstance"".""TimeAvailableEnd"", 
	                            ""UtmInstance"".""TimeLastModified"", 
	                            ""UtmInstance"".""TimeSubmitted"", 
	                            ""UtmInstance"".""UssBaseCallbackUrl"", 
	                            ""UtmInstance"".""UssInformationalUrl"", 
	                            ""UtmInstance"".""UssInstanceId"", 
	                            ""UtmInstance"".""UssName"", 
	                            ""UtmInstance"".""UssOpenapiUrl"", 
	                            ""UtmInstance"".""UssRegistrationUrl""
                            FROM ""UtmInstance""
                            WHERE
                                tsrange(""UtmInstance"".""TimeAvailableBegin"", ""UtmInstance"".""TimeAvailableEnd"", '[]')
                                            && tsrange(begindt, enddt, '[]')
                                AND
                                ST_3DIntersects(ST_SetSRID(""UtmInstance"".""CoverageArea""::geometry, 4326),
                                                ST_SetSRID(constraint_geography::geometry, 4326));
                        END;                                                                
                    $BODY$;

                    ALTER FUNCTION public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbygufi
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbygufi(
                         gufi uuid)
                            RETURNS TABLE(""Id"" integer,""Contact"" jsonb,""CoverageArea"" polygon,""DateCreated"" timestamp without time zone,""DateModified"" timestamp without time zone,""GridCells"" jsonb,""Notes"" text,""OrganizationId"" uuid,""TimeAvailableBegin"" timestamp without time zone,""TimeAvailableEnd"" timestamp without time zone,""TimeLastModified"" timestamp without time zone,""TimeSubmitted"" timestamp without time zone,""UssBaseCallbackUrl"" text,""UssInformationalUrl"" text,""UssInstanceId"" uuid,""UssName"" text,""UssOpenapiUrl"" text,""UssRegistrationUrl"" text) 
                            LANGUAGE 'sql'

                            COST 100
                            VOLATILE 
                            ROWS 1000
                        AS $BODY$

                                SELECT 
	                                ""UtmInstance"".""Id"", 
                                    ""UtmInstance"".""Contact"", 
	                                ""UtmInstance"".""CoverageArea"", 
	                                ""UtmInstance"".""DateCreated"", 
	                                ""UtmInstance"".""DateModified"", 
	                                ""UtmInstance"".""GridCells"", 
	                                ""UtmInstance"".""Notes"", 
	                                ""UtmInstance"".""OrganizationId"", 
	                                ""UtmInstance"".""TimeAvailableBegin"", 
	                                ""UtmInstance"".""TimeAvailableEnd"", 
	                                ""UtmInstance"".""TimeLastModified"", 
	                                ""UtmInstance"".""TimeSubmitted"", 
	                                ""UtmInstance"".""UssBaseCallbackUrl"", 
	                                ""UtmInstance"".""UssInformationalUrl"", 
	                                ""UtmInstance"".""UssInstanceId"", 
	                                ""UtmInstance"".""UssName"", 
	                                ""UtmInstance"".""UssOpenapiUrl"", 
	                                ""UtmInstance"".""UssRegistrationUrl""
                                FROM ""UtmInstance""
                                WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                        FROM ""UtmInstance"" ""UtmInstance_1""  join ""Operation"" ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
                                        WHERE ""Operation"".""Gufi"" = gufi  limit 1))
                                    AND ""UtmInstance"".""UssInstanceId"" NOT IN(SELECT ""UtmInstance_1"".""UssInstanceId"" FROM ""UtmInstance"" ""UtmInstance_1""
            					    join ""Operation"" ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
            					    WHERE ""Operation"".""Gufi"" = gufi  limit 1)


                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbygufi(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbyinstanceid
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbyinstanceid(
	                        ussinstanceid uuid)
                            RETURNS TABLE(""Id"" integer,""Contact"" jsonb,""CoverageArea"" polygon,""DateCreated"" timestamp without time zone,""DateModified"" timestamp without time zone,""GridCells"" jsonb,""Notes"" text,""OrganizationId"" uuid,""TimeAvailableBegin"" timestamp without time zone,""TimeAvailableEnd"" timestamp without time zone,""TimeLastModified"" timestamp without time zone,""TimeSubmitted"" timestamp without time zone,""UssBaseCallbackUrl"" text,""UssInformationalUrl"" text,""UssInstanceId"" uuid,""UssName"" text,""UssOpenapiUrl"" text,""UssRegistrationUrl"" text) 
                            LANGUAGE 'sql'

                            COST 100
                            VOLATILE 
                            ROWS 1000
                        AS $BODY$

                                SELECT 
	                                ""UtmInstance"".""Id"", 
                                    ""UtmInstance"".""Contact"", 
	                                ""UtmInstance"".""CoverageArea"", 
	                                ""UtmInstance"".""DateCreated"", 
	                                ""UtmInstance"".""DateModified"", 
	                                ""UtmInstance"".""GridCells"", 
	                                ""UtmInstance"".""Notes"", 
	                                ""UtmInstance"".""OrganizationId"", 
	                                ""UtmInstance"".""TimeAvailableBegin"", 
	                                ""UtmInstance"".""TimeAvailableEnd"", 
	                                ""UtmInstance"".""TimeLastModified"", 
	                                ""UtmInstance"".""TimeSubmitted"", 
	                                ""UtmInstance"".""UssBaseCallbackUrl"", 
	                                ""UtmInstance"".""UssInformationalUrl"", 
	                                ""UtmInstance"".""UssInstanceId"", 
	                                ""UtmInstance"".""UssName"", 
	                                ""UtmInstance"".""UssOpenapiUrl"", 
	                                ""UtmInstance"".""UssRegistrationUrl""

                                FROM ""UtmInstance""
                                WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
  		                            AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                        FROM ""UtmInstance"" ""UtmInstance_1""
                                        WHERE ""UtmInstance_1"".""UssInstanceId"" = ussInstanceId LIMIT 1))
							        AND ""UtmInstance"".""UssInstanceId"" <> ussInstanceId

                            
                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbyinstanceid(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");
        }
    }
}
