﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertabledronechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDroneAssociation_Drone_DroneId",
                table: "UserDroneAssociation");

            migrationBuilder.DropIndex(
                name: "IX_UserDroneAssociation_DroneId",
                table: "UserDroneAssociation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Drone",
                table: "Drone");

            migrationBuilder.DropColumn(
                name: "DroneId",
                table: "Drone");

            migrationBuilder.AddColumn<Guid>(
                name: "Uid",
                table: "UserDroneAssociation",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Drone",
                table: "Drone",
                column: "Uid");

            migrationBuilder.CreateIndex(
                name: "IX_UserDroneAssociation_Uid",
                table: "UserDroneAssociation",
                column: "Uid");

            migrationBuilder.AddForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation",
                column: "Uid",
                principalTable: "Drone",
                principalColumn: "Uid",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDroneAssociation_Drone_Uid",
                table: "UserDroneAssociation");

            migrationBuilder.DropIndex(
                name: "IX_UserDroneAssociation_Uid",
                table: "UserDroneAssociation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Drone",
                table: "Drone");

            migrationBuilder.DropColumn(
                name: "Uid",
                table: "UserDroneAssociation");

            migrationBuilder.AddColumn<int>(
                name: "DroneId",
                table: "Drone",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Drone",
                table: "Drone",
                column: "DroneId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDroneAssociation_DroneId",
                table: "UserDroneAssociation",
                column: "DroneId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserDroneAssociation_Drone_DroneId",
                table: "UserDroneAssociation",
                column: "DroneId",
                principalTable: "Drone",
                principalColumn: "DroneId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
