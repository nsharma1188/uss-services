﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class deletetriggers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformance_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformancelog_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.contingency_trigger() CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS conformance_check ON public.\"TelemetryMessage\" CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.\"ConformanceLog\" CASCADE;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformance_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformancelog_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.contingency_trigger() CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS conformance_check ON public.\"TelemetryMessage\" CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.\"ConformanceLog\" CASCADE;");
        }
    }
}
