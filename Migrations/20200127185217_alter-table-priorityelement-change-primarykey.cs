﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablepriorityelementchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PriorityElement",
                table: "PriorityElement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pointout",
                table: "Pointout");

            migrationBuilder.DropColumn(
                name: "PriorityElementId",
                table: "PriorityElement");

            migrationBuilder.DropColumn(
                name: "PointoutId",
                table: "Pointout");

            migrationBuilder.AddColumn<Guid>(
                name: "PriorityElementUid",
                table: "PriorityElement",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "PointoutUid",
                table: "Pointout",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_PriorityElement",
                table: "PriorityElement",
                column: "PriorityElementUid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pointout",
                table: "Pointout",
                column: "PointoutUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PriorityElement",
                table: "PriorityElement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Pointout",
                table: "Pointout");

            migrationBuilder.DropColumn(
                name: "PriorityElementUid",
                table: "PriorityElement");

            migrationBuilder.DropColumn(
                name: "PointoutUid",
                table: "Pointout");

            migrationBuilder.AddColumn<int>(
                name: "PriorityElementId",
                table: "PriorityElement",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "PointoutId",
                table: "Pointout",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PriorityElement",
                table: "PriorityElement",
                column: "PriorityElementId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Pointout",
                table: "Pointout",
                column: "PointoutId");
        }
    }
}
