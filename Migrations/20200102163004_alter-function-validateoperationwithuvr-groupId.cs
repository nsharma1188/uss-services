﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctionvalidateoperationwithuvrgroupId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__validateoperationwithuvr
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__validateoperationwithuvr(
	                    flightgeo polygon,
	                    begindt timestamp without time zone,
	                    enddt timestamp without time zone,
	                    minaltitudewgs84ft double precision,
	                    maxaltitudewgs84ft double precision)
                        RETURNS TABLE(""ConstraintMessageId"" integer, ""EffectiveTimeBegin"" timestamp without time zone, ""Geography"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""EffectiveTimeEnd"" timestamp without time zone, ""MessageId"" uuid, ""Reason"" text, ""Type"" text, ""ActualTimeEnd"" timestamp without time zone, ""IsRestriction"" boolean, ""MaxAltitude"" jsonb, ""MinAltitude"" jsonb, ""PermittedGufis"" text[], ""PermittedUas"" text[], ""Cause"" text, ""UssName"" text, ""IsExpired"" boolean, ""GroupId"" text) 
                        LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE 
                        ROWS 1000
                    AS $BODY$

	                    BEGIN

                            --Create Temporary Constraint Table

                            DROP TABLE IF EXISTS constraint_table;
                                CREATE TABLE constraint_table AS


                            SELECT

                                uvr.""ConstraintMessageId"",
			                    ST_SetSRID(uvr.""Geography""::geometry, 4326) AS ""UVRGeography""

                            FROM public.""ConstraintMessage"" AS uvr

                            WHERE
                                tsrange(uvr.""EffectiveTimeBegin"", uvr.""EffectiveTimeEnd"", '[]') && tsrange(begindt, enddt, '[]')

                                AND
                                numrange((uvr.""MinAltitude"" #>> '{altitude_value}'):: numeric, (uvr.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
				                    && numrange(minaltitudewgs84ft::numeric, maxaltitudewgs84ft::numeric)
                                AND uvr.""IsExpired"" = false;

                                --Create Final Result Table

                                DROP TABLE IF EXISTS final_table;
			                    CREATE TABLE final_table AS

                                SELECT* FROM constraint_table WHERE ST_Intersects(""UVRGeography"", ST_SetSRID(flightgeo::geometry,4326));

			                    RETURN QUERY

                                SELECT
                                    uvr.""ConstraintMessageId"",
                                    uvr.""EffectiveTimeBegin"",
                                    uvr.""Geography"",
                                    uvr.""DateCreated"",
                                    uvr.""DateModified"",
                                    uvr.""EffectiveTimeEnd"",
                                    uvr.""MessageId"",
                                    uvr.""Reason"",
                                    uvr.""Type"",
                                    uvr.""ActualTimeEnd"",
                                    uvr.""IsRestriction"",
                                    uvr.""MaxAltitude"",
                                    uvr.""MinAltitude"",
                                    uvr.""PermittedGufis"",
                                    uvr.""PermittedUas"",
                                    uvr.""Cause"",
                                    uvr.""UssName"",
									uvr.""IsExpired"",
                                    uvr.""GroupId""

                                FROM public.""ConstraintMessage"" AS uvr

                                INNER JOIN final_table AS tblFinal
                                    ON uvr.""ConstraintMessageId"" = tblFinal.""ConstraintMessageId"";                                                                                        
	                    END;                   
                        

                    $BODY$;

                    ALTER FUNCTION public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
        }
    }
}
