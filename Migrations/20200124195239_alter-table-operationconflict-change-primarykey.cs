﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableoperationconflictchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationConflict",
                table: "OperationConflict");

            migrationBuilder.DropColumn(
                name: "OperationConflictId",
                table: "OperationConflict");

            migrationBuilder.AddColumn<Guid>(
                name: "OperationConflictUid",
                table: "OperationConflict",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationConflict",
                table: "OperationConflict",
                column: "OperationConflictUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OperationConflict",
                table: "OperationConflict");

            migrationBuilder.DropColumn(
                name: "OperationConflictUid",
                table: "OperationConflict");

            migrationBuilder.AddColumn<int>(
                name: "OperationConflictId",
                table: "OperationConflict",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperationConflict",
                table: "OperationConflict",
                column: "OperationConflictId");
        }
    }
}
