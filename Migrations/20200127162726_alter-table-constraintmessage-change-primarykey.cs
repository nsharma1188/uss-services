﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableconstraintmessagechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConstraintMessage",
                table: "ConstraintMessage");

            migrationBuilder.DropColumn(
                name: "ConstraintMessageId",
                table: "ConstraintMessage");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConstraintMessage",
                table: "ConstraintMessage",
                column: "MessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ConstraintMessage",
                table: "ConstraintMessage");

            migrationBuilder.AddColumn<int>(
                name: "ConstraintMessageId",
                table: "ConstraintMessage",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ConstraintMessage",
                table: "ConstraintMessage",
                column: "ConstraintMessageId");
        }
    }
}
