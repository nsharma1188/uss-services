﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class createfunctionvalidatedroneassignation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__validatedroneassignation(
	                        droneid uuid,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                        Declare EffectiveTimeBegin timestamp without time zone;
                                    Declare EffectiveTimeEnd timestamp without time zone;
                                    Declare OperationId integer;

                                    DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;

                                    BEGIN
                                        CREATE TEMPORARY TABLE temp_opvolume
                                        (
                                            ""EffectiveTimeBegin"" timestamp without time zone,
                                            ""EffectiveTimeEnd"" timestamp without time zone,
                                            ""OperationId"" integer NOT NULL
                                        )


                                ON COMMIT DROP;

                                    INSERT INTO temp_opvolume
                                    SELECT

                                    opv.""EffectiveTimeBegin"", 
			                        opv.""EffectiveTimeEnd"", 
			                        opv.""OperationId""


                                FROM public.""OperationVolume"" AS opv

                                INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId""

                                INNER JOIN public.""UasRegistration"" o ON ops.""OperationId"" = o.""OperationId""
		                        WHERE ops.""IsInternalOperation"" = true AND ops.""State"" IN ('ACTIVATED','ACCEPTED','NONCONFORMING','ROGUE');

                                CREATE TEMPORARY TABLE temp_conflictingOperations
                                (
			                        ""OperationId"" integer NOT NULL

                                )


                                ON COMMIT DROP;

		                        OPEN cur_opvs;
                                LOOP
			                        -- fetch row into the film
                                    FETCH cur_opvs INTO EffectiveTimeBegin, EffectiveTimeEnd, OperationId;

			                        INSERT INTO temp_conflictingOperations
                                    SELECT op.""OperationId"" 
			                        FROM ""Operation"" op
                                    WHERE

                                        tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')
					                        && tsrange(begindt, enddt, '[]');						
			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;

		                        -- Close the cursor
                                CLOSE cur_opvs;

		                        RETURN QUERY


                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
                                    ops.""SlippyTileData"",
                                    ops.""DiscoveryReference""


                                FROM ""Operation"" ops
                                INNER JOIN temp_conflictingOperations co
                                    ON co.""OperationId"" = ops.""OperationId"";
                                END;                        

                        $BODY$;

                        ALTER FUNCTION public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");
        }
    }
}
