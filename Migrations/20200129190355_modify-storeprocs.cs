﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class modifystoreprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__checkoperationintersection
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__checkoperationintersection(gufi uuid)
                    RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                 AS $BODY$
        	            Declare OperationGeography polygon;
                        Declare EffectiveTimeBegin timestamp without time zone;
                        Declare EffectiveTimeEnd timestamp without time zone;
                        Declare MinAltitude numeric;
                        Declare MaxAltitude numeric;
                        Declare operation_Gufi uuid;

                        DECLARE cur_opvs CURSOR FOR SELECT * FROM temp_opvolume;
                        
                        BEGIN
                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
                                ""EffectiveTimeBegin"" timestamp without time zone,
                                ""EffectiveTimeEnd"" timestamp without time zone,
                                ""MinAltitude"" numeric,
                                ""MaxAltitude"" numeric,
                                ""operation_Gufi"" uuid NOT NULL
                            )

                            ON COMMIT DROP;

                            INSERT INTO temp_opvolume
                            SELECT
                                opv.""OperationGeography"",
			                    opv.""EffectiveTimeBegin"", 
			                    opv.""EffectiveTimeEnd"", 
			                    (opv.""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                    (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude"",
			                    opv.""Gufi""

                            FROM public.""OperationVolume"" AS opv 
                                INNER JOIN public.""Operation"" AS ops 
                                    ON opv.""Gufi"" = ops.""Gufi"" 
                            WHERE ops.""Gufi"" = Gufi ;

                            CREATE TEMPORARY TABLE temp_conflictingOperations
                            (
			                    ""operation_Gufi"" uuid NOT NULL
                            )

                            ON COMMIT DROP;

		                    OPEN cur_opvs;

                            LOOP
			                    -- fetch row into the film
                                FETCH cur_opvs INTO OperationGeography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude, operation_Gufi;

			                    INSERT INTO temp_conflictingOperations

                                SELECT opv.""Gufi"" 
			                    FROM ""OperationVolume"" opv
                                    INNER JOIN ""Operation"" o ON opv.""Gufi"" = o.""Gufi"" 
				                        AND o.""State"" NOT IN('CANCELLED', 'CLOSED')
                                        AND o.""IsInternalOperation""
			                    WHERE
                                    tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                    && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')
                                    AND
                                        numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                        && numrange(MinAltitude, MaxAltitude)
                                    AND
                                        ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326));

			                    -- exit when no more row to fetch

                                EXIT WHEN NOT FOUND;
		                    END LOOP;

		                    -- Close the cursor
                            CLOSE cur_opvs;

		                    RETURN QUERY

                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
								ops.""WayPointsList"",
								ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM ""Operation"" ops
                                INNER JOIN temp_conflictingOperations co 
                                    ON co.""operation_Gufi"" = ops.""Gufi""
                            WHERE ops.""Gufi"" <> gufi;
                        END;                                                                                
                    
                $BODY$;

                ALTER FUNCTION public.__checkoperationintersection(uuid)
                OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getactiveoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getactiveoperations(
	                    searchby text,
	                    searchlocation point,
	                    droneuid text,
	                    radius integer)
                    
                    RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE
                        ROWS 1000
                    AS $BODY$

                    DECLARE sourcegeo geometry;

                    BEGIN
                        -- CASE - WHEN USER WILL SEARCH BY LOCATION
                        IF(searchby = 'LOCATION') THEN

                            SELECT
                                ST_SetSRID(
                                    searchlocation::geometry,
                                    4326
                                )
                            INTO sourcegeo;

                            --Prepare Result & Return

                            RETURN QUERY

                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
								ops.""WayPointsList"",
								ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM public.""OperationVolume"" AS opv
                                INNER JOIN public.""Operation"" AS ops
                                    ON opv.""Gufi"" = ops.""Gufi""
                                    AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                            WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), sourcegeo, radius, false )
							    AND opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND ops.""IsInternalOperation"" = true;
                        END IF;

                        -- CASE - WHEN USER WILL SEARCH BY DRONE
                        IF(searchby = 'DRONE') THEN
                            --Prepare Result & Return
                            RETURN QUERY
                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
								ops.""WayPointsList"",
								ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM public.""OperationVolume"" AS opv
                                INNER JOIN public.""Operation"" AS ops
                                    ON opv.""Gufi"" = ops.""Gufi"" 
                                    AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                INNER JOIN public.""UasRegistration"" AS uas
                                    ON ops.""Gufi"" = uas.""Gufi""
                                INNER JOIN public.""Drone"" AS drn
                                    ON uas.""RegistrationId"" = drn.""Uid""
                            WHERE CAST(drn.""Uid"" AS text) LIKE CONCAT('%', droneuid)
                                AND opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND ops.""IsInternalOperation"" = true;
                        END IF;
                    END;                    
                    $BODY$;

                ALTER FUNCTION public.__getactiveoperations(text, point, text, integer)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getintersectingoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingoperations(
	                    messageid uuid)
                    
                    RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE 
                        ROWS 1000
                    AS $BODY$

	                    Declare Geography polygon;
                        Declare EffectiveTimeBegin timestamp without time zone;
                        Declare EffectiveTimeEnd timestamp without time zone;
                        Declare MinAltitude numeric;
                        Declare MaxAltitude numeric;
                        DECLARE cur_constraintmsg CURSOR FOR SELECT* FROM temp_constraint_message;

                        BEGIN
                            CREATE TEMPORARY TABLE temp_constraint_message
                            (
                                ""Geography"" polygon NOT NULL,
                                ""EffectiveTimeBegin"" timestamp without time zone,
                                ""EffectiveTimeEnd"" timestamp without time zone,
                                ""MinAltitude"" numeric,
                                ""MaxAltitude"" numeric
                            )
                            ON COMMIT DROP;

                            INSERT INTO temp_constraint_message
                            SELECT
                                ""Geography"",
			                    ""EffectiveTimeBegin"", 
			                    ""EffectiveTimeEnd"", 
			                    (""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                    (""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude""
                            FROM public.""ConstraintMessage"" 
                            WHERE ""MessageId"" = messageid ;

                            CREATE TEMPORARY TABLE temp_intersectingOperations
                            (
			                    ""Gufi"" uuid NOT NULL
                            )

                            ON COMMIT DROP;

		                    OPEN cur_constraintmsg;
                            LOOP
			                    -- fetch row into the film
                                FETCH cur_constraintmsg INTO Geography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude;

			                    INSERT INTO temp_intersectingOperations
                                SELECT opv.""Gufi"" 
			                    FROM ""OperationVolume"" opv 
                                    JOIN ""Operation"" o 
                                        ON opv.""Gufi"" = o.""Gufi"" 
										AND o.""State"" NOT IN('CANCELLED', 'CLOSED')
                                        AND o.""IsInternalOperation""
			                    WHERE
                                    tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]') 
                                        && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')
                                    AND
                                        numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                        && numrange(MinAltitude, MaxAltitude)
                                    AND
                                        ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(Geography::geometry, 4326));

			                    -- exit when no more row to fetch
                                EXIT WHEN NOT FOUND;
		                    END LOOP;

		                    -- Close the cursor
                            CLOSE cur_constraintmsg;

		                    RETURN QUERY

                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
								ops.""WayPointsList"",
								ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM ""Operation"" ops
                                INNER JOIN temp_intersectingOperations co
                                    ON co.""Gufi"" = ops.""Gufi"";
                            END;                                        

                        
            $BODY$;

            ALTER FUNCTION public.__getintersectingoperations(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validatedroneassignation
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");

            sql = @"CREATE OR REPLACE FUNCTION public.__validatedroneassignation(
	                    droneid uuid,
	                    begindt timestamp without time zone,
	                    enddt timestamp without time zone)

                        RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                        LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                    AS $BODY$

	                    BEGIN

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""EffectiveTimeBegin"" timestamp without time zone,
                                ""EffectiveTimeEnd"" timestamp without time zone,
                                ""Gufi"" uuid NOT NULL
                            )

                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_conflictingOperations
                            (
                                ""Gufi"" uuid NOT NULL
		                    )

		                    ON COMMIT DROP;

                            INSERT INTO temp_opvolume
                            SELECT
                                opv.""EffectiveTimeBegin"", 
		                        opv.""EffectiveTimeEnd"", 
		                        opv.""Gufi""

                            FROM public.""OperationVolume"" AS opv
                                INNER JOIN public.""Operation"" AS ops 
                                    ON opv.""Gufi"" = ops.""Gufi""
                                INNER JOIN public.""UasRegistration"" o 
                                    ON ops.""Gufi"" = o.""Gufi""
                            WHERE ops.""IsInternalOperation"" = true 
			                    AND ops.""State"" IN ('ACTIVATED','ACCEPTED','NONCONFORMING','ROGUE')
                                AND o.""RegistrationId"" = droneid;

                            INSERT INTO temp_conflictingOperations
                            SELECT 
                                op.""Gufi"" 

		                    FROM temp_opvolume op
                            WHERE tsrange(op.""EffectiveTimeBegin"", op.""EffectiveTimeEnd"", '[]')
				                    && tsrange(begindt, enddt, '[]');

                            RETURN QUERY

                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
								ops.""WayPointsList"",
								ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM ""Operation"" ops
                                INNER JOIN temp_conflictingOperations co
                                    ON co.""Gufi"" = ops.""Gufi"";
                        END;                                                

                    $BODY$;

                    ALTER FUNCTION public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getoperationsbycriteria
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getoperationsbycriteria(
	                    registration_ids text,
	                    submit_time timestamp without time zone,
	                    operation_states text,
	                    distance integer,
	                    reference_point text)

                        RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                        LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                    AS $BODY$

						DECLARE registrationIds uuid[];
                        DECLARE opstates text[];
                        DECLARE refpoint numeric[];
                        DECLARE sourcegeo geometry;
                        DECLARE radius numeric;
                        DECLARE OperationGeography polygon;
                        DECLARE gufi uuid;
                        DECLARE cur_opvs CURSOR FOR SELECT * FROM temp_opvolume;

                    BEGIN

                        CREATE TEMPORARY TABLE temp_opvolume
                        (
                            ""OperationGeography"" polygon NOT NULL,
                            ""Gufi"" uuid NOT NULL
                        )
                        ON COMMIT DROP;

                        CREATE TEMPORARY TABLE temp_ops
                        (
                            ""Gufi"" uuid NOT NULL
						)
						ON COMMIT DROP;

                        CREATE TEMPORARY TABLE final_result
                        (
                            ""Gufi"" uuid NOT NULL
						)
						ON COMMIT DROP;

                        --Check for Registration Ids

                        registrationIds := string_to_array(registration_ids, ',')::uuid[];

                        IF(array_length(registrationIds, 1) > 0) THEN

                            INSERT INTO temp_ops
                            SELECT
                                ops.""Gufi""
                            FROM public.""Operation"" AS ops
                                INNER JOIN public.""UasRegistration"" dr 
                                    ON dr.""Gufi"" = ops.""Gufi""
							WHERE ops.""IsInternalOperation"" 
							    AND dr.""RegistrationId"" = any(registrationIds)
                                AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                OR(ops.""State"" = 'CLOSED' AND ops.""DecisionTime"" BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW())
                            ORDER BY ops.""SubmitTime"" DESC;

						END IF;
						
						--Check for Operation Submit Time
                        IF(submit_time IS NOT NULL) THEN

                            INSERT INTO temp_ops
                            SELECT
                                ops.""Gufi""
                            FROM public.""Operation"" AS ops
                            WHERE ops.""IsInternalOperation"" 
							    AND ops.""SubmitTime"" > submit_time
                            ORDER BY ops.""SubmitTime"" DESC;

                        END IF;
							
						-- Check for Operation States

                        opstates := string_to_array(operation_states, ',')::text[];
							
						IF(array_length(opstates,1) > 0) THEN

                            INSERT INTO temp_ops
                            SELECT
                                ops.""Gufi""
                            FROM public.""Operation"" AS ops
                            WHERE ops.""IsInternalOperation"" 
							    AND ops.""State"" = any(opstates)
                            ORDER BY ops.""SubmitTime"" DESC;
                        END IF;
							
						-- Check for Reference Point

                        refpoint := string_to_array(reference_point, ',')::numeric[];
							
						IF(array_length(refpoint,1) >= 2) THEN								
								
							--Convert distance into meter as per Postgis specs
                            radius := distance* 0.3048 ;
								
							--Prepare Circle geometry based on Ref Point and Distance
                            sourcegeo := ST_Buffer(ST_SetSRID(ST_Point(refpoint[1], refpoint[2]),4326)::geography,radius);
								
							INSERT INTO temp_opvolume
                            SELECT
                                opv.""OperationGeography"",
								opv.""Gufi""
							FROM public.""OperationVolume"" AS opv
                                INNER JOIN public.""Operation"" AS ops 
                                    ON opv.""Gufi"" = ops.""Gufi"" 
                            WHERE ops.""IsInternalOperation"" 
                            ORDER BY ops.""SubmitTime"" DESC;

                            OPEN cur_opvs;
                            LOOP
			                    -- fetch row
                                FETCH cur_opvs INTO OperationGeography, Gufi;

                                IF(ST_Intersects(ST_SetSRID(sourcegeo::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326))) THEN

                                    INSERT INTO temp_ops
                                    SELECT Gufi;

                                END IF;

			                    -- exit when no more row to fetch
                                EXIT WHEN NOT FOUND;

		                    END LOOP;

                        END If;
							
						--Select Distinct Records
                        INSERT INTO final_result
                        
                        SELECT DISTINCT op.""Gufi"" 
                        FROM temp_ops AS op;
							
						-- Return Final Output
                        RETURN QUERY

                        SELECT
                            ops.""Gufi"",
			                ops.""ControllerLocation"",
			                ops.""DateCreated"",
			                ops.""DateModified"",
			                ops.""DecisionTime"",
			                ops.""FaaRule"",
			                ops.""FlightComments"",
			                ops.""State"",
			                ops.""SubmitTime"",
			                ops.""UserId"",
			                ops.""UssInstanceId"",
			                ops.""UssName"",
			                ops.""UpdateTime"",
			                ops.""IsInternalOperation"",
			                ops.""OrganizationId"",
			                ops.""Contact"",
			                ops.""SlippyTileData"",
							ops.""WayPointsList"",
							ops.""CoordinatesList"",
                            ops.""ATCComments"",
                            ops.""GroupId""

						FROM public.""Operation"" AS ops
                        INNER JOIN final_result op 
                            ON op.""Gufi"" = ops.""Gufi"";
                    END;                    

                $BODY$;

                ALTER FUNCTION public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getoperationswithinuss
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getoperationswithinuss(
	                        locationgeo point,
	                        ussinstanceid uuid,
	                        radius integer,
	                        reclimit integer,
	                        orgid uuid)
                    
                    RETURNS TABLE(""Gufi"" uuid, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE
                        ROWS 1000
                    AS $BODY$

	                DECLARE pointgeo geometry;
                    DECLARE ussgeo polygon;

                    BEGIN
                        CREATE TEMPORARY TABLE final_result
                        (
                            ""Gufi"" uuid NOT NULL
                        )
                        ON COMMIT DROP;

                        CREATE TEMPORARY TABLE temp_operations
                        (
                            ""Gufi"" uuid NOT NULL
		                )
		                ON COMMIT DROP;

                        CREATE TEMPORARY TABLE temp_opvolume
                        (
                            ""OperationGeography"" polygon NOT NULL,
			                ""Gufi"" uuid NOT NULL
		                )
		                ON COMMIT DROP;

                        --input radius value will be in feet
                        --We will convert into meter for supporting postgis function
                        --If radius is 0 feet then use default radius as 5000 meter / 5 km
                        --else will convert input radius feet value into meter

                        IF(radius = 0) THEN
                            radius := 5000;
                        ELSE
                            radius := radius * 0.3048;
                        END IF;

                        IF(ussinstanceid IS NOT NULL) THEN
                            -- Fetch Uss Coverage Area
                            SELECT 
                                utm.""CoverageArea"" 
                            INTO ussgeo From public.""UtmInstance"" utm 
                            WHERE utm.""UssInstanceId"" = ussinstanceid;

						    --First Filter Operations based on reclimit & organization
                            IF(reclimit > 0) THEN
							    --Fetch Internal Operation
                                INSERT INTO temp_operations

                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
							        AND ops.""UssInstanceId"" = ussinstanceid
                                ORDER BY ops.""Gufi"" DESC
                                LIMIT reclimit;

							    --Fetch External Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE ops.""IsInternalOperation"" = false 
							        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
							    ORDER BY ops.""Gufi"" DESC
                                LIMIT reclimit;
						    ELSE
							    --Fetch Internal Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
							        AND ops.""UssInstanceId"" = ussinstanceid
                                ORDER BY ops.""Gufi"" DESC;

							    --Fetch External Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE ops.""IsInternalOperation"" = false 
							        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
							    ORDER BY ops.""Gufi"" DESC;
						    END IF;

                        ELSE
                            IF(reclimit > 0) THEN
							    --Fetch Internal Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT 
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 									
							    ORDER BY ops.""Gufi"" DESC
                                LIMIT reclimit;

							    --Fetch External Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE ops.""IsInternalOperation"" = false 
							        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                ORDER BY ops.""Gufi"" DESC
                                LIMIT reclimit;
						    ELSE
							    --Fetch Internal Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 								
							    ORDER BY ops.""Gufi"" DESC;

							    --Fetch External Operation
                                INSERT INTO temp_operations
                                SELECT DISTINCT
                                    ops.""Gufi""
							    FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv 
                                        ON opv.""Gufi"" = ops.""Gufi""
							    WHERE ops.""IsInternalOperation"" = false 
							        AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                ORDER BY ops.""Gufi"" DESC;
						    END IF;
                        END IF;

                        IF(locationgeo IS NOT NULL) THEN
                            SELECT ST_SetSRID(locationgeo::geometry, 4326) INTO pointgeo;

                            INSERT INTO temp_opvolume
                            SELECT
                                opv.""OperationGeography"",
				                opv.""Gufi""
			                FROM public.""OperationVolume"" AS opv
                                INNER JOIN temp_operations AS ops 
                                    ON opv.""Gufi"" = ops.""Gufi""
			                WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
			                ORDER BY ops.""Gufi"" DESC;
		                ELSE
			                --Now Fetch Operation Volumes for Filtered Operations
                            INSERT INTO temp_opvolume
                            SELECT
                                opv.""OperationGeography"",
				                opv.""Gufi""
			                FROM public.""OperationVolume"" AS opv
                                INNER JOIN temp_operations AS ops 
                                    ON opv.""Gufi"" = ops.""Gufi""
			                ORDER BY ops.""Gufi"" DESC;
		                END IF;

                        --First Insert Internal Operations																					  
                        INSERT INTO final_result
                        SELECT DISTINCT 
                            ops.""Gufi"" 
                        FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv 
                                ON opv.""Gufi"" = ops.""Gufi""
                        WHERE ops.""IsInternalOperation"" = true;

                        --Then Insert External Operations
                        --We will consider only last 12 hour external operations
                        INSERT INTO final_result
                        SELECT DISTINCT 
                            ops.""Gufi"" 
                        FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv 
                                ON opv.""Gufi"" = ops.""Gufi"" 
                        WHERE ops.""IsInternalOperation"" = false
                            AND (DATE_PART('day', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)* 24 +
						        DATE_PART('hour', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)) <= 12;

                        RETURN QUERY
                            SELECT
                                ops.""Gufi"",
			                    ops.""ControllerLocation"",
			                    ops.""DateCreated"",
			                    ops.""DateModified"",
			                    ops.""DecisionTime"",
			                    ops.""FaaRule"",
			                    ops.""FlightComments"",
			                    ops.""State"",
			                    ops.""SubmitTime"",
			                    ops.""UserId"",
			                    ops.""UssInstanceId"",
			                    ops.""UssName"",
			                    ops.""UpdateTime"",
			                    ops.""IsInternalOperation"",
			                    ops.""OrganizationId"",
			                    ops.""Contact"",
			                    ops.""SlippyTileData"",
							    ops.""WayPointsList"",
							    ops.""CoordinatesList"",
                                ops.""ATCComments"",
                                ops.""GroupId""

                            FROM ""Operation"" ops
                                INNER JOIN final_result co
                                    ON co.""Gufi"" = ops.""Gufi""
                            ORDER BY ops.""DateCreated"" DESC;
                        END;                                                                                                                            
                $BODY$;

                ALTER FUNCTION public.__getoperationswithinuss(point, uuid, integer, integer, uuid)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validateoperationwithtfr

            sql = @"DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);
                        CREATE OR REPLACE FUNCTION public.__validateoperationwithtfr(
	                        flightgeo polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone,
	                        minaltitudewgs84ft double precision,
	                        maxaltitudewgs84ft double precision)
                    
                    RETURNS TABLE(""NotamNumber"" uuid, ""Authority"" text, ""BeginDate"" timestamp without time zone, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""Description"" text, ""EndDate"" timestamp without time zone, ""FacilityId"" integer, ""IssueDate"" timestamp without time zone, ""NotamReason"" text, ""PointOfContact"" text, ""Requirements"" text[], ""Restriction"" text, ""StateId"" integer, ""Status"" text, ""TypeId"" integer, ""UserId"" text, ""OrganizationId"" uuid, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE
                        ROWS 1000
                    AS $BODY$
                        
                        BEGIN
                            --Create Temporary Notam Table
                            DROP TABLE IF EXISTS notam_table;
                            CREATE TABLE notam_table AS

                            SELECT
                                notam.""NotamNumber"",
                                notam.""BeginDate"",
                                notam.""EndDate"",
                                CASE
                                    WHEN notamarea.""Center""::geometry ISNULL
                                        THEN
                                            ST_SetSRID(notamarea.""Region""::geometry, 4326)
                                        ELSE
                                            ST_Buffer(ST_SetSRID(notamarea.""Center""::geometry, 4326), notamarea.""Radius"")
                                    END AS ""Geography""

                            FROM public.""Notam"" AS notam
                                INNER JOIN public.""NotamArea"" AS notamarea
                                    ON notam.""NotamNumber"" = notamarea.""NotamNumber"" 
                                    AND notam.""Status"" = 'ACTIVATED'
                            WHERE
                                tsrange(notam.""BeginDate"", notam.""EndDate"", '[]') && tsrange(begindt, enddt, '[]')
                                AND
                                    int8range(notamarea.""MinAltitude""::bigint, notamarea.""MaxAltitude""::bigint)
            		                && int8range(minaltitudewgs84ft::bigint, maxaltitudewgs84ft::bigint);
        
                            --Create Final Result Table
                            DROP TABLE IF EXISTS final_table;
                            CREATE TABLE final_table AS
                            SELECT 
                                * 
                            FROM notam_table 
                            WHERE ST_Intersects(""Geography"", ST_SetSRID(flightgeo::geometry,4326));

                            RETURN QUERY
                            SELECT
                                notam.""NotamNumber"",
                                notam.""Authority"",
                                notam.""BeginDate"",
                                notam.""DateCreated"",
                                notam.""DateModified"",
                                notam.""Description"",
                                notam.""EndDate"",
                                notam.""FacilityId"",
                                notam.""IssueDate"",
                                notam.""NotamReason"",
                                notam.""PointOfContact"",
                                notam.""Requirements"",
                                notam.""Restriction"",
                                notam.""StateId"",
                                notam.""Status"",
                                notam.""TypeId"",
                                notam.""UserId"",
                                notam.""OrganizationId"",
                                notam.""GroupId""

                            FROM public.""Notam"" AS notam
                                INNER JOIN final_table AS tblFinal
                                    ON notam.""NotamNumber"" = tblFinal.""NotamNumber"";                                                                                        
                        END;                   
                    $BODY$;

                    ALTER FUNCTION public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validateoperationwithuvr
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");

            sql = @"CREATE OR REPLACE FUNCTION public.__validateoperationwithuvr(
	                    flightgeo polygon,
	                    begindt timestamp without time zone,
	                    enddt timestamp without time zone,
	                    minaltitudewgs84ft double precision,
	                    maxaltitudewgs84ft double precision)
                    
                    RETURNS TABLE(""MessageId"" uuid, ""EffectiveTimeBegin"" timestamp without time zone, ""Geography"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""EffectiveTimeEnd"" timestamp without time zone, ""Reason"" text, ""Type"" text, ""ActualTimeEnd"" timestamp without time zone, ""IsRestriction"" boolean, ""MaxAltitude"" jsonb, ""MinAltitude"" jsonb, ""PermittedGufis"" text[], ""PermittedUas"" text[], ""Cause"" text, ""UssName"" text, ""IsExpired"" boolean, ""GroupId"" text) 
                    LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE
                        ROWS 1000
                    AS $BODY$

	                    BEGIN
                            --Create Temporary Constraint Table

                            DROP TABLE IF EXISTS constraint_table;
                                CREATE TABLE constraint_table AS

                            SELECT
                                uvr.""MessageId"",
			                    ST_SetSRID(uvr.""Geography""::geometry, 4326) AS ""UVRGeography""
                            FROM public.""ConstraintMessage"" AS uvr
                            WHERE
                                tsrange(uvr.""EffectiveTimeBegin"", uvr.""EffectiveTimeEnd"", '[]') && tsrange(begindt, enddt, '[]')
                                AND
                                    numrange((uvr.""MinAltitude"" #>> '{altitude_value}'):: numeric, (uvr.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
	    			                    && numrange(minaltitudewgs84ft::numeric, maxaltitudewgs84ft::numeric);

			                --Create Final Result Table

                            DROP TABLE IF EXISTS final_table;
			                CREATE TABLE final_table AS

                            SELECT
                                * 
                            FROM constraint_table 
                            WHERE ST_Intersects(""UVRGeography"", ST_SetSRID(flightgeo::geometry,4326));

			                RETURN QUERY

                            SELECT
                                uvr.""MessageId"",
                                uvr.""EffectiveTimeBegin"",
                                uvr.""Geography"",
                                uvr.""DateCreated"",
                                uvr.""DateModified"",
                                uvr.""EffectiveTimeEnd"",
                                uvr.""Reason"",
                                uvr.""Type"",
                                uvr.""ActualTimeEnd"",
                                uvr.""IsRestriction"",
                                uvr.""MaxAltitude"",
                                uvr.""MinAltitude"",
                                uvr.""PermittedGufis"",
                                uvr.""PermittedUas"",
                                uvr.""Cause"",
                                uvr.""UssName"",
                                uvr.""IsExpired"",
                                uvr.""GroupId""

                            FROM public.""ConstraintMessage"" AS uvr
                                INNER JOIN final_table AS tblFinal
                                    ON uvr.""MessageId"" = tblFinal.""MessageId"";                                                                                        
	                    END;                   

                    $BODY$;

                    ALTER FUNCTION public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbygufi
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbygufi(
                         gufi uuid)
                    RETURNS TABLE(""UssInstanceId"" uuid, ""Contact"" jsonb, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""GridCells"" jsonb, ""Notes"" text, ""OrganizationId"" uuid, ""TimeAvailableBegin"" timestamp without time zone, ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""GroupId"" text, ""UserId"" text) 
                    LANGUAGE 'sql'

                        COST 100
                        VOLATILE 
                        ROWS 1000

                    AS $BODY$

                        SELECT 
	                        ""UtmInstance"".""UssInstanceId"", 
                            ""UtmInstance"".""Contact"", 
	                        ""UtmInstance"".""CoverageArea"", 
	                        ""UtmInstance"".""DateCreated"", 
	                        ""UtmInstance"".""DateModified"", 
	                        ""UtmInstance"".""GridCells"", 
	                        ""UtmInstance"".""Notes"", 
	                        ""UtmInstance"".""OrganizationId"", 
	                        ""UtmInstance"".""TimeAvailableBegin"", 
	                        ""UtmInstance"".""TimeAvailableEnd"", 
	                        ""UtmInstance"".""TimeLastModified"", 
	                        ""UtmInstance"".""TimeSubmitted"", 
	                        ""UtmInstance"".""UssBaseCallbackUrl"", 
	                        ""UtmInstance"".""UssInformationalUrl"", 
	                        ""UtmInstance"".""UssName"", 
	                        ""UtmInstance"".""UssOpenapiUrl"", 
	                        ""UtmInstance"".""UssRegistrationUrl"",
                            ""UtmInstance"".""GroupId"",
                            ""UtmInstance"".""UserId""

                        FROM ""UtmInstance""
                        WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
                            AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                            AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, 
                                ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                    FROM ""UtmInstance"" ""UtmInstance_1""  
                                        JOIN ""Operation"" 
                                            ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
                                    WHERE ""Operation"".""Gufi"" = gufi  limit 1 ))
                            AND ""UtmInstance"".""UssInstanceId"" NOT IN
                                ( SELECT ""UtmInstance_1"".""UssInstanceId"" 
                                    FROM ""UtmInstance"" ""UtmInstance_1""
            				            JOIN ""Operation"" 
                                            ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
            				        WHERE ""Operation"".""Gufi"" = gufi  limit 1 )

                    $BODY$;

                ALTER FUNCTION public.__getnetworkbygufi(uuid)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbyinstanceid
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbyinstanceid(
	                        ussinstanceid uuid)
                    
                    RETURNS TABLE(""UssInstanceId"" uuid, ""Contact"" jsonb, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""GridCells"" jsonb, ""Notes"" text, ""OrganizationId"" uuid, ""TimeAvailableBegin"" timestamp without time zone, ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""GroupId"" text, ""UserId"" text) 
                    LANGUAGE 'sql'

                        COST 100
                        VOLATILE 
                        ROWS 1000
                    AS $BODY$

                        SELECT 
	                        ""UtmInstance"".""UssInstanceId"", 
                            ""UtmInstance"".""Contact"", 
	                        ""UtmInstance"".""CoverageArea"", 
	                        ""UtmInstance"".""DateCreated"", 
	                        ""UtmInstance"".""DateModified"", 
	                        ""UtmInstance"".""GridCells"", 
	                        ""UtmInstance"".""Notes"", 
	                        ""UtmInstance"".""OrganizationId"", 
	                        ""UtmInstance"".""TimeAvailableBegin"", 
	                        ""UtmInstance"".""TimeAvailableEnd"", 
	                        ""UtmInstance"".""TimeLastModified"", 
	                        ""UtmInstance"".""TimeSubmitted"", 
	                        ""UtmInstance"".""UssBaseCallbackUrl"", 
	                        ""UtmInstance"".""UssInformationalUrl"", 
	                        ""UtmInstance"".""UssName"", 
	                        ""UtmInstance"".""UssOpenapiUrl"", 
	                        ""UtmInstance"".""UssRegistrationUrl"",
                            ""UtmInstance"".""GroupId"",
                            ""UtmInstance"".""UserId""

                        FROM ""UtmInstance""
                        WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
  		                    AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                            AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, 
                                ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                    FROM ""UtmInstance"" ""UtmInstance_1""
                                    WHERE ""UtmInstance_1"".""UssInstanceId"" = ussInstanceId LIMIT 1 ))
							AND ""UtmInstance"".""UssInstanceId"" <> ussInstanceId
                            
                    $BODY$;

                    ALTER FUNCTION public.__getnetworkbyinstanceid(uuid)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");
        }
    }
}
