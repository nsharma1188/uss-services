﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablesignupchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Signup",
                table: "Signup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceArea",
                table: "ServiceArea");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Signup");

            migrationBuilder.DropColumn(
                name: "ServiceAreaId",
                table: "ServiceArea");

            migrationBuilder.AddColumn<Guid>(
                name: "SignupUid",
                table: "Signup",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Signup",
                table: "Signup",
                column: "SignupUid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceArea",
                table: "ServiceArea",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Signup",
                table: "Signup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ServiceArea",
                table: "ServiceArea");

            migrationBuilder.DropColumn(
                name: "SignupUid",
                table: "Signup");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Signup",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "ServiceAreaId",
                table: "ServiceArea",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Signup",
                table: "Signup",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ServiceArea",
                table: "ServiceArea",
                column: "ServiceAreaId");
        }
    }
}
