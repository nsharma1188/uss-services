﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablenotamchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NotamArea_Notam_NotamId",
                table: "NotamArea");

            migrationBuilder.DropIndex(
                name: "IX_NotamArea_NotamId",
                table: "NotamArea");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notam",
                table: "Notam");

            migrationBuilder.DropColumn(
                name: "NotamId",
                table: "NotamArea");

            migrationBuilder.DropColumn(
                name: "NotamId",
                table: "Notam");

            migrationBuilder.AddColumn<Guid>(
                name: "NotamNumber",
                table: "NotamArea",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notam",
                table: "Notam",
                column: "NotamNumber");

            migrationBuilder.CreateIndex(
                name: "IX_NotamArea_NotamNumber",
                table: "NotamArea",
                column: "NotamNumber",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NotamArea_Notam_NotamNumber",
                table: "NotamArea",
                column: "NotamNumber",
                principalTable: "Notam",
                principalColumn: "NotamNumber",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_NotamArea_Notam_NotamNumber",
                table: "NotamArea");

            migrationBuilder.DropIndex(
                name: "IX_NotamArea_NotamNumber",
                table: "NotamArea");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notam",
                table: "Notam");

            migrationBuilder.DropColumn(
                name: "NotamNumber",
                table: "NotamArea");

            migrationBuilder.AddColumn<int>(
                name: "NotamId",
                table: "NotamArea",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NotamId",
                table: "Notam",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notam",
                table: "Notam",
                column: "NotamId");

            migrationBuilder.CreateIndex(
                name: "IX_NotamArea_NotamId",
                table: "NotamArea",
                column: "NotamId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_NotamArea_Notam_NotamId",
                table: "NotamArea",
                column: "NotamId",
                principalTable: "Notam",
                principalColumn: "NotamId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
