﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablevehicleenginedetailremoveunnecessaryfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleEngineDetail",
                table: "VehicleEngineDetail");

            migrationBuilder.DropColumn(
                name: "VehicleEngineDetailsId",
                table: "VehicleEngineDetail");

            migrationBuilder.AddColumn<Guid>(
                name: "VehicleEngineDetailsUid",
                table: "VehicleEngineDetail",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleEngineDetail",
                table: "VehicleEngineDetail",
                column: "VehicleEngineDetailsUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VehicleEngineDetail",
                table: "VehicleEngineDetail");

            migrationBuilder.DropColumn(
                name: "VehicleEngineDetailsUid",
                table: "VehicleEngineDetail");

            migrationBuilder.AddColumn<int>(
                name: "VehicleEngineDetailsId",
                table: "VehicleEngineDetail",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_VehicleEngineDetail",
                table: "VehicleEngineDetail",
                column: "VehicleEngineDetailsId");
        }
    }
}
