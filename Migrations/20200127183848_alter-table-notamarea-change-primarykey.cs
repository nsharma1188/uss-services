﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablenotamareachangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NotamArea",
                table: "NotamArea");

            migrationBuilder.DropColumn(
                name: "NotamAreaId",
                table: "NotamArea");

            migrationBuilder.AddColumn<Guid>(
                name: "NotamAreaUid",
                table: "NotamArea",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotamArea",
                table: "NotamArea",
                column: "NotamAreaUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NotamArea",
                table: "NotamArea");

            migrationBuilder.DropColumn(
                name: "NotamAreaUid",
                table: "NotamArea");

            migrationBuilder.AddColumn<int>(
                name: "NotamAreaId",
                table: "NotamArea",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NotamArea",
                table: "NotamArea",
                column: "NotamAreaId");
        }
    }
}
