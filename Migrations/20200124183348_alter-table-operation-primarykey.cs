﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableoperationprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.anra_view_latest_location
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            var sql = @"CREATE OR REPLACE VIEW public.anra_view_latest_location AS
                        WITH rankedpositions AS (
                                SELECT ""TelemetryMessage"".""TimeMeasured"",
                                ""TelemetryMessage"".""Gufi"",
                                dense_rank() OVER(PARTITION BY ""TelemetryMessage"".""Gufi"" ORDER BY ""TelemetryMessage"".""TimeMeasured"" DESC) AS rnk
                                FROM ""TelemetryMessage""
                                GROUP BY ""TelemetryMessage"".""Gufi"", ""TelemetryMessage"".""TimeMeasured""
                            ), latestpositions AS(
                                SELECT
                                t.""TelemetryMessageId"", 
			                    t.""AltitudeNumGpsSatellites"", 
			                    t.""BatteryRemaining"", 
			                    t.""Climbrate"", 
			                    t.""DateCreated"", 
			                    t.""DateModified"", 
			                    t.""EnroutePositionsId"", 
			                    t.""Gufi"", 
			                    t.""HdopGps"", 
			                    t.""Heading"", 
			                    t.""Location"", 
			                    t.""Mode"", 
			                    t.""Pitch"", 
			                    t.""Registration"", 
			                    t.""Roll"", 
			                    t.""TimeMeasured"", 
			                    t.""TimeSent"", 
			                    t.""UssName"", 
			                    t.""VdopGps"", 
			                    t.""Yaw"", 
			                    t.""UssInstanceId"", 
			                    t.""TrackBearing"",
                                t.""TrackBearingReference"",
			                    (t.""AltitudeGps"" ->> 'altitude_value'::text)::numeric AS ""AltitudeGps"", 
                                t.""TrackGroundSpeed""

                                FROM rankedpositions rp
                                    JOIN ""TelemetryMessage"" t ON t.""TimeMeasured"" = rp.""TimeMeasured""
                                WHERE rp.rnk = 1 AND rp.""TimeMeasured"" > (timezone('UTC'::text, CURRENT_TIMESTAMP) - '00:00:03'::interval second)
                            )
                        SELECT o.""UssInstanceId"",
                        o.""Gufi"",
                        o.""State"",
                        lp.""TelemetryMessageId"",
                        lp.""Location"",
                        lp.""AltitudeGps"",
                        lp.""TimeMeasured"",
                        d.""Uid"",
                        d.""CollisionThreshold"",
                        format('SRID=4326;POINT(%s %s %s)'::text, st_x(lp.""Location""::geometry), st_y(lp.""Location""::geometry), lp.""AltitudeGps"") AS ""Location3D""
                        FROM latestpositions lp
                            JOIN ""Operation"" o ON lp.""Gufi"" = o.""Gufi""
                            JOIN ""Drone"" d ON lp.""Registration"" = d.""Uid"";

                        ALTER TABLE public.anra_view_latest_location
                            OWNER TO postgres;

                        GRANT ALL ON TABLE public.anra_view_latest_location TO postgres;
                        GRANT SELECT ON TABLE public.anra_view_latest_location TO PUBLIC;";

            migrationBuilder.Sql(sql.ToString());

            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_Operation_OperationId",
                table: "ContingencyPlan");

            migrationBuilder.DropForeignKey(
                name: "FK_NegotiationAgreement_Operation_OperationId",
                table: "NegotiationAgreement");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationConflict_Operation_OperationId",
                table: "OperationConflict");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationVolume_Operation_OperationId",
                table: "OperationVolume");

            migrationBuilder.DropForeignKey(
                name: "FK_PriorityElement_Operation_OperationId",
                table: "PriorityElement");

            migrationBuilder.DropForeignKey(
                name: "FK_UasRegistration_Operation_OperationId",
                table: "UasRegistration");

            migrationBuilder.DropIndex(
                name: "IX_UasRegistration_OperationId",
                table: "UasRegistration");

            migrationBuilder.DropIndex(
                name: "IX_PriorityElement_OperationId",
                table: "PriorityElement");

            migrationBuilder.DropIndex(
                name: "IX_OperationVolume_OperationId",
                table: "OperationVolume");

            migrationBuilder.DropIndex(
                name: "IX_OperationConflict_OperationId",
                table: "OperationConflict");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Operation",
                table: "Operation");

            migrationBuilder.DropIndex(
                name: "IX_NegotiationAgreement_OperationId",
                table: "NegotiationAgreement");

            migrationBuilder.DropIndex(
                name: "IX_ContingencyPlan_OperationId",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "UasRegistration");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "PriorityElement");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "OperationVolume");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "OperationConflict");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "NegotiationAgreement");

            migrationBuilder.DropColumn(
                name: "OperationId",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<Guid>(
                name: "Gufi",
                table: "UasRegistration",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Gufi",
                table: "PriorityElement",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Gufi",
                table: "OperationVolume",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Gufi",
                table: "NegotiationAgreement",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "Gufi",
                table: "ContingencyPlan",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Operation",
                table: "Operation",
                column: "Gufi");

            migrationBuilder.CreateIndex(
                name: "IX_UasRegistration_Gufi",
                table: "UasRegistration",
                column: "Gufi");

            migrationBuilder.CreateIndex(
                name: "IX_PriorityElement_Gufi",
                table: "PriorityElement",
                column: "Gufi",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OperationVolume_Gufi",
                table: "OperationVolume",
                column: "Gufi");

            migrationBuilder.CreateIndex(
                name: "IX_OperationConflict_Gufi",
                table: "OperationConflict",
                column: "Gufi");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationAgreement_Gufi",
                table: "NegotiationAgreement",
                column: "Gufi");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_Gufi",
                table: "ContingencyPlan",
                column: "Gufi");

            migrationBuilder.AddForeignKey(
                name: "FK_ContingencyPlan_Operation_Gufi",
                table: "ContingencyPlan",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NegotiationAgreement_Operation_Gufi",
                table: "NegotiationAgreement",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationConflict_Operation_Gufi",
                table: "OperationConflict",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationVolume_Operation_Gufi",
                table: "OperationVolume",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PriorityElement_Operation_Gufi",
                table: "PriorityElement",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UasRegistration_Operation_Gufi",
                table: "UasRegistration",
                column: "Gufi",
                principalTable: "Operation",
                principalColumn: "Gufi",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_Operation_Gufi",
                table: "ContingencyPlan");

            migrationBuilder.DropForeignKey(
                name: "FK_NegotiationAgreement_Operation_Gufi",
                table: "NegotiationAgreement");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationConflict_Operation_Gufi",
                table: "OperationConflict");

            migrationBuilder.DropForeignKey(
                name: "FK_OperationVolume_Operation_Gufi",
                table: "OperationVolume");

            migrationBuilder.DropForeignKey(
                name: "FK_PriorityElement_Operation_Gufi",
                table: "PriorityElement");

            migrationBuilder.DropForeignKey(
                name: "FK_UasRegistration_Operation_Gufi",
                table: "UasRegistration");

            migrationBuilder.DropIndex(
                name: "IX_UasRegistration_Gufi",
                table: "UasRegistration");

            migrationBuilder.DropIndex(
                name: "IX_PriorityElement_Gufi",
                table: "PriorityElement");

            migrationBuilder.DropIndex(
                name: "IX_OperationVolume_Gufi",
                table: "OperationVolume");

            migrationBuilder.DropIndex(
                name: "IX_OperationConflict_Gufi",
                table: "OperationConflict");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Operation",
                table: "Operation");

            migrationBuilder.DropIndex(
                name: "IX_NegotiationAgreement_Gufi",
                table: "NegotiationAgreement");

            migrationBuilder.DropIndex(
                name: "IX_ContingencyPlan_Gufi",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "Gufi",
                table: "UasRegistration");

            migrationBuilder.DropColumn(
                name: "Gufi",
                table: "PriorityElement");

            migrationBuilder.DropColumn(
                name: "Gufi",
                table: "OperationVolume");

            migrationBuilder.DropColumn(
                name: "Gufi",
                table: "NegotiationAgreement");

            migrationBuilder.DropColumn(
                name: "Gufi",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "UasRegistration",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "PriorityElement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "OperationVolume",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "OperationConflict",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "Operation",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "NegotiationAgreement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OperationId",
                table: "ContingencyPlan",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Operation",
                table: "Operation",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_UasRegistration_OperationId",
                table: "UasRegistration",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_PriorityElement_OperationId",
                table: "PriorityElement",
                column: "OperationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OperationVolume_OperationId",
                table: "OperationVolume",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationConflict_OperationId",
                table: "OperationConflict",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationAgreement_OperationId",
                table: "NegotiationAgreement",
                column: "OperationId");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_OperationId",
                table: "ContingencyPlan",
                column: "OperationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContingencyPlan_Operation_OperationId",
                table: "ContingencyPlan",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_NegotiationAgreement_Operation_OperationId",
                table: "NegotiationAgreement",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationConflict_Operation_OperationId",
                table: "OperationConflict",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OperationVolume_Operation_OperationId",
                table: "OperationVolume",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PriorityElement_Operation_OperationId",
                table: "PriorityElement",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UasRegistration_Operation_OperationId",
                table: "UasRegistration",
                column: "OperationId",
                principalTable: "Operation",
                principalColumn: "OperationId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
