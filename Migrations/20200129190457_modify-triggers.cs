﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class modifytriggers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformance_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformancelog_trigger() CASCADE;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.contingency_trigger() CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS conformance_check ON public.\"TelemetryMessage\" CASCADE;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.\"ConformanceLog\" CASCADE;");

            //Triggers

            //conformance_trigger
            var sql = @"CREATE or REPLACE  FUNCTION public.conformance_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF 
                        AS $BODY$

                                DECLARE
		                            isGeographyConfirming  boolean;
		                            isTimeConfirming  boolean;
		                            isAltitudeConfirming  boolean;
		                            isConfirming  boolean;
		                            nonConfirmanceGap int;
		                            lastTimeStamp timestamp;
		                            lastIsConfirming boolean;  
		                            nonConfirmances int;
		                            operationState text;

                                BEGIN
	
	 	                            CREATE TEMPORARY TABLE temp_conformance
		                            (
			                            ""OperationGeography"" polygon NOT NULL,
                                        ""EffectiveTimeBegin"" timestamp without time zone,
                                        ""EffectiveTimeEnd"" timestamp without time zone,
                                        ""MinAltitude"" numeric,
			                            ""MaxAltitude"" numeric, 
			                            ""Gufi"" uuid NOT NULL
		                            )
		                            ON COMMIT DROP;

                                    INSERT INTO temp_conformance
                                    SELECT
                                        opv.""OperationGeography"",
			                            opv.""EffectiveTimeBegin"", 
			                            opv.""EffectiveTimeEnd"", 
			                            (opv.""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
									    (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude"",
			                            opv.""Gufi""

                                    FROM public.""OperationVolume"" AS opv 
                                        INNER JOIN public.""Operation"" AS ops 
                                            ON opv.""Gufi"" = ops.""Gufi"" 
                                    WHERE ops.""Gufi"" = New.""Gufi"" 
                                    AND ST_Intersects(ST_SetSRID(""OperationGeography""::geometry, 4326), ST_SetSRID(NEW.""Location""::geometry, 4326))
                                    AND (timezone('UTC'::text, CURRENT_TIMESTAMP) >=  ""EffectiveTimeBegin""
                                        AND timezone('UTC'::text, CURRENT_TIMESTAMP) <=  ""EffectiveTimeEnd"");
 
		                            SELECT EXISTS(SELECT 1 from ""temp_conformance"" WHERE ST_Intersects(ST_SetSRID(""OperationGeography""::geometry, 4326), ST_SetSRID(NEW.""Location""::geometry, 4326))) 
		                                INTO isGeographyConfirming;

                                    SELECT COALESCE((timezone('UTC'::text, CURRENT_TIMESTAMP) >=  Min(""EffectiveTimeBegin"") and timezone('UTC'::text, CURRENT_TIMESTAMP) <=  max(""EffectiveTimeEnd"")), FALSE)
		                                INTO isTimeConfirming from ""temp_conformance"";

		                            SELECT COALESCE(((NEW.""AltitudeGps"" #>> '{altitude_value}'):: numeric >=  Min(""MinAltitude"") and (NEW.""AltitudeGps"" #>> '{altitude_value}'):: numeric <=  Max(""MaxAltitude"")), FALSE) 
		                                INTO isAltitudeConfirming from ""temp_conformance"";


                                    isConfirming := (isAltitudeConfirming AND isGeographyConfirming AND isTimeConfirming);
													
		                            --timegap between now and last message

                                    lastIsConfirming := TRUE;
		                            SELECT t1.""IsConfirming"" 
                                        INTO lastIsConfirming 
                                    FROM public.""ConformanceLog"" t1 
                                    WHERE ""Gufi"" = New.""Gufi"" 
                                    ORDER BY ""TimeStamp"" DESC limit 1;

                                    nonConfirmances := 0;
		                            if not isConfirming then
                                        IF lastIsConfirming THEN

                                            SELECT COALESCE(max(""NonConformances""),0) 
                                                INTO nonConfirmances 
                                            FROM public.""ConformanceLog"" 
				                            WHERE ""Gufi"" = New.""Gufi""; 
				                            nonConfirmances := nonConfirmances + 1;
			                            END IF;
                                    end if;

		                            nonConfirmanceGap := 0;
		                            if not lastIsConfirming then
                                        SELECT t1.""DifferenceInSeconds"" 
                                            INTO nonConfirmanceGap 
                                        FROM public.""ConformanceLog"" t1 
                                        WHERE ""Gufi"" = New.""Gufi"" 
                                        ORDER BY t1.""TimeStamp"" DESC limit 1;
                                        
                                        SELECT t1.""TimeStamp"" 
                                            INTO lastTimeStamp 
                                        FROM public.""ConformanceLog"" t1 
                                        WHERE ""Gufi"" = New.""Gufi"" 
                                        ORDER BY t1.""TimeStamp"" DESC limit 1;

                                        SELECT nonConfirmanceGap + EXTRACT(EPOCH FROM timezone('UTC'::text, CURRENT_TIMESTAMP)) - EXTRACT(EPOCH FROM lastTimeStamp) 
                                            INTO nonConfirmanceGap;
                                    end if;
													
		                            SELECT ""State"" 
                                        INTO operationState 
                                    FROM ""Operation"" 
                                    WHERE ""Gufi"" = New.""Gufi""; 
													
		                            if nonConfirmanceGap >= 30 or nonConfirmances >=3 then
                                        isConfirming := FALSE;
			                            operationState = 'ROGUE';
		                            ELSIF operationState<> 'ROGUE' THEN
                                        IF isConfirming THEN

                                            operationState := 'ACTIVATED';
                                        ELSE
                                            operationState :=	'NONCONFORMING';
                                        END IF;
                                    END IF;


                                    INSERT INTO public.""ConformanceLog""(
		                                ""CreatedDate"",
		                                ""ModifiedDate"",
		                                ""Gufi"",
		                                ""IsConfirming"",
		                                ""IsAltitudeConfirming"",
		                                ""IsGeographyConfirming"",
		                                ""IsTimeConfirming"",
		                                ""TelemetryMessageId"",
		                                ""DifferenceInSeconds"",
		                                ""NonConformances"",
		                                ""State"",
		                                ""TimeStamp"")
                                    VALUES(timezone('UTC'::text, CURRENT_TIMESTAMP),

                                        timezone('UTC'::text, CURRENT_TIMESTAMP),
				                        New.""Gufi"",
				                        isConfirming,
				                        isAltitudeConfirming,
				                        isGeographyConfirming,
				                        isTimeConfirming,
				                        New.""TelemetryMessageId"",
				                        nonConfirmanceGap,
				                        nonConfirmances,
				                        operationState,
                                        timezone('UTC'::text, CURRENT_TIMESTAMP));

								    -- Drop Temp Table
                                    DROP TABLE temp_conformance;
                                    RETURN NEW;
                                END;
                        $BODY$;

                        ALTER FUNCTION public.conformance_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql);

            //conformance_check
            sql = @"DROP TRIGGER IF EXISTS conformance_check ON public.""TelemetryMessage"";
                    CREATE TRIGGER conformance_check AFTER INSERT ON public.""TelemetryMessage"" FOR EACH ROW EXECUTE PROCEDURE public.conformance_trigger();";

            migrationBuilder.Sql(sql);

            //conformancelog_trigger
            sql = @"CREATE OR REPLACE FUNCTION public.conformancelog_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF 
                        AS $BODY$

                            BEGIN        
    	                        PERFORM pg_notify('ConfirmanceNotification', json_build_object('gufi', NEW.""Gufi"",'isconfirming', NEW.""IsConfirming"", 
							    'nonconformances', NEW.""NonConformances"",'state', NEW.""State"",  'timediff', NEW.""DifferenceInSeconds"", 'timestamp', CURRENT_TIMESTAMP)::text);
                            RETURN NEW;
                            END;

                        $BODY$;

                        ALTER FUNCTION public.conformancelog_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //trigger_conformanceLog
            sql = @"DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.""ConformanceLog"";
                    CREATE TRIGGER ""trigger_conformanceLog"" AFTER INSERT ON public.""ConformanceLog"" FOR EACH ROW EXECUTE PROCEDURE public.conformancelog_trigger();";
            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformance_trigger();");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformancelog_trigger();");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS conformance_check ON public.\"TelemetryMessage\"");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.\"ConformanceLog\";");
        }
    }
}
