﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctiongetoperationswithinussv7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");

			var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationswithinuss(
	                        locationgeo point,
	                        ussinstanceid uuid,
	                        radius integer,
	                        reclimit integer,
	                        orgid uuid)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                    DECLARE pointgeo geometry;
                        DECLARE ussgeo polygon;

                        BEGIN
                            CREATE TEMPORARY TABLE final_result
                            (
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_operations
                            (
                                ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
			                    ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            --input radius value will be in feet
                            --We will convert into meter for supporting postgis function
                            --If radius is 0 feet then use default radius as 5000 meter / 5 km
                            --else will convert input radius feet value into meter

                            IF(radius = 0) THEN
                                radius := 5000;
                            ELSE
                                radius := radius * 0.3048;
                            END IF;

                            IF(ussinstanceid IS NOT NULL) THEN

                                -- Fetch Uss Coverage Area
                                Select utm.""CoverageArea"" INTO ussgeo From public.""UtmInstance"" utm Where utm.""UssInstanceId"" = ussinstanceid;

								--First Filter Operations based on reclimit & organization
                                IF(reclimit > 0) THEN
									--Fetch Internal Operation
                                    INSERT INTO temp_operations

                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
									AND ops.""UssInstanceId"" = ussinstanceid
                                    ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
									ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;
								ELSE
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
									AND ops.""UssInstanceId"" = ussinstanceid
                                    ORDER BY ops.""OperationId"" DESC;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
									ORDER BY ops.""OperationId"" DESC;
								END IF;

                            ELSE
                                IF(reclimit > 0) THEN
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                    ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 									
									ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;
								ELSE
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 								
									ORDER BY ops.""OperationId"" DESC;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    ORDER BY ops.""OperationId"" DESC;
								END IF;
                            END IF;

                            IF(locationgeo IS NOT NULL) THEN
                                SELECT ST_SetSRID(locationgeo::geometry, 4326) INTO pointgeo;

                                INSERT INTO temp_opvolume
                                SELECT
                                    opv.""OperationGeography"",
				                    opv.""OperationId""
			                    FROM public.""OperationVolume"" AS opv
                                    INNER JOIN temp_operations AS ops ON opv.""OperationId"" = ops.""OperationId""
			                    WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
			                    ORDER BY ops.""OperationId"" DESC;
		                    ELSE
			                    --Now Fetch Operation Volumes for Filtered Operations
                                INSERT INTO temp_opvolume
                                SELECT
                                    opv.""OperationGeography"",
				                    opv.""OperationId""
			                    FROM public.""OperationVolume"" AS opv
                                    INNER JOIN temp_operations AS ops ON opv.""OperationId"" = ops.""OperationId""
			                    ORDER BY ops.""OperationId"" DESC;
		                    END IF;

                            --First Insert Internal Operations																					  
                            INSERT INTO final_result
                            SELECT DISTINCT ops.""OperationId"" FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv ON opv.""OperationId"" = ops.""OperationId""
                            WHERE ops.""IsInternalOperation"" = true;

                            --Then Insert External Operations
                            --We will consider only last 12 hour external operations
                            INSERT INTO final_result
                            SELECT DISTINCT ops.""OperationId"" FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv ON opv.""OperationId"" = ops.""OperationId"" 
                            WHERE ops.""IsInternalOperation"" = false
                            AND (DATE_PART('day', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)* 24 +
								DATE_PART('hour', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)) <= 12;

                            RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
                                    ops.""SlippyTileData"",
                                    ops.""DiscoveryReference"",
                                    ops.""WayPointsList"",
									ops.""CoordinatesList""

                                FROM ""Operation"" ops
                                INNER JOIN final_result co
                                    ON co.""OperationId"" = ops.""OperationId""
                                    Order By ops.""OperationId"" DESC;
                            END;                                                                                                                            
                    $BODY$;

                    ALTER FUNCTION public.__getoperationswithinuss(point, uuid, integer, integer, uuid)
                        OWNER TO postgres;";

			migrationBuilder.Sql(sql.ToString());

		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
			migrationBuilder.Sql("DROP FUNCTION public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");
		}
    }
}
