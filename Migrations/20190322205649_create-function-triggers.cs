﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class createfunctiontriggers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getoperationsbygriddata
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbygriddata(integer, integer, integer);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationsbygriddata(
	                        zoom integer,
	                        x integer,
	                        y integer)
                        RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                        LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE 
                        ROWS 1000
                    AS $BODY$


	                        BEGIN

                                RETURN QUERY
                                SELECT

                                    ops.""OperationId"",
			                        ops.""AircraftComments"",
			                        ops.""AirspaceAuthorization"",
			                        ops.""ControllerLocation"",
			                        ops.""CreatedBy"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""FlightNumber"",
			                        ops.""GcsLocation"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""VolumesDescription"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""FlightSpeed"",
			                        ops.""FlightStartTime"",
			                        ops.""EasyId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""DiscoveryReference""

                                FROM ""Operation"" ops

                                where
                                    (ops.""SlippyTileData"" #>> '{x}'):: numeric = x 
			                        AND
                                    (ops.""SlippyTileData"" #>> '{y}'):: numeric = y 
			                        AND
                                    (ops.""SlippyTileData"" #>> '{zoom}'):: numeric = zoom			
			                        AND ops.""State"" NOT IN('PENDING', 'PROPOSED', 'CANCELLED', 'CLOSED');
                                    END;                    
            

                        
            $BODY$;

            ALTER FUNCTION public.__getoperationsbygriddata(integer, integer, integer)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__checkoperationintersection
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__checkoperationintersection(gufi uuid)
                    RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                 AS $BODY$

 
        	                    Declare OperationGeography polygon;
                                Declare EffectiveTimeBegin timestamp without time zone;
                                Declare EffectiveTimeEnd timestamp without time zone;
                                Declare MinAltitude numeric;
                                Declare MaxAltitude numeric;
                                Declare OperationId integer;
                                DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;
                                BEGIN
                                    CREATE TEMPORARY TABLE temp_opvolume
                                    (
                                        ""OperationGeography"" polygon NOT NULL,
                                        ""EffectiveTimeBegin"" timestamp without time zone,
                                        ""EffectiveTimeEnd"" timestamp without time zone,
                                        ""MinAltitude"" numeric,
                                        ""MaxAltitude"" numeric,
                                        ""OperationId"" integer NOT NULL
                                    )

                                    ON COMMIT DROP;

                                    INSERT INTO temp_opvolume
                                    SELECT
                                        opv.""OperationGeography"",
			                            opv.""EffectiveTimeBegin"", 
			                            opv.""EffectiveTimeEnd"", 
			                            (opv.""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                            (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude"",
			                            opv.""OperationId""

                                    FROM public.""OperationVolume"" AS opv INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" WHERE ops.""Gufi"" = Gufi ;

                                    CREATE TEMPORARY TABLE temp_conflictingOperations
                                    (
			                            ""OperationId"" integer NOT NULL
                                    )

                                    ON COMMIT DROP;

		                            OPEN cur_opvs;
                                    LOOP
			                            -- fetch row into the film
                                        FETCH cur_opvs INTO OperationGeography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude, OperationId;

			                            INSERT INTO temp_conflictingOperations
                                        SELECT opv.""OperationId"" 
			                            FROM ""OperationVolume"" opv
                                            INNER JOIN ""Operation"" o ON opv.""OperationId"" = o.""OperationId"" 
				                            AND o.""State"" NOT IN('CANCELLED', 'CLOSED')

                                            AND o.""IsInternalOperation""

			                            WHERE
                                            tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                            && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')

                                            AND
                                            numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                            && numrange(MinAltitude, MaxAltitude)
                                            AND

                                            ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326));

			                            -- exit when no more row to fetch
                                        EXIT WHEN NOT FOUND;
		                            END LOOP;

		                            -- Close the cursor
                                    CLOSE cur_opvs;

		                            RETURN QUERY

                                    SELECT
                                        ops.""OperationId"",
                                        ops.""AircraftComments"",
                                        ops.""AirspaceAuthorization"",
                                        ops.""ControllerLocation"",
                                        ops.""CreatedBy"",
                                        ops.""DateCreated"",
                                        ops.""DateModified"",
                                        ops.""DecisionTime"",
                                        ops.""FaaRule"",
                                        ops.""FlightComments"",
                                        ops.""FlightNumber"",
                                        ops.""GcsLocation"",
                                        ops.""Gufi"",
                                        ops.""State"",
                                        ops.""SubmitTime"",
                                        ops.""UserId"",
                                        ops.""UssInstanceId"",
                                        ops.""VolumesDescription"",
                                        ops.""UssName"",
                                        ops.""UpdateTime"",
                                        ops.""IsInternalOperation"",
                                        ops.""OrganizationId"",
                                        ops.""FlightSpeed"",
                                        ops.""FlightStartTime"",
                                        ops.""EasyId"",
                                        ops.""Contact"",
										ops.""SlippyTileData"",
										ops.""DiscoveryReference""

                                    FROM ""Operation"" ops
                                    INNER JOIN temp_conflictingOperations co
                                        ON co.""OperationId"" = ops.""OperationId""
                                    where ops.""Gufi"" <> gufi;
                                END;                                                                                
                    
            $BODY$;

            ALTER FUNCTION public.__checkoperationintersection(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getactiveoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getactiveoperations(
	                    searchby text,
	                    searchlocation point,
	                    droneuid text,
	                    radius integer)
                    RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


                        DECLARE sourcegeo geometry;

                        BEGIN
                            -- CASE - WHEN USER WILL SEARCH BY LOCATION
                            IF(searchby = 'LOCATION') THEN
                                SELECT
                                    ST_SetSRID(
                                        searchlocation::geometry,
                                        4326
                                    )
                                INTO sourcegeo;

                                --Prepare Result & Return

                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
									ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
									ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), sourcegeo, radius, false )                                                        
								    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;

                            -- CASE - WHEN USER WILL SEARCH BY DRONE
                            IF(searchby = 'DRONE') THEN
                                --Prepare Result & Return
                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
								    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
								    ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    INNER JOIN public.""UasRegistration"" AS uas
                                            ON ops.""OperationId"" = uas.""OperationId""
                                    INNER JOIN public.""Drone"" AS drn
                                            ON uas.""RegistrationId"" = drn.""Uid""
                                    WHERE drn.""Uid"" = CAST(droneuid AS uuid)

                                    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;
                    END;                    
            
            $BODY$;

            ALTER FUNCTION public.__getactiveoperations(text, point, text, integer)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getintersectingoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingoperations(
	                    messageid uuid)
                    RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


	                        Declare Geography polygon;
                            Declare EffectiveTimeBegin timestamp without time zone;
                            Declare EffectiveTimeEnd timestamp without time zone;
                            Declare MinAltitude numeric;
                            Declare MaxAltitude numeric;
                            DECLARE cur_constraintmsg CURSOR FOR SELECT* FROM temp_constraint_message;
                            BEGIN
                                CREATE TEMPORARY TABLE temp_constraint_message
                                (
                                    ""Geography"" polygon NOT NULL,
                                    ""EffectiveTimeBegin"" timestamp without time zone,
                                    ""EffectiveTimeEnd"" timestamp without time zone,
                                    ""MinAltitude"" numeric,
                                    ""MaxAltitude"" numeric
                                )
                                ON COMMIT DROP;

                                INSERT INTO temp_constraint_message
                                SELECT
                                    ""Geography"",
			                        ""EffectiveTimeBegin"", 
			                        ""EffectiveTimeEnd"", 
			                        (""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                        (""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude""
                                FROM public.""ConstraintMessage"" WHERE ""MessageId"" = messageid AND ""IsRestriction"" ;

                                CREATE TEMPORARY TABLE temp_intersectingOperations
                                (
			                        ""OperationId"" integer NOT NULL
                                )
                                ON COMMIT DROP;

		                        OPEN cur_constraintmsg;
                                LOOP
			                        -- fetch row into the film
                                    FETCH cur_constraintmsg INTO Geography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude;

			                        Insert into temp_intersectingOperations
                                    SELECT opv.""OperationId"" 
			                        from ""OperationVolume"" opv join ""Operation"" o on opv.""OperationId"" = o.""OperationId"" 
													                        AND o.""State"" NOT IN('CANCELLED', 'CLOSED')

                                                                            AND o.""IsInternalOperation""

			                        WHERE
                                        tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                        && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')


                                        AND
                                        numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                        && numrange(MinAltitude, MaxAltitude)

                                        AND

                                        ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(Geography::geometry, 4326));

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;

		                        -- Close the cursor
                                CLOSE cur_constraintmsg;

		                        RETURN QUERY

                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference""

                                FROM ""Operation"" ops
                                INNER JOIN temp_intersectingOperations co
                                    ON co.""OperationId"" = ops.""OperationId"";
                                END;                                        

                        
            $BODY$;

            ALTER FUNCTION public.__getintersectingoperations(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getintersectingusss
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingusss(
	                        constraint_geography polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone)
                            RETURNS TABLE(""Id"" integer, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""Notes"" text, ""TimeAvailableBegin"" timestamp without time zone, ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""OrganizationId"" uuid) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                        BEGIN

                            --Final Resultset
                            RETURN QUERY

                            SELECT
                                utm.""Id"",
								utm.""CoverageArea"",
								utm.""DateCreated"",
								utm.""DateModified"",
								utm.""Notes"",
								utm.""TimeAvailableBegin"",
								utm.""TimeAvailableEnd"",
								utm.""TimeLastModified"",
								utm.""TimeSubmitted"",
								utm.""UssBaseCallbackUrl"",
								utm.""UssInformationalUrl"",
								utm.""UssInstanceId"",
								utm.""UssName"",
								utm.""UssOpenapiUrl"",
								utm.""UssRegistrationUrl"",
								utm.""OrganizationId""

                            FROM ""UtmInstance"" utm
                            WHERE
                                tsrange(utm.""TimeAvailableBegin"", utm.""TimeAvailableEnd"", '[]')
                                            && tsrange(begindt, enddt, '[]')
                                AND
                                ST_3DIntersects(ST_SetSRID(utm.""CoverageArea""::geometry, 4326),
                                                ST_SetSRID(constraint_geography::geometry, 4326));
                        END;                                                                
                    $BODY$;

                    ALTER FUNCTION public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbygufi
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbygufi(
                         gufi uuid)
                            RETURNS TABLE(""Id"" integer, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""Notes"" text, ""TimeAvailableBegin"" timestamp without time zone, ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""OrganizationId"" uuid) 
                            LANGUAGE 'sql'

                            COST 100
                            VOLATILE 
                            ROWS 1000
                        AS $BODY$

                                SELECT ""UtmInstance"".""Id"",
                                ""UtmInstance"".""CoverageArea"",
                                ""UtmInstance"".""DateCreated"",
                                ""UtmInstance"".""DateModified"",
                                ""UtmInstance"".""Notes"",
                                ""UtmInstance"".""TimeAvailableBegin"",
                                ""UtmInstance"".""TimeAvailableEnd"",
                                ""UtmInstance"".""TimeLastModified"",
                                ""UtmInstance"".""TimeSubmitted"",
                                ""UtmInstance"".""UssBaseCallbackUrl"",
                                ""UtmInstance"".""UssInformationalUrl"",
                                ""UtmInstance"".""UssInstanceId"",
                                ""UtmInstance"".""UssName"",
                                ""UtmInstance"".""UssOpenapiUrl"",
                                ""UtmInstance"".""UssRegistrationUrl"",
                                ""UtmInstance"".""OrganizationId""
                                FROM ""UtmInstance""
                                WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
                                AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                        FROM ""UtmInstance"" ""UtmInstance_1""  join ""Operation"" ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
                                        WHERE ""Operation"".""Gufi"" = gufi  limit 1))
                                    AND ""UtmInstance"".""UssInstanceId"" NOT IN(SELECT ""UtmInstance_1"".""UssInstanceId"" FROM ""UtmInstance"" ""UtmInstance_1""
            					    join ""Operation"" ON (""UtmInstance_1"".""UssInstanceId"" = ""Operation"".""UssInstanceId"")
            					    WHERE ""Operation"".""Gufi"" = gufi  limit 1)


                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbygufi(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbyinstanceid
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbyinstanceid(
	                        ussinstanceid uuid)
                            RETURNS TABLE(""Id"" integer, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, 
                            ""DateModified"" timestamp without time zone, ""Notes"" text, ""TimeAvailableBegin"" timestamp without time zone, 
                            ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, 
                            ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, 
                            ""UssInstanceId"" uuid, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""OrganizationId"" uuid) 
                            LANGUAGE 'sql'

                            COST 100
                            VOLATILE 
                            ROWS 1000
                        AS $BODY$


                                SELECT ""UtmInstance"".""Id"",
                                ""UtmInstance"".""CoverageArea"",
                                ""UtmInstance"".""DateCreated"",
                                ""UtmInstance"".""DateModified"",
                                ""UtmInstance"".""Notes"",
                                ""UtmInstance"".""TimeAvailableBegin"",
                                ""UtmInstance"".""TimeAvailableEnd"",
                                ""UtmInstance"".""TimeLastModified"",
                                ""UtmInstance"".""TimeSubmitted"",
                                ""UtmInstance"".""UssBaseCallbackUrl"",
                                ""UtmInstance"".""UssInformationalUrl"",
                                ""UtmInstance"".""UssInstanceId"",
                                ""UtmInstance"".""UssName"",
                                ""UtmInstance"".""UssOpenapiUrl"",
                                ""UtmInstance"".""UssRegistrationUrl"",
                                ""UtmInstance"".""OrganizationId""

                                FROM ""UtmInstance""
                                WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
  		                            AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                        FROM ""UtmInstance"" ""UtmInstance_1""
                                        WHERE ""UtmInstance_1"".""UssInstanceId"" = ussInstanceId LIMIT 1))
							        AND ""UtmInstance"".""UssInstanceId"" <> ussInstanceId

                            
                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbyinstanceid(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getrunningoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getrunningoperations();");

            sql = @"CREATE OR REPLACE FUNCTION public.__getrunningoperations()
                    RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb,""DiscoveryReference"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


                        BEGIN
                            --Prepare Result &Return
                            RETURN QUERY
                            SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference""


                            FROM public.""Operation"" AS ops
                            WHERE ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED');

                            END;                                                          
                    
            $BODY$;

            ALTER FUNCTION public.__getrunningoperations()
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validateoperationwithtfr


            sql = @"DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);
                        CREATE OR REPLACE FUNCTION public.__validateoperationwithtfr(
	                        flightgeo polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone,
	                        minaltitudewgs84ft double precision,
	                        maxaltitudewgs84ft double precision)
                            RETURNS TABLE(""NotamId"" integer, ""Authority"" text, ""BeginDate"" timestamp without time zone, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""Description"" text, ""EndDate"" timestamp without time zone, ""FacilityId"" integer, ""IssueDate"" timestamp without time zone, ""NotamNumber"" uuid, ""NotamReason"" text, ""PointOfContact"" text, ""Requirements"" text[], ""Restriction"" text, ""StateId"" integer, ""Status"" text, ""TypeId"" integer, ""UserId"" text, ""OrganizationId"" uuid) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                                BEGIN

                                    --Create Temporary Notam Table
                                    DROP TABLE IF EXISTS notam_table;
                                    CREATE TABLE notam_table AS

                                    SELECT
                                        notam.""NotamId"",
                                        notam.""BeginDate"",
                                        notam.""EndDate"",
                                        CASE
                                            WHEN notamarea.""Center""::geometry ISNULL
                                                THEN
                                                    ST_SetSRID(notamarea.""Region""::geometry, 4326)
                                                ELSE
                                                    ST_Buffer(ST_SetSRID(notamarea.""Center""::geometry, 4326), notamarea.""Radius"")
                                            END AS ""Geography""

                                    FROM public.""Notam"" AS notam

                                        INNER JOIN public.""NotamArea"" AS notamarea

                                            ON notam.""NotamId"" = notamarea.""NotamId"" AND notam.""Status"" = 'ACTIVATED'

                                    WHERE
                                        tsrange(notam.""BeginDate"", notam.""EndDate"", '[]') && tsrange(begindt, enddt, '[]')

                                        AND
                                        int8range(notamarea.""MinAltitude""::bigint, notamarea.""MaxAltitude""::bigint)
        		                        && int8range(minaltitudewgs84ft::bigint, maxaltitudewgs84ft::bigint);
        
                                        --Create Final Result Table
                                        DROP TABLE IF EXISTS final_table;
                                        CREATE TABLE final_table AS
                                        SELECT * FROM notam_table WHERE ST_Intersects(""Geography"", ST_SetSRID(flightgeo::geometry,4326));

                                        RETURN QUERY
                                        SELECT
                                            notam.""NotamId"",
                                            notam.""Authority"",
                                            notam.""BeginDate"",
                                            notam.""DateCreated"",
                                            notam.""DateModified"",
                                            notam.""Description"",
                                            notam.""EndDate"",
                                            notam.""FacilityId"",
                                            notam.""IssueDate"",
                                            notam.""NotamNumber"",
                                            notam.""NotamReason"",
                                            notam.""PointOfContact"",
                                            notam.""Requirements"",
                                            notam.""Restriction"",
                                            notam.""StateId"",
                                            notam.""Status"",
                                            notam.""TypeId"",
                                            notam.""UserId"",
                                            notam.""OrganizationId""
                                        FROM public.""Notam"" AS notam
                                        INNER JOIN final_table AS tblFinal
                                            ON notam.""NotamId"" = tblFinal.""NotamId"";                                                                                        
                                END;                   
                        $BODY$;

                        ALTER FUNCTION public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validateoperationwithuvr
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");

            sql = @"CREATE OR REPLACE FUNCTION public.__validateoperationwithuvr(
	                    flightgeo polygon,
	                    begindt timestamp without time zone,
	                    enddt timestamp without time zone,
	                    minaltitudewgs84ft double precision,
	                    maxaltitudewgs84ft double precision)
                        RETURNS TABLE(""ConstraintMessageId"" integer, ""EffectiveTimeBegin"" timestamp without time zone, ""Geography"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""EffectiveTimeEnd"" timestamp without time zone, ""MessageId"" uuid, ""Reason"" text, ""Type"" text, ""ActualTimeEnd"" timestamp without time zone, ""IsRestriction"" boolean, ""MaxAltitude"" jsonb, ""MinAltitude"" jsonb, ""PermittedGufis"" text[], ""PermittedUas"" text[], ""Cause"" text, ""UssName"" text) 
                        LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE
                        ROWS 1000
                    AS $BODY$

	                    BEGIN

                            --Create Temporary Constraint Table

                            DROP TABLE IF EXISTS constraint_table;
                                CREATE TABLE constraint_table AS


                            SELECT

                                uvr.""ConstraintMessageId"",
			                    ST_SetSRID(uvr.""Geography""::geometry, 4326) AS ""UVRGeography""

                            FROM public.""ConstraintMessage"" AS uvr

                            WHERE
                                tsrange(uvr.""EffectiveTimeBegin"", uvr.""EffectiveTimeEnd"", '[]') && tsrange(begindt, enddt, '[]')

                                AND
                                numrange((uvr.""MinAltitude"" #>> '{altitude_value}'):: numeric, (uvr.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
				                    && numrange(minaltitudewgs84ft::numeric, maxaltitudewgs84ft::numeric);

			                    --Create Final Result Table

                                DROP TABLE IF EXISTS final_table;
			                    CREATE TABLE final_table AS

                                SELECT* FROM constraint_table WHERE ST_Intersects(""UVRGeography"", ST_SetSRID(flightgeo::geometry,4326));

			                    RETURN QUERY

                                SELECT
                                    uvr.""ConstraintMessageId"",
                                    uvr.""EffectiveTimeBegin"",
                                    uvr.""Geography"",
                                    uvr.""DateCreated"",
                                    uvr.""DateModified"",
                                    uvr.""EffectiveTimeEnd"",
                                    uvr.""MessageId"",
                                    uvr.""Reason"",
                                    uvr.""Type"",
                                    uvr.""ActualTimeEnd"",
                                    uvr.""IsRestriction"",
                                    uvr.""MaxAltitude"",
                                    uvr.""MinAltitude"",
                                    uvr.""PermittedGufis"",
                                    uvr.""PermittedUas"",
                                    uvr.""Cause"",
                                    uvr.""UssName""

                                FROM public.""ConstraintMessage"" AS uvr

                                INNER JOIN final_table AS tblFinal
                                    ON uvr.""ConstraintMessageId"" = tblFinal.""ConstraintMessageId"";                                                                                        
	                    END;                   
                        

                    $BODY$;

                    ALTER FUNCTION public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.anra_view_latest_location
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            sql = @"CREATE OR REPLACE VIEW public.anra_view_latest_location AS
                        WITH rankedpositions AS (
                                SELECT ""TelemetryMessage"".""TimeMeasured"",
                                ""TelemetryMessage"".""Gufi"",
                                dense_rank() OVER(PARTITION BY ""TelemetryMessage"".""Gufi"" ORDER BY ""TelemetryMessage"".""TimeMeasured"" DESC) AS rnk
                                FROM ""TelemetryMessage""
                                GROUP BY ""TelemetryMessage"".""Gufi"", ""TelemetryMessage"".""TimeMeasured""
                            ), latestpositions AS(
                                SELECT
                                t.""TelemetryMessageId"", 
			                    t.""AltitudeNumGpsSatellites"", 
			                    t.""BatteryRemaining"", 
			                    t.""Climbrate"", 
			                    t.""DateCreated"", 
			                    t.""DateModified"", 
			                    t.""EnroutePositionsId"", 
			                    t.""Gufi"", 
			                    t.""HdopGps"", 
			                    t.""Heading"", 
			                    t.""Location"", 
			                    t.""Mode"", 
			                    t.""Pitch"", 
			                    t.""Registration"", 
			                    t.""Roll"", 
			                    t.""TimeMeasured"", 
			                    t.""TimeSent"", 
			                    t.""UssName"", 
			                    t.""VdopGps"", 
			                    t.""Yaw"", 
			                    t.""UssInstanceId"", 
			                    t.""TrackBearing"",
                                t.""TrackBearingReference"",
			                    (t.""AltitudeGps"" ->> 'altitude_value'::text)::numeric AS ""AltitudeGps"", 
                                t.""TrackGroundSpeed""

                                FROM rankedpositions rp
                                    JOIN ""TelemetryMessage"" t ON t.""TimeMeasured"" = rp.""TimeMeasured""
                                WHERE rp.rnk = 1 AND rp.""TimeMeasured"" > (timezone('UTC'::text, CURRENT_TIMESTAMP) - '00:00:03'::interval second)
                            )
                        SELECT o.""OperationId"",
                        o.""UssInstanceId"",
                        o.""Gufi"",
                        o.""State"",
                        lp.""TelemetryMessageId"",
                        lp.""Location"",
                        lp.""AltitudeGps"",
                        lp.""TimeMeasured"",
                        d.""Uid"",
                        d.""CollisionThreshold"",
                        format('SRID=4326;POINT(%s %s %s)'::text, st_x(lp.""Location""::geometry), st_y(lp.""Location""::geometry), lp.""AltitudeGps"") AS ""Location3D""
                        FROM latestpositions lp
                            JOIN ""Operation"" o ON lp.""Gufi"" = o.""Gufi""
                            JOIN ""Drone"" d ON lp.""Registration"" = d.""Uid"";

                        ALTER TABLE public.anra_view_latest_location
                            OWNER TO postgres;

                        GRANT ALL ON TABLE public.anra_view_latest_location TO postgres;
                        GRANT SELECT ON TABLE public.anra_view_latest_location TO PUBLIC;";

            migrationBuilder.Sql(sql.ToString());

            //Triggers
            //public.collision_check_trigger
            sql = @"CREATE or REPLACE FUNCTION public.collision_check_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF 
                        AS $BODY$
	                        DECLARE
		                    Declare detectGeom geometry;
		                    Declare detectRow text;
	                        BEGIN
			                    Select ST_SetSRID(ST_MakePoint( New.""Longitude"", New.""Latitude"", New.""Altitude""),4326) into detectGeom;
			                    Select row_to_json(New) into detectRow;

			                    INSERT INTO public.""Collision""(""DateCreated"", ""DateModified"", ""Detect"", ""Gufi"", ""Position"", ""TimeStamp"", ""Uid"", ""PositionId"", ""DetectId"")
			                    Select now() at time zone 'utc',
						                    now() at time zone 'utc',
						                    row_to_json(NEW),
						                    ""Gufi"",
						                    row_to_json(lc),
						                    now() at time zone 'utc',
						                    ""Uid""::uuid,
						                    lc.""TelemetryMessageId"",
						                    New.""Id""
			                    from anra_view_latest_location lc
			                    where 
			                    lc.""Uid"" <> New.""Uid""
			                    and ST_3DDistance(ST_Transform(ST_GeomFromEWKT(st_asewkt(detectGeom)),4326), ST_Transform(ST_GeomFromEWKT(st_asewkt(ST_SetSRID(ST_MakePoint(ST_X(""Location""::geometry), ST_Y(lc.""Location""::geometry), ""AltitudeGps""),4326))),4326)) * 3.28084 < lc.""CollisionThreshold""
			                    and lc.""TimeMeasured"" > (timezone('UTC'::text, CURRENT_TIMESTAMP) - interval '5' second);

                            RETURN NEW;
	                        END;                            
                        $BODY$;

                        ALTER FUNCTION public.collision_check_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"CREATE or REPLACE FUNCTION public.collision_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF
                        AS $BODY$

	                        BEGIN
		                        PERFORM pg_notify('CollisionNotification', row_to_json(NEW)::text);
	                        RETURN NEW;
	                        END;

                        $BODY$;

                        ALTER FUNCTION public.collision_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"DROP TRIGGER IF EXISTS collision_notify ON public.""Collision"";
                        CREATE TRIGGER collision_notify
                        AFTER INSERT
                        ON public.""Collision""
                        FOR EACH ROW
                        EXECUTE PROCEDURE public.collision_trigger();";

            migrationBuilder.Sql(sql.ToString());

            //

            sql = @"CREATE or REPLACE  FUNCTION public.conformance_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF 
                        AS $BODY$


                                DECLARE
		                            isGeographyConfirming  boolean;
		                            isTimeConfirming  boolean;
		                            isAltitudeConfirming  boolean;
		                            isConfirming  boolean;
		                            nonConfirmanceGap int;
		                            lastTimeStamp timestamp;
		                            lastIsConfirming boolean;  
		                            nonConfirmances int;
		                            operationState text;
                                BEGIN
	
	 	                            CREATE TEMPORARY TABLE temp_conformance
		                            (
			                            ""OperationGeography"" polygon NOT NULL,

                                        ""EffectiveTimeBegin"" timestamp without time zone,

                                        ""EffectiveTimeEnd"" timestamp without time zone,

                                        ""MinAltitude"" numeric,
			                            ""MaxAltitude"" numeric, 
			                            ""OperationId"" integer NOT NULL
		                            )
		                            ON COMMIT DROP;

                                    Insert into temp_conformance
                                    SELECT
                                        opv.""OperationGeography"",
			                            opv.""EffectiveTimeBegin"", 
			                            opv.""EffectiveTimeEnd"", 
			                            (opv.""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
									    (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude"",
			                            opv.""OperationId""

                                    FROM public.""OperationVolume"" AS opv INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 
                                    WHERE ops.""Gufi"" = New.""Gufi"" 
                                    and ST_Intersects(ST_SetSRID(""OperationGeography""::geometry, 4326), ST_SetSRID(NEW.""Location""::geometry, 4326))
                                    and(timezone('UTC'::text, CURRENT_TIMESTAMP) >=  ""EffectiveTimeBegin"" and
                                       timezone('UTC'::text, CURRENT_TIMESTAMP) <=  ""EffectiveTimeEnd"");
 
		                            Select EXISTS(Select 1 from ""temp_conformance"" where ST_Intersects(ST_SetSRID(""OperationGeography""::geometry, 4326), ST_SetSRID(NEW.""Location""::geometry, 4326))) 
		                                into isGeographyConfirming;

                                    Select coalesce((timezone('UTC'::text, CURRENT_TIMESTAMP) >=  Min(""EffectiveTimeBegin"") and timezone('UTC'::text, CURRENT_TIMESTAMP) <=  max(""EffectiveTimeEnd"")), FALSE)  
		                                into isTimeConfirming from ""temp_conformance"";

		                            Select coalesce(((NEW.""AltitudeGps"" #>> '{altitude_value}'):: numeric >=  Min(""MinAltitude"") and (NEW.""AltitudeGps"" #>> '{altitude_value}'):: numeric <=  Max(""MaxAltitude"")), FALSE) 
		                                into isAltitudeConfirming from ""temp_conformance"";


                                    isConfirming := (isAltitudeConfirming AND isGeographyConfirming AND isTimeConfirming);
													
		                            --timegap between now and last message

                                    lastIsConfirming := TRUE;
		                            Select t1.""IsConfirming"" into lastIsConfirming from public.""ConformanceLog"" t1 where ""Gufi"" = New.""Gufi"" order by ""TimeStamp"" desc limit 1;

                                    nonConfirmances := 0;
		                            if not isConfirming then
                                        IF lastIsConfirming THEN

                                            Select coalesce(max(""NonConformances""),0) into nonConfirmances from public.""ConformanceLog"" 
				                            where ""Gufi"" = New.""Gufi""; 
				                            nonConfirmances := nonConfirmances + 1;
			                            END IF;
                                    end if;

		                            nonConfirmanceGap := 0;
		                            if not lastIsConfirming then
                                        Select t1.""DifferenceInSeconds"" into nonConfirmanceGap from public.""ConformanceLog"" t1 where ""Gufi"" = New.""Gufi"" order by t1.""TimeStamp"" desc limit 1;
                                        Select t1.""TimeStamp"" into lastTimeStamp from public.""ConformanceLog"" t1 where ""Gufi"" = New.""Gufi"" order by t1.""TimeStamp"" desc limit 1;
                                        Select nonConfirmanceGap + EXTRACT(EPOCH FROM timezone('UTC'::text, CURRENT_TIMESTAMP)) - EXTRACT(EPOCH FROM lastTimeStamp) into nonConfirmanceGap;
                                    end if;
													
		                            Select ""State"" into operationState from ""Operation"" where ""Gufi"" = New.""Gufi""; 
													
		                            if nonConfirmanceGap >= 30 or nonConfirmances >=3 then
                                        isConfirming := FALSE;
			                            operationState = 'ROGUE';
		                            ELSIF operationState<> 'ROGUE' THEN
                                        IF isConfirming THEN

                                            operationState := 'ACTIVATED';
                                        ELSE
                                            operationState :=	'NONCONFORMING';
                                        END IF;
                                    END IF;


                                    INSERT INTO public.""ConformanceLog""(
		                                ""DateCreated"",
		                                ""DateModified"",
		                                ""Gufi"",
		                                ""IsConfirming"",
		                                ""IsAltitudeConfirming"",
		                                ""IsGeographyConfirming"",
		                                ""IsTimeConfirming"",
		                                ""TelemetryMessageId"",
		                                ""DifferenceInSeconds"",
		                                ""NonConformances"",
		                                ""State"",
		                                ""TimeStamp"")
                                    VALUES(timezone('UTC'::text, CURRENT_TIMESTAMP),

                                        timezone('UTC'::text, CURRENT_TIMESTAMP),
				                        New.""Gufi"",
				                        isConfirming,
				                        isAltitudeConfirming,
				                        isGeographyConfirming,
				                        isTimeConfirming,
				                        New.""TelemetryMessageId"",
				                        nonConfirmanceGap,
				                        nonConfirmances,
				                        operationState,
                                        timezone('UTC'::text, CURRENT_TIMESTAMP));

								    -- Drop Temp Table
                                    DROP TABLE temp_conformance;
                                    RETURN NEW;
                                END;
                        $BODY$;

                        ALTER FUNCTION public.conformance_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql);

            //
            sql = @"DROP TRIGGER IF EXISTS conformance_check ON public.""TelemetryMessage"";
                    CREATE TRIGGER conformance_check AFTER INSERT ON public.""TelemetryMessage"" FOR EACH ROW EXECUTE PROCEDURE public.conformance_trigger();";

            migrationBuilder.Sql(sql);

            //
            sql = @"CREATE OR REPLACE FUNCTION public.conformancelog_trigger()
                            RETURNS trigger
                            LANGUAGE 'plpgsql'
                            COST 100
                            VOLATILE NOT LEAKPROOF 
                        AS $BODY$

                                                BEGIN        
    	                                            PERFORM pg_notify('ConfirmanceNotification', json_build_object('gufi', NEW.""Gufi"",'isconfirming', NEW.""IsConfirming"", 
							                        'nonconformances', NEW.""NonConformances"",'state', NEW.""State"",  'timediff', NEW.""DifferenceInSeconds"", 'timestamp', CURRENT_TIMESTAMP)::text);
                                                RETURN NEW;
                                                END;
                        $BODY$;

                        ALTER FUNCTION public.conformancelog_trigger()
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.""ConformanceLog"";
                    CREATE TRIGGER ""trigger_conformanceLog"" AFTER INSERT ON public.""ConformanceLog"" FOR EACH ROW EXECUTE PROCEDURE public.conformancelog_trigger();";
            migrationBuilder.Sql(sql);

            //
            sql = @"CREATE or REPLACE FUNCTION public.contingency_trigger()
                        RETURNS trigger
                        LANGUAGE 'plpgsql'
                        COST 100
                        VOLATILE NOT LEAKPROOF 
                    AS $BODY$

		                    BEGIN        
			                    if NEW.""ContingencyId"" <=0 then
 				                    Select coalesce(max(""ContingencyId""),0) + 1 into NEW.""ContingencyId"" from ""ContingencyPlan"" where ""OperationId"" = 150;
			                    end if;
			
			                    RETURN NEW;
		                    END;
                        

                    $BODY$;

                    ALTER FUNCTION public.contingency_trigger()
                        OWNER TO postgres;";
            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"DROP TRIGGER IF EXISTS trigger_contingency_id ON public.""ContingencyPlan"";
                        CREATE TRIGGER trigger_contingency_id
                            BEFORE INSERT or UPDATE
                            ON public.""ContingencyPlan""
                            FOR EACH ROW
                            EXECUTE PROCEDURE public.contingency_trigger();";
            migrationBuilder.Sql(sql.ToString());

            //

            sql = @"CREATE or REPLACE FUNCTION public.generate_operation_easyid()
                        RETURNS trigger
                        LANGUAGE 'plpgsql'
                        COST 100
                        VOLATILE NOT LEAKPROOF 
                    AS $BODY$

		            BEGIN        
			            IF (TG_OP = 'INSERT') THEN
	                        Update public.""Operation"" Set ""EasyId"" = concat('OP',New.""FlightNumber"",'-', CAST (New.""OperationId"" AS TEXT))

                            Where ""OperationId"" = New.""OperationId"";
                            RETURN NEW;
                        END IF;
                        RETURN NULL; --result is ignored since this is an AFTER trigger
                    END;
                        

                    $BODY$;

                    ALTER FUNCTION public.generate_operation_easyid()
                        OWNER TO postgres;";
            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"DROP TRIGGER IF EXISTS update_operation_set_easyid ON public.""Operation"";
                        CREATE TRIGGER update_operation_set_easyid
                            AFTER INSERT
                            ON public.""Operation""
                            FOR EACH ROW
                            EXECUTE PROCEDURE public.generate_operation_easyid();";
            migrationBuilder.Sql(sql.ToString());

            //
            sql = @"CREATE or REPLACE FUNCTION public.geographieswithoffset_trigger()
                RETURNS trigger
                LANGUAGE 'plpgsql'
                COST 100
                VOLATILE NOT LEAKPROOF 
                
            AS $BODY$

              DECLARE
                        nonConformanceGeometry polygon;
                        protectedGeometry polygon;

                BEGIN

                    --Create Non-Conformance Geography
                    SELECT ST_Transform(
                                    ST_Buffer(
                                        ST_Transform(ST_SetSRID(New.""OperationGeography""::geometry,4326),3857),
                                        10, 
                                        'endcap=flat join=round'
                                    )
                                ,4326
                            )::polygon into nonConformanceGeometry;

                        --Create Protected Geography
                    SELECT ST_Transform(
                                    ST_Buffer(
                                        ST_Transform(ST_SetSRID(nonConformanceGeometry::geometry,4326),3857),
                                        10, 
                                        'endcap=flat join=round'
                                    )
                                ,4326
                            )::polygon into protectedGeometry;

                        --Update OperationVolume Table

                        New.""NonConformanceGeography"" = nonConformanceGeometry;
                        New.""ProtectedGeography"" = protectedGeometry;

                        RETURN NEW;
                        END;                                             

            $BODY$;

            ALTER FUNCTION public.geographieswithoffset_trigger() OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            sql = @"DROP TRIGGER IF EXISTS trigger_geographieswithoffset ON public.""OperationVolume"";
                            CREATE TRIGGER trigger_geographieswithoffset BEFORE INSERT OR UPDATE ON public.""OperationVolume"" FOR EACH ROW EXECUTE PROCEDURE public.geographieswithoffset_trigger();";

            migrationBuilder.Sql(sql.ToString());

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbygriddata(integer, integer, integer);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getrunningoperations;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithtfr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validateoperationwithuvr(polygon, timestamp without time zone, timestamp without time zone, double precision, double precision);");
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.collision_check_trigger();");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.collision_trigger;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS collision_notify;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformance_trigger();");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.conformancelog_trigger();");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.contingency_trigger;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_contingency_id ON public.\"ContingencyPlan\";");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.generate_operation_easyid;");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS update_operation_set_easyid ON public.\"Operation\";");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_geographieswithoffset ON public.\"OperationVolume\"");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.geographieswithoffset_trigger()");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS conformance_check ON public.\"TelemetryMessage\"");
            migrationBuilder.Sql("DROP TRIGGER IF EXISTS trigger_conformanceLog ON public.\"ConformanceLog\";");
        }
    }
}
