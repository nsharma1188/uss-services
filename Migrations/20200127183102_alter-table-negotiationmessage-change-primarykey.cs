﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertablenegotiationmessagechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NegotiationMessage",
                table: "NegotiationMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LocalNetwork",
                table: "LocalNetwork");

            migrationBuilder.DropColumn(
                name: "NegotiationMessageId",
                table: "NegotiationMessage");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "LocalNetwork");

            migrationBuilder.AddColumn<Guid>(
                name: "NegotiationMessageUid",
                table: "NegotiationMessage",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "LocalNetworkId",
                table: "LocalNetwork",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_NegotiationMessage",
                table: "NegotiationMessage",
                column: "NegotiationMessageUid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LocalNetwork",
                table: "LocalNetwork",
                column: "LocalNetworkId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_NegotiationMessage",
                table: "NegotiationMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LocalNetwork",
                table: "LocalNetwork");

            migrationBuilder.DropColumn(
                name: "NegotiationMessageUid",
                table: "NegotiationMessage");

            migrationBuilder.DropColumn(
                name: "LocalNetworkId",
                table: "LocalNetwork");

            migrationBuilder.AddColumn<int>(
                name: "NegotiationMessageId",
                table: "NegotiationMessage",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "LocalNetwork",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_NegotiationMessage",
                table: "NegotiationMessage",
                column: "NegotiationMessageId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LocalNetwork",
                table: "LocalNetwork",
                column: "Id");
        }
    }
}
