﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctiongetactiveoperationsv1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getactiveoperations(
	                        searchby text,
	                        searchlocation point,
	                        droneuid text,
	                        radius integer)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text, ""WayPointsList"" jsonb) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                        DECLARE sourcegeo geometry;

                        BEGIN
                            -- CASE - WHEN USER WILL SEARCH BY LOCATION
                            IF(searchby = 'LOCATION') THEN
                                SELECT
                                    ST_SetSRID(
                                        searchlocation::geometry,
                                        4326
                                    )
                                INTO sourcegeo;

                                --Prepare Result & Return

                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
									ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
									ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference"",
                                    ops.""WayPointsList""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), sourcegeo, radius, false )                                                        
								    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;

                            -- CASE - WHEN USER WILL SEARCH BY DRONE
                            IF(searchby = 'DRONE') THEN
                                --Prepare Result & Return
                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
								    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
								    ops.""EasyId"",
                                    ops.""Contact"",
									ops.""SlippyTileData"",
									ops.""DiscoveryReference"",
									ops.""WayPointsList""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    INNER JOIN public.""UasRegistration"" AS uas
                                            ON ops.""OperationId"" = uas.""OperationId""
                                    INNER JOIN public.""Drone"" AS drn
                                            ON uas.""RegistrationId"" = drn.""Uid""
                                    WHERE CAST(drn.""Uid"" AS text) LIKE CONCAT('%', droneuid)

                                    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;
                        END;                    
                        $BODY$;

                    ALTER FUNCTION public.__getactiveoperations(text, point, text, integer)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION public.__getactiveoperations(text, point, text, integer);");
        }
    }
}
