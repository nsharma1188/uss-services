﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableurepchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pointout_Urep_UrepId",
                table: "Pointout");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Urep",
                table: "Urep");

            migrationBuilder.DropIndex(
                name: "IX_Pointout_UrepId",
                table: "Pointout");

            migrationBuilder.DropColumn(
                name: "UrepId",
                table: "Urep");

            migrationBuilder.DropColumn(
                name: "UrepId",
                table: "Pointout");

            migrationBuilder.AddColumn<Guid>(
                name: "UrepUid",
                table: "Urep",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UrepUid",
                table: "Pointout",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Urep",
                table: "Urep",
                column: "UrepUid");

            migrationBuilder.CreateIndex(
                name: "IX_Pointout_UrepUid",
                table: "Pointout",
                column: "UrepUid");

            migrationBuilder.AddForeignKey(
                name: "FK_Pointout_Urep_UrepUid",
                table: "Pointout",
                column: "UrepUid",
                principalTable: "Urep",
                principalColumn: "UrepUid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pointout_Urep_UrepUid",
                table: "Pointout");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Urep",
                table: "Urep");

            migrationBuilder.DropIndex(
                name: "IX_Pointout_UrepUid",
                table: "Pointout");

            migrationBuilder.DropColumn(
                name: "UrepUid",
                table: "Urep");

            migrationBuilder.DropColumn(
                name: "UrepUid",
                table: "Pointout");

            migrationBuilder.AddColumn<int>(
                name: "UrepId",
                table: "Urep",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "UrepId",
                table: "Pointout",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Urep",
                table: "Urep",
                column: "UrepId");

            migrationBuilder.CreateIndex(
                name: "IX_Pointout_UrepId",
                table: "Pointout",
                column: "UrepId");

            migrationBuilder.AddForeignKey(
                name: "FK_Pointout_Urep_UrepId",
                table: "Pointout",
                column: "UrepId",
                principalTable: "Urep",
                principalColumn: "UrepId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
