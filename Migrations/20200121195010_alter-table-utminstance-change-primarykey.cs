﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableutminstancechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UtmInstance",
                table: "UtmInstance");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "UtmInstance");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UtmInstance",
                table: "UtmInstance",
                column: "UssInstanceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UtmInstance",
                table: "UtmInstance");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "UtmInstance",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UtmInstance",
                table: "UtmInstance",
                column: "Id");
        }
    }
}
