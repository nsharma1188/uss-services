﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertabledetectsubscriberchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DetectSubscriber",
                table: "DetectSubscriber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Detect",
                table: "Detect");

            migrationBuilder.DropColumn(
                name: "DetectSubscriberId",
                table: "DetectSubscriber");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Detect");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DetectSubscriber",
                table: "DetectSubscriber",
                column: "SubscriptionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Detect",
                table: "Detect",
                column: "DetectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DetectSubscriber",
                table: "DetectSubscriber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Detect",
                table: "Detect");

            migrationBuilder.AddColumn<int>(
                name: "DetectSubscriberId",
                table: "DetectSubscriber",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Detect",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DetectSubscriber",
                table: "DetectSubscriber",
                column: "DetectSubscriberId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Detect",
                table: "Detect",
                column: "Id");
        }
    }
}
