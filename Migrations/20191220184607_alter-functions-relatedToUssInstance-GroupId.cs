﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctionsrelatedToUssInstanceGroupId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getintersectingusss
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingusss(
	                        constraint_geography polygon,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone)
                            RETURNS TABLE(""Id"" integer,""Contact"" jsonb,""CoverageArea"" polygon,""DateCreated"" timestamp without time zone,""DateModified"" timestamp without time zone,""GridCells"" jsonb,""Notes"" text,""OrganizationId"" uuid,""TimeAvailableBegin"" timestamp without time zone,""TimeAvailableEnd"" timestamp without time zone,""TimeLastModified"" timestamp without time zone,""TimeSubmitted"" timestamp without time zone,""UssBaseCallbackUrl"" text,""UssInformationalUrl"" text,""UssInstanceId"" uuid,""UssName"" text,""UssOpenapiUrl"" text,""UssRegistrationUrl"" text, ""GroupId"" text, ""UserId"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                        BEGIN

                            --Final Resultset
                            RETURN QUERY

                            SELECT 
	                            ""UtmInstance"".""Id"", 
                                ""UtmInstance"".""Contact"", 
	                            ""UtmInstance"".""CoverageArea"", 
	                            ""UtmInstance"".""DateCreated"", 
	                            ""UtmInstance"".""DateModified"", 
	                            ""UtmInstance"".""GridCells"", 
	                            ""UtmInstance"".""Notes"", 
	                            ""UtmInstance"".""OrganizationId"", 
	                            ""UtmInstance"".""TimeAvailableBegin"", 
	                            ""UtmInstance"".""TimeAvailableEnd"", 
	                            ""UtmInstance"".""TimeLastModified"", 
	                            ""UtmInstance"".""TimeSubmitted"", 
	                            ""UtmInstance"".""UssBaseCallbackUrl"", 
	                            ""UtmInstance"".""UssInformationalUrl"", 
	                            ""UtmInstance"".""UssInstanceId"", 
	                            ""UtmInstance"".""UssName"", 
	                            ""UtmInstance"".""UssOpenapiUrl"", 
	                            ""UtmInstance"".""UssRegistrationUrl"",
                                ""UtmInstance"".""GroupId"",
                                ""UtmInstance"".""UserId""
                            FROM ""UtmInstance""
                            WHERE
                                tsrange(""UtmInstance"".""TimeAvailableBegin"", ""UtmInstance"".""TimeAvailableEnd"", '[]')
                                            && tsrange(begindt, enddt, '[]')
                                AND
                                ST_3DIntersects(ST_SetSRID(""UtmInstance"".""CoverageArea""::geometry, 4326),
                                                ST_SetSRID(constraint_geography::geometry, 4326));
                        END;                                                                
                    $BODY$;

                    ALTER FUNCTION public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbygufi
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbygufi(
	                        gufi uuid)
                            RETURNS TABLE(""Id"" integer, ""Contact"" jsonb, ""CoverageArea"" polygon, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""GridCells"" jsonb, ""Notes"" text, ""OrganizationId"" uuid, ""TimeAvailableBegin"" timestamp without time zone, ""TimeAvailableEnd"" timestamp without time zone, ""TimeLastModified"" timestamp without time zone, ""TimeSubmitted"" timestamp without time zone, ""UssBaseCallbackUrl"" text, ""UssInformationalUrl"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UssOpenapiUrl"" text, ""UssRegistrationUrl"" text, ""GroupId"" text, ""UserId"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                        DECLARE OperationUssName text;
                            DECLARE OperationGeography polygon;
                            DECLARE OperationId integer;
                            DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;
                            BEGIN
                                CREATE TEMPORARY TABLE temp_opvolume
                                (
                                    ""OperationId"" integer NOT NULL,
                                    ""OperationGeography"" polygon NOT NULL
                                )
                                ON COMMIT DROP;

                                CREATE TEMPORARY TABLE temp_utms
                                (
                                    ""Id"" integer NOT NULL,
			                        ""UssName"" text NOT NULL
		                        )
		                        ON COMMIT DROP;

                                CREATE TEMPORARY TABLE final_result
                                (
                                    ""Id"" integer NOT NULL
		                        )
		                        ON COMMIT DROP;

                                Select ops.""UssName"" INTO OperationUssName From public.""Operation"" ops Where ops.""Gufi"" = gufi;

		                        INSERT INTO temp_opvolume
                                SELECT

                                    opv.""OperationId"",
			                        opv.""OperationGeography""
		                        FROM public.""OperationVolume"" AS opv

                                INNER JOIN public.""Operation"" AS ops

                                    ON opv.""OperationId"" = ops.""OperationId"" 

                                WHERE ops.""Gufi"" = gufi ;

                                OPEN cur_opvs;
                                LOOP
			                        -- fetch row into the film
                                    FETCH cur_opvs INTO OperationId, OperationGeography;

			                        INSERT INTO temp_utms
                                    SELECT utm.""Id"",utm.""UssName""
			                        FROM ""UtmInstance"" utm
                                    WHERE

                                        utm.""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)

                                        AND
                                        utm.""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)

                                        AND
                                        st_intersects(ST_SetSRID(utm.""CoverageArea""::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326));

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;

		                        -- Close the cursor
                                CLOSE cur_opvs;
		
		                        INSERT INTO final_result
                                SELECT DISTINCT utm.""Id""
		                        FROM temp_utms utm;

                                RETURN QUERY

                                SELECT
                                    utm.""Id"",
                                    utm.""Contact"",
                                    utm.""CoverageArea"",
                                    utm.""DateCreated"",
                                    utm.""DateModified"",
                                    utm.""GridCells"",
                                    utm.""Notes"",
                                    utm.""OrganizationId"",
                                    utm.""TimeAvailableBegin"",
                                    utm.""TimeAvailableEnd"",
                                    utm.""TimeLastModified"",
                                    utm.""TimeSubmitted"",
                                    utm.""UssBaseCallbackUrl"",
                                    utm.""UssInformationalUrl"",
                                    utm.""UssInstanceId"",
                                    utm.""UssName"",
                                    utm.""UssOpenapiUrl"",
                                    utm.""UssRegistrationUrl"",
                                    utm.""GroupId"",
                                    utm.""UserId""

                                FROM Public.""UtmInstance"" utm
                                INNER JOIN final_result tmp ON tmp.""Id"" = utm.""Id""

                                WHERE utm.""UssName"" NOT IN (OperationUssName);
                            END;        
                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbygufi(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getnetworkbyinstanceid
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getnetworkbyinstanceid(
	                        ussinstanceid uuid)
                            RETURNS TABLE(""Id"" integer,""Contact"" jsonb,""CoverageArea"" polygon,""DateCreated"" timestamp without time zone,""DateModified"" timestamp without time zone,""GridCells"" jsonb,""Notes"" text,""OrganizationId"" uuid,""TimeAvailableBegin"" timestamp without time zone,""TimeAvailableEnd"" timestamp without time zone,""TimeLastModified"" timestamp without time zone,""TimeSubmitted"" timestamp without time zone,""UssBaseCallbackUrl"" text,""UssInformationalUrl"" text,""UssInstanceId"" uuid,""UssName"" text,""UssOpenapiUrl"" text,""UssRegistrationUrl"" text, ""GroupId"" text, ""UserId"" text) 
                            LANGUAGE 'sql'

                            COST 100
                            VOLATILE 
                            ROWS 1000
                        AS $BODY$

                                SELECT 
	                                ""UtmInstance"".""Id"", 
                                    ""UtmInstance"".""Contact"", 
	                                ""UtmInstance"".""CoverageArea"", 
	                                ""UtmInstance"".""DateCreated"", 
	                                ""UtmInstance"".""DateModified"", 
	                                ""UtmInstance"".""GridCells"", 
	                                ""UtmInstance"".""Notes"", 
	                                ""UtmInstance"".""OrganizationId"", 
	                                ""UtmInstance"".""TimeAvailableBegin"", 
	                                ""UtmInstance"".""TimeAvailableEnd"", 
	                                ""UtmInstance"".""TimeLastModified"", 
	                                ""UtmInstance"".""TimeSubmitted"", 
	                                ""UtmInstance"".""UssBaseCallbackUrl"", 
	                                ""UtmInstance"".""UssInformationalUrl"", 
	                                ""UtmInstance"".""UssInstanceId"", 
	                                ""UtmInstance"".""UssName"", 
	                                ""UtmInstance"".""UssOpenapiUrl"", 
	                                ""UtmInstance"".""UssRegistrationUrl"",
                                    ""UtmInstance"".""GroupId"",
                                    ""UtmInstance"".""UserId""

                                FROM ""UtmInstance""
                                WHERE ""UtmInstance"".""TimeAvailableBegin"" < timezone('UTC'::text, CURRENT_TIMESTAMP)
  		                            AND ""UtmInstance"".""TimeAvailableEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    AND st_intersects(""UtmInstance"".""CoverageArea""::geometry, ( SELECT ""UtmInstance_1"".""CoverageArea""::geometry AS ""CoverageArea""
                                        FROM ""UtmInstance"" ""UtmInstance_1""
                                        WHERE ""UtmInstance_1"".""UssInstanceId"" = ussInstanceId LIMIT 1))
							        AND ""UtmInstance"".""UssInstanceId"" <> ussInstanceId

                            
                        $BODY$;

                        ALTER FUNCTION public.__getnetworkbyinstanceid(uuid)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingusss(polygon, timestamp without time zone, timestamp without time zone);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbygufi(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getnetworkbyinstanceid(uuid);");
        }
    }
}
