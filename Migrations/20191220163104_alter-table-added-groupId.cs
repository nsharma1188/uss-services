﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableaddedgroupId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "UtmInstance",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "Urep",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "Operation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "Notam",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupId",
                table: "Drone",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "UtmInstance");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Urep");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Operation");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Notam");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Drone");
        }
    }
}
