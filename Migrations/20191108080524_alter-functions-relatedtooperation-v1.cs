﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class alterfunctionsrelatedtooperationv1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //public.__getoperationsbygriddata
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbygriddata(integer, integer, integer);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationsbygriddata(
	                        zoom integer,
	                        x integer,
	                        y integer)
                        RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                        LANGUAGE 'plpgsql'

                        COST 100
                        VOLATILE 
                        ROWS 1000
                    AS $BODY$


	                        BEGIN

                                RETURN QUERY
                                SELECT

                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

                                FROM ""Operation"" ops

                                where
                                    (ops.""SlippyTileData"" #>> '{x}'):: numeric = x 
			                        AND
                                    (ops.""SlippyTileData"" #>> '{y}'):: numeric = y 
			                        AND
                                    (ops.""SlippyTileData"" #>> '{zoom}'):: numeric = zoom			
			                        AND ops.""State"" NOT IN('PENDING', 'PROPOSED', 'CANCELLED', 'CLOSED');
                                    END;                    
            

                        
            $BODY$;

            ALTER FUNCTION public.__getoperationsbygriddata(integer, integer, integer)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__checkoperationintersection
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__checkoperationintersection(gufi uuid)
                    RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                 AS $BODY$

 
        	                    Declare OperationGeography polygon;
                                Declare EffectiveTimeBegin timestamp without time zone;
                                Declare EffectiveTimeEnd timestamp without time zone;
                                Declare MinAltitude numeric;
                                Declare MaxAltitude numeric;
                                Declare OperationId integer;
                                DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;
                                BEGIN
                                    CREATE TEMPORARY TABLE temp_opvolume
                                    (
                                        ""OperationGeography"" polygon NOT NULL,
                                        ""EffectiveTimeBegin"" timestamp without time zone,
                                        ""EffectiveTimeEnd"" timestamp without time zone,
                                        ""MinAltitude"" numeric,
                                        ""MaxAltitude"" numeric,
                                        ""OperationId"" integer NOT NULL
                                    )

                                    ON COMMIT DROP;

                                    INSERT INTO temp_opvolume
                                    SELECT
                                        opv.""OperationGeography"",
			                            opv.""EffectiveTimeBegin"", 
			                            opv.""EffectiveTimeEnd"", 
			                            (opv.""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                            (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude"",
			                            opv.""OperationId""

                                    FROM public.""OperationVolume"" AS opv INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" WHERE ops.""Gufi"" = Gufi ;

                                    CREATE TEMPORARY TABLE temp_conflictingOperations
                                    (
			                            ""OperationId"" integer NOT NULL
                                    )

                                    ON COMMIT DROP;

		                            OPEN cur_opvs;
                                    LOOP
			                            -- fetch row into the film
                                        FETCH cur_opvs INTO OperationGeography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude, OperationId;

			                            INSERT INTO temp_conflictingOperations
                                        SELECT opv.""OperationId"" 
			                            FROM ""OperationVolume"" opv
                                            INNER JOIN ""Operation"" o ON opv.""OperationId"" = o.""OperationId"" 
				                            AND o.""State"" NOT IN('CANCELLED', 'CLOSED')

                                            AND o.""IsInternalOperation""

			                            WHERE
                                            tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                            && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')

                                            AND
                                            numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                            && numrange(MinAltitude, MaxAltitude)
                                            AND

                                            ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326));

			                            -- exit when no more row to fetch
                                        EXIT WHEN NOT FOUND;
		                            END LOOP;

		                            -- Close the cursor
                                    CLOSE cur_opvs;

		                            RETURN QUERY

                                    SELECT
                                        ops.""OperationId"",
			                            ops.""ControllerLocation"",
			                            ops.""DateCreated"",
			                            ops.""DateModified"",
			                            ops.""DecisionTime"",
			                            ops.""FaaRule"",
			                            ops.""FlightComments"",
			                            ops.""Gufi"",
			                            ops.""State"",
			                            ops.""SubmitTime"",
			                            ops.""UserId"",
			                            ops.""UssInstanceId"",
			                            ops.""UssName"",
			                            ops.""UpdateTime"",
			                            ops.""IsInternalOperation"",
			                            ops.""OrganizationId"",
			                            ops.""Contact"",
			                            ops.""SlippyTileData"",
									    ops.""WayPointsList"",
									    ops.""CoordinatesList"",
                                        ops.""ATCComments""

                                    FROM ""Operation"" ops
                                    INNER JOIN temp_conflictingOperations co
                                        ON co.""OperationId"" = ops.""OperationId""
                                    where ops.""Gufi"" <> gufi;
                                END;                                                                                
                    
            $BODY$;

            ALTER FUNCTION public.__checkoperationintersection(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getactiveoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getactiveoperations(
	                        searchby text,
	                        searchlocation point,
	                        droneuid text,
	                        radius integer)
                            RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

                        DECLARE sourcegeo geometry;

                        BEGIN
                            -- CASE - WHEN USER WILL SEARCH BY LOCATION
                            IF(searchby = 'LOCATION') THEN
                                SELECT
                                    ST_SetSRID(
                                        searchlocation::geometry,
                                        4326
                                    )
                                INTO sourcegeo;

                                --Prepare Result & Return

                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), sourcegeo, radius, false )                                                        
								    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;

                            -- CASE - WHEN USER WILL SEARCH BY DRONE
                            IF(searchby = 'DRONE') THEN
                                --Prepare Result & Return
                                RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

                                FROM public.""OperationVolume"" AS opv
                                    INNER JOIN public.""Operation"" AS ops
                                        ON opv.""OperationId"" = ops.""OperationId"" AND ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED')
                                    INNER JOIN public.""UasRegistration"" AS uas
                                            ON ops.""OperationId"" = uas.""OperationId""
                                    INNER JOIN public.""Drone"" AS drn
                                            ON uas.""RegistrationId"" = drn.""Uid""
                                    WHERE CAST(drn.""Uid"" AS text) LIKE CONCAT('%', droneuid)

                                    and opv.""EffectiveTimeBegin"" <= timezone('UTC'::text, CURRENT_TIMESTAMP)

                                    and opv.""EffectiveTimeEnd"" > timezone('UTC'::text, CURRENT_TIMESTAMP)
                                    and ops.""IsInternalOperation"" = true;
                            END IF;
                        END;                    
                        $BODY$;

                    ALTER FUNCTION public.__getactiveoperations(text, point, text, integer)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getintersectingoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getintersectingoperations(
	                    messageid uuid)
                    RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


	                        Declare Geography polygon;
                            Declare EffectiveTimeBegin timestamp without time zone;
                            Declare EffectiveTimeEnd timestamp without time zone;
                            Declare MinAltitude numeric;
                            Declare MaxAltitude numeric;
                            DECLARE cur_constraintmsg CURSOR FOR SELECT* FROM temp_constraint_message;
                            BEGIN
                                CREATE TEMPORARY TABLE temp_constraint_message
                                (
                                    ""Geography"" polygon NOT NULL,
                                    ""EffectiveTimeBegin"" timestamp without time zone,
                                    ""EffectiveTimeEnd"" timestamp without time zone,
                                    ""MinAltitude"" numeric,
                                    ""MaxAltitude"" numeric
                                )
                                ON COMMIT DROP;

                                INSERT INTO temp_constraint_message
                                SELECT
                                    ""Geography"",
			                        ""EffectiveTimeBegin"", 
			                        ""EffectiveTimeEnd"", 
			                        (""MinAltitude"" #>> '{altitude_value}'):: numeric AS ""MinAltitude"",
			                        (""MaxAltitude"" #>> '{altitude_value}'):: numeric AS ""MaxAltitude""
                                FROM public.""ConstraintMessage"" WHERE ""MessageId"" = messageid ;

                                CREATE TEMPORARY TABLE temp_intersectingOperations
                                (
			                        ""OperationId"" integer NOT NULL
                                )
                                ON COMMIT DROP;

		                        OPEN cur_constraintmsg;
                                LOOP
			                        -- fetch row into the film
                                    FETCH cur_constraintmsg INTO Geography,  EffectiveTimeBegin, EffectiveTimeEnd, MinAltitude, MaxAltitude;

			                        Insert into temp_intersectingOperations
                                    SELECT opv.""OperationId"" 
			                        from ""OperationVolume"" opv join ""Operation"" o on opv.""OperationId"" = o.""OperationId"" 
													                        AND o.""State"" NOT IN('CANCELLED', 'CLOSED')

                                                                            AND o.""IsInternalOperation""

			                        WHERE
                                        tsrange(opv.""EffectiveTimeBegin"", opv.""EffectiveTimeEnd"", '[]')
					                        && tsrange(EffectiveTimeBegin, EffectiveTimeEnd, '[]')


                                        AND
                                        numrange((opv.""MinAltitude"" #>> '{altitude_value}'):: numeric, (opv.""MaxAltitude"" #>> '{altitude_value}'):: numeric) 
					                        && numrange(MinAltitude, MaxAltitude)

                                        AND

                                        ST_3DIntersects(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), ST_SetSRID(Geography::geometry, 4326));

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;

		                        -- Close the cursor
                                CLOSE cur_constraintmsg;

		                        RETURN QUERY

                                SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

                                FROM ""Operation"" ops
                                INNER JOIN temp_intersectingOperations co
                                    ON co.""OperationId"" = ops.""OperationId"";
                                END;                                        

                        
            $BODY$;

            ALTER FUNCTION public.__getintersectingoperations(uuid)
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getrunningoperations
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getrunningoperations();");

            sql = @"CREATE OR REPLACE FUNCTION public.__getrunningoperations()
                    RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                    LANGUAGE 'plpgsql'

                    COST 100
                    VOLATILE 
                    ROWS 1000
                AS $BODY$


                        BEGIN
                            --Prepare Result &Return
                            RETURN QUERY
                            SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""


                            FROM public.""Operation"" AS ops
                            WHERE ops.""State"" NOT IN ('PROPOSED','ACCEPTED','CANCELLED','CLOSED');

                            END;                                                          
                    
            $BODY$;

            ALTER FUNCTION public.__getrunningoperations()
            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__validatedroneassignation
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");

            sql = @"CREATE OR REPLACE FUNCTION public.__validatedroneassignation(
	                        droneid uuid,
	                        begindt timestamp without time zone,
	                        enddt timestamp without time zone)
                            RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                        BEGIN

                                CREATE TEMPORARY TABLE temp_opvolume
                                (
                                    ""EffectiveTimeBegin"" timestamp without time zone,
                                    ""EffectiveTimeEnd"" timestamp without time zone,
                                    ""OperationId"" integer NOT NULL
                                )


                                ON COMMIT DROP;

                                    CREATE TEMPORARY TABLE temp_conflictingOperations
                                    (

                                    ""OperationId"" integer NOT NULL
		                        )

		                        ON COMMIT DROP;

                                    INSERT INTO temp_opvolume
                                    SELECT


                                opv.""EffectiveTimeBegin"", 
		                        opv.""EffectiveTimeEnd"", 
		                        opv.""OperationId""


                                FROM public.""OperationVolume"" AS opv


                                INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId""


                                INNER JOIN public.""UasRegistration"" o ON ops.""OperationId"" = o.""OperationId""
		                        WHERE ops.""IsInternalOperation"" = true 
			                        AND ops.""State"" IN ('ACTIVATED','ACCEPTED','NONCONFORMING','ROGUE')

                                    AND o.""RegistrationId"" = droneid;

                                INSERT INTO temp_conflictingOperations
                                SELECT op.""OperationId"" 
		                        FROM temp_opvolume op
                                WHERE

                                    tsrange(op.""EffectiveTimeBegin"", op.""EffectiveTimeEnd"", '[]')
				                        && tsrange(begindt, enddt, '[]');

                                RETURN QUERY

                                SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""


                                FROM ""Operation"" ops
                                INNER JOIN temp_conflictingOperations co
                                    ON co.""OperationId"" = ops.""OperationId"";
                                END;                                                

                        $BODY$;

                        ALTER FUNCTION public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone)
                            OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getoperationsbycriteria
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getoperationsbycriteria(
	                        registration_ids text,
	                        submit_time timestamp without time zone,
	                        operation_states text,
	                        distance integer,
	                        reference_point text)
                            RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

						    DECLARE registrationIds uuid[];
                            DECLARE opstates text[];
                            DECLARE refpoint numeric[];
                            DECLARE sourcegeo geometry;
                            DECLARE radius numeric;
                            DECLARE OperationGeography polygon;
                            DECLARE OperationId integer;
                            DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;

                        BEGIN

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_ops
                            (
                                ""OperationId"" integer NOT NULL
							)
							ON COMMIT DROP;

                            CREATE TEMPORARY TABLE final_result
                            (
                                ""OperationId"" integer NOT NULL
							)
							ON COMMIT DROP;

                            --Check for Registration Ids

                            registrationIds := string_to_array(registration_ids, ',')::uuid[];

                            IF(array_length(registrationIds, 1) > 0) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                INNER JOIN public.""UasRegistration"" dr ON dr.""OperationId"" = ops.""OperationId""
								Where
                                    ops.""IsInternalOperation"" 
									AND dr.""RegistrationId"" = any(registrationIds)
                                    AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')

                                    OR(ops.""State"" = 'CLOSED' AND ops.""DecisionTime"" BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW())

                                    ORDER BY ops.""SubmitTime"" DESC;
							END IF;
						
							--Check for Operation Submit Time
                            IF(submit_time IS NOT NULL) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                Where
                                    ops.""IsInternalOperation"" 
									AND ops.""SubmitTime"" > submit_time
                                    ORDER BY ops.""SubmitTime"" DESC;
                            END IF;
							
							-- Check for Operation States

                            opstates := string_to_array(operation_states, ',')::text[];
							
							IF(array_length(opstates,1) > 0) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                Where
                                    ops.""IsInternalOperation"" 
									AND ops.""State"" = any(opstates)
                                    ORDER BY ops.""SubmitTime"" DESC;
                            END IF;
							
							-- Check for Reference Point

                            refpoint := string_to_array(reference_point, ',')::numeric[];
							
							IF(array_length(refpoint,1) >= 2) THEN								
								
								--Convert distance into meter as per Postgis specs
                                radius := distance* 0.3048 ;
								
								--Prepare Circle geometry based on Ref Point and Distance
                                sourcegeo := ST_Buffer(ST_SetSRID(ST_Point(refpoint[1], refpoint[2]),4326)::geography,radius);
								
								INSERT INTO temp_opvolume
                                SELECT

                                    opv.""OperationGeography"",
									opv.""OperationId""
								FROM public.""OperationVolume"" AS opv

                                INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                WHERE ops.""IsInternalOperation"" 

                                ORDER BY ops.""SubmitTime"" DESC;

                                OPEN cur_opvs;
                                LOOP
			                        -- fetch row
                                    FETCH cur_opvs INTO OperationGeography, OperationId;

                                    IF(ST_Intersects(ST_SetSRID(sourcegeo::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326))) THEN
                                        INSERT INTO temp_ops
                                        SELECT OperationId;
                                    END IF;

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;
                            END If;
							
							--Select Distinct Records
                            INSERT INTO final_result
                            SELECT DISTINCT op.""OperationId"" FROM temp_ops AS op;
							
							-- Return Final Output
                            RETURN QUERY
                            SELECT
                                ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

							FROM public.""Operation"" AS ops
                            INNER JOIN final_result op ON op.""OperationId"" = ops.""OperationId"";
                        END;                    

                $BODY$;

                ALTER FUNCTION public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());

            //public.__getoperationswithinuss
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");

            sql = @"CREATE OR REPLACE FUNCTION public.__getoperationswithinuss(
	                        locationgeo point,
	                        ussinstanceid uuid,
	                        radius integer,
	                        reclimit integer,
	                        orgid uuid)
                            RETURNS TABLE(""OperationId"" integer, ""ControllerLocation"" point, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb, ""ATCComments"" text) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                    DECLARE pointgeo geometry;
                        DECLARE ussgeo polygon;

                        BEGIN
                            CREATE TEMPORARY TABLE final_result
                            (
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_operations
                            (
                                ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
			                    ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            --input radius value will be in feet
                            --We will convert into meter for supporting postgis function
                            --If radius is 0 feet then use default radius as 5000 meter / 5 km
                            --else will convert input radius feet value into meter

                            IF(radius = 0) THEN
                                radius := 5000;
                            ELSE
                                radius := radius * 0.3048;
                            END IF;

                            IF(ussinstanceid IS NOT NULL) THEN

                                -- Fetch Uss Coverage Area
                                Select utm.""CoverageArea"" INTO ussgeo From public.""UtmInstance"" utm Where utm.""UssInstanceId"" = ussinstanceid;

								--First Filter Operations based on reclimit & organization
                                IF(reclimit > 0) THEN
									--Fetch Internal Operation
                                    INSERT INTO temp_operations

                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
									AND ops.""UssInstanceId"" = ussinstanceid
                                    ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
									ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;
								ELSE
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 
									AND ops.""UssInstanceId"" = ussinstanceid
                                    ORDER BY ops.""OperationId"" DESC;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    AND ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326),ST_SetSRID(ussgeo::geometry, 4326))
									ORDER BY ops.""OperationId"" DESC;
								END IF;

                            ELSE
                                IF(reclimit > 0) THEN
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                    ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 									
									ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    ORDER BY ops.""OperationId"" DESC
                                    LIMIT reclimit;
								ELSE
									--Fetch Internal Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE(orgid IS NULL OR ops.""OrganizationId"" = orgid)
                                    AND ops.""IsInternalOperation"" = true 								
									ORDER BY ops.""OperationId"" DESC;

									--Fetch External Operation
                                    INSERT INTO temp_operations
                                    SELECT DISTINCT
                                        ops.""OperationId""
									FROM public.""Operation"" AS ops
                                    INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
									WHERE ops.""IsInternalOperation"" = false 
									AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')
                                    ORDER BY ops.""OperationId"" DESC;
								END IF;
                            END IF;

                            IF(locationgeo IS NOT NULL) THEN
                                SELECT ST_SetSRID(locationgeo::geometry, 4326) INTO pointgeo;

                                INSERT INTO temp_opvolume
                                SELECT
                                    opv.""OperationGeography"",
				                    opv.""OperationId""
			                    FROM public.""OperationVolume"" AS opv
                                    INNER JOIN temp_operations AS ops ON opv.""OperationId"" = ops.""OperationId""
			                    WHERE ST_DWithin(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), pointgeo, radius, false )
			                    ORDER BY ops.""OperationId"" DESC;
		                    ELSE
			                    --Now Fetch Operation Volumes for Filtered Operations
                                INSERT INTO temp_opvolume
                                SELECT
                                    opv.""OperationGeography"",
				                    opv.""OperationId""
			                    FROM public.""OperationVolume"" AS opv
                                    INNER JOIN temp_operations AS ops ON opv.""OperationId"" = ops.""OperationId""
			                    ORDER BY ops.""OperationId"" DESC;
		                    END IF;

                            --First Insert Internal Operations																					  
                            INSERT INTO final_result
                            SELECT DISTINCT ops.""OperationId"" FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv ON opv.""OperationId"" = ops.""OperationId""
                            WHERE ops.""IsInternalOperation"" = true;

                            --Then Insert External Operations
                            --We will consider only last 12 hour external operations
                            INSERT INTO final_result
                            SELECT DISTINCT ops.""OperationId"" FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv ON opv.""OperationId"" = ops.""OperationId"" 
                            WHERE ops.""IsInternalOperation"" = false
                            AND (DATE_PART('day', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)* 24 +
								DATE_PART('hour', timezone('utc', now())::timestamp - ops.""DateCreated""::timestamp)) <= 12;

                            RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
			                        ops.""ControllerLocation"",
			                        ops.""DateCreated"",
			                        ops.""DateModified"",
			                        ops.""DecisionTime"",
			                        ops.""FaaRule"",
			                        ops.""FlightComments"",
			                        ops.""Gufi"",
			                        ops.""State"",
			                        ops.""SubmitTime"",
			                        ops.""UserId"",
			                        ops.""UssInstanceId"",
			                        ops.""UssName"",
			                        ops.""UpdateTime"",
			                        ops.""IsInternalOperation"",
			                        ops.""OrganizationId"",
			                        ops.""Contact"",
			                        ops.""SlippyTileData"",
									ops.""WayPointsList"",
									ops.""CoordinatesList"",
                                    ops.""ATCComments""

                                FROM ""Operation"" ops
                                INNER JOIN final_result co
                                    ON co.""OperationId"" = ops.""OperationId""
                                    Order By ops.""OperationId"" DESC;
                            END;                                                                                                                            
                    $BODY$;

                    ALTER FUNCTION public.__getoperationswithinuss(point, uuid, integer, integer, uuid)
                        OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbygriddata(integer, integer, integer);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__checkoperationintersection(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getactiveoperations(text, point, text, integer);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getintersectingoperations(uuid);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getrunningoperations;");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__validatedroneassignation(uuid, timestamp without time zone, timestamp without time zone);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinuss(point, uuid, integer, integer, uuid);");
        }
    }
}
