﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class createfunctiongetoperationsbycriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationsbycriteria(
	                        registration_ids text,
	                        submit_time timestamp without time zone,
	                        operation_states text,
	                        distance integer,
	                        reference_point text)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text, ""WayPointsList"" jsonb) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

						    DECLARE registrationIds uuid[];
                            DECLARE opstates text[];
                            DECLARE refpoint numeric[];
                            DECLARE sourcegeo geometry;
                            DECLARE radius numeric;
                            DECLARE OperationGeography polygon;
                            DECLARE OperationId integer;
                            DECLARE cur_opvs CURSOR FOR SELECT* FROM temp_opvolume;

                        BEGIN

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_ops
                            (
                                ""OperationId"" integer NOT NULL
							)
							ON COMMIT DROP;

                            CREATE TEMPORARY TABLE final_result
                            (
                                ""OperationId"" integer NOT NULL
							)
							ON COMMIT DROP;

                            --Check for Registration Ids

                            registrationIds := string_to_array(registration_ids, ',')::uuid[];

                            IF(array_length(registrationIds, 1) > 0) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                INNER JOIN public.""UasRegistration"" dr ON dr.""OperationId"" = ops.""OperationId""
								Where
                                    ops.""IsInternalOperation"" 
									AND dr.""RegistrationId"" = any(registrationIds)
                                    AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')

                                    OR(ops.""State"" = 'CLOSED' AND ops.""DecisionTime"" BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW())

                                    ORDER BY ops.""SubmitTime"" DESC;
							END IF;
						
							--Check for Operation Submit Time
                            IF(submit_time IS NOT NULL) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                Where
                                    ops.""IsInternalOperation"" 
									AND ops.""SubmitTime"" > submit_time
                                    ORDER BY ops.""SubmitTime"" DESC;
                            END IF;
							
							-- Check for Operation States

                            opstates := string_to_array(operation_states, ',')::text[];
							
							IF(array_length(opstates,1) > 0) THEN

                                INSERT INTO temp_ops
                                SELECT
                                    ops.""OperationId""
                                FROM public.""Operation"" AS ops

                                Where
                                    ops.""IsInternalOperation"" 
									AND ops.""State"" = any(opstates)
                                    ORDER BY ops.""SubmitTime"" DESC;
                            END IF;
							
							-- Check for Reference Point

                            refpoint := string_to_array(reference_point, ',')::numeric[];
							
							IF(array_length(refpoint,1) >= 2) THEN								
								
								--Convert distance into meter as per Postgis specs
                                radius := distance* 0.3048 ;
								
								--Prepare Circle geometry based on Ref Point and Distance
                                sourcegeo := ST_Buffer(ST_SetSRID(ST_Point(refpoint[1], refpoint[2]),4326)::geography,radius);
								
								INSERT INTO temp_opvolume
                                SELECT

                                    opv.""OperationGeography"",
									opv.""OperationId""
								FROM public.""OperationVolume"" AS opv

                                INNER JOIN public.""Operation"" AS ops ON opv.""OperationId"" = ops.""OperationId"" 

                                WHERE ops.""IsInternalOperation"" 

                                ORDER BY ops.""SubmitTime"" DESC;

                                OPEN cur_opvs;
                                LOOP
			                        -- fetch row
                                    FETCH cur_opvs INTO OperationGeography, OperationId;

                                    IF(ST_Intersects(ST_SetSRID(sourcegeo::geometry, 4326), ST_SetSRID(OperationGeography::geometry, 4326))) THEN
                                        INSERT INTO temp_ops
                                        SELECT OperationId;
                                    END IF;

			                        -- exit when no more row to fetch
                                    EXIT WHEN NOT FOUND;
		                        END LOOP;
                            END If;
							
							--Select Distinct Records
                            INSERT INTO final_result
                            SELECT DISTINCT op.""OperationId"" FROM temp_ops AS op;
							
							-- Return Final Output
                            RETURN QUERY
                            SELECT
                                ops.""OperationId"",
								ops.""AircraftComments"",
								ops.""AirspaceAuthorization"",
								ops.""ControllerLocation"",
								ops.""CreatedBy"",
								ops.""DateCreated"",
								ops.""DateModified"",
								ops.""DecisionTime"",
								ops.""FaaRule"",
								ops.""FlightComments"",
								ops.""FlightNumber"",
								ops.""GcsLocation"",
								ops.""Gufi"",
								ops.""State"",
								ops.""SubmitTime"",
								ops.""UserId"",
								ops.""UssInstanceId"",
								ops.""VolumesDescription"",
								ops.""UssName"",
								ops.""UpdateTime"",
								ops.""IsInternalOperation"",
								ops.""OrganizationId"",
								ops.""FlightSpeed"",
								ops.""FlightStartTime"",
								ops.""EasyId"",
								ops.""Contact"",
								ops.""SlippyTileData"",
								ops.""DiscoveryReference"",
								ops.""WayPointsList""
							FROM public.""Operation"" AS ops
                            INNER JOIN final_result op ON op.""OperationId"" = ops.""OperationId"";
                        END;                    

                $BODY$;

                ALTER FUNCTION public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION public.__getoperationsbycriteria(text, timestamp without time zone, text, integer, text);");
        }
    }
}
