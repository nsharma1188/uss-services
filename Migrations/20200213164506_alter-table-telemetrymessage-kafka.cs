﻿using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertabletelemetrymessagekafka : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            migrationBuilder.DropForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage");

            migrationBuilder.DropIndex(
                name: "IX_ConformanceLog_TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "TelemetryMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageUid",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<string>(
                name: "TelemetryMessageId",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UssInstanceId",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "TimeSent",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "TimeMeasured",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "Registration",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "Location",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(NpgsqlPoint));

            migrationBuilder.AlterColumn<string>(
                name: "Gufi",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "EnroutePositionsId",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "AltitudeGps",
                table: "TelemetryMessage",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "jsonb",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TelemetryMessageId",
                table: "TelemetryMessage",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TelemetryMessageId",
                table: "ConformanceLog",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_TelemetryMessageId",
                table: "UtmMessage",
                column: "TelemetryMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ConformanceLog_TelemetryMessageId",
                table: "ConformanceLog",
                column: "TelemetryMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageId",
                table: "ConformanceLog",
                column: "TelemetryMessageId",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                table: "UtmMessage",
                column: "TelemetryMessageId",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS public.anra_view_latest_location;");

            migrationBuilder.DropForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.DropForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_UtmMessage_TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage");

            migrationBuilder.DropIndex(
                name: "IX_ConformanceLog_TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "TelemetryMessage");

            migrationBuilder.DropColumn(
                name: "TelemetryMessageId",
                table: "ConformanceLog");

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "UtmMessage",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "UssInstanceId",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeSent",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "TimeMeasured",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "Registration",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<NpgsqlPoint>(
                name: "Location",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "Gufi",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "EnroutePositionsId",
                table: "TelemetryMessage",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AltitudeGps",
                table: "TelemetryMessage",
                type: "jsonb",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "TelemetryMessage",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TelemetryMessageUid",
                table: "ConformanceLog",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_TelemetryMessage",
                table: "TelemetryMessage",
                column: "TelemetryMessageUid");

            migrationBuilder.CreateIndex(
                name: "IX_UtmMessage_TelemetryMessageUid",
                table: "UtmMessage",
                column: "TelemetryMessageUid");

            migrationBuilder.CreateIndex(
                name: "IX_ConformanceLog_TelemetryMessageUid",
                table: "ConformanceLog",
                column: "TelemetryMessageUid");

            migrationBuilder.AddForeignKey(
                name: "FK_ConformanceLog_TelemetryMessage_TelemetryMessageUid",
                table: "ConformanceLog",
                column: "TelemetryMessageUid",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageUid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtmMessage_TelemetryMessage_TelemetryMessageUid",
                table: "UtmMessage",
                column: "TelemetryMessageUid",
                principalTable: "TelemetryMessage",
                principalColumn: "TelemetryMessageUid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
