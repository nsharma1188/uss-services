﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableutmmessagechangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_UtmMessage_UtmMessageId",
                table: "ContingencyPlan");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UtmMessage",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_ContingencyPlan_UtmMessageId",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "UtmMessageId",
                table: "UtmMessage");

            migrationBuilder.DropColumn(
                name: "UtmMessageId",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<Guid>(
                name: "MessageId",
                table: "ContingencyPlan",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UtmMessage",
                table: "UtmMessage",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_MessageId",
                table: "ContingencyPlan",
                column: "MessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContingencyPlan_UtmMessage_MessageId",
                table: "ContingencyPlan",
                column: "MessageId",
                principalTable: "UtmMessage",
                principalColumn: "MessageId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContingencyPlan_UtmMessage_MessageId",
                table: "ContingencyPlan");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UtmMessage",
                table: "UtmMessage");

            migrationBuilder.DropIndex(
                name: "IX_ContingencyPlan_MessageId",
                table: "ContingencyPlan");

            migrationBuilder.DropColumn(
                name: "MessageId",
                table: "ContingencyPlan");

            migrationBuilder.AddColumn<int>(
                name: "UtmMessageId",
                table: "UtmMessage",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddColumn<int>(
                name: "UtmMessageId",
                table: "ContingencyPlan",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UtmMessage",
                table: "UtmMessage",
                column: "UtmMessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ContingencyPlan_UtmMessageId",
                table: "ContingencyPlan",
                column: "UtmMessageId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContingencyPlan_UtmMessage_UtmMessageId",
                table: "ContingencyPlan",
                column: "UtmMessageId",
                principalTable: "UtmMessage",
                principalColumn: "UtmMessageId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
