﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class createfunctiongetoperationswitihinarea : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS public.__getoperationswithinarea(polygon);");

            var sql = @"CREATE OR REPLACE FUNCTION public.__getoperationswithinarea(
	                        viewarea polygon)
                            RETURNS TABLE(""OperationId"" integer, ""AircraftComments"" text, ""AirspaceAuthorization"" uuid, ""ControllerLocation"" point, ""CreatedBy"" text, ""DateCreated"" timestamp without time zone, ""DateModified"" timestamp without time zone, ""DecisionTime"" timestamp without time zone, ""FaaRule"" text, ""FlightComments"" text, ""FlightNumber"" text, ""GcsLocation"" point, ""Gufi"" uuid, ""State"" text, ""SubmitTime"" timestamp without time zone, ""UserId"" text, ""UssInstanceId"" uuid, ""VolumesDescription"" text, ""UssName"" text, ""UpdateTime"" timestamp without time zone, ""IsInternalOperation"" boolean, ""OrganizationId"" uuid, ""FlightSpeed"" integer, ""FlightStartTime"" timestamp without time zone, ""EasyId"" text, ""Contact"" jsonb, ""SlippyTileData"" jsonb, ""DiscoveryReference"" text, ""WayPointsList"" jsonb, ""CoordinatesList"" jsonb) 
                            LANGUAGE 'plpgsql'

                            COST 100
                            VOLATILE
                            ROWS 1000
                        AS $BODY$

	                    DECLARE polygongeo geometry;

                        BEGIN
                            CREATE TEMPORARY TABLE final_result
                            (
                                ""OperationId"" integer NOT NULL
                            )
                            ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_operations
                            (
                                ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            CREATE TEMPORARY TABLE temp_opvolume
                            (
                                ""OperationGeography"" polygon NOT NULL,
			                    ""OperationId"" integer NOT NULL
		                    )
		                    ON COMMIT DROP;

                            SELECT ST_SetSRID(viewarea::geometry, 4326) INTO polygongeo;

                            --Fetch Internal Operation

                            INSERT INTO temp_operations
                            SELECT DISTINCT

                                ops.""OperationId""

                            FROM public.""Operation"" AS ops

                            INNER JOIN public.""OperationVolume"" opv ON opv.""OperationId"" = ops.""OperationId""
							WHERE ops.""IsInternalOperation"" = true
							AND ops.""State"" NOT IN ('PROPOSED','CANCELLED','CLOSED')

                            ORDER BY ops.""OperationId"" DESC;

							INSERT INTO temp_opvolume
                            SELECT

                                opv.""OperationGeography"",
								opv.""OperationId""
							FROM public.""OperationVolume"" AS opv

                                INNER JOIN temp_operations AS ops ON opv.""OperationId"" = ops.""OperationId""
							WHERE ST_Within(ST_SetSRID(opv.""OperationGeography""::geometry, 4326), polygongeo)
							ORDER BY ops.""OperationId"" DESC;

                            --First Insert Internal Operations
                            INSERT INTO final_result
                            SELECT DISTINCT ops.""OperationId"" FROM public.""Operation"" AS ops
                            INNER JOIN temp_opvolume opv ON opv.""OperationId"" = ops.""OperationId"";

                            RETURN QUERY
                                SELECT
                                    ops.""OperationId"",
                                    ops.""AircraftComments"",
                                    ops.""AirspaceAuthorization"",
                                    ops.""ControllerLocation"",
                                    ops.""CreatedBy"",
                                    ops.""DateCreated"",
                                    ops.""DateModified"",
                                    ops.""DecisionTime"",
                                    ops.""FaaRule"",
                                    ops.""FlightComments"",
                                    ops.""FlightNumber"",
                                    ops.""GcsLocation"",
                                    ops.""Gufi"",
                                    ops.""State"",
                                    ops.""SubmitTime"",
                                    ops.""UserId"",
                                    ops.""UssInstanceId"",
                                    ops.""VolumesDescription"",
                                    ops.""UssName"",
                                    ops.""UpdateTime"",
                                    ops.""IsInternalOperation"",
                                    ops.""OrganizationId"",
                                    ops.""FlightSpeed"",
                                    ops.""FlightStartTime"",
                                    ops.""EasyId"",
                                    ops.""Contact"",
                                    ops.""SlippyTileData"",
                                    ops.""DiscoveryReference"",
                                    ops.""WayPointsList"",
                                    ops.""CoordinatesList""

                                FROM ""Operation"" ops
                                INNER JOIN final_result co
                                    ON co.""OperationId"" = ops.""OperationId""
                                    Order By ops.""OperationId"" DESC;
                        END;                                                                                                                            
                    

                $BODY$;

                ALTER FUNCTION public.__getoperationswithinarea(polygon)
                    OWNER TO postgres;";

            migrationBuilder.Sql(sql.ToString());
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION public.__getoperationswithinarea(polygon);");
        }
    }
}
