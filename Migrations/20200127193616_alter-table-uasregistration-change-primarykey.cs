﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Migrations
{
    public partial class altertableuasregistrationchangeprimarykey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UasRegistration",
                table: "UasRegistration");

            migrationBuilder.DropColumn(
                name: "UasRegistrationId",
                table: "UasRegistration");

            migrationBuilder.AddColumn<Guid>(
                name: "UasRegistrationUid",
                table: "UasRegistration",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_UasRegistration",
                table: "UasRegistration",
                column: "UasRegistrationUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UasRegistration",
                table: "UasRegistration");

            migrationBuilder.DropColumn(
                name: "UasRegistrationUid",
                table: "UasRegistration");

            migrationBuilder.AddColumn<int>(
                name: "UasRegistrationId",
                table: "UasRegistration",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UasRegistration",
                table: "UasRegistration",
                column: "UasRegistrationId");
        }
    }
}
