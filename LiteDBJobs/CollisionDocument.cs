﻿using AnraUssServices.Common;
using AnraUssServices.ViewModel.Collisions;
using AnraUssServices.ViewModel.TelemetryMessages;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.LiteDBJobs
{
    public class CollisionDocument
    {        
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public CollisionDocument(ILogger<string> logger, AnraConfiguration configuration)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.DetectionLiteDbPath, $"collision_{DateTime.UtcNow.ToString("yyyy_MM_dd")}.db");

            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
        }

        public void AddOrUpdate(CollisionViewModel item)
        {
            var collisionCollection = database.GetCollection<CollisionViewModel>("Collisions");
            collisionCollection.Insert(item);
            IndexIssue(collisionCollection);
        }

        public List<CollisionViewModel> GetCollisionsList(string gufi)
        {
            var collisionCollection = database.GetCollection<CollisionViewModel>("Collisions");

            var collisons = collisionCollection.FindAll().Where(x => x.Gufi.Equals(Guid.Parse(gufi)))
                            .OrderByDescending(p => p.TimeStamp)
                            .ToList();

            return collisons;
        }

        private void IndexIssue(LiteCollection<CollisionViewModel> collisionCollection)
        {
            // Index on EnroutePositionsId            
            collisionCollection.EnsureIndex(x => x.CollisionId);
        }
    }
}
