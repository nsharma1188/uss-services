﻿using AnraUssServices.Common;
using AnraUssServices.Data;
using AnraUssServices.ViewModel.UtmInstances;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AnraUssServices.LiteDBJobs
{
    public class UssInstanceDocument
    {
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public UssInstanceDocument(ILogger<string> logger, AnraConfiguration configuration)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.DetectionLiteDbPath, $"ussinstance_{DateTime.UtcNow.ToString("yyyy_MM_dd")}.db");
            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
        }

        public void AddOrUpdate(UtmInstanceItem uss)
        {
            var ussCollection = database.GetCollection<UtmInstanceItem>("UssInstances");

            var isExistingUss = ussCollection.Exists(x => x.UssInstanceId == uss.UssInstanceId); 

            if (isExistingUss)
            {
                ussCollection.Update(uss);
            }
            else
            {
                ussCollection.Insert(uss);
                IndexIssue(ussCollection);
            }
        }

        public void AddMany(List<UtmInstanceItem> usss)
        {
            if (usss != null && usss.Any())
            {
                usss.ForEach(AddOrUpdate);
            }
        }

        public void DeleteMany(List<Guid> ussInstanceIds)
        {
            var ussCollection = database.GetCollection<UtmInstanceItem>("UssInstances");

            foreach (var ussInstanceId in ussInstanceIds)
            {
                ussCollection.Delete(ussInstanceId);
            }
        }


        private void IndexIssue(LiteCollection<UtmInstanceItem> ussCollection)
        {
            // Index on UssInstanceId            
            ussCollection.EnsureIndex(x => x.UssInstanceId);
        }
    }
}
