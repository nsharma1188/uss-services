﻿using AnraUssServices.Common;
using AnraUssServices.ViewModel.Drones;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AnraUssServices.LiteDBJobs
{
    public class DroneDocument
    {
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public DroneDocument(ILogger<string> logger, AnraConfiguration configuration)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.DetectionLiteDbPath, $"drone_{DateTime.UtcNow.ToString("yyyy_MM_dd")}.db");
            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
        }

        public void AddOrUpdate(DroneViewModel drone)
        {
            var droneCollection = database.GetCollection<DroneViewModel>("Drones");

            var isExistingDrone = droneCollection.Exists(x => x.Uid == drone.Uid); 

            if (isExistingDrone)
            {
                droneCollection.Update(drone);
            }
            else
            {                
                droneCollection.Insert(drone);
                IndexIssue(droneCollection);
            }
        }

        public void AddMany(List<DroneViewModel> drones)
        {
            if (drones != null && drones.Any())
            {
                drones.ForEach(AddOrUpdate);
            }
        }

        public List<DroneViewModel> GetDrones(string[] uids)
        {
            var droneCollection = database.GetCollection<DroneViewModel>("Drones");

            var drones = droneCollection.FindAll().Where(x => uids.Any(z => x.Uid.Contains(z))).ToList();

            return drones;
        }

        private void IndexIssue(LiteCollection<DroneViewModel> droneCollection)
        {
            // Index on Uid            
            droneCollection.EnsureIndex(x => x.Uid);
        }
    }
}
