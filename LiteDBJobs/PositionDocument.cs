﻿using AnraUssServices.Common;
using AnraUssServices.ViewModel.TelemetryMessages;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.LiteDBJobs
{
    public class PositionDocument
    {        
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public PositionDocument(ILogger<string> logger, AnraConfiguration configuration)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.DetectionLiteDbPath, $"position_{DateTime.UtcNow.ToString("yyyy_MM_dd")}.db");
            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
        }

        public async void AddOrUpdateAsync(TelemetryMessageViewModel position)
        {
            await Task.Run(() => { 
                var positionCollection = database.GetCollection<TelemetryMessageViewModel>("Positions");
                positionCollection.Insert(position);
                IndexIssue(positionCollection);
            });
        }

        public List<TelemetryMessageViewModel> CurrentPositions()
        {
            var positionCollection = database.GetCollection<TelemetryMessageViewModel>("Positions");

            var positions = positionCollection.FindAll().Where(x => x.TimeMeasured >= DateTime.UtcNow.AddSeconds(-20))
                            .ToList()
                            .GroupBy(p => p.Gufi)
                            .Select(g => g.OrderByDescending(c => c.TimeMeasured).First())
                            .Select(c => c);

            return positions.ToList();
        }

        private void IndexIssue(LiteCollection<TelemetryMessageViewModel> positionCollection)
        {
            // Index on EnroutePositionsId            
            positionCollection.EnsureIndex(x => x.EnroutePositionsId);
        }
    }
}
