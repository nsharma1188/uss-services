﻿using AnraUssServices.Common;
using AnraUssServices.ViewModel.Detects;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace AnraUssServices.LiteDBJobs
{
    public class DetectSubscribersDocument
    {        
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public DetectSubscribersDocument(ILogger<string> logger, AnraConfiguration configuration)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.DetectionLiteDbPath, $"detectsubscribers_{DateTime.UtcNow.ToString("yyyy_MM_dd")}.db");
            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
        }

        public void AddUpdateSubscription(DetectSubscriberViewModel detect)
        {            
            var subscriberCollection = database.GetCollection<DetectSubscriberViewModel>("DetectSubscribers");

            var isExistingSubscription = subscriberCollection.Exists(x => x.SubscriptionId.Equals(detect.SubscriptionId));

            if (isExistingSubscription)
            {                
                subscriberCollection.Update(detect);
            }
            else
            {
                subscriberCollection.Insert(detect);
                IndexIssue(subscriberCollection);
            }

        }

        public void Unsubscribe(string gufi)
        {
            var subscriberCollection = database.GetCollection<DetectSubscriberViewModel>("DetectSubscribers");

            var isExistingSubscription = subscriberCollection.Exists(x => x.Gufi.Equals(Guid.Parse(gufi)));

            if (isExistingSubscription)
            {
                var model = subscriberCollection.Find(x => x.Gufi.Equals(Guid.Parse(gufi))).FirstOrDefault();

                //Set IsActive to False
                model.IsActive = false;
                subscriberCollection.Update(model);
            }
        }

        private void IndexIssue(LiteCollection<DetectSubscriberViewModel> subscriberCollection)
        {
            // Index on SubscriptionId            
            subscriberCollection.EnsureIndex(x => x.SubscriptionId);
        }
    }
}
