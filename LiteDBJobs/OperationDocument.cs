﻿using AnraUssServices.Data;
using AnraUssServices.ViewModel.Operations;
using LiteDB;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.LiteDBJobs
{
    public class OperationDocument
    {
        private readonly LightDatabaseContext _context;

        public OperationDocument(LightDatabaseContext lightDatabaseContext)
        {
            _context = lightDatabaseContext;
        }

        public void AddOrUpdate(OperationViewModel operation)
        {
            var operationCollection = _context.LiteDBInstance.GetCollection<OperationViewModel>("Operations");

            var isExistingOperation = operationCollection.Exists(x => x.Gufi == operation.Gufi); 

            if (isExistingOperation)
            {
                operationCollection.Update(operation);
            }
            else
            {                
                operationCollection.Insert(operation);
                IndexIssue(operationCollection);
            }
        }

        public void AddMany(List<OperationViewModel> operations)
        {
            if (operations != null && operations.Any())
            {
                operations.ForEach(AddOrUpdate);
            }
        }

        private void IndexIssue(LiteCollection<OperationViewModel> operationCollection)
        {
            // Index on IssueId  
            operationCollection.EnsureIndex(x => x.Gufi);
        }
    }
}
