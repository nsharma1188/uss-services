﻿using MediatR;
using System;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncUssExchangeNotification : IRequest
    {
        public readonly string SourceUssName;
        public readonly string TargetUssName;
        public readonly string JwsHeader;
        public readonly string ExternalRequestPath;
        public readonly int? ExternalStatusCode;
        public readonly string ExternalResponse;
        public readonly string ExternalRequest;
        public readonly string ExternalHttpMethod;

        public DateTime ResponseTime;
        public DateTime RequestTime;

        public Jal.HttpClient.Model.HttpResponse InternalResponse;
        public Jal.HttpClient.Model.HttpRequest InternalRequest;
        public bool IsAnraRequest;


        //*********************Himanhshu****************
        public enum RequestType //need to add other types as required
        {
            NONE,
            OUTGOING_REQUEST_WITHOUT_GUFI
        }

        public RequestType RequestType_ = RequestType.NONE;   //Defines which type of request     
        //***************************************************

        public AsyncUssExchangeNotification(string requestBody, string responseBody, int? statusCode, string path, string jwsHeader, DateTime requestTime, DateTime responseTime, string sourceUssName, string httpMethod)
        {
            RequestTime = requestTime;
            ResponseTime = responseTime;
            IsAnraRequest = false;
            ExternalRequest = requestBody;
            ExternalResponse = responseBody;
            ExternalStatusCode = statusCode;
            ExternalRequestPath = path;
            JwsHeader = jwsHeader;
            SourceUssName = sourceUssName;
            ExternalHttpMethod = httpMethod;
            RequestType_ = RequestType.NONE;
        }

        public AsyncUssExchangeNotification(Jal.HttpClient.Model.HttpRequest internalRequest, Jal.HttpClient.Model.HttpResponse internalResponse, DateTime requestTime, DateTime responseTime, string targetUssName)
        {
            InternalRequest = internalRequest;
            InternalResponse = internalResponse;
            RequestTime = requestTime;
            ResponseTime = responseTime;
            IsAnraRequest = true;
            TargetUssName = targetUssName;
            RequestType_ = RequestType.NONE;
        }

        public AsyncUssExchangeNotification(String ussName,Jal.HttpClient.Model.HttpRequest internalRequest, Jal.HttpClient.Model.HttpResponse internalResponse, DateTime requestTime, DateTime responseTime)
        {
            InternalRequest = internalRequest;
            InternalResponse = internalResponse;
            RequestTime = requestTime;
            ResponseTime = responseTime;
            IsAnraRequest = true;
            TargetUssName = ussName;
            RequestType_ = RequestType.OUTGOING_REQUEST_WITHOUT_GUFI;            
        }
    }
}