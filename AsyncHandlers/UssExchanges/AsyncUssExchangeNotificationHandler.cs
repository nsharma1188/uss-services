﻿using AnraUssServices.Common;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.UssExchangeData;
using AnraUssServices.Data;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncUssExchangeNotificationHandler : AsyncRequestHandler<AsyncUssExchangeNotification>
    {
        private readonly AnraConfiguration anraConfiguration;
        private readonly AnraMqttClient mqttClient;
        private readonly LightDatabaseContext lightDatabaseContext;
		private readonly ILogger<string> logger;

        public AsyncUssExchangeNotificationHandler(LightDatabaseContext lightDatabaseContext,
            ILogger<string> logger,
            AnraConfiguration anraConfiguration,
            AnraMqttClient mqttClient)
        {
            this.logger = logger;
            this.mqttClient = mqttClient;
            this.lightDatabaseContext = lightDatabaseContext;
            this.anraConfiguration = anraConfiguration;
        }

        protected override async Task HandleCore(AsyncUssExchangeNotification request)
        {
            await Task.Factory.StartNew(() =>
            {
                if (request.IsAnraRequest)
                {                   

                    switch(request.RequestType_)
                    {
                        case AsyncUssExchangeNotification.RequestType.OUTGOING_REQUEST_WITHOUT_GUFI:
                            ProcessRequestInternalWithoutGufi(request);
                            break;
                        case AsyncUssExchangeNotification.RequestType.NONE:
                            ProcessRequestInternal(request);
                            break;
                        default:
                            ProcessRequestInternal(request);
                            break;
                    }
                    
                }
                else
                {
                    ProcessRequestExternal(request);
                }
            });
        }

        //Himanshu:- For Get Request without gufi. Need to be merge in outgoing request
        private void ProcessRequestInternalWithoutGufi(AsyncUssExchangeNotification notification)
        {
            try
            {
                var pathVars = notification.InternalRequest.Url.Split('/');

                Guid Pk = Guid.Empty;
                Guid.TryParse(pathVars[pathVars.Length - 1], out Pk);

                var ussExchangeInstance = new UssExchange(ReportingUssRoleEnum.SOURCEUSS, "Outgoing Request")
                {
                    event_id = string.Concat("USS_SPRINT1_SIM_", notification.RequestTime.ToString("YYYYMMDD"), "_", notification.RequestTime.ToString("HHMMSS")),
                    exchanged_data_pk = Pk,
                    exchanged_data_type = ParseDataType(pathVars[pathVars.Length - 2]),
                    reporting_uss_role = ReportingUssRoleEnum.SOURCEUSS,
                    source_uss = anraConfiguration.ClientId,
                    uss_name = anraConfiguration.ClientId,
                    target_uss = notification.TargetUssName,
                    time_request_initiation = notification.RequestTime,
                    time_request_completed = notification.ResponseTime,
                    endpoint = notification.InternalRequest.Url,
                    http_method = HttpMethodEnum.GET,
                    expected_http_response = 200,
                    actual_http_response = notification.InternalResponse.HttpStatusCode.HasValue ? (int)notification.InternalResponse.HttpStatusCode.Value : 404, //Setting 404 in case of unknow error response
                    response_content = notification.InternalResponse.Content != null ? notification.InternalResponse.Content.Read() : "",
                    request_content = notification.InternalRequest.Content.ToString()
                };

                //To-Do Commenting these lines as jws is optional field
                //var jwsHeader = notification.InternalRequest.Headers.Find(x => x.Name.Equals("x-utm-signed-payload", StringComparison.InvariantCultureIgnoreCase));
                //if (jwsHeader != null)
                //{
                //    ussExchangeInstance.jws = jwsHeader.Value;
                //}

                SaveAndNotify(ussExchangeInstance);
            }
            catch (Exception ex)
            {
                logger.LogError("UssExchangeHandler:: Exception={0}", ex);
            }
        }

        private void ProcessRequestInternal(AsyncUssExchangeNotification notification)
        {
            try
            {
                var pathVars = notification.InternalRequest.Url.Split('/');

                Guid Pk = Guid.Empty;
                Guid.TryParse(pathVars[pathVars.Length - 1], out Pk);                

                var ussExchangeInstance = new UssExchange(ReportingUssRoleEnum.SOURCEUSS, "Outgoing Request")
                {
                    event_id = string.Concat("USS_SPRINT1_SIM_", notification.RequestTime.ToString("YYYYMMDD"), "_", notification.RequestTime.ToString("HHMMSS")),
                    exchanged_data_pk = Pk,
                    exchanged_data_type = ParseDataType(pathVars[pathVars.Length - 2]),
                    reporting_uss_role = ReportingUssRoleEnum.SOURCEUSS,
                    source_uss = anraConfiguration.ClientId,
                    uss_name = anraConfiguration.ClientId,
                    target_uss = notification.TargetUssName,
                    time_request_initiation = notification.RequestTime,
                    time_request_completed = notification.ResponseTime,
                    endpoint = notification.InternalRequest.Url,
                    http_method = GetHttpMethod(notification.InternalRequest.HttpMethod.ToString().ToUpper()),
                    expected_http_response = notification.InternalRequest.HttpMethod.ToString().ToUpper().Equals("PUT") ? 204 : 200,
                    actual_http_response = notification.InternalResponse.HttpStatusCode.HasValue ? (int)notification.InternalResponse.HttpStatusCode.Value : 404, //Setting 404 in case of unknow error response
                    response_content = notification.InternalResponse.Content != null ? notification.InternalResponse.Content.Read() : "",
                    request_content = notification.InternalRequest.Content.ToString()
                };

                //To-Do Commenting these lines as jws is optional field
                //var jwsHeader = notification.InternalRequest.Headers.Find(x => x.Name.Equals("x-utm-signed-payload", StringComparison.InvariantCultureIgnoreCase));
                //if (jwsHeader != null)
                //{
                //    ussExchangeInstance.jws = jwsHeader.Value;
                //}

                SaveAndNotify(ussExchangeInstance);

				//Notify Internal USSExchange to NASA. NASA wants to send all the ussexchange at once.
				//ussExchangeDataHandler.Notify(ussExchangeInstance, pathVars);

			}
            catch (Exception ex)
            {
                logger.LogError("UssExchangeHandler:: Exception={0}", ex);
            }
        }

        private void ProcessRequestExternal(AsyncUssExchangeNotification notification)
        {
            try
            {
                var pathVars = notification.ExternalRequestPath.Split('/');

                Guid Pk = Guid.Empty;
                Guid.TryParse(pathVars[pathVars.Length - 1], out Pk);

                var ussExchangeInstance = new UssExchange(ReportingUssRoleEnum.TARGETUSS, "Incoming Request")
                {
                    event_id = string.Concat("USS_SPRINT1_SIM_", notification.RequestTime.ToString("YYYYMMDD"), "_", notification.RequestTime.ToString("HHMMSS")),
                    time_request_initiation = notification.RequestTime,
                    time_request_completed = notification.ResponseTime,
                    reporting_uss_role = ReportingUssRoleEnum.TARGETUSS,
                    target_uss = anraConfiguration.ClientId,
                    source_uss = notification.SourceUssName,
                    uss_name = anraConfiguration.ClientId,
                    exchanged_data_pk = Pk,
                    exchanged_data_type = ParseDataType(pathVars[pathVars.Length - 2]),
                    expected_http_response = notification.ExternalHttpMethod.ToUpper().Equals("PUT") ? 204 : 200,
                    endpoint = notification.ExternalRequestPath,
                    http_method = GetHttpMethod(notification.ExternalHttpMethod.ToUpper()),
                    actual_http_response = notification.ExternalStatusCode.HasValue ? notification.ExternalStatusCode.Value : 404,//Setting 404 in case of unknow error response
                    response_content = notification.ExternalResponse,
                    request_content = notification.ExternalRequest
                    //jws = notification.JwsHeader -- TO Do Commenting this as this is optional field
                };

                SaveAndNotify(ussExchangeInstance);
            }
            catch (Exception ex)
            {
                logger.LogError("UssExchangeHandler:: Exception={0}", ex);
            }
        }

        private void SaveAndNotify(UssExchange data)
        {
            lightDatabaseContext.SaveUssExchangeAsync(data);
            mqttClient.PublishTopic(MqttTopics.NotifyUssExchange, data);
        }

        private static ExchangedDataTypeEnum ParseDataType(string route)
        {
            switch (route.ToLower())
            {
                case "operation":
                case "operations": return ExchangedDataTypeEnum.OPERATION;
                case "uss": return ExchangedDataTypeEnum.USS_INSTANCE;
                case "negotiations": return ExchangedDataTypeEnum.NEGOTIATION_MESSAGE;
                case "constraints": return ExchangedDataTypeEnum.CONSTRAINT_MESSAGE;
                case "utm_messages": return ExchangedDataTypeEnum.UTM_MESSAGE;
                case "positions": return ExchangedDataTypeEnum.POSITION;
                default: return ExchangedDataTypeEnum.OTHER_SEE_COMMENT;
            }
        }

        private string ReadContent(Stream stream)
        {
            var data = string.Empty;
            try
            {
                var bodyStream = new MemoryStream();
                stream.CopyToAsync(bodyStream);
                bodyStream.Seek(0, SeekOrigin.Begin);
                using (var streamReader = new StreamReader(bodyStream))
                {
                    data = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                logger.LogError("UssExchangeHandler:: Ex={0}", ex);
            }

            return data;
        }

        private HttpMethodEnum GetHttpMethod(string httpMethodValue)
        {
            HttpMethodEnum httpMethod;
            Enum.TryParse(httpMethodValue, out httpMethod);
            return httpMethod;
        }
    }
}