﻿using AnraUssServices.Common;
using AnraUssServices.Dss.Models;
using AnraUssServices.Dss.ViewModel.IdentificationServiceArea;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{    
    public class DssNotificationHandler : AsyncRequestHandler<AsyncDssNotification>
    {
        private readonly IServiceProvider serviceProvider;
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly ILogger<DssNotificationHandler> logger;
        private IRepository<Models.ServiceArea> serviceProviderRepository;

        public DssNotificationHandler(ILogger<DssNotificationHandler> logger, IServiceProvider serviceProvider,
            AnraHttpClient httpClient,
            AnraConfiguration configuration)
        {
            this.logger = logger;
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.serviceProvider = serviceProvider;
        }

        protected override async Task HandleCore(AsyncDssNotification request)
        {
            await Task.Factory.StartNew(() =>
            {
                SetDependencies();
                ProcessRequest(request);
            });
        }

        private void SetDependencies()
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            serviceProviderRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.ServiceArea>>();
        }

        private void ProcessRequest(AsyncDssNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  DssNotificationHandler :: USS Instance = {0} , CRUD Action = {1}", request.UssInstance.UssInstanceId, EnumUtils.GetDescription(request.Action));
            #endif

            var serviceProvider = serviceProviderRepository.Find(request.UssInstance.UssInstanceId);

            //Map 3D Volume
            Volume3D vol3D = new Volume3D();
            vol3D.Footprint = request.UssInstance.CoverageArea.ToGeoPolygon();

            //Map 4D Volume
            Volume4D vol4D = new Volume4D();
            vol4D.SpatialVolume = vol3D;
            vol4D.TimeStart = request.UssInstance.TimeAvailableBegin;
            vol4D.TimeEnd = request.UssInstance.TimeAvailableEnd;

            //Create Identification Service Area Parameters Model Object 
            ISAParametersViewModel model = new ISAParametersViewModel {Extents = vol4D };

            string requestUrl = string.Empty;

            switch (request.Action)
            {
                case CRUDActionEnum.INSERT:

                    requestUrl = string.Format("{0}dss/identification_service_areas/{1}", configuration.DssServiceProviderUrl, request.UssInstance.UssInstanceId);
                    httpClient.Put(requestUrl, model, request.AccessToken);
                    break;

                case CRUDActionEnum.UPDATE:

                    if(serviceProvider != null)
                    {
                        requestUrl = string.Format("{0}dss/identification_service_areas/{1}/{2}", configuration.DssServiceProviderUrl, request.UssInstance.UssInstanceId,serviceProvider.Version);
                        httpClient.Put(requestUrl, model, request.AccessToken);
                    }
                    else
                    {
                        #if DEBUG
                            logger.LogInformation("ANRA::: DssNotificationHandler :: No Subscriber found (USS Instance = {0} , CRUD Action = {1}).", request.UssInstance.UssInstanceId, EnumUtils.GetDescription(request.Action));
                        #endif
                    }
                    
                    break;

                case CRUDActionEnum.DELETE:

                    if (serviceProvider != null)
                    {
                        requestUrl = string.Format("{0}dss/identification_service_areas/{1}/{2}", configuration.DssServiceProviderUrl, request.UssInstance.UssInstanceId, serviceProvider.Version);
                        httpClient.Delete<UTMRestResponse>(requestUrl, request.AccessToken);
                    }
                    else
                    {
                        #if DEBUG
                            logger.LogInformation("ANRA::: DssNotificationHandler :: No Subscriber found (USS Instance = {0} , CRUD Action = {1}).", request.UssInstance.UssInstanceId, EnumUtils.GetDescription(request.Action));
                        #endif
                    }

                    break;

                default:
                    break;
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END DssNotificationHandler :: USS Instance = {0} , CRUD Action = {1}", request.UssInstance.UssInstanceId, EnumUtils.GetDescription(request.Action));
            #endif
        }
    }
}