﻿using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.UtmInstances;
using MediatR;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncDssNotification : IRequest
    {
        public CRUDActionEnum Action { get; private set; }
        public UtmInstanceItem UssInstance { get; private set; }
        public string AccessToken { get; private set; }

        public AsyncDssNotification(UtmInstanceItem ussInstance, CRUDActionEnum action, string accessToken)
        {
            UssInstance = ussInstance;
            Action = action;
            AccessToken = accessToken;
        }
    }
}
