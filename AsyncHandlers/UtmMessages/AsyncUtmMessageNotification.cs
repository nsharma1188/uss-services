﻿using AnraUssServices.Utm.Models;
using MediatR;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncUtmMessageNotification : IRequest
    {
        public UTMMessage Message { get; private set; }
        public List<GridCellOperatorResponse> Operators { get; private set; }

        public AsyncUtmMessageNotification(UTMMessage message, List<GridCellOperatorResponse> operators)
        {
            Operators = operators;
            Message = message;
        }
    }
}