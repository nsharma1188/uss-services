﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class UtmMessageNotificationHandler : AsyncRequestHandler<AsyncUtmMessageNotification>
    {
        private readonly ViewerMqttNotification viewerMqttNotification;
        private const string endpoint = "/utm_messages";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<UtmMessageNotificationHandler> logger;

        public UtmMessageNotificationHandler(ILogger<UtmMessageNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            ViewerMqttNotification viewerMqttNotification,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.viewerMqttNotification = viewerMqttNotification;
        }

        protected override async Task HandleCore(AsyncUtmMessageNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => ProcessRequest(request));
        }

        private async void ProcessRequest(AsyncUtmMessageNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  UtmMessageNotificationHandler :: Gufi = {0}, Network Nodes:{1}, MessageId = {2}", request.Message.Gufi, request.Operators.Count(), request.Message.MessageId);
            #endif

            viewerMqttNotification.NotifyUtmMessage(request.Message);

            //Code to Send Notification to all USS networks in LUN
            if (request.Operators.Any())
            {
                foreach (var oprtor in request.Operators)
                {
                    if (oprtor.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                    {
                        await httpClient.Put(oprtor.UssBaseurl, endpoint + "/" + request.Message.MessageId, request.Message, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE), oprtor.Uss);
                    }
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END UtmMessageNotificationHandler :: Gufi = {0}, Network Nodes:{1}, MessageId = {2}", request.Message.Gufi, request.Operators.Count(), request.Message.MessageId);
            #endif
        }
    }
}