﻿using AnraUssServices.Common;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.ElasticSearch;
using AnraUssServices.ViewModel.Collisions;
using GeoAPI.Geometries;
using MediatR;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncCollisionNotificationHandler : AsyncRequestHandler<AsyncCollisionNotification>
    {
        private readonly CollisionIndex collisionIndex;
        private readonly OperationIndex operationIndex;
        private readonly GeometryFactory geometryFactory;
        private readonly DroneIndex droneIndex;
        private readonly PositionIndex positionIndex;
        private readonly AnraMqttClient anraMqttClient;
        private const string endpoint = "/operations";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsAuthentication fimsAuthentication;
        private readonly ILogger<string> logger;

        public AsyncCollisionNotificationHandler(ILogger<string> logger,
            AnraHttpClient httpClient,
            AnraMqttClient anraMqttClient,
            AnraConfiguration configuration,
            OperationIndex operationIndex,
            DroneIndex droneIndex,
            CollisionIndex collisionIndex,
            PositionIndex positionIndex)
        {
            this.logger = logger;
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.anraMqttClient = anraMqttClient;
            this.positionIndex = positionIndex;
            this.droneIndex = droneIndex;
            this.operationIndex = operationIndex;

            this.geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
            this.collisionIndex = collisionIndex;
        }

        protected override async Task HandleCore(AsyncCollisionNotification request)
        {
            await Task.Factory.StartNew(() => ProcessRequest(request));
        }

        private void ProcessRequest(AsyncCollisionNotification request)
        {
            if (configuration.IsCollisionDetectionEnabled)
            {
                var latestPositions = positionIndex.CurrentPositions();
                if (latestPositions.Any())
                {
                    var drones = droneIndex.GetDrones(latestPositions.Select(p => p.Registration).ToArray());

                    var dronePosition = drones.Join(latestPositions, d => d.Uid, p => p.Registration, (d, p) => new
                    {
                        p.Gufi,
                        d.Uid,
                        d.CollisionThreshold,
                        position = p,
                        droneLocation = new Coordinate(p.Location.X, p.Location.Y, p.AltitudeGpsWgs84Ft.Value)
                    });

                    var detects = request.Detects.SelectMany(p => p.Value).Select(d => new { Point = new Coordinate(d.Longitude, d.Latitude, d.Altitude), Detect = d }).ToList();

                    var collisionsTemp = dronePosition.SelectMany(dp => detects, (dp, detect) => new
                    {
                        dp.Gufi,
                        dp,
                        detect,
                        dp.CollisionThreshold,
                        Distance = DistanceHelper.CalculateDistance3D(dp.droneLocation.X, dp.droneLocation.Y, dp.droneLocation.Z, detect.Point.X, detect.Point.Y, detect.Point.Z)
                    });

                    logger.LogDebug("collisionsTemp {0}", collisionsTemp);

                    var collisions = dronePosition.SelectMany(dp => detects, (dp, detect) => new
                    {
                        dp.Gufi,
                        dp,
                        detect,
                        distanceInFeet = DistanceHelper.CalculateDistance3D(dp.droneLocation.X,
                        dp.droneLocation.Y,
                        dp.droneLocation.Z,
                        detect.Point.X,
                        detect.Point.Y,
                        detect.Point.Z)
                    })
                        .Where(x => x.distanceInFeet <= x.dp.CollisionThreshold && x.detect.Detect.Uid != x.dp.Uid)
                        .GroupBy(x => x.dp.Gufi)
                        .ToDictionary(p => p.Key, p => p.Select(x => new CollisionViewModel
                        {
                            CollisionId = Guid.NewGuid(),
                            Detect = x.detect.Detect,
                            Gufi = x.Gufi,
                            Position = x.dp.position,
                            TimeStamp = DateTime.UtcNow,
                            Uid = x.dp.Uid,
                            Distance3D = x.distanceInFeet,
                            Threshold = x.dp.CollisionThreshold
                        }));

                    if (collisions.Any())
                    {
                        logger.LogDebug("collisions {0}", collisions.Count);

                        foreach (var collision in collisions)
                        {
                            anraMqttClient.PublishTopic(string.Format(MqttTopics.CollisionWarning, collision.Key), new { Gufi = collision.Key });

                            collisionIndex.AddMany(collision.Value);
                        }
                    }
                }
            }
        }
    }
}