﻿using AnraUssServices.Models.Collisions;
using AnraUssServices.ViewModel.Detects;
using MediatR;
using System;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncCollisionNotification : IRequest
    {
        public Collision Collision { get; private set; }

        public Dictionary<Guid, List<DetectViewModel>> Detects { get; private set; }

        public AsyncCollisionNotification(Collision collision)
        {
            this.Collision = collision;
        }

        public AsyncCollisionNotification(Dictionary<Guid, List<DetectViewModel>> userDetects)
        {
            Detects = userDetects;
        }
    }
}