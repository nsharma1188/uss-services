﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class UssDiscoveryNotificationHandler : AsyncRequestHandler<AsyncUssDiscoveryNotification>
    {
        private const string endpoint = "/ussdisc";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<UssDiscoveryNotificationHandler> logger;

        public UssDiscoveryNotificationHandler(ILogger<UssDiscoveryNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        protected override async Task HandleCore(AsyncUssDiscoveryNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => ProcessRequest(request));
        }

        private void ProcessRequest(AsyncUssDiscoveryNotification request)
        {
            httpClient.Put(configuration.FimsBaseUrl, endpoint + "/" + request.UssInstance.UssInstanceId, request.UssInstance, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT));
        }
    }
}