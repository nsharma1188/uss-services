﻿using MediatR;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncUssDiscoveryNotification : IRequest
    {
        public bool IsNew { get; private set; }
        public Utm.Models.UssInstance UssInstance { get; private set; }

        public AsyncUssDiscoveryNotification(Utm.Models.UssInstance ussInstance, bool isNew = false)
        {
            UssInstance = ussInstance;
            IsNew = isNew;
        }
    }
}