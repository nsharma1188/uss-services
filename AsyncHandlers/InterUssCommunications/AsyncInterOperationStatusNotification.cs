﻿using AnraUssServices.Utm.Models;
using MediatR;
using System;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncInterOperationStatusNotification : IRequest
    {
        public List<GridCellOperatorResponse> Operators { get; private set; }

        public UTMMessage UtmMessage { get; set; }

        public AsyncInterOperationStatusNotification(UTMMessage message, List<GridCellOperatorResponse> operators)
        {
            UtmMessage= message;
            Operators = operators;
        }
    }
}