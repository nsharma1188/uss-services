﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{    
    public class InterOperationNotificationHandler : AsyncRequestHandler<AsyncInterOperationNotification>
    {
        private const string endpoint = "/operations";
        private const string enhancedendpoint = "/enhanced/operations";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<InterOperationNotificationHandler> logger;

        public InterOperationNotificationHandler(ILogger<InterOperationNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        protected override async Task HandleCore(AsyncInterOperationNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => ProcessRequest(request));
        }

        private void ProcessRequest(AsyncInterOperationNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  InterOperationNotificationHandler :: Gufi = {0}, Operators:{1}", request.Operation.Gufi, request.Operators.Count());
            #endif

            //Code to Send Notification to all USS networks in LUN
            if (request.Operators.Any())
            {                
                foreach (var op in request.Operators)
                {
                    #if DEBUG
                        logger.LogInformation($"ANRA::: START  InterOperationNotificationHandler :: Operator = {op.Uss}, AnnouncementLevel:{op.AnnouncementLevel}");
                    #endif

                    if (op.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                    {
                        //If operation is PUBLIC SAFETY then use external enhanced operation endpoint else normal operation endpoint
                        string opsEndPoint = request.Operation.PriorityElements.PriorityStatus.Equals(PriortyStatus.PUBLIC_SAFETY) ? string.Format("{0}/{1}", enhancedendpoint, request.Operation.Gufi) : string.Format("{0}/{1}", endpoint, request.Operation.Gufi);
                        string token = request.Operation.PriorityElements.PriorityStatus.Equals(PriortyStatus.PUBLIC_SAFETY) ? fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_PUBLICSAFETY) : fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION);
                        var response = httpClient.Put(op.UssBaseurl, opsEndPoint, request.Operation, token, op.Uss).Result;
                    }
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END InterOperationNotificationHandler :: Gufi = {0}, Operators:{1}", request.Operation.Gufi, request.Operators.Count());
            #endif
        }
    }
}