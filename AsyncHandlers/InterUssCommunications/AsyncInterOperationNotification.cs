﻿using AnraUssServices.Utm.Models;
using MediatR;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncInterOperationNotification : IRequest
    {
        public bool IsNewOperation { get; private set; }
        public List<GridCellOperatorResponse> Operators { get; private set; }
        public Utm.Models.Operation Operation { get; private set; }

        public AsyncInterOperationNotification(Utm.Models.Operation operation, List<GridCellOperatorResponse> operators, bool isNewOperation = false)
        {
            Operation = operation;
            Operators = operators;
            IsNewOperation = isNewOperation;
        }
    }
}
