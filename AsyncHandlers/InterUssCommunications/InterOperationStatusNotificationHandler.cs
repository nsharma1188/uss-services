﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class InterOperationStatusNotificationHandler : AsyncRequestHandler<AsyncInterOperationStatusNotification>
    {
        private const string endpoint = "/utm_messages";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<InterOperationStatusNotificationHandler> logger;

        public InterOperationStatusNotificationHandler(ILogger<InterOperationStatusNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        protected override async Task HandleCore(AsyncInterOperationStatusNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => ProcessRequestAsync(request));
        }

        private async void ProcessRequestAsync(AsyncInterOperationStatusNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  InterOperationStatusNotificationHandler :: Gufi = {0}, Operators:{1}", request.UtmMessage.Gufi, request.Operators.Count());
            #endif

            //Code to Send Notification to all USS networks in LUN
            if (request.Operators.Any())
            {
                foreach (var oprtor in request.Operators)
                {
                    if(oprtor.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                    {
                        await httpClient.Put(oprtor.UssBaseurl, endpoint + "/" + request.UtmMessage.MessageId, request.UtmMessage, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE), oprtor.Uss);
                    }                    
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END InterOperationStatusNotificationHandler :: Gufi = {0}, Network Nodes:{1}", request.UtmMessage.Gufi, request.Operators.Count());
            #endif
        }
    }
}