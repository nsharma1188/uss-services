﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Data;
using AnraUssServices.ElasticSearch;
using AnraUssServices.Models.Detects;
using AnraUssServices.ViewModel.Detects;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncDetectNotificationHandler : AsyncRequestHandler<AsyncDetectNotification>
    {
        private readonly IMediator mediator;
        private readonly ApplicationDbContext dbContext;
        private readonly AnraMqttClient mqttClient;
        private readonly DetectsIndex detectsIndex;
        private readonly DetectSubscriberIndex detectSubscriberIndex;
        private readonly ILogger<string> logger;

        public AsyncDetectNotificationHandler(ILogger<string> logger,
            AnraMqttClient mqttClient,
            IMediator mediator,
            DetectsIndex detectsIndex,
            ApplicationDbContext dbContext,
            DetectSubscriberIndex detectSubscriberIndex)
        {
            this.logger = logger;
            this.mqttClient = mqttClient;
            this.detectsIndex = detectsIndex;
            this.detectSubscriberIndex = detectSubscriberIndex;
            this.dbContext = dbContext;
            this.mediator = mediator;
        }

        protected override async Task HandleCore(AsyncDetectNotification request)
        {
            await Task.Factory.StartNew(() => ProcessRequestAsync(request));
        }

        private async Task ProcessRequestAsync(AsyncDetectNotification request)
        {
            try
            {
                logger.LogInformation("ANRA::: START  AsyncDetectNotificationHandler :: Detects = {0}", request.Detects.Count);

                detectsIndex.AddMany(request.Detects);

                await ProcessDetectsAsync(request.Detects);

                //SaveDetectsAsync(request.Detects);

                logger.LogInformation("ANRA::: END AsyncDetectNotificationHandler :: Detects = {0}", request.Detects.Count);
            }
            catch (Exception ex)
            {
                logger.LogError("ANRA::: END AsyncDetectNotificationHandler :: Exception = {0}", ex);
            }
        }

        private async Task ProcessDetectsAsync(List<DetectViewModel> detects)
        {
            logger.LogInformation("ProcessDetectsAsync :: Detects{0}", detects.Count());

            var groupedDetects = detects.GroupBy(p => p.Source).ToDictionary(p => p.Key, p => p.ToList());
            var userDetects = new Dictionary<Guid, List<DetectViewModel>>();
            foreach (var detect in detects)
            {
                var subscribers = detectSubscriberIndex.GetSubscribers(detect.Source, detect.Location);
                logger.LogInformation("ProcessDetectsAsync :: subscribers{0}", subscribers.Count());

                foreach (var sub in subscribers)
                {
                    if (!userDetects.ContainsKey(sub.UserId))
                    {
                        userDetects.Add(sub.UserId, new List<DetectViewModel>());
                    }

                    if (sub.SubscribedTypes.Contains(detect.Source))
                    {
                        userDetects[sub.UserId].Add(detect);
                    }
                }
            }

            if (userDetects.Any())
            {
                logger.LogInformation("ProcessDetectsAsync :: userSubTypes{0}", userDetects.Count());

                foreach (var pair in userDetects)
                {
                    logger.LogInformation("ProcessDetectsAsync :: useid {0} list = {1}", pair.Key, pair.Value);
                    var topic = string.Format(MqttTopics.UserDetects, pair.Key);
                    logger.LogInformation("ProcessDetectsAsync :: topic {0}", topic);
                    await mqttClient.PublishTopic(string.Format(MqttTopics.UserDetects, pair.Key), pair.Value);
                }

                mediator.Send(new AsyncCollisionNotification(userDetects));
            }
        }

        private void SaveDetectsAsync(List<DetectViewModel> detects)
        {
            var detectModels = detects.Adapt<IEnumerable<Detect>>();
            dbContext.Detects.AddRange(detectModels);
            dbContext.SaveChanges();
        }
    }
}