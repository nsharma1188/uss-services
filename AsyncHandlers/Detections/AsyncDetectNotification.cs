﻿using AnraUssServices.Models.Detects;
using AnraUssServices.ViewModel.Detects;
using MediatR;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncDetectNotification : IRequest
    {
        public List<DetectViewModel> Detects { get; private set; }

        public AsyncDetectNotification(List<DetectViewModel> detects)
        {
            Detects = detects;
        }
    }
}