﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class PositionNotificationHandler : AsyncRequestHandler<AsyncPositionNotification>
    {
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<PositionNotificationHandler> logger;
        private IRepository<UtmInstance> utmInstanceRepository;

        public PositionNotificationHandler(ILogger<PositionNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        protected override async Task HandleCore(AsyncPositionNotification request)
        {
            if (configuration.IsUtmEnabled)
            {
                await Task.Factory.StartNew(() => ProcessRequestAsync(request));
            }
        }

        private async Task ProcessRequestAsync(AsyncPositionNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  PositionNotificationHandler :: Gufi = {0}, Operators:{1}, PositionId = {2}", request.Position.Gufi, request.Operators.Count(), request.Position.EnroutePositionsId);
            #endif

            if (request.Operation.State == OperationState.CLOSED)
                return;

            //Code to Send Notification to all USS networks in LUN
            if (request.Operators.Any())
            {
                if (request.Operation.IsPublicSafety() || request.Operation.IsEmergency())
                {
                    //Always send position reports for public safety operations or operations in emergency status to other USS in LUN
                    request.Operators.ForEach(async network => {
                        if (network.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                        {
                            await httpClient.Put(network.UssBaseurl, "/positions/" + request.Position.EnroutePositionsId, request.Position, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION), network.Uss);
                        }
                    });
                }
                else if (request.IsSinglePositionReport)
                {
                    //Sending single position report for NC/Rogue state to other USS in LUN
                    request.Operators.ForEach(async network => {
                        if (network.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                        {
                            await httpClient.Put(network.UssBaseurl, "/positions/" + request.Position.EnroutePositionsId, request.Position, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION), network.Uss);
                        }
                    });
                }
                else
                {
                    //Sending reports on specific requests from other USS in LUN
                    foreach (var network in request.Operators)
                    {
                        if (network.AnnouncementLevel == AnnouncementLevelEnum.ALL && ShallForwardPositionReport(request.UtmMessages, network.Uss))
                        {
                            await httpClient.Put(network.UssBaseurl, "/positions/" + request.Position.EnroutePositionsId, request.Position, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION), network.Uss);
                        }
                    }
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END PositionNotificationHandler :: Gufi = {0}, Network Nodes:{1}, PositionId = {2}", request.Position.Gufi, request.Operators.Count(), request.Position.EnroutePositionsId);
            #endif
        }

        private bool ShallForwardPositionReport(List<UtmMessage> messages, string ussName)
        {
            var shallForward = false;

            if (messages != null && messages.Any())
            {
                var recentMessage = messages.Where(p => p.UssName.Equals(ussName)).OrderByDescending(p => p.SentTime).FirstOrDefault();
                shallForward = recentMessage != null && recentMessage.MessageType.Equals("PERIODIC_POSITION_REPORTS_START", StringComparison.InvariantCultureIgnoreCase);
            }

            #if DEBUG
                logger.LogInformation("ANRA::: Position Reporting to {0} is {1}", ussName, shallForward);
            #endif

            return shallForward;
        }
    }
}