﻿using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using MediatR;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncPositionNotification : IRequest
    {
        public Utm.Models.Position Position { get; private set; }

        public Utm.Models.Operation Operation { get; private set; }

        public List<GridCellOperatorResponse> Operators { get; private set; }

        public List<UtmMessage> UtmMessages { get; private set; }

        public bool IsSinglePositionReport { get; private set; }

        public AsyncPositionNotification(Utm.Models.Position position,
            Utm.Models.Operation operation,
            List<GridCellOperatorResponse> operators,
            List<UtmMessage> utmMessages,
            bool isSinglePositionReport = false)
        {
            Operators = operators;
            Position = position;
            Operation = operation;
            UtmMessages = utmMessages != null ? utmMessages.ToList() : new List<UtmMessage>();
            IsSinglePositionReport = isSinglePositionReport;
        }
    }
}