﻿using MediatR;
using System;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncInternalConflictNotification : IRequest
    {
        public readonly string ConflictedOperationGufi;
        public readonly Guid OperationGufi;
        public readonly bool CurrentOperationPrimary;

        public AsyncInternalConflictNotification(Guid operationGufi, string conflictedOperationGufi, bool currentOperationPrimary)
        {
            OperationGufi = operationGufi;
            ConflictedOperationGufi = conflictedOperationGufi;
            CurrentOperationPrimary = currentOperationPrimary;
        }
    }
}