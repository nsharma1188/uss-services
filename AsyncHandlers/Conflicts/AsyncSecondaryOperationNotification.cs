﻿using MediatR;
using System;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncSecondaryOperationNotification : IRequest
    {
        private readonly Guid operationGufi;
        private readonly string externalUssName;
        private readonly Guid externalUssInstanceId;
        private readonly Guid conflictingGufi;

        public Guid OperationGufi { get; private set; }
        public Guid ExternalUssInstanceId { get { return externalUssInstanceId; } }
        public Guid ConflictingOperationGufi { get { return conflictingGufi; } }
        public string ExternalUssName { get { return externalUssName; } }

        public AsyncSecondaryOperationNotification(Guid operationGufi, Guid conflictingGufi, Guid externalUssInstanceId, string externalUssName)
        {
            OperationGufi = operationGufi;
            this.externalUssInstanceId = externalUssInstanceId;
            this.externalUssName = externalUssName;
            this.operationGufi = operationGufi;
            this.conflictingGufi = conflictingGufi;
        }
    }
}