﻿using AnraUssServices.Common;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Models;
using Mapster;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
	public class AsyncInternalConflictNotificationHandler : AsyncRequestHandler<AsyncInternalConflictNotification>
	{
		private readonly GenericMqttNotification genericMqttNotification;
		private readonly IServiceProvider serviceProvider;
		private readonly ILogger<AsyncInternalConflictNotification> logger;
		private OperationStatusLogic OperationStatusLogic;
		private IRepository<OperationConflict> OperationConflictRepository;
		private IRepository<Models.Operation> OperationRepository;
		private string messageToNotify;

		public AsyncInternalConflictNotificationHandler(ILogger<AsyncInternalConflictNotification> logger, IServiceProvider serviceProvider,
			GenericMqttNotification genericMqttNotification)
		{
			this.logger = logger;
			this.serviceProvider = serviceProvider;
			this.genericMqttNotification = genericMqttNotification;
		}

		protected override async Task HandleCore(AsyncInternalConflictNotification request)
		{
			await Task.Factory.StartNew(() =>
			{
				SetDependencies();
				ProcessRequest(request);
			});
		}

		private void SetDependencies()
		{
			var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

			OperationStatusLogic = serviceScope.ServiceProvider.GetService<OperationStatusLogic>();
			OperationConflictRepository = serviceScope.ServiceProvider.GetService<IRepository<OperationConflict>>();

			OperationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();

			messageToNotify = String.Empty;

		}

		private void ProcessRequest(AsyncInternalConflictNotification request)
		{
            #if DEBUG
                logger.LogInformation("AsyncInternalConflictNotificationHandler Operation = {0}, Conflicted Operation:{1}", request.OperationGufi, request.ConflictedOperationGufi);
            #endif

            //Current Operation
            var operation = OperationRepository.Find(request.OperationGufi);

            //Conflicting Operation
            var conflictedOperation = OperationRepository.Find(Guid.Parse(request.ConflictedOperationGufi)).Adapt<Utm.Models.Operation>();

            if (request.CurrentOperationPrimary)
            {
                //Operation state wont be changed if the current state is ACTIVATED.
                if (conflictedOperation.State == OperationState.ACCEPTED)
                {
                    OperationStatusLogic.ToProposed(conflictedOperation.Gufi);
                }
            }

			var conflictDetailsList = BuildMessageToNotify(operation.Adapt<Utm.Models.Operation>());
			ConflictInformation message = new ConflictInformation()
			{
				Message = $"Your operation is in conflict with another ANRA operation ({conflictedOperation.Gufi})",
				ConflictDetails = conflictDetailsList
			};

            //Current Mqtt Notification
			OperationStatusLogic.NotifyOperator(operation, JsonConvert.SerializeObject(message));

            //New Mqtt notification 
            OperationStatusLogic.NotifyOperation(operation, MqttMessageType.CONFLICT.ToString(), JsonConvert.SerializeObject(message));

            //generic notification. Need to refractor
			genericMqttNotification.Send(operation.UserId, GenericNotificationType.CONFLICT, "Operation conflicting with another operation.");

			OperationConflictRepository.Add(new OperationConflict
			{
				//OperationId = operation.OperationId, //integer id removed
				Gufi = request.OperationGufi,
				ConflictingGufi = conflictedOperation.Gufi,
				ConflictingUssName = conflictedOperation.UssName,
				TimeStamp = DateTime.UtcNow,
				IsAnraPrimary = false
			});

            #if DEBUG
                logger.LogInformation("AsyncInternalConflictNotificationHandler END :: Submitted Operation = {0}, Conflicted Operation:{1}", request.OperationGufi, request.ConflictedOperationGufi);
            #endif
        }

		private List<Conflict> BuildMessageToNotify(Utm.Models.Operation operation)
		{
			List<Conflict> conflictDetails = new List<Conflict>();
			foreach (var opsVol in operation.OperationVolumes)
			{
				conflictDetails.Add(
					new Conflict()
					{
						EffectiveTimeBegin = opsVol.EffectiveTimeBegin.Value,
						EffectiveTimeEnd = opsVol.EffectiveTimeEnd.Value,
						MaxAltitude = opsVol.MaxAltitude.AltitudeValue,
						MinAltitude = opsVol.MinAltitude.AltitudeValue,
						OperationGeography = JsonConvert.SerializeObject(opsVol.OperationGeography),
						Ordinal = opsVol.Ordinal
					});
			}
			return conflictDetails;
		}
	}
}