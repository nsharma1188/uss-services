﻿using AnraUssServices.Common;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.Conflicts;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
	public class AsyncSecondaryOperationNotificationHandler : AsyncRequestHandler<AsyncSecondaryOperationNotification>
	{
		private readonly GenericMqttNotification genericMqttNotification;
		private readonly IServiceProvider serviceProvider;
		private readonly ILogger<AsyncSecondaryOperationNotification> logger;
		private OperationStatusLogic OperationStatusLogic;
		private IRepository<OperationConflict> OperationConflictRepository;
		private IRepository<Models.Operation> OperationRepository;		

		public AsyncSecondaryOperationNotificationHandler(ILogger<AsyncSecondaryOperationNotification> logger,
			IServiceProvider serviceProvider,
			GenericMqttNotification genericMqttNotification)
		{
			this.logger = logger;
			this.serviceProvider = serviceProvider;
			this.genericMqttNotification = genericMqttNotification;
		}

		protected override async Task HandleCore(AsyncSecondaryOperationNotification request)
		{
			await Task.Factory.StartNew(() =>
			{
				SetDependencies();
				ProcessRequest(request);
			});
		}

		private void SetDependencies()
		{
			var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

			OperationStatusLogic = serviceScope.ServiceProvider.GetService<OperationStatusLogic>();

			OperationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();

			OperationConflictRepository = serviceScope.ServiceProvider.GetService<IRepository<OperationConflict>>();
		}

		private void ProcessRequest(AsyncSecondaryOperationNotification request)
		{
            #if DEBUG
                logger.LogInformation("AsyncSecondaryOperationNotificationHandler Operation = {0}, Conflicted Operation:{1}", request.OperationGufi, request.ConflictingOperationGufi);
            #endif

            var operation = OperationRepository.Find(request.OperationGufi);
            var conflictedOperation = OperationRepository.Find(request.ConflictingOperationGufi);

            //In case a 409 is received, operation state wont be changed if the current state is ACTIVATED, but record the conflict
            if (operation.State == OperationState.ACCEPTED.ToString())
			{
				OperationStatusLogic.ToProposed(operation.Gufi);
			}

			SaveOperationConflict(request, operation);

            //Get Conflict Details
            var conflictDetails = GetConflicts(operation.Gufi);

            if (conflictDetails.Any())
            {
                //Notify Operator
                OperationStatusLogic.NotifyOperator(operation, JsonConvert.SerializeObject(conflictDetails, Formatting.Indented));

                genericMqttNotification.Send(operation.UserId, GenericNotificationType.CONFLICT, "Operation conflicting with another operation.");

                #if DEBUG
                    logger.LogInformation("AsyncSecondaryOperationNotificationHandler END :: Submitted Operation = {0}, Conflicted Operation:{1}, Notified Message:{2}", request.OperationGufi, request.ConflictingOperationGufi, "Conflict details found.");
                #endif
            }
            else
            {
                #if DEBUG
                    logger.LogInformation("AsyncSecondaryOperationNotificationHandler END :: Submitted Operation = {0}, Conflicted Operation:{1}, Notified Message:{2}", request.OperationGufi, request.ConflictingOperationGufi, "Unable to get conflict details.Conflicting operation not found in system.");
                #endif
            }            
		}

        private List<ConflictViewModel> GetConflicts(Guid gufi)
        {
            try
            {
                var lstOperationConflicts = new List<ConflictViewModel>();

                var conflicts = OperationConflictRepository.GetAll().Where(p => p.Gufi == gufi).Adapt<List<OperationConflictViewModel>>();

                if (conflicts.Any())
                {
                    conflicts.ForEach(x =>
                    {
                        var operationModel = OperationRepository.Find(x.ConflictingGufi);
                        if (operationModel != null && !operationModel.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED)))
                        {
                            var opsConflict = new ConflictViewModel();
                            opsConflict.OperationConflict = x;
                            opsConflict.Reason = OperationRepository.Find(x.ConflictingGufi).OperationVolumes.Adapt<List<Utm.Models.OperationVolume>>();
                            lstOperationConflicts.Add(opsConflict);
                        }
                    });
                }

                return lstOperationConflicts;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void SaveOperationConflict(AsyncSecondaryOperationNotification request, Models.Operation operation)
		{
			var conflictingGuids = ExtractGuidFromResponseMessage(request.ConflictingOperationGufi);

			foreach (var conflictingGufi in conflictingGuids)
			{
				//Find Operation Object
				var externalOps = OperationRepository.Find(conflictingGufi);

				OperationConflictRepository.Add(new OperationConflict
				{
					//OperationId = operation.OperationId,
					Gufi = request.OperationGufi,
					ConflictingGufi = conflictingGufi,
					ConflictingUssName = request.ExternalUssName,
					TimeStamp = DateTime.UtcNow,
					IsAnraPrimary = false
				});
			}
		}

		private static List<Guid> ExtractGuidFromResponseMessage(Guid message)
		{
			var result = new List<Guid>();
			var guids = Regex.Matches(message.ToString(), @"(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}");

			for (int i = 0; i < guids.Count; i++)
			{
				if (Guid.TryParse(guids[i].Value, out Guid newGuid))
				{
					result.Add(newGuid);
				}
			}

			return result;
		}
	}
}