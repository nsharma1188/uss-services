﻿using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using MediatR;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncConstraintMessageNotification : IRequest
    {
        public UASVolumeReservation Uvr { get; private set; }
        public List<GridCellOperatorResponse> Operators { get; private set; }
        public MqttMessageType MqttMessageType { get; private set; }

        public AsyncConstraintMessageNotification(UASVolumeReservation uvr, MqttMessageType mqttMessageType, 
            List<GridCellOperatorResponse> operators = null)
        {            
            Uvr = uvr;
            Operators = operators;
            MqttMessageType = mqttMessageType;
        }
    }
}