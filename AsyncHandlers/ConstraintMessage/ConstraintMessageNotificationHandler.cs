﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Data;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class ConstraintMessageNotificationHandler : AsyncRequestHandler<AsyncConstraintMessageNotification>
    {
        private const string endpoint = "/uvrs";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<ConstraintMessageNotificationHandler> logger;
        private readonly ViewerMqttNotification viewerMqttNotification;
        private readonly IServiceProvider serviceProvider;
        private DatabaseFunctions dbFunctions;        
        private IEnumerable<Models.Operation> intersectedOperations;
        private readonly OperationMqttNotification operationMqttNotification;

        public ConstraintMessageNotificationHandler(ILogger<ConstraintMessageNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            IServiceProvider serviceProvider,
            ViewerMqttNotification viewerMqttNotification,
            FimsTokenProvider fimsAuthentication,
            OperationMqttNotification operationMqttNotification)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.serviceProvider = serviceProvider;
            this.viewerMqttNotification = viewerMqttNotification;
            this.operationMqttNotification = operationMqttNotification;
        }

        protected override async Task HandleCore(AsyncConstraintMessageNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => {
                    SetDependencies();
                    ProcessRequestAsync(request);
                });
        }

        private void SetDependencies()
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            dbFunctions = serviceScope.ServiceProvider.GetService<DatabaseFunctions>();
        }

        private async void ProcessRequestAsync(AsyncConstraintMessageNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  ConstraintMessageNotificationHandler :: UVR MessageId = {0}", request.Uvr.MessageId);
            #endif

            //Code to Send Notification For UVR Creation to all Operators in Grid Cell
            if (request.Operators != null && request.Operators.Any())
            {
                //Filter operators except self
                var operators = request.Operators.Where(x => !x.Uss.Equals(configuration.ClientId)).ToList();

                //Filter Out duplicate operator records in case of multiple grid response.
                var distinctOperators = operators.GroupBy(x => x.Uss).Select(x => x.FirstOrDefault()).ToList();

                foreach (var oprtor in distinctOperators)
                {
                    if (oprtor.AnnouncementLevel == AnnouncementLevelEnum.ALL)
                    {
                        await httpClient.Put(oprtor.UssBaseurl, endpoint + "/" + request.Uvr.MessageId, request.Uvr, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT), oprtor.Uss);
                    }
                }
            }

            //Notify UVR to ANRA Operators
            CheckIntersectedOperationsAndNotify(request);

            #if DEBUG
                logger.LogInformation("ANRA::: END ConstraintMessageNotificationHandler :: UVR MessageId = {0}", request.Uvr.MessageId);
            #endif
        }

        private void CheckIntersectedOperationsAndNotify(AsyncConstraintMessageNotification request)
        {
            //Get Intersected Operations
            intersectedOperations = dbFunctions.GetIntersectingOperations(request.Uvr.MessageId);

            if (intersectedOperations.Any())
            {
                var ops = intersectedOperations.ToList();

                #if DEBUG
                    logger.LogInformation("ANRA::: START :: CheckIntersectedOperationsAndNotify :: {0} Conflicted Operation found",ops.Count);
                #endif

                //Check for Permitted Operations/Gufis, If Any and exclude them
                if (request.Uvr.PermittedGufis != null && request.Uvr.PermittedGufis.Any())
                {
                    ops.RemoveAll(x => request.Uvr.PermittedGufis.Contains(x.Gufi.ToString()));
                }

                //Check for Permitted Uas If Any and exclude them
                if (request.Uvr.PermittedUas != null && request.Uvr.PermittedUas.Any())
                {
                    var uasTypes = request.Uvr.PermittedUas.Select(e => e.ToString()).ToList();

                    ops.RemoveAll(x => uasTypes.Contains(x.FaaRule));
                }

                if (ops.Any())
                {
                    #if DEBUG
                        logger.LogInformation("ANRA::: CheckIntersectedOperationsAndNotify :: UVR Conflict Notifications");
                    #endif

                    ops.ForEach(x =>
                    {

                        var notification = new ConstraintMessageNotification
                        {
                            Gufi = x.Gufi,
                            Message = string.Concat("Close or Replan operation due to uvr in operation area.", (request.Uvr.PermittedGufis != null && request.Uvr.PermittedGufis.Any()) ? string.Format("Permitted Gufis - [{0}]", string.Join(",", request.Uvr.PermittedGufis)) : string.Empty),
                            ConstraintMessage = request.Uvr,
                            Timestamp = DateTime.UtcNow
                        };

                        //Currently Mobile client are also using this topic so we will remove this notification once the mobile makes 
                        //the changes
                        viewerMqttNotification.NotifyDynamicRestriction(notification);

                        //New notification for UVR to viewer
                        viewerMqttNotification.NotificationToViewer(request.MqttMessageType.ToString(), request.Uvr, MqttViewerType.UVR);

                        //Sending notification to the intersected operation
                        operationMqttNotification.NotifyOperation(notification, MqttMessageType.UVR.ToString(), x.Gufi.ToString());

                    });
                }
            }
            else
            {
                //If no intersection with any operation then simply notify the UVR to operators
                viewerMqttNotification.NotifyDynamicRestriction(request.Uvr);

                //New notification for UVR
                viewerMqttNotification.NotificationToViewer(request.MqttMessageType.ToString(), request.Uvr, MqttViewerType.UVR);
            }
        }
    }
}