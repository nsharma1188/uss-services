﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class OperationStatusNotificationHandler : AsyncRequestHandler<AsyncOperationStatusNotification>
    {
        private const string endpoint = "/utm_messages";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<OperationStatusNotificationHandler> logger;

        public OperationStatusNotificationHandler(ILogger<OperationStatusNotificationHandler> logger,
            AnraHttpClient httpClient,
            AnraConfiguration configuration,
            FimsTokenProvider fimsAuthentication)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
        }

        protected override async Task HandleCore(AsyncOperationStatusNotification request)
        {
            if (configuration.IsUtmEnabled)
                await Task.Factory.StartNew(() => ProcessRequest(request));
        }

        private void ProcessRequest(AsyncOperationStatusNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START  OperationStatusNotificationHandler :: Gufi = {0}, Network Nodes:{1}", request.UtmMessage.Gufi, request.Networks.Count());
            #endif

            //Code to Send Notification to all USS networks in LUN
            if (request.Networks.Any())
            {
                foreach (var network in request.Networks)
                {
                    httpClient.Put(network.UssBaseCallbackUrl, endpoint + "/" + request.UtmMessage.MessageId, request.UtmMessage, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE), network.UssName);
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA::: END OperationStatusNotificationHandler :: Gufi = {0}, Network Nodes:{1}", request.UtmMessage.Gufi, request.Networks.Count());
            #endif
        }
    }
}