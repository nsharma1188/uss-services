﻿using AnraUssServices.Utm.Models;
using MediatR;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncOperationProposedNotification : IRequest
    {
        public Operation Operation { get; private set; }

        public AsyncOperationProposedNotification(Operation operation)
        {
            Operation = operation;
        }
    }
}