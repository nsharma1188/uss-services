﻿using AnraUssServices.Utm.Models;
using MediatR;
using System;
using System.Collections.Generic;

namespace AnraUssServices.AsyncHandlers
{
    public class AsyncOperationStatusNotification : IRequest
    {
        public IEnumerable<Models.UtmInstance> Networks { get; private set; }

        public Operation Operation { get; private set; }

        public UTMMessage UtmMessage { get; set; }

        public AsyncOperationStatusNotification(UTMMessage message, IEnumerable<Models.UtmInstance> network)
        {
            UtmMessage= message;
            Networks = network;
        }
    }
}