﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Mapster;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AnraUssServices.AsyncHandlers
{
    public class OperationProposedNotificationHandler : AsyncRequestHandler<AsyncOperationProposedNotification>
    {
        private readonly IMediator mediator;
        private readonly IServiceProvider serviceProvider;
        private OperationStatusLogic operationStatusLogic;
        private IRepository<Models.Operation> operationRepository;
        private IRepository<Models.UtmInstance> utmInstanceRepository;
        private IRepository<OperationConflict> operationConflictRepository;
        private IRepository<Models.NegotiationMessage> negotiationMessageRepository;
		private IRepository<Models.NegotiationAgreement> negotiationAggrementRepository;
        private DatabaseFunctions dbFunctions;
        private const string endpoint = "/utm_messages";
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly ILogger<AsyncOperationProposedNotification> logger;
        private IEnumerable<Guid> conflictedOperations;
        private JSendSlippyResponse slippyResponse;
        private JSendGridCellMetadataResponse metaDataResponse;
        private SlippyTiles slippyTilesLogic;
        private GridCell gridCellLogic;
        private string messageToNotify;
        private readonly NegotiationApi negotiationApi;
		private readonly UtmMessenger utmMessenger;

		public OperationProposedNotificationHandler(ILogger<AsyncOperationProposedNotification> logger,
            AnraHttpClient httpClient,
            IServiceProvider serviceProvider,
            AnraConfiguration configuration,
            IMediator mediator,
            NegotiationApi negotiationApi,
            FimsTokenProvider fimsAuthentication,
			UtmMessenger utmMessenger)
        {
            this.logger = logger;
            this.fimsAuthentication = fimsAuthentication;
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.serviceProvider = serviceProvider;
            this.mediator = mediator;
            this.negotiationApi = negotiationApi;
			this.utmMessenger = utmMessenger;

        }

        private void SetDependencies()
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

            dbFunctions = serviceScope.ServiceProvider.GetService<DatabaseFunctions>();
            operationStatusLogic = serviceScope.ServiceProvider.GetService<OperationStatusLogic>();
            operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();
            operationConflictRepository = serviceScope.ServiceProvider.GetService<IRepository<OperationConflict>>();
            negotiationMessageRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.NegotiationMessage>>();
			negotiationAggrementRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.NegotiationAgreement>>();
			utmInstanceRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.UtmInstance>>();
            slippyTilesLogic = serviceScope.ServiceProvider.GetService<SlippyTiles>();
            gridCellLogic = serviceScope.ServiceProvider.GetService<GridCell>();
            messageToNotify = string.Empty;
        }

        protected override async Task HandleCore(AsyncOperationProposedNotification request)
        {
            await Task.Factory.StartNew(() =>
            {
                SetDependencies();
                ProcessRequest(request);
            });
        }

        private void ProcessRequest(AsyncOperationProposedNotification request)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: START :: OperationProposedNotificationHandler :: Gufi = {0}", request.Operation.Gufi);
            #endif

            //Check for Internal Conflict
            GetConflictingOperations(request.Operation.Gufi);

            if (!conflictedOperations.Any())
            {
                #if DEBUG
                    logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: START InterUssWorkflow :: Gufi = {0}, NO_CONFLICT", request.Operation.Gufi);
                #endif

                slippyResponse = slippyTilesLogic.GetSlippyTiles(configuration.DefaultZoomLevel, GetCSVCoordinateList(request.Operation));

                if (slippyResponse.Data.GridCells.Any())
                {
                    ProcessInterUssWorkflow(request.Operation, slippyResponse.Data.GridCells);
                }
            }
            else
            {
                //Conflict Workflow
                ConflictingOperationWorkflow(request.Operation);
            }
        }

        private void ConflictingOperationWorkflow(Utm.Models.Operation operation)
        {
            //Currently we are only supporting single conflict operation
            var conflictedOperationGufi = conflictedOperations.FirstOrDefault();
            var conflictedOperation = operationRepository.Find(conflictedOperationGufi).Adapt<Utm.Models.Operation>();

            if (operation.IsPrimary(conflictedOperation))
            {
                #if DEBUG
                    logger.LogInformation("Primary Operation");
                #endif

                //If the current operation is primary then the conflicted operation
                HandleSecondaryOperation(operation.Gufi, conflictedOperationGufi, true);

                //If Internal Operation is Primary Then We will write this operation to Grid and InterUssWorkflow will be follow
                #if DEBUG
                    logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: START InterUssWorkflow :: Gufi = {0}, INTERNAL CONFLICT", operation.Gufi);
                #endif

                slippyResponse = slippyTilesLogic.GetSlippyTiles(configuration.DefaultZoomLevel, GetCSVCoordinateList(operation));

                if (slippyResponse.Data.GridCells.Any())
                {
                    ProcessInterUssWorkflow(operation, slippyResponse.Data.GridCells);
                }
            }
            else
            {
                #if DEBUG
                    logger.LogInformation("Secondary Operation");
                #endif

                HandleSecondaryOperation(operation.Gufi, conflictedOperationGufi, false);
            }
        }

        private void HandleSecondaryOperation(Guid operationGufi, Guid conflictedOperationGufi, bool currentOperationPrimary)
        {
            mediator.Send(new AsyncInternalConflictNotification(operationGufi, conflictedOperationGufi.ToString(), currentOperationPrimary));
        }

        private void ProcessInterUssWorkflow(Utm.Models.Operation operation, List<JSendSlippyResponseDataGridCells> gridCells)
        {
            //Higher Priority Operation like PUBLIC SAFETY implementation
            if (operation.PriorityElements.PriorityStatus.Equals(PriortyStatus.PUBLIC_SAFETY))
            {
                //Directly write operation to grid
                WriteOperationToGrid(operation);
            }
            else
            {
                //Other type of operations workflow
                var gridCellMetaData = new List<JSendGridCellMetadataResponse>();

                foreach (var grid in gridCells)
                {
                    var response = gridCellLogic.GridCellOperators(grid.Zoom, grid.X, grid.Y);

                    if (response != null)
                    {
                        gridCellMetaData.Add(response);
                    }
                }

                if (gridCellMetaData.Any())
                {
                    bool IsConflictWorkflow = false;

                    var gridOperators = gridCellMetaData.SelectMany(x => x.Data.Operators).Where(p => !p.Uss.Equals(operation.UssName, StringComparison.InvariantCultureIgnoreCase)).ToList();

                    var operations = gridOperators.SelectMany(p => p.Operations).Where(x => !x.Gufi.Equals(Guid.Empty)).ToList();

                    var conflictingOps = gridCellLogic.GetOperatorConflictingOperations(operation, gridOperators);

                    if (conflictingOps.Any() && conflictingOps.Count > 0)
                    {
                        #if DEBUG
                            logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: InterUss Conflict Workflow :: Operation = {0}, Conflicted Operations = {1}", operation.Gufi, conflictingOps.Count);
                        #endif

                        IsConflictWorkflow = true;

                        WriteOperationToGrid(operation, true);

                        //Save Conflicted Operations
                        SaveOperationConflict(operation, conflictingOps);

                        //Notify Operator(s) about conflict
                        NotifyOperator(operation, conflictingOps);

                        //Start Negotiation Process
                        ProcessNegotiationWorkflow(operation, conflictingOps);
                    }
                    else
                    {
                        WriteOperationToGrid(operation);
                    }
                }
            }
        }

        private void NotifyOperator(Utm.Models.Operation operation, List<KeyValuePair<Utm.Models.Operation, List<ConflictDetail>>> conflictingOps)
        {
            var model = operationRepository.Find(operation.Gufi);

            //Current Mqtt Notification 
            operationStatusLogic.NotifyOperator(model, $"Operation conflicting with another operation. {messageToNotify}");

            //New Mqtt Notification
            operationStatusLogic.NotifyOperation(model, MqttMessageType.CONFLICT.ToString(), $"Operation conflicting with another operation. {messageToNotify}");
        }

        private void ProcessNegotiationWorkflow(Utm.Models.Operation operation, List<KeyValuePair<Utm.Models.Operation, List<ConflictDetail>>> conflictingOps)
        {
			try
			{
                #if DEBUG
                    logger.LogInformation("ANRA ::: OperationProposedNotificationHandler :: ProcessNegotiationWorkflow :: Operation {0}", operation.Gufi.ToString());
                #endif

                var gridCellDetails = gridCellLogic.GetGridCellDetails(slippyResponse.Data.GridCells);

				foreach (var op in conflictingOps)
				{
                    #if DEBUG
                        logger.LogInformation("ANRA ::: OperationProposedNotificationHandler :: ProcessNegotiationWorkflow :: ANRAOperation = {0}, ConflictingOperation = {1}", operation.Gufi.ToString(), op.Key.Gufi.ToString());
                    #endif

                    var opModel = op.Key;

					//Get operators from Grid
                    var operators = gridCellLogic.GetGridCellOperators(JsonConvert.SerializeObject(gridCellDetails.Keys.ToList()));
                    
                    //Fetch target uss for sending Negotiation Utm Message
                    var uss = operators.Where(x => x.Uss.Equals(opModel.UssName)).FirstOrDefault();

                    var operationModel = operationRepository.Find(operation.Gufi);

					//Create New Negotiation Request
					//var message = new Models.NegotiationMessage
					//{
					//	MessageId = Guid.NewGuid(),
					//	NegotiationId = Guid.NewGuid(),
					//	UssNameOfOriginator = operation.UssName,
					//	UssNameOfReceiver = opModel.UssName,
					//	Type = EnumUtils.GetDescription(NegotiationMessageTypeEnum.REPLAN_REQUESTED),
					//	ReplanSuggestion = EnumUtils.GetDescription(NegotiationReplanSuggestionEnum.ALTITUDE_CHANGE),
					//	GufiOfOriginator = operation.Gufi,
					//	GufiOfReceiver = opModel.Gufi,
					//	FreeText = "Please re-plan your operation."
					//};

					//var negotiationAggrement = new Models.NegotiationAgreement
					//{
					//	GufiOriginator = message.GufiOfOriginator,
					//	GufiReceiver = message.GufiOfReceiver,
					//	MessageId = Guid.NewGuid(),
					//	NegotiationId = message.NegotiationId,
					//	Type = EnumUtils.GetDescription(NegotiationAgreementTypeEnum.REPLAN),
					//	UssNameOfOriginator = message.UssNameOfOriginator,
					//	UssNameOfReceiver = message.UssNameOfReceiver,
					//	OperationId = operationModel.OperationId,
					//	FreeText = "Replan has been initiated"
					//};

					//Creating negotation message for Intersection only for now
					var message = new Models.NegotiationMessage
					{
						MessageId = Guid.NewGuid(),
						NegotiationId = Guid.NewGuid(),
						UssNameOfOriginator = operation.UssName,
						UssNameOfReceiver = opModel.UssName,
						Type = EnumUtils.GetDescription(NegotiationMessageTypeEnum.INTERSECTION_REQUESTED),
						ReplanSuggestion = null,
						GufiOfOriginator = operation.Gufi,
						GufiOfReceiver = opModel.Gufi,
						FreeText = "Intersection requested",
						V2vForOriginator = true,
						V2vForReceiver = true
					};


					var negotiationAggrement = new Models.NegotiationAgreement
					{
						GufiOriginator = message.GufiOfOriginator,
						GufiReceiver = message.GufiOfReceiver,
						MessageId = Guid.NewGuid(),
						NegotiationId = message.NegotiationId,
						Type = EnumUtils.GetDescription(NegotiationAgreementTypeEnum.INTERSECTION),
						UssNameOfOriginator = message.UssNameOfOriginator,
						UssNameOfReceiver = message.UssNameOfReceiver,
						Gufi = operationModel.Gufi,
						FreeText = "Intersection has been initiated"
					};

					//Save Negotiation Message
					negotiationMessageRepository.Add(message);

					//Save Negotiation Aggrement
					//negotiationAggrementRepository.Add(negotiationAggrement);
					SaveNegotiationAgreement(negotiationAggrement);

					//Send Negotiation Message
					var response = negotiationApi.PostNegotiationMessage(uss.UssBaseurl, message.Adapt<Utm.Models.NegotiationMessage>(), uss.Uss);

					if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.NoContent))
					{
                        #if DEBUG
                            logger.LogInformation("ANRA:::OperationProposedNotificationHandler :: 204 ProcessNegotiationWorkflow :: INTERSECTION_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response.HttpStatusCode.Value);
                        #endif

                        //TO DO: Changes done because of new Ready For ATC workflow introduce

                        if(this.configuration.ATCApprovalRequired)
                        {
                            //Set Ready For ATC state
                            operationStatusLogic.ToReadyForAtcAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                        }
                        else
                        {
                            operationStatusLogic.ToAcceptedAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                        }
                    }
                    else if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.Conflict))
					{
                        #if DEBUG
                            logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 409 ProcessNegotiationWorkflow :: INTERSECTION_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response.HttpStatusCode.Value);
                        #endif
                        var res = operationStatusLogic.ToClosedAsync(operation.Gufi).Result;
					}
					else
					{
                        #if DEBUG
                            logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: Unhandled scenario :: Operation = {0}, Response: {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response);
                        #endif
                    }

					//Commented the rest of the flow for now.

					//If Negotiation Accepted i.e. 204 Then Update Operation State to ACCEPTED & Write to Grid
					//If Negotiation Rejected i.e. 409 Then Update Operation State to CLOSED & Delete Operation from Grid

					//Need to re-address this workflow later. For now if the replan-requested is rejected then send a new message with INTERSECTION_REQUESTED is send
					// If the INTERSECTION_REQUESTED is rejected then update operation state to CLOSED and Delete operation from Grid.
					//if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.NoContent))
					//{
					//	logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 204 ProcessNegotiationWorkflow :: REPLAN_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response.HttpStatusCode.Value);
					//	operationStatusLogic.ToAcceptedAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);

					//	//Save the negotation in the LiteDb and also send that to NASA
					//	negotiationHandler.UssNegotiationReport(message.Adapt<Utm.Models.NegotiationMessage>(), operation);
					//}
					//else if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.Conflict))
					//{
					//logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 409 ProcessNegotiationWorkflow :: REPLAN_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response.HttpStatusCode.Value);
					////var res = operationStatusLogic.ToClosedAsync(operation.Gufi).Result;

					////Update Negotiation message for INTERSECTION_REQUESTED

					//message.MessageId = Guid.NewGuid();
					//message.V2vForOriginator = true;
					//message.V2vForReceiver = true;
					//message.Type = EnumUtils.GetDescription(NegotiationMessageTypeEnum.INTERSECTION_REQUESTED);
					//message.ReplanSuggestion = null;
					//message.FreeText = "Intersection requested";


					////Update Negotiation Aggrement for INTERSECTION
					//negotiationAggrement.MessageId = Guid.NewGuid();
					//negotiationAggrement.Type = EnumUtils.GetDescription(NegotiationAgreementTypeEnum.INTERSECTION);
					//negotiationAggrement.FreeText = "Intersection has been initiated";


					////Update the Negotiation Message table for the case of Intersection Requested
					//negotiationMessageRepository.Update(message);

					////Update Negotiation Aggrement table for the case of Intersection Requested
					////negotiationAggrementRepository.Update(negotiationAggrement);
					//SaveNegotiationAgreement(negotiationAggrement);

					//var newResponse = negotiationApi.PostNegotiationMessage(uss.UssBaseurl, message.Adapt<Utm.Models.NegotiationMessage>(), uss.Uss);

					//if (newResponse.HttpStatusCode.Value.Equals((int)HttpStatusCode.NoContent))
					//{
					//	logger.LogInformation("ANRA:::OperationProposedNotificationHandler :: 204 ProcessNegotiationWorkflow :: INTERSECTION_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), newResponse.HttpStatusCode.Value);
					//	operationStatusLogic.ToAcceptedAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);

					//	//Save the negotation in the LiteDb and also send that to NASA
					//	negotiationHandler.UssNegotiationReport(message.Adapt<Utm.Models.NegotiationMessage>(), operation);
					//}
					//else if (newResponse.HttpStatusCode.Value.Equals((int)HttpStatusCode.Conflict))
					//{
					//	logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 409 ProcessNegotiationWorkflow :: INTERSECTION_REQUESTED : Operation = {0}, Response = {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), newResponse.HttpStatusCode.Value);
					//	var res = operationStatusLogic.ToClosedAsync(operation.Gufi).Result;
					//}
					//}
					//else
					//{
					//	logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: Unhandled scenario :: Operation = {0}, Response: {1}", JsonConvert.SerializeObject(operation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }), response);
					//}
				}
			}
			catch (Exception ex)
			{
				logger.LogError("Exception ::: OperationProposedNotificationHandler:: ProcessNegotiationWorkflow :: Exception = {0}", ex.Message);
			}
        }

        private void WriteOperationToGrid(Utm.Models.Operation operation,bool IsConflictWorkflow = false)
        {
            var gridCellDetails = gridCellLogic.GetGridCellDetails(slippyResponse.Data.GridCells);

            var response = gridCellLogic.WriteOperation(operation, gridCellDetails).Value.Adapt<JSendGridCellMetadataResponse>();

            if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.OK))
            {
                if (IsConflictWorkflow)
                {
                    #if DEBUG
                        logger.LogInformation("ANRA ::: OperationProposedNotificationHandler :: Write Proposed OperationToGrid :: ConflictWorkflow");
                    #endif

                    SaveProposedOperation(operation, gridCellDetails.Keys.ToList());

					//During the case of conflict we will notify the proposed operation to others in LUN
					var model = operationRepository.Find(operation.Gufi);
					utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

				}
                else
                {
                    //TO DO: Changes done because of new Ready For ATC workflow introduce

                    if (this.configuration.ATCApprovalRequired)
                    {
                        //Set Ready For ATC state
                        operationStatusLogic.ToReadyForAtcAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                    }
                    else
                    {
                        operationStatusLogic.ToAcceptedAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                    }
                }
            }
            else if (response.HttpStatusCode.Value.Equals((int)HttpStatusCode.Conflict))
            {
                if (operation.PriorityElements.PriorityStatus.Equals(PriortyStatus.PUBLIC_SAFETY))
                {
                    #if DEBUG
                        //If 409 comes then mark the operation accepted
                        //Workflow yet not clear for this use case
                        logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 409 PUBLIC SAFETY OPERATION :: Operation = {0}", operation);
                    #endif

                    //TO DO: Changes done because of new Ready For ATC workflow introduce

                    if (this.configuration.ATCApprovalRequired)
                    {
                        //Set Ready For ATC state
                        operationStatusLogic.ToReadyForAtcAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                    }
                    else
                    {
                        operationStatusLogic.ToAcceptedAsync(operation.Gufi, gridCellDetails.Keys.ToList()).ConfigureAwait(false);
                    }
                }
                else
                {
                    #if DEBUG
                        logger.LogInformation("ANRA:::OperationProposedNotificationHandler:: 409 Conflict Workflow :: Operation = {0}", operation);
                    #endif
                    ProcessInterUssWorkflow(operation, gridCellDetails.Keys.ToList());
                }
            }
            else
            {
                #if DEBUG
                    logger.LogInformation("ANRA ::: OperationProposedNotificationHandler:: Unhandled scenario :: Operation = {0}, Response: {1}", operation, response);
                #endif
            }
        }

        private void SaveProposedOperation(Utm.Models.Operation operation, List<JSendSlippyResponseDataGridCells> gridData)
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

            var opContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
            var opsModel = operationRepository.Find(operation.Gufi);            
            opsModel.DecisionTime = DateTime.UtcNow;
            opsModel.SlippyTileData = JsonConvert.SerializeObject(gridData);

            opContext.Operations.Update(opsModel);
            opContext.SaveChanges();
        }

        private void SaveNegotiationAgreement(Models.NegotiationAgreement negotiationAgreement)
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

            var naContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
            naContext.NegotiationAgreements.Update(negotiationAgreement);
            naContext.SaveChanges();
        }

        private void GetConflictingOperations(Guid gufi)
        {
            conflictedOperations = dbFunctions.GetConflictingOperations(gufi).OrderByDescending(p => p.SubmitTime).Select(p => p.Gufi);
        }

        private string GetCSVCoordinateList(Utm.Models.Operation operation)
        {
            string csvList = string.Empty;

            operation.OperationVolumes.ForEach(opv =>
            {
                opv.OperationGeography.Coordinates[0].ForEach(x =>
                {
                    csvList = csvList + string.Format("{0},{1},", x[1], x[0]);
                });
            });

            return csvList.Substring(0, csvList.Length - 1);
        }

        private void SaveOperationConflict(Utm.Models.Operation operation, List<KeyValuePair<Utm.Models.Operation, List<ConflictDetail>>> conflictingOps)
        {
            var plannedOps = operationRepository.Find(operation.Gufi);

            foreach (var op in conflictingOps)
            {
                var opModel = op.Key;
                var conflictDetails = op.Value;


                //Build Message To Notify
                BuildMessageToNotify(opModel);

                operationConflictRepository.Add(new OperationConflict
                {
                    //OperationId = plannedOps.OperationId,
                    Gufi = plannedOps.Gufi,
                    ConflictingGufi = opModel.Gufi,
                    ConflictingUssName = opModel.UssName,
                    ConflictDetails = JsonConvert.SerializeObject(conflictDetails),
                    TimeStamp = DateTime.UtcNow,
                    IsAnraPrimary = false
                });
            }
        }

        private void BuildMessageToNotify(Utm.Models.Operation operation)
        {
            messageToNotify += $"Gufi:{operation.Gufi}::" + Environment.NewLine;
            var builder = new System.Text.StringBuilder();
            builder.Append(messageToNotify);

            foreach (var opsVol in operation.OperationVolumes)
            {
                builder.Append($"Vol-{opsVol.Ordinal}:: Min Alt.:{opsVol.MinAltitude.AltitudeValue},Max Alt.:{opsVol.MaxAltitude.AltitudeValue},Begin Time:{opsVol.EffectiveTimeBegin.Value.ToUniversalTime()},End Time:{opsVol.EffectiveTimeEnd.Value.ToUniversalTime()}" + Environment.NewLine);
            }
            messageToNotify = builder.ToString();
        }

        //private void BuildMessageToNotify(Utm.Models.Operation operation,List<ConflictDetail> conflictDetails)
        //{
        //    messageToNotify += $"Gufi:{operation.Gufi}::" + Environment.NewLine;
        //    var builder = new System.Text.StringBuilder();
        //    builder.Append(messageToNotify);

        //    if (conflictDetails.Any())
        //    {
        //        builder.Append("Conflict Details :: ");

        //        conflictDetails.ForEach(x => {
        //            builder.Append(BuildConflictReasonDetails(x) + Environment.NewLine);
        //        });
        //    }

        //    messageToNotify = builder.ToString();
        //}

        //private string BuildConflictReasonDetails(ConflictDetail reason)
        //{
        //    var builder = new System.Text.StringBuilder();

        //    if (reason != null && reason.IsIntersectingIn4D)
        //    {
        //        builder.Append($" Ordinal(s) are: Source Ordinal-{reason.SourceOrdinal}:: Target Ordinal-{reason.TargetOrdinal}");

        //        return builder.ToString();
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
        //}
    }
}