using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// allowableValues for type are Point, LineString, Polygon and MultiPolygon. Since UTM supports most but not all Geojson datatypes, UTM defines a a portion (or fragment) of the Geojson spec.For most cases, “Polygon” will be more appropriate for describing an operation. For trajectory-based operations, LineString will be more appropriate. Any value other than LineString or Polygon will cause rejection of a submitted plan. The only accepted value for a position is Point. Any value other than Point will cause rejection of a position submission.
    /// </summary>
    [DataContract]
    public class GeographyFragment
    {
        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GeographyFragment {\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}