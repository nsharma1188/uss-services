using AnraUssServices.Common;
using AnraUssServices.Common.Converters;
using AnraUssServices.Common.Validators;
using LiteDB;
using Nest;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Position
    {
        /// <summary>
        /// Each position will be assigned a UUIDv4 by the USS
        /// </summary>
        /// <value>Each position will be assigned a UUIDv4 by the USS</value>
        [BsonId]
        [DataMember(Name = "enroute_positions_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "enroute_positions_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "enroute_positions_id is not of the correct UUID version")]
        public Guid EnroutePositionsId { get; set; }

        /// <summary>
        /// The altitude as measured via a GPS device on the aircraft. Units in feet using the WGS84 reference system.
        /// </summary>
        /// <value>The altitude as measured via a GPS device on the aircraft. Units in feet using the WGS84 reference system.</value>
        [DataMember(Name = "altitude_gps", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude_gps")]
        [Required]        
        public Altitude AltitudeGps { get; set; }

        /// <summary>
        /// Number of satellites used in calculating the altitude_gps_wgs84_ft.
        /// </summary>
        /// <value>Number of satellites used in calculating the altitude_gps_wgs84_ft.</value>
        [DataMember(Name = "altitude_num_gps_satellites", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude_num_gps_satellites")]
        [Required]
        [Range(0, int.MaxValue)]
		[JsonConverter(typeof(CustomIntConverter))]
		public int? AltitudeNumGpsSatellites { get; set; }        

        /// <summary>
        /// Each operation has an GUFI assigned upon submission. Required upon POSTing a new position. It is a JSON string, but conforms to the UUID version 4 specification
        /// </summary>
        /// <value>Each operation has an GUFI assigned upon submission. Required upon POSTing a new position. It is a JSON string, but conforms to the UUID version 4 specification</value>
        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi is not of the correct UUID version")]
        public Guid Gufi { get; set; }

        /// <summary>
        /// The horizontal dilution of precision as provided by the onboard GPS.
        /// </summary>
        /// <value>The horizontal dilution of precision as provided by the onboard GPS.</value>
        [DataMember(Name = "hdop_gps", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "hdop_gps")]
        [Required]
        [Range(0, double.MaxValue)]
        public double? HdopGps { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        [DataMember(Name = "location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "location")]
        [Required]
        [PointValidator]
        public Point Location { get; set; }

        /// <summary>
        /// The time the position was measured. Likely the time provided with the GPS position reading.  Time must be before time received at FIMS based on FIMS system time.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time the position was measured. Likely the time provided with the GPS position reading.  Time must be before time received at FIMS based on FIMS system time.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "time_measured", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_measured")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeMeasured { get; set; }

        /// <summary>
        /// The time the position was sent by the USS to FIMS as measured by USS system time.  Time must be before time received at FIMS based on FIMS system time.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time the position was sent by the USS to FIMS as measured by USS system time.  Time must be before time received at FIMS based on FIMS system time.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "time_sent", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_sent")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeSent { get; set; }

        /// <summary>
        /// The direction of travel relative to track_bearing_reference in degrees. Value must be &gt;&#x3D; 0.0 and &lt; 360.0.  Note that FIXM allows for the value of 360.0, while UTM does not.  The reason is for clarity by not allowing two numbers (0.0 and 360.0) to represent the same measurement.
        /// </summary>
        /// <value>The direction of travel relative to track_bearing_reference in degrees. Value must be &gt;&#x3D; 0.0 and &lt; 360.0.  Note that FIXM allows for the value of 360.0, while UTM does not.  The reason is for clarity by not allowing two numbers (0.0 and 360.0) to represent the same measurement.</value>
        [DataMember(Name = "track_bearing", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "track_bearing")]
        [Required]
        [RangeValidator(0, 360, "track_bearing value must be greater or equal to 0.0 and less than 360.0")]
        public double? TrackBearing { get; set; }

        /// <summary>
        /// A code indicating the direction of the zero bearing. TRUE_NORTH  This value indicates that the direction is relative to True North. MAGNETIC_NORTH  This value indicates that the direction is relative to Magnetic North. This field is equivalent to
        /// </summary>
        /// <value>A code indicating the direction of the zero bearing. TRUE_NORTH  This value indicates that the direction is relative to True North. MAGNETIC_NORTH  This value indicates that the direction is relative to Magnetic North. This field is equivalent to</value>        
        [DataMember(Name = "track_bearing_reference", EmitDefaultValue = false)]
        [EnumDataType(typeof(TrackBearingReferenceEnum))]
        [JsonProperty(PropertyName = "track_bearing_reference")]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public TrackBearingReferenceEnum TrackBearingReference { get; set; }

        /// <summary>
        /// The reference quantities used to express the value of angles. [ISO 19103, chapter 6.5.7.10] As in FIXM, only a single option is provided (degrees). Including this field allows for clarity in the data provided and allows for the potential of other units of measure in the future.
        /// </summary>
        /// <value>The reference quantities used to express the value of angles. [ISO 19103, chapter 6.5.7.10] As in FIXM, only a single option is provided (degrees). Including this field allows for clarity in the data provided and allows for the potential of other units of measure in the future.</value>
        [DataMember(Name = "track_bearing_uom", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "track_bearing_uom")]
        [EnumDataType(typeof(TrackBearingUomEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public TrackBearingUomEnum TrackBearingUom { get; set; }

        /// <summary>
        /// Ground speed int the direction of travel. Value must be >= 0.0. In knots.
        /// </summary>
        /// <value>Ground speed int the direction of travel. Value must be >= 0.0. In knots.</value>
        [DataMember(Name = "track_ground_speed", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "track_ground_speed")]
        [Required]
        [Range(0, double.MaxValue)]
        public double? TrackGroundSpeed { get; set; }

        /// <summary>
        /// The reference quantities used to express the value of angles. [ISO 19103, chapter 6.5.7.10] As in FIXM, only a single option is provided (degrees). Including this field allows for clarity in the data provided and allows for the potential of other units of measure in the future.
        /// </summary>
        /// <value>The reference quantities used to express the value of angles. [ISO 19103, chapter 6.5.7.10] As in FIXM, only a single option is provided (degrees). Including this field allows for clarity in the data provided and allows for the potential of other units of measure in the future.</value>
        [DataMember(Name = "track_ground_speed_units", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "track_ground_speed_units")]
        [EnumDataType(typeof(UomGroundSpeedTypeEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public UomGroundSpeedTypeEnum TrackGroundSpeedUnits { get; set; }

        /// <summary>
        /// The name of the entity providing UAS Support Services. Populated by the service discovery system based on credential information provided by the USS. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>The name of the entity providing UAS Support Services. Populated by the service discovery system based on credential information provided by the USS. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [Required]
        [MinLength(1), MaxLength(250)]
        public string UssName { get; set; }

        /// <summary>
        /// An identifier that supplies information about the USS from the discovery perspecitve.  This field is currently vague due to research on the appropriate discovery approach.  It will be tightened when an operational system decides on the approach to discovery. For Gridded USS Discovery, this should be an x,y,z string where x and y are the coords of the grid and z is the zoom level.  For example:  \&quot;110,117,10\&quot; For FIMS Discovery, this should be the uss_instance_id which is a UUID.
        /// </summary>
        /// <value>An identifier that supplies information about the USS from the discovery perspecitve.  This field is currently vague due to research on the appropriate discovery approach.  It will be tightened when an operational system decides on the approach to discovery. For Gridded USS Discovery, this should be an x,y,z string where x and y are the coords of the grid and z is the zoom level.  For example:  \&quot;110,117,10\&quot; For FIMS Discovery, this should be the uss_instance_id which is a UUID.</value>
        [DataMember(Name = "discovery_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "discovery_reference")]        
        [MinLength(5), MaxLength(36)]
        public string DiscoveryReference { get; set; }

        /// <summary>
        /// The vertical dilultion of precision as provided by the onboard GPS.
        /// </summary>
        /// <value>The vertical dilultion of precision as provided by the onboard GPS.</value>
        [DataMember(Name = "vdop_gps", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vdop_gps")]
        [Required]
        [Range(0, double.MaxValue)]
        public double? VdopGps { get; set; }

        /// <summary>
        /// Comments
        /// </summary>        
        [DataMember(Name = "comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "comments")]
        [MinLength(0),MaxLength(1000)]
        public string Comments { get; set; }

    }
}