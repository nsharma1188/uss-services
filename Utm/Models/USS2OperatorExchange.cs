﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AnraUssServices.Utm.Models
{
	[DataContract]
	public class USS2OperatorExchange
	{
		/// <summary>
		/// A UUID assigned by the reporting USS for this instance of USS2OperatorExchange.
		/// </summary>
		[DataMember(Name = "uss_operator_exchange_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uss_operator_exchange_id")]
		[Required]
		[RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "uss_operator_exchange_id is not of the correct UUID version")]
		[StringLength(36)]
		public Guid uss_operator_exchange_id { get; set; }

		/// <summary>
		/// GUFI of the operation managed by reporting USS involved in this exchange.
		/// </summary>
		[DataMember(Name = "gufi", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "gufi")]
		[Required]
		[RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$")]
		[StringLength(36)]
		public Guid? gufi { get; set; }

		/// <summary>
		/// The reporting USSs ID as known by USS Network/FIMS.
		/// </summary>
		[DataMember(Name = "uss_name", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uss_name")]
		[Required]
		public string uss_name { get; set; }


		/// <summary>
		/// Example: Super Operator, Inc.
		/// </summary>
		[DataMember(Name = "operator", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "operator")]
		[Required]
		public string operators { get; set; }

		/// <summary>
		/// Number of position reports received from the operator to the USS. While USSs are required to be able to report at 1Hz to the USS Network, it is possible that the USS receives position reports a higher frequency.
		/// </summary>
		[DataMember(Name = "number_position_reports_received", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "number_position_reports_received")]
		public int number_position_reports_received { get; set; }

		/// <summary>
		/// Count of all messages received from the operator. This value should NOT contain position reports. It might contain operation plan information, state information about the operation, or other messages as requried or requested by the USS for the support of this operation.
		/// </summary>
		[DataMember(Name = "number_messages_from_operator_received", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "number_messages_from_operator_received")]
		public int number_messages_from_operator_received { get; set; }

		/// <summary>
		/// Count of all messages sent to the operator from the USS. This may include messages related to new constraints, information about nearby operations, approval messages, etc.
		/// </summary>
		[DataMember(Name = "number_messages_from_uss_sent", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "number_messages_from_uss_sent")]
		public int number_messages_from_uss_sent { get; set; }

		/// <summary>
		/// Were there any human interactions between individuals managing the USS and individuals involved in operating the vehicle? If ‘true’ then the ‘notes’ field is mandatory to provide some insight into this interaction.
		/// </summary>
		[DataMember(Name = "human_interactions", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "human_interactions")]
		[Required]
		public bool human_interactions { get; set; }

		/// <summary>
		/// For the duration of this operation was there continuous availability of USS services? The discontinuity or unavailability of services may have been planned or unplanned. If this field has a value of ‘true’ then the ‘notes’ field is mandatory to provide some insight into this interaction.
		/// </summary>
		[DataMember(Name = "continuous_uss_service", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "continuous_uss_service")]
		public bool continuous_uss_service { get; set; }

		/// <summary>
		/// Any additional notes to add context about the interaction between the USS and this operator.
		/// </summary>
		[DataMember(Name = "notes", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "notes")]
		public string notes { get; set; }
	}
}
