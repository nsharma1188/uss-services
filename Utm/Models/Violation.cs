using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Violation
    {
        /// <summary>
        /// Unique identifier for this constraint.
        /// </summary>
        /// <value>Unique identifier for this constraint.</value>
        [DataMember(Name = "constraining_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "constraining_id")]
        public string ConstrainingId { get; set; }

        /// <summary>
        /// (For backward compatibility only, see constraining_id
        /// </summary>
        /// <value>(For backward compatibility only, see constraining_id</value>
        [DataMember(Name = "constraining_ids", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "constraining_ids")]
        public List<string> ConstrainingIds { get; set; }

        /// <summary>
        /// The ordinal of the volume that conflicts with another operation, eg, the first volume of an existing flight acts as a constraint against proposed flights
        /// </summary>
        /// <value>The ordinal of the volume that conflicts with another operation, eg, the first volume of an existing flight acts as a constraint against proposed flights</value>
        [DataMember(Name = "constraining_volume", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "constraining_volume")]
        public int? ConstrainingVolume { get; set; }

        /// <summary>
        /// A report that a problem occurred thus Violation could not be determined.
        /// </summary>
        /// <value>A report that a problem occurred thus Violation could not be determined.</value>
        [DataMember(Name = "problemReport", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "problemReport")]
        public string ProblemReport { get; set; }

        /// <summary>
        /// The constraint type. eg, This is a gufi for OperationViolation (constraint is an existing flight), or an ID of a national park
        /// </summary>
        /// <value>The constraint type. eg, This is a gufi for OperationViolation (constraint is an existing flight), or an ID of a national park</value>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Set for OperationViolations, VolumeViolations, RogueViolations. The ordinal of the volume that is violating this constraint, eg, the proposed flight's second volume violated this constraint
        /// </summary>
        /// <value>Set for OperationViolations, VolumeViolations, RogueViolations. The ordinal of the volume that is violating this constraint, eg, the proposed flight's second volume violated this constraint</value>
        [DataMember(Name = "violating_volume", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "violating_volume")]
        public int? ViolatingVolume { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Violation {\n");
            sb.Append("  ConstrainingId: ").Append(ConstrainingId).Append("\n");
            sb.Append("  ConstrainingIds: ").Append(ConstrainingIds).Append("\n");
            sb.Append("  ConstrainingVolume: ").Append(ConstrainingVolume).Append("\n");
            sb.Append("  ProblemReport: ").Append(ProblemReport).Append("\n");
            sb.Append("  Type: ").Append(Type).Append("\n");
            sb.Append("  ViolatingVolume: ").Append(ViolatingVolume).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}