using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using AnraUssServices.Common;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GridCellOperatorResponse
    { 
        /// <summary>
        /// USS ID as pulled from the access token provided by OAuth.
        /// </summary>
        /// <value>USS ID as pulled from the access token provided by OAuth.</value>
        [Required]
        [DataMember(Name="uss")]
        public string Uss { get; set; }

        /// <summary>
        /// Base URL for the USS&#39;s web service endpoints hosting the required NASA API (https://app.swaggerhub.com/apis/utm/uss/).
        /// </summary>
        /// <value>Base URL for the USS&#39;s web service endpoints hosting the required NASA API (https://app.swaggerhub.com/apis/utm/uss/).</value>
        [Required]
        [DataMember(Name="uss_baseurl")]
        public string UssBaseurl { get; set; }

        /// <summary>
        /// Automatically managed version number of this USSs last update.
        /// </summary>
        /// <value>Automatically managed version number of this USSs last update.</value>
        [Required]
        [DataMember(Name="version")]
        public int? Version { get; set; }

        /// <summary>
        /// Forcing ms resolution and UTC only.
        /// </summary>
        /// <value>Forcing ms resolution and UTC only.</value>
        [Required]
        [DataMember(Name="timestamp")]
        public string Timestamp { get; set; }

        /// <summary>
        /// Earliest operation start time in this grid cell.
        /// </summary>
        /// <value>Earliest operation start time in this grid cell.</value>
        [Required]
        [DataMember(Name="minimum_operation_timestamp")]
        public DateTime MinimumOperationTimestamp { get; set; }

        /// <summary>
        /// Latest operation end time in this grid cell.
        /// </summary>
        /// <value>Latest operation end time in this grid cell.</value>
        [Required]
        [DataMember(Name="maximum_operation_timestamp")]
        public DateTime MaximumOperationTimestamp { get; set; }
        
        /// <summary>
        /// The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.
        /// </summary>
        /// <value>The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.</value>
        [Required]
        [DataMember(Name="announcement_level")]
        public AnnouncementLevelEnum? AnnouncementLevel { get; set; }

        /// <summary>
        /// Gets or Sets Zoom
        /// </summary>
        [Required]
        [DataMember(Name = "zoom")]
        public int Zoom { get; set; }

        /// <summary>
        /// Gets or Sets X
        /// </summary>
        [Required]
        [DataMember(Name = "x")]
        public int X { get; set; }

        /// <summary>
        /// Gets or Sets Y
        /// </summary>
        [Required]
        [DataMember(Name = "y")]
        public int Y { get; set; }

        /// <summary>
        /// Gets or Sets Operations
        /// </summary>
        [Required]
        [DataMember(Name="operations")]
        public List<GridCellOperationResponse> Operations { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GridCellOperatorResponse {\n");
            sb.Append("  Uss: ").Append(Uss).Append("\n");
            sb.Append("  UssBaseurl: ").Append(UssBaseurl).Append("\n");
            sb.Append("  Version: ").Append(Version).Append("\n");
            sb.Append("  Timestamp: ").Append(Timestamp).Append("\n");
            sb.Append("  MinimumOperationTimestamp: ").Append(MinimumOperationTimestamp).Append("\n");
            sb.Append("  MaximumOperationTimestamp: ").Append(MaximumOperationTimestamp).Append("\n");
            sb.Append("  AnnouncementLevel: ").Append(AnnouncementLevel).Append("\n");
            sb.Append("  Operations: ").Append(Operations).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
