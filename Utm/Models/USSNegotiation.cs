﻿using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class USSNegotiation
    {
        [DataMember(Name = "negotiation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "negotiation_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "negotiation_id is not of the correct UUID version")]        
        public Guid NegotiationId { get; set; }

		[DataMember(Name = "Uss_Name", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "Uss_Name")]
		[Required]
		public string Uss_Name { get; set; }

		[DataMember(Name = "reporter_gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reporter_gufi")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "reporter_gufi is not of the correct UUID version")]
        public Guid ReporterGufi { get; set; }

        [DataMember(Name = "other_gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "other_gufi")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "other_gufi is not of the correct UUID version")]
        public Guid OtherGufi { get; set; }

        [DataMember(Name = "message_ids", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_ids")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "message_id is not of the correct UUID version")]
        public List<Guid> MessageIds { get; set; }

        [DataMember(Name = "conflict_resolved", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "conflict_resolved")]
        [Required]
        public bool ConflictResolved { get; set; }

        [DataMember(Name = "reporter_op_action", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reporter_op_action")]
        [EnumDataType(typeof(NegotiationReporterOpActionEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public NegotiationReporterOpActionEnum ReporterOpAction { get; set; }

        [DataMember(Name = "other_op_action", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "other_op_action")]
        [EnumDataType(typeof(NegotiationReporterOpActionEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public NegotiationReporterOpActionEnum OtherOpAction { get; set; }

        [DataMember(Name = "notes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "notes")]
        [StringLength(1500)]
        public string FreeText { get; set; }
    }
}