using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// GeoJSon Polygon. Positions all 2D.
    /// </summary>
    [DataContract]
    public class Polygon
    {
        /// <summary>
        /// The type of Geometry. In this case, must be 'Polygon' per GeoJSON spec.
        /// </summary>
        /// <value>The type of Geometry. In this case, must be 'Polygon' per GeoJSON spec. </value>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets Coordinates
        /// </summary>
        [DataMember(Name = "coordinates", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "coordinates")]
        public List<List<List<double?>>> Coordinates { get; set; }
    }
}