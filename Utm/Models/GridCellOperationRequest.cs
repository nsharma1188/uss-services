using System;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GridCellOperationRequest
    { 
        /// <summary>
        /// GUFI for the Operation described this GridCellOperation.
        /// </summary>
        /// <value>GUFI for the Operation described this GridCellOperation.</value>
        [Required]
        [DataMember(Name="gufi")]
        public Guid Gufi { get; set; }

        /// <summary>
        /// The JWS signature of the Operation described by this GridCellOperation. The JWS signature is base64 URL encoded per specification.  The unencoded length of a SHA-256 signature is 32 bytes and the unencoded length of other potential hash functions can be much longer. The min and max should take into consideration all potential base64url encoding of the JWS algorithms listed in the RFC: https://tools.ietf.org/html/rfc7518#section-3 Currently, these values do not take this into consideration.
        /// </summary>
        /// <value>The JWS signature of the Operation described by this GridCellOperation. The JWS signature is base64 URL encoded per specification.  The unencoded length of a SHA-256 signature is 32 bytes and the unencoded length of other potential hash functions can be much longer. The min and max should take into consideration all potential base64url encoding of the JWS algorithms listed in the RFC: https://tools.ietf.org/html/rfc7518#section-3 Currently, these values do not take this into consideration.</value>
        [Required]
        [DataMember(Name="operation_signature")]
        public string OperationSignature { get; set; }

        /// <summary>
        /// Start time of operation.
        /// </summary>
        /// <value>Start time of operation.</value>
        [Required]
        [DataMember(Name="effective_time_begin")]
        public DateTime EffectiveTimeBegin { get; set; }

        /// <summary>
        /// End time of operation.
        /// </summary>
        /// <value>End time of operation.</value>
        [Required]
        [DataMember(Name="effective_time_end")]
        public DateTime EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GridCellOperationRequest {\n");
            sb.Append("  Gufi: ").Append(Gufi).Append("\n");
            sb.Append("  OperationSignature: ").Append(OperationSignature).Append("\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  EffectiveTimeEnd: ").Append(EffectiveTimeEnd).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
