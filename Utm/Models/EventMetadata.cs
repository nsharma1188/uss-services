  using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class EventMetadata
    {
        /// <summary>
        /// If true these data are intended for Data Collection. Essentially stating if particular data should be ignored during analysis. This may be modified after submission in the case that there was an issue during execution of the test/experiment that would invalidate the data that were collected.
        /// </summary>
        /// <value>If true these data are intended for Data Collection. Essentially stating if particular data should be ignored during analysis. This may be modified after submission in the case that there was an issue during execution of the test/experiment that would invalidate the data that were collected.</value>
        [DataMember(Name = "data_collection", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "data_collection")]
        [Required]        
        public bool DataCollection { get; set; }

        /// <summary>
        /// A string tag indicating which event this operation is associated. Will be used to select a set of rules for validation.
        /// </summary>
        /// <value>A string tag indicating which event this operation is associated. Will be used to select a set of rules for validation.</value>
        [DataMember(Name = "event_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "event_id")]
        [Required]
        [MinLength(1), MaxLength(100)]
        public string EventId { get; set; }

        /// <summary>
        /// Scenario
        /// </summary>
        /// <value>Scenario</value>
        [DataMember(Name = "scenario", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "scenario")]
        [Required]
        [MinLength(1), MaxLength(100)]
        public string Scenario { get; set; }

        /// <summary>
        /// The name or number of the test card.
        /// </summary>
        /// <value>The name or number of the test card.</value>
        [DataMember(Name = "test_card", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "test_card")]
        [Required]
        [MinLength(1), MaxLength(100)]
        public string TestCard { get; set; }

        /// <summary>
        /// Specific tests ids supported by these data. These ids should map to specific tests as outlined in the overall event description. As an example, Event \"TCL3_Demonstration\" at a test site may have a test_card called \"CARD_44\". The operation associated with this metadata block may support \"DAT2\", \"CNS1\", and \"CON4\" specific tests. If any one of those elements are missing (event_id, test_card, test_identifiers), then data analysis may become difficult if not impossible.
        /// </summary>
        /// <value>Specific tests ids supported by these data. These ids should map to specific tests as outlined in the overall event description. As an example, Event \"TCL3_Demonstration\" at a test site may have a test_card called \"CARD_44\". The operation associated with this metadata block may support \"DAT2\", \"CNS1\", and \"CON4\" specific tests. If any one of those elements are missing (event_id, test_card, test_identifiers), then data analysis may become difficult if not impossible.</value>
        [DataMember(Name = "test_run", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "test_run")]
        [Required]
        [MinLength(1), MaxLength(20)]
        public string TestRun { get; set; }

        /// <summary>
        /// Expected values: a call sign, test card role, or flight ID that links this operation to a particular test card flight. As an example, in our TCL2 demo, \"GCS3\" could have been used here. For multiple flights emanating from one location on a test card, this data element could include further information to link the operation to the test card flight, such as GCS1A.
        /// </summary>
        /// <value>Expected values: a call sign, test card role, or flight ID that links this operation to a particular test card flight. As an example, in our TCL2 demo, \"GCS3\" could have been used here. For multiple flights emanating from one location on a test card, this data element could include further information to link the operation to the test card flight, such as GCS1A.</value>
        [DataMember(Name = "call_sign", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "call_sign")]
        [Required]
        [MinLength(1), MaxLength(100)]
        public string CallSign { get; set; }

        /// <summary>
        /// Test type.
        /// </summary>
        /// <value>Test type.</value>
        [DataMember(Name = "test_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "test_type")]
        [Required]
        [EnumDataType(typeof(TestTypeEnum))]
        public TestTypeEnum TestType { get; set; }

        /// <summary>
        /// Hardware-in-the-loop (HWITL) flights refer to ground or airborne flights. Software-in-the loop (SWITL) flights would be used to tag flights do not use a physical vehicle, whether or not there is a full-featured simulation involved.
        /// </summary>
        /// <value>Hardware-in-the-loop (HWITL) flights refer to ground or airborne flights. Software-in-the loop (SWITL) flights would be used to tag flights do not use a physical vehicle, whether or not there is a full-featured simulation involved.</value>
        [DataMember(Name = "source", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source")]
        [Required]
        [EnumDataType(typeof(SourceEnum))]
        public SourceEnum Source { get; set; }

        /// <summary>
        /// Name of the testing location, such as NV, Moffett, Crows Landing, etc.  The supplier of the metadata should use a constant value for this field when the same location is used mulitple times across data submissions. E.g. \"AMES\" should always be \"AMES\" and not \"ames\" or \"Ames\" or \"NASA Ames\" at other various times.
        /// </summary>
        /// <value>Name of the testing location, such as NV, Moffett, Crows Landing, etc.  The supplier of the metadata should use a constant value for this field when the same location is used mulitple times across data submissions. E.g. \"AMES\" should always be \"AMES\" and not \"ames\" or \"Ames\" or \"NASA Ames\" at other various times.</value>
        [DataMember(Name = "location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "location")]
        [Required]
        [MinLength(1), MaxLength(100)]
        public string Location { get; set; }

        /// <summary>
        /// Test setting. Note that a LAB setting may involve HWITL source and FIELD settings may involve SWITL sources.  LAB settings may have GROUND or FLIGHT tests, same with FIELD settings.  This is some insight into the reason for these various fields.
        /// </summary>
        /// <value>Test setting. Note that a LAB setting may involve HWITL source and FIELD settings may involve SWITL sources.  LAB settings may have GROUND or FLIGHT tests, same with FIELD settings.  This is some insight into the reason for these various fields.</value>
        [DataMember(Name = "setting", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "setting")]
        [Required]
        [EnumDataType(typeof(SettingEnum))]
        public SettingEnum Setting { get; set; }

        /// <summary>
        /// Free text may be included with the original submission and/or added by a data_quality_engineer.  In the latter case, all previous free_text should be preserved, i.e. the data_quality_engineer should only append to the free_text field.
        /// </summary>
        /// <value>Free text may be included with the original submission and/or added by a data_quality_engineer.  In the latter case, all previous free_text should be preserved, i.e. the data_quality_engineer should only append to the free_text field.</value>
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [MaxLength(3000)]
        public string FreeText { get; set; }

        /// <summary>
        /// Email address of author handling data quality issue. If multiple individuals modify this metadata, this field will only hold the last such individual.  In that case, the engineer should make an effort to document as much as possible in the free_text field.
        /// </summary>
        /// <value>Email address of author handling data quality issue. If multiple individuals modify this metadata, this field will only hold the last such individual.  In that case, the engineer should make an effort to document as much as possible in the free_text field.</value>
        [DataMember(Name = "data_quality_engineer", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "data_quality_engineer")]
        [MaxLength(150)]
        public string DataQualityEngineer { get; set; }

        /// <summary>
        /// This metadata was modified from its original submission by a data_quality_engineer.
        /// </summary>
        /// <value>This metadata was modified from its original submission by a data_quality_engineer.</value>
        [DataMember(Name = "modified", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "modified")]
        public bool? Modified { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class EventMetadata {\n");
            sb.Append("  DataCollection: ").Append(DataCollection).Append("\n");
            sb.Append("  EventId: ").Append(EventId).Append("\n");
            sb.Append("  TestCard: ").Append(TestCard).Append("\n");
            sb.Append("  TestRun: ").Append(TestRun).Append("\n");
            sb.Append("  CallSign: ").Append(CallSign).Append("\n");
            sb.Append("  TestType: ").Append(TestType).Append("\n");
            sb.Append("  Source: ").Append(Source).Append("\n");
            sb.Append("  Location: ").Append(Location).Append("\n");
            sb.Append("  Setting: ").Append(Setting).Append("\n");
            sb.Append("  FreeText: ").Append(FreeText).Append("\n");
            sb.Append("  DataQualityEngineer: ").Append(DataQualityEngineer).Append("\n");
            sb.Append("  Modified: ").Append(Modified).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}