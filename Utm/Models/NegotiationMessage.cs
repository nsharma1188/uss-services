﻿using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class NegotiationMessage
    {
        /// <summary>
        /// A UUIDv4 assigned to this message by the originator
        /// </summary>
        /// <value>A UUIDv4 assigned to this message by the originator</value>
        [DataMember(Name = "message_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "message_id is not of the correct UUID version")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// An identifier held constant across multiple message_ids.  This value represents a single negotiation between two USSs for two specific operations.
        /// </summary>
        /// <value>An identifier held constant across multiple message_ids.  This value represents a single negotiation between two USSs for two specific operations.</value>
        [DataMember(Name = "negotiation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "negotiation_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "negotiation_id is not of the correct UUID version")]        
        public Guid NegotiationId { get; set; }


        /// <summary>
        /// A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name_of_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_originator")]
        [Required]
        [MinLength(4), MaxLength(250)]
        public string UssNameOfOriginator { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        [EnumDataType(typeof(NegotiationMessageTypeEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public NegotiationMessageTypeEnum Type { get; set; }

        /// <summary>
        /// Gets or Sets ReplanSuggestion
        /// </summary>
        [DataMember(Name = "replan_suggestion", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "replan_suggestion")]
        [EnumDataType(typeof(NegotiationReplanSuggestionEnum))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public NegotiationReplanSuggestionEnum? ReplanSuggestion { get; set; }

        /// <summary>
        /// A boolean field indicating whether the vehicle supported by the originating USS will be using the approved vehicle-to-vehicle communication approach. Required when the negotiation results in any intersection of operation plans between the two operations in the negotiation. This assumes that there is only one approved V2V approach in the future operational system.  If this is not the case, this field could need to be more explicit to ensure certainty that the two vehicles will be able to communicate while in flight.
        /// </summary>
        /// <value>A boolean field indicating whether the vehicle supported by the originating USS will be using the approved vehicle-to-vehicle communication approach. Required when the negotiation results in any intersection of operation plans between the two operations in the negotiation. This assumes that there is only one approved V2V approach in the future operational system.  If this is not the case, this field could need to be more explicit to ensure certainty that the two vehicles will be able to communicate while in flight.</value>
        [DataMember(Name = "v2v_for_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "v2v_for_originator")]
        public bool? V2vForOriginator { get; set; }

        /// <summary>
        /// A boolean field indicating whether the vehicle supported by the receiving USS will be using the approved vehicle-to-vehicle communication approach. Required when the negotiation results in any intersection of operation plans between the two operations in the negotiation. This assumes that there is only one approved V2V approach in the future operational system.  If this is not the case, this field could need to be more explicit to ensure certainty that the two vehicles will be able to communicate while in flight.
        /// </summary>
        /// <value>A boolean field indicating whether the vehicle supported by the receiving USS will be using the approved vehicle-to-vehicle communication approach. Required when the negotiation results in any intersection of operation plans between the two operations in the negotiation. This assumes that there is only one approved V2V approach in the future operational system.  If this is not the case, this field could need to be more explicit to ensure certainty that the two vehicles will be able to communicate while in flight.</value>
        [DataMember(Name = "v2v_for_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "v2v_for_receiver")]
        public bool? V2vForReceiver { get; set; }


        /// <summary>
        /// The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.</value>
        [DataMember(Name = "gufi_of_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_of_originator")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi_of_originator is not of the correct UUID version")]
        [JsonConverter(typeof(GuidJsonConvertor))]
        [Required]
        public Guid GufiOfOriginator { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS. </value>
        [DataMember(Name = "gufi_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_of_receiver")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi_of_receiver is not of the correct UUID version")]
        [JsonConverter(typeof(GuidJsonConvertor))]
        [Required]
        public Guid GufiOfReceiver { get; set; }

        /// <summary>
        /// x,y,z string where x and y are the coords of the grid and z is the zoom level. For example: “110,117,10”
        /// </summary>
        [DataMember(Name = "discovery_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "discovery_reference")]
        //[MinLength(5), MaxLength(36)]
        public string DiscoveryReference { get; set; }

        /// <summary>
        /// Gets or Sets FreeText
        /// </summary>
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [StringLength(1000)]
        public string FreeText { get; set; }
    }
}