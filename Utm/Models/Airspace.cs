using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Airspace
    {
        /// <summary>
        /// Gets or Sets Airspace
        /// </summary>
        [DataMember(Name = "airspace", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "airspace")]
        public string Airspac { get; set; }

        /// <summary>
        /// Gets or Sets AirspaceId
        /// </summary>
        [DataMember(Name = "airspace_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "airspace_id")]
        public string AirspaceId { get; set; }

        /// <summary>
        /// Gets or Sets Highalt
        /// </summary>
        [DataMember(Name = "highalt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "highalt")]
        public string Highalt { get; set; }

        /// <summary>
        /// Gets or Sets Lowalt
        /// </summary>
        [DataMember(Name = "lowalt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lowalt")]
        public string Lowalt { get; set; }

        /// <summary>
        /// Gets or Sets LowerAlt
        /// </summary>
        [DataMember(Name = "lower_alt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lower_alt")]
        public double? LowerAlt { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets TheGeog
        /// </summary>
        [DataMember(Name = "the_geog", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "the_geog")]
        public string TheGeog { get; set; }

        /// <summary>
        /// Gets or Sets UpperAlt
        /// </summary>
        [DataMember(Name = "upper_alt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "upper_alt")]
        public double? UpperAlt { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Airspace {\n");
            sb.Append("  Airspace: ").Append(Airspac).Append("\n");
            sb.Append("  AirspaceId: ").Append(AirspaceId).Append("\n");
            sb.Append("  Highalt: ").Append(Highalt).Append("\n");
            sb.Append("  Lowalt: ").Append(Lowalt).Append("\n");
            sb.Append("  LowerAlt: ").Append(LowerAlt).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  TheGeog: ").Append(TheGeog).Append("\n");
            sb.Append("  UpperAlt: ").Append(UpperAlt).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}