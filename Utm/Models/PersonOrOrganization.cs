using AnraUssServices.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class PersonOrOrganization
    {
        /// <summary>
        /// The full official name of the Person, State, Organisation, Authority, aircraft operating agency, handling agency etc. [FIXM]
        /// </summary>                
        [DataMember(Name = "name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "name")]
        [MinLength(1), MaxLength(60)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// An array of email addresses. To establish best practices, the order of the email addresses in the array should indicate the order that they should be used.
        /// Note that we do not include a regular expression for email addresses.Such a RE is quite unweildy if it attempts to be complete.
        /// The responsibility is on the USS providing the email address to ensure it is valid and operational.
        /// Several sources on the Internet can be found discussing email REs.                
        /// </summary>
        [DataMember(Name = "email_addresses", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "email_addresses")]
        [MinLength(1), MaxLength(5)]
        [Required]
        public List<string> EmailAddresses { get; set; }

        /// <summary>
        /// An array of phone numbers. To establish best practices, 
        /// the order of the phone numbers in the array should indicate the order that they should be used.
        /// </summary>
        [DataMember(Name = "phone_numbers", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "phone_numbers")]
        [MinLength(1), MaxLength(5)]        
        [Required]
        public List<string> PhoneNumbers { get; set; }

        /// <summary>
        /// Comments
        /// </summary>        
        [DataMember(Name = "comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "comments")]
        [MinLength(1),MaxLength(1000)]
        public string Comments { get; set; }
    }
}