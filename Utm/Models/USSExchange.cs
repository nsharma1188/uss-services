using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using LiteDB;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{

    /// <summary>
    /// This model captures performance and interoperability data for a USS. since
    /// these data are not captured explicitly in the USS network, it is important
    /// to have USSs self report on these elements.This information may inform
    /// future performance requirements and forensics of certain incidents.This
    /// may be an initial model that will be required operationally in terms of
    /// a USSs need to log interactions with other USSs.
    /// </summary>
    
    [DataContract]
    public class UssExchange
    {
        /// <summary>
        /// A UUID assingned by the reporting USS for this instance of USSExchange.
        /// </summary>
        /// <value>A UUID assingned by the reporting USS for this instance of USSExchange.</value>
        [DataMember(Name = "measurement_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "measurement_id")]
        [Required]
		[RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "measurement_id is not of the correct UUID version")]
        public Guid MeasurementId { get; set; }

        /// <summary>
        /// A string provided by the owner of the overall test (likely NASA) that identifies the event within which this data exchange occurs. NASA will define a pattern for this for consistency across tests.
        /// </summary>
        /// <value>A string provided by the owner of the overall test (likely NASA) that identifies the event within which this data exchange occurs. NASA will define a pattern for this for consistency across tests.</value>
        [DataMember(Name = "event_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "event_id")]
        [Required]
        [MinLength(3), MaxLength(100)]
        public string EventId { get; set; }

        /// <summary>
        /// Gets or Sets Primary Key in the Data Packet
        /// </summary>
        [DataMember(Name = "exchanged_data_pk", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "exchanged_data_pk")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "exchanged_data_pk is not of the correct UUID version")]
        public Guid ExchangedDataPk { get; set; }

        /// <summary>
        /// Gets or Sets ExchangedDataType
        /// </summary>
        [DataMember(Name = "exchanged_data_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "exchanged_data_type")]
        [EnumDataType(typeof(ExchangedDataTypeEnum))]
        [Required]
        public ExchangedDataTypeEnum ExchangedDataType { get; set; }

        /// <summary>
        /// Gets or Sets name of SourceUss
        /// </summary>
        [DataMember(Name = "source_uss", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source_uss")]
        [Required]
        public string SourceUss { get; set; }

        /// <summary>
        /// Gets or Sets name of TargetUss
        /// </summary>
        [DataMember(Name = "target_uss", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "target_uss")]
        [Required]
        public string TargetUss { get; set; }

        /// <summary>
        /// Gets or Sets name of Uss Name
        /// </summary>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [Required]
        public string UssName { get; set; }

        /// <summary>
        /// An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).
        /// </summary>
        /// <value>An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).</value>
        [DataMember(Name = "reporting_uss_role", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reporting_uss_role")]
        [EnumDataType(typeof(ReportingUssRoleEnum))]        
        public ReportingUssRoleEnum ReportingUssRole { get; set; }

        /// <summary>
        /// If SOURCE_USS, this is the time that the request is sent to the TARGET_USS. If TARGET_USS, this is the time that the request was received from the SOURCE_USS. Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).
        /// </summary>
        /// <value>If SOURCE_USS, this is the time that the request is sent to the TARGET_USS. If TARGET_USS, this is the time that the request was received from the SOURCE_USS. Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).</value>
        [DataMember(Name = "time_request_initiation", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_request_initiation")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime TimeRequestInitiation { get; set; }

        /// <summary>
        /// If SOURCE_USS, this is the time that the response was received from the TARGET_USS. If TARGET_USS, this is the time that the request was sent back to the SOURCE_USS.            Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).
        /// </summary>
        /// <value>If SOURCE_USS, this is the time that the response was received from the TARGET_USS. If TARGET_USS, this is the time that the request was sent back to the SOURCE_USS.            Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).</value>
        [DataMember(Name = "time_request_completed", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_request_completed")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime TimeRequestCompleted { get; set; }

        /// <summary>
        /// The endpoint to which the data request was initially sent.
        /// </summary>
        /// <value>The endpoint to which the data request was initially sent.</value>
        [DataMember(Name = "endpoint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "endpoint")]
        [Required]
        public string Endpoint { get; set; }

        /// <summary>
        /// The HTTP method used in this exchange.
        /// </summary>
        /// <value>The HTTP method used in this exchange.</value>
        [DataMember(Name = "http_method", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "http_method")]
        [EnumDataType(typeof(HttpMethodEnum))]
        [Required]
        public HttpMethodEnum HttpMethod { get; set; }


        /// <summary>
        /// The expected HTTP response.  This is required ONLY if the reporting_uss_role is SOURCE_USS.
        /// </summary>
        /// <value>The expected HTTP response.  This is required ONLY if the reporting_uss_role is SOURCE_USS.</value>
        [DataMember(Name = "expected_http_response", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "expected_http_response")]
        [Range(100, 599)]
        public int? ExpectedHttpResponse { get; set; }

        /// <summary>
        /// The actual HTTP response sent by the TARGET_USS to the SOURCE_USS. Must be reported by USSs in either role.
        /// </summary>
        /// <value>The actual HTTP response sent by the TARGET_USS to the SOURCE_USS. Must be reported by USSs in either role.</value>
        [DataMember(Name = "actual_http_response", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_http_response")]
        [Required]
        [Range(100, 599)]
        public int ActualHttpResponse { get; set; }

        /// <summary>
        /// The JWS that was included in the data exchange. If a PUT/POST then the SOURCE_USS would have generated and sent to TARGET_USS.  If a GET then the TARGET_USS would have generated and sent to the SOURCE_USS.  In either case, both the SOURCE_USS and the TARGET_USS should include the resulting JWS here.
        /// </summary>
        /// <value>The JWS that was included in the data exchange. If a PUT/POST then the SOURCE_USS would have generated and sent to TARGET_USS.  If a GET then the TARGET_USS would have generated and sent to the SOURCE_USS.  In either case, both the SOURCE_USS and the TARGET_USS should include the resulting JWS here.</value>
        [DataMember(Name = "jws", EmitDefaultValue = false)]
        public string Jws { get; set; }

        /// <summary>
        /// If available, include the public key for decoding the jws.
        /// </summary>
        /// <value>If available, include the public key for decoding the jws.</value>
        [DataMember(Name = "jws_public_key", EmitDefaultValue = false)]
        public string JwsPublicKey { get; set; }

        /// <summary>
        /// Any additional comments that could aid in analysis involving these data.
        /// </summary>
        /// <value>Any additional comments that could aid in analysis involving these data.</value>
        [DataMember(Name = "comments", EmitDefaultValue = false)]
        [MaxLength(1000)]
        public string Comments { get; set; }
    }
}