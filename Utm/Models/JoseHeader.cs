using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class JoseHeader
    {
        /// <summary>
        /// Gets or Sets Alg
        /// </summary>
        [DataMember(Name = "alg", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "alg")]
        public string Alg { get; set; }

        /// <summary>
        /// The FIMS Authorization Server only support 'JWT' (JSON Web Token) as the algorithm type.
        /// </summary>
        /// <value>The FIMS Authorization Server only support 'JWT' (JSON Web Token) as the algorithm type.</value>
        [DataMember(Name = "typ", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "typ")]
        public string Typ { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JoseHeader {\n");
            sb.Append("  Alg: ").Append(Alg).Append("\n");
            sb.Append("  Typ: ").Append(Typ).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}