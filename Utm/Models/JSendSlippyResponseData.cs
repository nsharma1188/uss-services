using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class JSendSlippyResponseData
    {
        /// <summary>
        /// Gets or Sets Zoom
        /// </summary>
        [Required]
        [DataMember(Name="zoom")]
        public int Zoom { get; set; }

        /// <summary>
        /// Gets or Sets GridCells
        /// </summary>
        [Required]
        [DataMember(Name="grid_cells")]
        public List<JSendSlippyResponseDataGridCells> GridCells { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JSendSlippyResponseData {\n");
            sb.Append("  Zoom: ").Append(Zoom).Append("\n");
            sb.Append("  GridCells: ").Append(GridCells).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
