using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class PointOut
    {
        public PointOut()
        {
            NorthRef = "MAGNETIC";
        }

        /// <summary>
        /// The altitude of the target. Units in feet using the WGS84 reference system. May be estimated.
        /// </summary>
        /// <value>The altitude of the target. Units in feet using the WGS84 reference system. May be estimated.</value>
        [DataMember(Name = "altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude")]
        [Required]
        public double? Altitude { get; set; }

        /// <summary>
        /// Degrees from north (as defined by north_ref) from reporter's position to target.
        /// </summary>
        /// <value>Degrees from north (as defined by north_ref) from reporter's position to target.</value>
        [DataMember(Name = "bearing", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "bearing")]
        [Required]
        public int? Bearing { get; set; }

        /// <summary>
        /// Nautical miles (horizontal only) from reporter's position to target.
        /// </summary>
        /// <value>Nautical miles (horizontal only) from reporter's position to target.</value>
        [DataMember(Name = "distance", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "distance")]
        [Required]
        public double? Distance { get; set; }

        /// <summary>
        /// The reference for 'north' in reporting values
        /// </summary>
        /// <value>The reference for 'north' in reporting values</value>
        [DataMember(Name = "north_ref", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "north_ref")]
        [Required]
        public string NorthRef { get; set; }

        /// <summary>
        /// Gets or Sets Point
        /// </summary>
        [DataMember(Name = "point", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "point")]
        [Required]
        public Point Point { get; set; }

        /// <summary>
        /// Any free text that cannot be captured using the standard fields.
        /// </summary>
        /// <value>Any free text that cannot be captured using the standard fields.</value>
        [DataMember(Name = "remark", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "remark")]
        public string Remark { get; set; }

        /// <summary>
        /// The current state of the target.
        /// </summary>
        /// <value>The current state of the target.</value>
        [DataMember(Name = "state", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "state")]
        [Required]
        public string State { get; set; }

        /// <summary>
        /// The track in decimal degrees relative to Double north_ref.
        /// </summary>
        /// <value>The track in decimal degrees relative to Double north_ref.</value>
        [DataMember(Name = "track", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "track")]
        [Required]
        public double? Track { get; set; }

        /// <summary>
        /// The type of vehicle being pointed out.
        /// </summary>
        /// <value>The type of vehicle being pointed out.</value>
        [DataMember(Name = "vehicle_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vehicle_type")]
        [Required]
        public string VehicleType { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class PointOut {\n");
            sb.Append("  Altitude: ").Append(Altitude).Append("\n");
            sb.Append("  Bearing: ").Append(Bearing).Append("\n");
            sb.Append("  Distance: ").Append(Distance).Append("\n");
            sb.Append("  NorthRef: ").Append(NorthRef).Append("\n");
            sb.Append("  Point: ").Append(Point).Append("\n");
            sb.Append("  Remark: ").Append(Remark).Append("\n");
            sb.Append("  State: ").Append(State).Append("\n");
            sb.Append("  Track: ").Append(Track).Append("\n");
            sb.Append("  VehicleType: ").Append(VehicleType).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}