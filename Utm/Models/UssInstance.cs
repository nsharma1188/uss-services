using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{ 

    /// <summary>
    /// A contiguous area within which a USS has committed to providing services for UAS operations.  The specific level of services provided may be contained within the data provided from here in combination with data provided directly from the USS.  In the future, the information provided by this object may include more specific information about the services provided by the USS or the types of UAS operations that are supported by this USS Instance.
    /// </summary>
    [DataContract]
    public class UssInstance
    {
        /// <summary>
        /// A unique USS Instance identifier, minted by the USS.
        /// </summary>
        /// <value>A unique USS Instance identifier, minted by the USS.</value>
        [DataMember(Name = "uss_instance_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_instance_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "uss_instance_id is not of the correct UUID version")]
        public Guid UssInstanceId { get; set; }

        /// <summary>
        /// The name of the entity providing UAS Support Services. Populated by the service discovery system based on credential information provided by the USS.
        /// </summary>
        /// <value>The name of the entity providing UAS Support Services. Populated by the service discovery system based on credential information provided by the USS.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [MinLength(1),MaxLength(250)]
        public string UssName { get; set; }

        /// <summary>
        /// The time at which the submission of this USS Instance was received at this discovery service.  Only modifiable by discovery service.
        /// </summary>
        /// <value>The time at which the submission of this USS Instance was received at this discovery service.  Only modifiable by discovery service.</value>
        [DataMember(Name = "time_submitted", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_submitted")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeSubmitted { get; set; }

        /// <summary>
        /// The time at which the USS will begin providing services for active UAS operations for this USS Instance.  Note that the USS may provide planning services prior to the time_available_begin depending on the policies of the USS.
        /// </summary>
        /// <value>The time at which the USS will begin providing services for active UAS operations for this USS Instance.  Note that the USS may provide planning services prior to the time_available_begin depending on the policies of the USS.</value>
        [DataMember(Name = "time_available_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_available_begin")]  
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime TimeAvailableBegin { get; set; }

        /// <summary>
        /// The time at which the USS will cease providing services for active UAS operations for this USS Instance.  This means that there will not be any UAS operations airborne after this time that would be supported by this USS Instance.
        /// </summary>
        /// <value>The time at which the USS will cease providing services for active UAS operations for this USS Instance.  This means that there will not be any UAS operations airborne after this time that would be supported by this USS Instance.</value>
        [DataMember(Name = "time_available_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_available_end")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime TimeAvailableEnd { get; set; }

        /// <summary>
        /// Gets or Sets CoverageArea
        /// </summary>
        [DataMember(Name = "coverage_area", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "coverage_area")]
        [Required]
        [PolygonValidator]
        public Polygon CoverageArea { get; set; }

        /// <summary>
        /// The last time there was an update to the data regarding this USS Instance. Only modifiable by discovery service.
        /// </summary>
        /// <value>The last time there was an update to the data regarding this USS Instance. Only modifiable by discovery service.</value>
        [DataMember(Name = "time_last_modified", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_last_modified")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeLastModified { get; set; }

        /// <summary>
        /// An optional contact email address for the USS.
        /// </summary>
        /// <value>An optional contact email address for the USS.</value>
        [DataMember(Name = "contact", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contact")]
        public PersonOrOrganization Contact { get; set; }

        /// <summary>
        /// A base URL used by the USS Discovery Service and other USS Instances to callback to the USS Instance.   USS Discovery will append \"/uss\" to the base URL in order to POST a new uss_instance to the USS Instance. It will also do the same to test the URL when it first receives a uss_instance.  USS Instances and other clients/services will append \"/operations\" to the base URL in order to share operations. Similarly with \"/utm_messages\", \"/negotiations\", etc.  The base URL will also be used in obtaining the websocket for position sharing.  This URL may be unique to the USS Instance or may be shared by other USS Instances created by the same USS.  URLs that are common to multiple USS Instances will receive only one callback.
        /// </summary>
        /// <value>A base URL used by the USS Discovery Service and other USS Instances to callback to the USS Instance.   USS Discovery will append \"/uss\" to the base URL in order to POST a new uss_instance to the USS Instance. It will also do the same to test the URL when it first receives a uss_instance.  USS Instances and other clients/services will append \"/operations\" to the base URL in order to share operations. Similarly with \"/utm_messages\", \"/negotiations\", etc.  The base URL will also be used in obtaining the websocket for position sharing.  This URL may be unique to the USS Instance or may be shared by other USS Instances created by the same USS.  URLs that are common to multiple USS Instances will receive only one callback.</value>
        [DataMember(Name = "uss_base_callback_url", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_base_callback_url")]
        [Required]
        public string UssBaseCallbackUrl { get; set; }

        /// <summary>
        /// An optional website URL for the USS for human use.
        /// </summary>
        /// <value>An optional website URL for the USS for human use.</value>
        [DataMember(Name = "uss_informational_url", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_informational_url")]        
        public string UssInformationalUrl { get; set; }

        /// <summary>
        /// An optional URL for the USS to supply an OpenAPI specification of its UAS Operator interface.
        /// </summary>
        /// <value>An optional URL for the USS to supply an OpenAPI specification of its UAS Operator interface.</value>
        [DataMember(Name = "uss_openapi_url", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_openapi_url")]
        public string UssOpenapiUrl { get; set; }

        /// <summary>
        /// An optional URL for UAS operators to register with this USS for services.
        /// </summary>
        /// <value>An optional URL for UAS operators to register with this USS for services.</value>
        [DataMember(Name = "uss_registration_url", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_registration_url")]
        public string UssRegistrationUrl { get; set; }

        /// <summary>
        /// Any additional free text that would aid consumers of the service discovery API in understanding this USS Instance.
        /// </summary>
        /// <value>Any additional free text that would aid consumers of the service discovery API in understanding this USS Instance.</value>
        [DataMember(Name = "notes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "notes")]
        public string Notes { get; set; }

        
    }
}