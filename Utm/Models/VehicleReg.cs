using System;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public partial class VehicleReg
    {
        /// <summary>
        /// Gets or Sets Uvin
        /// </summary>
        [DataMember(Name = "uvin")]
        public Guid? Uvin { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name = "date")]
        public string Date { get; set; }

        /// <summary>
        /// Gets or Sets RegisteredBy
        /// </summary>
        [DataMember(Name = "registeredBy")]
        public string RegisteredBy { get; set; }

        /// <summary>
        /// Gets or Sets NNumber
        /// </summary>
        [DataMember(Name = "nNumber")]
        public string NNumber { get; set; }

        /// <summary>
        /// Gets or Sets FaaNumber
        /// </summary>
        [DataMember(Name = "faaNumber")]
        public string FaaNumber { get; set; }

        /// <summary>
        /// A vehicle name identifier that has been given to the vehicle instance
        /// </summary>
        /// <value>A vehicle name identifier that has been given to the vehicle instance</value>
        [DataMember(Name = "vehicleName")]
        public string VehicleName { get; set; }

        /// <summary>
        /// Name of the manufacturer
        /// </summary>
        /// <value>Name of the manufacturer</value>
        [DataMember(Name = "manufacturer")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Model name
        /// </summary>
        /// <value>Model name</value>
        [DataMember(Name = "model")]
        public string Model { get; set; }

        /// <summary>
        /// Vehicle class
        /// </summary>
        /// <value>Vehicle class</value>
        [DataMember(Name = "class")]
        public string Class { get; set; }

        /// <summary>
        /// Designates whether or not the vehicle type is publically available.  If private it can only be viewed by the manufacturer
        /// </summary>
        /// <value>Designates whether or not the vehicle type is publically available.  If private it can only be viewed by the manufacturer</value>
        [DataMember(Name = "accessType")]
        public string AccessType { get; set; }

        /// <summary>
        /// Gets or Sets VehicleTypeId
        /// </summary>
        [DataMember(Name = "vehicleTypeId")]
        public Guid? VehicleTypeId { get; set; }
    }
}