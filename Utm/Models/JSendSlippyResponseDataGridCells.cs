using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class JSendSlippyResponseDataGridCells
    {
        /// <summary>
        /// Gets or Sets Link
        /// </summary>
        [Required]
        [DataMember(Name="link")]
        public string Link { get; set; }

        /// <summary>
        /// Gets or Sets Zoom
        /// </summary>
        [Required]
        [DataMember(Name="zoom")]
        public int Zoom { get; set; }

        /// <summary>
        /// Gets or Sets X
        /// </summary>
        [Required]
        [DataMember(Name="x")]
        public int X { get; set; }

        /// <summary>
        /// Gets or Sets Y
        /// </summary>
        [Required]
        [DataMember(Name="y")]
        public int Y { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JSendSlippyResponseDataGridCells {\n");
            sb.Append("  Link: ").Append(Link).Append("\n");
            sb.Append("  Zoom: ").Append(Zoom).Append("\n");
            sb.Append("  X: ").Append(X).Append("\n");
            sb.Append("  Y: ").Append(Y).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
