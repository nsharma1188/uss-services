using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class NationalPark
    {
        /// <summary>
        /// Gets or Sets DateEdit
        /// </summary>
        [DataMember(Name = "date_edit", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "date_edit")]
        public DateTime? DateEdit { get; set; }

        /// <summary>
        /// Gets or Sets Geog
        /// </summary>
        [DataMember(Name = "geog", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geog")]
        public string Geog { get; set; }

        /// <summary>
        /// Gets or Sets Gid
        /// </summary>
        [DataMember(Name = "gid", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gid")]
        public int? Gid { get; set; }

        /// <summary>
        /// Gets or Sets GisLocId
        /// </summary>
        [DataMember(Name = "gis_loc_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gis_loc_id")]
        public string GisLocId { get; set; }

        /// <summary>
        /// Gets or Sets GisNotes
        /// </summary>
        [DataMember(Name = "gis_notes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gis_notes")]
        public string GisNotes { get; set; }

        /// <summary>
        /// Gets or Sets GroupCode
        /// </summary>
        [DataMember(Name = "group_code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "group_code")]
        public string GroupCode { get; set; }

        /// <summary>
        /// Gets or Sets LandsCode
        /// </summary>
        [DataMember(Name = "lands_code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lands_code")]
        public string LandsCode { get; set; }

        /// <summary>
        /// Gets or Sets MetaMidf
        /// </summary>
        [DataMember(Name = "meta_midf", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "meta_midf")]
        public string MetaMidf { get; set; }

        /// <summary>
        /// Gets or Sets ParksId
        /// </summary>
        [DataMember(Name = "parks_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "parks_id")]
        public string ParksId { get; set; }

        /// <summary>
        /// Gets or Sets UnitCode
        /// </summary>
        [DataMember(Name = "unit_code", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "unit_code")]
        public string UnitCode { get; set; }

        /// <summary>
        /// Gets or Sets UnitName
        /// </summary>
        [DataMember(Name = "unit_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "unit_name")]
        public string UnitName { get; set; }

        /// <summary>
        /// Gets or Sets UnitType
        /// </summary>
        [DataMember(Name = "unit_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "unit_type")]
        public string UnitType { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class NationalPark {\n");
            sb.Append("  DateEdit: ").Append(DateEdit).Append("\n");
            sb.Append("  Geog: ").Append(Geog).Append("\n");
            sb.Append("  Gid: ").Append(Gid).Append("\n");
            sb.Append("  GisLocId: ").Append(GisLocId).Append("\n");
            sb.Append("  GisNotes: ").Append(GisNotes).Append("\n");
            sb.Append("  GroupCode: ").Append(GroupCode).Append("\n");
            sb.Append("  LandsCode: ").Append(LandsCode).Append("\n");
            sb.Append("  MetaMidf: ").Append(MetaMidf).Append("\n");
            sb.Append("  ParksId: ").Append(ParksId).Append("\n");
            sb.Append("  UnitCode: ").Append(UnitCode).Append("\n");
            sb.Append("  UnitName: ").Append(UnitName).Append("\n");
            sb.Append("  UnitType: ").Append(UnitType).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}