using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// An element defined by spatial and temporal bounds that affects UAS operations by limiting access to that 4D bounded volume. 
    /// </summary>
    [DataContract]
    public class UASVolumeReservation
    {
        /// <summary>
        /// A UUIDv4 assigned to this message by the originator
        /// </summary>
        /// <value>A UUIDv4 assigned to this message by the originator</value>
        [DataMember(Name = "message_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "message_id is not of the correct UUID version")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// A name identifying the originator of this message. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>A name identifying the originator of this message. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [MinLength(4), MaxLength(250)]
        [Required]
        public string UssName { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        [EnumDataType(typeof(ConstraintMessageType))]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public ConstraintMessageType Type { get; set; }

        /// <summary>
        /// The types of UAS operations that are permitted within the bound of the constraint. 
        /// When used in conjunction with permitted_gufis, the filters are an �OR� relationship. 
        /// Any operation that matches either filter is allowed within the bounds of the constraint. 
        /// This list of permitted_uas types is not to be considered a final version. It is provided 
        /// to initiate discussion of how the filter should be defined.
        /// </summary>
        [DataMember(Name = "permitted_uas", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "permitted_uas")]
        [Required]
        [MinLength(1),MaxLength(8)]
        public List<UasType> PermittedUas { get; set; }

        /// <summary>
        /// If the permitted_uas array contains "SUPPORT_LEVEL" this required_support field is required. 
        /// This field describes the support required by an operation to continue to operate within or to enter the UVR. 
        /// By remaining within or entering a UVR with the listed required support, the operator is attesting that it is using all elements listed in the required support array.
        /// To restate: the array of values in this field is an AND relationship, thus all elements must be utilized by each operation within such a UVR.
        /// If an operator does not have every support elements listed, then its operation must exit or avoid entry to the UVR.
        /// </summary>
        [DataMember(Name = "required_support", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "required_support")]
        public List<RequiredSupportType> RequiredSupport { get; set; }

        /// <summary>
        /// The specific UAS operations that are permitted within the counds of the constraint. 
        /// When used in conjunction with permitted_uas, the filters are an �OR� relationship. 
        /// Any operation that matches either filter is allowed within the bounds of the constraint. 
        /// The protocol for adding the appropriate GUFIs to this array needs to be discussed. 
        /// This may be a chicken/egg problem that will have to be worked through.
        /// </summary>
        [DataMember(Name = "permitted_gufis", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "permitted_gufis")]        
        [MinLength(0), MaxLength(100)]
        [GuidListValidator]
        public List<string> PermittedGufis { get; set; }

        /// <summary>
        /// Gets or Sets property of cause
        /// </summary>
        /// <value>Gets or Sets property of cause</value>
        [DataMember(Name = "cause", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cause")]
        [EnumDataType(typeof(ConstraintMessageCauseEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public ConstraintMessageCauseEnum Cause { get; set; }

        /// <summary>
        /// Gets or Sets ConstraintGeography
        /// </summary>
        [DataMember(Name = "geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geography")]
        [Required]
        [PolygonValidator]
        public Polygon Geography { get; set; }

        /// <summary>
        /// The time that the constraint will begin to be in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time that the constraint will begin to be in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime? EffectiveTimeBegin { get; set; }

        /// <summary>
        /// The time that the constraint will cease to be in effect..  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time that the constraint will cease to be in effect..  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime? EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Time that the constaint actually ceased being in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.  This value is likely used when a constraint ends early, but is not required.
        /// </summary>
        /// <value>Time that the constaint actually ceased being in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.  This value is likely used when a constraint ends early, but is not required.</value>
        [DataMember(Name = "actual_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ActualTimeEnd { get; set; }

        /// <summary>
        /// The minimum altitude of the constraint. Must be less than max_altitude.
        /// </summary>
        /// <value>The minimum altitude of the constraint. Must be less than max_altitude.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        [Required]
        public Altitude MinAltitude { get; set; }

        /// <summary>
        /// The maximum altitude of the constraint. Must be greater than min_altitude.
        /// </summary>
        /// <value>The maximum altitude of the constraint. Must be greater than min_altitude.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        [Required]
        public Altitude MaxAltitude { get; set; }

        /// <summary>
        /// The reason for the constraint. Meant for human consumption.
        /// </summary>
        /// <value>The reason for the constraint. Meant for human consumption.</value>
        [DataMember(Name = "reason", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reason")]
        [StringLength(1000)]
        public string Reason { get; set; }


        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ConstraintMessage {\n");
            sb.Append("  MessageId: ").Append(MessageId).Append("\n");            
            sb.Append("  UssName: ").Append(UssName).Append("\n");            
            sb.Append("  Type: ").Append(Type).Append("\n");
            sb.Append("  Geography: ").Append(Geography).Append("\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  EffectiveTimeEnd: ").Append(EffectiveTimeEnd).Append("\n");
            sb.Append("  ActualTimeEnd: ").Append(ActualTimeEnd).Append("\n");
            sb.Append("  MinAltitude: ").Append(MinAltitude).Append("\n");
            sb.Append("  MaxAltitude: ").Append(MaxAltitude).Append("\n");
            sb.Append("  Reason: ").Append(Reason).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

    }
}
