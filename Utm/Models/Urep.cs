using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Urep
    {
        public Urep()
        {
            Proximity = "VC";
        }
        /// <summary>
        /// Air temperature in degrees Celcius.
        /// </summary>
        /// <value>Air temperature in degrees Celcius.</value>
        [DataMember(Name = "air_temperature", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "air_temperature")]
        [Range(-273.15, double.MaxValue)]
        public double? AirTemperature { get; set; }

        /// <summary>
        /// An aircraft is in the vicinity of the report.
        /// </summary>
        /// <value>An aircraft is in the vicinity of the report.</value>
        [DataMember(Name = "aircraft_sightings", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "aircraft_sightings")]
        public List<PointOut> AircraftSighting { get; set; }

        /// <summary>
        /// The altitude as measured via a GPS device on the aircraft. Units ininternational feet using the WGS84 reference system.
        /// </summary>
        /// <value>The altitude as measured via a GPS device on the aircraft. Units ininternational feet using the WGS84 reference system.</value>
        [DataMember(Name = "altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude")]
        [Required]
        [Range(-250.0, double.MaxValue)]
        public double Altitude { get; set; }

        /// <summary>
        /// The UUID of the reporting operation, if appropriate.
        /// </summary>
        /// <value>The UUID of the reporting operation, if appropriate.</value>
        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi is not of the correct UUID version")]        
        public Guid Gufi { get; set; }

        /// <summary>
        /// A qualitative measure of icing intensity. Use 'NEG' when icing was expected, but not encountered.
        /// </summary>
        /// <value>A qualitative measure of icing intensity. Use 'NEG' when icing was expected, but not encountered.</value>
        [DataMember(Name = "icing_intensity", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "icing_intensity")]
        [EnumDataType(typeof(IcingIntensity))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public IcingIntensity IcingIntensity { get; set; }

        /// <summary>
        /// The type of icing encountered. Can only be submitted with icing_intensity.
        /// </summary>
        /// <value>The type of icing encountered. Can only be submitted with icing_intensity.</value>
        [DataMember(Name = "icing_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "icing_type")]
        [EnumDataType(typeof(IcingType))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public IcingType IcingType { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        [DataMember(Name = "location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "location")]
        [Required]
        [PointValidator]
        public Point Location { get; set; }

        /// <summary>
        /// Qualifier for any weather value provided. Use this field if the observed phenomenon is not at the provided location, but is nearby. Must be equal to VC.
        /// </summary>
        /// <value>Qualifier for any weather value provided. Use this field if the observed phenomenon is not at the provided location, but is nearby. Must be equal to VC.</value>
        [DataMember(Name = "proximity", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "proximity")]
        public string Proximity { get; set; }

        /// <summary>
        /// Any free text that cannot be captured using the standard fields. General use not advised.
        /// </summary>
        /// <value>Any free text that cannot be captured using the standard fields. General use not advised.</value>
        [DataMember(Name = "remarks", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "remarks")]
        [MaxLength(1000)]
        public string Remarks { get; set; }

        /// <summary>
        /// How the measured/observed activity was measured/observed. In the case of “OTHER” comments should be supplied in the “remarks” field.
        /// </summary>
        /// <value>How the measured/observed activity was measured/observed. In the case of “OTHER” comments should be supplied in the “remarks” field.</value>
        [DataMember(Name = "source", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source")]
        [Required]
        [EnumDataType(typeof(UrepSource))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public UrepSource Source { get; set; }

        /// <summary>
        /// UTM Client assigned time for when the UREP was sent to the UTM System.
        /// </summary>
        /// <value>UTM Client assigned time for when the UREP was sent to the UTM System.</value>
        [DataMember(Name = "time_submitted", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_submitted")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeSubmitted { get; set; }

        /// <summary>
        /// Client assigned time for when the data were initially measured.
        /// </summary>
        /// <value>Client assigned time for when the data were initially measured.</value>
        [DataMember(Name = "time_measured", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_measured")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeMeasured { get; set; }

        /// <summary>
        /// UTM assigned time for when the UREP was received at the UTM System for processing.
        /// </summary>
        /// <value>UTM assigned time for when the UREP was received at the UTM System for processing.</value>
        [DataMember(Name = "time_received", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_received")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? TimeReceived { get; set; }

        /// <summary>
        /// A qualitative measure of turbulence intensity. Use 'NEG' when turbulence was expected, but not encountered.
        /// </summary>
        /// <value>A qualitative measure of turbulence intensity. Use 'NEG' when turbulence was expected, but not encountered.</value>
        [DataMember(Name = "turbulence_intensity", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "turbulence_intensity")]
        [EnumDataType(typeof(TurbulenceIntensity))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public TurbulenceIntensity TurbulenceIntensity { get; set; }

        /// <summary>
        /// UTM System assigned identifier for a UREP.
        /// </summary>
        /// <value>UTM System assigned identifier for a UREP.</value>
        [DataMember(Name = "urep_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "urep_id")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "urep_id is not of the correct UUID version")]
        public string UrepId { get; set; }

        /// <summary>
        /// Gets or Sets UserId
        /// </summary>
        [DataMember(Name = "user_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_id")]
        [Required]
        [MaxLength(1000)]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or Sets OperatorId
        /// </summary>
        [DataMember(Name = "operator_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operator_id")]
        [MaxLength(1000)]
        public string OperatorId { get; set; }

        /// <summary>
        /// Visibility in statute miles, rounded down if necessary. Maximum value 99 indicates unrestricted visibility. Only use partial SM less than 1 SM (0.5 SM, but not 20.5 SM).
        /// </summary>
        /// <value>Visibility in statute miles, rounded down if necessary. Maximum value 99 indicates unrestricted visibility. Only use partial SM less than 1 SM (0.5 SM, but not 20.5 SM).</value>
        [DataMember(Name = "visibility", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "visibility")]
        public double? Visibility { get; set; }

        /// <summary>
        /// Flight visibility and flight weather, follows PIREP/METAR coding.
        /// </summary>
        /// <value>Flight visibility and flight weather, follows PIREP/METAR coding.</value>
        [DataMember(Name = "weather", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "weather")]
        [EnumDataType(typeof(Weather))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public Weather Weather { get; set; }

        /// <summary>
        /// Qualifier for any weather value provided.  Without a weather_intensity value, MODERATE is assumed.
        /// </summary>
        /// <value>Qualifier for any weather value provided.  Without a weather_intensity value, MODERATE is assumed.</value>
        [DataMember(Name = "weather_intensity", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "weather_intensity")]
        [EnumDataType(typeof(WeatherIntensity))]        
        [JsonConverter(typeof(EnumJsonConvertor))]
        public WeatherIntensity WeatherIntensity { get; set; }

        /// <summary>
        /// Magnetic wind direction, 0 to 359 degrees. Must be submitted when wind_speed is provided in the UREP .
        /// </summary>
        /// <value>Magnetic wind direction, 0 to 359 degrees. Must be submitted when wind_speed is provided in the UREP .</value>
        [DataMember(Name = "wind_direction", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "wind_direction")]
        [RangeValidator(0, 360, "wind_direction value must be greater or equal to 0.0 and less than 360.0")]
        public double? WindDirection { get; set; }

        /// <summary>
        /// Wind speed in knots. Must be submitted when wind_direction is provided in the UREP.
        /// </summary>
        /// <value>Wind speed in knots. Must be submitted when wind_direction is provided in the UREP.</value>
        [DataMember(Name = "wind_speed", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "wind_speed")]
        [Range(0,int.MaxValue)]
        public int? WindSpeed { get; set; }        
    }
}