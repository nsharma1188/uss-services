using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Mapster;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class ContingencyPlan
    {   

        /// <summary>
        /// A positive integer unique amongst the set of Contingencies for this operation. The integers may be ordered by some scheme, but this is not required.
        /// </summary>
        /// <value>A positive integer unique amongst the set of Contingencies for this operation. The integers may be ordered by some scheme, but this is not required.</value>
        [DataMember(Name = "contingency_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_id")]
        [Required]
        [Range(1,1000)]
        public int ContingencyId { get; set; }

        /// <summary>
        /// Gets or Sets ContingencyCause
        /// </summary>
        [DataMember(Name = "contingency_cause", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_cause")]
        [Required]        
        public List<string> ContingencyCause { get; set; }

        /// <summary>
        /// The type of contingency response. 1. LANDING   The operation will be landing by targeting the contingency_point. 2. LOITERING   The operation will loiter at the contingency_point at the specified altitude with the noted loiter_radius_ft. 3. RETURN_TO_BASE   The operation will return to base as specified by the contingency_point. The USS may issue an update to the operation plan to support this maneuver.
        /// </summary>
        /// <value>The type of contingency response. 1. LANDING   The operation will be landing by targeting the contingency_point. 2. LOITERING   The operation will loiter at the contingency_point at the specified altitude with the noted loiter_radius_ft. 3. RETURN_TO_BASE   The operation will return to base as specified by the contingency_point. The USS may issue an update to the operation plan to support this maneuver.</value>
        [DataMember(Name = "contingency_response", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_response")]
        [EnumDataType(typeof(ContingencyResponseEnum))]
        [Required]
        public ContingencyResponseEnum ContingencyResponse { get; set; }

        /// <summary>
        /// Gets or Sets ContingencyPoint
        /// </summary>
        [DataMember(Name = "contingency_polygon", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_polygon")]
        [PolygonValidator]
        [Required]
        public Polygon ContingencyPolygon { get; set; }

        /// <summary>
        /// The altitude for this contingency. Relevant and required for the LOITERING contingency_type, ignored for all other types. In WGS84 reference system using feet as units.
        /// </summary>
        /// <value>The altitude for this contingency. Relevant and required for the LOITERING contingency_type, ignored for all other types. In WGS84 reference system using feet as units.</value>
        [DataMember(Name = "loiter_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "loiter_altitude")]
        public Altitude LoiterAltitude { get; set; }

        /// <summary>
        /// Optional numerical value that can be used in ranking the preference of this Contingency versus any other within the set of Contingency for this operation. This may be thought of as a ranking of the potential landing sites with all other factors being held equal, though dynamic conditions will likely play a role in adjusting this ranking in real time by the USS or Operator.  For example, one Contingency may be significantly further from the operation at a given time and, thus, would be less preferred than it might be otherwise. Further interpretation of this field is left to the operator and USS.
        /// </summary>
        /// <value>Optional numerical value that can be used in ranking the preference of this Contingency versus any other within the set of Contingency for this operation. This may be thought of as a ranking of the potential landing sites with all other factors being held equal, though dynamic conditions will likely play a role in adjusting this ranking in real time by the USS or Operator.  For example, one Contingency may be significantly further from the operation at a given time and, thus, would be less preferred than it might be otherwise. Further interpretation of this field is left to the operator and USS. </value>
        [DataMember(Name = "relative_preference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "relative_preference")]
        [Range(0,double.MaxValue)]
        public double? RelativePreference { get; set; }

        /// <summary>
        /// Optional description of the contingency location.
        /// </summary>
        /// <value>Optional description of the contingency location.</value>
        [DataMember(Name = "contingency_location_description", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_location_description")]
        [EnumDataType(typeof(ContingencyLocationDescription))]
        [Required]
        public ContingencyLocationDescription ContingencyLocationDescription { get; set; }

        /// <summary>
        /// In the planning stage of an operation, this array may be populated with ordinals that correspond to the ordinal values supplied with each OperationVolume. This is an indicator that this particular ContingencyPlan is valid for use when the operation is active in any of the particular noted OperationVolumes.
        /// </summary>
        /// <value>In the planning stage of an operation, this array may be populated with ordinals that correspond to the ordinal values supplied with each OperationVolume. This is an indicator that this particular ContingencyPlan is valid for use when the operation is active in any of the particular noted OperationVolumes.</value>
        [DataMember(Name = "relevant_operation_volumes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "relevant_operation_volumes")]
        public List<int?> RelevantOperationVolumes { get; set; }

        /// <summary>
        /// Optional. Time that this location is expected to be first available. For example, if an operation begins at 1100, but this location is not available until 1105 at the earliest, then this field could indicate that fact. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Optional. Time that this location is expected to be first available. For example, if an operation begins at 1100, but this location is not available until 1105 at the earliest, then this field could indicate that fact. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "valid_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "valid_time_begin")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime ValidTimeBegin { get; set; }

        /// <summary>
        /// Optional. Time that this location is expected to become unavailable. For example, if an operation begins at 1100, but this location becomes closed for some reason at 1105, then this field could indicate that fact. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Optional. Time that this location is expected to become unavailable. For example, if an operation begins at 1100, but this location becomes closed for some reason at 1105, then this field could indicate that fact. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "valid_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "valid_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime ValidTimeEnd { get; set; }

        /// <summary>
        /// Free Text
        /// </summary>        
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [MinLength(0),MaxLength(1000)]
        public string FreeText { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ContingencyPlan {\n");
            sb.Append("  ContingencyId: ").Append(ContingencyId).Append("\n");
            sb.Append("  ContingencyCause: ").Append(ContingencyCause).Append("\n");
            sb.Append("  ContingencyResponse: ").Append(ContingencyResponse).Append("\n");
            sb.Append("  ContingencyPolygon: ").Append(ContingencyPolygon).Append("\n");
            sb.Append("  LoiterAltitudeWgs84Ft: ").Append(LoiterAltitude).Append("\n");
            sb.Append("  RelativePreference: ").Append(RelativePreference).Append("\n");
            sb.Append("  ContingencyLocationDescription: ").Append(ContingencyLocationDescription).Append("\n");
            sb.Append("  RelevantOperationVolumes: ").Append(RelevantOperationVolumes).Append("\n");
            sb.Append("  ValidTimeBegin: ").Append(ValidTimeBegin).Append("\n");
            sb.Append("  ValidTimeEnd: ").Append(ValidTimeEnd).Append("\n");
            sb.Append("  FreeText: ").Append(FreeText).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}