using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class JSendGridCellMetadataResponse
    { 
        /// <summary>
        /// Gets or Sets Status
        /// </summary>
        [Required]
        [DataMember(Name="status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or Sets HttpStatusCode
        /// </summary>
        [DataMember(Name = "http_status_code")]
        public int? HttpStatusCode { get; set; }

        /// <summary>
        /// The zxid, which is a long (64-bit) integer
        /// </summary>
        /// <value>The zxid, which is a long (64-bit) integer</value>
        [Required]
        [DataMember(Name="sync_token")]
        public long SyncToken { get; set; }

        /// <summary>
        /// Gets or Sets Data
        /// </summary>
        [Required]
        [DataMember(Name="data")]
        public GridCellMetadataResponse Data { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JSendGridCellMetadataResponse {\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  SyncToken: ").Append(SyncToken).Append("\n");
            sb.Append("  Data: ").Append(Data).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
