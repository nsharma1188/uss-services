using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Airport
    {
        /// <summary>
        /// Gets or Sets AirportId
        /// </summary>
        [DataMember(Name = "airport_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "airport_id")]
        public string AirportId { get; set; }

        /// <summary>
        /// Gets or Sets Beacon
        /// </summary>
        [DataMember(Name = "beacon", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "beacon")]
        public string Beacon { get; set; }

        /// <summary>
        /// Gets or Sets CycleDate
        /// </summary>
        [DataMember(Name = "cycle_date", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cycle_date")]
        public string CycleDate { get; set; }

        /// <summary>
        /// Gets or Sets Elev
        /// </summary>
        [DataMember(Name = "elev", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "elev")]
        public float? Elev { get; set; }

        /// <summary>
        /// Gets or Sets Geom2d
        /// </summary>
        [DataMember(Name = "geom_2d", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geom_2d")]
        public string Geom2d { get; set; }

        /// <summary>
        /// Gets or Sets Geom3d
        /// </summary>
        [DataMember(Name = "geom_3d", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geom_3d")]
        public string Geom3d { get; set; }

        /// <summary>
        /// Gets or Sets Gid
        /// </summary>
        [DataMember(Name = "gid", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gid")]
        public int? Gid { get; set; }

        /// <summary>
        /// Gets or Sets Hydro
        /// </summary>
        [DataMember(Name = "hydro", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "hydro")]
        public string Hydro { get; set; }

        /// <summary>
        /// Gets or Sets Icao
        /// </summary>
        [DataMember(Name = "icao", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "icao")]
        public string Icao { get; set; }

        /// <summary>
        /// Gets or Sets LocHdatum
        /// </summary>
        [DataMember(Name = "loc_hdatum", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "loc_hdatum")]
        public string LocHdatum { get; set; }

        /// <summary>
        /// Gets or Sets MagVar
        /// </summary>
        [DataMember(Name = "mag_var", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "mag_var")]
        public string MagVar { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Terrain
        /// </summary>
        [DataMember(Name = "terrain", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "terrain")]
        public string Terrain { get; set; }

        /// <summary>
        /// Gets or Sets WgsDatum
        /// </summary>
        [DataMember(Name = "wgs_datum", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "wgs_datum")]
        public string WgsDatum { get; set; }

        /// <summary>
        /// Gets or Sets WgsLat
        /// </summary>
        [DataMember(Name = "wgs_lat", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "wgs_lat")]
        public double? WgsLat { get; set; }

        /// <summary>
        /// Gets or Sets WgsLong
        /// </summary>
        [DataMember(Name = "wgs_long", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "wgs_long")]
        public double? WgsLong { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Airport {\n");
            sb.Append("  AirportId: ").Append(AirportId).Append("\n");
            sb.Append("  Beacon: ").Append(Beacon).Append("\n");
            sb.Append("  CycleDate: ").Append(CycleDate).Append("\n");
            sb.Append("  Elev: ").Append(Elev).Append("\n");
            sb.Append("  Geom2d: ").Append(Geom2d).Append("\n");
            sb.Append("  Geom3d: ").Append(Geom3d).Append("\n");
            sb.Append("  Gid: ").Append(Gid).Append("\n");
            sb.Append("  Hydro: ").Append(Hydro).Append("\n");
            sb.Append("  Icao: ").Append(Icao).Append("\n");
            sb.Append("  LocHdatum: ").Append(LocHdatum).Append("\n");
            sb.Append("  MagVar: ").Append(MagVar).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Terrain: ").Append(Terrain).Append("\n");
            sb.Append("  WgsDatum: ").Append(WgsDatum).Append("\n");
            sb.Append("  WgsLat: ").Append(WgsLat).Append("\n");
            sb.Append("  WgsLong: ").Append(WgsLong).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}