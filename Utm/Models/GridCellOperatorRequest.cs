using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using AnraUssServices.Common;

namespace AnraUssServices.Utm.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GridCellOperatorRequest
    { 
        /// <summary>
        /// Base URL for the USS&#39;s web service endpoints hosting the required NASA API (https://app.swaggerhub.com/apis/utm/uss/).
        /// </summary>
        /// <value>Base URL for the USS&#39;s web service endpoints hosting the required NASA API (https://app.swaggerhub.com/apis/utm/uss/).</value>
        [Required]
        [DataMember(Name="uss_baseurl")]
        public string UssBaseurl { get; set; }

        /// <summary>
        /// Earliest operation start time in this grid cell.
        /// </summary>
        /// <value>Earliest operation start time in this grid cell.</value>
        [Required]
        [DataMember(Name="minimum_operation_timestamp")]
        public string MinimumOperationTimestamp { get; set; }

        /// <summary>
        /// Latest operation end time in this grid cell.
        /// </summary>
        /// <value>Latest operation end time in this grid cell.</value>
        [Required]
        [DataMember(Name="maximum_operation_timestamp")]
        public string MaximumOperationTimestamp { get; set; }        

        /// <summary>
        /// The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.
        /// </summary>
        /// <value>The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.</value>
        [DataMember(Name="announcement_level")]
        public AnnouncementLevelEnum? AnnouncementLevel { get; set; }

        /// <summary>
        /// Gets or Sets Operations
        /// </summary>
        [Required]
        [DataMember(Name="operations")]
        public List<GridCellOperationRequest> Operations { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GridCellOperatorRequest {\n");
            sb.Append("  UssBaseurl: ").Append(UssBaseurl).Append("\n");
            sb.Append("  MinimumOperationTimestamp: ").Append(MinimumOperationTimestamp).Append("\n");
            sb.Append("  MaximumOperationTimestamp: ").Append(MaximumOperationTimestamp).Append("\n");
            sb.Append("  AnnouncementLevel: ").Append(AnnouncementLevel).Append("\n");
            sb.Append("  Operations: ").Append(Operations).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
