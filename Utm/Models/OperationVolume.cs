using AnraUssServices.Common;
using AnraUssServices.Common.Converters;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    [DataContract]
    public class OperationVolume
    {
        private DateTime? _effectiveTimeBegin = null;
        private DateTime? _effectiveTimeEnd = null;
        private DateTime? _actualTimeEnd = null;

        /// <summary>
        /// This integer represents the ordering of the operation volume within the set of operation volumes. Need not be consecutive integers.
        /// </summary>
        /// <value>This integer represents the ordering of the operation volume within the set of operation volumes. Need not be consecutive integers.</value>
        [DataMember(Name = "ordinal", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ordinal")]
        [Required]
        [Range(-1000,99999)]
        [JsonConverter(typeof(CustomIntConverter))]
        public int? Ordinal { get; set; }

        /// <summary>
        /// More description later.
        /// </summary>
        /// <value>More description later.</value>
        [DataMember(Name = "volume_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "volume_type")]
        [EnumDataType(typeof(VolumeType))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public VolumeType VolumeType { get; set; }

        /// <summary>
        /// Is this operation volume within 400' of a structure?
        /// </summary>
        /// <value>Is this operation volume within 400' of a structure?</value>
        [DataMember(Name = "near_structure", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "near_structure")]
        public bool? NearStructure { get; set; }

        /// <summary>
        /// Earliest time the operation will use the operation volume.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Earliest time the operation will use the operation volume.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime? EffectiveTimeBegin { get; set; }

        /// <summary>
        /// Latest time the operation will done with the operation volume. It must be greater than effective_time_begin.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Latest time the operation will done with the operation volume. It must be greater than effective_time_begin.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime? EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Time that the operational volume was freed for use by other operations.  Should be populated and stored by the USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Time that the operational volume was freed for use by other operations.  Should be populated and stored by the USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "actual_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ActualTimeEnd { get; set; }

        /// <summary>
        /// The minimum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.
        /// </summary>
        /// <value>The minimum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        [Required]
        public Altitude MinAltitude { get; set; }

        /// <summary>
        /// The maximum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.
        /// </summary>
        /// <value>The maximum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        [Required]
        public Altitude MaxAltitude { get; set; }

        /// <summary>
        /// Gets or Sets OperationGeography
        /// </summary>
        [DataMember(Name = "operation_geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_geography")]
        [Required]
        [PolygonValidator]
        public Polygon OperationGeography { get; set; }

        /// <summary>
        /// Describes whether the operation volume is beyond the visual line of sight of the RPIC.
        /// </summary>
        /// <value>Describes whether the operation volume is beyond the visual line of sight of the RPIC.</value>
        [DataMember(Name = "beyond_visual_line_of_sight", EmitDefaultValue = true)]
        [JsonProperty(PropertyName = "beyond_visual_line_of_sight")]
        [Required]
        public bool? BeyondVisualLineOfSight { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class OperationVolume {\n");
            sb.Append("  Ordinal: ").Append(Ordinal).Append("\n");
            sb.Append("  NearStructure: ").Append(NearStructure).Append("\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  EffectiveTimeEnd: ").Append(EffectiveTimeEnd).Append("\n");
            sb.Append("  ActualTimeEnd: ").Append(ActualTimeEnd).Append("\n");
            sb.Append("  MinAltitude: ").Append(MinAltitude).Append("\n");
            sb.Append("  MaxAltitude: ").Append(MaxAltitude).Append("\n");
            sb.Append("  OperationGeography: ").Append(OperationGeography).Append("\n");
            sb.Append("  BeyondVisualLineOfSight: ").Append(BeyondVisualLineOfSight).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}