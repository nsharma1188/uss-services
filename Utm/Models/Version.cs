using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Version
    {
        /// <summary>
        /// Title of the API
        /// </summary>
        /// <value>Title of the API</value>
        [DataMember(Name = "title", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        /// <summary>
        /// Version number for the API
        /// </summary>
        /// <value>Version number for the API</value>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "version")]
        public string Version1 { get; set; }

        /// <summary>
        /// Time of the latest build
        /// </summary>
        /// <value>Time of the latest build</value>
        [DataMember(Name = "build_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "build_time")]
        public string BuildTime { get; set; }

        /// <summary>
        /// URL(s) for API Documentation
        /// </summary>
        /// <value>URL(s) for API Documentation</value>
        [DataMember(Name = "api_docs", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "api_docs")]
        public List<string> ApiDocs { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Version {\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  Version: ").Append(Version1).Append("\n");
            sb.Append("  BuildTime: ").Append(BuildTime).Append("\n");
            sb.Append("  ApiDocs: ").Append(ApiDocs).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}