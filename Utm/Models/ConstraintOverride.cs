using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class ConstraintOverride
    {
        /// <summary>
        /// Gets or Sets ActualTimeEnd
        /// </summary>
        [DataMember(Name = "actual_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_time_end")]
        public string ActualTimeEnd { get; set; }

        /// <summary>
        /// The id of the constraint to be overridden.
        /// </summary>
        /// <value>The id of the constraint to be overridden.</value>
        [DataMember(Name = "constraint_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "constraint_id")]
        public string ConstraintId { get; set; }

        /// <summary>
        /// The table name of the constraint to be overridden.
        /// </summary>
        /// <value>The table name of the constraint to be overridden.</value>
        [DataMember(Name = "constraint_table_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "constraint_table_name")]
        public string ConstraintTableName { get; set; }

        /// <summary>
        /// Gets or Sets EffectiveTimeBegin
        /// </summary>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        public string EffectiveTimeBegin { get; set; }

        /// <summary>
        /// Gets or Sets ManagerIdEnd
        /// </summary>
        [DataMember(Name = "manager_id_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "manager_id_end")]
        public string ManagerIdEnd { get; set; }

        /// <summary>
        /// Gets or Sets ManagerIdStart
        /// </summary>
        [DataMember(Name = "manager_id_start", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "manager_id_start")]
        public string ManagerIdStart { get; set; }

        /// <summary>
        /// Gets or Sets OverrideId
        /// </summary>
        [DataMember(Name = "override_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "override_id")]
        public string OverrideId { get; set; }

        /// <summary>
        /// Gets or Sets PlannedTimeEnd
        /// </summary>
        [DataMember(Name = "planned_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "planned_time_end")]
        public string PlannedTimeEnd { get; set; }

        /// <summary>
        /// Gets or Sets SubmitTime
        /// </summary>
        [DataMember(Name = "submit_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time")]
        public string SubmitTime { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ConstraintOverride {\n");
            sb.Append("  ActualTimeEnd: ").Append(ActualTimeEnd).Append("\n");
            sb.Append("  ConstraintId: ").Append(ConstraintId).Append("\n");
            sb.Append("  ConstraintTableName: ").Append(ConstraintTableName).Append("\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  ManagerIdEnd: ").Append(ManagerIdEnd).Append("\n");
            sb.Append("  ManagerIdStart: ").Append(ManagerIdStart).Append("\n");
            sb.Append("  OverrideId: ").Append(OverrideId).Append("\n");
            sb.Append("  PlannedTimeEnd: ").Append(PlannedTimeEnd).Append("\n");
            sb.Append("  SubmitTime: ").Append(SubmitTime).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}