using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Metadata
    {
        /// <summary>
        /// The authorization server's issuer identifier, which is a URL that uses the \"https\" scheme and has no query or fragment components.  This is the location where \".well-known\" RFC 5785 resources containing information about the authorization server are published.  Using these well-known resources is described in Section 3.  The issuer identifier is used to prevent authorization server mix-up attacks, as described in \"OAuth 2.0 Mix-Up Mitigation\".
        /// </summary>
        /// <value>The authorization server's issuer identifier, which is a URL that uses the \"https\" scheme and has no query or fragment components.  This is the location where \".well-known\" RFC 5785 resources containing information about the authorization server are published.  Using these well-known resources is described in Section 3.  The issuer identifier is used to prevent authorization server mix-up attacks, as described in \"OAuth 2.0 Mix-Up Mitigation\".</value>
        [DataMember(Name = "issuer", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// URL of the authorization server's authorization endpoint [RFC6749].
        /// </summary>
        /// <value>URL of the authorization server's authorization endpoint [RFC6749].</value>
        [DataMember(Name = "authorization_endpoint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "authorization_endpoint")]
        public string AuthorizationEndpoint { get; set; }

        /// <summary>
        /// URL of the authorization server's token endpoint [RFC6749].  This is REQUIRED unless only the implicit grant type is used.
        /// </summary>
        /// <value>URL of the authorization server's token endpoint [RFC6749].  This is REQUIRED unless only the implicit grant type is used.</value>
        [DataMember(Name = "token_endpoint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "token_endpoint")]
        public string TokenEndpoint { get; set; }

        /// <summary>
        /// JSON array containing a list of the OAuth 2.0 [RFC6749] \"scope\" values that this authorization server supports. Servers MAY choose not to advertise some supported scope values even when this parameter is used.
        /// </summary>
        /// <value>JSON array containing a list of the OAuth 2.0 [RFC6749] \"scope\" values that this authorization server supports. Servers MAY choose not to advertise some supported scope values even when this parameter is used.</value>
        [DataMember(Name = "scopes_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "scopes_supported")]
        public List<Scope> ScopesSupported { get; set; }

        /// <summary>
        /// JSON array containing a list of the OAuth 2.0 \"response_type\" values that this authorization server supports. These values are required for responses from calls to the authorization endpoint, thus this array may be empty if no grant flows use the authorization endpoint, thus this server returns an empty array. (definitions in RFC 7591)
        /// </summary>
        /// <value>JSON array containing a list of the OAuth 2.0 \"response_type\" values that this authorization server supports. These values are required for responses from calls to the authorization endpoint, thus this array may be empty if no grant flows use the authorization endpoint, thus this server returns an empty array. (definitions in RFC 7591)</value>
        [DataMember(Name = "response_types_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "response_types_supported")]
        public List<string> ResponseTypesSupported { get; set; }

        /// <summary>
        /// JSON array containing a list of the OAuth 2.0 \"response_mode\" values that this authorization server supports, as specified in OAuth 2.0 Multiple Response Type Encoding Practices.  If omitted, the default is \"[\"query\", \"fragment\"]\".  The response mode value \"form_post\" is also defined in OAuth 2.0 Form Post Response Mode.dd
        /// </summary>
        /// <value>JSON array containing a list of the OAuth 2.0 \"response_mode\" values that this authorization server supports, as specified in OAuth 2.0 Multiple Response Type Encoding Practices.  If omitted, the default is \"[\"query\", \"fragment\"]\".  The response mode value \"form_post\" is also defined in OAuth 2.0 Form Post Response Mode.dd</value>
        [DataMember(Name = "response_modes_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "response_modes_supported")]
        public List<string> ResponseModesSupported { get; set; }

        /// <summary>
        /// JSON array containing a list of the OAuth 2.0 grant type values that this authorization server supports. (definitions in RFC 7591)
        /// </summary>
        /// <value>JSON array containing a list of the OAuth 2.0 grant type values that this authorization server supports. (definitions in RFC 7591)</value>
        [DataMember(Name = "grant_types_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "grant_types_supported")]
        public List<string> GrantTypesSupported { get; set; }

        /// <summary>
        /// JSON array containing a list of client authentication methods supported by this token endpoint.  Client authentication method values are used in the \"token_endpoint_auth_method\" parameter defined in Section 2 of [RFC7591].  If omitted, the default is \"client_secret_basic\" -- the HTTP Basic Authentication Scheme specified in Section 2.3.1 of OAuth 2.0 [RFC6749]. FIMS-Authz will only support private_key_jwt.
        /// </summary>
        /// <value>JSON array containing a list of client authentication methods supported by this token endpoint.  Client authentication method values are used in the \"token_endpoint_auth_method\" parameter defined in Section 2 of [RFC7591].  If omitted, the default is \"client_secret_basic\" -- the HTTP Basic Authentication Scheme specified in Section 2.3.1 of OAuth 2.0 [RFC6749]. FIMS-Authz will only support private_key_jwt.</value>
        [DataMember(Name = "token_endpoint_auth_methods_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "token_endpoint_auth_methods_supported")]
        public List<string> TokenEndpointAuthMethodsSupported { get; set; }

        /// <summary>
        /// JSON array containing a list of the JWS signing algorithms (alg values) supported by the token endpoint for the signature on the JWT [JWT] used to authenticate the client at the token endpoint for the \"private_key_jwt\" and \"client_secret_jwt\" authentication methods.  Servers SHOULD support \"RS256\". The value \"none\" MUST NOT be used.
        /// </summary>
        /// <value>JSON array containing a list of the JWS signing algorithms (alg values) supported by the token endpoint for the signature on the JWT [JWT] used to authenticate the client at the token endpoint for the \"private_key_jwt\" and \"client_secret_jwt\" authentication methods.  Servers SHOULD support \"RS256\". The value \"none\" MUST NOT be used.</value>
        [DataMember(Name = "token_endpoint_auth_signing_alg_values_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "token_endpoint_auth_signing_alg_values_supported")]
        public List<string> TokenEndpointAuthSigningAlgValuesSupported { get; set; }

        /// <summary>
        /// URL of a page containing human-readable information that developers might want or need to know when using the authorization server.  In particular, if the authorization server does not support Dynamic Client Registration, then information on how to register clients needs to be provided in this documentation.
        /// </summary>
        /// <value>URL of a page containing human-readable information that developers might want or need to know when using the authorization server.  In particular, if the authorization server does not support Dynamic Client Registration, then information on how to register clients needs to be provided in this documentation.</value>
        [DataMember(Name = "service_documentation", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "service_documentation")]
        public string ServiceDocumentation { get; set; }

        /// <summary>
        /// URL that the authorization server provides to the person registering the client to read about the authorization server's terms of service.  The registration process SHOULD display this URL to the person registering the client if it is given.
        /// </summary>
        /// <value>URL that the authorization server provides to the person registering the client to read about the authorization server's terms of service.  The registration process SHOULD display this URL to the person registering the client if it is given.</value>
        [DataMember(Name = "op_tos_uri", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "op_tos_uri")]
        public string OpTosUri { get; set; }

        /// <summary>
        /// URL of the authorization server's OAuth 2.0 revocation endpoint [RFC7009].
        /// </summary>
        /// <value>URL of the authorization server's OAuth 2.0 revocation endpoint [RFC7009].</value>
        [DataMember(Name = "revocation_endpoint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "revocation_endpoint")]
        public string RevocationEndpoint { get; set; }

        /// <summary>
        /// JSON array containing a list of client authentication methods supported by this revocation endpoint.  The valid client authentication method values are those registered in the IANA \"OAuth Token Endpoint Authentication Methods\" registry [IANA.OAuth.Parameters].
        /// </summary>
        /// <value>JSON array containing a list of client authentication methods supported by this revocation endpoint.  The valid client authentication method values are those registered in the IANA \"OAuth Token Endpoint Authentication Methods\" registry [IANA.OAuth.Parameters].</value>
        [DataMember(Name = "revocation_endpoint_auth_methods_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "revocation_endpoint_auth_methods_supported")]
        public List<string> RevocationEndpointAuthMethodsSupported { get; set; }

        /// <summary>
        /// URL of the authorization server's OAuth 2.0 introspection endpoint [RFC7662].
        /// </summary>
        /// <value>URL of the authorization server's OAuth 2.0 introspection endpoint [RFC7662].</value>
        [DataMember(Name = "introspection_endpoint", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "introspection_endpoint")]
        public string IntrospectionEndpoint { get; set; }

        /// <summary>
        /// JSON array containing a list of client authentication methods supported by this introspection endpoint.  The valid client authentication method values are those registered in the IANA \"OAuth Token Endpoint Authentication Methods\" registry [IANA.OAuth.Parameters] or those registered in the IANA \"OAuth Access Token Types\" registry [IANA.OAuth.Parameters].  (These values are and will remain distinct, due to Section 7.2.)
        /// </summary>
        /// <value>JSON array containing a list of client authentication methods supported by this introspection endpoint.  The valid client authentication method values are those registered in the IANA \"OAuth Token Endpoint Authentication Methods\" registry [IANA.OAuth.Parameters] or those registered in the IANA \"OAuth Access Token Types\" registry [IANA.OAuth.Parameters].  (These values are and will remain distinct, due to Section 7.2.)</value>
        [DataMember(Name = "introspection_endpoint_auth_methods_supported", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "introspection_endpoint_auth_methods_supported")]
        public List<string> IntrospectionEndpointAuthMethodsSupported { get; set; }

        /// <summary>
        /// Gets or Sets JwtClaims
        /// </summary>
        [DataMember(Name = "jwt_claims", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "jwt_claims")]
        public JwtClaimsSet JwtClaims { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Metadata {\n");
            sb.Append("  Issuer: ").Append(Issuer).Append("\n");
            sb.Append("  AuthorizationEndpoint: ").Append(AuthorizationEndpoint).Append("\n");
            sb.Append("  TokenEndpoint: ").Append(TokenEndpoint).Append("\n");
            sb.Append("  ScopesSupported: ").Append(ScopesSupported).Append("\n");
            sb.Append("  ResponseTypesSupported: ").Append(ResponseTypesSupported).Append("\n");
            sb.Append("  ResponseModesSupported: ").Append(ResponseModesSupported).Append("\n");
            sb.Append("  GrantTypesSupported: ").Append(GrantTypesSupported).Append("\n");
            sb.Append("  TokenEndpointAuthMethodsSupported: ").Append(TokenEndpointAuthMethodsSupported).Append("\n");
            sb.Append("  TokenEndpointAuthSigningAlgValuesSupported: ").Append(TokenEndpointAuthSigningAlgValuesSupported).Append("\n");
            sb.Append("  ServiceDocumentation: ").Append(ServiceDocumentation).Append("\n");
            sb.Append("  OpTosUri: ").Append(OpTosUri).Append("\n");
            sb.Append("  RevocationEndpoint: ").Append(RevocationEndpoint).Append("\n");
            sb.Append("  RevocationEndpointAuthMethodsSupported: ").Append(RevocationEndpointAuthMethodsSupported).Append("\n");
            sb.Append("  IntrospectionEndpoint: ").Append(IntrospectionEndpoint).Append("\n");
            sb.Append("  IntrospectionEndpointAuthMethodsSupported: ").Append(IntrospectionEndpointAuthMethodsSupported).Append("\n");
            sb.Append("  JwtClaims: ").Append(JwtClaims).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}