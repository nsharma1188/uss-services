﻿using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class NegotiationAgreement
    {
        /// <summary>
        /// An identifier unique to this particular message.
        /// </summary>
        /// <value>An identifier unique to this particular message.</value>
        [DataMember(Name = "message_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "message_id is not of the correct UUID version")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// An identifier held constant across multiple message_ids.  This value represents a single negotiation between two USSs for two specific operations.  Will be same value as used in NegotiationMessage for this particular negotiation between these operations.
        /// </summary>
        /// <value>An identifier held constant across multiple message_ids.  This value represents a single negotiation between two USSs for two specific operations.  Will be same value as used in NegotiationMessage for this particular negotiation between these operations.</value>
        [DataMember(Name = "negotiation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "negotiation_id")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "negotiation_id is not of the correct UUID version")]
        public Guid NegotiationId { get; set; }

        /// <summary>
        /// A name identifying the originator of this message. The maximum and minimum character length is based on a usable domain name
        /// </summary>
        /// <value>This name MUST be a duplicate of either uss_name_of_originator or uss_name_of_receiver. This field is required for security and data quality purposes.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]        
        [MinLength(4), MaxLength(250)]
        public string UssName { get; set; }

        /// <summary>
        /// Name of the USS originating the negotation. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>Name of the USS originating the negotation. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name_of_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_originator")]
        [Required]
        [MinLength(1), MaxLength(250)]
        public string UssNameOfOriginator { get; set; }

        /// <summary>
        /// Name of the USS receiving the original negotiation request. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>Name of the USS receiving the original negotiation request. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_receiver")]
        [Required]
        [MinLength(1), MaxLength(250)]        
        public string UssNameOfReceiver { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.</value>
        [DataMember(Name = "gufi_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_originator")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi_originator is not of the correct UUID version")]
        public Guid GufiOriginator { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the receiving USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS. 
        /// </summary>
        /// <value>The GUFI of the relevant operation of the receiving USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS. </value>
        [DataMember(Name = "gufi_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_receiver")]
        [Required]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi_receiver is not of the correct UUID version")]        
        public Guid GufiReceiver { get; set; }

        /// <summary>
        /// Gets or Sets FreeText
        /// </summary>
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [StringLength(1000)]
        public string FreeText { get; set; }

        /// <summary>
        /// An identifier that supplies information about the USS from the discovery perspecitve.  This field is currently vague due to research on the appropriate discovery approach.  It will be tightened when an operational system decides on the approach to discovery. For Gridded USS Discovery, this should be an x,y,z string where x and y are the coords of the grid and z is the zoom level.  For example:  \&quot;110,117,10\&quot; For FIMS Discovery, this should be the uss_instance_id which is a UUID.
        /// </summary>
        /// <value>An identifier that supplies information about the USS from the discovery perspecitve.  This field is currently vague due to research on the appropriate discovery approach.  It will be tightened when an operational system decides on the approach to discovery. For Gridded USS Discovery, this should be an x,y,z string where x and y are the coords of the grid and z is the zoom level.  For example:  \&quot;110,117,10\&quot; For FIMS Discovery, this should be the uss_instance_id which is a UUID.</value>
        [DataMember(Name = "discovery_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "discovery_reference")]        
        [MinLength(5), MaxLength(36)]
        public string DiscoveryReference { get; set; }

        /// <summary>
        /// The type of negotation result. INTERSECTION: both USSs agreed to intersect their operation plans. REPLAN: both USSs agreed on replan of receiving operation.  In this   case there would be no planned intersection of these operations.
        /// </summary>
        /// <value>The type of negotation result. INTERSECTION: both USSs agreed to intersect their operation plans. REPLAN: both USSs agreed on replan of receiving operation.  In this   case there would be no planned intersection of these operations.</value>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        [EnumDataType(typeof(NegotiationAgreementTypeEnum))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]       
        public NegotiationAgreementTypeEnum Type { get; set; }
    }
}