using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Operation
    {
        /// <summary>
        /// Created and assigned by USS. Validated as UUID version 4 specification.
        /// </summary>
        /// <value>Created and assigned by USS. Validated as UUID version 4 specification.</value>
        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$",ErrorMessage = "gufi is not of the correct UUID version")]
        [Required]
        public Guid Gufi { get; set; }

        /// <summary>
        /// Identity of the USS.  The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>Identity of the USS.  The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [Required]
        [MinLength(1),MaxLength(250)]
        public string UssName { get; set; }

        /// <summary>
        /// Time the operation submission was received by USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Time the operation submission was received by USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "submit_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]        
        public DateTime? SubmitTime { get; set; }

        /// <summary>
        /// A timestamp set by the USS any time the state of the operation is updated within the USS Network. An update may be minor or major, but if/when te Operation is shared in the USS Network as a PUT to its LUN, this field MUST reflect the time that update was provided.  When the operation is submitted for the first time to the LUN, this value MUST be equal to submit_time.  This value MUST be constant for each update. This means that all USSs have the same value for last_submitted_update_time even if, for example, one USS receives the update later than others due to HTTP retrys or is provided as a GET for this Operation.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted. This field is set and maintained by the USS managing the Operation and is communicated to other USSs.
        /// </summary>
        /// <value>A timestamp set by the USS any time the state of the operation is updated within the USS Network. An update may be minor or major, but if/when te Operation is shared in the USS Network as a PUT to its LUN, this field MUST reflect the time that update was provided.  When the operation is submitted for the first time to the LUN, this value MUST be equal to submit_time.  This value MUST be constant for each update. This means that all USSs have the same value for last_submitted_update_time even if, for example, one USS receives the update later than others due to HTTP retrys or is provided as a GET for this Operation.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted. This field is set and maintained by the USS managing the Operation and is communicated to other USSs.</value>
        [DataMember(Name = "update_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "update_time")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// Informative text about the aircraft. Not used by the UTM System. Only for human stakeholders.
        /// </summary>
        /// <value>Informative text about the aircraft. Not used by the UTM System. Only for human stakeholders.</value>
        [DataMember(Name = "aircraft_comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "aircraft_comments")]
        [StringLength(1000)]
        public string AircraftComments { get; set; }

        /// <summary>
        /// x,y,z string where x and y are the coords of the grid and z is the zoom level. For example: �110,117,10�
        /// </summary>
        [DataMember(Name = "discovery_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "discovery_reference")]
        //[MinLength(5), MaxLength(36)]
        public string DiscoveryReference { get; set; }


        /// <summary>
        /// Informative text about the operation. Not used by the UTM System. Only for human stakeholders.
        /// </summary>
        /// <value>Informative text about the operation. Not used by the UTM System. Only for human stakeholders.</value>
        [DataMember(Name = "flight_comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "flight_comments")]
        [StringLength(1000)]
        public string FlightComments { get; set; }

        /// <summary>
        /// Informative text about the operational volumes. Not used by the UTM System. Only for human stakeholders.
        /// </summary>
        /// <value>Informative text about the operational volumes. Not used by the UTM System. Only for human stakeholders.</value>
        [DataMember(Name = "volumes_description", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "volumes_description")]
        [StringLength(1000)]
        public string VolumesDescription { get; set; }

        /// <summary>
        /// The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.
        /// </summary>
        /// <value>The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.</value>
        [DataMember(Name = "uas_registrations", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uas_registrations")]
        [Required]
        [MinLength(1),MaxLength(1000)]
        public List<UasRegistration> UasRegistrations { get; set; }

        /// <summary>
        /// TBD
        /// </summary>
        /// <value>TBD</value>
        [DataMember(Name = "airspace_authorization", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "airspace_authorization")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "airspace_authorization is not of the correct UUID version")]
        [JsonConverter(typeof(GuidJsonConvertor))]
        public Guid? AirspaceAuthorization { get; set; }

        /// <summary>
        /// Optional. For use by USS for identification purposes.
        /// </summary>
        /// <value>Optional. For use by USS for identification purposes.</value>
        [DataMember(Name = "flight_number", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "flight_number")]
        [StringLength(100)]
        public string FlightNumber { get; set; }

        /// <summary>
        /// Contact
        /// </summary>
        [DataMember(Name = "contact", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contact")]
        [Required]
        public PersonOrOrganization Contact { get; set; }

        /// <summary>
        /// The current state of the operation.  Must be maintained by the USS. Some additional details in the USS Specification.  1. ACCEPTED   This operation has been deemed ACCEPTED by the supporting USS. This implies that the operation meets the requirements for operating in the airspace based on the type of operation submitted. 2. ACTIVATED   This operation is active. The transition from ACCEPTED to ACTIVATED is not an announced transition. The transition is implied based on the submitted start time of the operation (i.e. the effective_time_begin of the first OperationVolume). Note that an ACTIVATED operation is not necessarily airborne, but is assumed to be \"using\" the OperationVolumes that it has announced. 3. CLOSED   This operation is closed. It is not airborne and will not become airborne again. If the UAS and the crew will fly again, it would need to be as a new operation. A USS may announce the closure of any operation, but is not required to announce unless the operation was ROGUE or NONCOFORMING. 4. NONCORMING   See USS Specifcation for requirements to transition to this state. 5. ROGUE   See USS Specifcation for requirements to transition to this state.
        /// </summary>
        /// <value>The current state of the operation.  Must be maintained by the USS. Some additional details in the USS Specification.  1. ACCEPTED   This operation has been deemed ACCEPTED by the supporting USS. This implies that the operation meets the requirements for operating in the airspace based on the type of operation submitted. 2. ACTIVATED   This operation is active. The transition from ACCEPTED to ACTIVATED is not an announced transition. The transition is implied based on the submitted start time of the operation (i.e. the effective_time_begin of the first OperationVolume). Note that an ACTIVATED operation is not necessarily airborne, but is assumed to be \"using\" the OperationVolumes that it has announced. 3. CLOSED   This operation is closed. It is not airborne and will not become airborne again. If the UAS and the crew will fly again, it would need to be as a new operation. A USS may announce the closure of any operation, but is not required to announce unless the operation was ROGUE or NONCOFORMING. 4. NONCORMING   See USS Specifcation for requirements to transition to this state. 5. ROGUE   See USS Specifcation for requirements to transition to this state. </value>
        [DataMember(Name = "state", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "state")]
        [EnumDataType(typeof(OperationState))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public OperationState State { get; set; }

        /// <summary>
        /// Gets or Sets ControllerLocation
        /// </summary>
        [DataMember(Name = "controller_location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "controller_location")]
        [Required]
        [PointValidator]
        public Point ControllerLocation { get; set; }

        /// <summary>
        /// Gets or Sets GcsLocation
        /// </summary>
        [DataMember(Name = "gcs_location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gcs_location")]
        [PointValidator]
        public Point GcsLocation { get; set; }

        /// <summary>
        /// An array of ContingencyPlans wherein this operation may land if needed/required during operation. Aids in planning and communication during the execution of a contingency.
        /// </summary>
        /// <value>An array of ContingencyPlans wherein this operation may land if needed/required during operation. Aids in planning and communication during the execution of a contingency.</value>
        [DataMember(Name = "contingency_plans", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "contingency_plans")]
        [MinLength(1),MaxLength(250)]
        [Required]
        public List<ContingencyPlan> ContingencyPlans { get; set; }

        /// <summary>
        /// 1. PART_107   The operation follows FAA rule 107. Submission of such operations is mandatory  2. PART_107X   In general, operations are 107X if they are doing something that would require a waiver under current 107 rules. Submission of such operations is mandatory.  3. PART_101E   Submission of 101E would be required if operation is within 5 statute miles of an airport. Optional otherwise.  4. OTHER   Placeholder for other types of operations.
        /// </summary>
        /// <value>1. PART_107   The operation follows FAA rule 107. Submission of such operations is mandatory  2. PART_107X   In general, operations are 107X if they are doing something that would require a waiver under current 107 rules. Submission of such operations is mandatory.  3. PART_101E   Submission of 101E would be required if operation is within 5 statute miles of an airport. Optional otherwise.  4. OTHER   Placeholder for other types of operations.</value>
        [DataMember(Name = "faa_rule", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "faa_rule")]
        [EnumDataType(typeof(FaaRule))]
        [Required]
        [JsonConverter(typeof(EnumJsonConvertor))]
        public FaaRule FaaRule { get; set; }

        /// <summary>
        /// Gets or Sets PriorityElements
        /// </summary>
        [DataMember(Name = "priority_elements", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "priority_elements")]
        public PriorityElements PriorityElements { get; set; }

        /// <summary>
        /// The actual geographical information for the operation.  Maximum array length of 12 currently allowed.
        /// </summary>
        /// <value>The actual geographical information for the operation.  Maximum array length of 12 currently allowed.</value>
        [DataMember(Name = "operation_volumes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_volumes")]
        [Required]
        [MinLength(1),MaxLength(250)]
        public List<OperationVolume> OperationVolumes { get; set; }

        /// <summary>
        /// Gets or Sets Metadata
        /// </summary>
        [DataMember(Name = "metadata", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "metadata")]
        [Required]
        public EventMetadata Metadata { get; set; }

		/// <summary>
		/// negotiation_agreements
		/// </summary>        
		[DataMember(Name = "negotiation_agreements", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "negotiation_agreements")]
		[MinLength(1), MaxLength(250)]
		public List<NegotiationAgreement> NegotiationAgreements { get; set; }
    }
}