using AnraUssServices.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// Data elements used to describe the status of priority operation. Nominal operations need not include these data. priority_level must take one of the four values EMERGENCY, ALERT, CRITICAL, or NOTICE.  NOTICE must only be used with a priority_status of NONE.  The other values should be used based on impact to other operations and safety of people and property on the ground.
    /// </summary>
    [DataContract]
    public class PriorityElements
    {
        /// <summary>
        /// Gets or Sets PriorityLevel
        /// </summary>
        [DataMember(Name = "priority_level", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "priority_level")]
        [EnumDataType(typeof(Severity))]
        [Required]
        public Severity PriorityLevel { get; set; }

        /// <summary>
        /// If a PUBLIC_SAFETY operation is POSTed to any endpoint, the correct scope will be required. Any USS may POST an INFLIGHT_EMERGENCY with non-priority scopes.  NONE should only be used to indicate that an operation that previously had a priority status now has no priority status.
        /// </summary>
        /// <value>If a PUBLIC_SAFETY operation is POSTed to any endpoint, the correct scope will be required. Any USS may POST an INFLIGHT_EMERGENCY with non-priority scopes.  NONE should only be used to indicate that an operation that previously had a priority status now has no priority status.</value>
        [DataMember(Name = "priority_status", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "priority_status")]
        [EnumDataType(typeof(PriortyStatus))]
        [Required]
        public PriortyStatus PriorityStatus { get; set; }
    }
}