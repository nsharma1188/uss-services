﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AnraUssServices.Utm.Models
{
	public class DynamicRestrictionOccurence
	{
		/// <summary>
		/// A UUID assigned by the reporting USS for this instance of DynamicRestrictionOccurence.
		/// </summary>
		[DataMember(Name = "dr_occurence_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "dr_occurence_id")]
		public string dr_occurence_id { get; set; }

		/// <summary>
		/// GUFI of the operation managed by reporting USS involved in this exchange.
		/// </summary>
		[Required]
		[RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "uss_operator_exchange_id is not of the correct UUID version")]
		[StringLength(36)]
		[DataMember(Name = "gufi", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "gufi")]
		public Guid gufi { get; set; }

		/// <summary>
		/// The reporting USSs ID as known by USS Network/FIMS.
		/// </summary>
		[Required]
		[StringLength(36)]
		[DataMember(Name = "uss_name", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uss_name")]
		public string uss_name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DataMember(Name = "operator", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "operator")]
		public string operators { get; set; }

		/// <summary>
		/// The time that the USS received the ConstraintMessage describing this dynamic restriction. This would have been received by another USS or from FIMS. Same formatting rules as in other UTM exchanges(ms, ‘Z’).
		/// </summary>
		[Required]
		[StringLength(24)]
		[DataMember(Name = "time_uss_received", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "time_uss_received")]
		public string time_uss_received { get; set; }

		/// <summary>
		/// The time that the USS sent a message to an affected operation. Same formatting rules as in other UTM exchanges(ms, ‘Z’).
		/// </summary>
		[StringLength(24)]
		[DataMember(Name = "time_uss_sent", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "time_uss_sent")]
		public string time_uss_sent { get; set; }

		/// <summary>
		/// The time that the USS receives acknowledgement that the message was recevied. If an HTTP exchange this would be the time that the 20X was received. If some other exchange mechanism, then just the time that acknowledgement was received. Same formatting rules as in other UTM exchanges(ms, ‘Z’).
		/// </summary>
		[StringLength(24)]
		[DataMember(Name = "time_response_from_operator_received", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "time_response_from_operator_received")]
		public string time_response_from_operator_received { get; set; }

		/// <summary>
		/// How is the message sent to the operator? If ‘OTHER’ you must describe in ‘notes’ field. May expand this enumeration based on discussions with partners.
		/// </summary>
		[StringLength(24)]
		[DataMember(Name = "message_protocol", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "message_protocol")]
		public string message_protocol { get; set; }

		/// <summary>
		/// The actual data payload sent to the operator from the USS. May be difficult to apply a maxLength to this field, should discuss. This may be JSON formatted, so proper escaping of certain chars may be necessary.It may make sense to base64 encode this. Should discuss.
		/// </summary>
		[Required]
		[DataMember(Name = "message_to_operator", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "message_to_operator")]
		public string message_to_operator { get; set; }

		/// <summary>
		/// In terms of the test goals and the operator’s role in the test, did the operator respond correctly? Note that this might mean that the operator takes no action. For example, if the operator was expected to clear an area, did that operator clear the area? If the operator was allowed to stay in the area, did the operator understand that and continue operating? If the operator needed to recognize it was exempted from the DR, did the operator understand that fact and stay/enter the DR? If the DR did not affect the operator directly, did the operator recognize that and NOT change plans?
		/// </summary>
		[Required]
		[DataMember(Name = "successful_operator_response", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "successful_operator_response")]
		public bool successful_operator_response { get; set; }

		/// <summary>
		/// Notes
		/// </summary>
		[DataMember(Name = "notes", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "notes")]
		public string notes { get; set; }

	}
}
