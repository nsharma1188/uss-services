﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// A set of data describing the registration of the UAS that is to be used in an Operation.
    /// </summary>
    [DataContract]
    public class UasRegistration
    {
        /// <summary>
        /// A unique registration identifier, minted by the registration authority as a UUIDv4.
        /// </summary>
        /// <value>A unique registration identifier, minted by the registration authority as a UUIDv4.</value>
        [Required]
        [DataMember(Name = "registration_id")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "registration_id is not of the correct UUID version")]
        public Guid RegistrationId { get; set; }

        /// <summary>
        /// An Internet-reachable URL for the registration authority. More details to come, however, it is thought that this should be an endpoint allowing an unauthenticated GET to obtain metadata about the registrar.
        /// </summary>
        /// <value>An Internet-reachable URL for the registration authority. More details to come, however, it is thought that this should be an endpoint allowing an unauthenticated GET to obtain metadata about the registrar.</value>
        [Required]
        [DataMember(Name = "registration_location")]
        public string RegistrationLocation { get; set; }
    }
}