using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// The set of scopes used in UTM OAuth 2.0 implementation. Scopes take the form of &lt;namespace&gt;_&lt;operation&gt;.&lt;object&gt; where \&quot;operation\&quot; is the type of permission (read/write, etc.) and \&quot;object\&quot; is the type of thing the action is performed upon. A \&quot;write\&quot; action should be assumed to all read access to the subject as well as writing. The namespace may aid in deconfliction and clarity of scopes. Terms conform to INCITS 359.
    /// </summary>
    [DataContract]
    public class Scope
    {
        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Scope {\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}