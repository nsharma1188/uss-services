using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class SurveillanceOperation
    {
        /// <summary>
        /// Gets or Sets CoverageGeography
        /// </summary>
        [DataMember(Name = "coverage_geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "coverage_geography")]
        public GeographyFragment CoverageGeography { get; set; }

        /// <summary>
        /// Gets or Sets CreatedBy
        /// </summary>
        [DataMember(Name = "created_by", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "created_by")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or Sets DecisionTime
        /// </summary>
        [DataMember(Name = "decision_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "decision_time")]
        public DateTime? DecisionTime { get; set; }

        /// <summary>
        /// Must specify begin time
        /// </summary>
        /// <value>Must specify begin time</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        public DateTime? EffectiveTimeBegin { get; set; }

        /// <summary>
        /// Gets or Sets EffectiveTimeEnd
        /// </summary>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        public DateTime? EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Gets or Sets OtherInfo
        /// </summary>
        [DataMember(Name = "other_info", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "other_info")]
        public Object OtherInfo { get; set; }

        /// <summary>
        /// Must specify registration id
        /// </summary>
        /// <value>Must specify registration id</value>
        [DataMember(Name = "registration", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "registration")]
        public string Registration { get; set; }

        /// <summary>
        /// Gets or Sets State
        /// </summary>
        [DataMember(Name = "state", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        /// <summary>
        /// Must specify submit time
        /// </summary>
        /// <value>Must specify submit time</value>
        [DataMember(Name = "submit_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time")]
        public DateTime? SubmitTime { get; set; }

        /// <summary>
        /// Gets or Sets SubmitTimeReference
        /// </summary>
        [DataMember(Name = "submit_time_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time_reference")]
        public string SubmitTimeReference { get; set; }

        /// <summary>
        /// Gets or Sets SurveillanceGeographyString
        /// </summary>
        [DataMember(Name = "surveillance_geography_string", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "surveillance_geography_string")]
        public string SurveillanceGeographyString { get; set; }

        /// <summary>
        /// Gets or Sets SurveillanceOperationId
        /// </summary>
        [DataMember(Name = "surveillance_operation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "surveillance_operation_id")]
        public string SurveillanceOperationId { get; set; }

        /// <summary>
        /// Gets or Sets TimeReceived
        /// </summary>
        [DataMember(Name = "time_received", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_received")]
        public DateTime? TimeReceived { get; set; }

        /// <summary>
        /// Gets or Sets UserId
        /// </summary>
        [DataMember(Name = "user_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class SurveillanceOperation {\n");
            sb.Append("  CoverageGeography: ").Append(CoverageGeography).Append("\n");
            sb.Append("  CreatedBy: ").Append(CreatedBy).Append("\n");
            sb.Append("  DecisionTime: ").Append(DecisionTime).Append("\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  EffectiveTimeEnd: ").Append(EffectiveTimeEnd).Append("\n");
            sb.Append("  OtherInfo: ").Append(OtherInfo).Append("\n");
            sb.Append("  Registration: ").Append(Registration).Append("\n");
            sb.Append("  State: ").Append(State).Append("\n");
            sb.Append("  SubmitTime: ").Append(SubmitTime).Append("\n");
            sb.Append("  SubmitTimeReference: ").Append(SubmitTimeReference).Append("\n");
            sb.Append("  SurveillanceGeographyString: ").Append(SurveillanceGeographyString).Append("\n");
            sb.Append("  SurveillanceOperationId: ").Append(SurveillanceOperationId).Append("\n");
            sb.Append("  TimeReceived: ").Append(TimeReceived).Append("\n");
            sb.Append("  UserId: ").Append(UserId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}