using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class TokenKeyResponse
    {
        /// <summary>
        /// A designator for the algorithm used to sign the key.  Values defined by a Java specfication from Oracle (http://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html).
        /// </summary>
        /// <value>A designator for the algorithm used to sign the key.  Values defined by a Java specfication from Oracle (http://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html).</value>
        [DataMember(Name = "alg", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "alg")]
        public string Alg { get; set; }

        /// <summary>
        /// A key signed by the algorithm noted in 'alg' property.  The key is preceded by '-----BEGIN PUBLIC KEY-----\\n' without the single quotes and appended with '\\n-----END PUBLIC KEY-----' without the single quotes.
        /// </summary>
        /// <value>A key signed by the algorithm noted in 'alg' property.  The key is preceded by '-----BEGIN PUBLIC KEY-----\\n' without the single quotes and appended with '\\n-----END PUBLIC KEY-----' without the single quotes.</value>
        [DataMember(Name = "value", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class TokenKeyResponse {\n");
            sb.Append("  Alg: ").Append(Alg).Append("\n");
            sb.Append("  Value: ").Append(Value).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}