using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// The token provided by the FIMS Authorization Server is a JWS representing a JWT.  The token is actually a JWS Compact Serialization string as described in RFC 7515.  For clarity of documentation, we represent the pre-serialized components here as a JSON schema.  This may be problematic when using automated tools to validate the schema using this file as input.
    /// </summary>
    [DataContract]
    public class JwsCompactSerialization
    {
        /// <summary>
        /// Gets or Sets JoseHeader
        /// </summary>
        [DataMember(Name = "jose_header", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "jose_header")]
        public JoseHeader JoseHeader { get; set; }

        /// <summary>
        /// Gets or Sets JwsPayload
        /// </summary>
        [DataMember(Name = "jws_payload", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "jws_payload")]
        public JwtClaimsSet JwsPayload { get; set; }

        /// <summary>
        /// See RFC 7515 for details.
        /// </summary>
        /// <value>See RFC 7515 for details.</value>
        [DataMember(Name = "jws_signature", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "jws_signature")]
        public string JwsSignature { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JwsCompactSerialization {\n");
            sb.Append("  JoseHeader: ").Append(JoseHeader).Append("\n");
            sb.Append("  JwsPayload: ").Append(JwsPayload).Append("\n");
            sb.Append("  JwsSignature: ").Append(JwsSignature).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}