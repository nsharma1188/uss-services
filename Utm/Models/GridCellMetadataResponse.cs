using System;
using System.Text;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GridCellMetadataResponse
    {
        /// <summary>
        /// Gets or Sets Version
        /// </summary>
        [Required]
        [DataMember(Name = "version")]
        public int Version { get; set; }

        /// <summary>
        /// Forcing ms resolution and UTC only.
        /// </summary>
        /// <value>Forcing ms resolution and UTC only.</value>
        [Required]
        [DataMember(Name = "timestamp")]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or Sets Operators
        /// </summary>
        [DataMember(Name = "operators")]
        public List<GridCellOperatorResponse> Operators { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class GridCellMetadataResponse {\n");
            sb.Append("  Version: ").Append(Version).Append("\n");
            sb.Append("  Timestamp: ").Append(Timestamp).Append("\n");
            sb.Append("  Operators: ").Append(Operators).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
