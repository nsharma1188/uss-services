using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class Constraint
    {
        /// <summary>
        /// The maximum altitude.
        /// </summary>
        /// <value>The maximum altitude.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        public double? MaxAltitude { get; set; }

        /// <summary>
        /// The minimum altitude.
        /// </summary>
        /// <value>The minimum altitude.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        public double? MinAltitude { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Constraint {\n");
            sb.Append("  MaxAltitude: ").Append(MaxAltitude).Append("\n");
            sb.Append("  MinAltitude: ").Append(MinAltitude).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}