using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class ManagerConstraint
    {
        /// <summary>
        /// The beginning time of the manager constraint.
        /// </summary>
        /// <value>The beginning time of the manager constraint.</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        public DateTime? EffectiveTimeBegin { get; set; }

        /// <summary>
        /// The ending time of the manager constraint.
        /// </summary>
        /// <value>The ending time of the manager constraint.</value>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        public DateTime? EffectiveTimeEnd { get; set; }

        /// <summary>
        /// The geography of the manager constraint.
        /// </summary>
        /// <value>The geography of the manager constraint.</value>
        [DataMember(Name = "geog", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geog")]
        public string Geog { get; set; }

        /// <summary>
        /// Gets or Sets ManagerConstraintId
        /// </summary>
        [DataMember(Name = "manager_constraint_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "manager_constraint_id")]
        public string ManagerConstraintId { get; set; }

        /// <summary>
        /// The maximum altitude.
        /// </summary>
        /// <value>The maximum altitude.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        public double? MaxAltitude { get; set; }

        /// <summary>
        /// The minimum altitude.
        /// </summary>
        /// <value>The minimum altitude.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        public double? MinAltitude { get; set; }

        /// <summary>
        /// The reason for the manager constraint.
        /// </summary>
        /// <value>The reason for the manager constraint.</value>
        [DataMember(Name = "reason", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reason")]
        public string Reason { get; set; }

        /// <summary>
        /// Any additional remarks about the manager constraint.
        /// </summary>
        /// <value>Any additional remarks about the manager constraint.</value>
        [DataMember(Name = "remarks", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "remarks")]
        public string Remarks { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ManagerConstraint {\n");
            sb.Append("  EffectiveTimeBegin: ").Append(EffectiveTimeBegin).Append("\n");
            sb.Append("  EffectiveTimeEnd: ").Append(EffectiveTimeEnd).Append("\n");
            sb.Append("  Geog: ").Append(Geog).Append("\n");
            sb.Append("  ManagerConstraintId: ").Append(ManagerConstraintId).Append("\n");
            sb.Append("  MaxAltitude: ").Append(MaxAltitude).Append("\n");
            sb.Append("  MinAltitude: ").Append(MinAltitude).Append("\n");
            sb.Append("  Reason: ").Append(Reason).Append("\n");
            sb.Append("  Remarks: ").Append(Remarks).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}