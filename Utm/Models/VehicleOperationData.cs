﻿using System;
using System.Runtime.Serialization;

namespace AnraUssServices.Utm.Models
{
    public class VehicleOperationData
    {
        /// <summary>
        /// Gets or Sets RegistrationId
        /// </summary>
        [DataMember(Name = "registration_id")]
        public string RegistrationId { get; set; }

        /// <summary>
        /// Gets or Sets VehicleType
        /// </summary>
        [DataMember(Name = "vehicle_type")]
        public string VehicleType { get; set; }

        /// <summary>
        /// Gets or Sets OwnerName
        /// </summary>
        [DataMember(Name = "owner_name")]
        public string OwnerName { get; set; }

        /// <summary>
        /// Gets or Sets OwnerContact
        /// </summary>
        [DataMember(Name = "owner_contact")]
        public string OwnerContact { get; set; }

        /// <summary>
        /// Gets or Sets NNumber
        /// </summary>
        [DataMember(Name = "n_number")]
        public string NNumber { get; set; }

        /// <summary>
        /// Gets or Sets FaaRegistrationNumber
        /// </summary>
        [DataMember(Name = "faa_registration-number")]
        public string FaaRegistrationNumber { get; set; }

        /// <summary>
        /// Gets or Sets UssName
        /// </summary>
        [DataMember(Name = "uss_name")]
        public string UssName { get; set; }

        /// <summary>
        /// Gets or Sets UssInstanceId
        /// </summary>
        [DataMember(Name = "uss_instance_id")]
        public Guid? UssInstanceId { get; set; }

        /// <summary>
        /// Gets or Sets Gufi
        /// </summary>
        [DataMember(Name = "gufi")]
        public Guid? Gufi { get; set; }

        /// <summary>
        /// Gets or Sets OperationState
        /// </summary>
        [DataMember(Name = "operation_state")]
        public string OperationState { get; set; }

        /// <summary>
        /// Gets or Sets AdditionalDetails
        /// </summary>
        [DataMember(Name = "additional_details")]
        public string AdditionalDetails { get; set; }
    }
}