﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.Utm.Models
{
	public class SlippyTilesRequest
	{
		/// <summary>
		/// Gets or Sets Coordinates for slippy tiles
		/// </summary>
		[DataMember(Name = "coords", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "coords")]
		public string Coordinates { get; set; }

		/// <summary>
		/// Gets or Sets Coordinates Type for slippy tiles
		/// </summary>
		[DataMember(Name = "coord_type", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "coord_type")]
		public string Coordinates_Type { get; set; }



	}
}
