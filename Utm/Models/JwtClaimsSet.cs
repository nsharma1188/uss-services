using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class JwtClaimsSet
    {
        /// <summary>
        /// The \"iss\" (issuer) claim identifies the principal that issued the JWT. The URL of the FIMS Authorization Server.
        /// </summary>
        /// <value>The \"iss\" (issuer) claim identifies the principal that issued the JWT. The URL of the FIMS Authorization Server.</value>
        [DataMember(Name = "iss", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "iss")]
        public string Iss { get; set; }

        /// <summary>
        /// The \"sub\" (subject) claim identifies the principal that is the subject of the JWT.
        /// </summary>
        /// <value>The \"sub\" (subject) claim identifies the principal that is the subject of the JWT.</value>
        [DataMember(Name = "sub", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "sub")]
        public string Sub { get; set; }

        /// <summary>
        /// The \"exp\" (expiration time) claim identifies the expiration time on or after which the JWT MUST NOT be accepted for processing.  The processing of the \"exp\" claim requires that the current date/time MUST be before the expiration date/time listed in the \"exp\" claim.
        /// </summary>
        /// <value>The \"exp\" (expiration time) claim identifies the expiration time on or after which the JWT MUST NOT be accepted for processing.  The processing of the \"exp\" claim requires that the current date/time MUST be before the expiration date/time listed in the \"exp\" claim.</value>
        [DataMember(Name = "exp", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "exp")]
        public int? Exp { get; set; }

        /// <summary>
        /// The \"nbf\" (not before) claim identifies the time before which the JWT MUST NOT be accepted for processing.  The processing of the \"nbf\" claim requires that the current date/time MUST be after or equal to the not-before date/time listed in the \"nbf\" claim.
        /// </summary>
        /// <value>The \"nbf\" (not before) claim identifies the time before which the JWT MUST NOT be accepted for processing.  The processing of the \"nbf\" claim requires that the current date/time MUST be after or equal to the not-before date/time listed in the \"nbf\" claim.</value>
        [DataMember(Name = "nbf", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "nbf")]
        public int? Nbf { get; set; }

        /// <summary>
        /// The \"iat\" (issued at) claim identifies the time at which the JWT was issued.  This claim can be used to determine the age of the JWT.
        /// </summary>
        /// <value>The \"iat\" (issued at) claim identifies the time at which the JWT was issued.  This claim can be used to determine the age of the JWT.</value>
        [DataMember(Name = "iat", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "iat")]
        public int? Iat { get; set; }

        /// <summary>
        /// The \"jti\" (JWT ID) claim provides a unique identifier for the JWT. For this server, this is satisfied by the use of a UUID.
        /// </summary>
        /// <value>The \"jti\" (JWT ID) claim provides a unique identifier for the JWT. For this server, this is satisfied by the use of a UUID.</value>
        [DataMember(Name = "jti", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "jti")]
        public string Jti { get; set; }

        /// <summary>
        /// Gets or Sets Scope
        /// </summary>
        [DataMember(Name = "scope", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "scope")]
        public List<Scope> Scope { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class JwtClaimsSet {\n");
            sb.Append("  Iss: ").Append(Iss).Append("\n");
            sb.Append("  Sub: ").Append(Sub).Append("\n");
            sb.Append("  Exp: ").Append(Exp).Append("\n");
            sb.Append("  Nbf: ").Append(Nbf).Append("\n");
            sb.Append("  Iat: ").Append(Iat).Append("\n");
            sb.Append("  Jti: ").Append(Jti).Append("\n");
            sb.Append("  Scope: ").Append(Scope).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}