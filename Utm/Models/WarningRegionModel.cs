using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.Utm.Models
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class WarningRegionModel
    {
        /// <summary>
        /// Must specify confidence level
        /// </summary>
        /// <value>Must specify confidence level</value>
        [DataMember(Name = "confidence", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "confidence")]
        public int? Confidence { get; set; }

        /// <summary>
        /// Gets or Sets CreatedBy
        /// </summary>
        [DataMember(Name = "created_by", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "created_by")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Must specify expiration time
        /// </summary>
        /// <value>Must specify expiration time</value>
        [DataMember(Name = "expiration_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "expiration_time")]
        public DateTime? ExpirationTime { get; set; }

        /// <summary>
        /// Gets or Sets Geography
        /// </summary>
        [DataMember(Name = "geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geography")]
        public GeographyFragment Geography { get; set; }

        /// <summary>
        /// Gets or Sets OtherInfo
        /// </summary>
        [DataMember(Name = "other_info", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "other_info")]
        public Object OtherInfo { get; set; }

        /// <summary>
        /// Gets or Sets PreserveCurrent
        /// </summary>
        [DataMember(Name = "preserve_current", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "preserve_current")]
        public bool? PreserveCurrent { get; set; }

        /// <summary>
        /// Gets or Sets State
        /// </summary>
        [DataMember(Name = "state", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        /// <summary>
        /// Must specify submit time
        /// </summary>
        /// <value>Must specify submit time</value>
        [DataMember(Name = "submit_time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time")]
        public DateTime? SubmitTime { get; set; }

        /// <summary>
        /// Gets or Sets SubmitTimeReference
        /// </summary>
        [DataMember(Name = "submit_time_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "submit_time_reference")]
        public string SubmitTimeReference { get; set; }

        /// <summary>
        /// Must specify surveillance operations id
        /// </summary>
        /// <value>Must specify surveillance operations id</value>
        [DataMember(Name = "surveillance_operation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "surveillance_operation_id")]
        public string SurveillanceOperationId { get; set; }

        /// <summary>
        /// Gets or Sets TimeReceived
        /// </summary>
        [DataMember(Name = "time_received", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_received")]
        public string TimeReceived { get; set; }

        /// <summary>
        /// Gets or Sets UserId
        /// </summary>
        [DataMember(Name = "user_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or Sets WarningRegionId
        /// </summary>
        [DataMember(Name = "warning_region_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "warning_region_id")]
        public string WarningRegionId { get; set; }

        /// <summary>
        /// Get the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class WarningRegionModel {\n");
            sb.Append("  Confidence: ").Append(Confidence).Append("\n");
            sb.Append("  CreatedBy: ").Append(CreatedBy).Append("\n");
            sb.Append("  ExpirationTime: ").Append(ExpirationTime).Append("\n");
            sb.Append("  Geography: ").Append(Geography).Append("\n");
            sb.Append("  OtherInfo: ").Append(OtherInfo).Append("\n");
            sb.Append("  PreserveCurrent: ").Append(PreserveCurrent).Append("\n");
            sb.Append("  State: ").Append(State).Append("\n");
            sb.Append("  SubmitTime: ").Append(SubmitTime).Append("\n");
            sb.Append("  SubmitTimeReference: ").Append(SubmitTimeReference).Append("\n");
            sb.Append("  SurveillanceOperationId: ").Append(SurveillanceOperationId).Append("\n");
            sb.Append("  TimeReceived: ").Append(TimeReceived).Append("\n");
            sb.Append("  UserId: ").Append(UserId).Append("\n");
            sb.Append("  WarningRegionId: ").Append(WarningRegionId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Get the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}