﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Jal.HttpClient.Model;
using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AnraUssServices.Utm.Api
{
    public class UssVehicleRegistrationApi
    {
        private readonly string serviceUrl = "";
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
        private ILogger<string> logger;

        public UssVehicleRegistrationApi(AnraConfiguration configuration, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication,
            ILogger<string> logger)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.fimsAuthentication = fimsAuthentication;
            serviceUrl = configuration.UASServiceUrl;
            this.logger = logger;
        }

        public VehicleReg GetVehicleData(string uvin, string utmAuth)
        {

            //Get Vehicle Data Detail From UAS Server
                       

            VehicleReg vehicleData = null;

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(serviceUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Add("X-UTM-Auth", utmAuth);
                client.DefaultRequestHeaders.Add("Authorization", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_READ_UVIN));

                HttpResponseMessage response = client.GetAsync("uvins/" + uvin).Result;
                if (response.IsSuccessStatusCode)
                {
                    vehicleData = response.Content.ReadAsAsync<VehicleReg>().Result;
                }                
            }
            catch(Exception e)
            {                
                logger.LogInformation("Anra USS VehicleRegistrationApi:: GetVehicleData :: " + e.Message);
            }
            
            return vehicleData;

        }

        public VehiclePost registerVehicle(string utmAuthToken)
        {
            //Register Vehicle          

            try
            {
                HttpResponse httpResponse =  httpClient.PostVehicleRegistration(serviceUrl,fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_READ_UVIN), utmAuthToken);
                VehiclePost vP = httpResponse.Adapt<VehiclePost>();
                return vP;
            }
            catch (Exception e)
            {
                logger.LogInformation("Anra USS VehicleRegistrationApi:: registerVehicle :: " + e.Message);
                return null;
            }

        }

        private List<HttpHeader> GetDefaultHeaders(string authToken = "")
        {
            var headers = new List<HttpHeader>
            {
                new HttpHeader("content-type", "application/json")
            };            

            if (!string.IsNullOrEmpty(authToken))
            {
                headers.Add(new HttpHeader("X-UTM-Auth", authToken));
            }

            if (!string.IsNullOrEmpty(authToken))
            {
                headers.Add(new HttpHeader("Authorization", authToken));
            }

            return headers;
        }
    }
}
