﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AnraUssServices.Utm.Api
{
    public class InterUssApi
    {
        private readonly string serviceUrl = string.Empty;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;
		private readonly ILogger<InterUssApi> logger;

		public InterUssApi(AnraConfiguration configuration, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication,
			ILogger<InterUssApi> logger)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.fimsAuthentication = fimsAuthentication;
            serviceUrl = configuration.InterUssBaseUrl;
			this.logger = logger;
        }

        public JSendStatusResponse StatusGet()
        {
            return httpClient.Get<JSendStatusResponse>(serviceUrl + "/status").Adapt<JSendStatusResponse>();
        }

        public JSendSlippyResponse GetSlippyTilesInfo(int zoom, string coords, string coordtype)
        {
			var slippyTiles = new SlippyTilesRequest()
			{
				Coordinates = coords,
				Coordinates_Type = coordtype
			};

			//Made the change for now to fix the issue we were having related to the grid server while requesting slippy tiles 
			//return httpClient.Get<JSendSlippyResponse>(serviceUrl + $"/slippy/{zoom}?coords={coords}&coord_type={coordtype}").Adapt<JSendSlippyResponse>();

			return httpClient.Get<JSendSlippyResponse>(serviceUrl + $"/slippy/{zoom}", slippyTiles).Adapt<JSendSlippyResponse>();
		}

		public JSendGridCellMetadataResponse GetGridCellOperatorAsync(int zoom, int x, int y)
        {
            return httpClient.Get<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellOperator/{zoom}/{x}/{y}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse PutGridCellOperator(int zoom, int x, int y, GridCellOperatorRequest requestData, long syncToken)
        {
            return httpClient.Put(serviceUrl, $"/GridCellOperator/{zoom}/{x}/{y}", requestData, syncToken, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse PutGridCellOperation(int zoom, int x, int y, string gufi, GridCellOperationRequest requestData, long syncToken)
        {
            return httpClient.Put(serviceUrl, $"/GridCellOperation/{zoom}/{x}/{y}/{gufi}", requestData, syncToken, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public Operation GetOperatorOperationAsync(GridCellOperatorResponse opr, string gufi)
        {
			try
			{
				var result = httpClient.Get<Operation>($"{opr.UssBaseurl}/operations/{gufi}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION), opr.Uss);
				return result != null ? result.Adapt<Operation>() : null;
			} catch (Exception ex)
			{
				logger.LogError("Exception ::: InterUssApi :: GetOperatorOperationAsync : Operator : {0}", JsonConvert.SerializeObject(opr));
				return null;
			}
        }

        public JSendGridCellMetadataResponse DeleteGridCellOperator(int zoom, int x, int y)
        {
            return httpClient.Delete<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellOperator/{zoom}/{x}/{y}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse DeleteGridCellOperation(int zoom, int x, int y, Guid gufi)
        {
            return httpClient.Delete<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellOperation/{zoom}/{x}/{y}/{gufi}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse GetGridCellsOperator(int zoom, string coords, string coordtype = "polygon")
        {
            return httpClient.Get<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellsOperator/{zoom}?coords={coords}&coord_type={coordtype}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse PutGridCellsOperator(int zoom, GridCellsOperatorRequest requestData, long syncToken)
        {
            return httpClient.Put(serviceUrl, $"/GridCellsOperator/{zoom}", requestData, syncToken, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse DeleteGridCellsOperator(int zoom, string coords, string coordtype = "polygon")
        {
            return httpClient.Delete<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellsOperator/{zoom}?coords={coords}&coord_type={coordtype}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse PutGridCellsOperation(int zoom, string gufi, GridCellsOperationRequest requestData, long syncToken)
        {
            return httpClient.Put(serviceUrl, $"/GridCellsOperation/{zoom}/{gufi}", requestData, syncToken, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse DeleteGridCellsOperation(int zoom, Guid gufi, string coords, string coordtype = "polygon")
        {
            return httpClient.Delete<JSendGridCellMetadataResponse>(serviceUrl + $"/GridCellsOperation/{zoom}/{gufi}?coords={coords}&coord_type={coordtype}", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse DeleteUVR(int zoom, UASVolumeReservation uvr)
        {
            return httpClient.Delete(serviceUrl + $"/UVR/{zoom}/{uvr.MessageId}",uvr, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT)).Adapt<JSendGridCellMetadataResponse>();
        }

        public JSendGridCellMetadataResponse PutUVR(int zoom, UASVolumeReservation uvr)
        {
            return httpClient.Put(serviceUrl, $"/UVR/{zoom}/{uvr.MessageId}", uvr, 0, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT)).Adapt<JSendGridCellMetadataResponse>();
        }
    }
}