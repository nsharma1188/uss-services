﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Utm.Api
{
    public class UssApi
    {
        private readonly ILogger<UssApi> logger;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraHttpClient httpClient;

        public UssApi(ILogger<UssApi> logger, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication)
        {
            this.httpClient = httpClient;
            this.fimsAuthentication = fimsAuthentication;
            this.logger = logger;
        }

        public List<Operation> GetOperations(UssInstance uss, int duration)
        {
            var result = new List<Operation>();
            try
            {
                #if DEBUG
                    logger.LogInformation("GetOperations USS={0}, URL={1}", uss.UssName, uss.UssBaseCallbackUrl);
                #endif

                string[] states = { "ACCEPTED", "ACTIVATED", "NONCONFORMING", "ROGUE" };
                var submittime = Utilities.GetUtmFormatDateTimeString(DateTime.UtcNow.AddMinutes(duration));

                var url = uss.UssBaseCallbackUrl + "/operations?submit_time=" + submittime + "&state=" + String.Join(",", states);
                result = httpClient.Get<List<Operation>>(url, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION), uss.UssName).Adapt<List<Operation>>();
            }
            catch (Exception ex)
            {
                logger.LogError("UssApi::GetOperations ex {0}", ex);
            }

            return result;
        }

        public UTMRestResponse PostOperations(UssInstance uss, Operation operation)
        {
            return httpClient.Put(uss.UssBaseCallbackUrl, "/operations/" + operation.Gufi, operation, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_OPERATION)).Result;
        }
    }
}