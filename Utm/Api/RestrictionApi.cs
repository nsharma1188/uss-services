﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Microsoft.Extensions.Logging;
using System;

namespace AnraUssServices.Utm.Api
{
	public class RestrictionApi
	{
		private readonly FimsTokenProvider fimsAuthentication;
		private readonly AnraConfiguration configuration;
		private readonly AnraHttpClient httpClient;
		private readonly ILogger<RestrictionApi> logger;

		public RestrictionApi(AnraConfiguration configuration, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication,
			ILogger<RestrictionApi> logger)
		{
			this.configuration = configuration;
			this.fimsAuthentication = fimsAuthentication;
			this.httpClient = httpClient;
			this.logger = logger;
		}

		public void PutUVR(UASVolumeReservation uvr)
		{
			httpClient.Put(configuration.FimsBaseUrl + "/fims/uvrs/" + uvr.MessageId.ToString(), uvr, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT));
		}
	}
}
