﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;

namespace AnraUssServices.Utm.Api
{
    public class NegotiationApi
    {
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraHttpClient httpClient;

        public NegotiationApi(AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication)
        {
            this.httpClient = httpClient;
            this.fimsAuthentication = fimsAuthentication;
        }

        public UTMRestResponse PostNegotiationMessage(string ussCallbackUrl, NegotiationMessage negotiationMessage, string destinationUssName)
        {
            return httpClient.Put(ussCallbackUrl, $"/negotiations/{negotiationMessage.MessageId}", negotiationMessage, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE), destinationUssName).Result;
        }
    }
}