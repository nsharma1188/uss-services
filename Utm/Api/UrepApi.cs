﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Mapster;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Utm.Api
{
    public class UrepApi
    {
        private readonly string serviceUrl = "/urep";
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;

        public UrepApi(AnraConfiguration configuration, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.fimsAuthentication = fimsAuthentication;
            serviceUrl = configuration.FimsBaseUrl + "/urep";
        }

        public UTMRestResponse PostUrepInstance(Urep urep)
        {
            return httpClient.Post(serviceUrl + "/ureps", urep, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE)); 
        }

        public List<Urep> GetUreps()
        {
            string[] fields = {"air_temperature", "aircraft_sightings", "altitude","gufi","icing_intensity","icing_type","location","proximity","remarks","source","time_submitted","time_measured","time_received","turbulence_intensity","urep_id","user_id","visibility","weather","weather_intensity","wind_direction","wind_speed" };
            return httpClient.Get<List<Urep>>(serviceUrl + "/ureps?" + "fields=" + String.Join(",", fields), fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE), "fims-urep").Adapt<List<Urep>>();
        }
    }
}
