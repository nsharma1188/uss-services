﻿using AnraUssServices.Common;
using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Models;
using Mapster;
using System.Collections.Generic;

namespace AnraUssServices.Utm.Api
{
    public class UssDiscoveryApi
    {
        private readonly string serviceUrl = "/ussdisc";
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient httpClient;

        public UssDiscoveryApi(AnraConfiguration configuration, AnraHttpClient httpClient, FimsTokenProvider fimsAuthentication)
        {
            this.httpClient = httpClient;
            this.configuration = configuration;
            this.fimsAuthentication = fimsAuthentication;
            serviceUrl = configuration.FimsBaseUrl + "/ussdisc";
        }

        public List<Models.UssInstance> GetUssList()
        {
            return httpClient.Get<List<Models.UssInstance>>(serviceUrl + "/uss", fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT),"uss-discovery").Adapt<List<Models.UssInstance>>();
        }

        public UTMRestResponse PutUssInstance(UssInstance ussInstance)
        {
            return httpClient.Put(serviceUrl, "/uss/" + ussInstance.UssInstanceId, ussInstance, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT), ussInstance.UssName).Result;
        }

        public List<UssInstance> GetUssListForVehicle(double? lat, double? lon)
        {
            return httpClient.Get<List<UssInstance>>(serviceUrl + "/uss?" +
                "&contains_point=" + lon + "," + lat, fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT),"uss-discovery").Adapt<List<UssInstance>>();
        }
    }
}