﻿using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;

namespace AnraUssServices.Utm.Api
{
    public class RelmatechApi
    {
        private readonly ILogger<RelmatechApi> logger;        
        private readonly AnraHttpClient httpClient;
        private readonly AnraConfiguration configuration;

        public RelmatechApi(AnraConfiguration configuration, ILogger<RelmatechApi> logger, AnraHttpClient httpClient)
        {
            this.httpClient = httpClient;
            this.logger = logger;
            this.configuration = configuration;
        }        

        public RelmatechLoginResponse DoLogin()
        {
            string url = string.Concat(configuration.RelmaTechBaseUrl, "api/loginVerification");

            var requestData = new RelmatechLoginRequest();
            requestData.userName = configuration.RelmaTechUserName;
            requestData.password = configuration.RelmaTechMd5Pwd;

            var client = new System.Net.Http.HttpClient();
            var requestBody = new System.Net.Http.StringContent(JsonConvert.SerializeObject(requestData), System.Text.Encoding.UTF8, "application/json");
            var response = client.PostAsync(url, requestBody).Result;
            var result = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<RelmatechLoginResponse>(result);
            //return httpClient.Post(string.Concat(configuration.RelmaTechBaseUrl, "api/loginVerification"),requestData,string.Empty);
        }
    }

    public class RelmatechLoginRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
    }

    public class RelmatechLoginResponse
    {
        public int result { get; set; }
        public string sessionKey { get; set; }
        public RelmatechAllData allData { get; set; }
        public int role { get; set; }
        public string message { get; set; }
    }

    public class RelmatechAllData
    {
        public int user_id { get; set; }
        public string name { get; set; }
        public int role { get; set; }
        public string username { get; set; }
        public string email { get; set; }
    }
}