﻿using Newtonsoft.Json;
using System;

namespace AnraUssServices.Utm.Models
{
    public static class HttpResponseExtension
    {
        public static UTMRestResponse ToUTMRestResponse(this Jal.HttpClient.Model.HttpResponse httpResponse)
        {
            if (httpResponse.HttpStatusCode.HasValue)
            {
                var content = httpResponse.Content.Read();
                UTMRestResponse response = null;
                if (string.IsNullOrEmpty(content))
                {
                    response = new UTMRestResponse { HttpStatusCode = (int)httpResponse.HttpStatusCode, Message = "Request Processed." };
                }
                else
                {
                    try
                    {
                        response = JsonConvert.DeserializeObject<UTMRestResponse>(content);
                        response.HttpStatusCode = (int)httpResponse.HttpStatusCode;
                    }
                    catch (Exception ex)
                    {
                        response = new UTMRestResponse { Message = httpResponse.HttpStatusCode.ToString(), HttpStatusCode = (int)httpResponse.HttpStatusCode };
                    }                    
                }

                return response;
            }
            else
            {
                return new UTMRestResponse {  Message = "Missing HTTP Response Code in response." };
            }

        }

        public static JSendGridCellMetadataResponse ToGridCellMetadataResponse(this Jal.HttpClient.Model.HttpResponse httpResponse)
        {
            if (httpResponse.HttpStatusCode.HasValue)
            {
                var content = httpResponse.Content.Read();
                JSendGridCellMetadataResponse response = null;
                if (string.IsNullOrEmpty(content))
                {
                    response = new JSendGridCellMetadataResponse { Status = httpResponse.HttpStatusCode.ToString(), HttpStatusCode = (int)httpResponse.HttpStatusCode };
                }
                else
                {
                    try
                    {
                        response = JsonConvert.DeserializeObject<JSendGridCellMetadataResponse>(content);
                        response.Status = httpResponse.HttpStatusCode.ToString();
                        response.HttpStatusCode = (int)httpResponse.HttpStatusCode;
                    }
                    catch (Exception ex)
                    {
                        response = new JSendGridCellMetadataResponse { Status = httpResponse.HttpStatusCode.ToString(), HttpStatusCode = (int)httpResponse.HttpStatusCode };
                    }
                }

                return response;
            }
            else
            {
                return new JSendGridCellMetadataResponse { Status = "Missing HTTP Response Code in response." };
            }

        }

        public static JSendSlippyResponse ToSlippyResponse(this Jal.HttpClient.Model.HttpResponse httpResponse)
        {
            if (httpResponse.HttpStatusCode.HasValue)
            {
                var content = httpResponse.Content.Read();
                JSendSlippyResponse response = null;
                if (string.IsNullOrEmpty(content))
                {
                    response = new JSendSlippyResponse { Status = httpResponse.HttpStatusCode.ToString() };
                }
                else
                {
                    response = JsonConvert.DeserializeObject<JSendSlippyResponse>(content);
                    response.Status = httpResponse.HttpStatusCode.ToString();
                    response.HttpStatusCode = (int)httpResponse.HttpStatusCode;
                }

                return response;
            }
            else
            {
                return new JSendSlippyResponse { Message = "Missing HTTP Response Code in response." };
            }

        }
    }
}