﻿using NpgsqlTypes;
using System.Collections.Generic;

namespace AnraUssServices.Utm.Models
{
    public static class PointExtension
    {
        public static NpgsqlPoint ToNpgsqlPoint(this Point point)
        {
            return new NpgsqlPoint(point.Coordinates[0].HasValue ? point.Coordinates[0].Value : 0, point.Coordinates[1].HasValue ? point.Coordinates[1].Value : 0);
        }

        public static NpgsqlPoint? ToNpgsqlPointNullable(this Point point)
        {
            return new NpgsqlPoint(point == null || point.Coordinates == null ? 0 : point.Coordinates[0].Value, point == null || point.Coordinates == null ? 0 : point.Coordinates[1].Value);
        }

        public static Point ToPoint(this NpgsqlTypes.NpgsqlPoint npgsqlpoint)
        {
            var points = new List<double?>(); 
            points.Add(npgsqlpoint.X);
            points.Add(npgsqlpoint.Y);
             
            var obj = new Point
            {
                Type = nameof(Point),
                Coordinates = new List<double?> { npgsqlpoint.X, npgsqlpoint.Y }
            };

            return obj;
        }

        public static Point ToPointNullable(this NpgsqlTypes.NpgsqlPoint? npgsqlpoint)
        {
            var obj = new Point
            {
                Type = nameof(Point),
                Coordinates = new List<double?> { npgsqlpoint.HasValue ? npgsqlpoint.Value.X : 0, npgsqlpoint.HasValue ? npgsqlpoint.Value.Y : 0 }
            };

            return obj;
        }
    }
}