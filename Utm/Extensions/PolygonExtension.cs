﻿using AnraUssServices.Dss.Models;
using GeoAPI.Geometries;
using NpgsqlTypes;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Utm.Models
{
    public static class PolygonExtension
    {
        public static NpgsqlPolygon ToNpgsqlPolygon(this Polygon polygon)
        {
            var points = new List<NpgsqlPoint>();

            foreach (var point in polygon.Coordinates[0])
            {
                points.Add(new NpgsqlTypes.NpgsqlPoint(point[0].Value, point[1].Value));
            }

            var obj = new NpgsqlPolygon(points);
            return obj;
        }

        public static NpgsqlPolygon? ToNpgsqlPolygonNullable(this Polygon polygon)
        {
            var points = new List<NpgsqlPoint>();

            foreach (var point in polygon.Coordinates[0])
            {
                points.Add(new NpgsqlTypes.NpgsqlPoint(point[0].Value, point[1].Value));
            }

            var obj = new NpgsqlPolygon(points);
            return obj;
        }

        public static Polygon ToPolygon(this NpgsqlPolygon npgsqlpolygon)
        {
            var points = new List<List<double?>>();

            for (int i=0;i < npgsqlpolygon.Count;i++)
            {
                var point = new List<double?>
                {
                    npgsqlpolygon[i].X,
                    npgsqlpolygon[i].Y
                };
                points.Add(point);
            }

            var obj = new Polygon
            {
                Type = @"Polygon",
                Coordinates = new List<List<List<double?>>>()
            };
            obj.Coordinates.Add(points);
            return obj;
        }



        public static List<Coordinate> ToNTSCoordinates(this NpgsqlPolygon npgsqlpolygon)
        {
            var points = new List<Coordinate>();

            for (int i = 0; i < npgsqlpolygon.Count; i++)
            {
                var point = new Coordinate(npgsqlpolygon[i].X, npgsqlpolygon[i].Y);
                points.Add(point);
            }

            return points;
        }

        public static Polygon ToPolygonNullable(this NpgsqlPolygon? npgsqlpolygon)
        {
            var points = new List<List<double?>>();

            for (int i = 0; i < (npgsqlpolygon.HasValue ? npgsqlpolygon.Value.Count : 1); i++)
            {
                var point = new List<double?>();
                point.Add(npgsqlpolygon.HasValue ? npgsqlpolygon.Value[i].X : 0);
                point.Add(npgsqlpolygon.HasValue ? npgsqlpolygon.Value[i].Y : 0);
                points.Add(point);
            }

            var obj = new Polygon();
            obj.Type = "Polygon";
            obj.Coordinates = new List<List<List<double?>>>();
            obj.Coordinates.Add(points);
            return obj;
        }

        public static GeoPolygon ToGeoPolygon(this Polygon polygon)
        {
            var geoPolygon = new GeoPolygon();

            var points = new List<LatLngPoint>();

            foreach (var point in polygon.Coordinates[0])
            {
                var geopoint = new LatLngPoint();

                geopoint.Lng = point[0].Value;
                geopoint.Lat = point[1].Value;
                points.Add(geopoint);
            }

            if (points.Any())
            {
                geoPolygon.Vertices = points;
            }

            return geoPolygon;
        }
    }
}