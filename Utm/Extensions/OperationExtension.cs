﻿namespace AnraUssServices.Utm.Models
{
    public static class OperationExtension
    {
        public static bool IsActiveOrAccepted(this Operation operation)
        {
            return (operation.State == Common.OperationState.ACCEPTED || operation.State == Common.OperationState.ACTIVATED);
        }

        public static bool IsClosed(this Operation operation)
        {
            return (operation.State == Common.OperationState.CLOSED);
        }

        public static bool IsPublicSafety(this Operation operation)
        {
            return (operation.PriorityElements != null && operation.PriorityElements.PriorityStatus == Common.PriortyStatus.PUBLIC_SAFETY);
        }

        public static bool IsEmergency(this Operation operation)
        {
            return (operation.PriorityElements != null && operation.PriorityElements.PriorityStatus.ToString().Contains("EMERGENCY"));
        }

        public static bool IsActiveOrInEmergency(this Operation operation)
        {
            return operation.State == Common.OperationState.ACTIVATED || operation.State == Common.OperationState.NONCONFORMING || operation.State == Common.OperationState.ROGUE;
        }

        public static bool HavingSamePriority(this Operation externalOperation, Operation conflictedOperation)
        {
            if (externalOperation.PriorityElements != null && conflictedOperation.PriorityElements != null)
                return externalOperation.PriorityElements.PriorityStatus.Equals(conflictedOperation.PriorityElements.PriorityStatus);
            else
                return false;
        }

        public static bool HavingSameState(this Operation externalOperation, Operation conflictedOperation)
        {
            return externalOperation.State.Equals(conflictedOperation.State);
        }

        public static bool HavingHigherRelativePriority(this Operation externalOperation, Operation conflictedOperation)
        {
            if (externalOperation.PriorityElements != null && conflictedOperation.PriorityElements != null)
                return (int)externalOperation.PriorityElements.PriorityStatus > (int)conflictedOperation.PriorityElements.PriorityStatus;
            else
                return externalOperation.PriorityElements != null ? true : false;
        }

        public static bool HavingHigherState(this Operation externalOperation, Operation conflictedOperation)
        {
            return (int)externalOperation.State > (int)conflictedOperation.State;
        }

        public static bool HavingLowestGufi(this Operation externalOperation, Operation conflictedOperation)
        {
            return externalOperation.Gufi.CompareTo(conflictedOperation.Gufi) == -1;
        }

        /// <summary>
        /// Determines if the operation is Primary when compared with the Conflicted Operation based on Priorty Status
        /// </summary>
        /// <param name="externalOperation"></param>
        /// <param name="conflictedOperation"></param>
        /// <returns></returns>
        public static bool IsPrimary(this Operation externalOperation, Operation conflictedOperation)
        {
            var isPrimary = false;

            //if ((externalOperation.IsEmergency() || externalOperation.IsPublicSafety()) && (!conflictedOperation.IsPublicSafety() || !conflictedOperation.IsEmergency()))
            //{
            //    isPrimary = true;
            //}
            //else
            //{
            //    if (externalOperation.IsPublicSafety() && conflictedOperation.IsPublicSafety())
            //    {
            //        isPrimary = true;
            //    }
            //    else if ((!externalOperation.IsPublicSafety() && conflictedOperation.IsPublicSafety()) || (!externalOperation.IsEmergency() && conflictedOperation.IsEmergency()))
            //    {
            //        isPrimary = false;
            //    }
            //}

            /*
             USS uses the following ordering algorithm: we now first check the priority (priority_elements.priority_status: INFLIGHT_EMERGENCY > PUBLIC_SAFETY > NONE),
             then the operation state (state: ACTIVATED or NONCONFORMING or ROGUE > CREATED), then the gufi (lowest gufi is secondary)
             */

            //Check For Relative Priority
            if (externalOperation.HavingSamePriority(conflictedOperation))
            {
                //Check For Operation State
                if (externalOperation.HavingSameState(conflictedOperation))
                {
                    //Check For Lowest Gufi
                    isPrimary = externalOperation.HavingLowestGufi(conflictedOperation);
                }
                else
                {
                    //Resolve Am i Primary - Operation State
                    isPrimary = externalOperation.HavingHigherState(conflictedOperation);
                }
            }
            else
            {
                //Resolve Am i Primary - Relative Priority
                isPrimary = externalOperation.HavingHigherRelativePriority(conflictedOperation);
            }

            return isPrimary;
        }
    }
}