﻿
using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.NearEarth
{
    [DataContract]
    public class LandingZoneEvaluation
    {
        /// <summary>
        /// Created and assigned by USS. Validated as UUID version 4 specification.
        /// </summary>
        /// <value>Created and assigned by USS. Validated as UUID version 4 specification.</value>
        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        [RegularExpression(@"^[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-4[0-9a-fA-F]{3}\-[8-b][0-9a-fA-F]{3}\-[0-9a-fA-F]{12}$", ErrorMessage = "gufi is not of the correct UUID version")]
        [Required]
        public Guid Gufi { get; set; }

        /// <summary>
        /// UTC time 
        /// </summary>
        [DataMember(Name = "time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time")]
        //[Required]                
        public Time time;

        /// <summary>
        /// landing coordinates specified in original landing waypoint
        /// </summary>        
        [DataMember(Name = "nominal_point", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "nominal_point")]
        //[Required]
        public Coordinates nominal_point;

		/// <summary>
		/// True when safe_point_found is a valid safe landing point,
		/// False is no safe landing point was found
		/// </summary>        
		[DataMember(Name = "safe_point_found", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "safe_point_found")]
        //[Required]
        public bool safe_point_found;

		/// <summary>
		/// If safe_point_found is True, selected_point is the safe landing point chosen for landing. 
		/// If safe_point_found is False, selected point does not contain valid data.
		/// </summary>
		[DataMember(Name = "selected_point", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "selected_point")]        
        public Coordinates selected_point;

		/// <summary>
		/// number of values in a grid row
		/// </summary>        
		[DataMember(Name = "grid_width", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "grid_width")]
        public uint grid_width;

		/// <summary>
		/// number of values in a grid column
		/// </summary>        
		[DataMember(Name = "grid_height", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "grid_height")]
		public uint grid_height;

		/// <summary>
		/// the row of the grid cell corresponding to the selected_point. The first row is row 0.
		/// </summary>        
		[DataMember(Name = "selected_point_row", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "selected_point_row")]
		public uint selected_point_row;

		/// <summary>
		/// the column of the grid cell corresponding to the selected_point. The first column is column 0.
		/// </summary>        
		[DataMember(Name = "selected_point_column", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "selected_point_column")]
		public uint selected_point_column;

		/// <summary>
		/// the landing zone represented as a grid of equally-sized square areas with dimensions grid_width by grid_height. 
		/// Each grid value indicates the land-ability of the associated square area. The grid is 0-indexed and values are stored 
		/// in row-major order; for example, the grid value associated with the selected_point is grid[selected_point_row * grid_width + selected_point_column]
		/// </summary>        
		[DataMember(Name = "zone", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "zone")]
        public Landability[] grid;
    }

    [DataContract]
    public class LandingZoneMessage
    {
        [DataMember(Name = "landing_point", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "landing_point")]
        public Point LandingPoint { get; set; }

        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [StringLength(1000)]
        public string FreeText { get; set; }
    }

    public class Time
    {
        // seconds in UTC
        public uint sec;
        // nanoseconds in UTC
        public uint nsec;
    }

    public class Coordinates
    {
        // latitude
        public double lat;
        //longitude
        public double lon;
    }

	[JsonConverter(typeof(StringEnumConverter))]
	public enum Landability
	{
		// Not determined to be LANDABLE or NOT_LANDABLE
		[Description(nameof(UNKNOWN)), EnumMember(Value = "UNKNOWN ")]
		UNKNOWN = 0,

		// Not safe to land here
		[Description(nameof(NOT_LANDABLE)), EnumMember(Value = "NOT_LANDABLE  ")]
		NOT_LANDABLE = 1,

		// Safe to land here
		[Description(nameof(LANDABLE)), EnumMember(Value = "LANDABLE  ")]
		LANDABLE = 2

	}
}
