﻿using AnraUssServices.ViewModel.Detects;
using AnraUssServices.ViewModel.TelemetryMessages;
using LiteDB;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Collisions
{
    [DataContract]    
    public class CollisionViewModel
    {
        [BsonId]
        [DataMember(Name = "collision_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "collision_id")]
        public Guid CollisionId { get; set; }

        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        public Guid Gufi { get; set; }

        [DataMember(Name = "uuid", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uuid")]
        public string Uid { get; set; }

        [DataMember(Name = "time_stamp", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_stamp")]
        public DateTime TimeStamp { get; set; }

        [DataMember(Name = "detect_details", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "detect_details")]
        public DetectViewModel Detect { get; set; }

        [DataMember(Name = "postion_details", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "postion_details")]
        public TelemetryMessageViewModel Position { get; set; }

        [DataMember(Name = "distance_3d", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "distance_3d")]
        public Double Distance3D { get; set; }

        [DataMember(Name = "threshold", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "threshold")]
        public int Threshold { get; set; }

    }
}