﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class RequestStatus
    {
		[JsonProperty("status")]
		public StatusEnum Status { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }
    }
}
