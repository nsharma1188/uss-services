﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class ErrorResponse
    {
        public string Message { get; set; }
    }
}
