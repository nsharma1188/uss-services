﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class OperationEvent
    {
        [JsonProperty("gufi")]
        public Guid Gufi { get; set; }

        [JsonProperty("uss_instance_id")]
        public Guid UssInstanceId { get; set; }

        [JsonProperty("event_time")]
        public string EventTime { get; set; }

        [JsonProperty("event_type")]
        public EventTypeEnum EventType { get; set; }

		[JsonProperty("user_id")]
		public string UserId { get; set; }
	}
}
