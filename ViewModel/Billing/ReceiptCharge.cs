﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class ReceiptCharge
    {
		[JsonProperty("charge_id")]
		public string ChargeId { get; set; }

		[JsonProperty("charge_time")]
		public string ChargeTime { get; set; }

		[JsonProperty("amount")]
		public double Amount { get; set; }

		[JsonProperty("currency_code")]
		public string CurrencyCode { get; set; }

		[JsonProperty("type")]
		public BillingRuleTypeEnum Type { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }
	}
}