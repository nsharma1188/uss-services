﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class Receipt
    {
        [JsonProperty("receipt_id")]
        public string ReceiptId { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("uss_instance_id")]
        public string UssInstanceId { get; set; }

        [JsonProperty("receipt_time")]
        public string ReceiptTime { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("currency_code")]
        public string CurrencyCode { get; set; }

        [JsonProperty("charges")]
        public List<ReceiptCharge> ReceiptCharges { get; set; }
    }
}
