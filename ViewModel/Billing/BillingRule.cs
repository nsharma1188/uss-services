﻿using AnraUssServices.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Billing
{
    public class BillingRule
    {
        public BillingRuleTypeEnum Type { get; set; }
        public bool Active { get; set; }
        public double Cost { get; set; }
        public string CurrencyCode { get; set; }
    }
}
