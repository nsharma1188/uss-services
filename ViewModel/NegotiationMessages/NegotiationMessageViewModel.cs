﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.NegotiationMessages
{
    public class NegotiationMessageViewModel
    {
        
        /// <summary>
        /// A UUIDv4 assigned to this message by the originator
        /// </summary>
        /// <value>A UUIDv4 assigned to this message by the originator</value>
        [DataMember(Name = "message_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_id")]
        [Required]
        public Guid MessageId { get; set; }

        /// <summary>
        /// A UUIDv4 assigned to this message by the originator
        /// </summary>
        /// <value>A UUIDv4 assigned to this message by the originator</value>
        [DataMember(Name = "negotiation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "negotiation_id")]
        [Required]
        public Guid NegotiationId { get; set; }

        /// <summary>
        /// A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name_of_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_originator")]
        public string UssNameOfOriginator { get; set; }

        /// <summary>
        /// A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
        /// </summary>
        /// <value>A name identifying the originator of this message. MUST be the uss_name as known by the authorization server. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035. The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
        [DataMember(Name = "uss_name_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_receiver")]
        public string UssNameOfReceiver { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "replan_suggestion", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "replan_suggestion")]
        public string ReplanSuggestion { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the originating USS. Note that the origin and receiver roles are strictly dependent on which USS is generating this message. May be empty in the case of ATC communication to USS via FIMS.</value>
        [DataMember(Name = "gufi_of_originator", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_of_originator")]
        public Guid? GufiOfOriginator { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS. </value>
        [DataMember(Name = "gufi_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_of_receiver")]
        [Required]
        public Guid? GufiOfReceiver { get; set; }

        /// <summary>
        /// Gets or Sets FreeText
        /// </summary>
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        public string FreeText { get; set; }
      
        /// <summary>
        /// Date created
        /// </summary>
        [DataMember(Name = "date_created", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "date_created")]
        public DateTime DateCreated { get; set; }
    }
}