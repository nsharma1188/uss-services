﻿using AnraUssServices.Utm.Models;
using Mapster;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.NegotiationMessages
{
    public class NegotiationMessageInputModel
    {
        [IgnoreDataMember]
        public Guid MessageId { get; set; }

        [IgnoreDataMember]
        public Guid NegotiationId { get; set; }

        [JsonIgnore]
        public string UssNameOfOriginator { get; set; }

        [DataMember(Name = "uss_name_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name_of_receiver")]
        public string UssNameOfReceiver { get; set; }

        [IgnoreDataMember]
        public Guid GufiOfOriginator { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "replan_suggestion", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "replan_suggestion")]
        public string ReplanSuggestion { get; set; }

        /// <summary>
        /// The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS.
        /// </summary>
        /// <value>The GUFI of the relevant operation of the receiving USS. May be empty in cases of communication with ATC via FIMS. </value>
        [DataMember(Name = "gufi_of_receiver", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi_of_receiver")]
        [Required]
        public Guid GufiOfReceiver { get; set; }

        /// <summary>
        /// Gets or Sets FreeText
        /// </summary>
        [DataMember(Name = "free_text", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "free_text")]
        [Required]
        public string FreeText { get; set; }

        [JsonIgnore]
        public string Response { get; set; }

        public void Update(Models.Operation operation)
        {
            MessageId = Guid.NewGuid();
            NegotiationId = NegotiationId.Equals(Guid.Empty) ? Guid.NewGuid() : NegotiationId;
            UssNameOfOriginator = operation.UssName;
            GufiOfOriginator = operation.Gufi;           
        }

        public UTMRestResponse Send(string externalUssUrl, Utm.Api.NegotiationApi negotiationApi, string destinationUssName)
        {
            var response = negotiationApi.PostNegotiationMessage(externalUssUrl, this.Adapt<NegotiationMessage>(), destinationUssName);
            if (response != null)
            {
                Response = $"{response.HttpStatusCode} {response.Message}";
            }
            return response;
        }
    }
}