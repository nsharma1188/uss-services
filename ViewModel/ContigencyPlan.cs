﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel
{
    public class ContigencyPlan: Utm.Models.ContingencyPlan
    {
        [Required]
        public int OperationId { get; set; }
    }
}
