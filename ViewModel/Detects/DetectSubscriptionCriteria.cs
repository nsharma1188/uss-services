﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Detects
{
    public class DetectSubscriptionCriteria
    {
        [DataMember(Name = "types", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "types")]
        [EnumDataType(typeof(DetectSourceEnum))]
        [Required]
        public List<DetectSourceEnum> Types { get; set; }
    }
}