﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Detects
{
    public class SubscriptionViewModel
    {
        public string Gufi { get; set; }

        public string UssInstanceId { get; set; }

        public string UserId { get; set; }

        public bool IsCollisionSubscribed { get; set; }

        public string[] SubscribedTypes { get; set; }

        public bool IsRadarSubscribed { get; set; }
    }
}
