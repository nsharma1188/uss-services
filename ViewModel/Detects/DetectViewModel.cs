﻿using LiteDB;
using Nest;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Detects
{
    [DataContract]
    public class DetectViewModel
    {
        /// <summary>
        /// Detect Id
        /// </summary>
        /// <value>uuid</value>
        [BsonId]
        [DataMember(Name = "detect_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "detect_id")]
        public Guid DetectId { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "lat", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lat")]
        public double Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "lng", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lng")]
        public double Longitude { get; set; }

        /// <summary>
        /// Altitude
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "alt", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "alt")]
        public double Altitude { get; set; }

        /// <summary>
        /// Time Stamp
        /// </summary>
        /// <value>DateTime</value>
        [DataMember(Name = "time_stamp", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_stamp")]
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Source can be RADAR|DSRC|RF|ADSB
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "source", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source")]
        public string Source { get; set; }

        /// <summary>
        /// Source: GROUND_FIXED|GROUND_MOBILE|AIRBORNE|UNKNOWN
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "source_class", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source_class")]
        public string SourceClass { get; set; }


        /// <summary>
        /// Vendor
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "vendor", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vendor")]
        public string Vendor { get; set; }

        /// <summary>
        /// Metadata1
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "metadata1", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "metadata1")]
        public string Metadata1 { get; set; }

        /// <summary>
        /// Metadata2
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "metadata2", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "metadata2")]
        public string Metadata2 { get; set; }

        /// <summary>
        /// RawData
        /// </summary>
        /// <value>string</value>
        [DataMember(Name = "raw_data", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "raw_data")]
        public string RawData { get; set; }

        /// <summary>
        /// Heading
        /// </summary>
        /// <value>int</value>
        [DataMember(Name = "heading", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "heading")]
        public int? Heading { get; set; }

        /// <summary>
        /// Bearing
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "bearing", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "bearing")]
        public double? Bearing { get; set; }


        /// <summary>
        /// Bearing
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "uuid", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uuid")]
        public string Uid { get; set; }


        /// <summary>
        /// Bearing
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "user_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "userid")]
        public Guid UserId { get; set; }

        /// <summary>
        /// Detect Type - Track/Detect
        /// </summary>
        /// <value>double</value>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [GeoPoint]
        public GeoCoordinate Location {
            get
            {
                return new GeoCoordinate(Latitude, Longitude); 
            }
        } 
    }
}