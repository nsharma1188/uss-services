﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Detects
{
    public class AdsbInput
    {
        /// <summary>
		/// instanceId
		/// </summary>
		[DataMember(Name = "uss_instance", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_instance")]       
        public Guid USSInstanceId { get; set; }

        /// <summary>
		/// userId
		/// </summary>
		[DataMember(Name = "user_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_id")]        
        public Guid UserId { get; set; }

        /// <summary>
		/// min latitude
		/// </summary>
		[DataMember(Name = "latmin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "latmin")]       
        public double latmin { get; set; }

        /// <summary>
		/// max latitude
		/// </summary>
		[DataMember(Name = "latmax", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "latmax")]       
        public double latmax { get; set; }

        /// <summary>
		/// min longitude
		/// </summary>
		[DataMember(Name = "lngmin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lngmin")]       
        public double lngmin { get; set; }

        /// <summary>
        /// max longitude
        /// </summary>
        [DataMember(Name = "lngmax", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "lngmax")]      
        public double lngmax { get; set; }

        /// <summary>
        /// max longitude
        /// </summary>
        [DataMember(Name = "OpGufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "OpGufi")]       
        public Guid OpGufi { get; set; }

        [DataMember(Name = "opensky", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "opensky")]        
        public string opensky { get; set; }

    }
}

