﻿using AnraUssServices.Utm.Models;
using LiteDB;
using Nest;
using System;

namespace AnraUssServices.ViewModel.Detects
{
    public class DetectSubscriberViewModel
    {
        [BsonId]
        public Guid SubscriptionId { get; set; }

        public Guid Gufi { get; set; }

        public Guid UssInstanceId { get; set; }

        [GeoShape]
        public Polygon UssCoverageArea { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public bool IsActive { get; set; }

        public string DetectSoure { get; set; }

        public Guid UserId { get; set; }

        public string[] SubscribedTypes { get; set; }

        public string AdsbSource { get; set; }

        public string AdsbQuery { get; set; }
    }
}