using AnraUssServices.Common.Adsb;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Detects
{
    public class AdsbAreaData
    {
        public string icao24 { get; set; }
        public string callsign { get; set; }
        public string origin_country { get; set; }
        public DateTime timeStamp { get; set; }
        public int last_contact { get; set; }
        public float? longitude { get; set; }
        public float? latitude { get; set; }
        public float? baro_altitude { get; set; }
        public bool on_ground { get; set; }
        public float velocity { get; set; }
        public float? true_track { get; set; }
        public float vertical_rate { get; set; }
        public int[] sensors { get; set; }
        public float geo_altitude { get; set; }
        public string squawk { get; set; }
        public bool spi { get; set; }
        public int position_source { get; set; }


        public AdsbJsonStruct ToDetectPacket(Guid userId, Guid detectId)
        {
            var detect = new AdsbJsonStruct
            {
                detect_id = Guid.NewGuid(),
                user_id = userId,
                uuid = icao24,
                lat = latitude,
                lng = longitude,
                alt = geo_altitude * 3.28084, 
                heading = (int)true_track,
                time_stamp = timeStamp,
                source = "ADSB",
                vendor = "OPENSKY",
                type = "DETECT",
                metadata1 = "CallSign: " + (callsign != null ? callsign : "NULL") + ", Alt:" + Math.Round(geo_altitude * 3.28084, 2) + " feet",
                metadata2 = "HSpeed:" + (Math.Round(velocity * 3600, 2)) + " mph" + ", VSpeed:" + Math.Round(vertical_rate * 3.28084, 2) + " fps",
                raw_data = JsonConvert.SerializeObject(this)
            };

            return detect;
        }

    }
}
