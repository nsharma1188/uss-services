﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Detects
{
    public class AdsbOneskyModel
    {
        [DataMember(Name = "time", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time")]
        public int Time { get; set; }


        [DataMember(Name = "states", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "states")]
        public string[,] States { get; set; }
    }
}
