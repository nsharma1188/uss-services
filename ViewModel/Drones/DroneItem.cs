using AnraUssServices.Common.Lookups;
using LiteDB;
using Nest;
using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Drones
{
    /// <summary>
    /// Drone - An UAV used to perform operations.
    /// </summary>        
    public class DroneViewModel
    {
        private int _brandId;
        private int _typeId;

        /// <summary>
        /// Drone Id - system internal id to manage drones
        /// </summary>
        //public int DroneId { get; set; } //removed

        /// <summary>
        /// Drone Registration No.
        /// </summary>
        /// <value>Required as this is the UUID of Drone assigned by FAA</value>         
        [BsonId]
        public string Uid { get; set; }

        /// <summary>
        /// Drone Name
        /// </summary>        
        public string Name { get; set; }

        /// <summary>
        /// UDP Port - Used to receive telemetry packet
        /// </summary>
        public int UpdPort { get; set; }

        /// <summary>
        /// Drone Base Longitude - Used to set Drone home location longitude
        /// </summary>
        public string BaseLng { get; set; }

        /// <summary>
        /// Drone Base Latitude - Used to set Drone home location latitude
        /// </summary>
        public string BaseLat { get; set; }

        /// <summary>
        /// Drone Base Altitude - Used to set Drone default altitude
        /// </summary>
        public string BaseAlt { get; set; }

        /// <summary>
        /// FPV Source Url
        /// </summary>
        public string FpvSrcUrl { get; set; }

        /// <summary>
        /// Sensor Source Url
        /// </summary>
        public string SensorSrcUrl { get; set; } 

        /// <summary>
        /// Enable this flag to receive radar tracks
        /// </summary>
        public bool IsRadarEnabled { get; set; }

        /// <summary>
        /// Enable this flag to make drone active/inactive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Delete Flag used for soft delete
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Enable this flag to receive UTM updates
        /// </summary>
        public bool IsUtmEnabled { get; set; }

        /// <summary>
        /// Drone Brand Name
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// Drone Type Name
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Drone Type Name
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Drone Unique Remote Id
        /// </summary>
        public string RemoteId { get; set; }

        /// <summary>
        /// Organization
        /// </summary>
        public Guid OrganizationId { get; set; }

        /// <summary>
        /// Drone Brand Id
        /// </summary>
        public int BrandId
        {
            get { return _brandId; }
            set
            {
                _brandId = value;
                BrandName = DroneBrandLookup.GetLookup().Find(x => x.Id == BrandId).Name;
            }
        }

        /// <summary>
        /// Drone Type Id
        /// </summary>
        public int TypeId
        {
            get { return _typeId; }
            set
            {
                _typeId = value;
                TypeName = DroneTypeLookup.GetLookup().Find(x => x.Id == TypeId).Name;
            }
        }

        /// <summary>
        /// Collision Threshold
        /// </summary>
        public int CollisionThreshold { get; set; }

        /// <summary>
        /// Drone Brand Lookup
        /// </summary>
        public List<DroneBrandLookup> BrandLookup { get; set; } = new List<DroneBrandLookup>();

        /// <summary>
        /// Drone Type Lookup
        /// </summary>
        public List<DroneTypeLookup> TypeLookup { get; set; } = new List<DroneTypeLookup>();
    }
}