﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Operations
{
	public class WayPointViewModel
	{
		[DataMember(Name = "takeoff_point", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "takeoff_point")]
		public Coordinate TakeOffPoint { get; set; }

		[DataMember(Name = "landing_point", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "landing_point")]
		public Coordinate LandingPoint { get; set; }

		[DataMember(Name = "takeoff_landing_radius", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "takeoff_landing_radius")]
		public double TakeoffLandingRadius { get; set; }

		[DataMember(Name = "flight_speed", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "flight_speed")]
		public int FlightSpeed { get; set; }

		[DataMember(Name = "altitude_buffer", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "altitude_buffer")]
		public double AltitudeBuffer { get; set; }

		[DataMember(Name = "expand_factor", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "expand_factor")]
		public ExpandFactorTypeEnum ExpandFactor { get; set; }

		[DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "effective_time_begin")]
		[Required]
		public DateTime EffectiveTimeBegin { get; set; }

		[DataMember(Name = "waypoints", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "waypoints")]
		public List<Coordinate> Waypoints { get; set; }

	}


	public class Coordinate
	{
		[DataMember(Name = "latitude", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "latitude")]
		public double Latitude { get; set; }

		[DataMember(Name = "longitude", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "longitude")]
		public double Longititude { get; set; }

		[DataMember(Name = "altitude", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "altitude")]
		public double Altitude { get; set; }
	}

}
