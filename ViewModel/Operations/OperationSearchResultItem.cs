using AnraUssServices.Common.Lookups;
using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    public class OperationSearchResultItem : OperationViewModel
    {
        [DataMember(Name = "user_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_name")]
        public string UserName { get; set; }

        [DataMember(Name = "user_email", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_email")]
        public string UserEmail { get; set; }

        [DataMember(Name = "user_phone", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_phone")]
        public string UserPhoneNumber { get; set; }

        [DataMember(Name = "user_full_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_full_name")]
        public string UserFullName { get; set; }

        [DataMember(Name = "user_addressline1", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_addressline1")]
        public string UserAddressLine1 { get; set; }

        [DataMember(Name = "user_addressline2", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_addressline2")]
        public string UserAddressLine2 { get; set; }

        [DataMember(Name = "user_city", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_city")]
        public string UserCity { get; set; }

        [DataMember(Name = "user_state", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_state")]
        public string UserState { get; set; }

        [DataMember(Name = "user_zipcode", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_zipcode")]
        public string UserZipcode { get; set; }

        [DataMember(Name = "user_organization", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "user_organization")]
        public string UserOrganization { get; set; }        
    }
}