using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Altitudes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace AnraUssServices.ViewModel.Operations
{
    [DataContract]
    public class OperationVolumeViewModel
    {
        
        [DataMember(Name = "operation_volume_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_volume_id")]
        public Guid OperationVolumeUid { get; set; }

        private DateTime? _effectiveTimeBegin = null;
        private DateTime? _effectiveTimeEnd = null;
        private DateTime? _actualTimeEnd = null;

        /// <summary>
        /// This integer represents the ordering of the operation volume within the set of operation volumes. Need not be consecutive integers.
        /// </summary>
        /// <value>This integer represents the ordering of the operation volume within the set of operation volumes. Need not be consecutive integers.</value>
        [DataMember(Name = "ordinal", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "ordinal")]
        [Required]
        public int Ordinal { get; set; }

        /// <summary>
        /// More description later.
        /// </summary>
        /// <value>More description later.</value>
        [DataMember(Name = "volume_type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "volume_type")]
		[Required]
        public VolumeType VolumeType { get; set; }

        /// <summary>
        /// Is this operation volume within 400' of a structure?
        /// </summary>
        /// <value>Is this operation volume within 400' of a structure?</value>
        [DataMember(Name = "near_structure", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "near_structure")]
        public bool? NearStructure { get; set; }

        /// <summary>
        /// Earliest time the operation will use the operation volume.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Earliest time the operation will use the operation volume.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        [Required]
        public DateTime EffectiveTimeBegin { get; set; }

        /// <summary>
        /// Latest time the operation will done with the operation volume. It must be greater than effective_time_begin.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Latest time the operation will done with the operation volume. It must be greater than effective_time_begin.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Time that the operational volume was freed for use by other operations.  Should be populated and stored by the USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>Time that the operational volume was freed for use by other operations.  Should be populated and stored by the USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "actual_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_time_end")]
        public DateTime? ActualTimeEnd { get; set; }

        /// <summary>
        /// The minimum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.
        /// </summary>
        /// <value>The minimum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        public AltitudeViewModel MinAltitude { get; set; }

        /// <summary>
        /// The maximum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.
        /// </summary>
        /// <value>The maximum altitude for this operation in this operation volume. In WGS84 reference system using feet as units.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        public AltitudeViewModel MaxAltitude { get; set; }

        /// <summary>
        /// Gets or Sets OperationGeography
        /// </summary>
        [DataMember(Name = "operation_geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_geography")]
        [Required]
        public Polygon OperationGeography { get; set; }

        /// <summary>
        /// Describes whether the operation volume is beyond the visual line of sight of the RPIC.
        /// </summary>
        /// <value>Describes whether the operation volume is beyond the visual line of sight of the RPIC.</value>
        [DataMember(Name = "beyond_visual_line_of_sight", EmitDefaultValue = true)]
        [JsonProperty(PropertyName = "beyond_visual_line_of_sight")]
		[Required]
        public bool BeyondVisualLineOfSight { get; set; }

        /// <summary>
        /// Distance in feet
        /// </summary>
        /// <value>Volume linear distance in feet</value>
        [DataMember(Name = "volume_distance_feet", EmitDefaultValue = true)]
        [JsonProperty(PropertyName = "volume_distance_feet")]
        public double DistanceInFeet { get; set; }

		/// <summary>
		/// AGL altitude in feet
		/// </summary>
		/// <value>Above Ground Level altitude in feet</value>
		[DataMember(Name = "agl_altitude", EmitDefaultValue = true)]
		[JsonProperty(PropertyName = "agl_altitude")]
		[Required]
		public double AglAltitude { get; set; }

	}
}