﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Operations
{
	public class OperationVolumeHelperViewModel
	{
		/// <summary>
		/// List of waypoints. This field is for the mobile client use. 
		/// </summary>
		[DataMember(Name = "waypoints", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "waypoints")]
		public string Waypoints { get; set; }

		/// <summary>
		/// List of waypoints. This field is for 3D viewer 
		/// </summary>
		[DataMember(Name = "coordinatesList", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "coordinatesList")]
		public string CoordinatesList { get; set; }

		/// <summary>
		/// The actual geographical information for the operation.  Maximum array length of 12 currently allowed.
		/// </summary>
		/// <value>The actual geographical information for the operation.  Maximum array length of 12 currently allowed.</value>
		[DataMember(Name = "operation_volumes", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "operation_volumes")]
		public List<OperationVolumeViewModel> OperationVolumes { get; set; }
	}
}
