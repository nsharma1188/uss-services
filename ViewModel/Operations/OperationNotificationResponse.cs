﻿using AnraUssServices.Utm.Models;
using System;

namespace AnraUssServices.ViewModel.Operations
{
    public class OperationNotificationResponse
    {
        public Models.UtmInstance UssInstance { get; set; }
        public UTMRestResponse Response { get; set; }
    }
}