﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    public class OperationConflictViewModel
    {
        [DataMember(Name = "operataion_conflict_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operataion_conflict_id")]
        public int OperationConflictId { get; set; }

        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        public Guid Gufi { get; set; }

        [DataMember(Name = "conflicting_operation_gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "conflicting_operation_gufi")]
        public Guid ConflictingGufi { get; set; }

        [DataMember(Name = "conflicting_uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "conflicting_uss_name")]
        public string ConflictingUssName { get; set; }

        [DataMember(Name = "time_stamp", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "time_stamp")]
        public DateTime TimeStamp { get; set; }

        [DataMember(Name = "operation_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_id")]
        public int OperationId { get; set; }


        [DataMember(Name = "is_anra_primary", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "is_anra_primary")]
        public bool IsAnraPrimary { get; set; }

        [DataMember(Name = "conflict_details", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "conflict_details")]
        public string ConflictDetails { get; set; }
    }
}