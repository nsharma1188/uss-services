using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class AtcActionItem
    {
        /// <summary>
        /// Action
        /// </summary>        
        [DataMember(Name = "action", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "action")]
        public string Action { get; set; }

        /// <summary>
        /// Comments
        /// </summary>        
        [DataMember(Name = "comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "comments")]
        public string Comments { get; set; }
    }
}