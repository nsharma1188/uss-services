﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    public class LunActiveOperationsViewModel
    {
        [DataMember(Name = "operations", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operations")]
        public IEnumerable<OperationViewModel> Operations { get; set; }

        [DataMember(Name = "uss_instances", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_instances")]
        public IEnumerable<Utm.Models.UssInstance> UssInstances { get; set; }


        [DataMember(Name = "utm_messages", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "utm_messages")]
        public IEnumerable<Utm.Models.UTMMessage> UtmMessages { get; set; }
    }
}