using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Contacts;
using AnraUssServices.ViewModel.ContingencyPlans;
using LiteDB;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
	[DataContract]
	public class OperationViewModel
	{
		/// <summary>
		/// Created and assigned by USS. Validated as UUID version 4 specification.
		/// </summary>
		/// <value>Created and assigned by USS. Validated as UUID version 4 specification.</value>
		[BsonId]
		[DataMember(Name = "gufi", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "gufi")]
		public Guid Gufi { get; set; }

		/// <summary>
		/// Identity of the USS.  The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.
		/// </summary>
		/// <value>Identity of the USS.  The maximum and minimum character length is based on a usable domain name, and considering the maximum in RFC-1035.</value>
		[DataMember(Name = "uss_name", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uss_name")]
		public string UssName { get; set; }

		/// <summary>
		/// Identiy of the USS Instance hosting this operation.
		/// </summary>
		/// <value>Identiy of the USS Instance hosting this operation.</value>
		[DataMember(Name = "uss_instance_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uss_instance_id")]
		[Required]
		public Guid UssInstanceId { get; set; }

		/// <summary>
		/// Time the operation submission was received by USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
		/// </summary>
		/// <value>Time the operation submission was received by USS. Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
		[DataMember(Name = "submit_time", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "submit_time")]
		public DateTime SubmitTime { get; set; }

		/// <summary>
		/// A timestamp set by the USS any time the state of the operation is updated within the USS Network. An update may be minor or major, but if/when te Operation is shared in the USS Network as a PUT to its LUN, this field MUST reflect the time that update was provided.  When the operation is submitted for the first time to the LUN, this value MUST be equal to submit_time.  This value MUST be constant for each update. This means that all USSs have the same value for last_submitted_update_time even if, for example, one USS receives the update later than others due to HTTP retrys or is provided as a GET for this Operation.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted. This field is set and maintained by the USS managing the Operation and is communicated to other USSs.
		/// </summary>
		/// <value>A timestamp set by the USS any time the state of the operation is updated within the USS Network. An update may be minor or major, but if/when te Operation is shared in the USS Network as a PUT to its LUN, this field MUST reflect the time that update was provided.  When the operation is submitted for the first time to the LUN, this value MUST be equal to submit_time.  This value MUST be constant for each update. This means that all USSs have the same value for last_submitted_update_time even if, for example, one USS receives the update later than others due to HTTP retrys or is provided as a GET for this Operation.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted. This field is set and maintained by the USS managing the Operation and is communicated to other USSs.</value>
		[DataMember(Name = "update_time", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "update_time")]
		public DateTime? UpdateTime { get; set; }

		/// <summary>
		/// Informative text about the operation. Not used by the UTM System. Only for human stakeholders.
		/// </summary>
		/// <value>Informative text about the operation. Not used by the UTM System. Only for human stakeholders.</value>
		[DataMember(Name = "flight_comments", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "flight_comments")]
		[Required]
		public string FlightComments { get; set; }

		/// <summary>
		/// The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.
		/// </summary>
		/// <value>The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.</value>
		[DataMember(Name = "uas_registrations", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "uas_registrations")]
		[Required]
		public List<UasRegistrationViewModel> UasRegistrations { get; set; }

		/// <summary>
		/// This field is populated based on the provided credentials.
		/// </summary>
		/// <value>This field is populated based on the provided credentials.</value>
		[DataMember(Name = "user_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "user_id")]
		public string UserId { get; set; }

		/// <summary>
		/// Contact
		/// </summary>
		[DataMember(Name = "contact", EmitDefaultValue = false)]
		[JsonIgnore]
		public PersonOrOrganizationViewModel Contact { get; set; }


		/// <summary>
		/// The current state of the operation.  Must be maintained by the USS. Some additional details in the USS Specification.  1. ACCEPTED   This operation has been deemed ACCEPTED by the supporting USS. This implies that the operation meets the requirements for operating in the airspace based on the type of operation submitted. 2. ACTIVATED   This operation is active. The transition from ACCEPTED to ACTIVATED is not an announced transition. The transition is implied based on the submitted start time of the operation (i.e. the effective_time_begin of the first OperationVolume). Note that an ACTIVATED operation is not necessarily airborne, but is assumed to be \"using\" the OperationVolumes that it has announced. 3. CLOSED   This operation is closed. It is not airborne and will not become airborne again. If the UAS and the crew will fly again, it would need to be as a new operation. A USS may announce the closure of any operation, but is not required to announce unless the operation was ROGUE or NONCOFORMING. 4. NONCORMING   See USS Specifcation for requirements to transition to this state. 5. ROGUE   See USS Specifcation for requirements to transition to this state.
		/// </summary>
		/// <value>The current state of the operation.  Must be maintained by the USS. Some additional details in the USS Specification.  1. ACCEPTED   This operation has been deemed ACCEPTED by the supporting USS. This implies that the operation meets the requirements for operating in the airspace based on the type of operation submitted. 2. ACTIVATED   This operation is active. The transition from ACCEPTED to ACTIVATED is not an announced transition. The transition is implied based on the submitted start time of the operation (i.e. the effective_time_begin of the first OperationVolume). Note that an ACTIVATED operation is not necessarily airborne, but is assumed to be \"using\" the OperationVolumes that it has announced. 3. CLOSED   This operation is closed. It is not airborne and will not become airborne again. If the UAS and the crew will fly again, it would need to be as a new operation. A USS may announce the closure of any operation, but is not required to announce unless the operation was ROGUE or NONCOFORMING. 4. NONCORMING   See USS Specifcation for requirements to transition to this state. 5. ROGUE   See USS Specifcation for requirements to transition to this state. </value>
		[DataMember(Name = "state", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "state")]
		[EnumDataType(typeof(OperationState))]
		public OperationState? State { get; set; }

		/// <summary>
		/// Gets or Sets ControllerLocation
		/// </summary>
		[DataMember(Name = "controller_location", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "controller_location")]
		[Required]
		public Point ControllerLocation { get; set; }

		/// <summary>
		/// An array of ContingencyPlans wherein this operation may land if needed/required during operation. Aids in planning and communication during the execution of a contingency.
		/// </summary>
		/// <value>An array of ContingencyPlans wherein this operation may land if needed/required during operation. Aids in planning and communication during the execution of a contingency.</value>
		[DataMember(Name = "contingency_plans", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "contingency_plans")]
		public List<ContingencyPlanViewModel> ContingencyPlans { get; set; }

		/// <summary>
		/// An array of OperationConflicts
		/// </summary>
		/// <value>An array of OperationConflicts</value>
		[DataMember(Name = "operation_conflicts", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "operation_conflicts")]
		public List<OperationConflictViewModel> OperationConflicts { get; set; }

		/// <summary>
		/// 1. PART_107   The operation follows FAA rule 107. Submission of such operations is mandatory  2. PART_107X   In general, operations are 107X if they are doing something that would require a waiver under current 107 rules. Submission of such operations is mandatory.  3. PART_101E   Submission of 101E would be required if operation is within 5 statute miles of an airport. Optional otherwise.  4. OTHER   Placeholder for other types of operations.
		/// </summary>
		/// <value>1. PART_107   The operation follows FAA rule 107. Submission of such operations is mandatory  2. PART_107X   In general, operations are 107X if they are doing something that would require a waiver under current 107 rules. Submission of such operations is mandatory.  3. PART_101E   Submission of 101E would be required if operation is within 5 statute miles of an airport. Optional otherwise.  4. OTHER   Placeholder for other types of operations.</value>
		[DataMember(Name = "faa_rule", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "faa_rule")]
		[Required]
		public FaaRule FaaRule { get; set; }

		/// <summary>
		/// Gets or Sets PriorityElements
		/// </summary>
		[DataMember(Name = "priority_elements", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "priority_elements")]
		[Required]
		public PriorityElements PriorityElements { get; set; }

		/// <summary>
		/// The actual geographical information for the operation.  Maximum array length of 12 currently allowed.
		/// </summary>
		/// <value>The actual geographical information for the operation.  Maximum array length of 12 currently allowed.</value>
		[DataMember(Name = "operation_volumes", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "operation_volumes")]
		[Required]
		public List<OperationVolumeViewModel> OperationVolumes { get; set; }

		///<summary>
		/// Organization Id
		/// </summary>        
		[DataMember(Name = "organization_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "organization_id")]
		public Guid OrganizationId { get; set; }

		/// <summary>
		/// Pilot User Id
		/// </summary>
		/// <value>This field is populated based on the provided credentials.</value>
		[DataMember(Name = "contact_id", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "contact_id")]
		[Required]
		public string ContactId { get; set; }

		/// <summary>
		/// Anra Identifier
		/// </summary>
		[DataMember(Name = "is_internal", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "is_internal")]
		public string IsInternalOperation { get; set; }

		/// <summary>
		/// List of waypoints. This news field is for the mobile client use. 
		/// </summary>
		[DataMember(Name = "way_points_list", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "way_points_list")]
		public string WayPointsList { get; set; }

		/// <summary>
		/// List of coordinates. This news field is for the web UI for 3D viewer. 
		/// </summary>
		[DataMember(Name = "coordinates_list", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "coordinates_list")]
		public string CoordinatesList { get; set; }

        /// <summary>
		/// ATC comments regarding approval/Rejection 
		/// </summary>
		[DataMember(Name = "atc_comments", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "atc_comments")]
        public string ATCComments { get; set; }
    }
}