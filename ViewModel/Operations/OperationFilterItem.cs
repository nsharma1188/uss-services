using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class OperationFilterItem
    {
        /// <summary>
        /// Location - The Source Location Point Around Which Search Needs to Performed.
        /// </summary>        
        [DataMember(Name = "location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "location")]
        public Point Location { get; set; }

        /// <summary>
        /// Uss Coverage Area
        /// </summary>        
        [DataMember(Name = "uss_instance_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_instance_id")]
        public Guid UssInstanceId { get; set; }

        /// <summary>
        /// Radius - The Spherical Radius Around The Location Within Which Search Needs to Performed.
        /// </summary>        
        [DataMember(Name = "radius", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "radius")]
        public int Radius { get; set; }

        /// <summary>
        /// Record Limit - No. Of records to be return.
        /// </summary>        
        [DataMember(Name = "record_limit", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "record_limit")]
        public int RecordLimit { get; set; }
    }
}