using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Operations
{
    /// <summary>
    ///
    /// </summary>
    [DataContract]
    public class OperationSearchItem
    {
        /// <summary>
        /// Search By - Accepted Values are - 1 - "LOCATION" , 2 - "DRONE"
        /// </summary>        
        [DataMember(Name = "search_by", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "search_by")]
        public string SearchBy { get; set; }

        /// <summary>
        /// Location - The Source Location Point Around Which Search Needs to Performed.
        /// </summary>        
        [DataMember(Name = "location", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "location")]
        public Point Location { get; set; }

        /// <summary>
        /// Drone - The Drone Remote Id For Which Search Needs to Performed.
        /// </summary>        
        [DataMember(Name = "drone", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "drone")]
        public string Drone { get; set; }

        /// <summary>
        /// Search Radius - The Spherical Radius Around The Location Within Which Search Needs to Performed.
        /// </summary>        
        [DataMember(Name = "search_radius", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "search_radius")]
        public int SearchRadius { get; set; }
    }
}