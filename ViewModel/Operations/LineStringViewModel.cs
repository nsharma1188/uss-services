﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Operations
{
	public class LineStringViewModel
	{
		public string Type { get; set; }

		public List<List<double?>> Coordinates { get; set; }

		public double AltitudePointA { get; set; }

		public double AltitudePointB { get; set; }
	}
}
