﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using AnraUssServices.Common;
using AnraUssServices.Utm.Models;
using Newtonsoft.Json;

namespace AnraUssServices.ViewModel.Operations
{
	[DataContract]
	public class GenerateOperationVolumeKml
	{
		/// <summary>
		/// To specify the horizontal buffer 
		/// </summary>
		[DataMember(Name = "expand_factor", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "expand_factor")]
		[Required]
		public ExpandFactorTypeEnum ExpandFactor { get; set; }

		/// <summary>
		/// To specify the vertical buffer
		/// </summary>
		[DataMember(Name = "altitude_buffer", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "altitude_buffer")]
		[Required]
		public double AltitudeBuffer { get; set; }

		/// <summary>
		/// Earliest time the operation will use the operation volume.
		/// </summary>
		[DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "effective_time_begin")]
		[Required]
		public DateTime EffectiveTimeBegin { get; set; }

		/// <summary>
		/// Speed of the flight in feets per second.
		/// </summary>
		[DataMember(Name = "flight_speed", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "flight_speed")]
		[Required]
		public int FlightSpeed { get; set; }

		/// <summary>
		/// Json string generated using KML file
		/// </summary>
		[DataMember(Name = "kml_json", EmitDefaultValue = false)]
		[JsonProperty(PropertyName = "kml_json")]
		[Required]
		public string KmlJson { get; set; }

        /// <summary>
		/// Takeoff and Landing radius
		/// </summary>
		[DataMember(Name = "takeoff_landing_radius", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "takeoff_landing_radius")]
        [Required]
        public double TakeoffLandingRadius { get; set; }
    }
}
