﻿using System;

namespace AnraUssServices.ViewModel.Operations
{
    public class OperationNotification
    {
        public Guid Gufi { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
    }

    //public class OperationDeleteNotification
    //{
    //    public Guid Gufi { get; set; }        
    //    public bool IsDeleted { get; set; }        
    //    public DateTime Timestamp { get; set; }
    //}
}