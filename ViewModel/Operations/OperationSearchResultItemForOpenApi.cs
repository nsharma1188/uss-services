﻿using System;
using AnraUssServices.Utm.Models;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.ViewModel.Operations
{
    public class OperationSearchResultItemForOpenApi
    {
        /// <summary>
        /// Created and assigned by USS. Validated as UUID version 4 specification.
        /// </summary>
        /// <value>Created and assigned by USS. Validated as UUID version 4 specification.</value>
        [DataMember(Name = "gufi", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "gufi")]
        public Guid Gufi { get; set; }

        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        public string UssName { get; set; }

        /// <summary>
        /// The actual geographical information for the operation.  Maximum array length of 12 currently allowed.
        /// </summary>
        /// <value>The actual geographical information for the operation.  Maximum array length of 12 currently allowed.</value>
        [DataMember(Name = "operation_volumes", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operation_volumes")]        
        public List<OperationVolumeViewModel> OperationVolumes { get; set; }

        /// <summary>
        /// The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.
        /// </summary>
        /// <value>The registration data for the vehicle(s) to be used in this Operation. Note that this is an array to allow for future operations involving multiple vehicles (e.g. 'swarms' or tandem inspections).  This array MUST NOT be used as a list of potential vehicles for this Operation. If the vehicle data changes prior to an Operation, an update to the plan may be submitted with the updated vehicle information.</value>
        [DataMember(Name = "uas_registrations", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uas_registrations")]
        public List<Guid> UasRegistrations { get; set; }


        public OperationSearchResultItemForOpenApi(OperationSearchResultItem searchResult)
        {
            Gufi = searchResult.Gufi;
            UssName = searchResult.UssName;
            OperationVolumes = searchResult.OperationVolumes;
            UasRegistrations = searchResult.UasRegistrations.Select(p => p.RegistrationId.Value).ToList();
        }
    }
}
