﻿using AnraUssServices.ViewModel.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Organizations
{
	public class OrganizationItem : UserItem
	{
		public string OrganizationId { get; set; }
		public string GroupId { get; set; }
		public string Name { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
		public string LogoFile { get; set; }
		public int CurrencyId { get; set; }
		public int UnitId { get; set; }
		public string Website { get; set; }
		public string GovtLicenseNumber { get; set; }
		public string FederalTaxId { get; set; }
		public string Address { get; set; }
		public string BaseLat { get; set; }
		public string BaseLng { get; set; }
		public string AdsbSource { get; set; }

		public List<AdsbSourceLookupItem> AdsbSourceLookup { get; set; } = new List<AdsbSourceLookupItem>();
		public List<CurrencyLookupItem> CurrencyLookup { get; set; } = new List<CurrencyLookupItem>();
		public List<UnitLookupItem> UnitLookup { get; set; } = new List<UnitLookupItem>();
	}
}
