﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Organizations
{
	public class UnitLookupItem
	{
		public int Id { get; set; }
		public string Name { get; set; }

		public static List<UnitLookupItem> GetLookup()
		{
			var lookup = new List<UnitLookupItem>();
			lookup.Add(new UnitLookupItem { Id = 1, Name = "Imperial" });
			lookup.Add(new UnitLookupItem { Id = 2, Name = "Metric" });

			return lookup;
		}
	}
}
