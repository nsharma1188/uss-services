﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Organizations
{
	public class CurrencyLookupItem
	{

		public int Id { get; set; }
		public string Name { get; set; }

		public static List<CurrencyLookupItem> GetLookup()
		{
			var lookup = new List<CurrencyLookupItem>();
			lookup.Add(new CurrencyLookupItem { Id = 1, Name = "USD" });
			lookup.Add(new CurrencyLookupItem { Id = 2, Name = "Pound" });
			lookup.Add(new CurrencyLookupItem { Id = 3, Name = "Euro" });
			lookup.Add(new CurrencyLookupItem { Id = 4, Name = "NOK" });
			return lookup;
		}

	}
}
