﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Organizations
{
	public class OrganizationModel
	{
		public string OrganizationId { get; set; }
		public string UserId { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string GroupId { get; set; }
		public string GovtLicenseNumber { get; set; }
		public string FederalTaxId { get; set; }
		public string Website { get; set; }
		public string ContactEmail { get; set; }
		public string AdsbSource { get; set; }
		public int CurrencyId { get; set; }
		public string BaseLat { get; set; }
		public string BaseLng { get; set; }
		public int UnitId { get; set; }
		public string LogoFile { get; set; }
		public string ContactPhone { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public List<int> UserRole { get; set; } = new List<int>();
	}
}
