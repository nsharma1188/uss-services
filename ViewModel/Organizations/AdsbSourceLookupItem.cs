﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Organizations
{
	public class AdsbSourceLookupItem
	{
		public int Id { get; set; }
		public string Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static List<AdsbSourceLookupItem> GetLookup()
		{
			var lookup = new List<AdsbSourceLookupItem>();

			lookup.Add(new AdsbSourceLookupItem { Id = 1, Name = "OpenSky" });
			lookup.Add(new AdsbSourceLookupItem { Id = 2, Name = "FlightAware" });

			return lookup;
		}

	}
}
