using AnraUssServices.Common.Lookups;
using AnraUssServices.Models;
using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.NotamAreas
{
    /// <summary>
    /// Notam - Temporary Flight Restriction
    /// </summary>    
    public class NotamAreaItem
    {
        public int NotamAreaId { get; set; }

        public string AreaName { get; set; }

        public List<DateTime> EffetiveDates { get; set; }

        public Utm.Models.Polygon Region { get; set; }

        public Utm.Models.Point Center { get; set; }

        public int Radius { get; set; }

        public int MinAltitude { get; set; }

        public int MaxAltitude { get; set; }

        public int NotamId { get; set; }
    }
}