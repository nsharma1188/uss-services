using AnraUssServices.Common.Lookups;
using AnraUssServices.ViewModel.NotamAreas;
using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Notams
{
    /// <summary>
    /// Notam - Temporary Flight Restriction
    /// </summary>    
    public class NotamItem
    {
        private int _typeid;
        private int _facilityid;
        private int _stateid;


        //public int NotamId { get; set; }

        public Guid NotamNumber { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string NotamReason { get; set; }

        public string TypeName { get; set; }

        public int TypeId
        {
            get { return _typeid; }
            set
            {
                _typeid = value;
                TypeName = NotamTypeLookup.GetLookup().Find(x => x.Id == TypeId).Name;
            }
        }

        public string FacilityName { get; set; }

        public int FacilityId
        {
            get { return _facilityid; }
            set
            {
                _facilityid = value;
                FacilityName = FacilityLookup.GetLookup().Find(x => x.Id == FacilityId).Name;
            }
        }
                

        public string StateName { get; set; }

        public int StateId
        {
            get { return _stateid; }
            set
            {
                _stateid = value;
                StateName = StateLookup.GetLookup().Find(x => x.Id == StateId).Name;
            }
        }

        public string Description { get; set; }

        public string UserId { get; set; }

        public NotamAreaItem AffectedArea { get; set; }

        public string Restriction { get; set; }

        public string[] Requirements { get; set; }

        public string Authority { get; set; }

        public string PointOfContact { get; set; }

        public string Status { get; set; }

        public Guid OrganizationId { get; set; }

        /// <summary>
        /// Notam Type Lookup
        /// </summary>
        public List<NotamTypeLookup> TypesLookup { get; set; } = new List<NotamTypeLookup>();

        /// <summary>
        /// Facility Lookup
        /// </summary>
        public List<FacilityLookup> FacilitiesLookup { get; set; } = new List<FacilityLookup>();

        /// <summary>
        /// State Lookup
        /// </summary>
        public List<StateLookup> StatesLookup { get; set; } = new List<StateLookup>();
    }
}