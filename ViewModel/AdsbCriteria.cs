﻿using AnraUssServices.Utm.Models;
using GeoAPI;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel
{ 
    public class AdsbCriteria
    {

        public AdsbCriteria(List<Coordinate> coords)
        {

            var factory = new GeometryFactory(new PrecisionModel(), 4326);
            var poly = (NetTopologySuite.Geometries.Polygon)factory.CreatePolygon(new LinearRing(coords.ToArray()));
            var envelope = poly.Boundary.EnvelopeInternal;

            LatMin = envelope.MinY;
            LatMax = envelope.MaxY;

            LngMin = envelope.MinX;
            LngMax = envelope.MaxX;
        }
         
        public double LatMin { get; set; } 
        public double LatMax { get; set; } 
        public double LngMin { get; set; } 
        public double LngMax { get; set; }

        public string ToQuery()
        {
            var precision = 6;
            var query = "{" + string.Format("range lat {0} {1}", Math.Round(LatMin, precision), Math.Round(LatMax, precision)) + "} ";
            query += "{" + string.Format("range lon {0} {1}", Math.Round(LngMin, precision), Math.Round(LngMax, precision)) + "} {true inAir}";
            return query;
        }
    }
}