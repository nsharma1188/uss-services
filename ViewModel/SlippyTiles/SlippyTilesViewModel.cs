﻿using AnraUssServices.Utm.Models;

namespace AnraUssServices.ViewModel.SlippyTiles
{
	public class SlippyTilesViewModel
	{
		/// <summary>
		/// Zoom level
		/// </summary>
		public int zoom { get; set; }

		/// <summary>
		/// Polygon
		/// </summary>
		public Polygon polygon { get; set; }

		/// <summary>
		/// Point
		/// </summary>
		public Point point { get; set; }
	}
}
