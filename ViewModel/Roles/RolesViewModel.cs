﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Roles
{
    public class RolesViewModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public List<ScopesViewModel> Scopes { get; set; }
    }

    public class ScopesViewModel
    {
        public string Id { get; set; }

        public string ScopeName { get; set; }

        public bool IsAssigned { get; set; } = false;
    }
}
