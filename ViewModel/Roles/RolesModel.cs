﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Roles
{
    public class RolesModel
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public List<ScopeModel> Scopes { get; set; }
    }

    public class ScopeModel
    {
        public string Id { get; set; }

        public string ScopeName { get; set; }
    }
}
