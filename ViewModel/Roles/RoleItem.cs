﻿using AnraUssServices.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Roles
{
	public class RoleItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public bool IsAssigned { get; set; } = false;

		/// <summary>
		/// Description of the CurrentRole
		/// </summary>
		/// <returns></returns>
		public static List<RoleItem> GetRoleDescription()
		{
			var roles = new List<RoleItem>();

			roles.Add(new RoleItem { Id = 1, Name = EnumUserRole.SUPERADMIN.ToString() });
			roles.Add(new RoleItem { Id = 2, Name = EnumUserRole.ANSP.ToString() });
			roles.Add(new RoleItem { Id = 3, Name = EnumUserRole.ATC.ToString() });
			roles.Add(new RoleItem { Id = 4, Name = EnumUserRole.ADMIN.ToString() });
			roles.Add(new RoleItem { Id = 5, Name = EnumUserRole.PILOT.ToString() });

			return roles;
		}

		/// <summary>
		/// Returns Role based on the RoleId
		/// </summary>
		/// <returns></returns>
		public static RoleItem GetRole(int id)
		{
			var role = GetRoleDescription().Where(x => x.Id == id).FirstOrDefault();

			return role;
		}

	}
}
