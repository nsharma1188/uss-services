﻿using System;

namespace AnraUssServices.ViewModel.Audits
{
    public class AuditItem
    {
        public string GroupId { get; set; }
        public string IpAddress { get; set; }
        public bool IsValidUser { get; set; }
        public string Platform { get; set; }
        public bool Status { get; set; }
        public string UserId { get; set; }
        public string OrganizationId { get; set; }
        public string UserName { get; set; }
        public string TimeStamp { get; set; }
    }
}
