﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.Models.Audits;

namespace AnraUssServices.ViewModel.Audits
{
	public class FinalAudit
	{
		public int TotalLogins { get; set; }

		public int TotalSuccessfull { get; set; }

		public int TotalFail { get; set; }

		public int InvalidUserName { get; set; }

		public int InvalidPassword { get; set; }

		public List<Audit> AuditList { get; set; }

	}
}
