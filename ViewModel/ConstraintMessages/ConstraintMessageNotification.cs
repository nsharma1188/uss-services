﻿using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Altitudes;
using System;

namespace AnraUssServices.ViewModel.Operations
{
    public class ConstraintMessageNotification
    {
        public Guid Gufi { get; set; }        
        public string Message { get; set; }
        public UASVolumeReservation ConstraintMessage { get; set; }
        public DateTime Timestamp { get; set; }
    }
}