﻿using AnraUssServices.Common;
using AnraUssServices.Common.Validators;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Altitudes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.ConstraintMessages
{
    /// <summary>
    /// A message. Used to make other stakeholders aware about an issue.
    /// </summary>
    [DataContract]
    public class ConstraintMessageViewModel
    {
        /// <summary>
        /// A UUID assigned to this message by the originator
        /// </summary>
        /// <value>A UUID assigned to this message by the originator</value>
        [DataMember(Name = "message_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "message_id")]
        public Guid MessageId { get; set; }

        /// <summary>
        /// A name identifying the originator of this message. If a USS is the originator, then this field MUST be the uss_id as known by the authorization server.
        /// </summary>
        /// <value>A name identifying the originator of this message. If a USS is the originator, then this field MUST be the uss_id as known by the authorization server.</value>
        [DataMember(Name = "uss_name", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_name")]
        [MinLength(4), MaxLength(250)]
        [Required]
        public string UssName { get; set; }

        /// <summary>
        /// The types of UAS operations that are permitted within the bound of the constraint. 
        /// When used in conjunction with permitted_gufis, the filters are an ‘OR’ relationship. 
        /// Any operation that matches either filter is allowed within the bounds of the constraint. 
        /// This list of permitted_uas types is not to be considered a final version. It is provided 
        /// to initiate discussion of how the filter should be defined.
        /// </summary>
        [DataMember(Name = "permitted_uas", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "permitted_uas")]
        [Required]
        [MinLength(1), MaxLength(8)]
        public List<UasType> PermittedUas { get; set; }

        /// <summary>
        /// The specific UAS operations that are permitted within the counds of the constraint. 
        /// When used in conjunction with permitted_uas, the filters are an ‘OR’ relationship. 
        /// Any operation that matches either filter is allowed within the bounds of the constraint. 
        /// The protocol for adding the appropriate GUFIs to this array needs to be discussed. 
        /// This may be a chicken/egg problem that will have to be worked through.
        /// </summary>
        [DataMember(Name = "permitted_gufis", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "permitted_gufis")]
        [MinLength(0), MaxLength(100)]
        [GuidListValidator]
        public List<string> PermittedGufis { get; set; }

        /// <summary>
        /// Gets or Sets property of cause
        /// </summary>
        /// <value>Gets or Sets property of cause</value>
        [DataMember(Name = "cause", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "cause")]
        [EnumDataType(typeof(ConstraintMessageCauseEnum))]
        [Required]
        public ConstraintMessageCauseEnum Cause { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "type")]
        [EnumDataType(typeof(ConstraintMessageType))]
        public ConstraintMessageType Type { get; set; }

        /// <summary>
        /// Gets or Sets ConstraintGeography
        /// </summary>
        [DataMember(Name = "geography", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "geography")]
        [Required]
        [PolygonValidator]
        public Polygon Geography { get; set; }

        /// <summary>
        /// The time that the constraint will begin to be in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time that the constraint will begin to be in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_begin", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_begin")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime EffectiveTimeBegin { get; set; }

        /// <summary>
        /// The time that the constraint will cease to be in effect..  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.
        /// </summary>
        /// <value>The time that the constraint will cease to be in effect..  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ. Seconds may have up to millisecond accuracy (three positions after decimal).  The 'Z' implies UTC times and is the only timezone accepted.</value>
        [DataMember(Name = "effective_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "effective_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        [Required]
        public DateTime EffectiveTimeEnd { get; set; }

        /// <summary>
        /// Time that the constaint actually ceased being in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.  This value is likely used when a constraint ends early, but is not required.
        /// </summary>
        /// <value>Time that the constaint actually ceased being in effect.  Uses the ISO 8601 format conforming to pattern: YYYY-MM-DDThh:mm:ss.sssZ.  Seconds may have up to millisecond accuracy (three positions after decimal). The 'Z' implies UTC times and is the only timezone accepted.  This value is likely used when a constraint ends early, but is not required.</value>
        [DataMember(Name = "actual_time_end", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "actual_time_end")]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ActualTimeEnd { get; set; }

        /// <summary>
        /// The minimum altitude of the constraint. Must be less than max_altitude.
        /// </summary>
        /// <value>The minimum altitude of the constraint. Must be less than max_altitude.</value>
        [DataMember(Name = "min_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "min_altitude")]
        [Required]
        public AltitudeViewModel MinAltitude { get; set; }

        /// <summary>
        /// The maximum altitude of the constraint. Must be greater than min_altitude.
        /// </summary>
        /// <value>The maximum altitude of the constraint. Must be greater than min_altitude.</value>
        [DataMember(Name = "max_altitude", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "max_altitude")]
        [Required]
        public AltitudeViewModel MaxAltitude { get; set; }

        /// <summary>
        /// The reason for the constraint. Meant for human consumption.
        /// </summary>
        /// <value>The reason for the constraint. Meant for human consumption.</value>
        [DataMember(Name = "reason", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reason")]
        [StringLength(1000)]
        public string Reason { get; set; }

        /// <summary>
        /// External/Internal Constraint Message
        /// </summary>        
        [DataMember(Name = "is_restriction", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "is_restriction")]
        public bool IsRestriction { get; set; }

        /// <summary>
        /// IsExpired
        /// </summary>        
        [DataMember(Name = "is_expired", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "is_expired")]
        public bool IsExpired { get; set; }

        /// <summary>
        /// Id of specific Uss
        /// </summary>    
        [DataMember(Name = "uss_instance_id", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "uss_instance_id")]        
        public string UssInstanceId { get; set; }
    }
}
