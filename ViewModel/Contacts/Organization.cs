﻿using AnraUssServices.Common.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Contacts
{
	public class Organizations
	{
		public Guid OrganizationId { get; set; }
		public string Name { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
	}
}
