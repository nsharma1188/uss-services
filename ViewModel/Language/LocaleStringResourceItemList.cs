﻿using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Language
{
    public class LocaleStringResourceItemList
    {
        public List<LocaleStringResourceItem> localeStringResourceItems { get; set; }
    }
}
