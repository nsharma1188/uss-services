using System;

namespace AnraUssServices.ViewModel.Language
{
    public class LanguageResourceItem
    {
        public string ResourceName { get; set; }

        public string ResourceValue { get; set; }

        public Guid LanguageGufi { get; set; }
    }
}