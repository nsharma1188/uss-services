using System;

namespace AnraUssServices.ViewModel.Language
{
    public class LanguageItem
    {
        //public int Id { get; set; }

        //public Guid LanguageId { get; set; }

        public string Name { get; set; }

        public string LanguageCode { get; set; }

        public string LanguageCulture { get; set; }

        public string FlagImageFileName { get; set; }

        public bool Published { get; set; }

        public int DisplayOrder { get; set; }

        public static implicit operator LanguageItem(Models.Language v)
        {
            throw new NotImplementedException();
        }
    }
}