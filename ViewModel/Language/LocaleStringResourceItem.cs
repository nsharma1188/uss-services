using System;

namespace AnraUssServices.ViewModel.Language
{
    public class LocaleStringResourceItem
    {
        //public int Id { get; set; }

        //public Guid ResourceId { get; set; }

        public string ResourceName { get; set; }

        public string ResourceValue { get; set; }

        public int LanguageId { get; set; }
    }
}