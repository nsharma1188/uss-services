﻿using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Language
{
    public class LanguageResourceItemList
    {
        public List<LanguageResourceItem> languageResourceItems { get; set; }
    }
}
