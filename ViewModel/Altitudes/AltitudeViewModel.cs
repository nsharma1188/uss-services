﻿using AnraUssServices.Common;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Altitudes
{
    [DataContract]
    public class AltitudeViewModel
    {
        /// <summary>
        /// The numeric value of the altitude. Note that min and max values are added as a sanity check. 
        /// As use cases evolve and more options are made available in terms of units of measure or reference systems, 
        /// these bounds should be re-evaluated.
        /// </summary>                
        [DataMember(Name = "altitude_value", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "altitude_value")]
        public double AltitudeValue { get; set; }

        /// <summary>
        /// A code indicating the reference for a vertical distance. See AIXM 5.1 and FIXM 4.1.0.
        /// Currently, UTM only allows WGS84 with no immediate plans to allow other options.FIXM and AIXM allow for ‘SFC’ which is equivalent to AGL.                
        /// </summary>
        [DataMember(Name = "vertical_reference", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "vertical_reference")]
        [EnumDataType(typeof(VerticalReferenceTypeEnum))]
        public VerticalReferenceTypeEnum VerticalReference { get; set; }

        /// <summary>
        /// The reference quantities used to express the value of altitude.See FIXM 4.1.
        /// Currently, UTM only allows feet with no immediate plans to allow other options. FIXM allows for feet or meters.
        /// </summary>
        [DataMember(Name = "units_of_measure", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "units_of_measure")]
        [EnumDataType(typeof(UomHeightTypeEnum))]
        public UomHeightTypeEnum UnitsOfMeasure { get; set; }

        /// <summary>
        /// Experimental field for testing and discussion to determine applicability.
        /// </summary>
        [DataMember(Name = "source", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "source")]
        [EnumDataType(typeof(AltitudeSourceEnum))]
        public AltitudeSourceEnum? Source { get; set; }
    }
}
