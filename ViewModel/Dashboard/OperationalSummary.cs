﻿namespace AnraUssServices.ViewModel.Dashboard
{
    public class OperationalSummary
    {
        public string Month { get; set; }
        public int TotalNoneOps { get; set; }        
        public int TotalPublicSafteyOps { get; set; }
        public int TotalInflightEmergencyOps { get; set; }      
    }
}