namespace AnraUssServices.ViewModel.Dashboard
{
    /// <summary>
    /// Dashboard Item
    /// </summary>    
    public class DashboardItem
    {
        /// <summary>
        /// TotalOperations - Count of active operations of logged in USS Operator
        /// </summary>
        public int TotalOperations { get; set; }

        /// <summary>
        /// TotalDrones - Count of registered drones of logged in USS Operator
        /// </summary>
        public int TotalDrones { get; set; }

        /// <summary>
        /// TotalUSSInstances - Count of USS Instances of logged in USS Operator
        /// </summary>
        public int TotalUSSInstances { get; set; }

        /// <summary>
        /// TotalActiveUSSNetwork - Count of Active USS Instances of logged in USS Operator
        /// </summary>
        public int TotalActiveUSSNetwork { get; set; }

        /// <summary>
        /// TotalActiveUVR - Count of Active UVR Instances
        /// </summary>
        public int TotalActiveUvrs { get; set; }

    }
}