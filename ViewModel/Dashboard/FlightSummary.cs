﻿namespace AnraUssServices.ViewModel.Dashboard
{
    public class FlightSummary
    {
        /// <summary>
        /// Month
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// TotalFlights - Count of closed operations of logged in USS Operator
        /// </summary>
        public int TotalFlights { get; set; }

        /// <summary>
        /// TotalHours - Sum of Flight Duration Of Closed Operation of logged in USS Operator
        /// </summary>
        public decimal TotalHours { get; set; }       
    }
}