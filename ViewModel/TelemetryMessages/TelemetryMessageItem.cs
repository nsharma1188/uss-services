using AnraUssServices.ViewModel.Detects;
using Nest;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.TelemetryMessages
{
    /// <summary>
    /// A message. Used to make other stakeholders aware about an issue.
    /// </summary>
    [DataContract] 
    public class TelemetryMessageViewModel : Utm.Models.Position
    {
        /// <summary>
        /// The roll as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The roll as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "roll", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "roll")]
        [Required]
        public double? Roll { get; set; }

        /// <summary>
        /// The yaw as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The yaw as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "yaw", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "yaw")]
        [Required]
        public double? Yaw { get; set; }

        /// <summary>
        /// The pitch as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The pitch as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "pitch", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "pitch")]
        [Required]
        public double? Pitch { get; set; }

        /// <summary>
        /// The climbrate as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The pitch as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "climbrate", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "climbrate")]
        public double? Climbrate { get; set; }

        /// <summary>
        /// The heading as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The pitch as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "heading", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "heading")]
        public int? Heading { get; set; }

        /// <summary>
        /// The heading as measured via a GPS device on the aircraft.
        /// </summary>
        /// <value>The pitch as measured via a GPS device on the aircraft.</value>
        [DataMember(Name = "battery_remaining", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "battery_remaining")]
        public int? BatteryRemaining { get; set; }

        /// <summary>
        /// current state of the aircraft.
        /// </summary>
        /// <value>current state of the aircraft.
        [DataMember(Name = "mode", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "mode")]
        public string Mode { get; set; }

        /// <summary>
        /// The registration ID of the vehicle flying this operation. This registration value is provided to operators by a vehicle registrar.
        /// </summary>
        /// <value>The registration ID of the vehicle flying this operation. This registration value is provided to operators by a vehicle registrar.
        [DataMember(Name = "registration", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "registration")]
        [Required]
        public string Registration { get; set; }


        internal DetectViewModel[] ToDetectsArray()
        {
            var detect = new DetectViewModel
            {
                DetectId = Guid.NewGuid(),
                Altitude = this.AltitudeGps.AltitudeValue,
                Bearing = this.TrackBearing,
                Heading = this.Heading,
                Latitude = this.Location.Coordinates[1].Value,
                Longitude = this.Location.Coordinates[0].Value,
                Type = "TRACK",
                Vendor = "USS",
                Uid = Registration,
                TimeStamp = DateTime.UtcNow,
                Source = "RADAR",
                SourceClass = "UNKNOWN",
                Metadata1 = $"{UssName}({Gufi})"
            };

            return new DetectViewModel[] { detect }; 
        }
    }
}