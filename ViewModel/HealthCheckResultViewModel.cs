﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel
{
    public class HealthCheckResultViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
