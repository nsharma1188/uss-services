﻿using AnraUssServices.ViewModel.Operations;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AnraUssServices.ViewModel.Conflicts
{    
    public class ConflictViewModel
    {
        [DataMember(Name = "operationconflict", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "operationconflict")]
        public OperationConflictViewModel OperationConflict { get; set; }

        [DataMember(Name = "reason", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "reason")]
        public IEnumerable<Utm.Models.OperationVolume> Reason { get; set; }

        [DataMember(Name = "is_internal_conflict", EmitDefaultValue = false)]
        [JsonProperty(PropertyName = "is_internal_conflict")]
        public bool IsInternalConflict { get; set; }
    }
}
