using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Roles;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Users
{
    public class UserItem
    {
		public string UserId { get; set; }
		public string UserName { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public string OrganizationId { get; set; }
		public string OrganizationName { get; set; }
		public string CurrentPassword { get; set; }
		public string NewPassword { get; set; }
		public string GroupId { get; set; }

		public List<RoleItem> Roles { get; set; } = new List<RoleItem>();

		public List<OrganizationItem> Organizations { get; set; } = new List<OrganizationItem>();

	}
}