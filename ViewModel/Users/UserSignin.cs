﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Users
{
	public class UserSignin
	{
		/// <summary>
		/// Username 
		/// </summary>
		public string UserName { get; set; }

		/// <summary>
		/// Password
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// Verification Code
		/// </summary>
		public string VerificationCode { get; set; }
	}
}
