﻿using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Roles;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Users
{
    /// <summary>
    /// OAuth User's Response Model for Get
    /// </summary>
    public class UserResponseModel
    {
        [JsonProperty(PropertyName = "enabled")]
        public bool Enabled { get; set; }

        [JsonProperty(PropertyName = "credentialsNonExpired")]
        public bool CredentialsNonExpired { get; set; }

        [JsonProperty(PropertyName = "accountNonExpired")]
        public bool AccountNonExpired { get; set; }

        [JsonProperty(PropertyName = "accountNonLocked")]
        public bool AccountNonLocked { get; set; }

        [JsonProperty(PropertyName = "authorities")]
        public List<Authorities> Authorities { get; set; }

        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "UserName")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty(PropertyName = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [JsonProperty(PropertyName = "AddressLine2")]
        public string AddressLine2 { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "Zipcode")]
        public string Zipcode { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "GroupId")]
        public string GroupId { get; set; }

        [JsonProperty(PropertyName = "roles")]
        public List<RolesModel> Roles { get; set; }

        [JsonProperty(PropertyName = "organization")]
        public OrganizationItem Organization { get; set; }
    }

    public class Authorities
    {
        [JsonProperty(PropertyName = "authority")]
        public string Authority { get; set; }
    }
}
