﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.ViewModel.Users
{
    public class UserChangePassword
    {
        public string UserId { get; set; }

        public string UserPassword { get; set; }
    }
}
