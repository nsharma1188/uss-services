﻿using System;
using System.Collections.Generic;

namespace AnraUssServices.ViewModel.Ureps
{
    public class UrepCriteria
    {
        public List<string> fields { get; set; }
        public string sort { get; set; }
        public string sortD { get; set; }
        public int? limit { get; set; }
        public int? offset { get; set; }
        public double? lat { get; set; }
        public double? lon { get; set; }
        public double? radius { get; set; }
        public DateTime? before { get; set; }
        public DateTime? after { get; set; }
    }
}