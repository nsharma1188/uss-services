﻿using AnraUssServices.Common;
using AnraUssServices.Common.Kafka;
using AnraUssServices.Common.Mqtt;
using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.BackgroundServices
{
    public class KafkaBackgroundService : BaseBackgroundService
    {
        private readonly KafkaConsumer kafkaConsumer;
        private readonly AnraConfiguration anraConfiguration;

        public KafkaBackgroundService(KafkaConsumer kafkaConsumer, AnraConfiguration anraConfiguration)
        {
            this.kafkaConsumer = kafkaConsumer;
            this.anraConfiguration = anraConfiguration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //Starting the kafka consumer and adding the subscription
            kafkaConsumer.StartConsumer();

            var topic = $"{anraConfiguration.Product}-{anraConfiguration.Environment}-{KafkaTopics.ConformanceCheck}";

            //More Topic subscription can be added here
            var topicList = new List<string>(){ topic };

            kafkaConsumer.Subscribe(topicList);

            while (!stoppingToken.IsCancellationRequested)
            {
                //Processign the consumed data
                kafkaConsumer.ProcessConsumedMessage();
            }

            await Task.Delay(10, stoppingToken);
        }
    }
}
