﻿using AnraUssServices.BackgroundJobs.Notam;
using AnraUssServices.BackgroundJobs.Operation;
using AnraUssServices.BackgroundJobs.UssInstances;
using AnraUssServices.Common;
using FluentScheduler;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.BackgroundServices
{
    public class SchedulingService : BaseBackgroundService
    {
        private readonly AnraMqttClient anraMqttClient;
        private readonly IServiceProvider serviceProvider;
        private readonly AnraConfiguration configuration;
        private readonly ILogger<string> logger;

        public SchedulingService(ILogger<string> logger, IServiceProvider serviceProvider, AnraConfiguration configuration, AnraMqttClient anraMqttClient)
        {
            this.logger = logger;
            this.serviceProvider = serviceProvider;
            this.configuration = configuration;
            this.anraMqttClient = anraMqttClient;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            #if DEBUG
                logger.LogInformation("ANRA::: BackgroundJobSchedulingService");
            #endif

            await anraMqttClient.InitAsync();

            JobManager.AddJob(() => new OperationStatusJob(serviceProvider, logger).Execute(), (s) => s.WithName("OperationStatusJob").ToRunEvery(1).Minutes());
            JobManager.AddJob(() => new DetectionSubscriptionStatus(serviceProvider, logger).Execute(), (s) => s.WithName("SubscriptionExpirationJob").ToRunEvery(1).Minutes());
            JobManager.AddJob(() => new NotamJob(serviceProvider, logger).Execute(), (s) => s.WithName("NotamJob").ToRunNow().AndEvery(1).Minutes());
            JobManager.AddJob(() => new UssInstanceJob(serviceProvider, logger, configuration, anraMqttClient).Execute(), (s) => s.WithName("UssInstanceJob").ToRunNow().AndEvery(5).Minutes());
            JobManager.AddJob(() => new UVRJob(serviceProvider, logger).Execute(), (s) => s.WithName("UVRJob").ToRunNow().AndEvery(1).Minutes());
            await Task.Delay(10, stoppingToken);
        }

        public override void Dispose()
        {
            JobManager.StopAndBlock();
            _stoppingCts.Cancel();
        }
    }
}