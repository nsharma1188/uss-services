﻿using LiteDB;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace AnraUssServices.Common.UssExchangeData
{
	public class UssExchange
	{
		public UssExchange(ReportingUssRoleEnum reportingUssRole, string comment = "No Comments")
		{
			reporting_uss_role = reportingUssRole;
			comments = comment;
			measurement_id = Guid.NewGuid();
		}

		/// <summary>
		/// A UUID assingned by the reporting USS for this instance of USSExchange.
		/// </summary>
		/// <value>A UUID assingned by the reporting USS for this instance of USSExchange.</value>
		[DataMember(Name = "measurement_id", EmitDefaultValue = false)]
		[BsonId]
		public Guid measurement_id { get; set; }

		/// <summary>
		/// A string provided by the owner of the overall test (likely NASA) that identifies the event within which this data exchange occurs. NASA will define a pattern for this for consistency across tests.
		/// </summary>
		/// <value>A string provided by the owner of the overall test (likely NASA) that identifies the event within which this data exchange occurs. NASA will define a pattern for this for consistency across tests.</value>
		[DataMember(Name = "event_id", EmitDefaultValue = false)]
		public string event_id { get; set; }

		/// <summary>
		/// Gets or Sets Primary Key in the Data Packet
		/// </summary>
		[DataMember(Name = "exchanged_data_pk", EmitDefaultValue = false)]
		public Guid exchanged_data_pk { get; set; }

		/// <summary>
		/// Gets or Sets ExchangedDataType
		/// </summary>
		[DataMember(Name = "exchanged_data_type", EmitDefaultValue = false)]
		public ExchangedDataTypeEnum exchanged_data_type { get; set; }

		/// <summary>
		/// Gets or Sets name of SourceUss
		/// </summary>
		[DataMember(Name = "source_uss", EmitDefaultValue = false)]
		public string source_uss { get; set; }

		/// <summary>
		/// Gets or Sets name of TargetUss
		/// </summary>
		[DataMember(Name = "target_uss", EmitDefaultValue = false)]
		public string target_uss { get; set; }

		/// <summary>
		/// Gets or Sets name of Uss Name
		/// </summary>
		[DataMember(Name = "uss_name", EmitDefaultValue = false)]
		public string uss_name { get; set; }

		/// <summary>
		/// An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).
		/// </summary>
		/// <value>An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).</value>
		[DataMember(Name = "reporting_uss_role", EmitDefaultValue = false)]
		public ReportingUssRoleEnum reporting_uss_role { get; set; }

		/// <summary>
		/// If SOURCE_USS, this is the time that the request is sent to the TARGET_USS. If TARGET_USS, this is the time that the request was received from the SOURCE_USS. Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).
		/// </summary>
		/// <value>If SOURCE_USS, this is the time that the request is sent to the TARGET_USS. If TARGET_USS, this is the time that the request was received from the SOURCE_USS. Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).</value>
		[DataMember(Name = "time_request_initiation", EmitDefaultValue = false)]
		public DateTime time_request_initiation { get; set; }

		/// <summary>
		/// If SOURCE_USS, this is the time that the response was received from the TARGET_USS. If TARGET_USS, this is the time that the request was sent back to the SOURCE_USS.            Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).
		/// </summary>
		/// <value>If SOURCE_USS, this is the time that the response was received from the TARGET_USS. If TARGET_USS, this is the time that the request was sent back to the SOURCE_USS.            Same formatting rules as in other UTM exchanges (ms, &#39;Z&#39;).</value>
		[DataMember(Name = "time_request_completed", EmitDefaultValue = false)]
		public DateTime time_request_completed { get; set; }

		/// <summary>
		/// The endpoint to which the data request was initially sent.
		/// </summary>
		/// <value>The endpoint to which the data request was initially sent.</value>
		[DataMember(Name = "endpoint", EmitDefaultValue = false)]
		public string endpoint { get; set; }

		/// <summary>
		/// The HTTP method used in this exchange.
		/// </summary>
		/// <value>The HTTP method used in this exchange.</value>
		[DataMember(Name = "http_method", EmitDefaultValue = false)]
		public HttpMethodEnum http_method { get; set; }

		/// <summary>
		/// The expected HTTP response.  This is required ONLY if the reporting_uss_role is SOURCE_USS.
		/// </summary>
		/// <value>The expected HTTP response.  This is required ONLY if the reporting_uss_role is SOURCE_USS.</value>
		[DataMember(Name = "expected_http_response", EmitDefaultValue = false)]
		[Range(100, 599)]
		public int? expected_http_response { get; set; }

		/// <summary>
		/// The actual HTTP response sent by the TARGET_USS to the SOURCE_USS. Must be reported by USSs in either role.
		/// </summary>
		/// <value>The actual HTTP response sent by the TARGET_USS to the SOURCE_USS. Must be reported by USSs in either role.</value>
		[DataMember(Name = "actual_http_response", EmitDefaultValue = false)]
		[Range(100, 599)]
		public int actual_http_response { get; set; }

		/// <summary>
		/// The JWS that was included in the data exchange. If a PUT/POST then the SOURCE_USS would have generated and sent to TARGET_USS.  If a GET then the TARGET_USS would have generated and sent to the SOURCE_USS.  In either case, both the SOURCE_USS and the TARGET_USS should include the resulting JWS here.
		/// </summary>
		/// <value>The JWS that was included in the data exchange. If a PUT/POST then the SOURCE_USS would have generated and sent to TARGET_USS.  If a GET then the TARGET_USS would have generated and sent to the SOURCE_USS.  In either case, both the SOURCE_USS and the TARGET_USS should include the resulting JWS here.</value>
		[DataMember(Name = "jws", EmitDefaultValue = false)]
		public string jws { get; set; }

		/// <summary>
		/// If available, include the public key for decoding the jws.
		/// </summary>
		/// <value>If available, include the public key for decoding the jws.</value>
		[DataMember(Name = "jws_public_key", EmitDefaultValue = false)]
		public string jws_public_key { get; set; }

		/// <summary>
		/// Any additional comments that could aid in analysis involving these data.
		/// </summary>
		/// <value>Any additional comments that could aid in analysis involving these data.</value>
		[DataMember(Name = "comments", EmitDefaultValue = false)]
		[MaxLength(1000)]
		public string comments { get; set; }

		/// <summary>
		/// Any content in the response
		/// </summary>
		/// <value>Any additional comments that could aid in analysis involving these data.</value>
		[DataMember(Name = "response_content", EmitDefaultValue = false)]
		public string response_content { get; set; }


		/// <summary>
		/// Any content in the request
		/// </summary>
		/// <value>Any additional comments that could aid in analysis involving these data.</value>
		[DataMember(Name = "request_content", EmitDefaultValue = false)]
		public string request_content { get; set; }
	}
}
