﻿using AnraUssServices.Common.Mqtt;
using LiteDB;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AnraUssServices.Common.UssExchangeData
{
    public class UssExchangeDataHandler
    {
        private readonly AnraMqttClient mqttClient;
        private readonly ILogger<string> _logger;
        private readonly LiteDatabase database;

        public UssExchangeDataHandler(ILogger<string> logger, AnraConfiguration configuration, AnraMqttClient mqttClient)
        {
            _logger = logger;

            var dbName = Path.Combine(configuration.UssLiteDbPath, $"{DateTime.Now.ToString("yyyy_MM_dd")}.db");
            #if DEBUG
                _logger.LogInformation("LiteDbPath = " + dbName);
            #endif

            database = new LiteDatabase(dbName);
            this.mqttClient = mqttClient;
        }

        public async void SaveAsync(UssExchange item)
        {
            mqttClient.PublishTopic(MqttTopics.NotifyUssExchange, item);
            await Task.Run(() => database.GetCollection<UssExchange>("ussexchange").Insert(item));
        }
    }
}