﻿using AnraUssServices.Utm.Models;
using Mapster;
using System;
using System.Linq;

namespace AnraUssServices.Common
{
    public class UtmMessagesHelper
    {
		public readonly OperationSeverityHelper operationSeverityHelper;

		public UtmMessagesHelper(OperationSeverityHelper operationSeverityHelper)
		{
			this.operationSeverityHelper = operationSeverityHelper;
		}


		public UTMMessage GetConfirmingOperationUtmMessage(Models.Operation operation, Position lastKnownPosition, Models.UtmInstance ussInstance)
        {
            UTMMessage utmMessage = null;

            utmMessage = new UTMMessage
            {
                MessageId = Guid.NewGuid(),
                UssName = operation.UssName,
                DiscoveryReference = operation.UssInstanceId.ToString(),
                Gufi = operation.Gufi,
                SentTime = DateTime.UtcNow.AddSeconds(-1),
                FreeText = $"Operation is {operation.State}",
                Severity = Severity.NOTICE,
                MessageType = MessageTypeEnum.OPERATION_CONFORMING,
                LastKnownPosition = lastKnownPosition,
                Contingency = operation.ContingencyPlans != null ? operation.ContingencyPlans.FirstOrDefault().Adapt<ContingencyPlan>() : null,
                Callback = ussInstance.UssBaseCallbackUrl
            };

            return utmMessage;
        }

        public UTMMessage GetClosedOperationUtmMessage(Models.Operation operation, Position lastKnownPosition, Models.UtmInstance ussInstance, string lastState)
        {
            UTMMessage utmMessage = null;

			var severity = operationSeverityHelper.GetClosedOperationSeverity(lastState);

			utmMessage = new UTMMessage
            {
                MessageId = Guid.NewGuid(),                
                DiscoveryReference = operation.UssInstanceId.ToString(),
                UssName = operation.UssName,
                Gufi = operation.Gufi,
                SentTime = DateTime.UtcNow.AddSeconds(-1),
                FreeText = $"Operation is {operation.State}",
                Severity = severity,
                MessageType = MessageTypeEnum.OPERATION_CLOSED,
                LastKnownPosition = lastKnownPosition,
                Contingency = operation.ContingencyPlans != null ? operation.ContingencyPlans.FirstOrDefault().Adapt<ContingencyPlan>() : null,
                Callback = ussInstance.UssBaseCallbackUrl
            };

            return utmMessage;
        }

        public UTMMessage GetNonConfirmingOperationUtmMessage(Models.Operation operation, Position lastKnownPosition, Models.UtmInstance ussInstance)
        {
            UTMMessage utmMessage = null;

			var severity = operationSeverityHelper.GetNonConformingOperationSeverity(operation);

			utmMessage = new UTMMessage
            {
                MessageId = Guid.NewGuid(),                
                DiscoveryReference = operation.UssInstanceId.ToString(),
                UssName = operation.UssName,
                Gufi = operation.Gufi,
                SentTime = DateTime.UtcNow.AddSeconds(-1),
                FreeText = $"Operation is {operation.State}",
                Severity = severity,
				MessageType = MessageTypeEnum.OPERATION_NONCONFORMING,
                LastKnownPosition = lastKnownPosition,
                Contingency = operation.ContingencyPlans != null ? operation.ContingencyPlans.FirstOrDefault().Adapt<ContingencyPlan>() : null,
                Callback = ussInstance.UssBaseCallbackUrl
            };

            return utmMessage;
        }

        public UTMMessage GetRogueOperationUtmMessage(Models.Operation operation, Position lastKnownPosition, Models.UtmInstance ussInstance)
        {
			UTMMessage utmMessage = null;

			var severity = operationSeverityHelper.GetRogueOperationSeverity(operation);

			utmMessage = new UTMMessage
            {
                MessageId = Guid.NewGuid(),                
                DiscoveryReference = operation.UssInstanceId.ToString(),
                UssName = operation.UssName,
                Gufi = operation.Gufi,
                SentTime = DateTime.UtcNow.AddSeconds(-1),
                FreeText = $"Operation Status is {operation.State}",
                Severity = severity,
				MessageType = MessageTypeEnum.OPERATION_ROGUE,
                LastKnownPosition = lastKnownPosition,
                Contingency = operation.ContingencyPlans != null ? operation.ContingencyPlans.FirstOrDefault().Adapt<ContingencyPlan>() : null,
                Callback = ussInstance.UssBaseCallbackUrl
            };

            return utmMessage;
        }
    }
}