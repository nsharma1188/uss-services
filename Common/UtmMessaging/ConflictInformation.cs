﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.UtmMessaging
{
	public class ConflictInformation
	{
		public string Message { get; set; }
		public List<Conflict> ConflictDetails { get; set; }
	}

	public class Conflict
	{
		public int? Ordinal { get; set; }
		public DateTime EffectiveTimeBegin { get; set; }
		public DateTime EffectiveTimeEnd { get; set; }
		public double MinAltitude { get; set; }
		public double MaxAltitude { get; set; }
		public string OperationGeography { get; set; }
	}
}
