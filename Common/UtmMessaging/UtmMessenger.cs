﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnraUssServices.Common.UtmMessaging
{
    public class UtmMessenger
    {
        private readonly ViewerMqttNotification ViewerMqttNotification;
        private readonly ILogger<UtmMessenger> logger;
        private readonly AnraConfiguration configuration;
        private readonly DatabaseFunctions dbFunctions;
        private readonly IMediator mediator;
        private readonly GridCell gridCellLogic;
        private IRepository<Models.Operation> operationRepository;

        public UtmMessenger(AnraConfiguration configuration, 
            IMediator mediator, 
            DatabaseFunctions dbFunctions,
            GridCell gridCellLogic,
            ViewerMqttNotification ViewerMqttNotification,
            IRepository<Models.Operation> operationRepository,
            ILogger<UtmMessenger> logger)
        {
            this.mediator = mediator;
            this.dbFunctions = dbFunctions;
            this.configuration = configuration;
            this.gridCellLogic = gridCellLogic;
            this.logger = logger;
            this.ViewerMqttNotification = ViewerMqttNotification;
            this.operationRepository = operationRepository;
        }

        public async Task NotifyOperationToLUN(Models.Operation operation, List<GridCellOperatorResponse> operators, bool isNew = false)
        {
            if (configuration.IsUtmEnabled)
            {                
                var utmOperation = operation.Adapt<Utm.Models.Operation>();

                //Set Negotiation Agreement List to Null If It has no items.
                if (utmOperation.NegotiationAgreements.Count.Equals(0))
                {
                    utmOperation.NegotiationAgreements = null;
                }

				//To set the Discovery Reference to null if the length is not within the range (For Mobile Client)
				if (utmOperation.DiscoveryReference != null)
				{
					if (utmOperation.DiscoveryReference.Length < 5 || utmOperation.DiscoveryReference.Length > 36)
					{
						utmOperation.DiscoveryReference = null;
					}
				}

                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: NotifyOperationToLUN :: Operation : {0} ", JsonConvert.SerializeObject(utmOperation, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await mediator.Send(new AsyncInterOperationNotification(utmOperation, operators, isNew));

                //await ViewerMqttNotification.NotifyOperation(utmOperation);
            }
        }

        public async Task NotifyUVRToLUN(UASVolumeReservation uvr, MqttMessageType mqttMessageType, List<GridCellOperatorResponse> operators)
        {
            if (configuration.IsUtmEnabled)
            {
                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: NotifyUVRToLUN :: UVR : {0} ", JsonConvert.SerializeObject(uvr));
                #endif

                mediator.Send(new AsyncConstraintMessageNotification(uvr, mqttMessageType, operators));
            }
        }

        public async Task NotifyOperationStatusToLUN(UTMMessage utmMessage, Guid ussInstanceId)
        {
            if (configuration.IsUtmEnabled)
            {
                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: NotifyOperationStatusToLUN :: OperationStatus : {0}",JsonConvert.SerializeObject(utmMessage));
                #endif

                var lun = dbFunctions.GetLocalUssNetworkByUtmInstanceId(ussInstanceId);
                mediator.Send(new AsyncOperationStatusNotification(utmMessage, lun));

                ViewerMqttNotification.NotifyUtmMessage(utmMessage);
            }
        }

        public async Task NotifyOperationStatusToLUN(UTMMessage utmMessage, List<GridCellOperatorResponse> operators)
        {
            if (configuration.IsUtmEnabled)
            {
                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: NotifyOperationStatusToLUN :: OperationStatus : {0}", JsonConvert.SerializeObject(utmMessage));
                #endif

                mediator.Send(new AsyncInterOperationStatusNotification(utmMessage, operators));

                ViewerMqttNotification.NotifyUtmMessage(utmMessage);
            }
        }


        public async Task NotifyPositionToLUN(Position position, Utm.Models.Operation operation, List<UtmMessage> utmMessages, bool isSinglePositionReport = false)
        {
            if (configuration.IsUtmEnabled)
            {
				var model = operationRepository.Find(operation.Gufi);

                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: NotifyPositionToLUN :: Position : {0}",JsonConvert.SerializeObject(position));
                #endif

                await mediator.Send(new AsyncPositionNotification(position, operation, gridCellLogic.GetGridCellOperators(model.SlippyTileData), utmMessages, isSinglePositionReport));

                await ViewerMqttNotification.NotifyPositionMessage(position);
            }
        }

        public void PutUssDiscoveryService(UssInstance instance)
        {
            if (configuration.IsUtmEnabled)
            {
                #if DEBUG
                    logger.LogInformation("UtmMessenger ::: PutUssDiscoveryService :: UssInstance : {0}", JsonConvert.SerializeObject(instance));
                #endif

                mediator.Send(new AsyncUssDiscoveryNotification(instance));
            }
        }
    }
}