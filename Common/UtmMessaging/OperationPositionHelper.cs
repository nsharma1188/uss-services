﻿using AnraUssServices.Models;
using Mapster;
using System;
using System.Linq;

namespace AnraUssServices.Common
{
    public class OperationPositionHelper
    {
        public static Utm.Models.Position GetLastKnownOperationPosition(IRepository<TelemetryMessage> telemetryRepo, Guid gufi)
        {
            var lastPostion = telemetryRepo.GetAll().Where(p => p.Gufi.Equals(gufi.ToString())).OrderByDescending(p => p.TimeMeasured).FirstOrDefault();
            return lastPostion.Adapt<Utm.Models.Position>();
        }
    }
}