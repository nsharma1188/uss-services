﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AnraUssServices.Common
{
    public enum ContingencyCauseEnum
    {
        /// <summary>
        /// Enum LOSTC2UPLINK for "LOST_C2_UPLINK"
        /// </summary>
        [Description("LOST_C2_UPLINK"), EnumMember(Value = "LOST_C2_UPLINK")]
        LOSTC2UPLINK =  1,

        /// <summary>
        /// Enum LOSTC2DOWNLINK for "LOST_C2_DOWNLINK"
        /// </summary>
        [Description("LOST_C2_DOWNLINK"), EnumMember(Value = "LOST_C2_DOWNLINK")]
        LOSTC2DOWNLINK =  2,

        /// <summary>
        /// Enum LOSTNAV for "LOST_NAV"
        /// </summary>
        [Description("LOST_NAV"), EnumMember(Value = "LOST_NAV")]
        LOSTNAV =  3,

        /// <summary>
        /// Enum LOSTSAA for "LOST_SAA"
        /// </summary>
        [Description("LOST_SAA"), EnumMember(Value = "LOST_SAA")]
        LOSTSAA =  4,

        /// <summary>
        /// Enum LOWFUEL for "LOW_FUEL"
        /// </summary>
        [Description("LOW_FUEL"), EnumMember(Value = "LOW_FUEL")]
        LOWFUEL =  5,

        /// <summary>
        /// Enum NOFUEL for "NO_FUEL"
        /// </summary>
        [Description("NO_FUEL"), EnumMember(Value = "NO_FUEL")]
        NOFUEL =  6,

        /// <summary>
        /// Enum MECHANICALPROBLEM for "MECHANICAL_PROBLEM"
        /// </summary>
        [Description("MECHANICAL_PROBLEM"), EnumMember(Value = "MECHANICAL_PROBLEM")]
        MECHANICALPROBLEM =  7,

        /// <summary>
        /// Enum SOFTWAREPROBLEM for "SOFTWARE_PROBLEM"
        /// </summary>
        [Description("SOFTWARE_PROBLEM"), EnumMember(Value = "SOFTWARE_PROBLEM")]
        SOFTWAREPROBLEM =  8,

        /// <summary>
        /// Enum ENVIRONMENTAL for "ENVIRONMENTAL"
        /// </summary>
        [Description("ENVIRONMENTAL"), EnumMember(Value = "ENVIRONMENTAL")]
        ENVIRONMENTAL =  9,

        /// <summary>
        /// Enum SECURITY for "SECURITY"
        /// </summary>
        [Description("SECURITY"), EnumMember(Value = "SECURITY")]
        SECURITY =  10,

        /// <summary>
        /// Enum TRAFFIC for "TRAFFIC"
        /// </summary>
        [Description("TRAFFIC"), EnumMember(Value = "TRAFFIC")]
        TRAFFIC =  11,

        /// <summary>
        /// Enum LOSTUSS for "LOST_USS"
        /// </summary>
        [Description("LOST_USS"), EnumMember(Value = "LOST_USS")]
        LOSTUSS =  12,

        /// <summary>
        /// Enum OTHER for "OTHER"
        /// </summary>
        [Description("OTHER"), EnumMember(Value = "OTHER")]
        OTHER =  13,

        /// <summary>
        /// Enum ANY for "ANY"
        /// </summary>
        [Description("ANY"), EnumMember(Value = "ANY")]
        ANY =  14
    }

    /// <summary>
    /// The type of contingency response. 1. LANDING   The operation will be landing by targeting the contingency_point. 2. LOITERING   The operation will loiter at the contingency_point at the specified altitude with the noted loiter_radius_ft. 3. RETURN_TO_BASE   The operation will return to base as specified by the contingency_point. The USS may issue an update to the operation plan to support this maneuver.
    /// </summary>
    /// <value>The type of contingency response. 1. LANDING   The operation will be landing by targeting the contingency_point. 2. LOITERING   The operation will loiter at the contingency_point at the specified altitude with the noted loiter_radius_ft. 3. RETURN_TO_BASE   The operation will return to base as specified by the contingency_point. The USS may issue an update to the operation plan to support this maneuver.</value>
    public enum ContingencyResponseEnum
    {
        /// <summary>
        /// Enum LANDING for "LANDING"
        /// </summary>
        [Description("LANDING"), EnumMember(Value = "LANDING")]
        LANDING =  1,

        /// <summary>
        /// Enum LOITERING for "LOITERING"
        /// </summary>
        [Description("LOITERING"), EnumMember(Value = "LOITERING")]
        LOITERING =  2,

        /// <summary>
        /// Enum RETURNTOBASE for "RETURN_TO_BASE"
        /// </summary>
        [Description("RETURN_TO_BASE"), EnumMember(Value = "RETURN_TO_BASE")]
        RETURN_TO_BASE =  3,

        /// <summary>
        /// Enum OTHER for "OTHER"
        /// </summary>
        [Description("OTHER"), EnumMember(Value = "OTHER")]
        OTHER =  4
    }

    /// <summary>
        ///Description of the contingency location. PREPROGRAMMED: location that is determined prior to launch and programmed onto the UA OPERATOR_UPDATED: location that is (or will be) updated during
        ///operation by operator (e.g., sent to UA) UA_IDENTIFIED: location that is identified to be safe to land by the UA itself OTHER: location does not fit any of the defined categories
    /// </summary>
    /// <value>
        ///Description of the contingency location. PREPROGRAMMED: location that is determined prior to launch and programmed onto the UA OPERATOR_UPDATED: location that is (or will be) updated during
        ///operation by operator (e.g., sent to UA) UA_IDENTIFIED: location that is identified to be safe to land by the UA itself OTHER: location does not fit any of the defined categories
    /// </value>
    public enum ContingencyLocationDescription
    {
        [Description("PREPROGRAMMED"), EnumMember(Value = "PREPROGRAMMED")]
        PREPROGRAMMED = 1,

        [Description("OPERATOR_UPDATED"), EnumMember(Value = "OPERATOR_UPDATED")]
        OPERATOR_UPDATED = 2,

        [Description("UA_IDENTIFIED"), EnumMember(Value = "UA_IDENTIFIED")]
        UA_IDENTIFIED = 3,

        [Description("OTHER"), EnumMember(Value = "OTHER")]
        OTHER = 4
    }
}