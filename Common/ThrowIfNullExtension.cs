﻿using System;

namespace AnraUssServices.Common
{
    public static class CommonExtensions
    {
        internal static void ThrowIfNull<T>(this T o, string paramName) where T : class
        {
            if (o == null)
                throw new ArgumentNullException(paramName);
        }
    }
}