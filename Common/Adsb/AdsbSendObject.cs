﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Adsb
{
    public class AdsbSendObject
    {
        public List<AdsbJsonStruct> Detectpackets { get; set; }
    }
}
