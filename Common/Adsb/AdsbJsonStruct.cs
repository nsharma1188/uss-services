﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Adsb
{
    public class AdsbJsonStruct
    {
        public Guid detect_id { get; set; }
        public Guid user_id { get; set; }
        public string uuid { get; set; }
        public double? lat { get; set; }
        public double? lng { get; set; }
        public double alt { get; set; }
        public int heading { get; set; }
        public double bearing { get; set; }
        public DateTime time_stamp { get; set; }
        public string source { get; set; }          // RADAR|DSRC|RF|ADSB
        public string vendor { get; set; }          // ECHODYNE|COHDA|CACI|ETC|ONESKY
        public string source_class { get; set; }    // GROUND_FIXED|GROUND_MOBILE|AIRBORNE|UNKNOWN
        public string type { get; set; }            // DETECT|TRACK
        public string metadata1 { get; set; }
        public string metadata2 { get; set; }
        public string raw_data { get; set; }
    }
}
