﻿namespace AnraUssServices.Common.FimsAuth
{
    internal class UtmScopes
    {
        public const string UTM_NASA_GOV_WRITE_OPERATION = @"utm.nasa.gov_write.operation";
        public const string UTM_NASA_GOV_WRITE_MESSAGE = @"utm.nasa.gov_write.message";
        public const string UTM_NASA_GOV_READ_CONSTRAINT = @"utm.nasa.gov_read.constraint";
        public const string UTM_NASA_GOV_READ_UVIN = @"utm.nasa.gov_read.uvin";
        public const string UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT = @"utm.nasa.gov_write.conflictmanagement";
        public const string UTM_NASA_GOV_READ_SDSP = @"utm.nasa.gov_read.sdsp";
        public const string UTM_NASA_GOV_WRITE_CONSTRAINT = @"utm.nasa.gov_write.constraint";
        public const string UTM_NASA_GOV_READ_PUBLICSAFETY = @"utm.nasa.gov_read.publicsafety";
        public const string UTM_NASA_GOV_WRITE_PUBLICSAFETY = @"utm.nasa.gov_write.publicsafety";
    }
}