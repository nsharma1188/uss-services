﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.FimsAuth
{
    public class HasScope : AuthorizationHandler<HasScope>, IAuthorizationRequirement
    {
        private readonly string issuer;
        private readonly string scopes;

        public HasScope(string scopes, string issuer)
        {
            this.scopes = scopes;
            this.issuer = issuer;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasScope requirement)
        {
            // If user does not have the scope claim, get out of here
            if (!context.User.HasClaim(c => c.Type == "scope"))
                return Task.CompletedTask;

            //Check if the iss claims is valid 
            if (context.User.HasClaim(c => c.Type == "iss"))
            {
                var tokenIssuer = context.User.Claims.Where(c => c.Type == "iss").FirstOrDefault().Value;
                if(!tokenIssuer.Equals(issuer))
                {
                    return Task.CompletedTask;
                }
            }
            else
            {
                return Task.CompletedTask;
            }

            // Split the scopes string into an array
            var contextScopes = context.User.Claims.Where(c => c.Type == "scope").FirstOrDefault().Value.Split(" ");

            // Succeed if the scope array contains the required scope
            if (contextScopes.Contains(scopes))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
