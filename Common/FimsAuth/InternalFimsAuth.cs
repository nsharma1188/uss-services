﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.FimsAuth
{
	public class InternalFimsAuth
	{
		public string access_token { get; set; }
		public int expires_in { get; set; }
		public string iss { get; set; }
		public string scope { get; set; }
		public string sub { get; set; }
		public string token_type { get; set; }
	}
}
