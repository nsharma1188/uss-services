﻿using AnraUssServices.Common.FimsAuth;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Sentinel.OAuth.Client;
using Sentinel.OAuth.Core.Models.OAuth.Http;
using System;

namespace AnraUssServices.Common
{
    public class FimsAuthenticationClient
    {
        private readonly string[] scopes;
        private readonly ILogger<string> logger;
        private readonly AnraHttpClient anraHttpClient;
        private readonly AnraConfiguration anraConfig;
        private DateTime? expiration;
        private AccessTokenResponse tokenResponse;
        private string message;

        public FimsAuthenticationClient(AnraConfiguration anraConfig, ILogger<string> logger, string[] scopes)
        {
            this.anraConfig = anraConfig;
            this.logger = logger;
            this.scopes = scopes;
            this.GetAuthenticationToken();
        }

        public string Token
        {
            get
            {
                if (!anraConfig.IsUtmEnabled)
                    return null;

                if (!expiration.HasValue || expiration < DateTime.UtcNow.AddMinutes(2))
                {
                    GetAuthenticationToken();
                }

                return tokenResponse != null ? "Bearer " + tokenResponse.AccessToken : message; 
            }
        }
        /// <summary>
        /// Returns the authentication token based on the flag InternalFIMS
        /// If the InternalFIMS is true it uses our inernal FIMS server else it uses the NASA server
        /// </summary>
        private void GetAuthenticationToken()
        {
            try
            {
				if (anraConfig.InternalFIMS)
				{
					var url = anraConfig.FimsBaseUrl + "oauth/token?grant_type=client_credentials&scope=" + scopes[0].ToString();
					var requestBody = "";

					var client = new System.Net.Http.HttpClient();
					client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(
					System.Text.ASCIIEncoding.ASCII.GetBytes(
					string.Format("{0}:{1}", anraConfig.ClientId, anraConfig.Secret))));

                    var response = client.PostAsync(url, new System.Net.Http.StringContent(requestBody)).Result;
					var result = JsonConvert.DeserializeObject<InternalFimsAuth>(response.Content.ReadAsStringAsync().Result);

                    tokenResponse = new AccessTokenResponse()
					{
						AccessToken = result.access_token,
						TokenType = "Bearer"
					};

					expiration = DateTime.UtcNow.AddSeconds(result.expires_in);
				}
				else
				{

					var authPoints = new Sentinel.OAuth.Client.Models.AuthenticationEndpoints
					{
						AuthorizationCodeEndpointUrl = anraConfig.FimsBaseUrl + "/fimsAuthServer/oauth/authorize",
						TokenEndpointUrl = anraConfig.FimsBaseUrl + "/fimsAuthServer/oauth/token"
					};

					var s = new SentinelClientSettings(new Uri(anraConfig.FimsBaseUrl), anraConfig.ClientId, anraConfig.Secret, "", new TimeSpan(2, 0, 0), authPoints);

					using (var c = new SentinelOAuthClient(s))
					{
						var response = c.Authenticate(scopes);
						tokenResponse = response.Result;
						expiration = DateTime.UtcNow.AddSeconds(tokenResponse.ExpiresIn);
					}
				}
            }
            catch (Exception ex)
            {
                message = ex.Message;
                logger.LogError("FimsAuthentication {0}", ex);
            }
        }
    }
}