﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace AnraUssServices.Common.FimsAuth
{
    public class FimsTokenProvider
    {
        private readonly AnraConfiguration anraConfig;
        private readonly ILogger<string> logger;
        private Dictionary<string, FimsAuthenticationClient> Tokens;
        public static string ValidIssuer { get; internal set; }

        public FimsTokenProvider(AnraConfiguration anraConfig, ILogger<string> logger)
        {
            this.logger = logger;
            this.anraConfig = anraConfig;
            ValidIssuer = anraConfig.FimsBaseUrl + "/fimsAuthServer";
            Tokens = new Dictionary<string, FimsAuthenticationClient>();
        }

        /// <summary>
        /// Based on the scope it calls the authentication server to get the token for the respective scope
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public string GetToken(string scope)
        {
            if (!Tokens.ContainsKey(scope))
            {
                switch (scope)
                {
                    case UtmScopes.UTM_NASA_GOV_READ_CONSTRAINT:
                        var token1 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_READ_CONSTRAINT });
                        Tokens[UtmScopes.UTM_NASA_GOV_READ_CONSTRAINT] = token1;
                        break;

                    case UtmScopes.UTM_NASA_GOV_READ_UVIN:
                        var token2 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_READ_UVIN });
                        Tokens[UtmScopes.UTM_NASA_GOV_READ_UVIN] = token2;
                        break;

                    case UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT:
                        var token3 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT });
                        Tokens[UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT] = token3;
                        break;

                    case UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE:
                        var token4 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE });
                        Tokens[UtmScopes.UTM_NASA_GOV_WRITE_MESSAGE] = token4;
                        break;

                    case UtmScopes.UTM_NASA_GOV_WRITE_OPERATION:
                        var token5 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_WRITE_OPERATION });
                        Tokens[UtmScopes.UTM_NASA_GOV_WRITE_OPERATION] = token5;
                        break;

                    case UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT:
                        var token6 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT });
                        Tokens[UtmScopes.UTM_NASA_GOV_WRITE_CONSTRAINT] = token6;
                        break;

					case UtmScopes.UTM_NASA_GOV_WRITE_PUBLICSAFETY:
						var token7 = new FimsAuthenticationClient(anraConfig, logger, new string[] { UtmScopes.UTM_NASA_GOV_WRITE_PUBLICSAFETY });
						Tokens[UtmScopes.UTM_NASA_GOV_WRITE_PUBLICSAFETY] = token7;
						break;

					default: return null;
                }
            }

            return Tokens[scope].Token;
        }
    }
}