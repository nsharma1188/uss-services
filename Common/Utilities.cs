﻿using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace AnraUssServices.Common
{
    public class Utilities
    {
        static public string EncodeTo64(string toEncode)
        {
            var toEncodeAsBytes = System.Text.Encoding.ASCII.GetBytes(toEncode);
            var returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public List<Tuple<string, string>> GetMultiSortExpression(string sort, bool? sortIncreasing)
        {
            var sortExpressions = new List<Tuple<string, string>>();

            if (!string.IsNullOrEmpty(sort))
            {
                var sortOrder = sortIncreasing.HasValue && sortIncreasing.Value ? "asc" : "desc";
                foreach (var fieldName in sort.Split(','))
                {
                    sortExpressions.Add(new Tuple<string, string>(fieldName, sortOrder));
                }
            }

            return sortExpressions;
        }

        static public string GetUtmFormatDateTimeString(DateTime? datetime = null)
        {
            if (datetime.HasValue)
            {
                return datetime.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            }
            else
            {
                return DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            }
        }

        static public string GetBaseUrl(string url)
        {
            var uri = new Uri(url);
            return uri.GetLeftPart(UriPartial.Authority);
        }

        static public object CreateShappedObject(object obj, List<string> lstFields)
        {
            if (!lstFields.Any())
            {
                return obj;
            }
            else
            {
                var objectToReturn = new ExpandoObject();
                foreach (var field in lstFields)
                {
                    var fieldValue = obj.GetType()
                        .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                        .GetValue(obj, null);

                    ((IDictionary<string, object>)objectToReturn).Add(field, fieldValue);
                }

                return objectToReturn;
            }
        }

        /// <summary>
        /// Convert Longitude and Latitude to UTM Coordinates
        /// </summary>
        /// <param name="lon">todo: describe lon parameter on WGS84toUTM</param>
        /// <param name="lat">todo: describe lat parameter on WGS84toUTM</param>
        /// <returns>UTM Coordinates</returns>
        static private double?[] WGS84toUTM(double? lon, double? lat)
        {
            var x = lon * 20037508.34 / 180;
            double? y = Math.Log(Math.Tan((90 + (lat.HasValue ? lat.Value : 0)) * Math.PI / 360)) / (Math.PI / 180);
            y = y * 20037508.34 / 180;
            return new double?[] { x, y };
        }

        /// <summary>
        /// Convert UTM Coordinates to Geo LOcations(Longitude and Latitude)
        /// </summary>
        /// <param name="x">todo: describe x parameter on UTMtoWGS84Mercator</param>
        /// <param name="y">todo: describe y parameter on UTMtoWGS84Mercator</param>
        /// <returns>UTM Coordinates</returns>
        static private double?[] UTMtoWGS84Mercator(double? x, double? y)
        {
            var lon = (x / 20037508.34) * 180;
            var lat = (y / 20037508.34) * 180;

            lat = 180 / Math.PI * (2 * Math.Atan(Math.Exp((lat.HasValue ? lat.Value : 0) * Math.PI / 180)) - Math.PI / 2);
            return new double?[] { lon, lat };
        }

        /// <summary>
        /// Get Flight Geography With Non-Conformance and Protected Geography
        /// </summary>
        /// <param name="flightGeography">todo: describe flightGeography parameter on GetFlightGeographyWithOffset</param>
        /// <returns>Flight Geography With Non-Conformance and Protected Geography</returns>
        static public List<Utm.Models.Polygon> GetFlightGeographyWithOffset(string flightGeography)
        {
            var lstFlightGeoWithOffset = new List<Utm.Models.Polygon>();

            var flightGeo = JsonConvert.DeserializeObject<Utm.Models.Polygon>(flightGeography);

            //Get UTM Coordinates For Requested Flight Geography
            var nonConformanceUTMs = GetUTMCoordinates(flightGeo.Coordinates[0]);

            // START - Creation Of Non Conforming Flight Geography

            //Get LatLng Coordinates of Non Conforming Area for requested Flight Geography
            var nonConformancePolyCords = GetLatLngCoordinates(nonConformanceUTMs);

            //Add Non Conformance Area to Requested Flight Geography
            var nonConformancePolygon = new Utm.Models.Polygon
            {
                Type = "Polygon",
                Coordinates = nonConformancePolyCords
            };

            lstFlightGeoWithOffset.Add(nonConformancePolygon);

            // END - Creation Of Non Conforming Flight Geography

            // START - Creation Of Protected Flight Geography

            //Get UTM Coordinates For Protected Flight Geography (We will use non-conformance flight geography as input for this calculation)
            var protectedUTMs = GetUTMCoordinates(nonConformancePolyCords[0]);

            //Get LatLng Coordinates of Non Conforming Area for requested Flight Geography
            var protectedPolyCords = GetLatLngCoordinates(protectedUTMs);

            //Add Protected Area to Requested Flight Geography
            var protectedPolygon = new Utm.Models.Polygon
            {
                Type = "Polygon",
                Coordinates = protectedPolyCords
            };

            lstFlightGeoWithOffset.Add(protectedPolygon);

            // END - Creation Of Protected Flight Geography

            return lstFlightGeoWithOffset;
        }

        /// <summary>
        /// Get Flight Geography With Non-Conformance and Protected Geography
        /// </summary>
        /// <param name="polygonLatLngs">todo: describe polygonLatLngs parameter on GetUTMCoordinates</param>
        /// <returns>Flight Geography With Non-Conformance and Protected Geography</returns>
        static private List<GeoAPI.Geometries.Coordinate> GetUTMCoordinates(List<List<double?>> polygonLatLngs)
        {
            var geoInput = new List<GeoAPI.Geometries.Coordinate>();

            foreach (var latlng in polygonLatLngs)
            {
                var cord = new GeoAPI.Geometries.Coordinate();
                var point = WGS84toUTM(latlng[0], latlng[1]);
                cord.X = Convert.ToDouble(point[0]);
                cord.Y = Convert.ToDouble(point[1]);
                geoInput.Add(cord);
            }

            return geoInput;
        }

        /// <summary>
        /// Get Geo Locations(Longitude and Latitude) for input UTM Coordinates
        /// </summary>
        /// <param name="polygonCoordinates">todo: describe polygonCoordinates parameter on GetLatLngCoordinates</param>
        /// <returns>Latitude/Longitude Points</returns>
        static private List<List<List<double?>>> GetLatLngCoordinates(List<GeoAPI.Geometries.Coordinate> polygonCoordinates)
        {
            var gf = new GeometryFactory();
            var shell = gf.CreatePolygon(polygonCoordinates.ToArray());

            var polygon = shell.Buffer(10, (int)GeoAPI.Operation.Buffer.EndCapStyle.Flat);

            var polyLatLngs = new List<List<double?>>();

            var oCoordinates = polygon.Coordinates.ToArray();

            for (int i = 0; i < oCoordinates.Length; i++)
            {
                var oItem = polygon.Coordinates[i];
                var point = UTMtoWGS84Mercator(oItem.X, oItem.Y);
                var latlng = new List<double?>();
                latlng.Add(point[0]);
                latlng.Add(point[1]);
                polyLatLngs.Add(latlng);
            }

            var polyWithOffset = new List<List<List<double?>>>();
            polyWithOffset.Add(polyLatLngs);

            return polyWithOffset;
        }

        public static Microsoft.IdentityModel.Tokens.TokenValidationParameters GetTokenValidationParameters(string environment)
        {
            return new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidAudience = "AnraUsers",
                ValidateIssuer = true,
                ValidIssuer = "AnraTechnologies",
                ValidateIssuerSigningKey = true,
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = SecurityKeysStore.GetAnraPublicKey(environment)
            };
        }

        public static bool ArePolygonsSame(Utm.Models.Polygon polygon1, Utm.Models.Polygon polygon2)
        {
            string polygon1Json = JsonConvert.SerializeObject(polygon1);
            string polygon2Json = JsonConvert.SerializeObject(polygon2);

            
            var geoJsonReader = new GeoJsonReader();
            string geoJsonPolygon1 = polygon1Json.Replace("'", "\"");
            var objPolygon1 = geoJsonReader.Read<Polygon>(geoJsonPolygon1);

            string geoJsonPolygon2 = polygon2Json.Replace("'", "\"");
            var objPolygon2 = geoJsonReader.Read<Polygon>(geoJsonPolygon2);



            return objPolygon1.EqualsExact(objPolygon2);
        }
    }
}