﻿using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System.Linq;

namespace AnraUssServices.Common.InterUss
{
	public class InterUssHelper
	{
		private readonly GridCell gridCell;
		private readonly SlippyTiles slippyTiles;
		private readonly AnraConfiguration configuration;

		public InterUssHelper(GridCell gridCell, SlippyTiles slippyTiles, AnraConfiguration configuration)
		{
			this.gridCell = gridCell;
			this.slippyTiles = slippyTiles;
			this.configuration = configuration;
		}

		public GridCellOperatorResponse GetOperationUssOperator(Operation operation)
		{
			var slippyResponse = slippyTiles.GetSlippyTiles(configuration.DefaultZoomLevel, GetCSVCoordinateList(operation));

			if (slippyResponse.Data.GridCells.Any())
			{
				var grids = slippyResponse.Data.GridCells;

				if (grids.Any())
				{
					var gridCellOperators = gridCell.GetGridCellOperators(JsonConvert.SerializeObject(grids), false);

					if (gridCellOperators.Any())
					{
						return gridCellOperators.Where(x => x.Operations.Any(op => op.Gufi.Equals(operation.Gufi))).FirstOrDefault();
					}
				}
			}

			return null;
		}

		private string GetCSVCoordinateList(Operation operation)
		{
			string csvList = string.Empty;

			operation.OperationVolumes.ForEach(opv =>
			{
				opv.OperationGeography.Coordinates[0].ForEach(x =>
				{
					csvList = csvList + string.Format("{0},{1},", x[1], x[0]);
				});
			});

			return csvList.Substring(0, csvList.Length - 1);
		}
	}
}