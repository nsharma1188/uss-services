﻿using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AnraUssServices.Common.InterUss
{
    public class SlippyTiles
    {
        private readonly AnraConfiguration configuration;
        private readonly InterUssApi interUssApi;
        private readonly IRepository<Models.Operation> operationRepository;

        public SlippyTiles(InterUssApi interUssApi,
            AnraConfiguration configuration,
            IRepository<Models.Operation> operationRepository
        )
        {
            this.interUssApi = interUssApi;
            this.operationRepository = operationRepository;
            this.configuration = configuration;
        }

        public JSendSlippyResponse GetSlippyTiles(int zoom, string coords, string coordtype = "polygon")
        {
            return interUssApi.GetSlippyTilesInfo(zoom, coords, coordtype);
        }

        public ObjectResult UpdateTilesInfo(Guid gufi, List<JSendSlippyResponseDataGridCells> gridData)
        {
            var model = operationRepository.Find(gufi);

            if (model == null)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }

            var lastState = model.State;

            model.SlippyTileData = JsonConvert.SerializeObject(gridData);
            operationRepository.Update(model);

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation - {model.Gufi} tiles info updated."});
        }

        public JSendSlippyResponseDataGridCells[] GetSlippyTileInfo(Polygon polygon)
        {
            List<JSendSlippyResponseDataGridCells> response = new List<JSendSlippyResponseDataGridCells>();

            if (polygon != null)
            {
                string coordList = GetCSVCoordinateList(polygon);

                //Commenting Gurvinder Code as It is not returning correct tiles details
                
                //foreach (var coordinates in polygon.Coordinates[0])
                //{                    
                //    var lat = coordinates[1].Value;
                //    var lng = coordinates[0].Value;

                //    var pt = WorldToTilePos(lng, lat, configuration.DefaultZoomLevel);
                //    response.Add(new JSendSlippyResponseDataGridCells { X = (int) Math.Floor(pt.X), Y = (int)Math.Floor(pt.Y), Zoom = configuration.DefaultZoomLevel });
                //}
               response = GetSlippyTiles(configuration.DefaultZoomLevel, coordList).Data.GridCells;
            }

            //Remove Duplicate Grid Cells
            //response = response.GroupBy(x => x.X).Select(z => z.First()).ToList();                        

            return response.ToArray();
        }

        private static PointF WorldToTilePos(double lon, double lat, int zoom)
        {
            PointF p = new System.Drawing.Point();
            p.X = (float)((lon + 180.0) / 360.0 * (1 << zoom));
            p.Y = (float)((1.0 - Math.Log(Math.Tan(lat * Math.PI / 180.0) +
                1.0 / Math.Cos(lat * Math.PI / 180.0)) / Math.PI) / 2.0 * (1 << zoom));

            return p;
        }

        private static PointF TileToWorldPos(double tile_x, double tile_y, int zoom)
        {
            PointF p = new System.Drawing.Point();
            var n = Math.PI - ((2.0 * Math.PI * tile_y) / Math.Pow(2.0, zoom));

            p.X = (float)((tile_x / Math.Pow(2.0, zoom) * 360.0) - 180.0);
            p.Y = (float)(180.0 / Math.PI * Math.Atan(Math.Sinh(n)));

            return p;
        }

        public string GetCSVCoordinateList(Polygon polygon)
        {
            string csvList = string.Empty;

            polygon.Coordinates[0].ForEach(x =>
            {
                csvList = csvList + string.Format("{0},{1},", x[1], x[0]);
            });

            return csvList.Substring(0, csvList.Length - 1);
        }
    }
}