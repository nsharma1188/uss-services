﻿using AnraUssServices.Common.Security;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace AnraUssServices.Common.InterUss
{
    public class GridCell
    {
        private readonly InterUssApi interUssApi;
        private IRepository<UtmInstance> utmInstanceRepository;
        private IRepository<Models.Operation> operationRepository;
        private IRepository<OperationConflict> operationConflictRepository;
        private readonly JwsHelper jwsHelper;
        private DatabaseFunctions dbFunctions;
        private readonly ILogger<GridCell> logger;
        private AnraConfiguration _config;

        public GridCell(
            InterUssApi interUssApi,
            JwsHelper jwsHelper,
            DatabaseFunctions dbFunctions,
            ILogger<GridCell> logger,
            AnraConfiguration configuration,
            IRepository<Models.Operation> operationRepository,
            IRepository<OperationConflict> operationConflictRepository,
            IRepository<UtmInstance> utmInstanceRepository)
        {
            this.interUssApi = interUssApi;
            this.jwsHelper = jwsHelper;
            this.dbFunctions = dbFunctions;
            this.logger = logger;
            this.utmInstanceRepository = utmInstanceRepository;
            this.operationRepository = operationRepository;
            this.operationConflictRepository = operationConflictRepository;
            _config = configuration;
        }

        public JSendGridCellMetadataResponse GridCellOperators(int zoom, int x, int y)
        {
            return interUssApi.GetGridCellOperatorAsync(zoom, x, y);
        }

        public Dictionary<JSendSlippyResponseDataGridCells, JSendGridCellMetadataResponse> GetGridCellDetails(List<JSendSlippyResponseDataGridCells> grids)
        {
            var gridCells = new Dictionary<JSendSlippyResponseDataGridCells, JSendGridCellMetadataResponse>();

            foreach (var grid in grids)
            {
                var response = GridCellOperators(grid.Zoom, grid.X, grid.Y);
                gridCells.Add(grid, response);
            }

            return gridCells;
        }

        public void DeleteGridCellOperation(string gridCellsData, Guid gufi)
        {
            var gridCells = JsonConvert.DeserializeObject<List<JSendSlippyResponseDataGridCells>>(gridCellsData);

            gridCells.ForEach(cell =>
            {
                interUssApi.DeleteGridCellOperation(cell.Zoom, cell.X, cell.Y, gufi);
            });
        }

        public void DeleteGridCellOperator(UtmInstance utmInstance)
        {
            var gridCellsData = JsonConvert.DeserializeObject<IList<JSendSlippyResponseDataGridCells>>(utmInstance.GridCells);

            foreach (var gridCell in gridCellsData)
            {
                interUssApi.DeleteGridCellOperator(gridCell.Zoom, gridCell.X, gridCell.Y);
            }
        }

        public List<GridCellOperatorResponse> GetGridCellOperators(string gridData,bool excludeself = true)
        {
            var grids = !string.IsNullOrEmpty(gridData) ? JsonConvert.DeserializeObject<List<JSendSlippyResponseDataGridCells>>(gridData) : new List<JSendSlippyResponseDataGridCells>();

            var Operators = new List<GridCellOperatorResponse>();

            if (grids.Any())
            {
                grids.ForEach(grid =>
                {
                    var response = interUssApi.GetGridCellOperatorAsync(grid.Zoom, grid.X, grid.Y);
                    Operators.AddRange(response.Data.Operators);
                });
            }

            //Filter operators
            var operators = excludeself ? Operators.Where(x => !x.Uss.Equals(_config.ClientId)).ToList() : Operators.ToList();

            //Filter Out duplicate operator records in case of multiple grid response.
            var distinctOperators = excludeself ? operators.GroupBy(x => x.Uss).Select(x => x.FirstOrDefault()).ToList() : operators;

            #if DEBUG
                logger.LogDebug("GetGridCellOperators :: gridData = {0} :: All Operators = {1} :: Distinct Operators = {2}", gridData, operators.Count, distinctOperators.Count);
            #endif

            return distinctOperators;
        }

        public List<Guid> GetSelfOperations(List<JSendSlippyResponseDataGridCells> gridCells)
        {
            var operations = new List<Guid>();

            List<GridCellOperatorResponse> Operators = new List<GridCellOperatorResponse>();

            if (gridCells.Any())
            {
                gridCells.ForEach(grid =>
                {
                    var response = interUssApi.GetGridCellOperatorAsync(grid.Zoom, grid.X, grid.Y);
                    Operators.AddRange(response.Data.Operators);
                });
            }

            //Filter Anra operator from list of Operators
            var operators = Operators.Distinct().Where(x => x.Uss.Equals(_config.ClientId)).ToList();

            //Prepare List of Anra Operations
            operators.ForEach(opr =>
            {
                opr.Operations.ForEach(op =>
                {
                    operations.Add(op.Gufi);
                });
            });

            return operations;
        }

        public bool IsValidOperation(Guid gufi, List<JSendSlippyResponseDataGridCells> gridCells)
        {
            var operations = GetSelfOperations(gridCells);

            return operations.Contains(gufi);
        }

        public JSendGridCellMetadataResponse PutOperationToGridCell(Utm.Models.Operation operation, JSendSlippyResponseDataGridCells gridCell, long? syncToken)
        {
            var operationRequest = new GridCellOperationRequest
            {
                Gufi = operation.Gufi,
                OperationSignature = jwsHelper.GetJwsHeader(string.Empty),
                EffectiveTimeBegin = operation.OperationVolumes[0].EffectiveTimeBegin.Value,
                EffectiveTimeEnd = operation.OperationVolumes[0].EffectiveTimeEnd.Value
            };

            return interUssApi.PutGridCellOperation(gridCell.Zoom, gridCell.X, gridCell.Y, operation.Gufi.ToString(), operationRequest, syncToken.Value);
        }

        public List<JSendGridCellMetadataResponse> PutGridCellOperator(UtmInstance utmInstance)
        {
            var gridCellOperator = new GridCellOperatorRequest
            {
                UssBaseurl = utmInstance.UssBaseCallbackUrl,
                MinimumOperationTimestamp = utmInstance.TimeAvailableBegin.ToString(),
                MaximumOperationTimestamp = utmInstance.TimeAvailableEnd.ToString(),
                AnnouncementLevel = AnnouncementLevelEnum.ALL
            };

            var gridCellsData = JsonConvert.DeserializeObject<IList<JSendSlippyResponseDataGridCells>>(utmInstance.GridCells);

            var responses = new List<JSendGridCellMetadataResponse>();
            foreach (var gridCell in gridCellsData)
            {
                var gridCellMetadataResponse = interUssApi.GetGridCellOperatorAsync(gridCell.Zoom, gridCell.X, gridCell.Y);

                
                    var response = interUssApi.PutGridCellOperator(gridCell.Zoom, gridCell.X, gridCell.Y, gridCellOperator, gridCellMetadataResponse.SyncToken);
                    responses.Add(response);
                
            }
            return responses;
        }

        public ObjectResult WriteOperation(Utm.Models.Operation operation, Dictionary<JSendSlippyResponseDataGridCells, JSendGridCellMetadataResponse> grids)
        {
            #if DEBUG
                logger.LogInformation("ANRA:::GridCellLogic:: START HandleNoOperatorsWorkflow : Operation = {0}", operation.Gufi);
            #endif

            foreach (KeyValuePair<JSendSlippyResponseDataGridCells, JSendGridCellMetadataResponse> entry in grids)
            {
                var grid = entry.Key;
                var syncToken = entry.Value.SyncToken;

                var response = PutOperationToGridCell(operation, grid, syncToken);
                if (response.HttpStatusCode.Value != (int)HttpStatusCode.OK)
                {
                    #if DEBUG
                        logger.LogInformation("ANRA::GridCellLogic:: WriteOperation Failed: Operation = {0}, Response:{1}", operation.Gufi, response);
                    #endif

                    return new ObjectResult(response);
                }
            }

            #if DEBUG
                logger.LogInformation("ANRA:::GridCellLogic:: END HandleNoOperatorsWorkflow : Operation = {0}", operation.Gufi);
            #endif

            return new ObjectResult(new JSendGridCellMetadataResponse { HttpStatusCode = 200, Status = "OK" });
        }

        public List<KeyValuePair<Utm.Models.Operation, List<ConflictDetail>>> GetOperatorConflictingOperations(Utm.Models.Operation operation, List<GridCellOperatorResponse> operators)
        {
            var intersectingOperations = new List<KeyValuePair<Utm.Models.Operation, List<ConflictDetail>>>();

            if (operators.Any())
            {
                var externalOperations = new List<Utm.Models.Operation>();

                operators.ForEach(opr =>
                {
                    opr.Operations.ForEach(op =>
                    {
                        var externalOperation = interUssApi.GetOperatorOperationAsync(opr, op.Gufi.ToString());
                        if (externalOperation != null && externalOperation.Gufi != Guid.Empty)
                        {
                            externalOperations.Add(externalOperation);
                        }
                    });
                });

                //Check For 4D Intersections With Submitted Operation
                intersectingOperations = GisHelper.GetIntersectingOperations(operation, externalOperations);
            }

            return intersectingOperations;
        }

        public string[] GetMinMaxOperationTime(JSendSlippyResponseDataGridCells gridData, Utm.Models.Operation operation)
        {
            string[] minmaxDates = new string[2];

            var existingOps = dbFunctions.GetOperationsByGridData(gridData.Zoom, gridData.X, gridData.Y).ToList();

            List<DateTime> listDates = new List<DateTime>();

            if (existingOps.Any())
            {
                existingOps.ForEach(x =>
                {
                    x.OperationVolumes.ToList().ForEach(opv =>
                    {
                        listDates.Add(opv.EffectiveTimeBegin);
                        listDates.Add(opv.EffectiveTimeEnd);
                    });
                });

                //Add Planned Operation dates
                operation.OperationVolumes.ToList().ForEach(opv =>
                {
                    listDates.Add(opv.EffectiveTimeBegin.Value);
                    listDates.Add(opv.EffectiveTimeEnd.Value);
                });

                minmaxDates[0] = listDates.Min(p => p).ToString();
                minmaxDates[1] = listDates.Max(p => p).ToString();
            }
            else
            {
                //Add Planned Operation dates
                operation.OperationVolumes.ToList().ForEach(opv =>
                {
                    listDates.Add(opv.EffectiveTimeBegin.Value);
                    listDates.Add(opv.EffectiveTimeEnd.Value);
                });

                minmaxDates[0] = listDates.Min(p => p).ToString();
                minmaxDates[1] = listDates.Max(p => p).ToString();
            }

            return minmaxDates;
        }

        public JSendGridCellMetadataResponse DeleteUVR(int zoom, UASVolumeReservation uvr)
        {
            return interUssApi.DeleteUVR(zoom, uvr);
        }

        public JSendGridCellMetadataResponse WriteUVR(int zoom, UASVolumeReservation uvr)
        {
            return interUssApi.PutUVR(zoom, uvr);
        }
    }
}