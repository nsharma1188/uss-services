﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.Operations;
using System;

namespace AnraUssServices.Common
{
    public class OperationMqttNotification
    {
        private readonly AnraConfiguration configuration;
        private readonly AnraMqttClient mqttClient;

        public OperationMqttNotification(AnraMqttClient mqttClient, AnraConfiguration configuration, ViewerMqttNotification viewerMqttNotification)
        {
            this.mqttClient = mqttClient;
            this.configuration = configuration;
            ViewerMqttNotification = viewerMqttNotification;
        }

        private ViewerMqttNotification ViewerMqttNotification { get; }

        public void Notify(Operation operation, string message)
        {
            var notification = new OperationNotification
            {
                Gufi = operation.Gufi,
                Message = message,
                Status = operation.State.ToString(),
                Timestamp = DateTime.UtcNow
            };

            //Need to find a way to handel this on the new mqtt implementation (mobile app)
            mqttClient.PublishTopic(string.Format(MqttTopics.NotifyTopic, operation.UserId), notification);

            //Commenting for now. This is used by Web UI for state related update
            //ViewerMqttNotification.NotifyOperationStatus(notification);
        }

        /// <summary>
        /// Send Operation related notification to the client
        /// </summary>
        /// <param name="notificationMessage"></param>
        /// <param name="messageType"></param>
        /// <param name="gufi"></param>
        public void NotifyOperation(object notificationMessage, string messageType, string gufi)
        {
            var topic = string.Format(MqttTopics.OperationNotification, gufi);

            var mqttMessagePayload = new MqttMessagePayload()
            {
                Type = messageType,
                Message = notificationMessage,
                Topic = $"{configuration.Product}/{configuration.Environment}/{topic}"
            };

            mqttClient.PublishTopic(topic, mqttMessagePayload);
        }

    }
}