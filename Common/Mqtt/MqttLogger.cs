﻿using System;

namespace AnraUssServices.Common.Mqtt
{
    public class MqttLogger
    {
        private readonly AnraMqttClient client;

        public MqttLogger(AnraMqttClient client)
        {
            this.client = client;
        }

        public void Log(string source, string message)
        {
            var item = new
            {
                Resource = source,
                Message = message,
                TimeStamp = DateTime.Now
            };

            client.PublishTopic(MqttTopics.LogTopic, item);
        }

        public void Log(string source, object message)
        {
            var item = new
            {
                Resource = source,
                Message = message,
                TimeStamp = DateTime.Now
            };

            client.PublishTopic(MqttTopics.LogTopic, item);
        }

        public void Log(string topic, string source, object message)
        {
            var item = new
            {
                Resource = source,
                Message = message,
                TimeStamp = DateTime.Now
            };

            client.PublishTopic(topic, item);
        }
    }
}