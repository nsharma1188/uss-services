﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Mqtt
{
    public class MqttMessagePayload
    {
        /// <summary>
        /// The Mqtt Message type EMUM which define the type of message
        /// </summary>       
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }


        /// <summary>
        /// The actual message that are getting published through mqtt
        /// </summary>       
        [JsonProperty(PropertyName = "message")]
        public object Message { get; set; }

        /// <summary>
        /// Mqtt Topics that the message is being send
        /// </summary>
        [JsonProperty(PropertyName = "topic")]
        public object Topic { get; set; }
    }
}
