﻿using AnraUssServices.Common.Mqtt;
using System;

namespace AnraUssServices.Common
{
    public class GenericMqttNotification
    {
        private readonly ViewerMqttNotification viewerMqttNotification;
        private readonly AnraConfiguration configuration;
        private readonly AnraMqttClient mqttClient;

        public Guid MessageId { get; set; }

        public GenericNotificationType Type { get; set; }

        public string Message { get; set; }

        public GenericMqttNotification(AnraMqttClient mqttClient,
            AnraConfiguration configuration,
            ViewerMqttNotification viewerMqttNotification)
        {
            this.mqttClient = mqttClient;
            this.configuration = configuration;
            this.viewerMqttNotification = viewerMqttNotification;
        }

        public void Send(string userId, GenericNotificationType type, string message)
        {
            MessageId = Guid.NewGuid();
            Message = message;
            Type = type;
            mqttClient.PublishTopic(string.Format(MqttTopics.NotifyGenericTopic, userId), this);
        }
    }
}