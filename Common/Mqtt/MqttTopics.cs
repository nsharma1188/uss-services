﻿namespace AnraUssServices.Common.Mqtt
{
    public static class MqttTopics
    {
        /// <summary>
        /// Detects from different sources
        /// </summary>
        public const string MasterDetects = @"detection/inputs";

        ///// <summary>
        ///// Mqtt Queue for User Detects
        ///// Input Parameter: UserId
        ///// </summary>
        //public const string UserDetects = @"detects/{0}";

        ///// <summary>
        ///// Mqtt Queue for User Detects User Subscription Expiration
        ///// Input Parameter: UserId
        ///// </summary>
        //public const string UserDetectSubscriptionMessage = @"user/{0}/operation/{1}/subscription";

        /// <summary>
        /// Mqtt Queue for Operation Collision Warning
        /// Input Parameter: Gufi
        /// </summary>
        public const string CollisionWarning = @"operation/{0}/collision";

        /// <summary>
        /// OperationMqttNotification
        /// Input Parameter: UserId and Operation Gufi
        /// </summary>
        public const string NotifyTopic = @"{0}/operation";

        /// <summary>
        /// TelemetryConnection :: ReceiveAsync
        /// Input Parameter: Registration
        /// </summary>
        public const string TelementaryConnTopic = @"drone/{0}/telemetry";

        /// <summary>
        /// External Telemetry
        /// Input Parameter: External Operation Gufi
        /// </summary>
        public const string ExternalTelementary = @"operation/{0}/telemetry";

        ///// <summary>
        ///// OperationDeconflictionNotification :: PublishNotification
        ///// Input Parameter: Key and Gufi
        ///// </summary>
        //public const string OpeartionDeconflicationNotTopic = @"{0}/operation/{1}";

        /// <summary>
        /// UrepController :: AddUrepUsingPOST
        /// Input Parameter: Key and Gufi
        /// </summary>
        public const string UrepPostTopic = @"{0}/Urep/{1}";

        ///// <summary>
        ///// UssController :: CheckForPublicSafetyOperations
        ///// </summary>
        //public const string PublicSafetyAlertTopic = @"{0}/Alert";

        /// <summary>
        /// UssController :: PostUtmMessage
        /// </summary>
        public const string UtmMessagesTopic = @"UtmMessages";

        /// <summary>
        /// For logging purpose
        /// </summary>
        public const string LogTopic = @"log";

        ///// <summary>
        ///// For logging positions
        ///// </summary>
        //public const string LogTopicPositions = @"log/positions";

        ///// <summary>
        ///// NegotiationMqttNotification
        ///// Input Parameter: Operation Gufi
        ///// </summary>
        //public const string NegotiationTopic = @"negotiation/{0}";

        ///// <summary>
        ///// ANRA Viewer Operation State Notification
        ///// </summary>
        ///// Input Parameter: Operation Gufi
        //public const string NotifyOperationStateToViewer = @"operation/state";

        ///// <summary>
        ///// ANRA Viewer Delete Operation Notification
        ///// </summary>
        ///// Input Parameter: Operation Gufi
        //public const string NotifyDeleteOperationToViewer = @"operation/delete";

        /// <summary>
        /// ANRA Viewer MqttNotification
        /// </summary>
        /// Input Parameter: Operation Gufi
        public const string NotifyPositionToViewer = @"viewer/telemetry/{0}";

        /// <summary>
        /// ANRA Viewer MqttNotification
        /// </summary>
        public const string NotifyMessageToViewer = @"viewer/message";

        /// <summary>
        /// ANRA Viewer MqttNotification
        /// </summary>
        public const string NotifyLunCommunicationToViewer = @"viewer/lun";


        /// <summary>
        /// ANRA Generic MqttNotification
        /// </summary>
        public const string NotifyGenericTopic = @"notification/{0}/generic";

        /// <summary>
        /// ANRA Viewer Dynamic Restriction Notification
        /// </summary>
        /// Input Parameter: Operation Gufi
        public const string NotifyDynamicRestrictionToViewer = @"operation/{0}/uvr";


        /// <summary>
        /// ANRA Viewer Uss Exchange 
        /// </summary>
        public const string NotifyUssExchange = @"ussexchange";

        ///// <summary>
        ///// Mqtt Queue for Uvr
        ///// Input Parameter: MessageId
        ///// </summary>
        //public const string UvrTopic = @"uvr/{0}";

        /// <summary>
        /// Mqtt Queue for Uvr
        /// </summary>
        public const string UvrDelete = @"viewer/delete/uvr";

        /// <summary>
        /// Mqtt Queue for Notam
        /// </summary>
        public const string NotamDelete = @"viewer/delete/notam";

        /// <summary>
        /// ADSB DATA  
        /// </summary>
        public const string SendAdsbData = @"detection/opensky/{0}";

        /// <summary>
        /// Mqtt Queue for Notam        
        /// </summary>
        public const string NotamTopic = @"viewer/notam";

        /// <summary>
        /// Mqtt Queue for UVR        
        /// </summary>
        public const string NotifyUvrViewer = @"viewer/uvr";

        /// <summary>
        /// Landing Zone Evaluation
        /// Input Parameter: Operation Gufi
        /// </summary>
        public const string LandingZoneEvaluationTopic = @"operation/{0}/landingzone";



        //New MQTT Topics
        
        /// <summary>
        /// Mqtt Topics for the operation related notification
        /// Input Paramater : Operation Gufi
        /// </summary>
        public const string OperationNotification = @"operation/{0}";

        /// <summary>
        /// ANRA Viewer MqttNotification
        /// </summary>
        public const string NotifyOperationToViewer = @"operation/viewer";

        /// <summary>
        /// Mqtt topic for Notam related notification        
        /// </summary>
        public const string NotifyNotamToViewer = @"notam/viewer";

        /// <summary>
        /// Mqtt Queue for UVR        
        /// </summary>
        public const string NotifyUvrToViewer = @"uvr/viewer";
    }
}