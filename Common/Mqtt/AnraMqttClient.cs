﻿using AnraUssServices.BackgroundServices;
using AnraUssServices.Common;
using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Common.Telemetry;
using AnraUssServices.Data.Notifications;
using AnraUssServices.ViewModel.TelemetryMessages;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using static uPLibrary.Networking.M2Mqtt.MqttClient;

namespace AnraUssServices
{
    public class AnraMqttClient
    {
        private readonly ILogger<AnraMqttClient> _logger;
        private MqttClient _mqttClient;
        private readonly IServiceProvider serviceProvider;
        private IMediator mediator;
        private readonly AnraConfiguration _anraConfiguration;

        public AnraMqttClient(ILogger<AnraMqttClient> logger, IServiceProvider serviceProvider, AnraConfiguration anraConfiguration)
        {
            this.serviceProvider = serviceProvider;
            _anraConfiguration = anraConfiguration;
            _logger = logger;
        }

        public async Task InitAsync()
        {
            try
            {
                _mqttClient = new MqttClient(_anraConfiguration.MqttBrokerHost);
                _mqttClient.MqttMsgPublishReceived += new MqttMsgPublishEventHandler(MqttEventPublished);

                if (string.IsNullOrEmpty(_anraConfiguration.MqttBrokerUser))
                {
                    _mqttClient.Connect(Guid.NewGuid().ToString());
                }
                else
                {
                    string clientId = Guid.NewGuid().ToString();

                    #if DEBUG
                        _logger.LogInformation("ANRA USS :: MqttClient :: Init() : Client Id - " + clientId);
                    #endif

                    _mqttClient.Connect(clientId, _anraConfiguration.MqttBrokerUser, _anraConfiguration.MqttBrokerPwd);                    
                }

                //Kafka is handling the conformance check now
                //Subscribe(MqttTopics.ConformanceCheck);
            }
            catch (Exception ex)
            {
                _logger.LogError("ANRA USS :: MqttClient :: Init() " + ex.Message + ex.StackTrace);
            }
        }

        public void UnsubscribeTopic(string topic)
        {
            _mqttClient.Unsubscribe(new string[] { topic });
        }

        public void Subscribe(string topic)
        {
            try
            {
                var fullTopicName = $"{_anraConfiguration.Product}/{_anraConfiguration.Environment}/{topic}";
                _mqttClient.Subscribe(new string[] { fullTopicName }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            }
            catch (Exception ex)
            {
                _logger.LogError("ANRA USS :: MqttClient :: SubscribeTopic " + ex.Message + ex.StackTrace);
            }
        }

        public void PublishTopic(string topic, object obj)
        {
            try
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffZ",
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                    NullValueHandling = NullValueHandling.Ignore
                };

                serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());

                var msg = JsonConvert.SerializeObject(obj, Formatting.Indented, serializerSettings);
                topic = _anraConfiguration.Product + "/" + _anraConfiguration.Environment + "/" + topic;

                _mqttClient.Publish(topic, Encoding.UTF8.GetBytes(msg), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
            }
            catch (Exception ex)
            {
                _logger.LogError("ANRA USS :: MqttClient :: PublishTopic " + ex.Message + ex.StackTrace);
            }
        }

        private void MqttEventPublished(Object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                //var topic = _anraConfiguration.Product + "/" + _anraConfiguration.Environment + "/" + MqttTopics.ConformanceCheck;
                #if DEBUG
                _logger.LogInformation(string.Format("MqttClient :: MqttEventPublished : Topic - ", e.Topic));
                #endif

                //Currently this is getting handled by kafka consumer
                ////Checking for the Conformance
                //if(e.Topic.Contains(topic))
                //{
                //    var msg = Encoding.UTF8.GetString(e.Message);
                //    var confirmanceNotifaction = JsonConvert.DeserializeObject<ConfirmanceNotification>(msg);

                //    var _serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                //    using (_serviceScope)
                //    {
                //        var operationStatusLogic = _serviceScope.ServiceProvider.GetRequiredService<OperationStatusLogic>();

                //        switch (confirmanceNotifaction.State)
                //        {
                //            case OperationState.ACTIVATED:
                //                operationStatusLogic.ToActivateAsync(confirmanceNotifaction.Gufi).ConfigureAwait(false);
                //                break;

                //            case OperationState.NONCONFORMING:
                //                operationStatusLogic.ToNonConforming(confirmanceNotifaction.Gufi);
                //                break;

                //            case OperationState.ROGUE:
                //                operationStatusLogic.ToRogueAsync(confirmanceNotifaction.Gufi).ConfigureAwait(false);
                //                break;
                //            default:
                //                throw new Exception("Unexpected Case");
                //        }
                //    }
                //}
            }
            catch(Exception ex)
            {
                _logger.LogInformation(string.Format("MqttClient :: MqttEventPublished : Exception - ", ex.Message));
            }
        }
    }
}