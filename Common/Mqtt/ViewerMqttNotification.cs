﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using Jal.HttpClient.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class ViewerMqttNotification
    {
        private readonly AnraConfiguration configuration;
        private readonly AnraMqttClient mqttClient;

        private ILogger<string> Logger { get; }

        public ViewerMqttNotification(AnraMqttClient mqttClient, AnraConfiguration configuration, ILogger<string> logger)
        {
            this.mqttClient = mqttClient;
            this.configuration = configuration;
            Logger = logger;
        }

        public async Task NotifyOperation(Operation operation)
        {
            mqttClient.PublishTopic(MqttTopics.NotifyOperationToViewer, operation);
        }

        public async Task NotifyOperation(OperationViewModel operation)
        {
            mqttClient.PublishTopic(MqttTopics.NotifyOperationToViewer, operation);
        }

        public async Task NotifyUtmMessage(Utm.Models.UTMMessage uTMMessage)
        {
            mqttClient.PublishTopic(MqttTopics.NotifyMessageToViewer, uTMMessage);
        }

        public async Task NotifyPositionMessage(Position position)
        {
            mqttClient.PublishTopic(string.Format(MqttTopics.NotifyPositionToViewer, position.Gufi), position);
        }

        public async Task NotifyLunRequestToViewer(string ussName, string requestUrl, object obj, HttpResponse response)
        {
            try
            {
                if (response != null)
                {
                    int responseCode = (int)response.HttpStatusCode;
                    var responseMessage = response.Content != null ? response.Content.Read() : "";

                    mqttClient.PublishTopic(MqttTopics.NotifyLunCommunicationToViewer, new { USS = ussName, Url = requestUrl, Data = obj, ResponseCode = responseCode, ResponseMessage = responseMessage });
                }
                else
                {
                    mqttClient.PublishTopic(MqttTopics.NotifyLunCommunicationToViewer, new { USS = ussName, Url = requestUrl, Data = obj, ResponseCode = 0, ResponseMessage = "No Response" });
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("NotifyLunRequestToViewer :: ", ex);
            }
        }

        public async Task NotifyOperationStatus(OperationNotification notification)
        {
            //mqttClient.PublishTopic(MqttTopics.NotifyOperationStateToViewer, notification);
        }

        public async Task NotifyDynamicRestriction(ConstraintMessageNotification notification)
        {
            mqttClient.PublishTopic(string.Format(MqttTopics.NotifyDynamicRestrictionToViewer, notification.Gufi), notification);
        }

        public async Task NotifyDynamicRestriction(UASVolumeReservation uvr)
        {
            //Current UVR notification mqtt 
            //This need to be removed once we make the change on the mobile client
            mqttClient.PublishTopic(MqttTopics.NotifyUvrViewer, uvr);
        }

        /// <summary>
        /// Notify Operation, UVR and NOTAMS model to viewer for UI plotting
        /// </summary>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        /// <param name="viewerType"></param>
        public void NotificationToViewer(string messageType, object message, MqttViewerType viewerType)
        {
            string topic = string.Empty;

            if(viewerType == MqttViewerType.OPERATION)
            {
                topic = MqttTopics.NotifyOperationToViewer;
            }
            else if(viewerType == MqttViewerType.UVR)
            {
                topic = MqttTopics.NotifyUvrToViewer;
            }
            else
            {
                topic = MqttTopics.NotifyNotamToViewer;
            }

            //Creating mqtt message payload
            var mqttMessagePayload = new MqttMessagePayload()
            {
                Type = messageType,
                Message = message,
                Topic = $"{configuration.Product}/{configuration.Environment}/{topic}"
            };

            mqttClient.PublishTopic(topic, mqttMessagePayload);


        }

    }
}