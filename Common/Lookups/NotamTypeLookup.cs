﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class NotamTypeLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<NotamTypeLookup> GetLookup()
        {
            var lookup = new List<NotamTypeLookup>();
            lookup.Add(new NotamTypeLookup() { Id = 1, Name = "SPACE OPERATIONS" });
            lookup.Add(new NotamTypeLookup() { Id = 2, Name = "AIR SHOWS/SPORTS" });
            lookup.Add(new NotamTypeLookup() { Id = 3, Name = "SECURITY" });
            lookup.Add(new NotamTypeLookup() { Id = 4, Name = "HAZARDS" });
            lookup.Add(new NotamTypeLookup() { Id = 5, Name = "VIP" });
            lookup.Add(new NotamTypeLookup() { Id = 6, Name = "SPECIAL" });

            return lookup;
        }
    }
}