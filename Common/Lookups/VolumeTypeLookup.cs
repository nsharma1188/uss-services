﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class VolumeTypeLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<VolumeTypeLookup> GetLookup()
        {
            var lookup = new List<VolumeTypeLookup>();
            lookup.Add(new VolumeTypeLookup() { Id = 1, Name = "ABOV" });
            lookup.Add(new VolumeTypeLookup() { Id = 2, Name = "TBOV" });
            return lookup;
        }
    }
}