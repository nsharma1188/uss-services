﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class FacilityLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<FacilityLookup> GetLookup()
        {
            var lookup = new List<FacilityLookup>();
            lookup.Add(new FacilityLookup { Id = 1, Name = "USA" });
            lookup.Add(new FacilityLookup { Id = 2, Name = "Other" });
            return lookup;
        }
    }
}
