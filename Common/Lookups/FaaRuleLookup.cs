﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class FaaRuleLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<FaaRuleLookup> GetLookup()
        {
            var lookup = new List<FaaRuleLookup>();
            lookup.Add(new FaaRuleLookup() { Id = 1, Name = "PART_107" });
            lookup.Add(new FaaRuleLookup() { Id = 2, Name = "PART_107X" });
            lookup.Add(new FaaRuleLookup() { Id = 3, Name = "PART_101E" });
            lookup.Add(new FaaRuleLookup() { Id = 4, Name = "OTHER" });
            return lookup;
        }
    }
}