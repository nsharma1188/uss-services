﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class DroneBrandLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<DroneBrandLookup> GetLookup()
        {
            var lookup = new List<DroneBrandLookup>();
            lookup.Add(new DroneBrandLookup() { Id = 1, Name = "SenseFly" });
            lookup.Add(new DroneBrandLookup() { Id = 2, Name = "3DRobotics" });
            lookup.Add(new DroneBrandLookup() { Id = 3, Name = "Arialtronics" });
            lookup.Add(new DroneBrandLookup() { Id = 4, Name = "Blade" });
            lookup.Add(new DroneBrandLookup() { Id = 5, Name = "Cyber Technology" });
            lookup.Add(new DroneBrandLookup() { Id = 6, Name = "DIY" });
            lookup.Add(new DroneBrandLookup() { Id = 7, Name = "DJI" });
            lookup.Add(new DroneBrandLookup() { Id = 8, Name = "DroneRC" });
            lookup.Add(new DroneBrandLookup() { Id = 9, Name = "EyeFly" });
            lookup.Add(new DroneBrandLookup() { Id = 10, Name = "FlyingEye" });
            lookup.Add(new DroneBrandLookup() { Id = 11, Name = "FreeFly" });
            lookup.Add(new DroneBrandLookup() { Id = 12, Name = "HoneyComb" });
            lookup.Add(new DroneBrandLookup() { Id = 13, Name = "Lumenier" });
            lookup.Add(new DroneBrandLookup() { Id = 14, Name = "Parrot" });
            lookup.Add(new DroneBrandLookup() { Id = 15, Name = "Sabre" });
            lookup.Add(new DroneBrandLookup() { Id = 16, Name = "Steadidrone" });
            lookup.Add(new DroneBrandLookup() { Id = 17, Name = "TBS" });
            lookup.Add(new DroneBrandLookup() { Id = 18, Name = "TrimbleNavigation" });
            lookup.Add(new DroneBrandLookup() { Id = 19, Name = "Walkera" });
            lookup.Add(new DroneBrandLookup() { Id = 20, Name = "Yuneec" });
            return lookup;
        }
    }
}