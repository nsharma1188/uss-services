﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class DroneTypeLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<DroneTypeLookup> GetLookup()
        {
            var lookup = new List<DroneTypeLookup>();
            lookup.Add(new DroneTypeLookup() { Id = 1, Name = "FixedWing" });
            lookup.Add(new DroneTypeLookup() { Id = 2, Name = "Copter" });
            lookup.Add(new DroneTypeLookup() { Id = 3, Name = "Unknown" });
            return lookup;
        }
    }
}