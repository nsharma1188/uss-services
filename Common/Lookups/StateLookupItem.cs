﻿using System.Collections.Generic;

namespace AnraUssServices.Common.Lookups
{
    public class StateLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static List<StateLookup> GetLookup()
        {
            var lookup = new List<StateLookup>();
            lookup.Add(new StateLookup { Id = 1, Name = "AL" });
            lookup.Add(new StateLookup { Id = 2, Name = "AK" });
            lookup.Add(new StateLookup { Id = 3, Name = "AJ" });
            lookup.Add(new StateLookup { Id = 4, Name = "AR" });
            lookup.Add(new StateLookup { Id = 5, Name = "CA" });
            lookup.Add(new StateLookup { Id = 6, Name = "CO" });
            lookup.Add(new StateLookup { Id = 7, Name = "CT" });
            lookup.Add(new StateLookup { Id = 8, Name = "DE" });
            lookup.Add(new StateLookup { Id = 9, Name = "FL" });
            lookup.Add(new StateLookup { Id = 10, Name = "GA" });
            lookup.Add(new StateLookup { Id = 11, Name = "HI" });
            lookup.Add(new StateLookup { Id = 12, Name = "ID" });
            lookup.Add(new StateLookup { Id = 13, Name = "IL" });
            lookup.Add(new StateLookup { Id = 14, Name = "IN" });
            lookup.Add(new StateLookup { Id = 15, Name = "IA" });
            lookup.Add(new StateLookup { Id = 16, Name = "KS" });
            lookup.Add(new StateLookup { Id = 17, Name = "KY" });
            lookup.Add(new StateLookup { Id = 18, Name = "LA" });
            lookup.Add(new StateLookup { Id = 19, Name = "ME" });
            lookup.Add(new StateLookup { Id = 20, Name = "MD" });
            lookup.Add(new StateLookup { Id = 21, Name = "MA" });
            lookup.Add(new StateLookup { Id = 22, Name = "MI" });
            lookup.Add(new StateLookup { Id = 23, Name = "MN" });
            lookup.Add(new StateLookup { Id = 24, Name = "MS" });
            lookup.Add(new StateLookup { Id = 25, Name = "MO" });
            lookup.Add(new StateLookup { Id = 26, Name = "MT" });
            lookup.Add(new StateLookup { Id = 27, Name = "NE" });
            lookup.Add(new StateLookup { Id = 28, Name = "NV" });
            lookup.Add(new StateLookup { Id = 29, Name = "NH" });
            lookup.Add(new StateLookup { Id = 30, Name = "NJ" });
            lookup.Add(new StateLookup { Id = 31, Name = "NM" });
            lookup.Add(new StateLookup { Id = 32, Name = "NY" });
            lookup.Add(new StateLookup { Id = 33, Name = "NC" });
            lookup.Add(new StateLookup { Id = 34, Name = "ND" });
            lookup.Add(new StateLookup { Id = 35, Name = "OH" });
            lookup.Add(new StateLookup { Id = 36, Name = "OK" });
            lookup.Add(new StateLookup { Id = 37, Name = "OR" });
            lookup.Add(new StateLookup { Id = 38, Name = "PA" });
            lookup.Add(new StateLookup { Id = 39, Name = "RI" });
            lookup.Add(new StateLookup { Id = 40, Name = "SC" });
            lookup.Add(new StateLookup { Id = 41, Name = "SD" });
            lookup.Add(new StateLookup { Id = 42, Name = "TN" });
            lookup.Add(new StateLookup { Id = 43, Name = "TX" });
            lookup.Add(new StateLookup { Id = 44, Name = "UT" });
            lookup.Add(new StateLookup { Id = 45, Name = "VT" });
            lookup.Add(new StateLookup { Id = 46, Name = "VA" });
            lookup.Add(new StateLookup { Id = 47, Name = "WA" });
            lookup.Add(new StateLookup { Id = 48, Name = "WV" });
            lookup.Add(new StateLookup { Id = 49, Name = "WI" });
            lookup.Add(new StateLookup { Id = 50, Name = "WY" });
            lookup.Add(new StateLookup { Id = 51, Name = "Other" });
            return lookup;
        }
    }
}
