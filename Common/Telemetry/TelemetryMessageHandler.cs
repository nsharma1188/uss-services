﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.Data;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.TelemetryMessages;
using Mapster;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Telemetry
{
    public class TelemetryMessageHandler
    {
        public static Dictionary<string, TelemetryMessageHandler> TelemetryMessageHanlders = new Dictionary<string, TelemetryMessageHandler>();

        public Guid Gufi { get; set; }
        public Guid UssInstanceId { get; set; }
        public Utm.Models.Operation Operation { get; set; }
        public ApplicationDbContext ApplicationDbContext { get; set; }
        public PositionDocument PositionDocument { get; set; }
        public UtmMessenger UtmMessenger { get; set; }
        public IRepository<UtmMessage> UtmMessageRepository { get; set; }
        public AnraMqttClient AnraMqttClient { get; set; }
        public ViewerMqttNotification ViewerMqttNotification { get; set; }        

        public static TelemetryMessageHandler GetHandler(IServiceProvider serviceProvider, AnraMqttClient anraMqttClient, string gufi)
        {
            if (TelemetryMessageHanlders.ContainsKey(gufi) && TelemetryMessageHanlders[gufi] != null)
            {
                return TelemetryMessageHanlders[gufi];
            }
            else
            {
                var messageHandler = new TelemetryMessageHandler();

                var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();

                var operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();
                var operation = operationRepository.Find(Guid.Parse(gufi));
                messageHandler.UssInstanceId = operation.UssInstanceId;
                messageHandler.Operation = operation.Adapt<Utm.Models.Operation>();
                messageHandler.ApplicationDbContext = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                messageHandler.PositionDocument = serviceScope.ServiceProvider.GetService<PositionDocument>();
                messageHandler.UtmMessenger = serviceScope.ServiceProvider.GetService<UtmMessenger>();
                messageHandler.UtmMessageRepository = serviceScope.ServiceProvider.GetService<IRepository<UtmMessage>>();                
                messageHandler.ViewerMqttNotification = serviceScope.ServiceProvider.GetService<ViewerMqttNotification>();
                messageHandler.AnraMqttClient = anraMqttClient;

                TelemetryMessageHanlders[gufi] = messageHandler;

                return messageHandler;
            }
        }

        public async Task SavePosition(TelemetryMessageViewModel messageItem)
        {
            if (!Operation.UasRegistrations.Select(p => p.RegistrationId.ToString()).Contains(messageItem.Registration))
            {
                return;
            }

            //If Operation of the incoming telemetry is closed then don't process anything further.
            if (Operation.State.Equals(OperationState.CLOSED))
            {
                return;
            }

            AnraMqttClient.PublishTopic(string.Format(MqttTopics.TelementaryConnTopic, messageItem.Registration), messageItem);

            messageItem.TimeSent = DateTime.UtcNow;
            messageItem.TrackBearing = Math.Abs((messageItem.TrackBearing == 360.0 ? messageItem.TrackBearing = 0.0 : messageItem.TrackBearing).Value);
			messageItem.TimeMeasured = messageItem.TimeMeasured > DateTime.UtcNow ? DateTime.UtcNow : messageItem.TimeMeasured;
            messageItem.UssName = Operation.UssName;

            //Save to LiteDB
            PositionDocument.AddOrUpdateAsync(messageItem);

            var model = messageItem.Adapt<TelemetryMessage>();
            ApplicationDbContext.TelemetryMessages.Add(model);
            await ApplicationDbContext.SaveChangesAsync();

            await ViewerMqttNotification.NotifyPositionMessage(messageItem);

            await AsyncNotifyLUN(messageItem);

            AnraMqttClient.PublishTopic($"detection/inputs/{UssInstanceId}", messageItem.ToDetectsArray());
        }

        private async Task AsyncNotifyLUN(TelemetryMessageViewModel messageItem)
        {
            var utmPosition = messageItem.Adapt<Position>();

            if (Operation.IsPublicSafety() || Operation.IsEmergency())
            {
                UtmMessenger.NotifyPositionToLUN(utmPosition, Operation, null);
            }
            else
            {
                var utmMessages = UtmMessageRepository.GetAll().Where(p => p.Gufi == Operation.Gufi && p.MessageType.StartsWith("PERIODIC_POSITION_REPORTS", StringComparison.InvariantCultureIgnoreCase)).ToList();

                UtmMessenger.NotifyPositionToLUN(utmPosition, Operation, utmMessages);
            }
        }
    }
}