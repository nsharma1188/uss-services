﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Notification
{
	public class SendNotification
	{
		private readonly AnraConfiguration configuration;
		private readonly ILogger<SendNotification> _logger;

		public SendNotification(AnraConfiguration configuration, ILogger<SendNotification> logger)
		{
			this.configuration = configuration;
			_logger = logger;
		}

		public async void SendEmail(string emailAddress, string emailBody)
		{
			try
			{
				SmtpClient client = new SmtpClient();
				client.Host = configuration.SmtpHost;
				client.EnableSsl = true;
				client.UseDefaultCredentials = false;
				client.Credentials = new System.Net.NetworkCredential(configuration.SendersEmail, configuration.SendersEmailPassword);
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				await client.SendMailAsync(configuration.SendersEmail, emailAddress, "Email Verification ", emailBody).ConfigureAwait(false);
			}
			catch (Exception ex)
			{
                _logger.LogError($"SendNotification ::: SendEmail :: Exception : {ex.Message}");

            }
		}

        public async void SendEmail(string emailAddress, string emailBody,string emailSubject)
        {
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(configuration.SendersEmail);
                msg.To.Add(emailAddress);
                msg.Subject = emailSubject;
                msg.Body = emailBody;
                msg.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();
                client.Host = configuration.SmtpHost;
                client.EnableSsl = true;                
                client.UseDefaultCredentials = false;                
                client.Credentials = new System.Net.NetworkCredential(configuration.SendersEmail, configuration.SendersEmailPassword);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                await client.SendMailAsync(msg).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"SendNotification ::: SendEmail :: Exception : {ex.Message}");
            }
        }

        //Currently we are not supporting text notification for USS services
        public async void SendText(string phoneNumber, string verificationCode)
		{
			try
			{
				var textMessage = $"The one time password (otp) for registration is {verificationCode}";
				var client = new HttpClient();
				var task = await client.GetAsync("https://login.transit-mobile.com/apiaccess/SMSSend.php?email=" + configuration.SmsEmail + "&password=" + configuration.SmsPassword + "&keyword=GOMOTEXT&smsto=" + phoneNumber + "&network=UNKNOWNUS&smsmsg=" + textMessage);
			}
			catch (Exception ex)
			{
                _logger.LogError($"SendNotification ::: SendText :: Exception : {ex.Message}");
            }
		}
	}
}
