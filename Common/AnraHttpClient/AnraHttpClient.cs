﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Common.Security;
using AnraUssServices.Utm.Models;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Interface;
using Jal.HttpClient.Model;
using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AnraUssServices.Common
{
    public sealed class AnraHttpClient
    {
        private readonly IMediator mediator;
        private readonly JwsHelper jwsHelper;
        private readonly AnraConfiguration anraConfiguration;
        private readonly ILogger<AnraHttpClient> logger;
        private IHttpHandler httpClient;
        private readonly IServiceProvider serviceProvider;

        private ViewerMqttNotification ViewerMqttNotification { get; }

        public AnraHttpClient(ILogger<AnraHttpClient> logger,
            HttpClientLogger httpClientLogger,
            ViewerMqttNotification viewerMqttNotification,
            IServiceProvider serviceProvider,
            JwsHelper jwsHelper,
            IMediator mediator,
            AnraConfiguration anraConfiguration)
        {
            this.logger = logger;
            this.serviceProvider = serviceProvider;
            ViewerMqttNotification = viewerMqttNotification;
            httpClient = HttpHandler.Builder.Create;
            httpClient.Interceptor = httpClientLogger;
            this.anraConfiguration = anraConfiguration;
            this.jwsHelper = jwsHelper;
            this.mediator = mediator;
        }

        public object Get<T>(string url, string accessToken = "", string ussName = "")
        {
            try
            {
                var request = new HttpRequest(url, HttpMethod.Get);
                var requestTime = DateTime.UtcNow;
                if (!string.IsNullOrEmpty(accessToken))
                {
                    request.Headers.Add(new HttpHeader("authorization", accessToken));
                }

                request.Content.ContentType = "application/json";
                request.Content.CharacterSet = "charset=utf-8";

                using (var response = httpClient.Send(request))
                {
                    var content = response.Content.Read();
                    if (!string.IsNullOrEmpty(ussName))
                    {
                        mediator.Send(new AsyncUssExchangeNotification(request, response, requestTime, DateTime.UtcNow, ussName));
                    }
                    return Deserialize(content, typeof(T));
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
                logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);
                return null;
            }
        }

        public async Task<UTMRestResponse> Put<T>(string url, string endpoint, T data, string authToken, string ussName = "")
        {
            try
            {
                var request = new HttpRequest(url + endpoint, HttpMethod.Put);
                request.Headers.AddRange(GetDefaultHeaders(authToken));
                request.Content = new HttpRequestStringContent(Serialize(data))
                {
                    ContentType = "application/json",
                    CharacterSet = "charset=utf-8"
                };
                //var jwsHeaderValue = jwsHelper.GetJwsHeader(data);
                //request.Headers.Add(new HttpHeader("x-utm-signed-payload", jwsHeaderValue));

                var requestTime = DateTime.UtcNow;
                var response = httpClient.Send(request);

                await ViewerMqttNotification.NotifyLunRequestToViewer(ussName, url, data, response);

                if (!string.IsNullOrEmpty(ussName))
                {
                    await mediator.Send(new AsyncUssExchangeNotification(request, response, requestTime, DateTime.UtcNow, ussName));
                }

                return response.Adapt<UTMRestResponse>();
            }
            catch (Exception ex)
            {
                logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
                logger.LogError("Exception::: AnraHttpClient :: data = {0}", Serialize(data));
                logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);
                return new UTMRestResponse { HttpStatusCode = 400, Message = ex.Message };
            }
        }

        public JSendGridCellMetadataResponse Put<T>(string url, string endpoint, T data, long syncToken, string accessToken)
        {
            try
            {
                var request = new HttpRequest(url + endpoint, HttpMethod.Put);

                request.Headers.Add(new HttpHeader("authorization", accessToken));
                request.Headers.Add(new HttpHeader("accept", "application/json"));

                //sync_token header will be added if current request require it else dont include this into header
                if(syncToken > 0)
                {
                    request.Headers.Add(new HttpHeader("sync_token", syncToken.ToString()));
                }                

                request.Content = new HttpRequestStringContent(Serialize(data))
                {
                    ContentType = "application/json"
                };

                var requestTime = DateTime.UtcNow;
                var response = httpClient.Send(request);

                return response.Adapt<JSendGridCellMetadataResponse>();
            }
            catch (Exception ex)
            {
                logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
                logger.LogError("Exception::: AnraHttpClient :: data = {0}", Serialize(data));
                logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);
                return new { Status = "Bad Request", HttpStatusCode = 400, SyncToken = syncToken }.Adapt<JSendGridCellMetadataResponse>();
            }
        }

        public JSendGridCellMetadataResponse Delete<T>(string url, T data, string accessToken = "")
        {
            var request = new HttpRequest(url, HttpMethod.Delete);

            if (!string.IsNullOrEmpty(accessToken))
            {
                request.Headers.Add(new HttpHeader("authorization", accessToken));
            }

            request.Content = new HttpRequestStringContent(Serialize(data))
            {
                ContentType = "application/json"
            };

            var response = httpClient.Send(request);

            return response.Adapt<JSendGridCellMetadataResponse>();
        }

        public T Delete<T>(string url, string accessToken = "")
        {
            var request = new HttpRequest(url, HttpMethod.Delete);

            if (!string.IsNullOrEmpty(accessToken))
            {
                request.Headers.Add(new HttpHeader("authorization", accessToken));
            }

            request.Content.ContentType = "application/json";
            request.Content.CharacterSet = "charset=utf-8";

            var response = httpClient.Send(request);            

            return response.Adapt<T>();
        }

        public HttpResponse PostVehicleRegistration(string url, string authToken, string utmAuthToken)
        {
            try
            {
                var request = new HttpRequest(url, HttpMethod.Post);

                var listHeaders = GetDefaultHeaders(authToken);

                listHeaders.Add(new HttpHeader("X-UTM-Auth", utmAuthToken));

                request.Headers.AddRange(listHeaders);

                return httpClient.Send(request);//.Adapt<UTMRestResponse>();
            }
            catch (Exception ex)
            {
                return new HttpResponse { HttpStatusCode = System.Net.HttpStatusCode.BadRequest, Exception = new System.Net.WebException(ex.Message) };
            }
        }

        public UTMRestResponse Post(string url, object data, string authToken)
        {
            try
            {   
                var request = new HttpRequest(url, HttpMethod.Post);
                request.Headers.AddRange(GetDefaultHeaders(authToken));
                request.Content = new HttpRequestStringContent(Serialize(data))
                {
                    ContentType = "application/json",
                    CharacterSet = "charset=utf-8"
                };

                //request.Headers.Add(new HttpHeader("x-utm-signed-payload", jwsHelper.GetJwsHeader(data)));

                var response = httpClient.Send(request);

                return response.Adapt<UTMRestResponse>();
            }
            catch (Exception ex)
            {
                logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
                logger.LogError("Exception::: AnraHttpClient :: data = {0}", Serialize(data));
                logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);

                return new UTMRestResponse { HttpStatusCode = 400, Message = ex.Message };
            }
        }

		/// <summary>
		/// Handling Operation Event for Billing engine
		/// </summary>
		public object PostOperationEvent<T>(string url, object data, string authToken = null)
		{
			try
			{
				var request = new HttpRequest(url, HttpMethod.Post);

				if (authToken != null)
				{
					request.Headers.AddRange(GetDefaultHeaders(authToken));
				}

				request.Content = new HttpRequestStringContent(Serialize(data))
				{
					ContentType = "application/json"
				};

				var requestTime = DateTime.UtcNow;

				var response = httpClient.SendAsync(request).Result.Content.Read();

				return response;
			}
			catch (Exception ex)
			{
				logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
				logger.LogError("Exception::: AnraHttpClient :: data = {0}", Serialize(data));
				logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);

				return null;
			}
		}

		public object Get<T>(string url, object data)
		{
			try
			{
				using (var client = new System.Net.Http.HttpClient())
				{
					var request = new System.Net.Http.HttpRequestMessage
					{
						RequestUri = new Uri(url),
						Method = System.Net.Http.HttpMethod.Get,
					};

					var requestBody = Serialize(data);
					request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));

					request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

					var result = client.SendAsync(request).Result;
					result.EnsureSuccessStatusCode();
					var response = result.Content.ReadAsStringAsync().Result;
					return Deserialize(response, typeof(T));
				}
			}
			catch (Exception ex)
			{
				logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
				logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);
				return null;
			}
		}

		public void Put<T>(string url, T data, string accessToken)
		{
			try
			{
				var request = new HttpRequest(url, HttpMethod.Put);

				request.Headers.Add(new HttpHeader("authorization", accessToken));

				request.Content = new HttpRequestStringContent(Serialize(data))
				{
					ContentType = "application/json"
				};

				var requestTime = DateTime.UtcNow;
				var response = httpClient.Send(request);
			}
			catch (Exception ex)
			{
				logger.LogError("Exception::: AnraHttpClient :: url = {0}", url);
				logger.LogError("Exception::: AnraHttpClient :: data = {0}", Serialize(data));
				logger.LogError("Exception::: AnraHttpClient :: exception = {0}", ex);
			}
		}

		private List<HttpHeader> GetDefaultHeaders(string authToken = "")
        {
            var headers = new List<HttpHeader>
            {
                new HttpHeader("content-type", "application/json")
            };

            if (!string.IsNullOrEmpty(authToken))
            {
                headers.Add(new HttpHeader("authorization", authToken));
            }

            return headers;
        }

        private string Serialize(object obj)
        {
            var serializerSettings = new JsonSerializerSettings
            {
                DateFormatString = "yyyy-MM-ddTHH:mm:ss.fffZ",
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore
            };

            serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            return obj != null ? JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, serializerSettings) : string.Empty;
        }

        /// <summary>
        /// Deserialize the JSON string into a proper object.
        /// </summary>
        /// <param name="content">HTTP body (e.g. string, JSON).</param>
        /// <param name="type">Object type.</param>
        /// <param name="headers">HTTP headers.</param>
        /// <returns>Object representation of the JSON string.</returns>
        public object Deserialize(string content, Type type)
        {
            if (type == typeof(Object)) // return an object
            {
                return content;
            }

            if (type.Name.StartsWith("System.Nullable`1[[System.DateTime")) // return a datetime object
            {
                return DateTime.Parse(content, null, System.Globalization.DateTimeStyles.AssumeUniversal);
            }

            if (type == typeof(String) || type.Name.StartsWith("System.Nullable")) // return primitive type
            {
                return ConvertType(content, type);
            }

            // at this point, it must be a model (json)
            try
            {
                return JsonConvert.DeserializeObject(content, type);
            }
            catch (IOException)
            {
                throw;
            }
        }

        /// <summary>
        /// Dynamically cast the object into target type.
        /// Ref: http://stackoverflow.com/questions/4925718/c-dynamic-runtime-cast
        /// </summary>
        /// <param name="source">Object to be casted</param>
        /// <param name="dest">Target type</param>
        /// <returns>Casted object</returns>
        public static Object ConvertType(Object source, Type dest)
        {
            return Convert.ChangeType(source, dest);
        }
    }
}