﻿using AnraUssServices.Common.Mqtt;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Model;
using Microsoft.Extensions.Logging;
using System.Text;

namespace AnraUssServices.Common
{
    public class HttpClientLogger : AbstractHttpInterceptor
    { 
        private readonly ILogger<HttpClientLogger> _log;

        public HttpClientLogger(ILogger<HttpClientLogger> log)
        {
            _log = log; 
        }

        public override void OnEntry(HttpRequest request)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"ANRA:::HTTP CLIENT:::REQUEST:::{request.Url}");
            builder.AppendLine($"{request.HttpMethod.ToString().ToUpper()} {request.Uri.PathAndQuery} HTTP/1.1");
            foreach (var httpHeader in request.Headers)
            {
                builder.AppendLine($"{httpHeader.Name}: {httpHeader.Value} ");
            }
            var contenttype = request.Content.GetContentType();
            if (!string.IsNullOrWhiteSpace(contenttype))
            {
                builder.AppendLine($"Content-Type: {contenttype}");
            }
            builder.AppendLine("");
            builder.AppendLine($"{request.Content}");

            #if DEBUG
            _log.LogInformation(builder.ToString());
            #endif
        }

        public override void OnExit(HttpResponse response, HttpRequest request)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"ANRA:::HTTP CLIENT:::RESPONSE");
            builder.AppendLine($"HTTP/1.1 {response.HttpStatusCode}{response.HttpExceptionStatus}");
            builder.AppendLine($"Duration: {response.Duration} ms");
            if (response.Exception != null)
            {
                builder.AppendLine($"Exception: {response.Exception.Message}");
            }
            foreach (var httpHeader in response.Headers)
            {
                builder.AppendLine($"{httpHeader.Name}: {httpHeader.Value} ");
            }
            builder.AppendLine("");

            if (response.Content.IsString())
            {
                builder.AppendLine($"{Truncate(response.Content.Read())}");
            }
            #if DEBUG
            _log.LogInformation(builder.ToString());
            #endif
        }

        public string Truncate(string content)
        {
            if (string.IsNullOrEmpty(content)) return content;
            return content.Length <= 4096 ? content : content.Substring(0, 4096) + "...";
        }
    }
}