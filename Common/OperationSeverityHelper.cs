﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.Common.Validators;
using AnraUssServices.Utm.Models;
using Microsoft.Extensions.DependencyInjection;

namespace AnraUssServices.Common
{
	public class OperationSeverityHelper
	{
		private readonly AnraConfiguration anraConfiguration;
		private readonly AirSpaceIntersectionValidator airSpaceIntersectionValidator;
		private bool airSpaceIntersect;

		public OperationSeverityHelper(AnraConfiguration anraConfiguration, AirSpaceIntersectionValidator airSpaceIntersectionValidator)
		{
			this.airSpaceIntersectionValidator = airSpaceIntersectionValidator;
			this.anraConfiguration = anraConfiguration;
			airSpaceIntersect = false;
		}
		public Severity GetClosedOperationSeverity(string lastState)
		{
			/*TO-DO
			 * OPERATION_CLOSED  CRITICAL If operation was ROGUE or NONCONFORMING at any time.                        	|
             OPERATION_CLOSED    NOTICE  If not CRITICAL.             
			 */

			if (lastState == EnumUtils.GetDescription(OperationState.NONCONFORMING) || lastState == EnumUtils.GetDescription(OperationState.ROGUE))
			{
				return Severity.CRITICAL;
			}
			else
			{
				return Severity.NOTICE;
			}

		}

		public Severity GetNonConformingOperationSeverity(Models.Operation operation)
		{
			/*
			 * TO-DO
			 *OPERATION_NONCONFORMING  EMERGENCY  Operation intersecting another op plan or unauthz'ed airspace.              	
             OPERATION_NONCONFORMING   ALERT      Operation expected to intersect another op plan or unauthz'ed airspace.     	
             OPERATION_NONCONFORMING   CRITICAL    If not EMERGENCY nor ALERT.   
			 */

			//Still need to check for unauthorized airspace case


			if (anraConfiguration.IsAirSpaceBoundaryCheckEnable)
			{
				airSpaceIntersect = airSpaceIntersectionValidator.CheckAirspaceIntersection(operation);
			}

			if (operation.OperationConflicts != null || airSpaceIntersect)
			{
				return Severity.EMERGENCY;
			}
			else
			{
				return Severity.CRITICAL;
			}
		}

		public Severity GetRogueOperationSeverity(Models.Operation operation)
		{
			/*
			 * TO-DO
			 *OPERATION_ROGUE  EMERGENCY   Current/impending intersection with another op plan or unauthz'ed airspace. 	
              OPERATION_ROGUE    ALERT       If not EMERGENCY.   
			 */

			//Still need to check for unauthorized airspace case

			if (anraConfiguration.IsAirSpaceBoundaryCheckEnable)
			{
				airSpaceIntersect = airSpaceIntersectionValidator.CheckAirspaceIntersection(operation);
			}

			if (operation.OperationConflicts != null || airSpaceIntersect)
			{
				return Severity.EMERGENCY;
			}
			else
			{
				return Severity.ALERT;
			}
		}

	}
}
