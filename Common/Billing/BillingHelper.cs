﻿using AnraUssServices.Common.OAuthClient;
using AnraUssServices.Models;
using AnraUssServices.ViewModel.Billing;
using AnraUssServices.ViewModel.Users;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Billing
{
	public class BillingHelper
	{
		private readonly AnraConfiguration configuration;
        private readonly ILogger<BillingHelper> logger;
		private UserClient userClient;
		private IRepository<Models.Operation> operationRepository;
		private readonly IServiceProvider serviceProvider;
		private AnraHttpClient anraHttpClient;

		public BillingHelper(AnraConfiguration configuration, ILogger<BillingHelper> logger,
			IServiceProvider serviceProvider)
		{
			this.configuration = configuration;
            this.logger = logger;
			this.serviceProvider = serviceProvider;
			SetDependencies();
		}

		private void SetDependencies()
		{
			var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
			operationRepository = serviceScope.ServiceProvider.GetService<IRepository<Models.Operation>>();
			userClient = serviceScope.ServiceProvider.GetService<UserClient>();
			anraHttpClient = serviceScope.ServiceProvider.GetService<AnraHttpClient>();
		}

		///<summary>
		/// Sends the operation event to the billing engine.
		/// </summary>
		/// <param name="operationEvent"></param>
		/// <returns></returns>
		public async void PostOperationEvent(OperationEvent operationEvent)
        {
            try
            {
				var url = string.Concat(configuration.BillingServiceUrl, $"/operationevent/");

				var request = anraHttpClient.PostOperationEvent<RequestStatus>(url, operationEvent);

                #if DEBUG
                    logger.LogInformation("BillingHelper ::: PostOperationEvent :: Response : {0}", request.ToString());
                #endif  

            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: BillingHelper :: AddOperationEvent : ex = {0}", ex.Message);
            }
        }


		/// <summary>
		/// Requests the full list opf historical and current receipts of a drone operator.
		/// </summary>
		/// <param name="authorization"></param>
		/// <param name="groupId"></param>
		/// <returns></returns>
		public List<Receipt> GetReceipts(string authorization, string groupId)
		{
            //Getting the list of users on the group
			var usersList = userClient.GetUsers(authorization, groupId);

            var newUsersList = new List<UserItem>();
			var newReceiptList = new List<Receipt>();
		

			foreach (var user in usersList)
			{
				if (user.Roles.Exists(x => x.Name == EnumUserRole.ADMIN.ToString() ||
				 x.Name == EnumUserRole.PILOT.ToString()))
				{
					newUsersList.Add(user);
				}
			}

			foreach (var user in newUsersList)
			{
				var ussinstanceList = operationRepository.GetAll().ToList().Where(x => x.UserId == user.UserId).ToList()
					.Select(x => x.UssInstanceId).Distinct().ToList();
			
				foreach (var ussinstanceId in ussinstanceList)
				{
					var url = string.Concat(configuration.BillingServiceUrl, $"receipts/?user_id={user.UserId}&uss_instance_id={ussinstanceId.ToString()}");

					try
					{
						using (var client = new System.Net.Http.HttpClient())
						{
							var request = new System.Net.Http.HttpRequestMessage
							{
								RequestUri = new Uri(url),
								Method = System.Net.Http.HttpMethod.Get,
							};

							request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(""));
							request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
							var result = client.SendAsync(request).Result;
							var response = result.Content.ReadAsStringAsync().Result;

							var receiptList = JsonConvert.DeserializeObject<List<Receipt>>(response);

							newReceiptList.AddRange(receiptList);
						}
					}
					catch (Exception ex)
					{
						logger.LogError("Exception ::: BillingHelper :: AddOperationEvent : ex = {0}", ex.Message);
					}
					
				}
			}

			return newReceiptList;
		}
	}
}





