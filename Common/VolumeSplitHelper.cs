﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.Data;
using AnraUssServices.ViewModel.Altitudes;
using AnraUssServices.ViewModel.Operations;
using GeoAPI.Geometries;
using GeoCoordinatePortable;
using Microsoft.Extensions.DependencyInjection;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;

namespace AnraUssServices.Common
{
	public class VolumeSplitHelper
	{
		private readonly ElevationApi elevationApi;
		private readonly DatabaseFunctions dbFunctions;
		private readonly AnraConfiguration configuration;

		public VolumeSplitHelper(ElevationApi elevationApi, DatabaseFunctions dbFunctions, AnraConfiguration configuration)
		{
			this.dbFunctions = dbFunctions;
			this.elevationApi = elevationApi;
			this.configuration = configuration;
		}

		/// <summary>
		/// Creates linestring between two points 
		/// </summary>
		public List<LineStringViewModel> SplitLineString(double x0, double y0, double x1, double y1, double distance, double altitudePointA, double altitudePointB)
		{
			var newEdgeLength = configuration.VolumeSplittingLength; //maximum edgeLength from the config file for the volume splitting

			var endFraction = newEdgeLength / distance;
			var lineSplittingFraction = endFraction;
			List<LineStringViewModel> lineStrings = new List<LineStringViewModel>();
			var startFraction = 0.0;
			double newAltitudePointA = 0.0;
			double newAltitudePointB = 0.0;

			while (endFraction <= 1)
			{
				var splitGeographyPoint = dbFunctions.GetSplitGeography(x0, y0, x1, y1, startFraction, endFraction);
				var lineString = JsonConvert.DeserializeObject<LineStringViewModel>(splitGeographyPoint.ToString());

				if (altitudePointA == altitudePointB)
				{
					newAltitudePointA = altitudePointA;
					newAltitudePointB = altitudePointB;
				}
				else
				{
					var theta = Math.Atan(((altitudePointB - altitudePointA) / distance));
					var a = new GeoCoordinate(y0, x0);
					var b = new GeoCoordinate(Convert.ToDouble(lineString.Coordinates[1][1]), Convert.ToDouble(lineString.Coordinates[1][0]));
					var newDistance = (a.GetDistanceTo(b)) * 3.28084; //distance in feet 

					if (newAltitudePointA == 0)
					{
						newAltitudePointA = altitudePointA;
					}
					else
					{
						newAltitudePointA = newAltitudePointB;
					}

					if (startFraction > 0 && newAltitudePointA == 0)
					{
						newAltitudePointA = newAltitudePointB;
					}

					newAltitudePointB = (Math.Tan(theta) * newDistance) + altitudePointA;
				}

				lineStrings.Add(new LineStringViewModel()
				{
					Type = "LineString",
					Coordinates = lineString.Coordinates,
					AltitudePointA = newAltitudePointA,
					AltitudePointB = newAltitudePointB
				});

				startFraction = endFraction;
				endFraction = endFraction + lineSplittingFraction;


				if (endFraction > 1)
				{
					splitGeographyPoint = dbFunctions.GetSplitGeography(x0, y0, x1, y1, startFraction, 1.0);
					lineString = JsonConvert.DeserializeObject<LineStringViewModel>(splitGeographyPoint.ToString());

					if (altitudePointA == altitudePointB)
					{
						newAltitudePointA = altitudePointA;
						newAltitudePointB = altitudePointB;
					}
					else
					{
						var theta = Math.Atan(((altitudePointB - altitudePointA) / distance));
						var a = new GeoCoordinate(y0, x0);
						var b = new GeoCoordinate(Convert.ToDouble(lineString.Coordinates[1][1]), Convert.ToDouble(lineString.Coordinates[1][0]));
						var newDistance = (a.GetDistanceTo(b)) * 3.28084; //distance in feet 
						if (newAltitudePointA == 0)
						{
							newAltitudePointA = altitudePointA;
						}
						else
						{
							newAltitudePointA = newAltitudePointB;
						}
						if (startFraction > 0 && newAltitudePointA == 0)
						{
							newAltitudePointA = newAltitudePointB;
						}
						newAltitudePointB = (Math.Tan(theta) * newDistance) + altitudePointA;
					}
					lineStrings.Add(new LineStringViewModel()
					{
						Type = "LineString",
						Coordinates = lineString.Coordinates,
						AltitudePointA = newAltitudePointA,
						AltitudePointB = newAltitudePointB
					});
				}
			}
			return lineStrings;
		}


		/// <summary>
		/// Returns OperationVolumeViewModel volume using the linestring 
		/// </summary>
		public OperationVolumeViewModel CalculateVolume(LineStringViewModel lineString, GenerateOperationVolumeKml operationVolumeProperties, double aglAltitude)
		{
			var factory = new GeometryFactory(new PrecisionModel(), 4326);
			var altitudeBuffer = operationVolumeProperties.AltitudeBuffer;
            var expandFactor = 0.0;

            if (operationVolumeProperties.ExpandFactor.ToString().Equals("LOW"))
            {
                expandFactor = 0.00005;
            }
            else if(operationVolumeProperties.ExpandFactor.ToString().Equals("MEDIUM"))
            {
                expandFactor = 0.00009;
            }
            else
            {
                expandFactor = 0.0001;
            }

			var lineSegment = new LineSegment(Convert.ToDouble(lineString.Coordinates[0][0]), Convert.ToDouble(lineString.Coordinates[0][1]), Convert.ToDouble(lineString.Coordinates[1][0]), Convert.ToDouble(lineString.Coordinates[1][1]));
			var geometry = lineSegment.ToGeometry(factory);
			var g1 = geometry.Buffer(expandFactor, 4, GeoAPI.Operation.Buffer.EndCapStyle.Round);

			var distance = DistanceHelper.CalculateDistance3D(lineSegment.GetCoordinate(0).Y, lineSegment.GetCoordinate(0).X, lineString.AltitudePointA, lineSegment.GetCoordinate(1).Y, lineSegment.GetCoordinate(1).X, lineString.AltitudePointB);

			var elevationData = elevationApi.GetElevationData(lineString);
			var elevations = elevationData.Results.Select(x => x.Elevation).ToList();
			var elevationPointA = elevations[0] * 3.28084; //For feet
			var elevationPointB = elevations[1] * 3.28084;

			var poly = (NetTopologySuite.Geometries.Polygon)g1;
			var volumeGeography = ToGeoJsonString(poly);

			var minAltitude = Math.Floor(Math.Min((lineString.AltitudePointA + elevationPointA), (lineString.AltitudePointB + elevationPointB))) - operationVolumeProperties.AltitudeBuffer;
			var maxAltutude = Math.Floor(Math.Max(lineString.AltitudePointA + elevationPointA, lineString.AltitudePointB + elevationPointB)) + operationVolumeProperties.AltitudeBuffer;

			if ((minAltitude < elevationPointA) || (minAltitude < elevationPointB))
			{
				if (lineString.AltitudePointA < lineString.AltitudePointB)
				{
					minAltitude = elevationPointA;
				}
                else if(lineString.AltitudePointA == lineString.AltitudePointB)
                {
                    if(elevationPointA < elevationPointB)
                    {
                        minAltitude = elevationPointA;
                    }
                    else
                    {
                        minAltitude = elevationPointB;
                    }
                }
				else
				{
					minAltitude = elevationPointB;
				}
			}

			var minAltitudeModel = new AltitudeViewModel
			{
				AltitudeValue = Convert.ToInt32(Math.Floor(minAltitude)),
				VerticalReference = VerticalReferenceTypeEnum.W84,
				UnitsOfMeasure = UomHeightTypeEnum.FT,
				Source = AltitudeSourceEnum.ONBOARD_SENSOR
			};

			var maxAltitudeModel = new AltitudeViewModel
			{
				AltitudeValue = Convert.ToInt32(Math.Ceiling(maxAltutude)),
				VerticalReference = VerticalReferenceTypeEnum.W84,
				UnitsOfMeasure = UomHeightTypeEnum.FT,
				Source = AltitudeSourceEnum.ONBOARD_SENSOR
			};

			var volume = new OperationVolumeViewModel
			{
				VolumeType = VolumeType.TBOV,
				BeyondVisualLineOfSight = true,
				NearStructure = false,
				DistanceInFeet = distance,
				OperationGeography = JsonConvert.DeserializeObject<Utm.Models.Polygon>(volumeGeography),
				MinAltitude = minAltitudeModel,
				MaxAltitude = maxAltitudeModel,
				AglAltitude = aglAltitude
			};
			return volume;
		}

		public string ToGeoJsonString(IGeometry feature)
		{
			var writer = new GeoJsonWriter();
			var geoJson = writer.Write(feature);
			return geoJson;
		}

        /// <summary>
        /// Generates takeoff and landing volumes. Min and Max altitude in feets.
        /// </summary>
        public OperationVolumeViewModel GetTakeoffLandingVolume(Utm.Models.Polygon polygon, double minAltitude, double maxAltitude, double aglAltitude)
        {
            //var distance = DistanceHelper.CalculateDistance3D(lineSegment.GetCoordinate(0).Y, lineSegment.GetCoordinate(0).X, lineString.AltitudePointA, lineSegment.GetCoordinate(1).Y, lineSegment.GetCoordinate(1).X, lineString.AltitudePointB);

            var minAltitudeModel = new AltitudeViewModel
            {
                AltitudeValue = Convert.ToInt32(Math.Floor(minAltitude)), // Need floor value - Himanshu
                VerticalReference = VerticalReferenceTypeEnum.W84,
                UnitsOfMeasure = UomHeightTypeEnum.FT,
                Source = AltitudeSourceEnum.ONBOARD_SENSOR
            };

            var maxAltitudeModel = new AltitudeViewModel
            {
                AltitudeValue = Convert.ToInt32(Math.Ceiling(maxAltitude)), //Need ceiling value  - Himanshu
                VerticalReference = VerticalReferenceTypeEnum.W84,
                UnitsOfMeasure = UomHeightTypeEnum.FT,
                Source = AltitudeSourceEnum.ONBOARD_SENSOR
            };

            var volume = new OperationVolumeViewModel
            {
                VolumeType = VolumeType.TBOV,
                BeyondVisualLineOfSight = true,
                NearStructure = false,
                DistanceInFeet = (maxAltitude - minAltitude)*2,
                OperationGeography = polygon,
                MinAltitude = minAltitudeModel,
                MaxAltitude = maxAltitudeModel,
				AglAltitude = aglAltitude
			};

            return volume;
        }
    }



}