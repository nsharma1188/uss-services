﻿using System;
using System.Collections.Generic;
using System.Linq;
using AnraUssServices.Common.ElevationHelper;
using AnraUssServices.ViewModel.Operations;
using GeoAPI.Geometries;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Interface;
using Jal.HttpClient.Model;
using NetTopologySuite.Features;
using Newtonsoft.Json;

namespace AnraUssServices.Common
{
    public class ElevationApi
    {
        private IHttpHandler httpClient;
        private readonly AnraConfiguration _anraConfiguration;

        public ElevationApi(AnraConfiguration anraConfiguration)
        {
            _anraConfiguration = anraConfiguration;
            httpClient = HttpHandler.Builder.Create;
        }

        /// <summary>
        /// Returns the elevation data from the spatial server using the features type
        /// </summary>
        /// <param name="features"></param>
        /// <returns></returns>
        internal ElevationResult GetElevationData(List<IFeature> features)
        {
            var points = features.Select(x => x.Geometry.Coordinates).ToList();
            var coordinates = points.SelectMany(c => c).Select(p => p.Y + "," + p.X).ToArray();            
            var url = string.Format("{0}{1}&key={2}", _anraConfiguration.MslUrl, string.Join('|', coordinates), _anraConfiguration.MslKey);

            var request = new HttpRequest(url, HttpMethod.Get);

            using (var response = httpClient.Send(request))
            {
                var content = response.Content.Read();
                var result = JsonConvert.DeserializeObject<ElevationResult>(content);
                return result;
            }
        }

        /// <summary>
        /// Returns the elevation data from the spatial server using the linestring
        /// </summary>
        /// <param name="lineString"></param>
        /// <returns></returns>
		internal ElevationResult GetElevationData(LineStringViewModel lineString)
		{
            var points = lineString.Coordinates.ToList();
			string locations = points[0][1]+","+points[0][0]+"|"+ points[1][1] + "," + points[1][0];

            var url = string.Format("{0}{1}&key={2}", _anraConfiguration.MslUrl, locations, _anraConfiguration.MslKey);

            var request = new HttpRequest(url, HttpMethod.Get);

			using (var response = httpClient.Send(request))
			{
				var content = response.Content.Read();
				var result = JsonConvert.DeserializeObject<ElevationResult>(content);
				return result;
			}
		}

        /// <summary>
        /// Returns the elevation data from the spatial server using the lat and lon
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        internal ElevationResult GetElevationData(double latitude, double longitude)
        {            
            var url = string.Format("{0}{1},{2}&key={3}", _anraConfiguration.MslUrl, latitude, longitude, _anraConfiguration.MslKey);

            var request = new HttpRequest(url, HttpMethod.Get);

            using (var response = httpClient.Send(request))
            {
                var content = response.Content.Read();
                var result = JsonConvert.DeserializeObject<ElevationResult>(content);
                return result;
            }
        }

        /// <summary>
        /// Returns the data for the path and sample
        /// </summary>
        /// <param name="path"></param>
        /// <param name="sample"></param>
        /// <returns></returns>
        internal ElevationResult GetElevationData(string path,string sample )
        {
            string url = string.Format("{0}{1}&samples={2}&key={3}", _anraConfiguration.MslUrl, path, sample, _anraConfiguration.MslKey);

            var request = new HttpRequest(url, HttpMethod.Get);

            using (var response = httpClient.Send(request))
            {
                var content = response.Content.Read();
                var result = JsonConvert.DeserializeObject<ElevationResult>(content);
                return result;
            }
        }

        /// <summary>
        /// Returns the MSL at that lat lon
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        internal double GetMsl(double latitude, double longitude)
        {            
            var url = string.Format("{0}{1},{2}&key={3}", _anraConfiguration.MslUrl, latitude, longitude, _anraConfiguration.MslKey);

            var request = new HttpRequest(url, HttpMethod.Get);

            using (var response = httpClient.Send(request))
            {
                var content = response.Content.Read();
                var result = JsonConvert.DeserializeObject<ElevationResult>(content);
                var elevationItem = result.Results.FirstOrDefault();

                return elevationItem != null ? elevationItem.Elevation : 0;                
            }
        }
    }
}