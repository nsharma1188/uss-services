﻿using System.Collections.Generic;

namespace AnraUssServices.Common.ElevationHelper
{
    public class ElevationResult
    {
        public string Status { get; set; }
        public List<ElevationResultItem> Results { get; set; }
    }

    public class ElevationResultItem
    {
        public double Elevation;
        public double Resolution;
        public ElevationLocation Location;
    }

    public class ElevationLocation
    {
        public double Lat;
        public double Lng;
    }
}