﻿using Confluent.Kafka;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Kafka
{
    public class KafkaProducer
    {
        private IProducer<Null, string> producer { get; set; }
        private ProducerConfig conf { get; set; }
        private AnraConfiguration configuration { get; set; }
        private readonly ILogger<KafkaProducer> _logger;

        public KafkaProducer(AnraConfiguration configuration, ILogger<KafkaProducer> logger)
        {
            this.configuration = configuration;
            _logger = logger;

            //Starting the Kafka Producer
            StartProducer();

        }

        /// <summary>
        /// Start Kafka Producer
        /// </summary>
        private void StartProducer()
        {
            try
            {
                //Producer configuration
                this.conf = new ProducerConfig
                {
                    BootstrapServers = $"{configuration.KafkaUrl}:{configuration.KafkaPort}"
                };

                this.producer = new ProducerBuilder<Null, string>(conf).Build();

                _logger.LogInformation("KafkaProducer ::: StartKafkaProducer :: Success");
            }
            catch(Exception ex)
            {
                _logger.LogInformation("KafkaProducer ::: StartKafkaProducer :: Failed");
                _logger.LogError("Exception ::: KafkaProducer :: StartKafkaProducer : ex = {0}",ex.Message);
            }

        }

        /// <summary>
        /// Send data using Kafka server
        /// </summary>
        /// <param name="data"></param>
        /// <param name="topic"></param>
        /// <returns></returns>
        public async Task Produce(object data, string topic)
        {
            var newData = JsonConvert.SerializeObject(data);
            try
            {
                var fullTopicName = $"{configuration.Product}-{configuration.Environment}-{topic}";

                producer.Produce(fullTopicName, new Message<Null, string>
                {
                    Value = newData
                });
            }
            catch(Exception ex)
            {
                _logger.LogError("Exception ::: KafkaProducer :: Produce : ex= {0}",ex.Message + ex.StackTrace);
            }
        }
    }
}
