﻿using AnraUssServices.Common.OperationStatusLogics;
using AnraUssServices.Data.Notifications;
using Confluent.Kafka;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AnraUssServices.Common.Kafka
{
    public class KafkaConsumer
    {
        private IConsumer<Ignore, string> _consumer;
        private AnraConfiguration configuration { get; set; }
        private readonly ILogger<KafkaConsumer> _logger;
        private readonly IServiceProvider serviceProvider;

        public KafkaConsumer(AnraConfiguration configuration, ILogger<KafkaConsumer> logger,
            IServiceProvider serviceProvider)
        {
            this.configuration = configuration;
            _logger = logger;
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Starting the Kafka Consumer
        /// </summary>
        public void StartConsumer()
        {
            try
            {
                //Configuration for kafka consumer
                var config = new ConsumerConfig()
                {
                    GroupId = configuration.KafkaGroupId,
                    BootstrapServers = $"{configuration.KafkaUrl}:{configuration.KafkaPort}",
                    AutoOffsetReset = AutoOffsetReset.Earliest,
                    EnableAutoCommit = true
                };

                _consumer = new ConsumerBuilder<Ignore, string>(config).Build();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Exception ::: KafkaConsumer :: StartConsumerKafka : Error Connecting to Kafka");
                _logger.LogError($"Exception ::: KafkaConsumer :: StartConsumerKafka : ex ={ex.Message}");
            }
        }

        /// <summary>
        /// Subscribe to a topic on kafka
        /// </summary>
        /// <param name="topic"></param>
        public void Subscribe(List<string> topics)
        {
            try
            {
                _consumer.Subscribe(topics);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception ::: KafkaConsumer :: SubscribeKafkaTopic : ex ={ex.Message}");
            }
        }

        /// <summary>
        /// Processing the Kafka Consumed Message
        /// </summary>
        public void ProcessConsumedMessage()
        {
            try
            {
                //Checking for the conformance notification
                var cr = _consumer.Consume();
                var test = cr.Value;
                var confirmanceNotifaction = JsonConvert.DeserializeObject<ConfirmanceNotification>(cr.Value);

                var _serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                using (_serviceScope)
                {
                    //Based on the conformance notification state we do various operation state process
                    var operationStatusLogic = _serviceScope.ServiceProvider.GetRequiredService<OperationStatusLogic>();

                    switch (confirmanceNotifaction.State)
                    {
                        case OperationState.ACTIVATED:
                            operationStatusLogic.ToActivateAsync(confirmanceNotifaction.Gufi).ConfigureAwait(false);
                            break;

                        case OperationState.NONCONFORMING:
                            operationStatusLogic.ToNonConforming(confirmanceNotifaction.Gufi);
                            break;

                        case OperationState.ROGUE:
                            operationStatusLogic.ToRogueAsync(confirmanceNotifaction.Gufi).ConfigureAwait(false);
                            break;
                        default:
                            throw new Exception("Unexpected Case");
                    }
                }
            }
            catch (ConsumeException ex)
            {
                _logger.LogError($"ConsumeException ::: KafkaConsumer :: ProcessConsumedMessage() : ex ={ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception ::: KafkaConsumer :: ProcessConsumedMessage() : ex ={ex.Message}");
            }
        }
    }
}
