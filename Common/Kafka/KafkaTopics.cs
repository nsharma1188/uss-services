﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Kafka
{
    public class KafkaTopics
    {
        /// <summary>
        /// Kafka topics for conformance check.
        /// </summary>
        public const string ConformanceCheck = @"notify-conformance";

        /// <summary>
        /// Kafka topics for Radar, Collision Subscription check.
        /// </summary>
        public const string CheckSubscription = @"subscription";

        /// <summary>
        /// Kafka topics for uss creation notification
        /// </summary>
        public const string NotifyUssInstanceCreated = @"uss-created";

    }
}
