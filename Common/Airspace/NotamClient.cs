﻿using AnraUssServices.Data;
using AnraUssServices.Utm.Models;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common.Airspace
{
    public class NotamClient
    {
        private readonly ApplicationDbContext _context;
        private readonly DatabaseFunctions _dbFunctions;

        public NotamClient(ApplicationDbContext context, DatabaseFunctions dbFunctions)
        {
            _context = context;
            _dbFunctions = dbFunctions;
        }

        public List<Models.Notam> GetNotams(OperationVolume operationVolume)
        {                        
            var notams = _dbFunctions.ValidateOperationWithNotam(operationVolume.OperationGeography.ToNpgsqlPolygon(), operationVolume.EffectiveTimeBegin.Value, operationVolume.EffectiveTimeEnd.Value,operationVolume.MinAltitude.AltitudeValue,operationVolume.MaxAltitude.AltitudeValue).ToList();
            return notams;
        }

        public List<Models.ConstraintMessage> GetUVRs(OperationVolume operationVolume)
        {
            var uvrs = _dbFunctions.ValidateOperationWithUVR(operationVolume.OperationGeography.ToNpgsqlPolygon(), operationVolume.EffectiveTimeBegin.Value, operationVolume.EffectiveTimeEnd.Value, operationVolume.MinAltitude.AltitudeValue, operationVolume.MaxAltitude.AltitudeValue).ToList();
            return uvrs;
        }
    }
}