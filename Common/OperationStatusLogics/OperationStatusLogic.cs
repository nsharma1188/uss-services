﻿using AnraUssServices.Common.Billing;
using AnraUssServices.Common.InterUss;
using AnraUssServices.Common.Notification;
using AnraUssServices.Common.UtmMessaging;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Billing;
using AnraUssServices.ViewModel.Drones;
using AnraUssServices.ViewModel.Operations;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnraUssServices.Common.OperationStatusLogics
{
    public class OperationStatusLogic
    {
        private readonly IRepository<TelemetryMessage> telemetryRepo;
        private readonly IRepository<Models.Operation> operationRepository;
        private readonly IRepository<UtmInstance> utmInstanceRepository;
        private readonly IRepository<Drone> droneRepository;
        private readonly UtmMessenger utmMessenger;
        private readonly ILogger<OperationStatusLogic> logger;
        private readonly OperationMqttNotification operationMqttNotification;
        private readonly GridCell gridCellLogic;
		private OperationDocument OperationDocument { get; }
        private DroneDocument DroneDocument { get; }
		private UtmMessagesHelper utmMessagesHelper;
        private readonly AnraConfiguration configuration;
        private readonly SendNotification sendNotification;
		private readonly BillingHelper billingHelper;
        private readonly ViewerMqttNotification viewerMqttNotification;

        public OperationStatusLogic(IRepository<Models.Operation> operationRepository,
            IRepository<Drone> droneRepository,
            IRepository<UtmInstance> utmInstanceRepository,
            IRepository<TelemetryMessage> telemetryRepo,
            ILogger<OperationStatusLogic> logger,
            UtmMessenger utmMessenger,
            OperationDocument operationDocument,
            DroneDocument droneDocument,
            GridCell gridCellLogic,
            OperationMqttNotification operationMqttNotification,
            SendNotification sendNotification,
            AnraConfiguration configuration,
            UtmMessagesHelper utmMessagesHelper,
			BillingHelper billingHelper, ViewerMqttNotification viewerMqttNotification)
        {
            OperationDocument = operationDocument;
            DroneDocument = droneDocument;
            this.logger = logger;
            this.operationMqttNotification = operationMqttNotification;
            this.utmMessenger = utmMessenger;
            this.operationRepository = operationRepository;
            this.telemetryRepo = telemetryRepo;
            this.droneRepository = droneRepository;
            this.gridCellLogic = gridCellLogic;
            this.sendNotification = sendNotification;
            this.utmInstanceRepository = utmInstanceRepository;
			this.utmMessagesHelper = utmMessagesHelper;
            this.configuration = configuration;
			this.billingHelper = billingHelper;
            this.viewerMqttNotification = viewerMqttNotification;
        }

        public ObjectResult ToProposed(Models.Operation operation)
        {
            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = @"Operation Proposed/" + operation.Gufi });
        }

        public ObjectResult ToProposed(Guid gufi)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }

            var lastState = model.State;

            if (model.State != EnumUtils.GetDescription(OperationState.PROPOSED))
            {
                model.State = EnumUtils.GetDescription(OperationState.PROPOSED);
                model.DecisionTime = DateTime.UtcNow;
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) state reset to proposed."});
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change state.Operation({model.Gufi}) already in {model.State} state."});
            }
        }

        public async Task<ObjectResult> ToProposedAsync(Guid gufi, List<JSendSlippyResponseDataGridCells> gridData)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }
            
            model.DecisionTime = DateTime.UtcNow;
            model.SlippyTileData = JsonConvert.SerializeObject(gridData);
            operationRepository.Update(model);

            //Current Mqtt Notification
            NotifyOperator(model);

            //New Mqtt Notification for Operation
            NotifyOperation(model, MqttMessageType.STATUS.ToString());

            await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) in proposed state due to conflict."});
        }

        public async Task<ObjectResult> ToReadyForAtcAsync(Guid gufi, List<JSendSlippyResponseDataGridCells> gridData)
        {
            //var model = operationRepository.Find(gufi.ToString());

            //if (model == null || !model.IsInternalOperation)
            //{
            //    return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            //}

            //model.DecisionTime = DateTime.UtcNow;
            //model.SlippyTileData = JsonConvert.SerializeObject(gridData);
            //model.State = EnumUtils.GetDescription(OperationState.READYFORATC);
            //operationRepository.Update(model);

            //NotifyOperator(model);

            //await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

            //return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) Ready For ATC." });

            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }

            var lastState = model.State;

            if (model.State == EnumUtils.GetDescription(OperationState.PROPOSED))
            {
                model.State = EnumUtils.GetDescription(OperationState.READYFORATC);
                model.DecisionTime = DateTime.UtcNow;
                model.SlippyTileData = JsonConvert.SerializeObject(gridData);
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToReadyForAtcAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) Ready For ATC." });
            }

            if (model.State == EnumUtils.GetDescription(OperationState.ACTIVATED))
            {
                model.DecisionTime = DateTime.UtcNow;
                model.SlippyTileData = JsonConvert.SerializeObject(gridData);
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToReadyForAtcAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) updated." });
            }

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to accepted." });
        }

        public async Task<ObjectResult> ToActivateAsync(Guid gufi)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });

            var uss = utmInstanceRepository.Find(model.UssInstanceId);

            if (model.State != EnumUtils.GetDescription(OperationState.ACTIVATED) && model.State != EnumUtils.GetDescription(OperationState.ROGUE) && model.State != EnumUtils.GetDescription(OperationState.CLOSED))
            {
                var lastState = model.State;
                model.State = EnumUtils.GetDescription(OperationState.ACTIVATED);
                model.DecisionTime = DateTime.UtcNow;
                operationRepository.Update(model);
                 
                if (lastState.Equals(OperationState.NONCONFORMING.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    var utmMessage = utmMessagesHelper.GetConfirmingOperationUtmMessage(model, GetLastKnownPosition(gufi),uss);

                    await utmMessenger.NotifyOperationStatusToLUN(utmMessage, gridCellLogic.GetGridCellOperators(model.SlippyTileData));
                }

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) activated."});
            }

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to activated."});
        }

        public async Task<ObjectResult> ToAcceptedAsync(Guid gufi, List<JSendSlippyResponseDataGridCells> gridData)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }

            var lastState = model.State;

			if (model.State == EnumUtils.GetDescription(OperationState.PROPOSED))
            {
                model.State = EnumUtils.GetDescription(OperationState.ACCEPTED);                
                model.DecisionTime = DateTime.UtcNow;
                model.SlippyTileData = JsonConvert.SerializeObject(gridData);
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToAcceptedAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) accepted."});
            }

            if (model.State == EnumUtils.GetDescription(OperationState.ACTIVATED))
            {
                model.DecisionTime = DateTime.UtcNow;
                model.SlippyTileData = JsonConvert.SerializeObject(gridData);
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());  

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToAcceptedAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) updated." });
            }

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to accepted."});
        }

        public async Task<ObjectResult> ToAcceptedAsync(Guid gufi, string atcComments = "NA")
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });
            }

            var lastState = model.State;

            if (model.State == EnumUtils.GetDescription(OperationState.READYFORATC))
            {
                model.State = EnumUtils.GetDescription(OperationState.ACCEPTED);
                model.ATCComments = atcComments;
                model.DecisionTime = DateTime.UtcNow;                
                operationRepository.Update(model);

                //Current Mqtt notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToAcceptedAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                await utmMessenger.NotifyOperationToLUN(model, gridCellLogic.GetGridCellOperators(model.SlippyTileData), true);

                var message = JsonConvert.SerializeObject(model.Adapt<Utm.Models.Operation>());

                StringBuilder SbBody = new StringBuilder();
                SbBody.Append("Dear ATC Admin, <br/>");
                SbBody.Append("The following operation has been approved. <br/><br/>");
                SbBody.Append("<b>OPERATION GUFI:</b><br/>");
                SbBody.Append(model.Gufi.ToString() + "<br/><br/>");
                SbBody.Append("<b>OPERATION STATE:</b><br/>");
                SbBody.Append(model.State + "<br/><br/>");
                SbBody.Append("<b>DETAILS (RAW):</b><br/>");
                SbBody.Append(message);
                SbBody.Append("<br/><br/><br/>");
                SbBody.Append("<b>Regards,</b><br/>");
                SbBody.Append("ANRA Notification Manager");
                SbBody.Append("<hr/>");

                sendNotification.SendEmail(configuration.ATCSupervisorEmail, SbBody.ToString(), "ATC UAS Operation Action");

				//Calling EVERIS endpoint for operationevent related work for billing purpose
				var operationEventModel = new OperationEvent()
				{
					EventTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"),
					EventType = EventTypeEnum.ISSUED,
					Gufi = model.Gufi,
					UssInstanceId = model.UssInstanceId,
					UserId = model.UserId
				};
				billingHelper.PostOperationEvent(operationEventModel);


				return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) accepted." });
            }

            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to accepted." });
        }

        public ObjectResult ToNonConforming(Guid gufi)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });

            var uss = utmInstanceRepository.Find(model.UssInstanceId);

            var lastState = model.State;

            if (model.State == EnumUtils.GetDescription(OperationState.ACCEPTED) || model.State == EnumUtils.GetDescription(OperationState.ACTIVATED))
            {
                model.State = EnumUtils.GetDescription(OperationState.NONCONFORMING);
                model.DecisionTime = DateTime.UtcNow;
                operationRepository.Update(model);

                //Current mqtt notification
				NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToNonConforming :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                //Notify LUN
                var utmMessage = utmMessagesHelper.GetNonConfirmingOperationUtmMessage(model, GetLastKnownPosition(gufi), uss);
                utmMessenger.NotifyOperationStatusToLUN(utmMessage, gridCellLogic.GetGridCellOperators(model.SlippyTileData));

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) Non-confirming."});
            }
            return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to non-confirming."});
        }

        public async Task<ObjectResult> ToClosedAsync(Guid gufi,string atcComments = "NA")
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });

            var uss = utmInstanceRepository.Find(model.UssInstanceId);

            if (uss == null)
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Uss not found.Uss Instance - {model.UssInstanceId.ToString()}" });

            var lastState = model.State;

			if (model.State != EnumUtils.GetDescription(OperationState.CLOSED))
            {
                model.State = EnumUtils.GetDescription(OperationState.CLOSED);
                model.ATCComments = atcComments;
                model.DecisionTime = DateTime.UtcNow;
                operationRepository.Update(model);

                //Change Logic Due to Sprint3 - Since Operation With PROPOSED state is also written to grid
                //So PROPOSED operation should also be closed
                //Also below logic will execute if the operation is written in grid

                if (!string.IsNullOrEmpty(model.SlippyTileData))
                {
                    var utmMessage = utmMessagesHelper.GetClosedOperationUtmMessage(model, GetLastKnownPosition(gufi), uss, lastState);
                    await utmMessenger.NotifyOperationStatusToLUN(utmMessage, gridCellLogic.GetGridCellOperators(model.SlippyTileData));
                    gridCellLogic.DeleteGridCellOperation(model.SlippyTileData, model.Gufi);
                }

                //Current Mqtt notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                //New Mqtt Notification for viewer
                viewerMqttNotification.NotificationToViewer(MqttMessageType.CLOSED.ToString(), model.Adapt<OperationViewModel>(), MqttViewerType.OPERATION);

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToClosedAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                if (lastState.Equals(EnumUtils.GetDescription(OperationState.READYFORATC)))
				{
					var message = JsonConvert.SerializeObject(model.Adapt<Utm.Models.Operation>());

					StringBuilder SbBody = new StringBuilder();
					SbBody.Append("Dear ATC Admin, <br/>");
					SbBody.Append("The following operation has been rejected. <br/><br/>");
					SbBody.Append("<b>OPERATION GUFI:</b><br/>");
					SbBody.Append(model.Gufi.ToString() + "<br/><br/>");
					SbBody.Append("<b>OPERATION STATE:</b><br/>");
					SbBody.Append(model.State + "<br/><br/>");
					SbBody.Append("<b>DETAILS (RAW):</b><br/>");
					SbBody.Append(message);
					SbBody.Append("<br/><br/><br/>");
					SbBody.Append("<b>Regards,</b><br/>");
					SbBody.Append("ANRA Notification Manager");
					SbBody.Append("<hr/>");

					sendNotification.SendEmail(configuration.ATCSupervisorEmail, SbBody.ToString(), "ATC UAS Operation Action");

					//Calling EVERIS endpoint for operationevent related work for billing purpose
					//This is the case when the ATC cancelled or rejected the operations.
					var operationEventModel = new OperationEvent()
					{
						EventTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"),
						EventType = EventTypeEnum.CANCELED_ATC,
						Gufi = model.Gufi,
						UssInstanceId = model.UssInstanceId,
						UserId = model.UserId
					};
					billingHelper.PostOperationEvent(operationEventModel);
				}
				else
				{
					//This is the case for the completed operations and user closed the operation 
					//Calling EVERIS endpoint for operationevent related work for billing purpose
					var operationEventModel = new OperationEvent()
					{
						EventTime = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ"),
						EventType = EventTypeEnum.FINISHED,
						Gufi = model.Gufi,
						UssInstanceId = model.UssInstanceId,
						UserId = model.UserId
					};
					billingHelper.PostOperationEvent(operationEventModel);
				}

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) closed."});
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to closed."});
            }
        }

        public async Task<ObjectResult> ToRogueAsync(Guid gufi)
        {
            var model = operationRepository.Find(gufi);

            if (model == null || !model.IsInternalOperation)
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = @"Operation not found." });

            var uss = utmInstanceRepository.Find(model.UssInstanceId);

            var lastState = model.State;
            if (model.State == EnumUtils.GetDescription(OperationState.NONCONFORMING) || model.State == EnumUtils.GetDescription(OperationState.ACTIVATED))
            {
                model.State = EnumUtils.GetDescription(OperationState.ROGUE);
                model.DecisionTime = DateTime.UtcNow;
                operationRepository.Update(model);

                //Current Mqtt Notification
                NotifyOperator(model);

                //New Mqtt Notification for Operation
                NotifyOperation(model, MqttMessageType.STATUS.ToString());

                #if DEBUG
                logger.LogInformation("OperationStatusLogic ::: ToRogueAsync :: Operation : {0}", JsonConvert.SerializeObject(model, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects }));
                #endif

                var utmMessage = utmMessagesHelper.GetRogueOperationUtmMessage(model, GetLastKnownPosition(gufi), uss);
                await utmMessenger.NotifyOperationStatusToLUN(utmMessage, gridCellLogic.GetGridCellOperators(model.SlippyTileData));

                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 200, Message = $"Operation({model.Gufi}) Rogue."});
            }
            else
            {
                return new ObjectResult(new UTMRestResponse { HttpStatusCode = 400, Message = $"Cannot change operation({model.Gufi}) state from {model.State} to rogue."});
            }
        }

        private void NotifyOperator(Models.Operation operation)
        {
            operationMqttNotification.Notify(operation, operation.State);
        }

        public void NotifyOperator(Models.Operation operation, string message)
        {
            operationMqttNotification.Notify(operation.Adapt<Models.Operation>(), message);
        }

        private async void NotifyPositionToLUNAsync(Models.Operation operation, string lastState)
        {
            var uss = utmInstanceRepository.Find(operation.UssInstanceId);

            var lastKnownPosition = telemetryRepo.GetAll().Where(p => p.Gufi.Equals(operation.Gufi.ToString())).OrderByDescending(p => p.TimeMeasured).FirstOrDefault().Adapt<Position>();

            var utmMessage = utmMessagesHelper.GetClosedOperationUtmMessage(operation, lastKnownPosition, uss, lastState);

            await utmMessenger.NotifyPositionToLUN(lastKnownPosition, operation.Adapt<Utm.Models.Operation>(), null, true);
        }

        public Position GetLastKnownPosition(Guid gufi)
        {
            return telemetryRepo.GetAll().Where(p => p.Gufi.Equals(gufi.ToString())).OrderByDescending(p => p.TimeMeasured).FirstOrDefault().Adapt<Position>();
        }

        public async void BulkActivateAsync(Guid[] operationIds)
        {
            #if DEBUG
                logger.LogInformation("BulkActivate {0}", operationIds);
            #endif

            foreach (var operationId in operationIds)
            {
                await ToActivateAsync(operationId);
            }

            if (operationIds.Any())
            {
                var operations = operationRepository.GetAll().Where(p => p.IsInternalOperation && operationIds.Contains(p.Gufi));
                OperationDocument.AddMany(operations.Adapt<List<OperationViewModel>>());

                var droneUids = operations.SelectMany(p => p.UasRegistrations.Select(x => x.RegistrationId.Value)).ToList();
                var drones = droneRepository.GetAll().Where(d => droneUids.Contains(d.Uid)).Adapt<List<DroneViewModel>>();
                DroneDocument.AddMany(drones);
            }
        }

        public async void BulkCloseAsync(Guid[] operationIds)
        {
            #if DEBUG
                logger.LogInformation("BulkClose {0}", operationIds);
            #endif

            foreach (var operationId in operationIds)
            {
                await ToClosedAsync(operationId);
            }
        }

        /// <summary>
        /// Operation related Notification
        /// </summary>
        public void NotifyOperation(Models.Operation operation, string mqttMessageType, string message = null)
        {
            var notification = new OperationNotification
            {
                Gufi = operation.Gufi,
                Message = message,
                Status = operation.State.ToString(),
                Timestamp = DateTime.UtcNow
            };

            operationMqttNotification.NotifyOperation(notification, mqttMessageType, operation.Gufi.ToString());
        }

        ///// <summary>
        ///// Operation Delete Notification
        ///// </summary>
        ///// <param name="gufi"></param>
        ///// <param name="mqttMessageType"></param>
        //public void NotifyOperationDelete(Guid gufi, string mqttMessageType)
        //{
        //    var notification = new OperationDeleteNotification
        //    {
        //        Gufi = gufi,
        //        IsDeleted = true,
        //        Timestamp = DateTime.UtcNow
        //    };

        //    operationMqttNotification.NotifyOperation(notification, mqttMessageType, gufi.ToString());
        //}
    }
}