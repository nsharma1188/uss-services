﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.AsyncHandlers;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using GeoAPI.Geometries;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using Newtonsoft.Json;

namespace AnraUssServices.Common
{
	public class ReplanGeographyHelper
	{
		private readonly IRepository<Models.Operation> operationRepository;
		private readonly VolumeSplitHelper VolumeSplitHelper;
		private readonly DatabaseFunctions databaseFunctions;
		private readonly IMediator mediator;
		private readonly IRepository<Models.NegotiationAgreement> negotiationRepository;
		private readonly ILogger<string> logger;

		public ReplanGeographyHelper(IRepository<Models.Operation> operationRepository, VolumeSplitHelper VolumeSplitHelper,
			DatabaseFunctions databaseFunctions, IMediator mediator, IRepository<Models.NegotiationAgreement> negotiationRepository,
			ILogger<string> logger)
		{
			this.operationRepository = operationRepository;
			this.VolumeSplitHelper = VolumeSplitHelper;
			this.databaseFunctions = databaseFunctions;
			this.mediator = mediator;
			this.negotiationRepository = negotiationRepository;
			this.logger = logger; 
		}

		public UTMRestResponse GetReplanGeography(Utm.Models.NegotiationMessage negotiation_message)
		{

			var response = new UTMRestResponse();
			IGeometry newReplanGeography = null;

			try
			{
				var conflictingOperation = operationRepository.Find(negotiation_message.GufiOfOriginator).Adapt<Utm.Models.Operation>();
				var currentOperation = operationRepository.Find(negotiation_message.GufiOfReceiver).Adapt<Utm.Models.Operation>();

				if (conflictingOperation.OperationVolumes.Select(x => x.VolumeType).FirstOrDefault().ToString() != "ABOV" &&
					currentOperation.OperationVolumes.Select(x => x.VolumeType).FirstOrDefault().ToString() != "ABOV")
				{
					response.HttpStatusCode = StatusCodes.Status409Conflict;
					response.Message = "Automatic Replan is not Successfull";
					return response;
				}

				var conflictingOperationVolume= conflictingOperation.OperationVolumes.Select(x => x.OperationGeography).FirstOrDefault();
				var currentOperationVolume = currentOperation.OperationVolumes.Select(x => x.OperationGeography).FirstOrDefault();

				var newConflictingOperationPolygon = GetNetTopologyPolygon(conflictingOperationVolume);
				var newCurrentOperationPolygon = GetNetTopologyPolygon(currentOperationVolume);

				var currentOperationArea = newCurrentOperationPolygon.Area;
				var intersectingArea = newCurrentOperationPolygon.Intersection(newConflictingOperationPolygon).Area;
				var intersectionPercentage = (intersectingArea / currentOperationArea) * 100;

				if (intersectionPercentage <= 20)
				{
					//Replan automatically and send 204

					newReplanGeography = newCurrentOperationPolygon.Difference(newConflictingOperationPolygon);

					if (newReplanGeography.GeometryType == "MultiPolygon")
					{
						var polygonList = databaseFunctions.GetPolygon(newReplanGeography.ToString());
						newReplanGeography = GetLargerPolygon(polygonList);
						if (((newReplanGeography.Area / currentOperationArea) * 100) < 80)
						{
							response.HttpStatusCode = StatusCodes.Status409Conflict;
							response.Message = "Automatic Replan is not Successfull";
							return response;
						}
					}

					var polygon = (NetTopologySuite.Geometries.Polygon)newReplanGeography;
					var newOperationVolume = VolumeSplitHelper.ToGeoJsonString(polygon);
					HandelUpdatedOperation(currentOperation, newOperationVolume, negotiation_message);
					response.HttpStatusCode = StatusCodes.Status204NoContent;
					response.Message = "Able to Replan Successfully";
				}
				else
				{
					response.HttpStatusCode = StatusCodes.Status409Conflict;
					response.Message = "Automatic Replan is not Successfull";
				}
			}
			catch (Exception ex)
			{
				response.HttpStatusCode = StatusCodes.Status409Conflict;
				response.Message = "Automatic Replan is not Successfully";
				logger.LogError("Exception :: ReplanGeographyHelper : Error: {0}",ex.ToString());
			}
			return response;
		}


		public void HandelUpdatedOperation(Utm.Models.Operation currentOperation, string newOperationVolume, Utm.Models.NegotiationMessage negotiation_message)
		{
			var newOperationGeography = JsonConvert.DeserializeObject<Utm.Models.Polygon>(newOperationVolume);
			var currentOperationVolumes = currentOperation.OperationVolumes.FirstOrDefault();
			var currentOperationVolumesList = new List<Utm.Models.OperationVolume>();

			currentOperationVolumesList.Add(new Utm.Models.OperationVolume()
			{
				ActualTimeEnd = currentOperationVolumes.ActualTimeEnd,
				BeyondVisualLineOfSight = currentOperationVolumes.BeyondVisualLineOfSight,
				EffectiveTimeBegin = currentOperationVolumes.EffectiveTimeBegin,
				EffectiveTimeEnd = currentOperationVolumes.EffectiveTimeEnd,
				MaxAltitude = currentOperationVolumes.MaxAltitude,
				MinAltitude = currentOperationVolumes.MinAltitude,
				NearStructure = currentOperationVolumes.NearStructure,
				OperationGeography = newOperationGeography,
				Ordinal = currentOperationVolumes.Ordinal,
				VolumeType = currentOperationVolumes.VolumeType
			});
			currentOperation.OperationVolumes = currentOperationVolumesList;

			var negotiationAgreementList = new List<Utm.Models.NegotiationAgreement>();

			var negotiationAgreement = new Utm.Models.NegotiationAgreement()
			{
				DiscoveryReference = negotiation_message.DiscoveryReference,
				FreeText = negotiation_message.FreeText,
				GufiOriginator = negotiation_message.GufiOfOriginator,
				GufiReceiver = negotiation_message.GufiOfReceiver,
				MessageId = negotiation_message.MessageId,
				NegotiationId = negotiation_message.NegotiationId,
				Type = (negotiation_message.Type.ToString().Contains("REPLAN_REQUESTED") || negotiation_message.Type.ToString().Contains("REPLAN_CANCELLED")) ? NegotiationAgreementTypeEnum.REPLAN : NegotiationAgreementTypeEnum.INTERSECTION,
				UssName = negotiation_message.UssNameOfOriginator,
				UssNameOfOriginator = negotiation_message.UssNameOfOriginator,
				UssNameOfReceiver = currentOperation.UssName
			};

			negotiationAgreementList.Add(negotiationAgreement);
			currentOperation.NegotiationAgreements = negotiationAgreementList;
			var existingOperation = operationRepository.Find(currentOperation.Gufi);
			existingOperation = currentOperation.Adapt<Models.Operation>();
			operationRepository.Update(existingOperation);

			mediator.Send(new AsyncOperationProposedNotification(existingOperation.Adapt<Utm.Models.Operation>()));
		}

		/// <summary>
		/// Creating NetTopology polygon using the current polygon coordinates
		/// </summary>
		public IGeometry GetNetTopologyPolygon(Utm.Models.Polygon currentPolygon)
		{
			var polygonCoordinates = currentPolygon.Coordinates.FirstOrDefault();

			var newCoordinates = new Coordinate[polygonCoordinates.Count];

			for (int i = 0; i < polygonCoordinates.Count; i++)
			{
				newCoordinates[i] = new Coordinate(polygonCoordinates[i][0].Value, polygonCoordinates[i][1].Value);
			}

			IGeometry polygon = new NetTopologySuite.Geometries.Polygon(new LinearRing(newCoordinates));

			return polygon;
		}

		/// <summary>
		/// For the case when the current operation is splitting into two polygon by the conflicting operation
		/// </summary>
		public IGeometry GetLargerPolygon(List<Utm.Models.Polygon> polygonList)
		{
			var firstPolygon = polygonList[0];
			var secondPolygon = polygonList[1];

			var netTopologyFirstPolygon = GetNetTopologyPolygon(firstPolygon);
			var netTolologySecondPolygon = GetNetTopologyPolygon(secondPolygon);

			return netTopologyFirstPolygon.Area > netTolologySecondPolygon.Area ? netTopologyFirstPolygon: netTolologySecondPolygon;

		}
	}
}
