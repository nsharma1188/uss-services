﻿using AnraUssServices.Utm.Models;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class GisHelper
    {

        private static ConflictDetail Get4DIntersectionDetails(OperationVolume operationVol, OperationVolume existingOperationVol)        
        {
            var conflictDetail = new ConflictDetail();

            bool isGeographyIntersecting = false;
            bool isAltitudeIntersecting = false;
            bool isTimeIntersecting = false;

            //Geography Check            
            isGeographyIntersecting = CommonValidator.IsPolygonIntersecting(operationVol.OperationGeography, existingOperationVol.OperationGeography);

            //Altitude Check
            isAltitudeIntersecting = (operationVol.MaxAltitude.AltitudeValue > existingOperationVol.MinAltitude.AltitudeValue && operationVol.MinAltitude.AltitudeValue <= existingOperationVol.MaxAltitude.AltitudeValue);

            //Time Check
            isTimeIntersecting = CommonValidator.IsTemporalIntersection(existingOperationVol.EffectiveTimeEnd.Value, operationVol.EffectiveTimeBegin.Value);            

            conflictDetail.SourceOrdinal = operationVol.Ordinal.Value;
            conflictDetail.TargetOrdinal = existingOperationVol.Ordinal.Value;
            conflictDetail.IsIntersectingIn4D = isGeographyIntersecting && isAltitudeIntersecting && isTimeIntersecting;

            return conflictDetail;
        }
        
        public static List<KeyValuePair<Operation, List<ConflictDetail>>> GetIntersectingOperations(Operation plannedOp,List<Operation> externalOps)
        {
            var intersectingOperations = new List<KeyValuePair<Operation, List<ConflictDetail>>>();
            

            //Check For 4D Intersections With Submitted Operation
            if (externalOps.Any())
            {
                //Loop each existing Operation
                externalOps.ForEach(externalOperation =>
                {
                    var reasons = new List<ConflictDetail>();

                    //Loop each volume
                    externalOperation.OperationVolumes.ForEach(externalOperationVOlume =>
                    {
                        //loop submitted operation volumes and try to check for intersection
                        plannedOp.OperationVolumes.ForEach(ov =>
                        {
                            var reason = Get4DIntersectionDetails(ov, externalOperationVOlume);

                            if(reason.IsIntersectingIn4D)
                                reasons.Add(reason);                            
                        });
                    });

                    if(reasons.Any())
                        intersectingOperations.Add(new KeyValuePair<Operation, List<ConflictDetail>>(externalOperation,reasons));
                });
            }

            return intersectingOperations;
        }
    }

    public class ConflictDetail
    {
        public int SourceOrdinal { get; set; }
        public int TargetOrdinal { get; set; }
        public bool IsIntersectingIn4D { get; set; }
    }
}