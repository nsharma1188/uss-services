﻿using AnraUssServices.Utm.Models;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace AnraUssServices.Common
{
    public class CommonValidator
    {
        public static bool IsUUIDv4(Guid? guidVal)
        {
            Regex UUIDv4Regex = new Regex(@"^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$", RegexOptions.IgnoreCase);

            if (guidVal.HasValue)
            {
                return UUIDv4Regex.IsMatch(guidVal.Value.ToString());
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidEnum(Type enumType, object enumVal)
        {
            return Enum.IsDefined(enumType, enumVal);
        }

        public static bool IsAltitudeOverlapping(double minAlt1, double maxAlt1, double minAlt2, double maxAlt2)
        { 
            return (maxAlt1 > minAlt2 && minAlt1 <= maxAlt2);
            //return (minAlt1 == minAlt2 && maxAlt1 == maxAlt2) ? false : (maxAlt1 > minAlt2 && minAlt1 <= maxAlt2);
        }

        public static bool IsTemporalIntersection(DateTime precedingVolumeEndTime, DateTime succeedingVolumeStartTime)
        {
            return precedingVolumeEndTime.Subtract(succeedingVolumeStartTime).TotalMinutes > 0;
        }

        public static bool AreVolumesTimeInCorrectOrder(OperationVolume precedingVolume, OperationVolume succeedingVolume)
        {
            var result = 
                (succeedingVolume.EffectiveTimeBegin.Value.Ticks - precedingVolume.EffectiveTimeBegin.Value.Ticks  >= 0 
                    &&
                succeedingVolume.EffectiveTimeEnd.Value.Ticks - precedingVolume.EffectiveTimeBegin.Value.Ticks >= 0)
                ||
                (succeedingVolume.EffectiveTimeEnd.Value.Ticks - precedingVolume.EffectiveTimeBegin.Value.Ticks >= 0
                    &&
                succeedingVolume.EffectiveTimeEnd.Value.Ticks - precedingVolume.EffectiveTimeEnd.Value.Ticks >= 0);

            return result;
        }

        public static bool IsPolygonIntersecting(Utm.Models.Polygon succeedingPolygon, Utm.Models.Polygon precedingPolygon)
        {
            string succeedingPolygonJson = JsonConvert.SerializeObject(succeedingPolygon);
            string precedingPolygonJson = JsonConvert.SerializeObject(precedingPolygon);

            if (string.IsNullOrEmpty(succeedingPolygonJson) || string.IsNullOrEmpty(precedingPolygonJson))
            {
                return false;
            }
            else
            {
                string succeedingPolygonGeoJson = succeedingPolygonJson.Replace("'", "\"");
                string precedingPolygonGeoJson = precedingPolygonJson.Replace("'", "\"");


                var geoJsonReader = new GeoJsonReader();

                try
                {
                    var objSucceedingPolygon = geoJsonReader.Read<NetTopologySuite.Geometries.Polygon>(succeedingPolygonGeoJson);
                    var objPrecedingPolygon = geoJsonReader.Read<NetTopologySuite.Geometries.Polygon>(precedingPolygonGeoJson);

                    //Check if a valid geometry
                    if (objSucceedingPolygon.IsValid && objPrecedingPolygon.IsValid)
                    {
                        return objSucceedingPolygon.Intersects(objPrecedingPolygon);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
    }
}