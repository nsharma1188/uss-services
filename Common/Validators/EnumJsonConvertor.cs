﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace AnraUssServices.Common.Validators
{
    public class EnumJsonConvertor : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            Type type = IsNullableType(objectType) ? Nullable.GetUnderlyingType(objectType) : objectType;
            return type.IsEnum;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
			try
			{
				bool isNullable = IsNullableType(objectType);
				Type enumType = isNullable ? Nullable.GetUnderlyingType(objectType) : objectType;

				string[] names = Enum.GetNames(enumType);

				if (reader.TokenType == JsonToken.String)
				{
					string enumText = reader.Value.ToString();

					if (!string.IsNullOrEmpty(enumText))
					{
						string match = names
							.Where(n => string.Equals(n, enumText, StringComparison.OrdinalIgnoreCase))
							.FirstOrDefault();

						if (match != null)
						{
							return Enum.Parse(enumType, match);
						}
						else
						{
							throw new JsonSerializationException("Invalid enum input");
						}
					}
				}
				else if (reader.TokenType == JsonToken.Integer)
				{
					throw new JsonSerializationException("Invalid enum input");
				}

				return null;
			}
			catch (Exception ex)
			{
				return null;
			}
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }

        private bool IsNullableType(Type t)
        {
            return (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
    }
}