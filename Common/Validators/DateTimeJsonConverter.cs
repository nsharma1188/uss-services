﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace AnraUssServices.Common.Validators
{
    public class DateTimeJsonConverter : DateTimeConverterBase
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Date)
                return reader.Value;

            if (DateTime.TryParseExact(reader.Value.ToString(), "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.CurrentCulture, DateTimeStyles.AssumeUniversal, out DateTime dt))
            {
                return dt.ToUniversalTime();
            }
            else
            {
                throw new Exception($"Invalid DateTime {reader.Value}.");
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }
    }
}