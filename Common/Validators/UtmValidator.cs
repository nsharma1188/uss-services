﻿using AnraUssServices.Common.InterUss;
using AnraUssServices.Models;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.UtmInstances;
using Mapster;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class UtmValidator
    {        
        private List<string> _errors;
        private readonly SlippyTiles slippyTiles;
        private readonly InterUssApi interUssApi;
        private AnraConfiguration _config;
        private readonly IRepository<Models.Operation> operationRepository;


        public List<string> Errors
        {
            get { return _errors; }
        }

        public UtmValidator(
            SlippyTiles slippyTiles,
            InterUssApi interUssApi, 
            AnraConfiguration configuration, 
            IRepository<Models.Operation> operationRepository)
        {
            _errors = new List<string>();
            this.slippyTiles = slippyTiles;
            this.interUssApi = interUssApi;
            _config = configuration;
            this.operationRepository = operationRepository;
        }

        public bool IsValid(UtmInstanceItem utmInstance,bool isEdit = false)
        {
            //Check for Edit Uss case
            if (isEdit)
            {                
                var anyActiveOperations = operationRepository.GetAll().Any(p => p.UssInstanceId == utmInstance.UssInstanceId && p.IsInternalOperation && p.State != OperationState.CLOSED.ToString());

                if (anyActiveOperations && !Utilities.ArePolygonsSame(utmInstance.CoverageArea.Adapt<Polygon>(), utmInstance.CoverageArea))
                {
                    _errors.Add("Change in coverage area found. Uss can't update as open operation exists for the USS.");
                }
            }            

            //Check for valid Uss Instance Time
            var isValidUSSInstance = utmInstance.TimeAvailableEnd.Subtract(utmInstance.TimeAvailableBegin).TotalHours >= 1;

            if (!(utmInstance.TimeAvailableEnd.Subtract(utmInstance.TimeAvailableBegin).TotalHours >= 1))
			{
				_errors.Add("A USS Instance must have a total active time of at least one hour");
			}

            //Validate Uss Grid Data
            if (_config.IsUssGridValidationEnabled)
            {
                ValidateUssGridData(utmInstance.CoverageArea);
            }                

            return _errors.Count() <= 0;
        }

        void ValidateUssGridData(Polygon ussCoverageArea)
        {
            var gridCellsData = slippyTiles.GetSlippyTileInfo(ussCoverageArea);

            var responses = new List<JSendGridCellMetadataResponse>();
            foreach (var gridCell in gridCellsData)
            {
                var gridCellMetadataResponse = interUssApi.GetGridCellOperatorAsync(gridCell.Zoom, gridCell.X, gridCell.Y);

                if (gridCellMetadataResponse != null && gridCellMetadataResponse.Status.Equals("success"))
                {
                    List<GridCellOperatorResponse> operators = gridCellMetadataResponse.Data.Operators;

                    if (operators.Any() && operators.Select(x => x.Uss.Equals(_config.ClientId)).FirstOrDefault().Equals(true))
                    {
                        _errors.Add(string.Format("Uss instance already exists in grid({0}/{1}/{2})", gridCell.X, gridCell.Y, gridCell.Zoom));
                        return;
                    }
                }
            }
        }
    }
}