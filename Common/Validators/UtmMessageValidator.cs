﻿using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class UtmMessageValidator
    {
        private readonly IRepository<Models.Operation> operationRepository;
        private List<string> _errors;

		public List<string> Errors
        {
            get { return _errors; }
        }

        public UtmMessageValidator(IRepository<Models.Operation> operationRepository)
        {
            _errors = new List<string>();
            this.operationRepository = operationRepository;
        }

        public bool IsValid(UTMMessage utmMessage ,  Guid message_id = new Guid())
        {
			if (message_id != utmMessage.MessageId)
			{
				_errors.Add("The message_id provided did not match with the UTMMessage message_id");
			}

			if ((utmMessage.MessageType.Equals(MessageTypeEnum.UNPLANNED_LANDING) ||
				utmMessage.MessageType.Equals(MessageTypeEnum.UNCONTROLLED_LANDING) ||
				utmMessage.MessageType.Equals(MessageTypeEnum.CONTINGENCY_PLAN_INITIATED)) &&
				(!utmMessage.Severity.Equals(Severity.CRITICAL) && !utmMessage.Severity.Equals(Severity.ALERT)))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be either ALERT or CRITICAL.");
			}

			if (utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_NONCONFORMING) &&
				(int)utmMessage.Severity < (int)Severity.CRITICAL)
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be either EMERGENCY or ALERT or CRITICAL .");
			}

			if (utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_ROGUE) &&
				(int)utmMessage.Severity < (int)Severity.ALERT)
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be either EMERGENCY or ALERT.");
			}

			if (utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_CLOSED) &&
				(!utmMessage.Severity.Equals(Severity.CRITICAL) && !utmMessage.Severity.Equals(Severity.NOTICE)))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be either NOTICE or CRITICAL.");
			}

			if ((utmMessage.MessageType.Equals(MessageTypeEnum.CONTINGENCY_PLAN_CANCELLED) ||
				utmMessage.MessageType.Equals(MessageTypeEnum.UNAUTHORIZED_AIRSPACE_ENTRY)) && !utmMessage.Severity.Equals(Severity.CRITICAL))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be CRITICAL.");
			}

			if ((utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_START) ||
			   utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_END) ||
			   utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_CONFORMING)) && !utmMessage.Severity.Equals(Severity.NOTICE))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be NOTICE.");
			}

			if (utmMessage.MessageType.Equals(MessageTypeEnum.UNAUTHORIZED_AIRSPACE_PROXIMITY) && !utmMessage.Severity.Equals(Severity.WARNING))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be WARNING");
			}

			if (utmMessage.MessageType.Equals(MessageTypeEnum.OTHER_SEE_FREE_TEXT) &&
				(!utmMessage.Severity.Equals(Severity.ALERT) && !utmMessage.Severity.Equals(Severity.INFORMATIONAL)))
			{
				_errors.Add("MessageType: " + utmMessage.MessageType.ToString() + " :: severity must be either ALERT or INFORMATIONAL");
			}

			if ((utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_START) ||
                utmMessage.MessageType.Equals(MessageTypeEnum.PERIODIC_POSITION_REPORTS_END))
                && (utmMessage.Gufi.Equals(Guid.Empty) || utmMessage.Gufi == null))
            {
                _errors.Add("UTMMessage.gufi required because messageType: " + utmMessage.MessageType.ToString() + " requires gufi to be provided.");
            }

            if ((utmMessage.MessageType.Equals(MessageTypeEnum.UNPLANNED_LANDING) ||
                utmMessage.MessageType.Equals(MessageTypeEnum.UNCONTROLLED_LANDING) ||
                utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_NONCONFORMING) ||
                utmMessage.MessageType.Equals(MessageTypeEnum.OPERATION_ROGUE)) && (utmMessage.Gufi == Guid.Empty || utmMessage.Gufi == null))
            {
                _errors.Add("UTMMessage.gufi required because messageType: " + utmMessage.MessageType.ToString() + " requires gufi to be provided.");
            }

            return _errors.Count() <= 0;
        }

        public bool IsValidRequest(UTMMessage utmMessage)
        {
            var operation = operationRepository.Find(utmMessage.Gufi);

            return operation != null && (operation.IsInternalOperation ? true : operation.UssName.Equals(utmMessage.UssName));
        }
    }
}