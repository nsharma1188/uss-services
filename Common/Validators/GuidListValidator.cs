﻿using Mapster;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AnraUssServices.Common.Validators
{    
    public class GuidListValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                bool isList = value is IList;

                if (isList)
                {
                    var _errors = new List<string>();
                    var collection = ((IEnumerable)value).Cast<object>().ToList();
                    bool isValid = true;
                    collection.ForEach(x => {
                        var type = x.GetType();

                        if (!CommonValidator.IsUUIDv4(Guid.Parse(x.ToString())))
                        {
                            isValid = false;
                            _errors.Add(string.Format("gufi - {0}) is not of the correct UUID version", x.ToString()));
                        }
                    });

                    if (isValid)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult(string.Join(", ", _errors));
                    }
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
