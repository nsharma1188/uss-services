﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AnraUssServices.LiteDBJobs;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.TelemetryMessages;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Interface;
using Jal.HttpClient.Model;
using LiteDB;
using Newtonsoft.Json;

namespace AnraUssServices.Common.Validators
{
	public class AirSpaceIntersectionValidator
	{
		private readonly AnraConfiguration anraConfiguration;
		private readonly PositionDocument getPosition;
		private IHttpHandler httpClient;


		public AirSpaceIntersectionValidator(AnraConfiguration anraConfiguration, PositionDocument getPosition)
		{
			httpClient = HttpHandler.Builder.Create;
			this.anraConfiguration = anraConfiguration;
			this.getPosition = getPosition;
		}

		public bool CheckAirspaceIntersection(Models.Operation operation)
		{
			try
			{

				var positionDataList = getPosition.CurrentPositions();
				var currentOperationPosition = positionDataList.Where(x => x.Gufi == operation.Gufi).FirstOrDefault();
				var currentPosition = currentOperationPosition.Location;

				var data = JsonConvert.SerializeObject(currentPosition);
				var url = anraConfiguration.MapEngineServiceUrl;
				var request = new HttpRequest(url, HttpMethod.Post);
				request.Content = new HttpRequestStringContent(data)
				{
					ContentType = "application/json",
					CharacterSet = "charset=utf-8"
				};

				var response = httpClient.Send(request);
				var checkIntersection = JsonConvert.DeserializeObject<UTMRestResponse>(response.Content.Read());
				if (checkIntersection.Message.Equals("CONTROLLED AIRSPACE"))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}
	}
}
