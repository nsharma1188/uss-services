﻿using AnraUssServices.Utm.Models;
using Mapster;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Common.Validators
{
    public class PointValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var point = value.Adapt<Point>();
                var isValidPointGeometry = point.Type.Equals(nameof(Point)) && point.Coordinates.Count == 2 && point.Coordinates[0].HasValue && point.Coordinates[1].HasValue;

                if (isValidPointGeometry)
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult(string.Format("Invalid Point.(Type = {0},Coordinate Size = {1})",point.Type,point.Coordinates.Count));
                }
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
