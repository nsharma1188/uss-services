﻿using AnraUssServices.Common.Airspace;
using AnraUssServices.Data;
using AnraUssServices.Utm.Models;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
	public class OperationVolumeValidator
	{
		private List<string> _errors;

		public List<string> Errors => _errors;

		private readonly ApplicationDbContext _context;
		private readonly AnraConfiguration _config;
		private readonly ILogger<OperationVolumeValidator> _logger;
		private NotamClient _notamClient;
		private bool _isInternalOperation;
		private bool _validateOperationTime;
		private NpgsqlPolygon _ussCoverageArea;
		private static int counter = 0;
        private Models.UtmInstance _ussInstance;
        private Operation _operation;


        public OperationVolumeValidator(ApplicationDbContext context, AnraConfiguration configuration, ILogger<OperationVolumeValidator> logger, NotamClient notamClient)
		{
			_errors = new List<string>();
			_context = context;
			_config = configuration;
			_logger = logger;
			_notamClient = notamClient;
			counter = 0;
		}

		public bool Validate(Operation operation, bool isInternalOperation, bool validateOperationTime, Models.UtmInstance ussInstance)
		{
			_isInternalOperation = isInternalOperation;
			_validateOperationTime = validateOperationTime;
            _ussInstance = ussInstance;
            _operation = operation;

            //Order by Ordinal
            var opVolumes = operation.OperationVolumes.OrderBy(x => x.Ordinal.Value).ToList();

            //Perform Custom Validation
            ValidateCustomChecks(opVolumes);

            opVolumes.ForEach(ValidateVolume);

			if (_errors.Count() <= 0)
			{
				//Validate Intersections (Space/Time/Altitude)
				ValidateIntersections(opVolumes);
			}

			return _errors.Count <= 0;
		}

        private void ValidateCustomChecks(List<OperationVolume> volumes)
        {
            //Check for Duplicate Ordinals
            if (volumes.GroupBy(a => a.Ordinal.Value).Where(a => a.Count() > 1).Any())
            {
                _errors.Add("Operation Volumes have repeating ordinals");                
            }
            
            for (int i = 1; i < volumes.Count; i++)
            {
                var precedingVolume = volumes[i - 1];
                var succeedingVolume = volumes[i];

                //Check For Succeding Op Vol Begin Time Must be After Preceding Op Vol Begin Time
                if (precedingVolume.EffectiveTimeBegin.Value.Subtract(succeedingVolume.EffectiveTimeBegin.Value).TotalSeconds > 0)
                {
                    _errors.Add(string.Format("OperationVolume ordinal-{0} effectiveTimeBegin is before ordinal-{1}", succeedingVolume.Ordinal.Value, precedingVolume.Ordinal.Value));
                }                

                //check Time Ordering
                if (!CommonValidator.AreVolumesTimeInCorrectOrder(precedingVolume, succeedingVolume))
                {                    
                    _errors.Add(string.Format("OperationVolume times are not in sequence.Ordinals are - {0},{1}", precedingVolume.Ordinal.Value, succeedingVolume.Ordinal.Value));
                }               
            }
        }

		private void ValidateVolume(OperationVolume operationVol)
		{
            //Validate Altitude
            //ValidateAltitude(operationVol);

            //Validate Times
            ValidateOperationTime(operationVol);

            //Validate Operation Geography Size is within 6000ft
            ValidatePolygonSize(operationVol);

            //Validate Operation Volume With Notams & UVRs
            if (_errors.Count <= 0)
            {				
				if (_isInternalOperation)
				{
                    //Validate If Any volume of Operation is outside USS coverage area for Internal Operations
                    ValidateOperationIsOutsideUSS(_operation);

                    //If Public safety operation then no need to validate with notam or UVR
                    if (!_operation.PriorityElements.PriorityStatus.Equals(PriortyStatus.PUBLIC_SAFETY))
                    {
                        //Validate operation volume with any existing Notams
                        ValidateNotams(operationVol);

                        //Validate operation volume with any existing UVR's
                        ValidateUVRs(operationVol);
                    }
                }                
            }
		}

        private void ValidateOperationIsOutsideUSS(Operation operation)
        {
            var factory = new GeometryFactory(new PrecisionModel(), 4326);
            var ussCoords = _ussInstance.CoverageArea.ToNTSCoordinates();            
            var ussArea = (NetTopologySuite.Geometries.Polygon)factory.CreatePolygon(new LinearRing(ussCoords.ToArray()));

            operation.OperationVolumes.ForEach(vol => {
                var opVolCoords = vol.OperationGeography.ToNpgsqlPolygon().ToNTSCoordinates();
                var opVolArea = (NetTopologySuite.Geometries.Polygon)factory.CreatePolygon(new LinearRing(opVolCoords.ToArray()));

                if (!ussArea.Covers(opVolArea))
                {
                    _errors.Add(string.Format("Operation volume area is outside USS coverage area.Ordinal -{0}", vol.Ordinal.Value));
                }
            });
        }

        private void ValidateNotams(OperationVolume operationVol)
		{
			//TODO: Commented for Nasa checkout
			//Calling below Db function here because of Postgres Numrange function restriction 
			//i.e upper bound must be greater than or equal to lower bound
			var notams = _notamClient.GetNotams(operationVol);
			if (notams != null && notams.Any())
			{
				_errors.Add("Notam Constraint(s): " + notams.Select(p => p.Restriction).FirstOrDefault());
			}
		}

        private void ValidateUVRs(OperationVolume operationVol)
        {
			var uvrs = _notamClient.GetUVRs(operationVol);
			List<string> Ids = new List<string>();

			if (uvrs != null && uvrs.Any())
			{
				uvrs.ForEach(x => {
					if (!x.PermittedUas.Contains(EnumUtils.GetDescription(_operation.FaaRule)))
					{
						Ids.Add(x.MessageId.ToString());
					}
				});

				if (Ids.Any())
				{
					_errors.Add("UVR Constraint(s): " + Ids.FirstOrDefault());
				}
			}
        }

        private void ValidateAltitude(OperationVolume operationVol)
		{
			//Each operation volume height > 0
			if (operationVol.MaxAltitude.AltitudeValue - operationVol.MinAltitude.AltitudeValue <= 0)
			{
				_errors.Add(string.Format("min_altitude must be less than max_altitude.Ordinal -{0}", operationVol.Ordinal.Value));
			}
		}

		private void ValidateOperationTime(OperationVolume operationVol)
		{
			//Each operation volume duration > 0
			//effective_time_begin < effective_time_end
			if (operationVol.EffectiveTimeEnd.Value.Subtract(operationVol.EffectiveTimeBegin.Value).TotalMinutes <= 0)
			{
				_errors.Add(string.Format("effective_time_begin must be before effective_time_end.Ordinal -{0}",operationVol.Ordinal.Value));
			}

			//actual_time_end > effective_time_begin (when actual_time_end is not null)
			if (operationVol.ActualTimeEnd.HasValue && operationVol.EffectiveTimeBegin > operationVol.ActualTimeEnd.Value)
			{
				_errors.Add(string.Format("effective_time_begin should less than actual_time_end if not null.Ordinal -{0}", operationVol.Ordinal.Value));
			}

			//The planned duration of an op vol must be less than T
			if (operationVol.EffectiveTimeEnd.Value.Subtract(operationVol.EffectiveTimeBegin.Value).TotalMinutes > _config.OperationVolumeMaxDuration)
			{
                _errors.Add(string.Format("operation volume planned duration shouldn't greater than T ({0} minutes).Ordinal -{1}", _config.OperationVolumeMaxDuration.ToString(),operationVol.Ordinal.Value));
			}

			//Amazon complained that it takes few seconds to submit operations from there side, so they were failing this validation.
			if (_isInternalOperation && _validateOperationTime && operationVol.EffectiveTimeBegin.Value <= DateTime.UtcNow.AddSeconds(-15))
			{
                _errors.Add(string.Format("Incorrect begin time for operation.Ordinal -{0}", operationVol.Ordinal.Value));                
			}

			if (_isInternalOperation && operationVol.EffectiveTimeEnd.Value <= DateTime.UtcNow)
			{
                _errors.Add(string.Format("Incorrect end time for operation.Ordinal -{0}", operationVol.Ordinal.Value));                
			}

            //Check Operation Volume times should not beyond Uss expiration i.e. effective end datetime
            if (_isInternalOperation && (operationVol.EffectiveTimeBegin.Value >= _ussInstance.TimeAvailableEnd.Value.AddMinutes(-5) || operationVol.EffectiveTimeEnd.Value >= _ussInstance.TimeAvailableEnd.Value.AddMinutes(-5)))
            {
                _errors.Add(string.Format("Operation volume begin or end time should not beyond uss expiration datetime.Ordinal -{0}", operationVol.Ordinal.Value));
            }
        }

		private void ValidateIntersections(List<OperationVolume> volumes)
		{
			//When ordered by ordinal values, a succeeding operation volume
			//must have a 2D or 3D spatial intersection with its immediately
			//preceding operation volume.

			bool is2DIntersects = true;
			bool isTimeIntersects = true;
			bool isAltIntersects = true;

			var operationVolumes = volumes.OrderBy(x => x.Ordinal.Value).ToList();

			for (int i = 1; i < operationVolumes.Count; i++)
			{
				var precedingVolume = operationVolumes[i - 1];
				var succeedingVolume = operationVolumes[i];

                //Check 2D Intersection
                if (!CommonValidator.IsPolygonIntersecting(succeedingVolume.OperationGeography, precedingVolume.OperationGeography))
				{
					is2DIntersects = false;
                    _errors.Add(string.Format("Volumes must have 2D intersection.Ordinals are - {0},{1}", succeedingVolume.Ordinal.Value, precedingVolume.Ordinal.Value));                    
				}

				//check Time Overlap
				if (!CommonValidator.IsTemporalIntersection(precedingVolume.EffectiveTimeEnd.Value, succeedingVolume.EffectiveTimeBegin.Value))
				{
					isTimeIntersects = false;
                    _errors.Add(string.Format("Volumes must have time intersection.Ordinals are - {0},{1}", succeedingVolume.Ordinal.Value, precedingVolume.Ordinal.Value));                    
				}

				//check Altitude Overlap
				if (!CommonValidator.IsAltitudeOverlapping(precedingVolume.MinAltitude.AltitudeValue, precedingVolume.MaxAltitude.AltitudeValue, succeedingVolume.MinAltitude.AltitudeValue, succeedingVolume.MaxAltitude.AltitudeValue))
				{
					isAltIntersects = false;
                    _errors.Add(string.Format("Volumes must have altitude intersection.Ordinals are - {0},{1}", succeedingVolume.Ordinal.Value, precedingVolume.Ordinal.Value));                    
				}

				//Check 3D Intersection
				if (!(is2DIntersects && isTimeIntersects && isAltIntersects))
				{
                    _errors.Add(string.Format("Volumes must have 3D intersection.Ordinals are - {0},{1}", succeedingVolume.Ordinal.Value, precedingVolume.Ordinal.Value));                    
				}
			}
		}


		//Check for Polygon Edge Size.
		private void ValidatePolygonSize(OperationVolume operationVol)
		{
			var coordinates = operationVol.OperationGeography.Coordinates.ToList().FirstOrDefault();

			double perimeter = 0;
			var valueRequired = _config.OperationVolumeMaxLength;

			for (int i = coordinates.Count - 2; i >= 0; i--)
			{
				var lon1 = Convert.ToDouble(coordinates[i][0]);
				var lat1 = Convert.ToDouble(coordinates[i][1]);
				var lon2 = Convert.ToDouble(coordinates[i+1][0]);
				var lat2 = Convert.ToDouble(coordinates[i+1][1]);

				var rlat1 = lat1 * (Math.PI / 180.0);
				var rlon1 = lon1 * (Math.PI / 180.0);
				var rlat2 = lat2 * (Math.PI / 180.0);
				var rlon2 = lon2 * (Math.PI / 180.0) - rlon1;

				var d3 = Math.Pow(Math.Sin((rlat2 - rlat1) / 2.0), 2.0) + Math.Cos(rlat1) * Math.Cos(rlat2) *
					Math.Pow(Math.Sin(rlon2 / 2.0), 2.0);

				perimeter = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
				var perimeterInFeet = perimeter * 3.28084; //meters to ft.

				if (perimeterInFeet > valueRequired)
				{
                    _errors.Add(string.Format("The polygon edge is larger then the permitted value of {0} ft.Please re-plan.Ordinal -{1}", valueRequired, operationVol.Ordinal.Value));
				}
			}
		}
	}
}