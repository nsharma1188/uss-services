﻿using AnraUssServices.AsyncHandlers;
using AnraUssServices.Utm.Models;
using MediatR;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace AnraUssServices.Common
{
    public sealed class ApiUssExchangeFilterAttribute : ActionFilterAttribute
    {
        private readonly IMediator mediator;
        private readonly ILogger<string> logger;
        private DateTime RequestTime;		
        private ObjectResult objectResult;
        private string ussName = string.Empty;
        private string currentRequestEntity = string.Empty;
        private string currentRequestBody = string.Empty;
        private string currentRequestSourceUss = string.Empty;

        public ApiUssExchangeFilterAttribute(IMediator mediator, ILogger<string> logger)
        {
            this.logger = logger;
            this.mediator = mediator;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            RequestTime = DateTime.UtcNow;

			if (context.HttpContext.Request.Method.ToUpper().Equals("PUT"))
			{
                switch (IsValidRequest(context))
                {
                    case -1:
                        objectResult = new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.BadRequest, Message = string.Format("Invalid request data. Couldn't parse to entity - {0}", ParseDataType(currentRequestEntity)) });
                        objectResult.StatusCode = StatusCodes.Status400BadRequest;
                        context.Result = objectResult;
                        break;
                    case 0:
                        objectResult = new ObjectResult(new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = "Invalid request. Token subject claim is not matching with uss name." });
                        objectResult.StatusCode = StatusCodes.Status403Forbidden;
                        context.Result = objectResult;
                        break;
                    default:
                        base.OnActionExecuting(context);
                        break;
                }
            }
            else
			{
				base.OnActionExecuting(context);
			}            
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var requestBody = ReadContent(context.HttpContext.Request.Body);
            var responseBody = ReadContent(context.HttpContext.Response.Body);
            var feature = context.HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            var originalRequestPath = feature?.OriginalPath?? context.HttpContext.Request.Path.Value;

            int? responseCode = 0;
            if (context.Result.GetType().Equals(typeof(BadRequestObjectResult)))
            {
                var result = context.Result as BadRequestObjectResult;
                var response = new UTMRestResponse { HttpStatusCode = result.StatusCode, Message = JsonConvert.SerializeObject(result.Value) };
                result.Value = response.ToJson();

                responseCode = result.StatusCode;

                responseBody = JsonConvert.SerializeObject(response);
            }
            else
            {
                responseCode = GetStatusCode(context);
            }

            var clientId = GetClaim(context.HttpContext.Request,"sub");

            mediator.Send(new AsyncUssExchangeNotification(requestBody, responseBody, responseCode, originalRequestPath, GetJwsHeaderValue(context), RequestTime, DateTime.UtcNow, clientId, context.HttpContext.Request.Method));

		}

        private static int GetStatusCode(ActionExecutedContext context)
        {
            if (context.HttpContext.Request.Method.Equals("GET"))
            {
                return (int)HttpStatusCode.OK;
            }
            else
            {
                if (context.Result.GetType().Equals(typeof(ObjectResult)))
                {
                    return (context.Result as ObjectResult).StatusCode.Value;
                }
                else if (context.Result.GetType().Equals(typeof(StatusCodeResult)))
                {
                    return (context.Result as StatusCodeResult).StatusCode;
                }
                else if (context.Result.GetType().Equals(typeof(JsonResult)))
                {
                    return (context.Result as JsonResult).StatusCode.Value;
                }
                else
                {
                    //Internal Server Error
                    return (int)HttpStatusCode.InternalServerError;
                }
            }                        
        }

        private static string GetClaim(HttpRequest request,string claimType)
        {
            try
            {
                var authHeader = request.Headers.Keys.Where(x => x.Equals("authorization", StringComparison.InvariantCultureIgnoreCase)).Select(k => k).FirstOrDefault();
                var token = request.Headers[authHeader].ToString().Replace("Bearer", "").Trim();
                var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
                var jwt = jwtSecurityTokenHandler.ReadJwtToken(token);
                return jwt.Claims.First(claim => claim.Type.Equals(claimType)).Value;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        private static string GetJwsHeaderValue(ActionExecutedContext context)
        {
            var jwsHeaderValue = string.Empty;
            var jwsHeader = context.HttpContext.Request.Headers.Keys.Where(x => x.Equals("x-utm-signed-payload", StringComparison.InvariantCultureIgnoreCase)).Select(k => k).FirstOrDefault();
            if (jwsHeader != null)
            {
                jwsHeaderValue = context.HttpContext.Request.Headers[jwsHeader];
            }

            return jwsHeaderValue;
        }

        private string ReadContent(Stream stream)
        {
            var data = string.Empty;
            try
            {
                stream.Position = 0;
                using (var streamReader = new StreamReader(stream, Encoding.UTF8, true, 1024, true))
                {
                    data = streamReader.ReadToEnd();
                }
                stream.Position = 0;
            }
            catch (Exception ex)
            {
                logger.LogError("UssExchangeHandler:: Ex={0}", ex);
            }

            return data;
        }

        private int IsValidRequest(ActionExecutingContext context)
        {
			try
			{
                //Current Request Source Uss Name
				currentRequestSourceUss = GetClaim(context.HttpContext.Request, "sub");
                //Current Request Body i.e. Payload string
				currentRequestBody = ReadContent(context.HttpContext.Request.Body);
                //Current Request Entity Name
				var pathVars = context.HttpContext.Request.Path.Value.Split('/');
				currentRequestEntity = pathVars[pathVars.Length - 2];

				switch (currentRequestEntity.ToLower())
				{
					case "operation":
					case "operations":
						ussName = JsonConvert.DeserializeObject<Operation>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
					case "uss":
						ussName = JsonConvert.DeserializeObject<UssInstance>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
                    case "negotiations":
						ussName = JsonConvert.DeserializeObject<NegotiationMessage>(currentRequestBody).UssNameOfOriginator;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
                    case "constraints":
						ussName = JsonConvert.DeserializeObject<UASVolumeReservation>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
                    case "utm_messages":
						ussName = JsonConvert.DeserializeObject<UTMMessage>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
                    case "positions":
						ussName = JsonConvert.DeserializeObject<Position>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);
                    case "uvrs":
						ussName = JsonConvert.DeserializeObject<UASVolumeReservation>(currentRequestBody).UssName;
                        return CheckRequestBodyUssName(ussName, currentRequestSourceUss);

                    default: return 1;
				}
			}
			catch (Exception ex)
			{
				logger.LogError("UssExchangeHandler:: Ex={0}", ex);
				return -1;
			}
        }

        private static ExchangedDataTypeEnum ParseDataType(string route)
        {
            switch (route.ToLower())
            {
                case "operation":
                case "operations": return ExchangedDataTypeEnum.OPERATION;
                case "uss": return ExchangedDataTypeEnum.USS_INSTANCE;
                case "negotiations": return ExchangedDataTypeEnum.NEGOTIATION_MESSAGE;
                case "constraints": return ExchangedDataTypeEnum.CONSTRAINT_MESSAGE;
                case "utm_messages": return ExchangedDataTypeEnum.UTM_MESSAGE;
                case "positions": return ExchangedDataTypeEnum.POSITION;
                default: return ExchangedDataTypeEnum.OTHER_SEE_COMMENT;
            }
        }

        private int CheckRequestBodyUssName(string ussName, string currentRequestSourceUss)
        {
            if (ussName == "" || ussName == null || ussName.Length > 250)
            {
                return -1;
            }
            if (ussName.Equals(currentRequestSourceUss))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}