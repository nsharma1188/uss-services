﻿using AnraUssServices.Utm.Models;
using Mapster;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AnraUssServices.Common.Validators
{
    public class PolygonValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var polygon = value.Adapt<Polygon>();
                bool isValidPolygonGeometry = polygon.Type.Equals(nameof(Polygon)) && polygon.Coordinates.Count == 1;

                if (isValidPolygonGeometry)
                {
                    //Polygon minItems 3 (min 3 pairs of coords)
                    if (polygon.Coordinates[0].Count < 4)
                    {
                        return new ValidationResult(string.Format("LinearRing Actual length: {0} is less than minimum required length 4.", polygon.Coordinates[0].Count));
                    }
                    //Polygon maxItems 100 (max 100 pairs of coords)
                    if (polygon.Coordinates[0].Count > 101)
                    {
                        return new ValidationResult(string.Format("LinearRing Actual length: {0} is greater than maximum required length 101.", polygon.Coordinates[0].Count));
                    }

                    //Polygon first pair of coords equal to last pair of coords (i.e. is a Linear Ring).
                    if (!IsFirstLastItemSame(polygon.Coordinates[0]))
                    {
                        return new ValidationResult("First pair of coordinates must be equal to last pair of coordinates.");
                    }

                    //operation_geography is Polygon                

                    string polygonJson = JsonConvert.SerializeObject(polygon);

                    if (string.IsNullOrEmpty(polygonJson))
                    {
                        return new ValidationResult("" + validationContext.DisplayName + " is required");
                    }
                    else
                    {
                        string geoJson = polygonJson.Replace("'", "\"");
                        var geoJsonReader = new GeoJsonReader();

                        var objPolygon = geoJsonReader.Read<NetTopologySuite.Geometries.Polygon>(geoJson);

                        //Check if a valid geometry
                        if (!objPolygon.IsValid)
                        {
                            return new ValidationResult(string.Format("{0} is not a valid geometry.", validationContext.DisplayName));
                        }
                        else
                        {
                            //Each pair of coords is exactly 2 elements
                            int cnt = 0;
                            foreach (var pair in objPolygon.Coordinates)
                            {
                                if ((!Double.IsNaN(pair.X) && !Double.IsNaN(pair.Y) && Double.IsNaN(pair.Z)) == false)
                                {                                    
                                    return new ValidationResult(string.Format("{0}.coordinates[{1}] :: Each pair of coordinates must have exactly 2 elements.", validationContext.DisplayName, cnt));
                                }
                                cnt++;
                            }

                            //Polygon array has single polygon (no "holes")
                            if (objPolygon.Holes.Length > 0)
                            {
                                return new ValidationResult(string.Format("{0} must have single polygon(no holes).", validationContext.DisplayName));
                            }

                            //Each operation 2D area > 0
                            if (objPolygon.Area <= 0)
                            {
                                return new ValidationResult(string.Format("{0} doesn't have a valid 2D area.", validationContext.DisplayName));
                            }

                            //Each spatial dimension of an op vol's bounding box must have 
                            //length less than M. Should discuss with partners the value of M(6000 Ft).
                            double boundingBoxLength = objPolygon.Length * 3.28084; //converting to feet

                            if (boundingBoxLength > 6000)
                            {                                
                                return new ValidationResult(string.Format("{0} length shouldn't greater than M (6000 feet).", validationContext.DisplayName));
                            }
                        }
                    }

                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult(string.Format("Invalid Polygon.(Type = {0},Coordinate Size = {1})",polygon.Type,polygon.Coordinates.Count));
                }
            }
            else
            {
                return new ValidationResult("" + validationContext.DisplayName + " is required");
            }
        }

        private bool IsFirstLastItemSame(List<List<double?>> cords)
        {
            var firstCord = cords.First();
            var lastCord = cords.Last();

            return firstCord.First().Equals(lastCord.First()) && firstCord.Last().Equals(lastCord.Last());
        }
    }
}
