﻿using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class UvrValidator
    {        
        private List<string> _errors;
        private readonly IRepository<ConstraintMessage> constraintsRepository;

        public List<string> Errors
        {
            get { return _errors; }
        }

        public UvrValidator(IRepository<ConstraintMessage> constraintsRepository)
        {
            _errors = new List<string>();
            this.constraintsRepository = constraintsRepository;
        }

        public bool IsValid(UASVolumeReservation uvr, bool is_internal_uvr, UtmInstance utmInstance = null, Guid message_id = new Guid())
        {
            if (message_id != uvr.MessageId)
            {
                _errors.Add("The message_id provided did not match with the uvr message_id");
            }

            //effective_time_begin < effective_time_end
            if (uvr.EffectiveTimeEnd.Value.Subtract(uvr.EffectiveTimeBegin.Value).TotalMinutes <= 0)
            {
                _errors.Add("effective_time_begin must be before effective_time_end.");
            }

            if (uvr.MaxAltitude.AltitudeValue - uvr.MinAltitude.AltitudeValue <= 0)
            {
                _errors.Add("min_altitude must be less than max_altitude.");
            }

            if(uvr.PermittedUas.Distinct().ToList().Count > 1 && uvr.PermittedUas.Distinct().Contains(UasType.NOT_SET))
            {
                _errors.Add("permitted_uas-If NOT_SET is included, then no other strings allowed.");
            }

            if(uvr.PermittedUas.Distinct().Contains(UasType.SUPPORT_LEVEL))
            {
                if (uvr.RequiredSupport != null)
                {
                    if (uvr.RequiredSupport.Count == 0)
                    {
                        _errors.Add("For permitted_uas SUPPORT_LEVEL,  field is required");
                    }
                }
                else
                {
                    _errors.Add("For permitted_uas SUPPORT_LEVEL,  field is required");
                }
            }

            if (is_internal_uvr)
            {
                ValidateUVROutsideUSS(uvr, utmInstance);
            }

            return _errors.Count() <= 0;
        }

        public bool ValidateUVROutsideUSS(UASVolumeReservation uvr, UtmInstance _utmInstance)
        {
            var factory = new GeometryFactory(new PrecisionModel(), 4327);
            var ussCoords = _utmInstance.CoverageArea.ToNTSCoordinates();
            var ussArea = (NetTopologySuite.Geometries.Polygon)factory.CreatePolygon(new LinearRing(ussCoords.ToArray()));
            var uvrCoords = uvr.Geography.ToNpgsqlPolygon().ToNTSCoordinates();
            var uvrArea = (NetTopologySuite.Geometries.Polygon)factory.CreatePolygon(new LinearRing(uvrCoords.ToArray()));

            if (!ussArea.Covers(uvrArea))
            {
                _errors.Add(string.Format("UVR area is outside of USS."));
            }

            return _errors.Count() <= 0;
        }
    }
}