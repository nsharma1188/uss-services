﻿using AnraUssServices.Utm.Models;
using Mapster;
using System;
using System.ComponentModel.DataAnnotations;

namespace AnraUssServices.Common.Validators
{
    public class RangeValidator : ValidationAttribute
    {        
        private readonly double MinValue;
        private readonly double MaxValue;
        private string DisplayMessage;

        public RangeValidator(double minValue,double maxValue,string displayMessage)
        {            
            MinValue = minValue;
            MaxValue = maxValue;
            DisplayMessage = displayMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool isValidRange = true;

            if (value != null)
            {
                if (value is double)
                {
                    isValidRange = (double)value >= MinValue && (double)value < MaxValue;
                }
                else
                {
                    DisplayMessage = FormatErrorMessage(validationContext.DisplayName);
                    isValidRange = false;
                }

                if (isValidRange)
                {
                    return ValidationResult.Success;
                }
                else
                {
                    return new ValidationResult(DisplayMessage);
                }
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
