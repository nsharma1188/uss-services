﻿using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class PositionValidator
    {        
        private List<string> _errors;
        private readonly IRepository<Models.Operation> operationRepository;

        public List<string> Errors
        {
            get { return _errors; }
        }

        public PositionValidator(IRepository<Models.Operation> operationRepository)
        {
            _errors = new List<string>();
            this.operationRepository = operationRepository;
        }

        public bool IsValid(Position position, Guid position_id = new Guid())
        {
            if (position_id != position.EnroutePositionsId)
            {
                _errors.Add("The positionId provided did not match with Position enroute_positions_id");
            }

            // time Measured < time Sent
            if (position.TimeSent.Value.Subtract(position.TimeMeasured.Value).TotalMinutes < 0)
            {
                _errors.Add("time_measured must be before time_sent");
            }

            return _errors.Count() <= 0;
        }

        public bool IsValidRequest(Position position)
        {
            var operation = operationRepository.Find(position.Gufi);

            return operation != null && operation.UssName.Equals(position.UssName);
        }
    }
}