﻿using AnraUssServices.Common.InterUss;
using AnraUssServices.Data;
using AnraUssServices.Models;
using AnraUssServices.Utm.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common
{
    public class OperationValidator
    {
        private readonly IRepository<Models.UtmInstance> utmInstance;
        private readonly IRepository<Models.Operation> operationRepository;
        private List<string> _errors;
        private readonly ApplicationDbContext _context;
        private OperationVolumeValidator _operationVolumeValidator;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DatabaseFunctions _dbFunctions;
        private bool _isInternalOperation;
		private bool _validateOperationTime;
        private readonly GridCell gridCell;
        private SlippyTiles slippyTilesLogic;
        private readonly AnraConfiguration configuration;

        public List<string> Errors
        {
            get { return _errors; }
        }

        public OperationValidator(
            ApplicationDbContext context, 
            OperationVolumeValidator operationVolumeValidator, 
            IHttpContextAccessor httpContextAccessor, 
            IRepository<Models.UtmInstance> utmInstance,
            DatabaseFunctions dbFunctions,
            GridCell gridCell,
            SlippyTiles slippyTilesLogic,
            AnraConfiguration configuration,
            IRepository<Models.Operation> operationRepository)
        {
            _errors = new List<string>();
            _context = context;
            _operationVolumeValidator = operationVolumeValidator;
            _httpContextAccessor = httpContextAccessor;
            this.utmInstance = utmInstance;
            this.operationRepository = operationRepository;
            _dbFunctions = dbFunctions;
            this.gridCell = gridCell;
            this.slippyTilesLogic = slippyTilesLogic;
            this.configuration = configuration;
        }

        public bool IsValidRequest(Utm.Models.Operation operation)
        {
            var existingOps = operationRepository.Find(operation.Gufi);

            return existingOps == null ? true : existingOps.UssName.Equals(operation.UssName);
        }

        public bool IsValid(Utm.Models.Operation operation, bool isInternalOperation = false, bool validateOperationTime = false, Guid gufi = new Guid(), Guid ussInstanceId = new Guid())
        {
            _isInternalOperation = isInternalOperation;
            _validateOperationTime = validateOperationTime;

            bool isNewOperation = operationRepository.Find(operation.Gufi) == null;

            if (gufi != operation.Gufi)
			{
				_errors.Add("The gufi provided did not match with the operation gufi");
			}

			if (string.IsNullOrEmpty(operation.State.ToString()))
            {
                _errors.Add("state is required.");
            }

			if (string.IsNullOrEmpty(operation.FaaRule.ToString()))
			{
				_errors.Add("FAA rule is required");
			}

			if (string.IsNullOrEmpty(operation.ControllerLocation.ToString()))
			{
				_errors.Add("controller location is required");
			}

			if (operation.SubmitTime.Value > operation.UpdateTime.Value)
			{
				_errors.Add(" Operation submit time after update time");
			}

			if (_isInternalOperation && !string.IsNullOrEmpty(operation.PriorityElements.PriorityStatus.ToString()))
            {
                var isLawEnforcementUser = _httpContextAccessor.HttpContext.User.FindAll("Role").ToList().Exists(x => x.Value.Equals(EnumUtils.GetDescription(EnumUserRole.LAWENFORCEMENT)));

                if (operation.PriorityElements.PriorityStatus == PriortyStatus.PUBLIC_SAFETY && !isLawEnforcementUser)
                {
                    _errors.Add("Public safety operation not permitted.");
                }
            }

            //Validate Priority Element If Any
            ValidatePriorityElement(operation);

            //Validate Contingency
            //ValidateContingency(operation);

            //Validate Volume
            ValidateOperationVolumes(operation, ussInstanceId);

            if(isNewOperation && isInternalOperation)
            {
                //Validate Max. Allowed Active Operations Within Uss
                ValidateMaxAllowedActiveOperationsWithinUss(ussInstanceId);

                //Validate Drone Assignation
                ValidateDroneAssignation(operation);
            }

            return _errors.Count() <= 0;
        }

        private bool CanBeWrittenIntoGrid(Utm.Models.Operation operation, UtmInstance ussInstance)
        {
            var gridCellOperators = gridCell.GetGridCellOperators(ussInstance.GridCells, false);
            var anraOperator = gridCellOperators.Where(x => x.Uss.Equals(ussInstance.UssName)).ToList();
            
            var slippyResponse = slippyTilesLogic.GetSlippyTiles(configuration.DefaultZoomLevel, GetCSVCoordinateList(operation));
            List<bool> gridsMatched = new List<bool>();

            if (slippyResponse.Data.GridCells.Any())
            {
                var opGrids = slippyResponse.Data.GridCells;

                opGrids.ForEach(grid => {
                    bool flag = anraOperator.Any(opt => opt.Zoom.Equals(grid.Zoom) && opt.X.Equals(grid.X) && opt.Y.Equals(grid.Y));
                    gridsMatched.Add(flag);
                });

                //Check If operation geography grids are having available uss grids.
                if(gridsMatched.Any(x => x.Equals(false)))
                {
                    return false;                    
                }
            }

            return true;
        }

        private void ValidateDroneAssignation(Utm.Models.Operation operation)
        {
            //Order by Ordinal
            var opVolumes = operation.OperationVolumes.OrderBy(x => x.Ordinal.Value).ToList();
            Guid droneid = operation.UasRegistrations.FirstOrDefault().RegistrationId;

            opVolumes.ForEach(opv => {
                var op = _dbFunctions.ValidateDroneAssignation(droneid, opv.EffectiveTimeBegin.Value, opv.EffectiveTimeEnd.Value).FirstOrDefault();

                if(op != null)
                {
                    _errors.Add(string.Format("Drone({0}) is already assigned to an active operation(Gufi - {1}).",droneid.ToString(), op.Gufi.ToString()));
                    return;
                }
            });
        }

        private void ValidateMaxAllowedActiveOperationsWithinUss(Guid ussInstanceId)
        {
            //Get Total Active Operations within selected uss
            int activeOps = operationRepository.GetAll()
                .Where(x => x.UssInstanceId.Equals(ussInstanceId))
                .Where(x => !x.State.Equals(EnumUtils.GetDescription(OperationState.PROPOSED)) && !x.State.Equals(EnumUtils.GetDescription(OperationState.CLOSED)))
                .Select(x => x.Gufi).Count();

            if (activeOps >= configuration.MaxAllowedActiveOperationsWithinUss)
            {
                _errors.Add(string.Format("Maximum capacity of active operations reached for selected Uss. Max. {0} active operations are allowed.", configuration.MaxAllowedActiveOperationsWithinUss));
                return;
            }
        }

        private void ValidatePriorityElement(Utm.Models.Operation operation)
        {
            if(operation.PriorityElements != null)
            {
                if(operation.PriorityElements.PriorityStatus == PriortyStatus.NONE && (int)operation.PriorityElements.PriorityLevel > (int)Severity.WARNING)
                {
                    _errors.Add(string.Format("For priority_status:{0} acceptable priority_level values are WARNING, NOTICE and INFORMATIONAL only.", operation.PriorityElements.PriorityStatus.ToString()));
                }

                if ((operation.PriorityElements.PriorityStatus == PriortyStatus.PUBLIC_SAFETY ||
                operation.PriorityElements.PriorityStatus == PriortyStatus.EMERGENCY_AIRBORNE_IMPACT ||
                operation.PriorityElements.PriorityStatus == PriortyStatus.EMERGENCY_GROUND_IMPACT ||
                operation.PriorityElements.PriorityStatus == PriortyStatus.EMERGENCY_AIR_AND_GROUND_IMPACT) && (int)operation.PriorityElements.PriorityLevel < (int)Severity.CRITICAL)
                {
                    _errors.Add(string.Format("For priority_status:{0} acceptable priority_level values are CRITICAL, ALERT and EMERGENCY only.", operation.PriorityElements.PriorityStatus.ToString()));
                }
            }
        }

        private void ValidateContingency(Utm.Models.Operation operation)
        {
            if(operation.ContingencyPlans!= null && operation.ContingencyPlans.Any())
            {
                //Get Min/Max DateTime
                List<DateTime> listDates = new List<DateTime>();
                
                operation.OperationVolumes.ToList().ForEach(opv =>
                {
                    listDates.Add(opv.EffectiveTimeBegin.Value);
                    listDates.Add(opv.EffectiveTimeEnd.Value);
                });

                DateTime minDateTime = listDates.Min(p => p);
                DateTime maxDateTime = listDates.Max(p => p);

                //NASA has not implemented the check for the Contingency plan begin and end time to be within operation time
                //Need to uncomment once the check is implemented on the NASA end. 

                operation.ContingencyPlans.ForEach(x =>
                {
                    if (!(x.ValidTimeBegin >= minDateTime && x.ValidTimeEnd <= maxDateTime))
                    {
                        _errors.Add("Contingency Plan begin and end time should within operation time.");
                        return;
                    }
                });
            }
            else
            {
                _errors.Add("Contingency Plan is required.");
            }
        }

        private void ValidateOperationVolumes(Utm.Models.Operation operation, Guid ussInstanceId)
        {
            var ussInstance = utmInstance.Find(ussInstanceId);

            if (_isInternalOperation)
            {
                if (ussInstance == null)
                {
                    _errors.Add("uss_instance_id is not a valid uss instance.");
                    return;
                }
                else
                {
                    if (!CanBeWrittenIntoGrid(operation, ussInstance))
                    {
                        _errors.Add(string.Format("Operation can't be write into the grid.Operator not available."));
                        return;
                    }
                }
            }

            if (!_operationVolumeValidator.Validate(operation,_isInternalOperation, _validateOperationTime, ussInstance))
            {
                _errors.AddRange(_operationVolumeValidator.Errors);
            }
        }

        private string GetCSVCoordinateList(Utm.Models.Operation operation)
        {
            string csvList = string.Empty;

            operation.OperationVolumes.ForEach(opv =>
            {
                opv.OperationGeography.Coordinates[0].ForEach(x =>
                {
                    csvList = csvList + string.Format("{0},{1},", x[1], x[0]);
                });
            });

            return csvList.Substring(0, csvList.Length - 1);
        }
    }
}