﻿using GeoAPI.Geometries;
using Geodesy;
using NetTopologySuite.Geometries;
using System;

namespace AnraUssServices.Common
{
    public class DistanceHelper
    {
        private static GeodeticCalculator calculator = new GeodeticCalculator(Ellipsoid.Sphere);

        public static IPoint CreatePointNTS(double x, double y, double z)
        {
            var factory = new GeometryFactory(new PrecisionModel(), 4326);
            return factory.CreatePoint(new Coordinate(x, y, z));
        }

        public static double CalculateDistance3D(double lat1Deg, double lon1Deg, double alt1Feet, double lat2Deg, double lon2Deg, double alt2Feet)
        {
            const int earthRadius = 6378137;             //radius of earth in meters
            const double flattening = 1 / 298.257224;        // flattening

            var lat1 = lat1Deg * Math.PI / 180;  // convert to radians
            var lon1 = lon1Deg * Math.PI / 180; // convert to radians
            var alt1 = alt1Feet / 3.28084;     // convert to meters

            var lat2 = lat2Deg * Math.PI / 180;  // convert to radians
            var lon2 = lon2Deg * Math.PI / 180; // convert to radians
            var alt2 = alt2Feet / 3.28084;     // convert to meters

            // calculate xyz per reference
            var c1 = 1 / Math.Sqrt(Math.Pow(Math.Cos(lat1), 2) + Math.Pow((1 - flattening), 2) * Math.Pow(Math.Sin(lat1), 2));
            var s1 = Math.Pow((1 - flattening), 2) * c1;
            var coord1 = new double[3];
            coord1[0] = (earthRadius * c1 + alt1) * Math.Cos(lat1) * Math.Cos(lon1);
            coord1[1] = (earthRadius * c1 + alt1) * Math.Cos(lat1) * Math.Sin(lon1);
            coord1[2] = (earthRadius * s1 + alt1) * Math.Sin(lat1);

            // calculate xyz per reference
            var c2 = 1 / Math.Sqrt(Math.Pow(Math.Cos(lat2), 2) + Math.Pow((1 - flattening), 2) * Math.Pow(Math.Sin(lat2), 2));
            var s2 = Math.Pow((1 - flattening), 2) * c2;

            var coord2 = new double[3];
            coord2[0] = (earthRadius * c2 + alt2) * Math.Cos(lat2) * Math.Cos(lon2);
            coord2[1] = (earthRadius * c2 + alt2) * Math.Cos(lat2) * Math.Sin(lon2);
            coord2[2] = (earthRadius * s2 + alt2) * Math.Sin(lat2);

            // take the difference to get the distance in each directions
            var dif = new double[3];
            dif[0] = coord1[0] - coord2[0];
            dif[1] = coord1[1] - coord2[1];
            dif[2] = coord1[2] - coord2[2];

            // take the norm to get the distance magnitude
            var dist_m = Math.Sqrt(Math.Pow(dif[0], 2) + Math.Pow(dif[1], 2) + Math.Pow(dif[2], 2));

            // convert to feet
            var dist_ft = dist_m * 3.28084;

            return dist_ft;
        }
    }
}