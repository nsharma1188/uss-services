﻿using Jose;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace AnraUssServices.Common.Security
{
    public class JwsHelper
    {
        private readonly ILogger<JwsHelper> logger;
        private readonly AnraConfiguration anraConfiguration;

        public JwsHelper(AnraConfiguration anraConfiguration, ILogger<JwsHelper> logger)
        {
            this.anraConfiguration = anraConfiguration;
            this.logger = logger;
        }

        public string GetJwsHeader<T>(T payload)
        {
            var privateKey = SecurityKeysStore.GetAnraPrivateKey(anraConfiguration.Environment);
            var privateKid = SecurityKeysStore.GetAnraPrivateKeyId(anraConfiguration.Environment);

            var headers = new Dictionary<string, object>{
                         { "alg", "RS256" },
                         { "typ", "JOSE" },
                         { "kid", privateKid },
                         { "x5u", $"{anraConfiguration.AnraServiceBaseUrl}/.well-known/uas-traffic-management/utm.jwks"},
                         { "x5t#S256", SecurityKeysStore.GetThumbprint(anraConfiguration.Environment)},
                         { "crit", Array.Empty<string>()}
                    };

            var token = JWT.Encode(JsonConvert.SerializeObject(payload), privateKey, Jose.JwsAlgorithm.RS256, extraHeaders: headers);

            return token;
        }

        public bool VerifyJwsHeader(string token)
        {
            var isValid = true;

            try
            {
                var jwk = JWT.Headers<JsonWebKey>(token);

                JsonWebKeySet jwkinfo = null;
                using (var cli = new HttpClient())
                {
                    var result = cli.GetStringAsync(jwk.X5u).Result;
                    jwkinfo = JsonConvert.DeserializeObject<JsonWebKeySet>(result);
                }

                if (jwkinfo != null)
                {
                    var jwtkey = jwkinfo.Keys.Single(item => item.Kid == jwk.Kid && item.Alg == jwk.Alg);
                    var paylod = JWT.Decode(token, Convert.FromBase64String(jwtkey.X5tS256));
                }
                else
                {
                    isValid = false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError("JWSHelper:::{0}", ex);
                isValid = false;
            }

            return isValid;
        }
    }
}