﻿using AnraUssServices.Common.Security;
using Mapster;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace AnraUssServices.Common
{
    /// <summary>
    /// Util class to allow restoring RSA parameters from JSON as the normal
    /// RSA parameters class won't restore private key info.
    /// </summary>
    internal class SecurityKeysStore
    {
        public static SecurityKey GetFimsSecurityKey()
        {
            SecurityKey signInKey = null;
            using (var publicRsa = RSA.Create())
            {
                var filePath = $"KeyStore/fimskey.json";
                var keyParams = JsonConvert.DeserializeObject<AnraRsaParameters>(File.ReadAllText(filePath)).Adapt<RSAParameters>();

                publicRsa.ImportParameters(new RSAParameters { Modulus = keyParams.Modulus, Exponent = keyParams.Exponent });
                signInKey = new RsaSecurityKey(publicRsa)
                {
                    KeyId = "a84a6a63047dddbae6d139b7a64565eff3a8eca1"
                };
            }
            return signInKey;
        }

        public static SecurityKey GetAnraPublicKey(string environment)
        {
            SecurityKey signInKey = null;
            using (var rsa = RSA.Create())
            {
                var filePath = $"KeyStore/{environment}-public-key.json";
                var keyParams = JsonConvert.DeserializeObject<AnraRsaParameters>(File.ReadAllText(filePath)).Adapt<RSAParameters>();

                rsa.ImportParameters(keyParams);
                signInKey = new RsaSecurityKey(rsa)
                {
                    KeyId = Base64UrlEncoder.Encode(GetKeyHash(keyParams))
                };
            }
            return signInKey;
        }

        public static RSA GetAnraPrivateKey(string environment)
        {
            var rsa = RSA.Create();
            var filePath = $"KeyStore/{environment}-pvt-key.json";
            var keyParams = JsonConvert.DeserializeObject<AnraRsaParameters>(File.ReadAllText(filePath)).Adapt<RSAParameters>();

            rsa.ImportParameters(keyParams);
            return rsa;
        }

        public static string GetAnraPrivateKeyId(string environment)
        {
            var filePath = $"KeyStore/{environment}-pvt-key.json";
            var keyParams = JsonConvert.DeserializeObject<RSAParameters>(File.ReadAllText(filePath));

            return Base64UrlEncoder.Encode(GetKeyHash(keyParams));
        }

        public static JsonWebKey GetPublicKeyAsJWK(AnraConfiguration configuration)
        {
            var filePath = $"KeyStore/{configuration.Environment}-public-key.json";
            var keyParams = JsonConvert.DeserializeObject<RSAParameters>(File.ReadAllText(filePath));

            var jsonWebKey = new JsonWebKey
            {
                Alg = "RS256",
                Kid = Base64UrlEncoder.Encode(GetKeyHash(keyParams)),
                Kty = "JOSE",
                Use = "verify",
                E = Convert.ToBase64String(keyParams.Exponent),
                N = Convert.ToBase64String(keyParams.Modulus),
                X5u = $"{configuration.AnraServiceBaseUrl}/.well-known/uas-traffic-management/utm.jwks",
                X5tS256 = Convert.ToBase64String(GetThumbprint(configuration.Environment))
            };

            return jsonWebKey;
        }

        private static byte[] GetKeyHash(RSAParameters keyParams)
        {
            var dict = new Dictionary<string, string>() {
                    {"e", Convert.ToBase64String(keyParams.Exponent)},
                    {"n", Convert.ToBase64String(keyParams.Modulus)}
                };

            var hash = SHA256.Create();
            return hash.ComputeHash(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(dict)));
        }

        public static byte[] GetThumbprint(string environment)
        {
            switch (environment.ToLower())
            {
                case "dev": return Convert.FromBase64String("TOO/4R9WOzZykgeT+EHi+cpDlJlkKJZBEPK074Hqc/A=");
                case "prod": return Convert.FromBase64String("U0KxlPP/WkdvvIofr5uUv55PH7h3eE5LlSzbNfy8OHk=");
                default: return null;
            }
        }

        public static byte[] SignData(object data, string environment)
        {
            using (var cryptoServiceProvider = new RSACryptoServiceProvider())
            {
                var filePath = $"KeyStore/{environment}-pvtkey.json";
                var keyParams = JsonConvert.DeserializeObject<RSAParameters>(File.ReadAllText(filePath));

                cryptoServiceProvider.ImportParameters(keyParams);

                using (var sHA256CryptoServiceProvider = new SHA256CryptoServiceProvider())
                {
                    return cryptoServiceProvider.SignData(ObjectToByteArray(data), sHA256CryptoServiceProvider);
                }
            }
        }

        private static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            var bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static string GetHash(string text, string key)
        {
            var textBytes = Encoding.UTF8.GetBytes(text);
            var keyBytes = Encoding.UTF8.GetBytes(key);

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
            {
                hashBytes = hash.ComputeHash(textBytes);
            }

            return Convert.ToBase64String(hashBytes);
        }
    }
}