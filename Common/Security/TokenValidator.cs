﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class TokenValidator
    {
        private readonly AnraConfiguration configuration;
        private readonly IHttpContextAccessor contextAccessor;

        public TokenValidator(IHttpContextAccessor contextAccessor, AnraConfiguration configuration)
        {
            this.contextAccessor = contextAccessor;
            this.configuration = configuration;
        }

        public async Task<bool> ValidateTokenAsync()
        {
            var retval = false;
            try
            {
                var token = contextAccessor.HttpContext.Request.Query["token"];
                if (!string.IsNullOrEmpty(token))
                {
                    var handler = new JwtSecurityTokenHandler();
                    var validationParameters = Utilities.GetTokenValidationParameters(configuration.Environment);
                    Microsoft.IdentityModel.Tokens.SecurityToken validatedToken;
                    var user = handler.ValidateToken(token, validationParameters, out validatedToken);
                    retval = true;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Token");
            }

            return retval;
        }

		public bool IsTokenValid(string token)
		{
			var valid = false;
			try
			{
				if (!string.IsNullOrEmpty(token))
				{
					var handler = new JwtSecurityTokenHandler();
					var validationParameters = Utilities.GetTokenValidationParameters(configuration.Environment);
					Microsoft.IdentityModel.Tokens.SecurityToken validatedToken;
					var user = handler.ValidateToken(token, validationParameters, out validatedToken);
					valid = true;
				}
			}
			catch (Exception ex)
			{
				valid = false;
			}

			return valid;
		}
    }
}
