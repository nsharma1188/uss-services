﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.Security
{
	public class TokenAuthority
	{
		/// <summary>
		/// User ID
		/// </summary>
		[JsonProperty(PropertyName = "uid")]
		public string UserId { get; set; }

		/// <summary>
		/// Organazation
		/// </summary>
		[JsonProperty(PropertyName = "oid")]
		public string OrganizationId { get; set; }

		/// <summary>
		/// Group Id
		/// </summary>
		[JsonProperty(PropertyName = "gid")]
		public string GroupId { get; set; }

		/// <summary>
		/// User Roles
		/// </summary>
		[JsonProperty(PropertyName = "role")]
		public List<string> UserRoles { get; set; }
	}
}
