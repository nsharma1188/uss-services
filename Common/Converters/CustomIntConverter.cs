﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace AnraUssServices.Common.Converters
{
	public class CustomIntConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteValue(value);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			try
			{

				if (reader.TokenType == JsonToken.String)
				{
					throw new JsonSerializationException("Invalid integer input");
				}

				return Int32.Parse(reader.Value.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(int);
		}
	}
}