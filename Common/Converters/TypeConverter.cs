﻿using AnraUssServices.Dss.Models;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Altitudes;
using AnraUssServices.ViewModel.Contacts;
using Mapster;
using Newtonsoft.Json;
using NpgsqlTypes;

namespace AnraUssServices.Converters
{
    public static class TypeConverters  //Mapster.IOb  Mapster.Typead ITypeConverter<Polygon, NpgsqlPolygon>
    {
        public static void Configure()
        {
            TypeAdapterConfig.GlobalSettings.Default.PreserveReference(true);

            ConfigurePolygonMapping();
            ConfigurePointMapping();

            TypeAdapterConfig<Jal.HttpClient.Model.HttpResponse, UTMRestResponse>.NewConfig().PreserveReference(true).MapWith(response => response.ToUTMRestResponse(), applySettings: true);
            TypeAdapterConfig<Jal.HttpClient.Model.HttpResponse, JSendGridCellMetadataResponse>.NewConfig().PreserveReference(true).MapWith(response => response.ToGridCellMetadataResponse(), applySettings: true);
            TypeAdapterConfig<Jal.HttpClient.Model.HttpResponse, JSendSlippyResponse>.NewConfig().PreserveReference(true).MapWith(response => response.ToSlippyResponse(), applySettings: true);

            TypeAdapterConfig<PersonOrOrganization, string>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.SerializeObject(p), applySettings: true);
            TypeAdapterConfig<string, PersonOrOrganization>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.DeserializeObject<PersonOrOrganization>(p), applySettings: true);
            TypeAdapterConfig<PersonOrOrganizationViewModel, string>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.SerializeObject(p), applySettings: true);
            TypeAdapterConfig<string, PersonOrOrganizationViewModel>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.DeserializeObject<PersonOrOrganizationViewModel>(p), applySettings: true);

            TypeAdapterConfig<AltitudeViewModel, string>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.SerializeObject(p), applySettings: true);
            TypeAdapterConfig<string, AltitudeViewModel>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.DeserializeObject<AltitudeViewModel>(p), applySettings: true);
            TypeAdapterConfig<Altitude, string>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.SerializeObject(p), applySettings: true);
            TypeAdapterConfig<string, Altitude>.NewConfig().PreserveReference(true).MapWith(p => JsonConvert.DeserializeObject<Altitude>(p), applySettings: true);
        } 

        private static void ConfigurePointMapping()
        {
            TypeAdapterConfig<Point, NpgsqlPoint>.NewConfig().PreserveReference(true).MapWith(point => point.ToNpgsqlPoint(), applySettings: true);

            TypeAdapterConfig<NpgsqlPoint, Point>.NewConfig().PreserveReference(true).MapWith(point => point.ToPoint(), applySettings: true);

            TypeAdapterConfig<NpgsqlPoint, NpgsqlPoint>.NewConfig().PreserveReference(true).MapWith(point => point, applySettings: true);

            TypeAdapterConfig<Point, NpgsqlPoint?>.NewConfig().PreserveReference(true).MapWith(point => point.ToNpgsqlPointNullable(), applySettings: true);

            TypeAdapterConfig<NpgsqlPoint?, Point>.NewConfig().PreserveReference(true).MapWith(point => point.ToPointNullable(), applySettings: true);
        }

        private static void ConfigurePolygonMapping()
        {
            TypeAdapterConfig<Polygon, NpgsqlPolygon>.NewConfig().PreserveReference(true).MapWith(polygon => polygon.ToNpgsqlPolygon(), applySettings: true);

            TypeAdapterConfig<NpgsqlPolygon, Polygon>.NewConfig().PreserveReference(true).MapWith(polygon => polygon.ToPolygon(), applySettings: true);

            TypeAdapterConfig<Polygon, NpgsqlPolygon?>.NewConfig().PreserveReference(true).MapWith(polygon => polygon.ToNpgsqlPolygonNullable(), applySettings: true);

            TypeAdapterConfig<NpgsqlPolygon?, Polygon>.NewConfig().PreserveReference(true).MapWith(polygon => polygon.ToPolygonNullable(), applySettings: true);

            TypeAdapterConfig<Polygon, GeoPolygon>.NewConfig().PreserveReference(true).MapWith(polygon => polygon.ToGeoPolygon(), applySettings: true);
        }
    }
}