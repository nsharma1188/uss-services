﻿using AnraUssServices.Common.FimsAuth;
using AnraUssServices.Utm.Api;
using AnraUssServices.Utm.Models;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Interface;
using Jal.HttpClient.Model;
using Mapster;
using Microsoft.Extensions.HealthChecks;
using Newtonsoft.Json;
using Sentinel.OAuth.Client;
using Sentinel.OAuth.Core.Models.OAuth.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class HealthCheckInterUssService : IHealthCheck
    {
        private readonly string serviceUrl = string.Empty;
        private readonly AnraConfiguration anraConfiguration;
        private readonly FimsTokenProvider fimsAuthentication;
        private IHttpHandler httpClient;

        public HealthCheckInterUssService(AnraConfiguration anraConfiguration, FimsTokenProvider fimsAuthentication)
        {
            this.anraConfiguration = anraConfiguration;
            this.fimsAuthentication = fimsAuthentication;
            httpClient = HttpHandler.Builder.Create;
            serviceUrl = anraConfiguration.InterUssBaseUrl;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var status = CheckStatus.Unhealthy;

            try
            {
                status = CheckInterUssService() ? CheckStatus.Healthy : CheckStatus.Unhealthy;
            }
            catch (Exception)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"InterUss Communication: {status}"));
        }

        private bool CheckInterUssService()
        {
            try
            {
                var token = fimsAuthentication.GetToken(UtmScopes.UTM_NASA_GOV_WRITE_CONFLICTMANAGEMENT);

                if (!string.IsNullOrEmpty(token))
                {
                    //var request = new HttpRequest(string.Concat(serviceUrl, "/status"), HttpMethod.Get);
                    var request = new HttpRequest(string.Concat(serviceUrl, "/slippy/10?coords=",GetCSVCoordinateStaticList(), "&coord_type=polygon"), HttpMethod.Get);
                    var requestTime = DateTime.UtcNow;

                    request.Headers.Add(new HttpHeader("authorization", token));
                    request.Content.ContentType = "application/json";
                    request.Content.CharacterSet = "charset=utf-8";
                    
                    using (var response = httpClient.Send(request))
                    {
                        var content = response.Content.Read();
                       // var result = JsonConvert.DeserializeObject(content, typeof(JSendStatusResponse)).Adapt<JSendStatusResponse>();
                        var result = JsonConvert.DeserializeObject(content, typeof(JSendSlippyResponse)).Adapt<JSendSlippyResponse>();
                        return result != null && result.Status.Equals("success");
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private string GetCSVCoordinateStaticList()
        {
            string csvList = string.Empty;
                
            csvList = "29.026754383981334,-95.7619857788086,28.99913237453024,-95.78533172607422,28.998832094727163,-95.74516296386719,29.031257272155848,-95.74172973632812,29.026754383981334,-95.7619857788086";

            return csvList;
        }
    }
}