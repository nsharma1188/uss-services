﻿using Jal.HttpClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Jal.HttpClient.Interface;
using Microsoft.Extensions.HealthChecks;
using System.Threading;
using Jal.HttpClient.Impl;
using AnraUssServices.Common.OAuthClient;

namespace AnraUssServices.Common.HealthChecks
{
    public class HealthCheckOauthService : IHealthCheck
    {
        private readonly AnraConfiguration anraConfiguration;
        private IHttpHandler httpClient;

        public HealthCheckOauthService(AnraConfiguration anraConfiguration)
        {
            this.anraConfiguration = anraConfiguration;
            httpClient = HttpHandler.Builder.Create;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default)
        {
            var status = CheckStatus.Unhealthy;
            var url = anraConfiguration.OAuthServerUrl + "login";
            var content = new LoginPayload();

            content.username = anraConfiguration.OAuthClientId;
            content.password = anraConfiguration.OAuthSecret;

            var request = new HttpRequest(url, HttpMethod.Post);
            var requestBody = JsonConvert.SerializeObject(content);
            request.Content = new HttpRequestStringContent(requestBody)
            {
                ContentType = "application/json",
                CharacterSet = "charset=utf-8"
            };

            try
            {
                using (var response = httpClient.Send(request))
                {
                    status = response != null ? CheckStatus.Healthy : CheckStatus.Unhealthy;
                }
            }
            catch (Exception ex)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"Oauth Sevice: {status}"));
        }


        private bool CheckOauthService()
        {
            var url = anraConfiguration.OAuthServerUrl + "login";
            var content = new LoginPayload();

            content.username = anraConfiguration.OAuthClientId;
            content.password = anraConfiguration.OAuthSecret;

            var request = new HttpRequest(url, HttpMethod.Post);
            var requestBody = JsonConvert.SerializeObject(content);
            request.Content = new HttpRequestStringContent(requestBody)
            {
                ContentType = "application/json",
                CharacterSet = "charset=utf-8"
            };

            try
            {
                var response = httpClient.Send(request);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }

    public class Credential
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
