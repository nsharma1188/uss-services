﻿using Microsoft.Extensions.HealthChecks;
using System;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class HealthCheckUasService : IHealthCheck
    {
        private readonly AnraConfiguration anraConfiguration;

        public HealthCheckUasService(AnraConfiguration anraConfiguration)
        {
            this.anraConfiguration = anraConfiguration;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var status = CheckStatus.Unhealthy;

            try
            {
                var url = new Uri(anraConfiguration.UASServiceUrl);
                using (var client = new TcpClient(url.Host, 443))
                {
                    status = CheckStatus.Healthy;
                }
            }
            catch (Exception)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"UAS Service: {status}"));
        }
    }
}