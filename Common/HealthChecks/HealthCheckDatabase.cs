﻿using AnraUssServices.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class HealthCheckDatabase : IHealthCheck
    {
        private readonly IServiceProvider serviceProvider;

        public HealthCheckDatabase(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var status = CheckStatus.Unhealthy;
            using (serviceScope)
            {
                var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                status = db != null ? CheckStatus.Healthy : CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"Database connection: {status}"));

        }
    }

}
