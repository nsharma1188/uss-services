﻿using AnraUssServices.Common.FimsAuth;
using Microsoft.Extensions.HealthChecks;
using Newtonsoft.Json;
using Sentinel.OAuth.Client;
using Sentinel.OAuth.Core.Models.OAuth.Http;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class HealthCheckFimsService : IHealthCheck
    {
        private readonly IServiceProvider serviceProvider;
        private readonly FimsTokenProvider fimsAuthentication;
        private readonly AnraConfiguration anraConfiguration;

        public HealthCheckFimsService(AnraConfiguration anraConfiguration)
        {
            this.anraConfiguration = anraConfiguration;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var status = CheckStatus.Unhealthy;

            try
            {
                var url = new Uri(anraConfiguration.FimsBaseUrl);
                using (var client = new TcpClient(url.Host, 443))
                {
                    status = CheckStatus.Healthy;
                }

                if (status == CheckStatus.Healthy)
                {
                    status = CheckAuthenticationServices() ? CheckStatus.Healthy : CheckStatus.Unhealthy;
                }
            }
            catch (Exception ex)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"FIMS Communication: {status}"));
        }

        private bool CheckAuthenticationServices()
        {
			AccessTokenResponse tokenResponse = null;

			if (anraConfiguration.InternalFIMS)
			{
				var url = anraConfiguration.FimsBaseUrl + "oauth/token?grant_type=client_credentials&scope=utm.nasa.gov_write.operation";
				var requestBody = "";

				var client = new System.Net.Http.HttpClient();
				client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(
				System.Text.ASCIIEncoding.ASCII.GetBytes(
				string.Format("{0}:{1}", anraConfiguration.ClientId, anraConfiguration.Secret))));
				var response = client.PostAsync(url, new System.Net.Http.StringContent(requestBody)).Result;
				var result = JsonConvert.DeserializeObject<InternalFimsAuth>(response.Content.ReadAsStringAsync().Result);
				tokenResponse = new AccessTokenResponse()
				{
					AccessToken = result.access_token,
					TokenType = "Bearer"
				};
			}
			else
			{

				var authPoints = new Sentinel.OAuth.Client.Models.AuthenticationEndpoints
				{
					AuthorizationCodeEndpointUrl = anraConfiguration.FimsBaseUrl + "/fimsAuthServer/oauth/authorize",
					TokenEndpointUrl = anraConfiguration.FimsBaseUrl + "/fimsAuthServer/oauth/token"
				};

				var s = new SentinelClientSettings(new Uri(anraConfiguration.FimsBaseUrl), anraConfiguration.ClientId, anraConfiguration.Secret, "", new TimeSpan(2, 0, 0), authPoints);

				using (var c = new SentinelOAuthClient(s))
				{
					var response = c.Authenticate(new string[] { UtmScopes.UTM_NASA_GOV_WRITE_OPERATION });
					tokenResponse = response.Result;
				}
			}

			return tokenResponse != null && tokenResponse.AccessToken != null;
        }
    }
}