﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnraUssServices.Common.ElevationHelper;
using Jal.HttpClient.Impl;
using Jal.HttpClient.Interface;
using Jal.HttpClient.Model;
using Microsoft.Extensions.HealthChecks;
using Newtonsoft.Json;

namespace AnraUssServices.Common.HealthChecks
{
    public class HealthCheckMapEngine : IHealthCheck
    {
        private readonly string serviceUrl = string.Empty;
        private readonly string MslKey = string.Empty;
        private readonly AnraConfiguration anraConfiguration;
        private IHttpHandler httpClient;

        public HealthCheckMapEngine(AnraConfiguration anraConfiguration)
        {
            this.anraConfiguration = anraConfiguration;
            serviceUrl = anraConfiguration.MslUrl;
            MslKey = anraConfiguration.MslKey;
            httpClient = HttpHandler.Builder.Create;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default)
        {
            var status = CheckStatus.Unhealthy;
            var request = new HttpRequest(string.Concat(serviceUrl, "29.026754383981334,-95.7619857788086", "&key=", MslKey), HttpMethod.Get);

            // request.Headers.Add(new HttpHeader("authorization", token));
            request.Content.ContentType = "application/json";
            request.Content.CharacterSet = "charset=utf-8";

            try
            {
                using (var response = httpClient.Send(request))
                {
                    var content = response.Content.Read();
                    var result = JsonConvert.DeserializeObject<ElevationResult>(content);

                    status = CheckStatus.Healthy;
                }
            }
            catch (Exception ex)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"MapEngine Communication: {status}"));
        }
    }
}
