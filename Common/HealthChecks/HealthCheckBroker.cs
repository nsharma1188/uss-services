﻿using Microsoft.Extensions.HealthChecks;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.Common
{
    public class HealthCheckBroker : IHealthCheck
    {
        private readonly AnraConfiguration anraConfiguration;

        public HealthCheckBroker(AnraConfiguration anraConfiguration)
        {
            this.anraConfiguration = anraConfiguration;
        }

        public ValueTask<IHealthCheckResult> CheckAsync(CancellationToken cancellationToken = default)
        {
            var status = CheckStatus.Unhealthy;

            try
            {
                using (var client = new TcpClient(anraConfiguration.MqttBrokerHost, anraConfiguration.MqttBrokerPort))
                {
                    status = CheckStatus.Healthy;
                }
            }
            catch (SocketException)
            {
                status = CheckStatus.Unhealthy;
            }

            return new ValueTask<IHealthCheckResult>(HealthCheckResult.FromStatus(status, $"Broker Communication: {status}"));
        }
    }
}