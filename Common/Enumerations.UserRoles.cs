﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AnraUssServices.Common
{
    public enum EnumUserRole : short
    {
        [Description("SUPERADMIN"), EnumMember(Value = "SUPERADMIN")]
        SUPERADMIN = 1,

		[Description("ANSP"), EnumMember(Value = "ANSP")]
        ANSP = 2,

		[Description("ATC"), EnumMember(Value = "ATC")]
        ATC = 3,

		[Description("ADMIN"), EnumMember(Value = "ADMIN")]
        ADMIN = 4,

		[Description("PILOT"), EnumMember(Value = "PILOT")]
        PILOT = 5,

        //Hiding for now since these are not added on the OAuth server
        //[Description("PAYLOADOPERATOR"), EnumMember(Value = "PAYLOADOPERATOR")]
        //PAYLOADOPERATOR = 6,

        //[Description("VISUALOBSERVER"), EnumMember(Value = "VISUALOBSERVER")]
        //VISUALOBSERVER = 7,

        [Description("LAWENFORCEMENT"), EnumMember(Value = "LAWENFORCEMENT")]
        LAWENFORCEMENT = 8,

        //[Description("GUEST"), EnumMember(Value = "GUEST")]
        //GUEST = 9
    }
}