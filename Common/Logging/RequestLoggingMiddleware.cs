﻿using AnraUssServices.Common.Mqtt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AnraUssServices
{
    public class RequestLoggingMiddleware
    {
        private readonly MqttLogger mqttLogger;
        private readonly RequestDelegate next;
        private readonly ILogger<RequestLoggingMiddleware> _logger;
        private Func<string, Exception, string> _defaultFormatter = (state, exception) => state;

        public RequestLoggingMiddleware(RequestDelegate next, ILogger<RequestLoggingMiddleware> logger, MqttLogger mqttLogger)
        {
            this.next = next;
            _logger = logger;
            this.mqttLogger = mqttLogger;
        }

        public async Task Invoke(HttpContext context)
        {
            var requestBodyStream = new MemoryStream();
            var originalRequestBody = context.Request.Body;

            await context.Request.Body.CopyToAsync(requestBodyStream);
            requestBodyStream.Seek(0, SeekOrigin.Begin);

            var url = UriHelper.GetDisplayUrl(context.Request);
            using (var streamReader = new StreamReader(requestBodyStream))
            {
                var requestBodyText = streamReader.ReadToEnd();

                var builder = new StringBuilder();
                builder.AppendLine($"ANRA REQUEST: {context.Request.Method} {url}");
                builder.AppendLine($"Headers: {JsonConvert.SerializeObject(context.Request.Headers)}");
                builder.AppendLine($"Body: {requestBodyText}");
                builder.AppendLine("");

                #if DEBUG
                    _logger.LogInformation(builder.ToString());                
                    mqttLogger.Log("ANRA REQUEST::", builder.ToString());
                #endif

                requestBodyStream.Seek(0, SeekOrigin.Begin);
                context.Request.Body = requestBodyStream;

                await next(context);
                context.Request.Body = originalRequestBody;
            }
        }
    }
}
