﻿using AnraUssServices.Common.Mqtt;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AnraUssServices
{
    public class ResponseLoggingMiddleware
    {
        private readonly MqttLogger mqttLogger;
        private readonly ILogger<ResponseLoggingMiddleware> logger;
        private readonly RequestDelegate _next;

        public ResponseLoggingMiddleware(RequestDelegate next, ILogger<ResponseLoggingMiddleware> logger, MqttLogger mqttLogger)
        {
            _next = next;
            this.logger = logger;
            this.mqttLogger = mqttLogger;
        }

        public async Task Invoke(HttpContext context)
        {
            if (logger.IsEnabled(LogLevel.Debug))
            {
                var original = context.Response.Body;

                var stream = new MemoryStream();
                context.Response.Body = stream;
                await _next(context);

                stream.Seek(0, SeekOrigin.Begin);
                var body = new StreamReader(stream).ReadToEnd();
                if (!string.IsNullOrEmpty(body))
                {
                    var url = UriHelper.GetDisplayUrl(context.Request); 

                    var builder = new StringBuilder();
                    builder.AppendLine($"ANRA RESPONSE: {url}");
                    builder.AppendLine($"Body: {body}");
                    builder.AppendLine("");

                    #if DEBUG
                        logger.LogInformation(builder.ToString());
                        mqttLogger.Log("ANRA RESPONSE::", builder.ToString());
                    #endif

                    stream.Seek(0, SeekOrigin.Begin);
                    await stream.CopyToAsync(original);
                }
            }
            else
            {
                await _next(context);
            }
        }
    }
}