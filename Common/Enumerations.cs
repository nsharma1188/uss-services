using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace AnraUssServices.Common
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum UTMMessageOrigin
    {
        [Description(nameof(FIMS)), EnumMember(Value = "FIMS")]
        FIMS = 1,

        [Description(nameof(USS)), EnumMember(Value = "USS")]
        USS = 2,

        [Description(nameof(OTHER)), EnumMember(Value = "OTHER")]
        OTHER = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum AirSpeedSource
    {
        [Description(nameof(MEASURED)), EnumMember(Value = "MEASURED")]
        MEASURED = 1,

        [Description(nameof(ESTIMATED)), EnumMember(Value = "ESTIMATED")]
        ESTIMATED = 2,

        [Description(nameof(OTHER)), EnumMember(Value = "OTHER")]
        OTHER = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum TrackBearingReferenceEnum
    {
        [Description(nameof(TRUE_NORTH)), EnumMember(Value = "TRUE_NORTH")]
        TRUE_NORTH = 1,

        [Description(nameof(MAGNETIC_NORTH)), EnumMember(Value = "MAGNETIC_NORTH")]
        MAGNETIC_NORTH = 2,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum TrackBearingUomEnum
    {
        [Description(nameof(DEG)), EnumMember(Value = "DEG")]
        DEG = 0,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Severity
    {
        [Description(nameof(INFORMATIONAL)), EnumMember(Value = "INFORMATIONAL")]
        INFORMATIONAL = 1,

        [Description(nameof(NOTICE)), EnumMember(Value = "NOTICE")]
        NOTICE = 2,

        [Description(nameof(WARNING)), EnumMember(Value = "WARNING")]
        WARNING = 3,

        [Description(nameof(CRITICAL)), EnumMember(Value = "CRITICAL")]
        CRITICAL = 4,

        [Description(nameof(ALERT)), EnumMember(Value = "ALERT")]
        ALERT = 5,

        [Description(nameof(EMERGENCY)), EnumMember(Value = "EMERGENCY")]
        EMERGENCY = 6,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum PriortyStatus
    {
        [Description(nameof(NONE)), EnumMember(Value = "NONE")]
        NONE = 1,

        [Description(nameof(PUBLIC_SAFETY)), EnumMember(Value = "PUBLIC_SAFETY")]
        PUBLIC_SAFETY = 2,

        [Description(nameof(EMERGENCY_AIRBORNE_IMPACT)), EnumMember(Value = "EMERGENCY_AIRBORNE_IMPACT")]
        EMERGENCY_AIRBORNE_IMPACT = 3,

        [Description(nameof(EMERGENCY_GROUND_IMPACT)), EnumMember(Value = "EMERGENCY_GROUND_IMPACT")]
		EMERGENCY_GROUND_IMPACT = 4,

        [Description(nameof(EMERGENCY_AIR_AND_GROUND_IMPACT)), EnumMember(Value = "EMERGENCY_AIR_AND_GROUND_IMPACT")]
		EMERGENCY_AIR_AND_GROUND_IMPACT = 5,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum MessageTypeEnum
    {
        /// <summary>
        /// Enum UNPLANNEDLANDINGEnum for "UNPLANNED_LANDING"
        /// </summary>
        [EnumMember(Value = "UNPLANNED_LANDING")]
        UNPLANNED_LANDING = 1,

        /// <summary>
        /// Enum UNCONTROLLEDLANDINGEnum for "UNCONTROLLED_LANDING"
        /// </summary>
        [EnumMember(Value = "UNCONTROLLED_LANDING")]
        UNCONTROLLED_LANDING = 2,

        /// <summary>
        /// Enum OPERATIONNONCONFORMINGEnum for "OPERATION_NONCONFORMING"
        /// </summary>
        [EnumMember(Value = "OPERATION_NONCONFORMING")]
        OPERATION_NONCONFORMING = 3,

        /// <summary>
        /// Enum OPERATIONROGUEEnum for "OPERATION_ROGUE"
        /// </summary>
        [EnumMember(Value = "OPERATION_ROGUE")]
        OPERATION_ROGUE = 4,

        /// <summary>
        /// Enum OPERATIONCONFORMINGEnum for "OPERATION_CONFORMING"
        /// </summary>
        [EnumMember(Value = "OPERATION_CONFORMING")]
        OPERATION_CONFORMING = 5,

        /// <summary>
        /// Enum OPERATIONCLOSEDEnum for "OPERATION_CLOSED"
        /// </summary>
        [EnumMember(Value = "OPERATION_CLOSED")]
        OPERATION_CLOSED = 6,

        /// <summary>
        /// Enum CONTINGENCYPLANINITIATEDEnum for "CONTINGENCY_PLAN_INITIATED"
        /// </summary>
        [EnumMember(Value = "CONTINGENCY_PLAN_INITIATED")]
        CONTINGENCY_PLAN_INITIATED = 7,

        /// <summary>
        /// Enum CONTINGENCYPLANCANCELLEDEnum for "CONTINGENCY_PLAN_CANCELLED"
        /// </summary>
        [EnumMember(Value = "CONTINGENCY_PLAN_CANCELLED")]
        CONTINGENCY_PLAN_CANCELLED = 8,

        /// <summary>
        /// Enum PERIODICPOSITIONREPORTSSTARTEnum for "PERIODIC_POSITION_REPORTS_START"
        /// </summary>
        [EnumMember(Value = "PERIODIC_POSITION_REPORTS_START")]
        PERIODIC_POSITION_REPORTS_START = 9,

        /// <summary>
        /// Enum PERIODICPOSITIONREPORTSENDEnum for "PERIODIC_POSITION_REPORTS_END"
        /// </summary>
        [EnumMember(Value = "PERIODIC_POSITION_REPORTS_END")]
        PERIODIC_POSITION_REPORTS_END = 10,

        /// <summary>
        /// Enum UNAUTHORIZEDAIRSPACEPROXIMITYEnum for "UNAUTHORIZED_AIRSPACE_PROXIMITY"
        /// </summary>
        [EnumMember(Value = "UNAUTHORIZED_AIRSPACE_PROXIMITY")]
        UNAUTHORIZED_AIRSPACE_PROXIMITY = 11,

        /// <summary>
        /// Enum UNAUTHORIZEDAIRSPACEENTRYEnum for "UNAUTHORIZED_AIRSPACE_ENTRY"
        /// </summary>
        [EnumMember(Value = "UNAUTHORIZED_AIRSPACE_ENTRY")]
        UNAUTHORIZED_AIRSPACE_ENTRY = 12,

        /// <summary>
        /// Enum OTHERSEEFREETEXTEnum for "OTHER_SEE_FREE_TEXT"
        /// </summary>
        [EnumMember(Value = "OTHER_SEE_FREE_TEXT")]
        OTHER_SEE_FREE_TEXT = 13
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum OperationState : short
    {       
        [Description(nameof(PROPOSED)), EnumMember(Value = "PROPOSED")]
        PROPOSED = 1,

        [Description(nameof(READYFORATC)), EnumMember(Value = "READYFORATC")]
        READYFORATC = 2,

        [Description(nameof(ACCEPTED)), EnumMember(Value = "ACCEPTED")]
        ACCEPTED = 3,

        [Description(nameof(ACTIVATED)), EnumMember(Value = "ACTIVATED")]
        ACTIVATED = 4,

        [Description(nameof(CLOSED)), EnumMember(Value = "CLOSED")]
        CLOSED = 5,

        [Description(nameof(NONCONFORMING)), EnumMember(Value = "NONCONFORMING")]
        NONCONFORMING = 6,

        [Description(nameof(ROGUE)), EnumMember(Value = "ROGUE")]
        ROGUE = 7,        
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum UrepSource : short
    {
        [Description(nameof(VEHICLE)), EnumMember(Value = "VEHICLE")]
        VEHICLE = 1,

        [Description(nameof(OBSERVER)), EnumMember(Value = "OBSERVER")]
        OBSERVER = 2,

        [Description(nameof(GROUND_SENSOR)), EnumMember(Value = "GROUND_SENSOR")]
        GROUND_SENSOR = 3,

        [Description(nameof(AIRBORNE_SENSOR)), EnumMember(Value = "AIRBORNE_SENSOR")]
        AIRBORNE_SENSOR = 4,

        [Description(nameof(OTHER)), EnumMember(Value = "OTHER")]
        OTHER = 5,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum IcingIntensity : short
    {
        [Description(nameof(TRACE)), EnumMember(Value = "TRACE")]
        TRACE = 1,

        [Description(nameof(TRACE_LGT)), EnumMember(Value = "TRACE_LGT")]
        TRACE_LGT = 2,

        [Description(nameof(LGT)), EnumMember(Value = "LGT")]
        LGT = 3,

        [Description(nameof(LGT_MOD)), EnumMember(Value = "LGT_MOD")]
        LGT_MOD = 4,

        [Description(nameof(MOD)), EnumMember(Value = "MOD")]
        MOD = 5,

        [Description(nameof(MOD_SEV)), EnumMember(Value = "MOD_SEV")]
        MOD_SEV = 6,

        [Description(nameof(SEV)), EnumMember(Value = "SEV")]
        SEV = 7,

        [Description(nameof(NEG)), EnumMember(Value = "NEG")]
        NEG = 8,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum IcingType : short
    {
        [Description(nameof(RIME)), EnumMember(Value = "RIME")]
        RIME = 1,

        [Description(nameof(CLR)), EnumMember(Value = "CLR")]
        CLR = 2,

        [Description(nameof(MX)), EnumMember(Value = "MX")]
        MX = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum TurbulenceIntensity : short
    {
        [Description(nameof(LGT)), EnumMember(Value = "LGT")]
        LGT = 1,

        [Description(nameof(MOD)), EnumMember(Value = "MOD")]
        MOD = 2,

        [Description(nameof(SEV)), EnumMember(Value = "SEV")]
        SEV = 3,

        [Description(nameof(EXTRM)), EnumMember(Value = "EXTRM")]
        EXTRM = 4,

        [Description(nameof(LGT_MOD)), EnumMember(Value = "LGT_MOD")]
        LGT_MOD = 5,

        [Description(nameof(MOD_SEV)), EnumMember(Value = "MOD_SEV")]
        MOD_SEV = 6,

        [Description(nameof(SEV_EXTRM)), EnumMember(Value = "SEV_EXTRM")]
        SEV_EXTRM = 7,

        [Description(nameof(NEG)), EnumMember(Value = "NEG")]
        NEG = 8,
    }


    [JsonConverter(typeof(StringEnumConverter))]
    public enum Weather : short
    {
        [Description(nameof(DRSN)), EnumMember(Value = "DRSN")]
        DRSN = 1,

        [Description(nameof(BLSN)), EnumMember(Value = "BLSN")]
        BLSN = 2,

        [Description(nameof(DRDU)), EnumMember(Value = "DRDU")]
        DRDU = 3,

        [Description(nameof(DRSA)), EnumMember(Value = "DRSA")]
        DRSA = 4,

        [Description(nameof(DZ)), EnumMember(Value = "DZ")]
        DZ = 5,

        [Description(nameof(FZDZ)), EnumMember(Value = "FZDZ")]
        FZDZ = 6,

        [Description(nameof(DU)), EnumMember(Value = "DU")]
        DU = 7,

        [Description(nameof(BLDU)), EnumMember(Value = "BLDU")]
        BLDU = 8,

        [Description(nameof(DS)), EnumMember(Value = "DS")]
        DS = 9,

        [Description(nameof(FG)), EnumMember(Value = "FG")]
        FG = 10,

        [Description(nameof(FZFG)), EnumMember(Value = "FZFG")]
        FZFG = 11,

        [Description(nameof(FZRA)), EnumMember(Value = "FZRA")]
        FZRA = 12,

        [Description(nameof(FC)), EnumMember(Value = "FC")]
        FC = 13,

        [Description(nameof(GR)), EnumMember(Value = "GR")]
        GR = 14,

        [Description(nameof(SHGR)), EnumMember(Value = "SHGR")]
        SHGR = 15,

        [Description(nameof(HZ)), EnumMember(Value = "HZ")]
        HZ = 16,

        [Description(nameof(IC)), EnumMember(Value = "IC")]
        IC = 17,

        [Description(nameof(PL)), EnumMember(Value = "PL")]
        PL = 18,

        [Description(nameof(SHPL)), EnumMember(Value = "SHPL")]
        SHPL = 19,

        [Description(nameof(BR)), EnumMember(Value = "BR")]
        BR = 20,

        [Description(nameof(BCFG)), EnumMember(Value = "BCFG")]
        BCFG = 21,

        [Description(nameof(PRFG)), EnumMember(Value = "PRFG")]
        PRFG = 22,

        [Description(nameof(RA)), EnumMember(Value = "RA")]
        RA = 23,

        [Description(nameof(SHRA)), EnumMember(Value = "SHRA")]
        SHRA = 24,

        [Description(nameof(SA)), EnumMember(Value = "SA")]
        SA = 25,

        [Description(nameof(BLSA)), EnumMember(Value = "BLSA")]
        BLSA = 26,

        [Description(nameof(SS)), EnumMember(Value = "SS")]
        SS = 27,

        [Description(nameof(MIFG)), EnumMember(Value = "MIFG")]
        MIFG = 28,

        [Description(nameof(SHGS)), EnumMember(Value = "SHGS")]
        SHGS = 29,

        [Description(nameof(GS)), EnumMember(Value = "GS")]
        GS = 30,

        [Description(nameof(FU)), EnumMember(Value = "FU")]
        FU = 31,

        [Description(nameof(SG)), EnumMember(Value = "SG")]
        SG = 32,

        [Description(nameof(SN)), EnumMember(Value = "SN")]
        SN = 33,

        [Description(nameof(SHSN)), EnumMember(Value = "SHSN")]
        SHSN = 34,

        [Description(nameof(PY)), EnumMember(Value = "PY")]
        PY = 35,

        [Description(nameof(SQ)), EnumMember(Value = "SQ")]
        SQ = 36,

        [Description(nameof(TS)), EnumMember(Value = "TS")]
        TS = 37,

        [Description(nameof(UP)), EnumMember(Value = "UP")]
        UP = 38,

        [Description(nameof(VA)), EnumMember(Value = "VA")]
        VA = 39,

        [Description(nameof(PO)), EnumMember(Value = "PO")]
        PO = 40,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum WeatherIntensity : short
    {
        [Description("HEAVY"), EnumMember(Value = "HEAVY")]
        HEAVY = 1,

        [Description("MODERATE"), EnumMember(Value = "MODERATE")]
        MODERATE = 2,

        [Description("LIGHT"), EnumMember(Value = "LIGHT")]
        LIGHT = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum FaaRule : short
    {
        [Description("PART_107"), EnumMember(Value = "PART_107")]
        PART_107 = 1,

        [Description("PART_107X"), EnumMember(Value = "PART_107X")]
        PART_107X = 2,

        [Description("PART_101E"), EnumMember(Value = "PART_101E")]
        PART_101E = 3,

        [Description(nameof(OTHER)), EnumMember(Value = "OTHER")]
        OTHER = 4,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum VolumeType : short
    {
        [Description("ABOV"), EnumMember(Value = "ABOV")]
        ABOV = 1,

        [Description("TBOV"), EnumMember(Value = "TBOV")]
        TBOV = 2,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ConstraintMessageType
    {
        [EnumMember(Value = "DYNAMIC_RESTRICTION")]
        DYNAMIC_RESTRICTION = 1,

        [EnumMember(Value = "STATIC_ADVISORY")]
        STATIC_ADVISORY = 2
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ConstraintMessageCauseEnum
    {
        [EnumMember(Value = "WEATHER")]
        WEATHER = 1,

        [EnumMember(Value = "ATC")]
        ATC = 2,

        [EnumMember(Value = "SECURITY")]
        SECURITY = 3,

        [EnumMember(Value = "SAFETY")]
        SAFETY = 4,

        [EnumMember(Value = "MUNICIPALITY")]
        MUNICIPALITY = 5,

        [EnumMember(Value = "OTHER")]
        OTHER = 6
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum UasType
    {
        [EnumMember(Value = "NOT_SET")]
        NOT_SET = 1,

        [EnumMember(Value = "PUBLIC_SAFETY")]
        PUBLIC_SAFETY = 2,

        [EnumMember(Value = "SECURITY")]
        SECURITY = 3,

        [EnumMember(Value = "NEWS_GATHERING")]
        NEWS_GATHERING = 4,

        [EnumMember(Value = "VLOS")]
        VLOS = 5,

		[EnumMember(Value = "SUPPORT_LEVEL")]
		SUPPORT_LEVEL = 6,

		[EnumMember(Value = "PART_107")]
        PART_107 = 7,

        [EnumMember(Value = "PART_101E")]
        PART_101E = 8,

        [EnumMember(Value = "PART_107X")]
        PART_107X = 9,

        [EnumMember(Value = "RADIO_LINE_OF_SIGHT")]
        RADIO_LINE_OF_SIGHT = 10
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum RequiredSupportType
    {
        [EnumMember(Value = "V2V")]
        V2V = 1,

        [EnumMember(Value = "DAA")]
        DAA = 2,

        [EnumMember(Value = "ADSB_OUT")]
        ADSB_OUT = 3,

        [EnumMember(Value = "ADSB_IN")]
        ADSB_IN = 4,

        [EnumMember(Value = "CONSPICUITY")]
        CONSPICUITY = 5,

        [EnumMember(Value = "ENHANCED_NAVIGATION")]
        ENHANCED_NAVIGATION = 6,

        [EnumMember(Value = "ENHANCED_SAFE_LANDING ")]
        ENHANCED_SAFE_LANDING = 7
    }

    internal enum OperationSearchType : short
    {
        [Description(nameof(LOCATION))]
        LOCATION = 1,

        [Description(nameof(DRONE))]
        DRONE = 2,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum TestTypeEnum
    {
        /// <summary>
        /// Enum GROUNDEnum for "GROUND"
        /// </summary>
        [Description(nameof(GROUND)), EnumMember(Value = "GROUND")]
        GROUND = 1,

        /// <summary>
        /// Enum FLIGHTEnum for "FLIGHT"
        /// </summary>
        [Description(nameof(FLIGHT)), EnumMember(Value = "FLIGHT")]
        FLIGHT = 2
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum SourceEnum
    {
        /// <summary>
        /// Enum HWITLEnum for "HWITL"
        /// </summary>
        [EnumMember(Value = "HWITL")]
        HWITL = 1,

        /// <summary>
        /// Enum SWITLEnum for "SWITL"
        /// </summary>
        [EnumMember(Value = "SWITL")]
        SWITL = 2
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum SettingEnum
    {
        /// <summary>
        /// Enum LABEnum for "LAB"
        /// </summary>
        [EnumMember(Value = "LAB")]
        LAB = 1,

        /// <summary>
        /// Enum FIELDEnum for "FIELD"
        /// </summary>
        [EnumMember(Value = "FIELD")]
        FIELD = 2
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum NotamState : short
    {
        [Description(nameof(ACTIVATED)), EnumMember(Value = "ACTIVATED")]
        ACTIVATED = 1,

        [Description(nameof(CANCELLED)), EnumMember(Value = "CANCELLED")]
        CANCELLED = 2,

        [Description(nameof(EXPIRED)), EnumMember(Value = "EXPIRED")]
        EXPIRED = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum StateEnum
    {
        /// <summary>
        /// Enum CRUISE for "CRUISE"
        /// </summary>
        [EnumMember(Value = "CRUISE")]
        CRUISE = 1,

        /// <summary>
        /// Enum DESCENT for "DESCENT"
        /// </summary>
        [EnumMember(Value = "DESCENT")]
        DESCENT = 2,

        /// <summary>
        /// Enum CLIMB for "CLIMB"
        /// </summary>
        [EnumMember(Value = "CLIMB")]
        CLIMB = 3,

        /// <summary>
        /// Enum HOVER for "HOVER"
        /// </summary>
        [EnumMember(Value = "HOVER")]
        HOVER = 4,

        /// <summary>
        /// Enum LOITER for "LOITER"
        /// </summary>
        [EnumMember(Value = "LOITER")]
        LOITER = 5,

        /// <summary>
        /// Enum UNKNOWN for "UNKNOWN"
        /// </summary>
        [EnumMember(Value = "UNKNOWN")]
        UNKNOWN = 6
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum VehicleTypeEnum
    {
        /// <summary>
        /// Enum SMALLUAS for "SMALL_UAS"
        /// </summary>
        [EnumMember(Value = "SMALL_UAS")]
        SMALLUAS = 1,

        /// <summary>
        ///  LARGEUAS for "LARGE_UAS"
        /// </summary>
        [EnumMember(Value = "LARGE_UAS")]
        LARGEUAS = 2,

        /// <summary>
        /// Enum HELICOPTER for "HELICOPTER"
        /// </summary>
        [EnumMember(Value = "HELICOPTER")]
        HELICOPTER = 3,

        /// <summary>
        /// Enum SMALLMANNEDFIXEDWING for "SMALL_MANNED_FIXED_WING"
        /// </summary>
        [EnumMember(Value = "SMALL_MANNED_FIXED_WING")]
        SMALLMANNEDFIXEDWING = 4,

        /// <summary>
        /// Enum HANGGLIDER for "HANG_GLIDER"
        /// </summary>
        [EnumMember(Value = "HANG_GLIDER")]
        HANGGLIDER = 5,

        /// <summary>
        /// Enum BALLOON for "BALLOON"
        /// </summary>
        [EnumMember(Value = "BALLOON")]
        BALLOON = 6,

        /// <summary>
        /// Enum LARGEMANNEDFIXEDWING for "LARGE_MANNED_FIXED_WING"
        /// </summary>
        [EnumMember(Value = "LARGE_MANNED_FIXED_WING")]
        LARGEMANNEDFIXEDWING = 7,

        /// <summary>
        /// Enum OTHER for "OTHER"
        /// </summary>
        [EnumMember(Value = "OTHER")]
        OTHER = 8
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum GenericNotificationType : short
    {
        [Description(nameof(CONFLICT)), EnumMember(Value = "CONFLICT")]
        CONFLICT = 1,

        [Description(nameof(NEGOTIATION)), EnumMember(Value = "NEGOTIATION")]
        NEGOTIATION = 2,

        [Description(nameof(NO_ACTION)), EnumMember(Value = "NO_ACTION")]
        NO_ACTION = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum AltitudeSourceEnum
    {
        [Description(nameof(ONBOARD_SENSOR)), EnumMember(Value = "ONBOARD_SENSOR")]
        ONBOARD_SENSOR = 0,

        [Description(nameof(OTHER)), EnumMember(Value = "OTHER")]
        OTHER = 1,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum UomHeightTypeEnum
    {
        [Description(nameof(FT)), EnumMember(Value = "FT")]
        FT = 0
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum VerticalReferenceTypeEnum
    {
        [Description(nameof(W84)), EnumMember(Value = "W84")]
        W84 = 0
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum UomGroundSpeedTypeEnum
    {
        [Description(nameof(KT)), EnumMember(Value = "KT")]
        KT = 0,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum PermittedUasTypeEnum
    {
        [Description(nameof(NOT_SET)), EnumMember(Value = "NOT_SET")]
        NOT_SET = 1,

        [Description(nameof(PUBLIC_SAFETY)), EnumMember(Value = "PUBLIC_SAFETY")]
        PUBLIC_SAFETY = 2,

        [Description(nameof(SECURITY)), EnumMember(Value = "SECURITY")]
        SECURITY = 3,

        [Description(nameof(NEWS_GATHERING)), EnumMember(Value = "NEWS_GATHERING")]
        NEWS_GATHERING = 4,

        [Description(nameof(VLOS)), EnumMember(Value = "VLOS")]
        VLOS = 5,

        [Description(nameof(PART_107)), EnumMember(Value = "PART_107")]
        PART_107 = 6,

        [Description(nameof(PART_101E)), EnumMember(Value = "PART_101E")]
        PART_101E = 7,

        [Description(nameof(PART_107X)), EnumMember(Value = "PART_107X")]
        PART_107X = 8,

        [Description(nameof(RADIO_LINE_OF_SIGHT)), EnumMember(Value = "RADIO_LINE_OF_SIGHT")]
        RADIO_LINE_OF_SIGHT = 9,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum ExchangedDataTypeEnum
    {

        /// <summary>
        /// Enum OPERATION for value: OPERATION
        /// </summary>
        [EnumMember(Value = "operation")]
        OPERATION = 1,

        /// <summary>
        /// Enum POSITION for value: POSITION
        /// </summary>
        [EnumMember(Value = "position")]
        POSITION = 2,

        /// <summary>
        /// Enum UTMMESSAGE for value: UTM_MESSAGE
        /// </summary>
        [EnumMember(Value = "utm_message")]
        UTM_MESSAGE = 3,

        /// <summary>
        /// Enum CONSTRAINTMESSAGE for value: CONSTRAINT_MESSAGE
        /// </summary>
        [EnumMember(Value = "constraint_message")]
        CONSTRAINT_MESSAGE = 4,

        /// <summary>
        /// Enum NEGOTIATIONMESSAGE for value: NEGOTIATION_MESSAGE
        /// </summary>
        [EnumMember(Value = "negotiation_message")]
        NEGOTIATION_MESSAGE = 5,

        /// <summary>
        /// Enum USSINSTANCE for value: USS_INSTANCE
        /// </summary>
        [EnumMember(Value = "uss_instance")]
        USS_INSTANCE = 6,

        /// <summary>
        /// Enum OTHERSEECOMMENT for value: OTHER_SEE_COMMENT
        /// </summary>
        [EnumMember(Value = "other_see_comment")]
        OTHER_SEE_COMMENT = 7
    }

    /// <summary>
    /// An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).
    /// </summary>
    /// <value>An enum indicating if the USS providing these data was the one that initiated the request (SOURCE_USS) or the USS that received the request (TARGET_USS).</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ReportingUssRoleEnum
    {

        /// <summary>
        /// Enum SOURCEUSS for value: SOURCE_USS
        /// </summary>
        [EnumMember(Value = "SOURCE_USS")]
        SOURCEUSS = 1,

        /// <summary>
        /// Enum TARGETUSS for value: TARGET_USS
        /// </summary>
        [EnumMember(Value = "TARGET_USS")]
        TARGETUSS = 2
    }

    /// <summary>
    /// The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.
    /// </summary>
    /// <value>The level of announcements the USS would like to recieve related to operations in this grid cell.  Current just a binary, but expect this enumeration to grow as use cases are developed.  For example, USSs may want just security related announcements, or would only like announcements that involve changed geographies.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AnnouncementLevelEnum
    {
        /// <summary>
        /// Enum NONEEnum for NONE
        /// </summary>
        [EnumMember(Value = "NONE")]
        NONE = 1,

        /// <summary>
        /// Enum ALLEnum for ALL
        /// </summary>
        [EnumMember(Value = "ALL")]
        ALL = 2
    }

    /// <summary>
    /// The type of negotation result. INTERSECTION: both USSs agreed to intersect their operation plans. REPLAN: both USSs agreed on replan of receiving operation.  In this   case there would be no planned intersection of these operations.
    /// </summary>
    /// <value>The type of negotation result. INTERSECTION: both USSs agreed to intersect their operation plans. REPLAN: both USSs agreed on replan of receiving operation.  In this   case there would be no planned intersection of these operations.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NegotiationAgreementTypeEnum
    {
		/// <summary>
		/// INTERSECTION
		/// </summary>
		[Description(nameof(INTERSECTION)), EnumMember(Value = "INTERSECTION")]
        INTERSECTION = 1,

		/// <summary>
		/// REPLAN
		/// </summary>
		[Description(nameof(REPLAN)), EnumMember(Value = "REPLAN")]
		REPLAN = 2
    }


    /// <summary>
    /// The type of negotiation message.  Currently just requesting another USS replan an operation, or request that a USS allow an intersection of operations.  In addition, add ability to cancel previous agreed negotiation.
    /// </summary>
    /// <value>The type of negotiation message.  Currently just requesting another USS replan an operation, or request that a USS allow an intersection of operations.  In addition, add ability to cancel previous agreed negotiation.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NegotiationMessageTypeEnum
    {
        /// <summary>
        /// REPLAN_REQUESTED
        /// </summary>
        [Description(nameof(REPLAN_REQUESTED)), EnumMember(Value = "REPLAN_REQUESTED")]
        REPLAN_REQUESTED = 1,

        /// <summary>
        /// REPLAN_CANCELLED
        /// </summary>
        [Description(nameof(REPLAN_CANCELLED)), EnumMember(Value = "REPLAN_CANCELLED")]
        REPLAN_CANCELLED = 2,

        /// <summary>
        /// INTERSECTION_REQUESTED
        /// </summary>
        [Description(nameof(INTERSECTION_REQUESTED)), EnumMember(Value = "INTERSECTION_REQUESTED")]
        INTERSECTION_REQUESTED = 3,

        /// <summary>
        /// INTERSECTION_CANCELLED
        /// </summary>
        [Description(nameof(INTERSECTION_CANCELLED)), EnumMember(Value = "INTERSECTION_CANCELLED")]
        INTERSECTION_CANCELLED = 4
    }

    /// <summary>
    /// When 'type' is REPLAN_REQUESTED, the originating USS may supply a suggestion as to how the receiving USS might alter the plan under 
    /// its management to minimize the conflict. This field is optional in all cases.  It should not be sent with any message that does not contain type = REPLAN_REQUESTED. 
    /// The suggestion is truly a suggestion.  The receiving USS may solve (or not solve) a conflict in any manner it deems appropriate.
    /// </summary>
    /// <value>When 'type' is REPLAN_REQUESTED, the originating USS may supply a suggestion as to how the receiving USS might alter the plan under 
    /// its management to minimize the conflict. This field is optional in all cases.  It should not be sent with any message that does not contain type = REPLAN_REQUESTED. 
    /// The suggestion is truly a suggestion.  The receiving USS may solve (or not solve) a conflict in any manner it deems appropriate.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NegotiationReplanSuggestionEnum
    {
        /// <summary>
        /// ALTITUDE_CHANGE
        /// </summary>
        [Description(nameof(ALTITUDE_CHANGE)), EnumMember(Value = "ALTITUDE_CHANGE")]
        ALTITUDE_CHANGE = 1,

        /// <summary>
        /// DEPART_LATER
        /// </summary>
        [Description(nameof(DEPART_LATER)), EnumMember(Value = "DEPART_LATER")]
        DEPART_LATER = 2,

        /// <summary>
        /// LAND_EARLIER
        /// </summary>
        [Description(nameof(LAND_EARLIER)), EnumMember(Value = "LAND_EARLIER")]
        LAND_EARLIER = 3,

        /// <summary>
        /// HORIZONTAL_CHANGE
        /// </summary>
        [Description(nameof(HORIZONTAL_CHANGE)), EnumMember(Value = "HORIZONTAL_CHANGE")]
        HORIZONTAL_CHANGE = 4
    }

    /// <summary>
    /// Type of query (point, path, or polygon)
    /// </summary>
    /// <value>Type of query (point, path, or polygon)</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CoordTypeEnum
    {
        /// <summary>
        /// Enum PointEnum for point
        /// </summary>
        [EnumMember(Value = "point")]
        point = 1,

        /// <summary>
        /// Enum PathEnum for path
        /// </summary>
        [EnumMember(Value = "path")]
        path = 2,

        /// <summary>
        /// Enum PolygonEnum for polygon
        /// </summary>
        [EnumMember(Value = "polygon")]
        polygon = 3
    }

    /// <summary>
    /// An enum HTTP method used in this exchange..
    /// </summary>
    /// <value>An enum HTTP method used in this exchange..</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum HttpMethodEnum
    {

        /// <summary>
        /// GET
        /// </summary>
        [EnumMember(Value = "GET")]
        GET = 1,

        /// <summary>
        /// POST
        /// </summary>
        [EnumMember(Value = "POST")]
        POST = 2,

        /// <summary>
        /// PUT
        /// </summary>
        [EnumMember(Value = "PUT")]
        PUT = 3,

        /// <summary>
        /// DELETE
        /// </summary>
        [EnumMember(Value = "DELETE")]
        DELETE = 4,

        /// <summary>
        /// HEAD
        /// </summary>
        [EnumMember(Value = "HEAD")]
        HEAD = 5,

        /// <summary>
        /// TRACE
        /// </summary>
        [EnumMember(Value = "TRACE")]
        TRACE = 6,

        /// <summary>
        /// OPTIONS
        /// </summary>
        [EnumMember(Value = "OPTIONS")]
        OPTIONS = 7,

        /// <summary>
        /// CONNECT
        /// </summary>
        [EnumMember(Value = "CONNECT")]
        CONNECT = 8,

        /// <summary>
        /// PATCH
        /// </summary>
        [EnumMember(Value = "PATCH")]
        PATCH = 9
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum NegotiationReporterOpActionEnum
    {
        [Description(nameof(NONE)), EnumMember(Value = "NONE")]
        NONE = 1,

        [Description(nameof(SPATIAL_MOD)), EnumMember(Value = "SPATIAL_MOD")]
        SPATIAL_MOD = 2,

        [Description(nameof(TEMPORAL_MOD)), EnumMember(Value = "TEMPORAL_MOD")]
        TEMPORAL_MOD = 3,

        [Description(nameof(SPATIAL_AND_TEMPORAL_MOD)), EnumMember(Value = "SPATIAL_AND_TEMPORAL_MOD")]
        SPATIAL_AND_TEMPORAL_MOD = 4,

        [Description(nameof(OTHER_MOD)), EnumMember(Value = "OTHER_MOD")]
        OTHER_MOD = 5,

        [Description(nameof(UNKNOWN)), EnumMember(Value = "UNKNOWN")]
        UNKNOWN = 6,
    }

    /// <summary>
    /// How is the message sent to the operator? If ‘OTHER’ you must describe in ‘notes’ field. May expand this enumeration based on discussions with partners.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MessageProtocol
    {

        /// <summary>
        /// HTTP
        /// </summary>
        [Description(nameof(HTTP)), EnumMember(Value = "HTTP")]
        HTTP = 1,

        /// <summary>
        /// WEBSOCKETS
        /// </summary>
        [Description(nameof(WEBSOCKETS)), EnumMember(Value = "WEBSOCKETS")]
        WEBSOCKETS = 2,

        /// <summary>
        /// EMAIL
        /// </summary>
        [Description(nameof(EMAIL)), EnumMember(Value = "EMAIL")]
        EMAIL = 3,

        /// <summary>
        /// SMS
        /// </summary>
        [Description(nameof(SMS)), EnumMember(Value = "SMS")]
        SMS = 4
    }

    /// <summary>
    /// Expand Factor type LOW = 0.00005, MEDIUM = 0.00009 and HIGH = 0.0001
    /// </summary>
    /// <value>Expand Factor type LOW = 0.00005, MEDIUM = 0.00009 and HIGH = 0.0001</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ExpandFactorTypeEnum
    {
        /// <summary>
        /// LOW
        /// </summary>
        [EnumMember(Value = "LOW")]
        LOW = 1,

        /// <summary>
        /// MEDIUM
        /// </summary>
        [EnumMember(Value = "MEDIUM")]
        MEDIUM = 2,

        /// <summary>
        /// HIGH
        /// </summary>
        [EnumMember(Value = "HIGH")]
        HIGH = 3
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum CRUDActionEnum
    {
        [Description(nameof(INSERT)), EnumMember(Value = "INSERT")]
        INSERT = 1,

        [Description(nameof(UPDATE)), EnumMember(Value = "UPDATE")]
        UPDATE = 2,

        [Description(nameof(DELETE)), EnumMember(Value = "DELETE")]
        DELETE = 3,
    }

	/// <summary>
	/// Signup Types
	/// </summary>
	[JsonConverter(typeof(StringEnumConverter))]
	public enum SignupType
	{

		[Description(nameof(Individual)), EnumMember(Value = "Individual")]
		Individual = 1,

		[Description(nameof(Organization)), EnumMember(Value = "Organization")]
		Organization = 2

	}


    [JsonConverter(typeof(StringEnumConverter))]
    public enum StatusEnum
    {
        [Description(nameof(ACK)), EnumMember(Value = "ACK")]
        ACK = 1,

        [Description(nameof(NAK)), EnumMember(Value = "NAK")]
        NAK = 2
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum BillingRuleTypeEnum
    {
        [Description(nameof(OPERATION_ISSUANCE)), EnumMember(Value = "OPERATION_ISSUANCE")]
        OPERATION_ISSUANCE = 1,

        [Description(nameof(OPERATION_DURATION)), EnumMember(Value = "OPERATION_DURATION")]
        OPERATION_DURATION = 2,

        [Description(nameof(OPERATION_CANCELLED_ATC)), EnumMember(Value = "OPERATION_CANCELLED_ATC")]
        OPERATION_CANCELLED_ATC = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum EventTypeEnum
    {
        [Description(nameof(ISSUED)), EnumMember(Value = "ISSUED")]
        ISSUED = 1,

        [Description(nameof(FINISHED)), EnumMember(Value = "FINISHED")]
        FINISHED = 2,

        [Description(nameof(CANCELED_ATC)), EnumMember(Value = "CANCELED_ATC")]
        CANCELED_ATC = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum IssueCategory
    {
        [Description(nameof(Issue)), EnumMember(Value = "Issue")]
        Issue = 1,

        [Description(nameof(Enhancement)), EnumMember(Value = "Enhancement")]
        Enhancement = 2,

        [Description(nameof(Other)), EnumMember(Value = "Other")]
        Other = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum IssuePriority
    {
        [Description(nameof(High_Impact)), EnumMember(Value = "High_Impact")]
        High_Impact = 1,

        [Description(nameof(Moderate_Impact)), EnumMember(Value = "Moderate_Impact")]
        Moderate_Impact = 2,

        [Description(nameof(No_Impact)), EnumMember(Value = "No_Impact")]
        No_Impact = 3,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum IssueStatus
    {
        [Description(nameof(Initial)), EnumMember(Value = "Initial")]
        Initial = 1,

        [Description(nameof(Open)), EnumMember(Value = "Open")]
        Open = 2,

        [Description(nameof(InProgress)), EnumMember(Value = "InProgress")]
        InProgress = 3,

        [Description(nameof(Completed)), EnumMember(Value = "Completed")]
        Completed = 4,

        [Description(nameof(Closed)), EnumMember(Value = "Closed")]
        Closed = 5,
    }

    /// <summary>
    /// Mqtt Message Type for mqtt notiofi
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MqttMessageType
    {
        /// <summary>
        /// Operation Notification Type
        /// </summary>
        [Description(nameof(STATUS)), EnumMember(Value = "STATUS")]
        STATUS = 1,

        /// <summary>
        /// Operation Notification Type
        /// </summary>
        [Description(nameof(CONFLICT)), EnumMember(Value = "CONFLICT")]
        CONFLICT = 2,

        /// <summary>
        /// Operation Notification Type
        /// </summary>
        [Description(nameof(COLLISION)), EnumMember(Value = "COLLISION")]
        COLLISION = 3,

        /// <summary>
        /// Message Type if the operation has interected with an active UVR
        /// </summary>
        [Description(nameof(UVR)), EnumMember(Value = "UVR")]
        UVR = 4,

        /// <summary>
        /// Message Type if the operation has interected with an active UVR
        /// </summary>
        [Description(nameof(CREATED)), EnumMember(Value = "CREATED")]
        CREATED = 5,

        /// <summary>
        /// Message Type if the operation has interected with an active UVR
        /// </summary>
        [Description(nameof(UPDATED)), EnumMember(Value = "UPDATED")]
        UPDATED = 6,

        /// <summary>
        /// Message Type if the operation has interected with an active UVR
        /// </summary>
        [Description(nameof(CLOSED)), EnumMember(Value = "CLOSED")]
        CLOSED = 7,

        /// <summary>
        /// Message Type if the operation has interected with an active UVR
        /// </summary>
        [Description(nameof(NEGOTIATION)), EnumMember(Value = "NEGOTIATION")]
        NEGOTIATION = 8
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum MqttViewerType
    {
        [Description(nameof(OPERATION)), EnumMember(Value = "OPERATION")]
        OPERATION = 1,

        [Description(nameof(UVR)), EnumMember(Value = "UVR")]
        UVR = 2,

        [Description(nameof(NOTAM)), EnumMember(Value = "NOTAM")]
        NOTAM = 3,
    }





    internal class EnumUtils
    {
        public static string GetDescription(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }
    }


}