﻿using AnraUssServices.Common.Mqtt;
using AnraUssServices.Common.Telemetry;
using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.TelemetryMessages;
using AnraUssServices.WebSocketManager;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AnraUssServices.Sockets
{
    public class TelemetryConnection : WebSocketConnection
    {
        private readonly TelemetryConnectionHandler handler;
        private TelemetryMessageHandler messageHanlder;

        public TelemetryConnection(TelemetryConnectionHandler handler)
            : base(handler)
        {
            this.handler = handler;
        }

        public string socketId { get; set; }

        public void InitTelemetryMessageHandler()
        {
            messageHanlder = TelemetryMessageHandler.GetHandler(handler.serviceProvider, handler.anraMqttClient, socketId);
        }

        public override UTMRestResponse Receive(string message)
        {
            return new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.Forbidden, Message = "Hint: use SendAsync()" };
        }

        public override async Task ReceiveAsync(string message)
        {
            try
            {
                #if DEBUG
                handler.logger.LogInformation("telemetry Message {0}", message);
                #endif

                var receiveMessage = JsonConvert.DeserializeObject<TelemetryMessageViewModel>(message);
                messageHanlder.SavePosition(receiveMessage);

                handler.anraMqttClient.PublishTopic(string.Format(MqttTopics.TelementaryConnTopic, receiveMessage.Registration), receiveMessage);
            }
            catch (Exception ex)
            {
                handler.logger.LogError("telemetry message error {0}", ex);

                var response = new UTMRestResponse { HttpStatusCode = (int)HttpStatusCode.PreconditionFailed, Message = ex.Message };
                await SendMessageAsync(JsonConvert.SerializeObject(response));
            }
        }
    }
}