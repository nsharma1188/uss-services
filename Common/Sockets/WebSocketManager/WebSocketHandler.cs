﻿using AnraUssServices.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AnraUssServices.WebSocketManager
{
    public abstract class WebSocketHandler
    {
        public ILogger<string> logger;
        public AnraMqttClient anraMqttClient;
        public IServiceProvider serviceProvider;
        public ViewerMqttNotification viewerMqttNotification;

        protected abstract int BufferSize { get; }

        private List<WebSocketConnection> _connections = new List<WebSocketConnection>();

        public List<WebSocketConnection> Connections { get => _connections; }

        public async Task ListenConnection(WebSocketConnection connection)
        {
            try
            {
                var buffer = new byte[BufferSize];

                while (connection.WebSocket.State == WebSocketState.Open)
                {
                    var result = await connection.WebSocket.ReceiveAsync(
                        buffer: new ArraySegment<byte>(buffer),
                        cancellationToken: CancellationToken.None);

                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        var message = Encoding.UTF8.GetString(buffer, 0, result.Count);

                        await connection.ReceiveAsync(message);
                    }
                    else if (result.MessageType == WebSocketMessageType.Close)
                    {
                        await OnDisconnected(connection);
                    }
                }
            }
            catch (Exception ex)
            {
                await connection.WebSocket.CloseAsync(WebSocketCloseStatus.InternalServerError, ex.Message, CancellationToken.None);
                logger.LogError("${WebSocketHandler ::: ERROR ListenConnection} {0}", ex);
            }
        }

        public virtual async Task OnDisconnected(WebSocketConnection connection)
        {
            try
            {
                if (connection != null)
                {
                    _connections.Remove(connection);

                    await connection.WebSocket.CloseAsync(
                        closeStatus: WebSocketCloseStatus.NormalClosure,
                        statusDescription: "Closed by the WebSocketHandler",
                        cancellationToken: CancellationToken.None);
                }
            }
            catch (Exception ex)
            {
                await connection.WebSocket.CloseAsync(WebSocketCloseStatus.InternalServerError, ex.Message, CancellationToken.None);
                logger.LogError("${WebSocketHandler ::: ERROR OnDisconnected} {0}", ex);
            }
        }

        public abstract Task<WebSocketConnection> OnConnected(HttpContext context);
    }
}