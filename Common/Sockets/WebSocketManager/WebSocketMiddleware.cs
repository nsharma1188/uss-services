﻿using AnraUssServices.Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.WebSocketManager
{
    /// <summary>
    /// This should the last middleware in the pipeline when use websocket
    /// </summary>
    public class WebSocketMiddleware
    {
        private readonly TokenValidator tokenValidator;
        private readonly ILogger<string> logger;
        private readonly RequestDelegate _next;
        private WebSocketHandler _webSocketHandler { get; set; }

        public WebSocketMiddleware(RequestDelegate next, WebSocketHandler webSocketHandler, TokenValidator tokenValidator, ILogger<string> logger)
        {
            _next = next;
            _webSocketHandler = webSocketHandler;
            this.logger = logger;
            this.tokenValidator = tokenValidator;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.WebSockets.IsWebSocketRequest)
            {
                var connection = await _webSocketHandler.OnConnected(context);
                if (connection != null)
                {
                    await _webSocketHandler.ListenConnection(connection);
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
            }
        }
    }
}
