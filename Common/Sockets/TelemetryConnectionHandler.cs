﻿using AnraUssServices.Common;
using AnraUssServices.WebSocketManager;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Sockets
{
    public class TelemetryConnectionHandler : WebSocketHandler
    {
        private readonly TokenValidator tokenValidator;

        public TelemetryConnectionHandler(ILogger<string> logger,
            TokenValidator tokenValidator,
            IServiceProvider serviceProvider,
            ViewerMqttNotification viewerNotification,
            AnraMqttClient anraMqttClient)
        {
            this.logger = logger;
            this.tokenValidator = tokenValidator;
            viewerMqttNotification = viewerNotification;
            this.serviceProvider = serviceProvider;
            this.anraMqttClient = anraMqttClient;
        }

        protected override int BufferSize { get => 1024 * 4; }

        public override async Task<WebSocketConnection> OnConnected(HttpContext context)
        {
            var gufi = context.Request.Query["gufi"];

            var isValid = await tokenValidator.ValidateTokenAsync();

            if (!string.IsNullOrEmpty(gufi) && isValid)
            {
                AbortExistingConnection(gufi);

                var webSocket = await context.WebSockets.AcceptWebSocketAsync();
                var connection = new TelemetryConnection(this)
                {
                    socketId = gufi,
                    WebSocket = webSocket
                };
                connection.InitTelemetryMessageHandler();
                return connection;
            }

            return null;
        }

        private void AbortExistingConnection(Microsoft.Extensions.Primitives.StringValues gufi)
        {
            var existingConnnection = Connections.FirstOrDefault(m => ((TelemetryConnection)m).socketId == gufi);

            if (existingConnnection != null)
            {
                Connections.Remove(existingConnnection);
                existingConnnection.WebSocket.Abort();
            }
        }
    }
}