﻿using AnraUssServices.Utm.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace AnraUssServices.Common.OAuthClient
{
    public class OAuthHttpClient
    {
        private HttpClientHandler clientHandler = new HttpClientHandler();

        private HttpClient client { get; set; }

        public OAuthHttpClient()
        {
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
            client = new HttpClient(clientHandler);
        }


        public T Get<T>(string url, string authorization)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(url),
                Method = System.Net.Http.HttpMethod.Get
            };

            //client.DefaultRequestHeaders.Add("Authorization", authorization);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization.Remove(0, 6));

            var result = client.SendAsync(request).Result;
            var response = JsonConvert.DeserializeObject<T>(result.Content.ReadAsStringAsync().Result);
            return response;
        }


        public UTMRestResponse Post(string url, string authorization, object content)
        {
            var restResponse = new UTMRestResponse();
            try
            {
                var request = new System.Net.Http.HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = System.Net.Http.HttpMethod.Post
                };

                var requestBody = JsonConvert.SerializeObject(content);
                request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization.Remove(0, 6));

                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var result = client.SendAsync(request).Result;

                restResponse.HttpStatusCode = (int)result.StatusCode;
                restResponse.Message = result.ReasonPhrase;

                return restResponse;

            }
            catch (Exception ex)
            {
                return restResponse;
            }
        }

        public UTMRestResponse Put(string url, string authorization, object content)
        {
            var restResponse = new UTMRestResponse();
            try
            {
                var request = new System.Net.Http.HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = System.Net.Http.HttpMethod.Put,
                };

                var requestBody = JsonConvert.SerializeObject(content);
                request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));

                //client.DefaultRequestHeaders.Add("Authorization", authorization);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization.Remove(0, 6));
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                var result = client.SendAsync(request).Result;

                restResponse.HttpStatusCode = (int)result.StatusCode;
                restResponse.Message = result.ReasonPhrase;

                return restResponse;

            }
            catch (Exception ex)
            {
                return restResponse;
            }
        }

        public UTMRestResponse Delete(string url, string authorization)
        {
            var restResponse = new UTMRestResponse();

            try
            {
                var request = new System.Net.Http.HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = System.Net.Http.HttpMethod.Delete
                };

                //client.DefaultRequestHeaders.Add("Authorization", authorization);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization.Remove(0, 6));

                var result = client.SendAsync(request).Result;

                restResponse.HttpStatusCode = (int)result.StatusCode;
                restResponse.Message = result.ReasonPhrase;

                return restResponse;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public T Post<T>(string url, string authorization, object content)
        {

            var request = new System.Net.Http.HttpRequestMessage
            {
                RequestUri = new Uri(url),
                Method = System.Net.Http.HttpMethod.Post
            };

            var requestBody = JsonConvert.SerializeObject(content);
            request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorization.Remove(0, 6));

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var result = client.SendAsync(request).Result;

            var response = JsonConvert.DeserializeObject<T>(result.Content.ReadAsStringAsync().Result);
            return response;

        }

    }
}
