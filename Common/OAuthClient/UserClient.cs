﻿using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Roles;
using AnraUssServices.ViewModel.Users;
using Mapster;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace AnraUssServices.Common.OAuthClient
{
    public class UserClient
    {
        private readonly AnraConfiguration configuration;
        private readonly ILogger<UserClient> logger;
        private readonly OAuthHttpClient oauthHttpClient;

        public UserClient(AnraConfiguration configuration, AnraHttpClient anraHttpClient,
            ILogger<UserClient> logger, OAuthHttpClient oauthHttpClient)
        {
            this.configuration = configuration;
            this.logger = logger;
            this.oauthHttpClient = oauthHttpClient;
        }


        /// <summary>
        /// Returns the user details based on the user id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public UserItem GetUserDetails(string userId, string authorization)
        {
            var user = new UserItem();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/user/{userId}");

                var response = oauthHttpClient.Get<UserResponseModel>(url, authorization);

                user = response.Adapt<UserItem>();

                user.Roles = GetCurrentUserRole(user.Roles.Select(x => x.Id).ToList());
                user.Organizations.Add(response.Organization);
                user.Email = user.UserName;

                return user;
            }
            catch (Exception ex)
            {
                return user;
            }
        }

        /// <summary>
        /// Returns the list of user based on groupId or organizationId or all the users if groupId and OrganizationId is null.
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="groupId"></param>
        /// <param name="organazationId"></param>
        /// <returns></returns>
        public List<UserItem> GetUsers(string authorization, string groupId = null, string organazationId = null)
        {
            var users = new List<UserItem>();
            try
            {
                if (groupId != null)
                {
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/users/groupid/{groupId}");
                    var existingUsers = oauthHttpClient.Get<List<UserResponseModel>>(url, authorization);

                    foreach (var item in existingUsers)
                    {

                        var user = item.Adapt<UserItem>();
                        user.Roles = GetCurrentUserRole(item.Roles.Select(x => x.Id).ToList());
                        user.Organizations.Add(item.Organization);
                        user.Email = item.UserName;
                        user.OrganizationId = item.Organization.OrganizationId;
                        users.Add(user);

                    }

                    return users;
                }
                else if (organazationId != null)
                {
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/users/organizationid/{organazationId}");
                    var existingUsers = oauthHttpClient.Get<List<UserResponseModel>>(url, authorization);

                    foreach (var item in existingUsers)
                    {
                        var user = item.Adapt<UserItem>();
                        user.Roles = GetCurrentUserRole(item.Roles.Select(x => x.Id).ToList());
                        user.Organizations.Add(item.Organization);
                        user.Email = item.UserName;
                        user.OrganizationId = item.Organization.OrganizationId;
                        users.Add(user);
                    }

                    return users;
                }
                else
                {
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/users");
                    var existingUsers = oauthHttpClient.Get<List<UserResponseModel>>(url, authorization);

                    foreach (var item in existingUsers)
                    {
                        if (item.UserName != configuration.OAuthClientId)
                        {
                            var user = item.Adapt<UserItem>();
                            user.Roles = GetCurrentUserRole(item.Roles.Select(x => x.Id).ToList());
                            user.Organizations.Add(item.Organization);
                            user.Email = item.UserName;
                            user.OrganizationId = item.Organization.OrganizationId;
                            users.Add(user);
                        }
                    }

                    return users;
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: GetUsers : ex = {0}", ex.Message);
                return users;
            }
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="authorization"></param>
        public UTMRestResponse CreateUser(UserModel user, string authorization)
        {
            var response = new UTMRestResponse();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/user");
                response = oauthHttpClient.Post(url, authorization, user);

                //Check the response code from the OAuth server
                if (response.HttpStatusCode == 201)
                {
                    response.Message = "User Created";
                }
                else if (response.HttpStatusCode == 400)
                {
                    response.Message = "Email already exist. Please use a differet email to create user.";
                }
                else
                {
                    logger.LogError("Error ::: UserClient :: CreateUser : ex = {0}", JsonConvert.SerializeObject(response));
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: CreateUser : ex = {0}", ex.Message);
                return response;
            }
        }

        /// <summary>
        /// Update the exisitng user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="authorization"></param>
        /// <param name="userId"></param>
        public UTMRestResponse UpdateUser(UserModel user, string authorization, string userId)
        {
            var response = new UTMRestResponse();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/user/{userId}");

                response = oauthHttpClient.Put(url, authorization, user);

                //Check the response code from the OAuth server
                if (response.HttpStatusCode == 200)
                {
                    response.Message = "User Updated Successfully";
                }
                else if (response.HttpStatusCode == 400)
                {
                    response.Message = "Email already exist. Please use a differet email.";
                }
                else
                {

                    logger.LogError("Error ::: UserClient :: UpdateUser : ex = {0}", JsonConvert.SerializeObject(response));
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: UpdateUser : ex = {0}", ex.Message);
                return response;
            }


        }

        /// <summary>
        /// Remove the exisitng user
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="userId"></param>
        public UTMRestResponse RemoveUser(string authorization, string userId)
        {
            var response = new UTMRestResponse();

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/user/{userId}");

                response = oauthHttpClient.Delete(url, authorization);

                if (response.HttpStatusCode == 200)
                {
                    response.Message = "User Removed Successfully";
                }
                else if (response.HttpStatusCode == 404)
                {
                    response.Message = "User doesn't exits";
                }
                else
                {

                    logger.LogError("Error ::: UserClient :: RemoveUser : ex = {0}", JsonConvert.SerializeObject(response));

                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: RemoveUser : ex = {0}", ex.Message);
                return response;

            }
        }

        /// <summary>
        /// Validate Password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userPassword"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public bool ValidatePassword(string userId, string userPassword, string authorization)
        {
            try
            {
                var user = new PasswordValidation()
                {
                    UserId = userId,
                    UserPassword = userPassword
                };

                var url = string.Concat(configuration.OAuthServerUrl, $"api/user/checkpassword");

                var response = oauthHttpClient.Post<bool>(url, authorization, user);

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: ValidatePassword : ex = {0}", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Change user's password
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public UTMRestResponse ChangePassword(string authorization, UserChangePassword user)
        {
            var response = new UTMRestResponse();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/user/changepassword");

                response = oauthHttpClient.Post(url, authorization, user);

                //Check the response code from the OAuth server
                if (response.HttpStatusCode == 200)
                {
                    response.Message = "Password changed successfully";
                }
                else
                {

                    logger.LogError("Error ::: UserClient :: ChangePassword : ex = {0}", JsonConvert.SerializeObject(response));
                    response.Message = "Error while changing password";
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: ChangePassword : ex = {0}", ex.Message);
                return response;
            }
        }

        /// <summary>
		/// Returns the List of RoleItem based on the role value
		/// </summary>
		/// <param name="rolesList"></param>
		/// <returns></returns>
		public List<RoleItem> GetCurrentUserRole(List<int> rolesList)
        {
            var usersRole = new List<RoleItem>();

            foreach (var item in rolesList)
            {
                var role = RoleItem.GetRole(item);
                role.IsAssigned = true;
                usersRole.Add(role);
            }

            return usersRole;
        }

        /// <summary>
        /// Get the bearer token from OAuth
        /// </summary>
        /// <returns></returns>
        public OAuthToken GetToken(bool IsInternal)
        {
            var token = new OAuthToken();
            try
            {
                var clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                var client = new System.Net.Http.HttpClient(clientHandler);
                var url = configuration.OAuthServerUrl + "login";

                var content = new LoginPayload();

                content.username = configuration.OAuthClientId;
                content.password = configuration.OAuthSecret;

                var request = new System.Net.Http.HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = System.Net.Http.HttpMethod.Post,
                };

                var requestBody = JsonConvert.SerializeObject(content);
                request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
                token = JsonConvert.DeserializeObject<OAuthToken>(response);

                return token;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: GetToken : ex = {0}", ex.Message);
                return token;
            }
        }

        /// <summary>
        /// Gets the Token from the OAuth Server based on the userName and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetToken(string userName, string password)
        {
            try
            {
                var clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

                var client = new System.Net.Http.HttpClient(clientHandler);
                var url = configuration.OAuthServerUrl + "login";

                var content = new LoginPayload();

                content.username = userName;
                content.password = password;

                var request = new System.Net.Http.HttpRequestMessage
                {
                    RequestUri = new Uri(url),
                    Method = System.Net.Http.HttpMethod.Post,
                };

                var requestBody = JsonConvert.SerializeObject(content);
                request.Content = new System.Net.Http.ByteArrayContent(Encoding.UTF8.GetBytes(requestBody));
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: GetToken : ex = {0}", ex.Message);
                return null;
            }
        }
    }
}
