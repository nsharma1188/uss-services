﻿using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Roles;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.OAuthClient
{
    public class RoleScopeClient
    {
        private readonly AnraConfiguration configuration;
        private readonly ILogger<RoleScopeClient> logger;
        private readonly OAuthHttpClient oauthHttpClient;

        public RoleScopeClient(AnraConfiguration configuration, ILogger<RoleScopeClient> logger, 
            OAuthHttpClient oauthHttpClient)
        {
            this.configuration = configuration;
            this.logger = logger;
            this.oauthHttpClient = oauthHttpClient;
        }
        /// <summary>
        /// Get the list of roles form OAuth server
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public List<RolesModel> GetRolesList(string authorization)
        {
            var rolesList = new List<RolesModel>();
            
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/roles");

                rolesList = oauthHttpClient.Get<List<RolesModel>>(url, authorization);

                return rolesList;
            }
            catch(Exception ex)
            {
                logger.LogError("Exception ::: RoleScopeClient :: GetRolesList : ex = {0}", ex.Message);
                return rolesList;
            }

        }

        /// <summary>
        /// Add a new Role
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public UTMRestResponse CreateRole(string authorization, RolesModel role)
        {
            var response = new UTMRestResponse();

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/role");
                response = oauthHttpClient.Post(url, authorization, role);

                //Check the response code from the OAuth server
                if (response.HttpStatusCode == 201)
                {
                    response.Message = "New Role Added";
                }
                else
                {
                    logger.LogError("Error ::: RoleScopeClient :: CreateRole : ex = {0}", JsonConvert.SerializeObject(response));
                    response.Message = "Error while adding role";
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: RoleScopeClient :: CreateRole : ex = {0}", ex.Message);
                return response;
            }

        }

        /// <summary>
        /// Update Existing Role
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public UTMRestResponse UpdateRole(string authorization, RolesModel role)
        {
            var response = new UTMRestResponse();

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/role/{role.Id}");
                response = oauthHttpClient.Put(url, authorization, role);

                //Check the response code from the OAuth server
                if (response.HttpStatusCode == 200)
                {
                    response.Message = "Role's Updated Successfully";
                }
                else
                {
                    logger.LogError("Error ::: RoleScopeClient :: UpdateRole : ex = {0}", JsonConvert.SerializeObject(response));

                    response.Message = "Error while updating role";
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: RoleScopeClient :: UpdateRole : ex = {0}", ex.Message);
                return response;
            }

        }

        /// <summary>
        /// Get the list of scope from the OAuth server
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public List<ScopeModel> GetScopes(string authorization, int roleId = 0)
        {
            var scopeList = new List<ScopeModel>();

            try
            {
                //List of scope based on role
                if (roleId != 0)
                {
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/role/{roleId}");
                    var response = oauthHttpClient.Get<RolesModel>(url, authorization);
                    scopeList = response.Scopes;

                    return scopeList;
                }
                else
                {
                    //Get all the scopes
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/scopes");
                    scopeList = oauthHttpClient.Get<List<ScopeModel>>(url, authorization);

                    return scopeList;
                }

            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: RoleScopeClient :: GetScopes : ex = {0}", ex.Message);
                return scopeList;
            }
        }
    }
}
