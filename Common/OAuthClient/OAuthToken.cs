﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnraUssServices.Common.OAuthClient
{
	public class OAuthToken
	{
		[JsonProperty("token")]
		public string AccessToken { get; set; }

		[JsonProperty("type")]
		public string TokenType { get; set; }

		[JsonProperty("expires_in")]
		public int ExpiresIn { get; set; }

		[JsonProperty("scopes")]
		public string Scopes { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }


        public string GetAuthorizationToken()
		{
			return TokenType + " " + AccessToken;
		}

	}	
}
