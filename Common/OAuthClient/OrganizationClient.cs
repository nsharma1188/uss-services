﻿using AnraUssServices.Utm.Models;
using AnraUssServices.ViewModel.Operations;
using AnraUssServices.ViewModel.Organizations;
using AnraUssServices.ViewModel.Users;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AnraUssServices.Common.OAuthClient
{
    public class OrganizationClient
    {
        private readonly AnraConfiguration configuration;
        private readonly AnraHttpClient anraHttpClient;
        private readonly ILogger<OrganizationClient> logger;
        private readonly OAuthHttpClient oauthHttpClient;

        public OrganizationClient(AnraConfiguration configuration, AnraHttpClient anraHttpClient,
            ILogger<OrganizationClient> logger, OAuthHttpClient oauthHttpClient)
        {
            this.configuration = configuration;
            this.anraHttpClient = anraHttpClient;
            this.logger = logger;
            this.oauthHttpClient = oauthHttpClient;
        }

        /// <summary>
        /// Returns the ADSB source of the organization based on the organization id
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public string GetOrganizationAdsbSource(Guid organizationId, string authorization)
        {
            var adsb = string.Empty;

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/organization/{organizationId.ToString()}");
                var orginazation = oauthHttpClient.Get<OrganizationItem>(url, authorization);
                adsb = orginazation.AdsbSource;

                return adsb;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: OrganizationClient :: GetOrganizationAdsbSource : ex = {0}", ex.Message);
                return adsb;
            }
        }


        public OperationSearchResultItem GetOrganizationDetails(string organizationId, string authorization)
        {
            var organizationInfo = new OperationSearchResultItem();

            try
            {

                var organization = new OrganizationItem();

                var url = string.Concat(configuration.OAuthServerUrl, $"api/organization/{organizationId}");
                var orginazation = oauthHttpClient.Get<OrganizationItem>(url, authorization);

                if (organization != null)
                {
                    organizationInfo.UserAddressLine1 = organization.Address;
                    organizationInfo.UserEmail = organization.Email;
                    organizationInfo.UserOrganization = organization.Name;
                    organizationInfo.UserPhoneNumber = organization.ContactPhone;
                }

                return organizationInfo;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: OrganizationClient :: GetOrganizationAdsbSource : ex = {0}", ex.Message);
                return organizationInfo;
            }
        }

        /// <summary>
        /// Returns the list of organazations based on groupId or all the orginazation if groupId is null;
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public List<OrganizationItem> GetOrganizations(string authorization, string groupId = null)
        {
            var orginazationsList = new List<OrganizationItem>();

            try
            {
                if (groupId == null)
                {
                    var url = string.Concat(configuration.OAuthServerUrl, $"api/organizations");
                    orginazationsList = oauthHttpClient.Get<List<OrganizationItem>>(url, authorization);

                    return orginazationsList;
                }
                else
                {

                    var url = string.Concat(configuration.OAuthServerUrl, $"api/users/groupid/{groupId}");
                    var existingUsers = oauthHttpClient.Get<List<UserResponseModel>>(url, authorization);
                    foreach (var user in existingUsers)
                    {
                        if (!user.Roles.Exists(x => x.RoleName == EnumUserRole.ANSP.ToString() ||
                         x.RoleName == EnumUserRole.ATC.ToString()))
                        {
                            orginazationsList.Add(user.Organization);
                        }
                    }

                    //Listing only the distinct organization
                    var orginazationsId = orginazationsList.Select(x => x.OrganizationId).Distinct().ToList();
                    var newOrginazationsList = new List<OrganizationItem>();

                    foreach(var id in orginazationsId)
                    {
                        newOrginazationsList.Add(orginazationsList.Where(x => x.OrganizationId == id).FirstOrDefault());
                    }

                    return newOrginazationsList;
                }
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: OrganizationClient :: GetOrganizationAdsbSource : ex = {0}", ex.Message);
                return orginazationsList;
            }
        }

        /// <summary>
        /// Returns the Orginazation Details based on the organazation id
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public OrganizationItem GetOrganization(string organizationId, string authorization)
        {
            var orginazation = new OrganizationItem();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/organization/{organizationId}");
                orginazation = oauthHttpClient.Get<OrganizationItem>(url, authorization);

                return orginazation;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: OrganizationClient :: GetOrganizationAdsbSource : ex = {0}", ex.Message);
                return orginazation;
            }
        }

        /// <summary>
        /// Create a new orginazation
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public UTMRestResponse CreateOrganazation(OrganizationModel organization, string authorization)
        {
            var response = new UTMRestResponse();

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/organization/admin");
                response = oauthHttpClient.Post(url, authorization, organization);

                if (response.HttpStatusCode == 201)
                {
                    response.Message = "Organization Created";
                }
                else if (response.HttpStatusCode == 400)
                {
                    response.Message = "Email already exist. Please use a differet email to create organization admin.";
                }
                else
                {
                    logger.LogError("Error ::: UserClient :: CreateUser : ex = {0}", JsonConvert.SerializeObject(response));
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: CreateUser : ex = {0}", ex.Message);
                return response;
            }
        }


        /// <summary>
        ///  Update the exisitng organization
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="authorization"></param>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        public UTMRestResponse UpdateOrganization(OrganizationModel organization, string authorization, string organizationId)
        {
            var response = new UTMRestResponse();

            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/organization/{organizationId}");

                response = oauthHttpClient.Put(url, authorization, organization);

                if (response.HttpStatusCode == 200)
                {
                    response.Message = "Organization Updated Successfully";
                }
                else
                {
                    response.Message = "Error while updating organization";

                    logger.LogError("Error ::: UserClient :: CreateUser : ex = {0}", JsonConvert.SerializeObject(response));
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: CreateUser : ex = {0}", ex.Message);
                return response;
            }
        }

        /// <summary>
        ///  Update the exisitng organization
        /// </summary>
        /// <param name="authorization"></param>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        public UTMRestResponse DeleteOrganization(string authorization, string organizationId)
        {
            var response = new UTMRestResponse();
            try
            {
                var url = string.Concat(configuration.OAuthServerUrl, $"api/delete/id/{organizationId}");
                response = oauthHttpClient.Delete(url, authorization);

                if (response.HttpStatusCode == 200)
                {
                    response.Message = "Organization Removed Successfully";
                }
                else
                {
                    response.Message = "Error while removing organization";
                    logger.LogError("Error ::: UserClient :: CreateUser : ex = {0}", JsonConvert.SerializeObject(response));
                }

                return response;
            }
            catch (Exception ex)
            {
                logger.LogError("Exception ::: UserClient :: CreateUser : ex = {0}", ex.Message);
                return response;
            }
        }

        /// <summary>
        /// Get ATC org id for creating new ATC
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public string GetATCId(string authorization)
        {
            var url = string.Concat(configuration.OAuthServerUrl, $"api/organizations/atc");
            var response = oauthHttpClient.Get<List<OrganizationItem>>(url, authorization);

            return response.FirstOrDefault().OrganizationId;
        }

        /// <summary>
        /// Get ANSP org id for creating new ANSp
        /// </summary>
        /// <param name="authorization"></param>
        /// <returns></returns>
        public string GetANSPId(string authorization)
        {
            var url = string.Concat(configuration.OAuthServerUrl, $"api/organizations/ansp");
            var response = oauthHttpClient.Get<List<OrganizationItem>>(url, authorization);

            return response.FirstOrDefault().OrganizationId;
        }
    }
}
