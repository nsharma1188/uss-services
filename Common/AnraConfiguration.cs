﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AnraUssServices.Common
{
    public class AnraConfiguration
    {
        //Product Settings
        public string Product;

        public string ConnectionString;
        public string Environment;

        public string DefaultOrganization;
        public string DefaultSuperAdmin;
        public string DefaultSuperAdminPassword;
        public string DefaultAdmin;
        public string DefaultPilot;
        public string DefaultPaylodOperator;
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string CurrentVersion { get; set; }
        public string DroneLocationUrl { get; set; }

        //DSS Settings
        public string DssServiceProviderUrl { get; set; }

        //MQTT Broker Memebrs
        public string MqttBrokerHost;

        public int MqttBrokerPort;
        public string MqttBrokerUser;
        public string MqttBrokerPwd;
        public string UssServiceUrl;
        public string DetectionServiceUrl;
        public string TelemetryServiceUrl;
        public string WeatherServiceUrl;

        //FIMS Settings
        public string FimsBaseUrl { get; private set; }
		public bool InternalFIMS { get; private set; }

		/// <summary>
		/// InterUss Settings
		/// </summary>
		public string InterUssBaseUrl { get; private set; }
        public int DefaultZoomLevel { get; private set; }

		/// <summary>
		/// Email and Text settings
		/// </summary>
		public string SmsPassword;
		public string SmsEmail;
		public string SmtpHost;
		public string SendersEmail;
		public string SendersEmailPassword;
        public string ATCSupervisorEmail;

        /// <summary>
        /// Controls communication, If disabled no communication with NASA or other USS
        /// </summary>
        public bool IsUtmEnabled { get; set; }

        /// <summary>
        /// Controls DSS communication, If disabled no communication with DSS
        /// </summary>
        public bool IsDssEnabled { get; set; }

        public string ClientId { get; private set; }
        public string ApiVersion { get; private set; }
        public string Secret { get; private set; }
        public Guid DefaultUssInstanceId { get; private set; }
        public string FimsServiceUrl { get; set; }
        public string UASServiceUrl { get; set; }

        public bool EnableSensitiveDataLogging { get; set; }

        public string FlightAwareUserName { get; set; }
        public string FlightAwareApikey { get; set; }
        public string FlightAwareApiUrl { get; set; }
        public bool IsFlightAwareEnabled { get; set; }

        public string OpenskyUserName { get; set; }
        public string OpenskyApiPassword { get; set; }
        public bool IsOpenSkyEnabled { get; set; }
        public int OpenskyApiCallInterval { get; set; }

        public bool IsConformanceTriggerEnabled { get; set; }
        public bool IsCollisionDetectionEnabled { get; set; }
        public int AdsbSubscriptionDuration { get; private set; }
        public int FlightAwareCallInterval { get; private set; }
        public int UssResyncIntervalInMinutes { get; set; }        

        public int OperationVolumeMaxDuration { get; set; }
        public int MaxAllowedActiveOperationsWithinUss { get; set; }
        public int OperationVolumeMaxLength { get; set; }
        public string AnraServiceBaseUrl { get; set; }
        public string UssLiteDbPath { get; private set; }
        public string DetectionLiteDbPath { get; private set; }
        public string MslUrl { get; private set; }
        public string MslKey { get; private set; }
		public string MapEngineServiceUrl { get; set; }
		public bool IsAirSpaceBoundaryCheckEnable { get; set; }
		public bool SendUVRToFIMSEnable { get; set; }
        public bool SendAutoPositionRequest { get; set; }
        public bool IsUssGridValidationEnabled { get; set; }
		public bool IsSimulatedFlight { get; set; }
        public bool ATCApprovalRequired { get; set; }
        public int VolumeSplittingLength { get; set; }
        public int AltitudeBuffer { get; set; }

        //RelmaTech Settings
        public string RelmaTechBaseUrl { get; private set; }        
        public string RelmaTechUserName { get; private set; }
        public string RelmaTechMd5Pwd { get; private set; }

		/// <summary>
		/// OAuth Server Clients 
		/// </summary>
		public string OAuthClientId { get; private set; }
		public string OAuthSecret { get; private set; }
		public string OAuthServerUrl { get; private set; }

        //Billing Service
        public string BillingServiceUrl { get; set; }

        //KafkaSettings
        public string KafkaUrl { get; set; }
        public string KafkaPort { get; set; }
        public string KafkaGroupId { get; set; }

        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(new AnraConfiguration
            {
                EnableSensitiveDataLogging = configuration.GetSection("Logging").GetValue<bool>("EnableSensitiveDataLogging"),
                Product = configuration.GetSection("ProductSettings").GetValue<string>("Product"),
                CurrentVersion = configuration.GetSection("ProductSettings").GetValue<string>("CurrentVersion"),
                IsUtmEnabled = configuration.GetSection("ProductSettings").GetValue<bool>("IsUtmEnabled"),
                IsDssEnabled = configuration.GetSection("ProductSettings").GetValue<bool>("IsDssEnabled"),
                DroneLocationUrl  = configuration.GetSection("ProductSettings").GetValue<string>("DroneLocationUrl"),
                ConnectionString = configuration.GetConnectionString("DataAccessPostgreSqlProvider"),
                UssLiteDbPath = configuration.GetConnectionString("UssLiteDbPath"),
                DetectionLiteDbPath = configuration.GetConnectionString("DetectionLiteDbPath"),
                Environment = configuration.GetSection("ProductSettings").GetValue<string>("Environment"),
                UssServiceUrl = configuration.GetSection("ProductSettings").GetValue<string>("UssServiceUrl"),
                TelemetryServiceUrl = configuration.GetSection("ProductSettings").GetValue<string>("TelemetryServiceUrl"),
                WeatherServiceUrl = configuration.GetSection("ProductSettings").GetValue<string>("WeatherServiceUrl"),
                DssServiceProviderUrl = configuration.GetSection("ProductSettings").GetValue<string>("DssServiceProviderUrl"),
                DefaultUssInstanceId = configuration.GetSection("ProductSettings").GetValue<Guid>("DefaultUssInstanceId"),
                DetectionServiceUrl = configuration.GetSection("ProductSettings").GetValue<string>("DetectionServiceUrl"),
                IsConformanceTriggerEnabled = configuration.GetSection("ProductSettings").GetValue<bool>("IsConformanceTriggerEnabled"),
                IsCollisionDetectionEnabled = configuration.GetSection("ProductSettings").GetValue<bool>("IsCollisionDetectionEnabled"),
                UssResyncIntervalInMinutes = configuration.GetSection("ProductSettings").GetValue<int>("UssResyncIntervalInMinutes"),
                OperationVolumeMaxDuration = configuration.GetSection("ProductSettings").GetValue<int>("OperationVolumeMaxDuration"),
                MaxAllowedActiveOperationsWithinUss = configuration.GetSection("ProductSettings").GetValue<int>("MaxAllowedActiveOperationsWithinUss"),
                OperationVolumeMaxLength = configuration.GetSection("ProductSettings").GetValue<int>("OperationVolumeMaxLength"),
                AnraServiceBaseUrl = configuration.GetSection("ProductSettings").GetValue<string>("ServiceBaseUrl"),
				MapEngineServiceUrl = configuration.GetSection("ProductSettings").GetValue<string>("MapEngineServiceUrl"),
				IsAirSpaceBoundaryCheckEnable = configuration.GetSection("ProductSettings").GetValue<bool>("IsAirSpaceBoundaryCheckEnable"),
				SendUVRToFIMSEnable = configuration.GetSection("ProductSettings").GetValue<bool>("SendUVRToFIMSEnable"),
                SendAutoPositionRequest = configuration.GetSection("ProductSettings").GetValue<bool>("SendAutoPositionRequest"),
                IsUssGridValidationEnabled = configuration.GetSection("ProductSettings").GetValue<bool>("IsUssGridValidationEnabled"),
				IsSimulatedFlight = configuration.GetSection("ProductSettings").GetValue<bool>("IsSimulatedFlight"),
                ATCApprovalRequired = configuration.GetSection("ProductSettings").GetValue<bool>("ATCApprovalRequired"),
                VolumeSplittingLength = configuration.GetSection("ProductSettings").GetValue<int>("VolumeSplittingLength"),
                AltitudeBuffer = configuration.GetSection("ProductSettings").GetValue<int>("AltitudeBuffer"),


                MqttBrokerHost = configuration.GetSection("MQTTBrokerSettings").GetValue<string>("MqttBrokerHost"),
                MqttBrokerPort = configuration.GetSection("MQTTBrokerSettings").GetValue<int>("MqttBrokerPort"),
                MqttBrokerUser = configuration.GetSection("MQTTBrokerSettings").GetValue<string>("MqttBrokerUser"),
                MqttBrokerPwd = configuration.GetSection("MQTTBrokerSettings").GetValue<string>("MqttBrokerPwd"),

                FimsBaseUrl = configuration.GetSection("FIMSSettings").GetValue<string>("FimsBaseUrl"),
                FimsServiceUrl = configuration.GetSection("FIMSSettings").GetValue<string>("FimsServiceUrl"),
                ClientId = configuration.GetSection("FIMSSettings").GetValue<string>("ClientId"),
                Secret = configuration.GetSection("FIMSSettings").GetValue<string>("Secret"),
                ApiVersion = configuration.GetSection("FIMSSettings").GetValue<string>("ApiVersion"),
				InternalFIMS = configuration.GetSection("FIMSSettings").GetValue<bool>("InternalFIMS"),


				DefaultOrganization = "ANRA Technologies",
                DefaultSuperAdmin = "admin@flyanra.com",
                DefaultSuperAdminPassword = "AnRaBaSe1$",
                DefaultAdmin = "sales@flyanra.com",
                DefaultPilot = "rkumar@flyanra.com",
                DefaultPaylodOperator = "anubhav.pandey@flyanra.com",
                Issuer = "AnraTechnologies",
                Audience = "AnraUsers",
                UASServiceUrl = configuration.GetSection("FIMSSettings").GetValue<string>("UASsBaseUrl") + "api/",

                FlightAwareUserName = configuration.GetSection("FlightAware").GetValue<string>("UserName"),
                FlightAwareApikey = configuration.GetSection("FlightAware").GetValue<string>("Apikey"),
                FlightAwareApiUrl = configuration.GetSection("FlightAware").GetValue<string>("ApiUrl"),
                IsFlightAwareEnabled = configuration.GetSection("FlightAware").GetValue<bool>("IsEnabled"),
                FlightAwareCallInterval = configuration.GetSection("FlightAware").GetValue<int>("FlightAwareApiCallInterval"),
                InterUssBaseUrl = configuration.GetSection("InterUssSettings").GetValue<string>("InterUssBaseUrl"),
                DefaultZoomLevel = configuration.GetSection("InterUssSettings").GetValue<int>("DefaultZoomLevel"),
                MslUrl = configuration.GetSection("Msl").GetValue<string>("MslUrl"),
                MslKey = configuration.GetSection("Msl").GetValue<string>("key"),

                IsOpenSkyEnabled = configuration.GetSection("OpenSky").GetValue<bool>("IsEnabled"),
                OpenskyApiCallInterval=configuration.GetSection("OpenSky").GetValue<int>("openskyApiCallInterval"),
                AdsbSubscriptionDuration = (configuration.GetSection("OpenSky").GetValue<bool>("IsEnabled") ? configuration.GetSection("OpenSky").GetValue<int>("SubscriptionDurationInMinutes") : configuration.GetSection("FlightAware").GetValue<int>("SubscriptionDurationInMinutes")),

                RelmaTechBaseUrl = configuration.GetSection("RelmaTechSettings").GetValue<string>("RelmaTechBaseUrl"),
                RelmaTechUserName = configuration.GetSection("RelmaTechSettings").GetValue<string>("RelmaTechUserName"),
                RelmaTechMd5Pwd = configuration.GetSection("RelmaTechSettings").GetValue<string>("RelmaTechMd5Pwd"),

				SmsEmail = configuration.GetSection("EmailTextSettings").GetValue<string>("SmsEmail"),
				SmsPassword = configuration.GetSection("EmailTextSettings").GetValue<string>("SmsPassword"),
				SendersEmail = configuration.GetSection("EmailTextSettings").GetValue<string>("SendersEmail"),
				SendersEmailPassword = configuration.GetSection("EmailTextSettings").GetValue<string>("SendersEmailPassword"),
				SmtpHost = configuration.GetSection("EmailTextSettings").GetValue<string>("SmtpHost"),
                ATCSupervisorEmail = configuration.GetSection("EmailTextSettings").GetValue<string>("ATCSupervisorEmail"),

                OAuthServerUrl = configuration.GetSection("FIMSSettings").GetValue<string>("OAuthServerUrl"),
				OAuthClientId = configuration.GetSection("FIMSSettings").GetValue<string>("OAuthClientId"),
				OAuthSecret = configuration.GetSection("FIMSSettings").GetValue<string>("OAuthSecret"),

                BillingServiceUrl = configuration.GetSection("BillingSettings").GetValue<string>("BillingServiceUrl"),

                KafkaUrl = configuration.GetSection("KafkaServerSettings").GetValue<string>("KafkaUrl"),
                KafkaPort = configuration.GetSection("KafkaServerSettings").GetValue<string>("KafkaPort"),
                KafkaGroupId = configuration.GetSection("KafkaServerSettings").GetValue<string>("KafkaGroupId")
            });
        }
    }
}