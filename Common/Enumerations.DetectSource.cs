﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace AnraUssServices.Common
{
    public enum DetectSourceEnum : short
    {
        [Description("DSRC"), EnumMember(Value = "DSRC")]
        DSRC = 1,

        [Description("RADAR"), EnumMember(Value = "RADAR")]
        RADAR = 2,

        [Description("RF"), EnumMember(Value = "RF")]
        RF = 3,

        [Description("ADSB"), EnumMember(Value = "ADSB")]
        ADSB = 4,

        [Description("COLLISION"), EnumMember(Value = "COLLISION")]
        COLLISION = 5
    }
}